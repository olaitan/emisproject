<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\School;

Route::get('/', function () {
    return view('currentIndex');
});

Route::get('/new', function () {
    return view('currentAddSchool');
});

Route::get('/search', function () {
    return view('currentSearch');
})->name('search_schools');

//Offline
Route::get('/offline_data_entry', function () {
    return view('offline_entry');
})->name('offline_data_entry');

Route::get('/offline_batch_upload', function () {
    return view('offlineBatchUpload');
})->name('offline_batch_upload');

//Reports
Route::get('/general_report', function () {
    return view('general_report');
})->name('general_report');

Route::get('/school_characteristics_report', function () {
    return view('school_characteristics_report');
})->name('school_characteristics_report');

Route::get('/enrolment_report', function () {
    return view('enrolment_report');
})->name('enrolment_report');

Route::get('/repeaters_report', function () {
    return view('repeaters_report');
})->name('repeaters_report');

Route::get('/school_facility_report', function () {
    return view('school_facility_report');
})->name('school_facility_report');

Route::get('/school_streams_report', function () {
    return view('school_streams_report');
})->name('school_streams_report');

Route::get('/school_teachers_report', function () {
    return view('school_teachers_report');
})->name('school_teachers_report');

Route::get('/metadatas', function () {
    return view('metadataConfig');
})->name('Config_Metadatas');

Route::get('/login', function () {
    return view('login');
})->name('login');

Route::get('/user', function () {
    return view('profile');
})->name('profile');

Route::get('/edituser', function () {
    return view('accounts');
})->name('account');

Route::get('/configure', function () {
    return view('configure');
})->name('configure');

Route::get('/userlogs', function () {
    return view('userlogs');
})->name('configure');

Route::get('/userID', function () {
    return view('userprofile');
})->name('userprofile');

Route::get('/stateMetadatas', function () {
    return view('statemetadata');
})->name('statemetadata');

Route::get('/api', function () {
    return view('api');
})->name('api');

Route::get('/clientAPI', function () {
    return view('client');
})->name('client');

Route::get('/lgaMetadatas', function () {
    return view('lga');
})->name('lga');

Route::get('/generic/search',"SchoolController@genericsearch")->name('genericsearch_schools');

Route::get('/school/{schoolcode}/year/{year}',"SchoolController@showschoolform")->name('schoolform');

Route::get('/school/{schoolcode}/year/{year}/preview',"SchoolController@showschoolformpreview")->name('schoolformpreview');
