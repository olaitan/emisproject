<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/Testing',function (Request $request) {
    return "Got your request and here is my response";
})->name('index');

//Route for schoolcontroller

Route::match(['get', 'post'],'generic/search/schools',"SchoolController@gsearch")->name('genericsearch_schools');


//Route::get('/school/{schoolcode}/year/{year}', "SchoolController@schoolform")->name('school_form');

//Route for locationcontroller
Route::get('/states', "LocationController@states")->name('states');
Route::post('/Lga/Save', "LocationController@savelgas")->name('savelgas');
Route::post('/Lga/Delete', "LocationController@deletelgas")->name('deletelgas');

//Route for userlogs
Route::get('/userlogs', "ApiAuth\UserController@getuserlogs")->name('userlogs');

//Route for metadatacontroller
Route::get('/search/metadata',"MetadataController@searchmetadata")->name('searchmetadata');
Route::get('schoolcharacteristics/metadata/{formtype}', "MetadataController@schoolcharacteristicsmetadata")->name('schchar_metadata');

//Route for staff
Route::get('/staffs/{schoolcode}/year/{year}', "StaffController@staffs")->name('staffs');

//get roles
Route::get('/roles', "ApiAuth\UserController@getroles")->name('getroles');

//census year
Route::get('/CensusYear/Current', "SchoolController@getcensusyear")->name('getcensusyear');
Route::post('Section/CensusYear/Set', "SchoolController@setcensusyear");

//Oauth
//Route::post('Login', 'ApiAuth\UserController@login');
Route::post('Section/AppClient/Register', 'ApiAuth\UserController@register_appclient');

Route::post('Section/User/Remove', 'ApiAuth\UserController@remove');
Route::post('Section/User/Change', 'ApiAuth\UserController@change');
Route::post('Section/User/SavePassword', 'ApiAuth\UserController@savepassword');

Route::post('Section/User/SearchEmail', 'ApiAuth\UserController@searchemail');
Route::post('Section/User/CreatePassword', 'ApiAuth\UserController@createpassword');
Route::post('Section/User/Login', 'ApiAuth\UserController@login');
Route::post('Section/AppClient/Login', 'ApiAuth\UserController@login_appclient');

    //Reports
    //Route::match(['get', 'post'],'/reports/search/schools',"ReportsController@general_search")->name('general_reports_search_schools');


//Api Auth
Route::group(['middleware' => 'auth:api'], function(){
    //reports
    Route::match(['get', 'post'],'/reports/search/schoolcharacteristics',"ReportsController@characteristics_search")->name('characteristics_reports_search_schools');
    Route::match(['get', 'post'],'/reports/search/enrolment',"ReportsController@enrolment_search")->name('enrolment_reports_search_schools');
    Route::match(['get', 'post'],'/reports/search/repeaters',"ReportsController@repeaters_search")->name('repeaters_reports_search_schools');
    Route::match(['get', 'post'],'/reports/search/school_facility',"ReportsController@facility_search")->name('school_facility_reports_search_schools');
    Route::match(['get', 'post'],'/reports/search/school_streams',"ReportsController@streams_search")->name('school_streams_reports_search_schools');
    Route::match(['get', 'post'],'/reports/search/school_teachers',"ReportsController@teachers_search")->name('school_teachers_reports_search_schools');
    Route::match(['get', 'post'],'/reports/overview',"ReportsController@overview")->name('reports_overview');

    
    Route::post('Section/User/SaveProfile', 'ApiAuth\UserController@saveprofile');
    Route::post('Section/User/SaveProfilePic', 'ApiAuth\UserController@picUpload');

    Route::match(['get', 'post'],'/search/schools',"SchoolController@search")->name('search_schools');
    

    //get metadata for all the forms
    Route::get('/all/metadata','MetadataController@all_metadata');

    //save the state theme
    Route::post('State/Theme', 'SchoolController@savetheme');

    //get the school data
    Route::match(['get','post'],'/school/{schoolcode}/year/{year}', "SchoolController@schoolform")->name('school_form');


    Route::post('Section/AppClient/Logout', 'ApiAuth\UserController@logoutApiAppclient');
    Route::post('Section/User/Logout', 'ApiAuth\UserController@logoutApi');
    Route::post('Details', 'ApiAuth\UserController@details');
    Route::post('Section/User/Addnew', 'ApiAuth\UserController@addnew');
 
    //Route for SchoolIdentification section A
    //Get the school identification
    Route::post('Section/SchoolRegistration',"SchoolController@registerschool");
    Route::post('Section/SchoolIdentification',"SchoolIdentificationController@store");
    //Route for SchoolCharacteristics section B
    //Get the school characteristics
    Route::post('Section/SchoolCharacteristics',"SchoolController@saveschoolcharacteristics");
    Route::post('Section/Add/School',"SchoolController@addschool");
    Route::post('Section/Remove/SchoolLevel',"SchoolController@removeschoollevel");

    //Route for SchoolEnrollment 
    //Get the birthcertificate
    Route::post('Section/SchoolEnrollment/BirthCertificate',"EnrollmentController@savebirthcertificate");
    //Get the entrant
    Route::post('Section/SchoolEnrollment/Entrant',"EnrollmentController@saveentrant");
    //Get the enrollment
    Route::post('Section/SchoolEnrollment/EnrollmentByAge',"EnrollmentController@saveenrollmentbyage");
    //Get the studentflow
    Route::post('Section/SchoolEnrollment/StudentFlow',"EnrollmentController@savestudentflow");
    //Get the specialneeds
    Route::post('Section/SchoolEnrollment/SpecialNeeds',"EnrollmentController@savespecialneeds");
    //Get the orphan
    Route::post('Section/SchoolEnrollment/Orphan',"EnrollmentController@saveorphan");
    //Get the examination
    Route::post('Section/SchoolEnrollment/Examination',"EnrollmentController@saveexamination");

    //Route for Staff
    //Get the staff
    Route::post('Section/Staff/NoOfStaff',"StaffController@savenumberofstaffs");
    Route::post('Section/Staff/Addnew',"StaffController@savestaff");
    Route::post('Section/Staff/Addlist',"StaffController@savestaffs");
    Route::post('Section/Staff/Remove',"StaffRemovalController@removestaff");

    //Route for classroom
    //Get the classroom
    Route::post('Section/Classroom/NoOfClassroom',"ClassroomsController@savenumberofclassrooms");
    Route::post('Section/Classroom/Addnew',"ClassroomsController@saveclassroom");
    Route::post('Section/Classroom/Remove',"ClassroomsController@removeclassroom");
    Route::post('Section/Room/Otherroom',"ClassroomsController@saveotherrooms");
    Route::post('Section/Classroom/Addlist',"ClassroomsController@saveclassrooms");

    //Route for workshop
    //Get the workshop
    Route::post('Section/Workshop/Addnew',"WorkshopController@saveworkshop");
    Route::post('Section/Workshop/Addlist',"WorkshopController@saveworkshops");
    Route::post('Section/Workshop/Remove',"WorkshopController@removeworkshop");

    //Route for facility
    //Get the drinking water
    Route::post('Section/Facility/DrinkingWater',"FacilityController@savewatersource");
    //Get the facilities available
    Route::post('Section/Facility/Available',"FacilityController@savefacilitiesavailable");
    //Get the shared facilities
    Route::post('Section/Facility/SharedFacilities',"FacilityController@savesharedfacilities");
    //Get the toilet type
    Route::post('Section/Facility/Toilet',"FacilityController@savetoilettype");
    //Get the source of power
    Route::post('Section/Facility/PowerSource',"FacilityController@savesourceofpower");
    //Get the health facility
    Route::post('Section/Facility/HealthFacility',"FacilityController@savehealthfacility");
    //Get the fence facility
    Route::post('Section/Facility/FenceFacility',"FacilityController@savefencefacility");
    //Get the seaters
    Route::post('Section/Facility/Seater',"FacilityController@saveseaters");
    //Get the ownership
    Route::post('Section/Facility/Ownership',"FacilityController@saveownership");
    //Get the school building
    Route::post('Section/Facility/SchoolBuilding',"FacilityController@saveschoolbuilding");
    //Get the play room
    Route::post('Section/Facility/PlayRoom',"FacilityController@saveplayroom");
    //Get the play facility
    Route::post('Section/Facility/PlayFacilities',"FacilityController@saveplayfacility");
    //Get the learning material
    Route::post('Section/Facility/LearningMaterial',"FacilityController@savelearningmaterial");

    Route::post('Section/Remove/DrinkingSource',"FacilityController@removedwsource");
    Route::post('Section/Remove/SharedFacility',"FacilityController@removesharedfacility");
    Route::post('Section/Remove/PowerSource',"FacilityController@removepowersource");
    Route::post('Section/Remove/LearningMaterial',"FacilityController@removelearningmaterial");
    Route::post('Section/Remove/PlayFacility',"FacilityController@removeplayfacility");

    //Route for students by subject
    //Get the subjects
    Route::post('Section/Subject/Students',"StudentBySubjectController@savesubjects");

    //Route for books
    //Get the books
    Route::post('Section/Book/Student',"StudentTeacherBookController@savestudents");
    Route::post('Section/Book/Teacher',"StudentTeacherBookController@saveteachers");

    //Route for teachingqualification
    //Get the teaching qualification
    Route::post('Section/Teacher/Qualification',"TeacherQualificationController@saveteachersqualification");

    //Route for caregiversmanual
    //Get the caregivers manual
    Route::post('Section/Caregivers/Manual',"CaregiverManualController@savecgmanual");

    //Route for undertaking
    //Get the undertaking
    Route::post('Section/Undertaking',"UndertakingController@saveundertaking");

    Route::match(['get', 'post'],'/search/state/schools',"SchoolController@stateschools")->name('stateschools');

    //Route for metadatacontroller
    Route::get('/metadatas', "MetadataController@getmetadatas")->name('metadatas');
    Route::post('/Metadata/Save', "MetadataController@savemetadatas")->name('savemetadatas');
    Route::post('/Metadata/Delete', "MetadataController@deletemetadatas")->name('deletemetadatas');

});
