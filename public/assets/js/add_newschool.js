//vue vm for the form

var app = new Vue({
    el: '#app',
    data: {
        results: [],
        resultsSave: [],
        //school identification
        schoolcode: '',
        schoolname: '',
        schoolstreet: '',
        schooltype:'',
        schoolemail: '',
        schoollga: '',
        schooltown: '',
        schooltelephone: '',
        schoolstate: '',
        fetch_states: [],
        state_picked: '',
        state_index: [],
        get_state_code: '',

        results_lga: [],
        fetch_lga: [],
        lga_picked: '',
        lga_index: [],
        get_lga_code: '',

        counter_state: 0,
        counter_lga: 0,

        location_type_ref: [],
        location_type_val: [],
        metadata: [],

        //metadata arrays
        fetch_location: [],
        fetch_levels: [],
        fetch_category: [],
    
        year: '',
        auth_user:'',
        config:'',
        fetch_yearslist:[],
        IsPublic:false,
        IsVS:false,
        IsPrivate:false,
        schoollevel:'',
        level_of_educations:[],
        user_role:0,
        user_state:'',
        schoolward:''

    },

    mounted: function(){
        if(this.$cookies.isKey("user")){
            var dt=new Date();
            for(i=dt.getFullYear();i>1859;i--){
                this.fetch_yearslist.push(i);
            }
            //get the access token
            this.user=this.$cookies.get("user");
            this.config = {
                headers: {
                  Accept: "application/json",
                  Authorization:"Bearer "+this.user.session,
                }
              }
              //setting the theme color
            var leftTab = document.querySelectorAll('.theme');
            for (let i = 0; i < leftTab.length; i++) {
                leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
            } 
            axios.post("/api/Details",
            {
                email:this.user.email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.auth_user=response.data;
                this.user_role=this.auth_user.role_id;
                this.user_state=this.auth_user.state;
            }).catch(error=>{
                this.$cookies.remove("user");
                window.location.assign("/login");
            })

            axios.get("/api/states").then(response => {

                this.fetch_states = response.data;//returns the json data
    
                //this.results =  JSON.parse(response.data);
                //console.log(this.results);
                console.log('r: ', JSON.stringify(response.data, null, 2));
                
            });


        }else{
            window.location.assign("/login");
        }
    },

    methods:{
        changelevel:function(){
            if(this.schooltype=='public'){
                this.IsPublic=true;
                this.IsVS=false;
                this.IsPrivate=false;
            }else if(this.schooltype=='sciencevocational'){
                this.IsVS=true;
                this.IsPublic=false;
                this.IsPrivate=false;
            }else if(this.schooltype=='private'){
                this.IsPublic=false;
                this.IsVS=false;
                this.IsPrivate=true;
            }
        },
           //log out
        logout:function(){
            axios.post("/api/Section/User/Logout",
            {
                email:this.user_email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.$cookies.remove("user");
                window.location.assign("/login");
            })
        },
        getLGA: function () {
            var arrayOfObjects2 = [];

            for (var i = 0; i < this.fetch_states[this.state_picked].data.lgas.length; i++) {
                var obj2 = {};
                obj2["id"] = this.fetch_states[this.state_picked].data.lgas[i].data.id;
                obj2["name"] = this.fetch_states[this.state_picked].data.lgas[i].data.name;
                obj2["lgacode"] = this.fetch_states[this.state_picked].data.lgas[i].data.lgacode;
                obj2["index"]=i+1;
                arrayOfObjects2.push(obj2);
            }
            
            this.fetch_lga = arrayOfObjects2;
        },
        popAlertMsg: function (message) {
            var alertBox = document.getElementById('alert');
            this.responseMsg = message;
            if (message != 'success'){
                alertBox.style.backgroundColor = '#ff0000';
            }
            alertBox.classList.add('active');
            setTimeout(() => {
                alertBox.classList.replace('active', null);
                this.responseMsg = '';

                //clear the field after successfull school creation
                this.year="";
                this.schooltype="";
                this.schoollevel="";
                this.level_of_educations="";
                this.schoolname="";
                this.schoolstreet="";
                this.schooltown="";
                this.state_picked="";
                this.lga_picked="";
                this.schooltelephone="";
                this.schoolemail="";
            }, 1500);
        },
        addschool: function(){
            //generate school code
                this.get_state_code = this.fetch_states[this.state_picked].data.name;
                this.get_lga_code = this.fetch_lga[this.lga_picked];
                
                var temp={
                    year:this.year,
                    school_type:this.schooltype,
                    schoollevel:this.schoollevel,
                    level_of_educations:this.level_of_educations,
                    school_name: this.schoolname,
                    school_address: this.schoolstreet,
                    town: this.schooltown,
                    lga: this.get_lga_code.name,
                    lgacode:this.get_lga_code.index,
                    state: this.get_state_code,
                    telephone: this.schooltelephone,
                    email: this.schoolemail,
                    ward:this.schoolward
                };
                console.log(temp);
            axios.post("/api/Section/Add/School",
                    {
                        year:this.year,
                        school_type:this.schooltype,
                        schoollevel:this.schoollevel,
                        level_of_educations:this.level_of_educations,
                        school_name: this.schoolname,
                        school_address: this.schoolstreet,
                        town: this.schooltown,
                        lga: this.get_lga_code.name,
                        lgacode:this.get_lga_code.index,
                        state: this.get_state_code,
                        telephone: this.schooltelephone,
                        email: this.schoolemail,
                        ward:this.schoolward
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            this.schoolcode=this.resultsSave.schoolcode;
                            //run 
                            this.popAlertMsg("success");
                        } else {
                            alert("Check the form for the required data");
                        }
                    });
        }

    },
    
});