(function () {
  const deviceWidth = window.innerWidth;  
  const links = document.querySelectorAll('div.links p');
  const tabP = document.querySelectorAll('div#left-tab a>p'); 
  
  
  const left = document.getElementById('left-tab');
  const right = document.getElementById('right-tab');
  
  
  function toggleResponsive(arr) {
    arr.forEach(element => {
      element.classList.add('minimize');
    });
  }
  
  if (deviceWidth < 767) {
    left.classList.add('minimize');
    if (right) {
      right.classList.add('minimize');
    }
    toggleResponsive(links);
    toggleResponsive(tabP);
  }

  window.addEventListener('resize', function () {
    if (window.innerWidth < 767) {
      left.classList.add('minimize');
      if (right) {
        right.classList.add('minimize');
      }
      toggleResponsive(links);
      toggleResponsive(tabP);
    }
  });
  
})();


// function checkScreen() {
// }
// checkScreen();


$(document).ready(function() {
  var nextButton = $("button#next");
  var previousButton = $("button#previous");
  var saveButton = $("button#save");
  //var divTab = $("div.tab-contents>div").length-1;
  var i = 1;
  //var searchBTN = $('#searchBTN');
  //$("div#loader").addClass("hide");


  //Next Button Action
  // nextButton.bind("click", function() {
  //   var forms = $("div#tab" + i + " form");
  //   //forms.parsley().validate();
  //   // if (!forms.parsley().isValid()) {
  //   //   return true;
  //   // } else {
  //     $("div#loader")
  //       .removeClass("hide")
  //       .addClass("show");
  //     $("div.tab-contents").addClass("hide");
  //     $(this)
  //       .parents("div#tab" + i)
  //       .removeClass("show-current-tab");
  //     setTimeout(showLoader, 800);
  //     //setTimeout(removeAlert, 500);
  //     function showLoader() {
  //       i++;
  //       $("div#tab" + i).addClass("show-current-tab");
  //       $("div#loader")
  //         .removeClass("show")
  //         .addClass("hide");
  //       $("div.tab-contents")
  //         .removeClass("hide")
  //         .addClass("show animate-bottom");
  //       $("div#alert").addClass("active");
  //     }
  //     setTimeout(function() {
  //       $("div#alert").removeClass("active");
  //     }, 2500);
    
  // });

  //Save Button Action
  // saveButton.bind("click", function() {
  //   $("div#loader").removeClass("hide").addClass("show");
  //   setTimeout(showLoader, 1000);
  //   function showLoader() {
  //     $("div#tab" + i).addClass("show-current-tab");
  //     $("div#loader").removeClass("show").addClass("hide");
  //   }
  // });
    // $("div.tab-contents").removeClass("hide").addClass("show animate-bottom");
    // $("div#alert").addClass("active");
    // $("div.tab-contents").addClass("hide");
    // setTimeout(removeAlert, 500);
    // setTimeout(function() {
    //   // $("div#alert").removeClass("active");
    // }, 2500);

  //Previous Button Action
  // previousButton.bind("click", function() {
  //   $("div#loader")
  //     .removeClass("hide")
  //     .addClass("show");
  //   $("div.tab-contents").addClass("hide");
  //   $(this)
  //     .parents("div#tab" + i)
  //     .removeClass("show-current-tab");
  //   setTimeout(preShowLoader, 1000);
  //   function preShowLoader() {
  //     i--;
  //     $("div#tab" + i).addClass("show-current-tab");
  //     $("div#loader")
  //       .removeClass("show")
  //       .addClass("hide");
  //     $("div.tab-contents")
  //       .removeClass("hide")
  //       .addClass("show animate-bottom");
  //   }
  // });

  $("#addSchool").bind("click", function() {
    $("div#alert").addClass("active");
  });

  setInterval(removeAlert, 500);
  const aler = $("div#alert");
  function removeAlert() {
    if (aler.hasClass("active")) {
      $(this).removeClass("active");
    }
  }

  $("span#collapse").bind("click", function() {
    $("div#left-tab").toggleClass("minimize");
    $("div#left-tab>div.logo>a>p").toggleClass("minimize");
    $("div#left-tab>div.links>ul>li>a>p").toggleClass("minimize");
    $("div#right-tab").toggleClass("minimize");
  });

  //Add Button form
  var addButton = $("button#addBtn");
  var dialogSubmitBtn = $("button#submit-staff");
  var closeDialog = $("div#cancel");
  var coverLayer = $("div#coverlayer");

  addButton.bind("click", function() {
    $("div#coverLayer").addClass("active");
    $("div#dialog-form").addClass("active");
  });
  dialogSubmitBtn.bind("click", function() {
    alert("Submitted");
    //How to use ajax to send information
  });
  closeDialog.bind("click", function() {
    $("div#coverLayer").removeClass("active");
    $("div#dialog-form").removeClass("active");
  });
  coverLayer.bind("click", function() {
    $("div#coverLayer").removeClass("active");
    $("div#dialog-form").removeClass("active");
  });

  $("input").change(function() {
    $(this).addClass("editted");
  });
  $("input[type='number']").change(function() {
    $(this).addClass("editted");
  });

  //School Add Button form
  var addSchButton = $("button#addClassBtn");
  var workshopButton = $("button#addWorkBtn");
  var SchSubmitBtn = $("button#submitClassRoom");
  var dialogCancelBt = $("button#cancelClassRoom");
  var coverLayera = $("div#coverlayer");
  var cancelWorks = $("button#cancelWks");
  addSchButton.bind("click", function() {
    $("div#coverLayer").addClass("active");
    $("div#addClassRooms").addClass("active");
  });
  workshopButton.bind("click", function() {
    $("div#coverLayer").addClass("active");
    $("div#addWorkShop").addClass("active");
  });
  SchSubmitBtn.bind("click", function() {
    alert("Submitted");
    //How to use ajax to send information
  });
  dialogCancelBt.bind("click", function() {
    $("div#coverLayer").removeClass("active");
    $("div#addClassRooms").removeClass("active");
  });
  cancelWorks.bind("click", function() {
    $("div#coverLayer").removeClass("active");
    $("div#addWorkShop").removeClass("active");
  });
  coverLayera.bind("click", function() {
    $("div#coverLayer").removeClass("active");
    $("div#addClassRooms").removeClass("active");
  });

  //Changing forms in dialog
  var newStaff = $("button#addStaff");
  newStaff.bind("click", function() {
    if ($("div.removeStaff").hasClass("active")) {
      $("div.removeStaff").removeClass("active");
    }
    if ($("div.editStaff").hasClass("active")) {
      $("div.editStaff").removeClass("active");
    }
    $("div.addNewStaff").addClass("active");
  });

  //Edit Staff in dialog
  var EditStaff = $("p#editStaff");
  EditStaff.bind("click", function() {
    if ($("div.addNewStaff").hasClass("active")) {
      $("div.addNewStaff").removeClass("active");
    }
    if ($("div.removeStaff").hasClass("active")) {
      $("div.removeStaff").removeClass("active");
    }
    $("div.editStaff").addClass("active");
  });
  //Remove Staff in dialog
  var removeStaff = $("p#removeStaff");
  removeStaff.bind("click", function() {
    if ($("div.addNewStaff").hasClass("active")) {
      $("div.addNewStaff").removeClass("active");
    }
    if ($("div.editStaff").hasClass("active")) {
      $("div.editStaff").removeClass("active");
    }
    $("div.removeStaff").addClass("active");
  });

  /* Staff List */
  var moreActions = $("td#actionBtn");
  var removeButton = $("button#Remove");
  removeButton.click(function() {
    $("div#clearStaff").removeClass("active");
  });
  moreActions.click(function() {
    $("div#clearStaff").addClass("active");
  });

  $("#myTab a").on("click", function(e) {
    e.preventDefault();
    $("#myTab a").removeClass("active");
    $(this).addClass("active");
  });

  /* User Profile Page */

  const tabMove = $("ul.seed>li>a");
  tabMove.bind("click", function(e) {
    e.preventDefault();
    tabMove.removeClass("active");
    $(this).addClass("active");
    $("div.tabContent>div").removeClass("active");
    let a = $(this).attr("href");
    $("div.tabContent>div" + a).addClass("active");
  });

  var exitRemove = document.getElementById("closers");
  if (exitRemove) {
    exitRemove.addEventListener("click", function() {
      document.getElementById("clearStaff").classList.remove("active");
    });
  }
});

/* var btnChangeTheme = document.querySelector('#saveColor');
var themeColor = document.querySelector('#colorTheme');
btnChangeTheme.addEventListener('click', ()=>{
    var leftTab = document.querySelectorAll('.theme');
    for (let i = 0; i < leftTab.length; i++) {
        leftTab[i].style.setProperty('--themeColor', themeColor.value);
    }  
}) */



/* For Collapsing Extra Links */
window.addEventListener("load", () => {
  let extraSettingLink = document.getElementById("moreLinks");
  extraSettingLink.addEventListener("click", function(el) {
    el.preventDefault();
    document.getElementById("extraLinks").classList.toggle("active");
  });

  let extraReportLink = document.getElementById("moreReportLinks");
  extraReportLink.addEventListener("click", function(el) {
    el.preventDefault();
    document.getElementById("extraReportLinks").classList.toggle("active");
  });

  let extraOfflineLink = document.getElementById("moreOfflineLinks");
  extraOfflineLink.addEventListener("click", function(el) {
    el.preventDefault();
    document.getElementById("extraOfflineLinks").classList.toggle("active");
  });

});


