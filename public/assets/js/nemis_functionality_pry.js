var url = window.location.href;//get the school code and year by splitting the url
var res = url.split("/");
var schoolcode = res[4];
var year = res[6];

const customMessages = {
    custom: {
      'School Name': {
        required: 'Required: Please enter School Name',
        alpha: 'Alpha: Please enter alpha chars only.'
      }
    }
  };
var app = new Vue({
    el: '#app',
    data: {
        //School Identification errors
        school_identification_errors: [],

        responseMsg: '',
        results: [],
        resultsSave: [],
        //school identification
        schoolcode: '',
        schoolname: '',
        xcoordinate:'',
        ycoordinate:'',
        zcoordinate:'',
        schoolstreet: '',
        schoolemail: '',
        schoollga: '',
        schooltown: '',
        schooltelephone: '',
        schoolward: '',
        schoolstate: '',

        fetch_states: [],
        state_picked: '',
        state_index: [],
        get_state_code: '',

        results_lga: [],
        fetch_lga: [],
        lga_picked: '',
        lga_index: [],
        get_lga_code: '',

        counter_state: 0,
        counter_lga: 0,

        location_type_ref: [],
        location_type_val: [],
        metadata: [],

        //metadata arrays
        fetch_location: [],
        fetch_levels: [],
        fetch_category: [],
        fetch_authority: [],
        fetch_ownership: [],
        fetch_birth_certificate: [],
        fetch_b_c: [],
        fetch_b_c_no: [],
        fetch_age: [],
        fetch_age_primary: [],
        fetch_a: [],
        fetch_a_no: [],
        fetch_streams: [],
        fetch_year_by_age: [],
        fetch_year_by_age_no: [],
        fetch_y_b_a1: [], fetch_y_b_a2: [], fetch_y_b_a3: [],
        fetch_y_b_no1: [], fetch_y_b_no2: [], fetch_y_b_no3: [],
        fetch_all_ages: [],
        fetch_repeaters: [], fetch_r:[], 
        fetch_pupilflow: [],
        fetch_dropouts: [], fetch_drop:[],
        fetch_transfer_in: [], fetch_t_in:[],
        fetch_transfer_out: [], fetch_t_out:[],
        fetch_promoted: [], fetch_pro:[],
        fetch_examination:[],
        fetch_all_special_needs: [], fetch_special_needs: [], fetch_special_needs_item: [],
        fetch_s_n1: [], fetch_s_n2: [], fetch_s_n3: [],
        fetch_s_n_no1: [], fetch_s_n_no2: [], fetch_s_n_no3: [],
        fetch_a_s_n: [], fetch_a_s_n_no: [],
        fetch_orphans:[],

        inputParams: [],

        exam_type:'',
        registered_male:'',
        registered_female:'',
        registered_total:'', 
        took_part_male:'',
        took_part_female:'',
        took_part_total:'',
        passed_male:'',
        passed_female:'',
        passed_total:'',

        //enrolment by age
        kindergarten1_streams:'',
        kindergarten2_streams:'',
        nursery1_streams:'',
        nursery2_streams:'',
        nursery3_streams:'',
        below3_kindergarten1_male:'',
        below3_kindergarten1_female:'',
        below3_kindergarten2_male:'',
        below3_kindergarten2_female:'',
        age3_kindergarten1_male:'',
        age3_kindergarten1_female:'',
        age3_kindergarten2_male:'',
        age3_kindergarten2_female:'',
        age3_nursery1_male:'',
        age3_nursery1_female:'',
        age3_nursery2_male:'',
        age3_nursery2_female:'',
        age3_nursery3_male:'',
        age3_nursery3_female:'',
        age4_nursery1_male:'',
        age4_nursery1_female:'',
        age4_nursery2_male:'',
        age4_nursery2_female:'',
        age4_nursery3_male:'',
        age4_nursery3_female:'',
        age5_nursery1_male:'',
        age5_nursery1_female:'',
        age5_nursery2_male:'',
        age5_nursery2_female:'',
        age5_nursery3_male:'',
        age5_nursery3_female:'',
        above5_nursery1_male:'',
        above5_nursery1_female:'',
        above5_nursery2_male:'',
        above5_nursery2_female:'',
        above5_nursery3_male:'',
        above5_nursery3_female:'',

        //total 
        kindergarten1_male_total:'',
        kindergarten1_female_total:'',
        kindergarten2_male_total:'',
        kindergarten2_female_total:'',
        nursery1_male_total:'',
        nursery1_female_total:'',
        nursery2_male_total:'',
        nursery2_female_total:'',
        nursery3_male_total:'',
        nursery3_female_total:'',

        p1_entrant_male_total:'',
        p1_entrant_female_total:'',
        p1_eccd_entrant_male_total:'',
        p1_eccd_entrant_female_total:'',

        yearbyage_p1_male_total:'',
        yearbyage_p1_female_total:'',
        yearbyage_p2_male_total:'',
        yearbyage_p2_female_total:'',
        yearbyage_p3_male_total:'',
        yearbyage_p3_female_total:'',
        yearbyage_p4_male_total:'',
        yearbyage_p4_female_total:'',
        yearbyage_p5_male_total:'',
        yearbyage_p5_female_total:'',
        yearbyage_p6_male_total:'',
        yearbyage_p6_female_total:'',


        //school characteristics
        year: '',
        year_of_establishment: '',
        location_of_school: '',
        level_of_education: '',
        type_of_school: '',
        shifts_choice: '',
        shifts_choice_bi: '',
        facilities_choice: '',
        boarding_choice:'',
        facilities_shared: '',
        multigrade_choice: '',
        multigrade_choice_bi: '',
        average_distance: '',
        school_distance_lga:'',
        student_distance: '',
        students_boarding_female: '',
        students_boarding_male: '',
        sdp_choice: '',
        sdp_choice_bi: '',
        sbmc_choice: '',
        sbmc_choice_bi: '',
        pta_choice: '',
        pta_choice_bi: '',
        date_inspection: '',
        no_of_inspection: '',
        authority_choice: '',
        cash_transfer: '',
        grants_choice: '',
        grants_choice_bi: '',
        guard_choice: '',
        guard_choice_bi: '',
        ownership_choice: '',

        //C4
        p1_stream:'',
        p2_stream:'',
        p3_stream:'',
        p4_stream:'',
        p5_stream:'',
        p6_stream:'',
        
        p1_stream_with_multigrade:'',
        p2_stream_with_multigrade:'',
        p3_stream_with_multigrade:'',
        p4_stream_with_multigrade:'',
        p5_stream_with_multigrade:'',
        p6_stream_with_multigrade:'',

        p1_repeaters_male: '',
        p1_repeaters_female:'',
        p2_repeaters_male:'',
        p2_repeaters_female:'',
        p3_repeaters_male:'',
        p3_repeaters_female:'',
        p4_repeaters_male: '',
        p4_repeaters_female:'',
        p5_repeaters_male:'',
        p5_repeaters_female:'',
        p6_repeaters_male:'',
        p6_repeaters_female:'',

        prevyear_primary6_male:'',
        prevyear_primary6_female:'',

        //breadcrumbs href
        schoolstate_href:'',
        lga_href:'',
        level_of_education_href:'',
        school_type_href:'',

        //staff
        non_teaching_staff_male:'',
        non_teaching_staff_female:'',
        non_teaching_staff_total:'',
        teaching_staff_male:'',
        teaching_staff_female:'',
        teaching_staff_total:'',
        staff_id:'',
        staff_file_no:'',
        staff_name:'',
        staff_gender:'',
        staff_type:'',
        salary_source:'',
        staff_yob:'',
        staff_yfa:'',
        staff_ypa:'',
        staff_yps:'',
        staff_level:'',
        present:'',
        academic_qualification:'',
        teaching_qualification:'',
        area_specialisation:'',
        subject_taught:'',
        teaching_type:'',
        is_teaching_ss:'',
        attended_training:'',

        //fetch array for staff features
        fetch_teachingtype:[],
        fetch_subjecttaught:[],
        fetch_specialisation:[],
        fetch_academicqualification:[],
        fetch_teachingqualification:[],
        fetch_present:[],
        fetch_salarysource:[],
        fetch_stafftype:[],
        //fetch array for the staffs
        fetch_staffs:[],

        //Classrooms
        no_of_classrooms:'',
        classes_held_outside:'',
        fetch_otherRooms:[],
        fetch_drinkingwater_source:[],
        fetch_power_source:[],
        fetch_toilet_type:[],
        fetch_facilities:[],
        shared_facilities:[],
        
        //classrooms
        blackboard:'',
        seating:'',
        wall_material:'',
        floor_material:'',
        
        fetch_wallmaterial:[],
        fetch_classrooms:[],
        fetch_floormaterial:[],
        fetch_roofmaterial:[],
        roof_material:'',
        classroom_length:'',
        classroom_width:'',
        present_condition:'',
        fetch_presentcondition:[],
        year_of_construction:'',

        //facilities
        sources_of_drinking_water:[],
        sources_of_power:[],
        fetch_healthfacility:[],
        health_facility:'',
        fence_facility:'',
        fetch_fence:[],
        fetch_mainsubjects:[],
        fetch_subjects:[],
        fetch_seaters:[],
        fetch_learningmaterials:[],
        learning_materials:[],
        fetch_playfacility:[],
        play_facilities:[],
        fetch_playroom:[],
        facility_playroom:'',
        eccd_playrooms:'',

        caregivers_male:'',
        caregivers_female:'',
        caregivers_total:'',
        fetch_caregiversmanual:[],

         //removal
         fetch_removalcategory:[],
         fetch_stateschools:[],
         censusyear:year,
 
         removalcat:'',
         stateschool_picked:'',
         removedstaff_index:'',

         //teachers qualification
         tq_preprimary_male_total:'',
         tq_preprimary_female_total:'',
         tq_primary_male_total:'',
         tq_primary_female_total:'',
         tq_male_total:'',
         tq_female_total:'',

         //undertaking
        attestation_headteacher_name:'',
        attestation_headteacher_telephone:'',
        attestation_headteacher_signdate:'',
        attestation_enumerator_name:'',
        attestation_enumerator_position:'',
        attestation_enumerator_telephone:'',
        attestation_supervisor_name:'',
        attestation_supervisor_position:'',
        attestation_supervisor_telephone:'',

        auth_user:'',
        config:'',
        fetch_yearslist:[],
        registered_censusyear:'',
        ispreprimary:false,
        isprimary:false,
        
        datapreviewurl:''
    },

    watch:{
        facilities_choice: function() {        
            var facilitiesShared = document.getElementById("no_shared-facilities");
            if (this.facilities_choice == 0) {
              if (facilitiesShared) {
                facilitiesShared.required = false;
                facilitiesShared.disabled = true;
              }
            } else {
              facilitiesShared.required = true;
              facilitiesShared.disabled = false;
            }
          },
          boarding_choice: function() {        
            var boardingmale = document.getElementById("boarding_male");
            var boardingfemale = document.getElementById("boarding_female");
            if (this.boarding_choice == 0) {
                
                boardingmale.required = false;
                boardingmale.disabled = true;
                boardingfemale.required = false;
                boardingfemale.disabled = true;
              
            } else {
              boardingmale.required = true;
              boardingmale.disabled = false;
              boardingfemale.required = true;
              boardingfemale.disabled = false;
            }
          }
    },

    created() {                                                       // added
        // change 'en' to your locale                                 // added
        //this.$validator.localize('en', customMessages);               // added
    },

    mounted: function(){
        if(this.$cookies.isKey("user")){
            //initialized the years array
            var dt=new Date();
            for(i=1860;i<dt.getFullYear();i++){
                this.fetch_yearslist.push(i);
            }
            //setting the theme color
            var leftTab = document.querySelectorAll('.theme');
            for (let i = 0; i < leftTab.length; i++) {
                leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
            } 
            //get the access token
            this.user=this.$cookies.get("user");
            this.config = {
                headers: {
                  Accept: "application/json",
                  Authorization:"Bearer "+this.user.session,
                }
              }

            axios.post("/api/Details",
            {
                email:this.user.email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.auth_user=response.data;
            }).catch(error=>{
                this.$cookies.remove("user");
                window.location.assign("/login");
            })

            
            this.schoolcode=schoolcode;
            this.datapreviewurl="/school/" + schoolcode.toLowerCase() + "/year/" + year+"/preview";
        axios.get("/api/school/" + schoolcode.toLowerCase() + "/year/" + year,this.config).then(response => {
            this.results = response.data;//returns the json data

            
            //this.results =  JSON.parse(response.data);
            //console.log(this.results);
            if (this.results[0].type == "SchoolForm"){
                console.log('r: ', JSON.stringify(response.data, null, 2));
            
                //bind the data to the inputs
                //School Identification
                this.schoolname=this.results[0].data.school_identification["school_name"];
                this.registered_censusyear=year;
                this.xcoordinate=this.results[0].data.school_identification["xcoordinate"];
                this.ycoordinate=this.results[0].data.school_identification["ycoordinate"];
                this.zcoordinate=this.results[0].data.school_identification["zcoordinate"];
                this.schoolstreet=this.results[0].data.school_identification["address"];
                this.schoolemail=this.results[0].data.school_identification["email_address"];
                this.schoollga=this.results[0].data.school_identification["lga"];
                this.schooltown=this.results[0].data.school_identification["town"];
                this.schooltelephone=this.results[0].data.school_identification["school_telephone"];
                this.schoolward=this.results[0].data.school_identification["ward"];
                this.schoolstate=this.results[0].data.school_identification["state"];
    
    
                
                //School Characteristics
                //bind the metadata
                this.fetch_location=this.results[1].data.data.filter(function(reference){
                    return reference.reference=="Location Type";
                });
                //this.fetch_location=this.results[1].data.data[3].data;
                
                this.fetch_levels=this.results[1].data.data.filter(function(reference){
                    return reference.reference=="School Level";
                });
                this.fetch_category=this.results[1].data.data.filter(function(reference){
                    return reference.reference=="School Category";
                });
                this.fetch_authority=this.results[1].data.data.filter(function(reference){
                    return reference.reference=="Ownership - 2013";
                });
                this.fetch_ownership=this.results[1].data.data.filter(function(reference){
                    return reference.reference=="Ownership - 2013";
                });
    
                //bind the data
                this.year_of_establishment=this.results[0].data.school_characteristics["year_of_establishment"];
                this.location_of_school=this.results[0].data.school_characteristics["location"];
                this.level_of_education=this.results[0].data.school_characteristics["levels_of_education_offered"];
                this.type_of_school=this.results[0].data.school_characteristics["type_of_school"];
                this.shifts_choice=this.results[0].data.school_characteristics["shifts"];
                this.facilities_choice=this.results[0].data.school_characteristics["shared_facilities"];
                this.facilities_shared=this.results[0].data.school_characteristics["sharing_with"];
                this.multigrade_choice=this.results[0].data.school_characteristics["multi_grade_teaching"];    
                this.average_distance=this.results[0].data.school_characteristics["school_average_distance_from_catchment_communities"];
                this.school_distance_lga=this.results[0].data.school_characteristics["distance_from_lga"];
                this.student_distance=this.results[0].data.school_characteristics["students_distance_from_school"];
                this.students_boarding_female=this.results[0].data.school_characteristics["students_boarding"].female;
                this.students_boarding_male=this.results[0].data.school_characteristics["students_boarding"].male;
                this.sdp_choice=this.results[0].data.school_characteristics["school_development_plan_sdp"];
                this.sbmc_choice=this.results[0].data.school_characteristics["school_based_management_committee_sbmc"];    
                this.pta_choice=this.results[0].data.school_characteristics["parents_teachers_association_pta"];  
                this.date_inspection=this.results[0].data.school_characteristics["date_of_last_inspection_visit"];
                this.no_of_inspection=this.results[0].data.school_characteristics["no_of_inspection"];//no of last inspection
                this.authority_choice=this.results[0].data.school_characteristics["authority_of_last_inspection"];
                this.cash_transfer=this.results[0].data.school_characteristics["conditional_cash_transfer"];
                this.grants_choice=this.results[0].data.school_characteristics["school_grants"];            
                this.guard_choice=this.results[0].data.school_characteristics["security_guard"];            
                this.ownership_choice=this.results[0].data.school_characteristics["ownership"];
                
    
                //bind the breadcrumbs href
                this.schoolstate_href='/generic/search?state='+encodeURI(this.schoolstate);
                this.lga_href='/generic/search?lga='+encodeURI(this.schoollga);
                this.school_type_href='/generic/search?state='+encodeURIComponent(this.schoolstate)+'&'+'lga='+encodeURI(this.schoollga)+'&'+'schooltype='+encodeURI('public');
                this.level_of_education_href='/generic/search?state='+encodeURI(this.schoolstate)+'&'+'lga='+encodeURI(this.schoollga)+'&'+'schooltype='+encodeURI('public')+'&'+'schoollevel='+encodeURI(this.level_of_education);
    
                if(this.level_of_education=="Pre-primary"){
                    this.ispreprimary=true;
                    this.isprimary=false;
                }else if(this.level_of_education=="Primary"){
                    this.isprimary=true;
                    this.ispreprimary=false;
                }else if(this.level_of_education=="Pre-primary and primary"){
                    this.isprimary=true;
                    this.ispreprimary=true;
                }
    
                //Apply the metadata to the form sections
                //bind the states list
                var arrayOfObjects = []
    
                for (var i = 0; i < this.results[1].attributes.states.length; i++) {//loop the states
                    var obj = {};
                    obj["name"] = this.results[1].attributes.states[i].data.name;
                    obj["code"] = this.results[1].attributes.states[i].data.statecode;
                   
                    //save the state name as a key and the value is the index
                    if(obj["name"]==this.schoolstate){
                        this.state_picked=i;
                    }
                    arrayOfObjects.push(obj);
                }
                //this.state_picked=this.results[1].attributes.states[0].data.name;
    
                this.fetch_states = arrayOfObjects;
               // console.log('r: ', JSON.stringify(this.fetch_states, null, 2));
                var arrayOfObjects2 = []
                var tempIndex=0;
    
                ////LGA
                for (var i = 0; i < this.results[1].attributes.states.length; i++) {
                    
                    for (var j = 0; j < this.results[1].attributes.states[i].data.lgas.length; j++) {
                        var obj2 = {};
                        obj2["name"] = this.results[1].attributes.states[i].data.lgas[j].data.name;
                        obj2["code"] = this.results[1].attributes.states[i].data.lgas[j].data.lgacode;
    
                        arrayOfObjects2.push(obj2);
                        if(obj2["name"]==this.schoollga){
                            this.lga_picked=tempIndex;
                            
                        }
                        tempIndex++;
                    }
                }
                // this.lga_picked = this.results[1].attributes.states[0].data.lgas[0].data.name;
                //alert(this.lga_picked);   
                this.fetch_lga = arrayOfObjects2;
    
                
                //SECTION C ENROLLMENT
                //C1
                //Bind metadata
                this.fetch_birth_certificate=this.results[1].data.data.filter(function(reference){
                    return reference.reference=="Birth Certificate Type";
                });
                this.fetch_birth_certificate.sort(function(a,b){return a.order-b.order});
    
                
                
                //find functions
                function checkNpc(bc) {
                    return bc.value == "National Population Commission";
                }
                function checkHospital(bc) {
                    return bc.value == "Hospital";
                }
                function checkLga(bc) {
                    return bc.value == "LGA";
                }
                function checkCourt(bc) {
                    return bc.value == "Court";
                }
                function checkUn(bc) {
                    return bc.value == "UN";
                }
                function checkOthers(bc) {
                    return bc.value == "Others";
                }
    
                //Bind data 
                //loop this rather that set it all manually like this; check out public js file for tips
                this.fetch_birth_certificate.find(checkNpc).kindergarten1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten1"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).kindergarten1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten1"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).kindergarten1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten1"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).kindergarten1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten1"}).female:"";
    
                this.fetch_birth_certificate.find(checkNpc).kindergarten2_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten2"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).kindergarten2_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten2"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).kindergarten2_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten2"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).kindergarten2_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten2"}).female:"";
    
                this.fetch_birth_certificate.find(checkNpc).nursery1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery1"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).nursery1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery1"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).nursery1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery1"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).nursery1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery1"}).female:"";
    
                this.fetch_birth_certificate.find(checkNpc).nursery2_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery2"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).nursery2_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery2"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).nursery2_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery2"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).nursery2_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery2"}).female:"";
    
                this.fetch_birth_certificate.find(checkNpc).nursery3_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery3"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery3"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).nursery3_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery3"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery3"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).nursery3_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery3"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery3"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).nursery3_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery3"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery3"}).female:"";
    
                this.fetch_birth_certificate.find(checkNpc).primary1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Primary1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Primary1"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).primary1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Primary1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Primary1"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).primary1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Primary1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Primary1"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).primary1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Primary1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Primary1"}).female:"";
                
               
                //C2
                //Bind metadata
                
                this.fetch_age=this.results[1].data.data.filter(function(reference){
                    return reference.reference=="Age Category" && reference.level=="2";
                });
                this.fetch_age.sort(function(a,b){return a.order-b.order});
    
                //primary
                this.fetch_age_primary=this.results[1].data.data.filter(function(reference){
                    return reference.reference=="Age Category" && reference.level=="2";
                });
                this.fetch_age_primary.sort(function(a,b){return a.order-b.order});
    
                
                //Bind data
                this.fetch_age.find(function(age){return age.value=="below 6"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"}).male:"";
                this.fetch_age.find(function(age){return age.value=="below 6"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"}).female:"";
                this.fetch_age.find(function(age){return age.value=="6"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"}).male:"";
                this.fetch_age.find(function(age){return age.value=="6"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"}).female:"";
                this.fetch_age.find(function(age){return age.value=="7"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"}).male:"";
                this.fetch_age.find(function(age){return age.value=="7"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"}).female:"";
                this.fetch_age.find(function(age){return age.value=="Above 11 Years"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"}).male:"";
                this.fetch_age.find(function(age){return age.value=="Above 11 Years"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"}).female:"";
                this.fetch_age.find(function(age){return age.value=="8"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"}).male:"";
                this.fetch_age.find(function(age){return age.value=="8"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"}).female:"";
                this.fetch_age.find(function(age){return age.value=="9"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"}).male:"";
                this.fetch_age.find(function(age){return age.value=="9"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"}).female:"";
                this.fetch_age.find(function(age){return age.value=="10"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"}).male:"";
                this.fetch_age.find(function(age){return age.value=="10"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"}).female:"";
                this.fetch_age.find(function(age){return age.value=="11"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"}).male:"";
                this.fetch_age.find(function(age){return age.value=="11"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"}).female:"";
                //this.fetch_age.find(function(age){return age.value=="5"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="5"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="5"}).male:"";
                //this.fetch_age.find(function(age){return age.value=="5"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="5"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="5"}).female:"";
    
                //Bind data
                this.fetch_age.find(function(age){return age.value=="below 6"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"}).eccd_male:"";
                this.fetch_age.find(function(age){return age.value=="below 6"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"}).eccd_female:"";
                this.fetch_age.find(function(age){return age.value=="6"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"}).eccd_male:"";
                this.fetch_age.find(function(age){return age.value=="6"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"}).eccd_female:"";
                this.fetch_age.find(function(age){return age.value=="7"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"}).eccd_male:"";
                this.fetch_age.find(function(age){return age.value=="7"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"}).eccd_female:"";
                this.fetch_age.find(function(age){return age.value=="Above 11 Years"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"}).eccd_male:"";
                this.fetch_age.find(function(age){return age.value=="Above 11 Years"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"}).eccd_female:"";
                this.fetch_age.find(function(age){return age.value=="8"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"}).eccd_male:"";
                this.fetch_age.find(function(age){return age.value=="8"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"}).eccd_female:"";
                this.fetch_age.find(function(age){return age.value=="9"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"}).eccd_male:"";
                this.fetch_age.find(function(age){return age.value=="9"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"}).eccd_female:"";
                this.fetch_age.find(function(age){return age.value=="10"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"}).eccd_male:"";
                this.fetch_age.find(function(age){return age.value=="10"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"}).eccd_female:"";
                this.fetch_age.find(function(age){return age.value=="11"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"}).eccd_male:"";
                this.fetch_age.find(function(age){return age.value=="11"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"}).eccd_female:"";
    
                //Primary Enrollment by age
                //Bind primary data
                this.p1_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary1"}))?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary1"}).stream:"";
                this.p2_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary2"}).stream:"";
                this.p3_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary3"}).stream:"";
                this.p4_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary4"}).stream:"";
                this.p5_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary5"}).stream:"";
                this.p6_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary6"}).stream:"";
    
                this.p1_stream_with_multigrade=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary1"}))?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary1"}).stream_with_multigrade:"";
                this.p2_stream_with_multigrade=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary2"}).stream_with_multigrade:"";
                this.p3_stream_with_multigrade=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary3"}).stream_with_multigrade:"";
                this.p4_stream_with_multigrade=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary4"}).stream_with_multigrade:"";
                this.p5_stream_with_multigrade=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary5"}).stream_with_multigrade:"";
                this.p6_stream_with_multigrade=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary6"}).stream_with_multigrade:"";
    
                this.p1_repeaters_male=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary1"}).male:"";
                this.p1_repeaters_female=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary1"}).female:"";
                this.p2_repeaters_male=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary2"}).male:"";
                this.p2_repeaters_female=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary2"}).female:"";
                this.p3_repeaters_male=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary3"}).male:"";
                this.p3_repeaters_female=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary3"}).female:"";
                this.p4_repeaters_male=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary4"}).male:"";
                this.p4_repeaters_female=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary4"}).female:"";
                this.p5_repeaters_male=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary5"}).male:"";
                this.p5_repeaters_female=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary5"}).female:"";
                this.p6_repeaters_male=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary6"}).male:"";
                this.p6_repeaters_female=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary6"}).female:"";
    
    
                this.prevyear_primary6_male=(this.results[0].data.enrollment.year_by_age.data.prev_year[0])?this.results[0].data.enrollment.year_by_age.data.prev_year[0].male:"";
                this.prevyear_primary6_female=(this.results[0].data.enrollment.year_by_age.data.prev_year[0])?this.results[0].data.enrollment.year_by_age.data.prev_year[0].female:"";
                
    
                //age below 6
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary1"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary1"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary2"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary2"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary3"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary3"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary4"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary4"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary5"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary5"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary6"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary6"}).female:"";
    
                //age 6
                this.fetch_age_primary.find(function(age){return age.value=="6"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary1"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="6"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary1"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="6"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary2"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="6"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary2"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="6"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary3"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="6"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary3"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="6"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary4"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="6"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary4"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="6"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary5"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="6"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary5"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="6"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary6"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="6"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary6"}).female:"";
    
                //age 7
                this.fetch_age_primary.find(function(age){return age.value=="7"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary1"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="7"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary1"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="7"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary2"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="7"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary2"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="7"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary3"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="7"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary3"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="7"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary4"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="7"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary4"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="7"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary5"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="7"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary5"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="7"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary6"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="7"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary6"}).female:"";
    
                //age 8
                this.fetch_age_primary.find(function(age){return age.value=="8"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary1"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="8"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary1"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="8"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary2"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="8"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary2"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="8"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary3"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="8"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary3"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="8"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary4"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="8"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary4"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="8"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary5"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="8"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary5"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="8"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary6"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="8"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary6"}).female:"";
    
                //age 9
                this.fetch_age_primary.find(function(age){return age.value=="9"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary1"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="9"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary1"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="9"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary2"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="9"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary2"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="9"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary3"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="9"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary3"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="9"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary4"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="9"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary4"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="9"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary5"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="9"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary5"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="9"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary6"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="9"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary6"}).female:"";
    
                //age 10
                this.fetch_age_primary.find(function(age){return age.value=="10"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary1"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="10"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary1"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="10"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary2"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="10"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary2"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="10"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary3"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="10"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary3"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="10"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary4"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="10"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary4"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="10"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary5"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="10"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary5"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="10"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary6"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="10"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary6"}).female:"";
    
                //age 11
                this.fetch_age_primary.find(function(age){return age.value=="11"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary1"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="11"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary1"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="11"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary2"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="11"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary2"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="11"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary3"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="11"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary3"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="11"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary4"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="11"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary4"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="11"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary5"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="11"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary5"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="11"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary6"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="11"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary6"}).female:"";
    
    
                 //age above 11
                 this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary1"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary1"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary2"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary2"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary3"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary3"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary4"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary4"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary5"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary5"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary6"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary6"}).female:"";
     
    
    
                //C3
                //Bind metadata
                //not binding anything
    
                //Bind data
                this.kindergarten1_streams=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Kindergarten1"}).stream:"";
                this.kindergarten2_streams=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Kindergarten2"}).stream:"";
                this.nursery1_streams=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Nursery1"}).stream:"";
                this.nursery2_streams=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Nursery2"}).stream:"";
                this.nursery3_streams=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Nursery3"}).stream:"";
    
                //below3
                this.below3_kindergarten1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten1"}).male:"";
                this.below3_kindergarten1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten1"}).female:"";
                this.below3_kindergarten2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten2"}).male:"";
                this.below3_kindergarten2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten2"}).female:"";
                
                //3
                this.age3_kindergarten1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten1"}).male:"";
                this.age3_kindergarten1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten1"}).female:"";
                this.age3_kindergarten2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten2"}).male:"";
                this.age3_kindergarten2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten2"}).female:"";
                this.age3_nursery1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery1"}).male:"";
                this.age3_nursery1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery1"}).female:"";
                this.age3_nursery2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery2"}).male:"";
                this.age3_nursery2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery2"}).female:"";
                this.age3_nursery3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery3"}).male:"";
                this.age3_nursery3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery3"}).female:"";
    
                //4
                this.age4_nursery1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery1"}).male:"";
                this.age4_nursery1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery1"}).female:"";
                this.age4_nursery2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery2"}).male:"";
                this.age4_nursery2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery2"}).female:"";
                this.age4_nursery3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery3"}).male:"";
                this.age4_nursery3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery3"}).female:"";
    
    
                //5
                this.age5_nursery1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery1"}).male:"";
                this.age5_nursery1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery1"}).female:"";
                this.age5_nursery2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery2"}).male:"";
                this.age5_nursery2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery2"}).female:"";
                this.age5_nursery3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery3"}).male:"";
                this.age5_nursery3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery3"}).female:"";
    
    
                //above 5
                this.above5_nursery1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery1"}).male:"";
                this.above5_nursery1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery1"}).female:"";
                this.above5_nursery2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery2"}).male:"";
                this.above5_nursery2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery2"}).female:"";
                this.above5_nursery3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery3"}).male:"";
                this.above5_nursery3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery3"}).female:"";
    
    
                
                //C4
                //Bind metadata
                this.fetch_pupilflow=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Enrolment Items 2016";
                });
                this.fetch_pupilflow.sort(function(a,b){return a.value<b.value});
    
                
                //Bind Data
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary3"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary4_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary4"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary4_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary4"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary5_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary5"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary5_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary5"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary6_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary6"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary6_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary6"}).female:"";
    
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary3"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary4_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary4"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary4_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary4"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary5_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary5"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary5_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary5"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary6_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary6"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary6_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary6"}).female:"";
    
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary3"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary4_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary4"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary4_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary4"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary5_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary5"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary5_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary5"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary6_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary6"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary6_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary6"}).female:"";
    
    
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary3"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary4_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary4"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary4_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary4"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary5_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary5"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary5_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary5"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary6_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary6"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary6_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary6"}).female:"";
    
    
    
                //C5
                //Bind metadata
                this.fetch_special_needs=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Pupil Challenges 2009";
                });
                this.fetch_special_needs.sort(function(a,b){return a.value>b.value});
    
                //Bind data
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).eccd_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Eccd"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).eccd_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Eccd"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).nursery_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nurs"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).nursery_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nurs"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).nursery3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nursery3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).nursery3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nursery3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary4_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary4"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary4_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary4"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary5_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary5"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary5_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary5"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary6_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary6"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary6_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary6"}).female:"";
    
    
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).eccd_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Eccd"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).eccd_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Eccd"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).nursery_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nurs"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).nursery_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nurs"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).nursery3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nursery3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).nursery3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nursery3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary4_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary4"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary4_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary4"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary5_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary5"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary5_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary5"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary6_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary6"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary6_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary6"}).female:"";
    
    
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).eccd_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Eccd"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).eccd_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Eccd"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).nursery_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nurs"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).nursery_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nurs"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).nursery3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nursery3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).nursery3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nursery3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary4_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary4"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary4_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary4"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary5_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary5"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary5_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary5"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary6_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary6"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary6_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary6"}).female:"";
    
    
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).eccd_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Eccd"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).eccd_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Eccd"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).nursery_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nurs"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).nursery_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nurs"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).nursery3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nursery3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).nursery3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nursery3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary4_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary4"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary4_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary4"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary5_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary5"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary5_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary5"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary6_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary6"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary6_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary6"}).female:"";
    
    
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).eccd_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Eccd"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).eccd_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Eccd"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).nursery_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nurs"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).nursery_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nurs"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).nursery3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nursery3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).nursery3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nursery3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary4_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary4"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary4_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary4"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary5_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary5"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary5_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary5"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary6_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary6"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary6_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary6"}).female:"";
    
    
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).eccd_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Eccd"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).eccd_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Eccd"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).nursery_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nurs"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).nursery_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nurs"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).nursery3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nursery3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).nursery3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nursery3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary4_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary4"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary4_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary4"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary5_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary5"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary5_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary5"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary6_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary6"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary6_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary6"}).female:"";
    
    
    /** 
                //C6
                //Bind data
                this.registered_male=this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).registered_male;
                this.registered_female=this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).registered_female;
                this.registered_total=this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).registered_total;
                this.took_part_male=this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).took_part_male;
                this.took_part_female=this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).took_part_female;
                this.took_part_total=this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).took_part_total;
                this.passed_male=this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).passed_male;
                this.passed_female=this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).passed_female;
                this.passed_total=this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).passed_total;
    */
    
                //C
                //Fetch Orphans
                //Bind metadata
                this.fetch_orphans=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Orphan Type";
                });
                this.fetch_orphans.sort(function(a,b){return a.order>b.order});
    
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).eccd_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Eccd"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).eccd_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Eccd"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).nursery_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nurs"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).nursery_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nurs"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).nursery3_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nursery3"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).nursery3_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nursery3"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary1_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary1"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary1_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary1"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary2_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary2"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary2_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary2"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary3_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary3"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary3_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary3"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary4_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary4"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary4_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary4"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary5_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary5"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary5_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary5"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary6_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary6"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary6_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary6"}).female:"";
    
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).eccd_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Eccd"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).eccd_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Eccd"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).nursery_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nurs"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).nursery_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nurs"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).nursery3_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nursery3"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).nursery3_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nursery3"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary1_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary1"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary1_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary1"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary2_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary2"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary2_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary2"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary3_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary3"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary3_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary3"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary4_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary4"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary4_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary4"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary5_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary5"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary5_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary5"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary6_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary6"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary6_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary6"}).female:"";
    
    
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).eccd_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Eccd"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).eccd_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Eccd"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).nursery_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nurs"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).nursery_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nurs"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).nursery3_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nursery3"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).nursery3_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nursery3"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary1_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary1"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary1_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary1"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary2_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary2"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary2_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary2"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary3_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary3"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary3_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary3"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary4_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary4"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary4_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary4"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary5_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary5"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary5_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary5"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary6_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary6"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary6_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary6"}).female:"";
    
    
                //sum total
                this.kindergarten1_male_total=parseInt(this.below3_kindergarten1_male)+parseInt(this.age3_kindergarten1_male);
                this.kindergarten1_female_total=parseInt(this.below3_kindergarten1_female)+parseInt(this.age3_kindergarten1_female);
                this.kindergarten2_male_total=parseInt(this.below3_kindergarten2_male)+parseInt(this.age3_kindergarten2_male);
                this.kindergarten2_female_total=parseInt(this.below3_kindergarten2_female)+parseInt(this.age3_kindergarten2_female);
                this.nursery1_male_total=parseInt(this.age3_nursery1_male)+parseInt(this.age4_nursery1_male)+parseInt(this.age5_nursery1_male)+parseInt(this.above5_nursery1_male);
                this.nursery1_female_total=parseInt(this.age3_nursery1_female)+parseInt(this.age4_nursery1_female)+parseInt(this.age5_nursery1_female)+parseInt(this.above5_nursery1_female);
                this.nursery2_male_total=parseInt(this.age3_nursery2_male)+parseInt(this.age4_nursery2_male)+parseInt(this.age5_nursery2_male)+parseInt(this.above5_nursery2_male);
                this.nursery2_female_total=parseInt(this.age3_nursery2_female)+parseInt(this.age4_nursery2_female)+parseInt(this.age5_nursery2_female)+parseInt(this.above5_nursery2_female);
                this.nursery3_male_total=parseInt(this.age3_nursery3_male)+parseInt(this.age4_nursery3_male)+parseInt(this.age5_nursery3_male)+parseInt(this.above5_nursery3_male);
                this.nursery3_female_total=parseInt(this.age3_nursery3_female)+parseInt(this.age4_nursery3_female)+parseInt(this.age5_nursery3_female)+parseInt(this.above5_nursery3_female);
    
    
    
                //Staff
                this.non_teaching_staff_male=this.results[0].data.staff.number_of_non_teaching_staffs["male"];
                this.non_teaching_staff_female=this.results[0].data.staff.number_of_non_teaching_staffs["female"];
                this.non_teaching_staff_total=this.results[0].data.staff.number_of_non_teaching_staffs["total"];
    
                this.teaching_staff_male=this.results[0].data.staff.number_of_teaching_staffs["male"];
                this.teaching_staff_female=this.results[0].data.staff.number_of_teaching_staffs["female"];
                this.teaching_staff_total=this.results[0].data.staff.number_of_teaching_staffs["total"];
    
                this.caregivers_male=this.results[0].data.staff.number_of_caregivers["male"];
                this.caregivers_female=this.results[0].data.staff.number_of_caregivers["female"];
    
                //Staffs
                this.fetch_staffs=this.results[0].data.staff.information_on_all_staff.data;
    
                //still under the staff section
                //metadata
                this.fetch_teachingtype=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Teacher Appointment Type";
                });
                this.fetch_teachingtype.sort(function(a,b){return a.order>b.order});
    
                 //Classrooms
                 this.fetch_classrooms=this.results[0].data.classrooms.information_on_all_classrooms.data;
    
                 //present condition
                 this.fetch_presentcondition=this.results[1].data.data.filter(function(metadata){
                     return metadata.reference=="Classroom Present Condition";
                 });
                 this.fetch_presentcondition.sort(function(a,b){return a.order>b.order});
                 
                 //floor
                 this.fetch_floormaterial=this.results[1].data.data.filter(function(metadata){
                     return metadata.reference=="Floor Material";
                 });
                 this.fetch_floormaterial.sort(function(a,b){return a.order>b.order});
                 
                 //wall
                 this.fetch_wallmaterial=this.results[1].data.data.filter(function(metadata){
                     return metadata.reference=="Wall Material";
                 });
                 this.fetch_wallmaterial.sort(function(a,b){return a.order>b.order});
    
                 //roof
                 this.fetch_roofmaterial=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Roof Material";
                });
                this.fetch_roofmaterial.sort(function(a,b){return a.order>b.order});
                 
                //Salary Source
                this.fetch_salarysource=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Salary Source";
                });
                this.fetch_salarysource.sort(function(a,b){return a.order>b.order});
    
                //Main Secondary Subject Taught
                this.fetch_specialisation=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Main Secondary Subject Taught";
                });
                this.fetch_specialisation.sort(function(a,b){return a.order>b.order});
    
                this.fetch_subjecttaught=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Main Secondary Subject Taught";
                });
                this.fetch_subjecttaught.sort(function(a,b){return a.order>b.order});
    
                //Present 2016
                this.fetch_present=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Present 2016";
                });
                this.fetch_present.sort(function(a,b){return a.order>b.order});
    
                //Staff Type
                this.fetch_stafftype=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Primary Staff Type";
                });
                this.fetch_stafftype.sort(function(a,b){return a.order>b.order});
    
                //Academic Qualification 2009
                this.fetch_academicqualification=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Academic Qualification 2009";
                });
                this.fetch_academicqualification.sort(function(a,b){return a.order>b.order});
    
                //Teaching Qualifications
                this.fetch_teachingqualification=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Teaching Qualifications";
                });
                this.fetch_teachingqualification.sort(function(a,b){return a.order>b.order});
    
                //Staff Removal Category
                
                this.fetch_removalcategory=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Staff Removal Category";
                });
                this.fetch_removalcategory.sort(function(a,b){return a.order>b.order});
    
                //Classroom
                this.no_of_classrooms=this.results[0].data.classrooms.number_of_classrooms;
                this.classes_held_outside=this.results[0].data.classrooms.are_classes_held_outside;
                this.eccd_playrooms=this.results[0].data.classrooms.eccd_playrooms;
                //Bind metadata
                this.fetch_otherRooms=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Room Type 2016";
                });
                this.fetch_otherRooms.sort(function(a,b){return a.order>b.order});
    
                this.fetch_otherRooms.find(function(room){return room.value=="Staff room"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Staff room"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Staff room"}).number:"";
                this.fetch_otherRooms.find(function(room){return room.value=="Office"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Office"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Office"}).number:"";
                this.fetch_otherRooms.find(function(room){return room.value=="Laboratories"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Laboratories"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Laboratories"}).number:"";
                this.fetch_otherRooms.find(function(room){return room.value=="Store room"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Store room"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Store room"}).number:"";
                this.fetch_otherRooms.find(function(room){return room.value=="Others"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Others"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Others"}).number:"";
                
                //Drinking water
                //Bind metadata
                this.fetch_drinkingwater_source=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Water Supply Type";
                });
                this.fetch_drinkingwater_source.sort(function(a,b){return a.order>b.order});
                this.sources_of_drinking_water=this.results[0].data.facilities.source_of_drinking_water.data;
    
                //facilities
                //Bind metadata
                this.fetch_facilities=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Useable Facility 2013";
                });
                this.fetch_facilities.sort(function(a,b){return a.order>b.order});
    
                this.fetch_facilities.find(function(facility){return facility.value=="Toilet"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Toilet"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Toilet"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Toilet"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Toilet"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Toilet"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Computer"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Computer"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Computer"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Computer"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Computer"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Computer"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Water Source"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Water Source"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Water Source"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Water Source"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Water Source"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Water Source"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Laboratory"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Laboratory"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Laboratory"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Laboratory"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Laboratory"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Laboratory"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Classroom"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Classroom"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Classroom"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Classroom"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Classroom"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Classroom"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Library"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Library"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Library"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Library"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Library"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Library"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Play Ground"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Play Ground"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Play Ground"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Play Ground"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Play Ground"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Play Ground"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Others"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Others"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Others"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Others"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Others"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Others"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Wash hand facility"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Wash hand facility"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Wash hand facility"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Wash hand facility"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Wash hand facility"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Wash hand facility"}).notuseable:"";
    
    
                //shared facilities
                this.shared_facilities=this.results[0].data.facilities.shared_facilities.data;
    
    
                //power source
                //Bind metadata
                this.fetch_power_source=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Power Facilities";
                });
                this.fetch_power_source.sort(function(a,b){return a.order>b.order});
                this.sources_of_power=this.results[0].data.facilities.sources_of_power.data;   
    
                //toilet type
                //Bind metadata
                this.fetch_toilet_type=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Toilet Type 2009";
                });
                this.fetch_toilet_type.sort(function(a,b){return a.order>b.order});
    
                //toilet Pit
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).students_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).students_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).students_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).teachers_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).teachers_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).teachers_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).both_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).both_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).both_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"}).mixed:"";
    
                //Bucket system
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).students_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).students_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).students_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).teachers_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).teachers_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).teachers_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).both_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).both_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).both_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"}).mixed:"";
    
                //toilet Water flush
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).students_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).students_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).students_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).teachers_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).teachers_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).teachers_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).both_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).both_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).both_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"}).mixed:"";
    
                //toilet Pit
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).students_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).students_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).students_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).teachers_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).teachers_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).teachers_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).both_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).both_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).both_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"}).mixed:"";
    
    
                //health facility
                this.fetch_healthfacility=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Available Health Facility 2009";
                });
                this.fetch_healthfacility.sort(function(a,b){return a.order>b.order});
                this.health_facility=this.results[0].data.facilities.health_facility.data;
                
                //fence
                this.fetch_fence=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Available Fence Wall 2016";
                });
                this.fetch_fence.sort(function(a,b){return a.order>b.order});
                this.fence_facility=this.results[0].data.facilities.fence.data;
    
                //undertaking
                this.attestation_headteacher_name=this.results[0].data.undertaking.attestation_headteacher_name;
                this.attestation_headteacher_telephone=this.results[0].data.undertaking.attestation_headteacher_telephone;
                this.attestation_headteacher_signdate=this.results[0].data.undertaking.attestation_headteacher_signdate;
                this.attestation_enumerator_name=this.results[0].data.undertaking.attestation_enumerator_name;
                this.attestation_enumerator_position=this.results[0].data.undertaking.attestation_enumerator_position;
                this.attestation_enumerator_telephone=this.results[0].data.undertaking.attestation_enumerator_telephone;
                this.attestation_supervisor_name=this.results[0].data.undertaking.attestation_supervisor_name;
                this.attestation_supervisor_position=this.results[0].data.undertaking.attestation_supervisor_position;
                this.attestation_supervisor_telephone=this.results[0].data.undertaking.attestation_supervisor_telephone;
    
    
                //seater
                this.fetch_seaters=[{"value":"ECCD","order":"1"},{"value":"Nurs","order":"2"},{"value":"Primary1","order":"3"},{"value":"Primary2","order":"4"},{"value":"Primary3","order":"5"},{"value":"Primary4","order":"6"},{"value":"Primary5","order":"7"},{"value":"Primary6","order":"8"}];
                //this.fetch_seaters.sort(function(a,b){return a.order>b.order});
                for (var i = 0; i < this.fetch_seaters.length; i++) {
                    var Tclass=this.fetch_seaters[i].value;
                    this.fetch_seaters[i].seater1=(this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="1 Seater"})!=null)?this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="1 Seater"}).capacity:"";
                    this.fetch_seaters[i].seater2=(this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="2 Seater"})!=null)?this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="2 Seater"}).capacity:"";
                    this.fetch_seaters[i].seater3=(this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="3 Seater"})!=null)?this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="3 Seater"}).capacity:"";
                    this.fetch_seaters[i].seater4=(this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="4 Seater"})!=null)?this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="4 Seater"}).capacity:"";
                    this.fetch_seaters[i].seater5=(this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="5 Seater"})!=null)?this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="5 Seater"}).capacity:"";
                    this.fetch_seaters[i].seater6=(this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="6 Seater"})!=null)?this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="6 Seater"}).capacity:"";
                    
                    }
    
                //teaching qualification
                for(var i=0; i< this.fetch_teachingqualification.length; i++){
                    var tq=this.fetch_teachingqualification[i].value;
                    this.fetch_teachingqualification[i].preprimary_male=(this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Pre-primary" && p.qualification==tq})!=null)?this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Pre-primary" && p.qualification==tq}).male:"";
                    this.fetch_teachingqualification[i].preprimary_female=(this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Pre-primary" && p.qualification==tq})!=null)?this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Pre-primary" && p.qualification==tq}).female:"";
                    this.fetch_teachingqualification[i].primary_male=(this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Primary" && p.qualification==tq})!=null)?this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Primary" && p.qualification==tq}).male:"";
                    this.fetch_teachingqualification[i].primary_female=(this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Primary" && p.qualification==tq})!=null)?this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Primary" && p.qualification==tq}).female:"";
                    }
                
                //student by subject
                this.fetch_subjects=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Students By Subject Area JSS";
                });
                this.fetch_subjects.sort(function(a,b){return a.order>b.order});
    
                for (var i = 0; i < this.fetch_subjects.length; i++) {
                var subject=this.fetch_subjects[i].value;
                this.fetch_subjects[i].jss1_pupils_male=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss1"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss1"}).male:"";
                this.fetch_subjects[i].jss1_pupils_female=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss1"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss1"}).female:"";
                this.fetch_subjects[i].jss2_pupils_male=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss2"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss2"}).male:"";
                this.fetch_subjects[i].jss2_pupils_female=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss2"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss2"}).female:"";
                this.fetch_subjects[i].jss3_pupils_male=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss3"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss3"}).male:"";
                this.fetch_subjects[i].jss3_pupils_female=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss3"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss3"}).female:"";
    
                }
    
                //playroom
                this.fetch_playroom=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Available Play Room";
                });
                this.fetch_playroom.sort(function(a,b){return a.order>b.order});
                this.facility_playroom=this.results[0].data.facilities.playroom.data[0];
    
                //playfacility
                this.fetch_playfacility=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Play Facility 2016";
                });
                this.fetch_playfacility.sort(function(a,b){return a.order>b.order});
                this.play_facilities=this.results[0].data.facilities.playfacilities.data;
    
                //materials
                this.fetch_learningmaterials=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Learning Materials";
                });
                this.fetch_learningmaterials.sort(function(a,b){return a.order>b.order});
                this.learning_materials=this.results[0].data.facilities.materials.data;
                
    
                //main subject
                this.fetch_mainsubjects=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Main Secondary Subject Taught";
                });
                this.fetch_mainsubjects.sort(function(a,b){return a.order>b.order});
    
                //h1
                for (var i = 0; i < this.fetch_mainsubjects.length; i++) {
                    var subject=this.fetch_mainsubjects[i].value;
                    this.fetch_mainsubjects[i].preprimary_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Nurs"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Nurs"}).value:"";
                    this.fetch_mainsubjects[i].prm1_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary1"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary1"}).value:"";
                    this.fetch_mainsubjects[i].prm2_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary2"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary2"}).value:"";
                    this.fetch_mainsubjects[i].prm3_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary3"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary3"}).value:"";
                    this.fetch_mainsubjects[i].prm4_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary4"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary4"}).value:"";
                    this.fetch_mainsubjects[i].prm5_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary5"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary5"}).value:"";
                    this.fetch_mainsubjects[i].prm6_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary6"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary6"}).value:"";
                    
                    }
        
                //h2
                for (var i = 0; i < this.fetch_mainsubjects.length; i++) {
                    var subject=this.fetch_mainsubjects[i].value;
                    this.fetch_mainsubjects[i].preprimary_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Nurs"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Nurs"}).value:"";
                    this.fetch_mainsubjects[i].prm1_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary1"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary1"}).value:"";
                    this.fetch_mainsubjects[i].prm2_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary2"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary2"}).value:"";
                    this.fetch_mainsubjects[i].prm3_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary3"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary3"}).value:"";
                    this.fetch_mainsubjects[i].prm4_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary4"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary4"}).value:"";
                    this.fetch_mainsubjects[i].prm5_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary5"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary5"}).value:"";
                    this.fetch_mainsubjects[i].prm6_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary6"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary6"}).value:"";
                    
                    }
    
                //caregiversmanual
                this.fetch_caregiversmanual=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Caregiver Manual Type";
                });
                this.fetch_caregiversmanual.sort(function(a,b){return a.order>b.order});
    
                for (var i = 0; i < this.fetch_caregiversmanual.length; i++) {
                var subject=this.fetch_caregiversmanual[i].value;
                this.fetch_caregiversmanual[i].available=(this.results[0].data.caregiver_manual.data.find(function(p){return p.category==subject })!=null)?this.results[0].data.caregiver_manual.data.find(function(p){return p.category==subject}).available:"";
                
                }
    
                //caregivers total
                this.caregivers_total=parseInt(this.caregivers_male)+parseInt(this.caregivers_female);
    
                //sum total for entrant
                this.p1_entrant_male_total=this.fetch_age.reduce(function(entrantmale_total, item){
                    
                    return entrantmale_total + parseInt((item.male==null)?0:item.male); 
                  },0);
                this.p1_entrant_female_total=this.fetch_age.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt((item.female==null)?0:item.female); 
                    },0);
                this.p1_eccd_entrant_male_total=this.fetch_age.reduce(function(entrantmale_total, item){
                    
                    return entrantmale_total + parseInt((item.eccd_male==null)?0:item.eccd_male); 
                  },0);
                this.p1_eccd_entrant_female_total=this.fetch_age.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt((item.eccd_female==null)?0:item.eccd_female); 
                    },0);
                
                
    
                //sum total for year by age
                this.yearbyage_p1_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt((item.primary1_male==null)?0:item.primary1_male); 
                    },0);
                this.yearbyage_p1_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt((item.primary1_female==null)?0:item.primary1_female); 
                    },0);
                this.yearbyage_p2_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt((item.primary2_male==null)?0:item.primary2_male); 
                    },0);
                this.yearbyage_p2_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt((item.primary2_female==null)?0:item.primary2_female); 
                    },0);
                this.yearbyage_p3_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt((item.primary3_male==null)?0:item.primary3_male); 
                    },0);
                this.yearbyage_p3_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt((item.primary3_female==null)?0:item.primary3_female); 
                    },0);
                this.yearbyage_p4_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt((item.primary4_male==null)?0:item.primary4_male); 
                    },0);
                this.yearbyage_p4_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt((item.primary4_female==null)?0:item.primary4_female); 
                    },0);
                this.yearbyage_p5_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt((item.primary5_male==null)?0:item.primary5_male); 
                    },0);
                this.yearbyage_p5_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt((item.primary5_female==null)?0:item.primary5_female); 
                    },0);
                this.yearbyage_p6_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt((item.primary6_male==null)?0:item.primary6_male); 
                    },0);
                this.yearbyage_p6_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt((item.primary6_female==null)?0:item.primary6_female); 
                    },0);
            
                    this.tq_preprimary_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                
                        return male_total + parseInt((item.preprimary_male==null)?0:item.preprimary_male); 
                        },0);
                        this.tq_preprimary_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                        
                            return female_total + parseInt((item.preprimary_female==null)?0:item.preprimary_female); 
                          },0);
                          this.tq_primary_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                        
                            return male_total + parseInt((item.primary_male==0)?0:item.primary_male); 
                          },0);
                          this.tq_primary_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                        
                            return female_total + parseInt((item.primary_female==null)?0:item.primary_female); 
                          },0);



                
                
                //set the spinner to stop loading             
                $("div#loader").addClass("hide");
            }else{
                //set the spinner to stop loading             
                $("div#loader").addClass("hide");
                alert(this.results[1].message);
            }
        });

        axios.get("/api/search/state/schools",this.config).then(response => {

            this.resultsSave = response.data;
            //console.log(JSON.stringify(response.data, null, 2));

            this.fetch_stateschools=response.data;
            
        });

        var previousButton = document.querySelectorAll('button#previous');
        previousButton.forEach(element => {
            element.addEventListener('click', function () {
            var tabP = document.querySelectorAll('div#content > div');
            for (let index = 0; index < tabP.length; index++) {
                if (tabP[index].className == 'show-current-tab'){
                tabP[index].classList.remove('show-current-tab');
                var newPrevI = index - 1;
                tabP[newPrevI].classList.add('show-current-tab');
                }             
            }
            })
        }); 

        function forLoader(params) {    
            var clickedButton = document.querySelectorAll(params);
            clickedButton.forEach(element => {                
                element.addEventListener('click', function () {
                    var loader = document.getElementById('loader');
                    loader.classList.replace('hide','show');
                    console.log(loader);
                });
            });
        }
        
        forLoader('button#next');
        forLoader('button#save');


        }else{
            window.location.assign("/login");
        }

        
    },

    methods:{
        print_form:function(){
            window.print();
        },
        checkgrade:function(message,event){
            var regex = /^((\d|1[0-7])\/(\d|1[0-5]))$/gm;
            if(!regex.test(message)){
                alert("Fix Grade level/step => ("+message+") to match [1-17]/[1-15]")
            }
            
        },
        popAlertMsg: function (message) {
            setTimeout(() => {
                if (loader.classList.contains('show')){
                    loader.classList.replace('show', 'hide');
                }
            }, 300);
            var alertBox = document.getElementById('alert');
            this.responseMsg = message;
            if (message.indexOf("Error") >= 0 ){
                alertBox.style.backgroundColor = '#ff0000';
            }
            alertBox.classList.add('active');
            setTimeout(() => {
                alertBox.classList.replace('active', null);
                this.responseMsg = '';
            }, 1500);
        },
        moveNext: function () {
            // this.showLoader();
            var tab = document.querySelectorAll('div#content > div');            
            for (let index = 0; index < tab.length; index++) {
                if (tab[index].className == 'show-current-tab'){
                    var tabContent = document.getElementById('content');
                    tabContent.classList.add('hide');
                    tab[index].classList.remove('show-current-tab');
                // var loader = document.getElementById('loader');
                // loader.classList.replace('hide', 'show');
                setTimeout(() => {
                    tabContent.classList.replace('hide', null);
                    var newIndex = index + 1;
                    tab[newIndex].classList.add('show-current-tab');            
                }, 500);
                }        
            }
        },
        //tq sum
        tq_preprimary_male_tot: function(){
            this.tq_preprimary_male_total=0;
            this.tq_preprimary_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                
                return male_total + parseInt((item.preprimary_male==null)?0:item.preprimary_male); 
              },0);

              this.tq_male_total=0;
              this.tq_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                  
                  return male_total + parseInt((item.primary_male==null)?0:item.primary_male)+parseInt((item.preprimary_male==null)?0:item.preprimary_male); 
                },0);
        },

        tq_preprimary_female_tot: function(){
            this.tq_preprimary_female_total=0;
            this.tq_preprimary_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                
                return female_total + parseInt((item.preprimary_female==null)?0:item.preprimary_female); 
              },0);
              this.tq_female_total=0;
              this.tq_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                  
                  return female_total + parseInt((item.primary_female==null)?0:item.primary_female)+parseInt((item.preprimary_female==null)?0:item.preprimary_female); 
                },0);
        },

        tq_primary_male_tot: function(){
            this.tq_primary_male_total=0;
            this.tq_primary_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                
                return male_total + parseInt((item.primary_male==null)?0:item.primary_male); 
              },0);

              this.tq_male_total=0;
              this.tq_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                  
                  return male_total + parseInt((item.primary_male==null)?0:item.primary_male)+parseInt((item.preprimary_male==null)?0:item.preprimary_male); 
                },0);
        },

        tq_primary_female_tot: function(){
            this.tq_primary_female_total=0;
            this.tq_primary_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                
                return female_total + parseInt((item.primary_female==null)?0:item.primary_female); 
              },0);

              this.tq_female_total=0;
              this.tq_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                  
                  return female_total + parseInt((item.primary_female==null)?0:item.primary_female)+parseInt((item.preprimary_female==null)?0:item.preprimary_female); 
                },0);
        },
        //sum
        kindergarten1_male_tot: function(){
            this.kindergarten1_male_total=0;
            this.kindergarten1_male_total=parseInt((this.below3_kindergarten1_male==null)?0:this.below3_kindergarten1_male)+parseInt((this.age3_kindergarten1_male==null)?0:this.age3_kindergarten1_male);
        },

        kindergarten1_female_tot:function(){
            this.kindergarten1_female_total=0;
            this.kindergarten1_female_total=parseInt((this.below3_kindergarten1_female==null)?0:this.below3_kindergarten1_female)+parseInt((this.age3_kindergarten1_female==null)?0:this.age3_kindergarten1_female);

        },

        kindergarten2_male_tot:function(){
            this.kindergarten2_male_total=0;
            this.kindergarten2_male_total=parseInt((this.below3_kindergarten2_male==null)?0:this.below3_kindergarten2_male)+parseInt((this.age3_kindergarten2_male==0)?0:this.age3_kindergarten2_male);

        },

        kindergarten2_female_tot:function(){
            this.kindergarten2_female_total=0;
            this.kindergarten2_female_total=parseInt((this.below3_kindergarten2_female==null)?0:this.below3_kindergarten2_female)+parseInt((this.age3_kindergarten2_female==null)?0:this.age3_kindergarten2_female);

        },

        nursery1_male_tot:function(){
            this.nursery1_male_total=0;
            this.nursery1_male_total=parseInt((this.age3_nursery1_male==null)?0:this.age3_nursery1_male)+parseInt((this.age4_nursery1_male==null)?0:this.age4_nursery1_male)+parseInt((this.age5_nursery1_male==null)?0:this.age5_nursery1_male)+parseInt((this.above5_nursery1_male==null)?0:this.above5_nursery1_male);

        },

        nursery1_female_tot:function(){
            this.nursery1_female_total=0;
            this.nursery1_female_total=parseInt((this.age3_nursery1_female==null)?0:this.age3_nursery1_female)+parseInt((this.age4_nursery1_female==null)?0:this.age4_nursery1_female)+parseInt((this.age5_nursery1_female==null)?0:this.age5_nursery1_female)+parseInt((this.above5_nursery1_female==null)?0:this.above5_nursery1_female);

        },

        nursery2_male_tot:function(){
            this.nursery2_male_total=0;
            this.nursery2_male_total=parseInt((this.age3_nursery2_male==null)?0:this.age3_nursery2_male)+parseInt((this.age4_nursery2_male==null)?0:this.age4_nursery2_male)+parseInt((this.age5_nursery2_male==null)?0:this.age5_nursery2_male)+parseInt((this.above5_nursery2_male==null)?0:this.above5_nursery2_male);

        },

        nursery2_female_tot:function(){
            this.nursery2_female_total=0;
            this.nursery2_female_total=parseInt((this.age3_nursery2_female==null)?0:this.age3_nursery2_female)+parseInt((this.age4_nursery2_female==null)?0:this.age4_nursery2_female)+parseInt((this.age5_nursery2_female==0)?0:this.age5_nursery2_female)+parseInt((this.above5_nursery2_female==null)?0:this.above5_nursery2_female);

        },

        nursery3_male_tot:function(){
            this.nursery3_male_total=0;
            this.nursery3_male_total=parseInt((this.age3_nursery3_male==null)?0:this.age3_nursery3_male)+parseInt((this.age4_nursery3_male==null)?0:this.age4_nursery3_male)+parseInt((this.age5_nursery3_male==null)?0:this.age5_nursery3_male)+parseInt((this.above5_nursery3_male==null)?0:this.above5_nursery3_male);

        },
        
        nursery3_female_tot:function(){
            this.nursery3_female_total=0;
            this.nursery3_female_total=parseInt((this.age3_nursery3_female==null)?0:this.age3_nursery3_female)+parseInt((this.age4_nursery3_female==null)?0:this.age4_nursery3_female)+parseInt((this.age5_nursery3_female==null)?0:this.age5_nursery3_female)+parseInt((this.above5_nursery3_female==null)?0:this.above5_nursery3_female);

        },

        //sum total methods
        entrantmale_total: function(){
            this.p1_entrant_male_total=0;
            this.p1_entrant_male_total=this.fetch_age.reduce(function(entrantmale_total, item){
                
                return entrantmale_total + parseInt((item.male=='')?0:item.male); 
              },0);
        },

        entrantfemale_total:function(){
            this.p1_entrant_female_total=0;
            this.p1_entrant_female_total=this.fetch_age.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt((item.female=='')?0:item.female); 
                },0);
        },

        eccd_entrantmale_total:function(){
            this.p1_eccd_entrant_male_total=0;
            this.p1_eccd_entrant_male_total=this.fetch_age.reduce(function(entrantmale_total, item){
                
                return entrantmale_total + parseInt((item.eccd_male=='')?0:item.eccd_male); 
              },0);
        },

        eccd_entrantfemale_total:function(){
            this.p1_eccd_entrant_female_total=0;
            this.p1_eccd_entrant_female_total=this.fetch_age.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt((item.eccd_female=='')?0:item.eccd_female); 
                },0);
        },


        yearbyage_p1_male_tot:function(){
            this.yearbyage_p1_male_total=0;
            this.yearbyage_p1_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.primary1_male); 
                },0);
        },

        yearbyage_p1_female_tot:function(){
            this.yearbyage_p1_female_total=0;
            this.yearbyage_p1_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt((item.primary1_female==null)?0:item.primary1_female); 
                },0);
        },

        yearbyage_p2_male_tot:function(){
            this.yearbyage_p2_male_total=0;
            this.yearbyage_p2_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt((item.primary2_male==null)?0:item.primary2_male); 
                },0);
        },

        yearbyage_p2_female_tot:function(){
            this.yearbyage_p2_female_total=0;
            this.yearbyage_p2_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt((item.primary2_female==null)?0:item.primary2_female); 
                },0);
        },

        yearbyage_p3_male_tot:function(){
            this.yearbyage_p3_male_total=0;
            this.yearbyage_p3_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt((item.primary3_male==null)?0:item.primary3_male); 
                },0);
        },

        yearbyage_p3_female_tot:function(){
            this.yearbyage_p3_female_total=0;
            this.yearbyage_p3_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt((item.primary3_male==null)?0:item.primary3_male); 
                },0);
        },

        yearbyage_p4_male_tot:function(){
            this.yearbyage_p4_male_total=0;
            this.yearbyage_p4_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt((item.primary4_male==null)?0:item.primary4_male); 
                },0);
        },

        yearbyage_p4_female_tot:function(){
            this.yearbyage_p4_female_total=0;
            this.yearbyage_p4_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt((item.primary4_female==null)?0:item.primary4_female); 
                },0);
        },

        yearbyage_p5_male_tot:function(){
            this.yearbyage_p5_male_total=0;
            this.yearbyage_p5_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt((item.primary5_male==null)?0:item.primary5_male); 
                },0);
        },

        yearbyage_p5_female_tot:function(){
            this.yearbyage_p5_female_total=0;
            this.yearbyage_p5_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt((item.primary5_female==null)?0:item.primary5_female); 
                },0);
        },

        yearbyage_p6_male_tot:function(){
            this.yearbyage_p6_male_total=0;
            this.yearbyage_p6_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt((item.primary6_male==null)?0:item.primary6_male); 
                },0);
        },

        yearbyage_p6_female_tot:function(){
            this.yearbyage_p6_female_total=0;
            this.yearbyage_p6_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt((item.primary6_female==null)?0:item.primary6_female); 
                },0);
        },

        //staff total
        non_teaching_staff_total_change:function(){
            this.non_teaching_staff_total=0;
            this.non_teaching_staff_total=parseInt(this.non_teaching_staff_male)+parseInt(this.non_teaching_staff_female);
        },
        teaching_staff_total_change:function(){
            this.teaching_staff_total=0;
            this.teaching_staff_total=parseInt(this.teaching_staff_male)+parseInt(this.teaching_staff_female);
        },
        caregivers_total_change:function(){
            this.caregivers_total=0;
            this.caregivers_total=parseInt(this.caregivers_male)+parseInt(this.caregivers_female);
        },

        setLevelBool:function(){
            if(this.level_of_education=="Pre-primary"){
                this.ispreprimary=true;
                this.isprimary=false;
            }else if(this.level_of_education=="Primary"){
                this.isprimary=true;
                this.ispreprimary=false;
            }else if(this.level_of_education=="Pre-primary and primary"){
                this.isprimary=true;
                this.ispreprimary=true;
            }
        },

        //save the sections
        saveCensusYear: function (event) {
            if (event.target.id === 'next') {                
              this.popAlertMsg('Success');
              this.moveNext();
              console.log('next');
            } else if (event.target.id === 'save') {
              this.popAlertMsg('Saved');
              console.log('save');
            }
          },
          
        validNumericOrDecimalOrNull:function(number){
            var re = /(^$)|(^[1-9]\d*(\.\d+)?$)/;
            return re.test(number);
        },
      
        validAlphabetOrNull:function(alphabet){
            var re = /(^$)|(^[A-Za-z]+$)/;
            return re.test(alphabet);
        },
      
        validNumericOrDecimal:function(number){
            var re = /(^[1-9]\d*(\.\d+)?$)/;
            return re.test(number);
        },
      
        validAlphabet:function(alphabet){
            var re = /(^[A-Za-z]+$)/;
            return re.test(alphabet);
        },
      
        validEmail:function(email){
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },

        saveIdentification: function (event) {
            //use veevalidate later for this
            this.school_identification_errors=[];
            if(!this.validNumericOrDecimalOrNull(this.xcoordinate) || !this.validNumericOrDecimalOrNull(this.ycoordinate) 
            || !this.validNumericOrDecimalOrNull(this.zcoordinate) || this.validNumericOrDecimalOrNull(this.schoolname) 
            || this.validNumericOrDecimal(this.schooltown) || this.validNumericOrDecimal(this.schoolward) 
            || !this.validEmail(this.schoolemail)){
                
                //post errors
                this.popAlertMsg('Check the form for the required data');
                (!this.validNumericOrDecimalOrNull(this.xcoordinate))?this.school_identification_errors.push("x coordinate should be numeric decimal"):null;
                (!this.validNumericOrDecimalOrNull(this.ycoordinate))?this.school_identification_errors.push("y coordinate should be numeric decimal"):null;
                (!this.validNumericOrDecimalOrNull(this.zcoordinate))?this.school_identification_errors.push("z coordinate should be numeric decimal"):null;
                (this.validNumericOrDecimalOrNull(this.schoolname))?this.school_identification_errors.push("School Name should be alphabets"):null;
                (this.validNumericOrDecimal(this.schooltown))?this.school_identification_errors.push("School town should be alphabets"):null;
                (!this.validEmail(this.schoolemail))?this.school_identification_errors.push("School email incorrect format"):null;
                (this.validNumericOrDecimal(this.schoolward))?this.school_identification_errors.push("School ward should be alphabets"):null;
            }else{

                this.get_state_code = this.fetch_states[this.state_picked].name
                //alert(this.get_state_code);
                this.get_lga_code = this.fetch_lga[this.lga_picked].name;
                //alert(this.get_lga_code);
                
                axios.post("/api/Section/SchoolRegistration",
                {
                    school_code: schoolcode.toLowerCase(),
                    register_year: this.registered_censusyear,
                    year:year
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert("Data Saved to DB");
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            // this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(response.data.message);
                        //console.log(response.data);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                });
                axios.post("/api/Section/SchoolIdentification",
                {
                    school_code: schoolcode.toLowerCase(),
                    school_name: this.schoolname,
                    xcoordinate: this.xcoordinate,
                    ycoordinate: this.ycoordinate,
                    zcoordinate: this.zcoordinate,
                    school_address: this.schoolstreet,
                    town: this.schooltown,
                    ward: this.schoolward,
                    lga: this.get_lga_code,
                    state: this.get_state_code,
                    telephone: this.schooltelephone,
                    email: this.schoolemail
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert("Data Saved to DB");
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert("Check the form for the required data");
                        // this.popAlertMsg(this.resultsSave.status);
                        this.popAlertMsg('Check the form for the required data');
                    }
                });
            }
        },

        //log out
        logout:function(){
            axios.post("/api/Section/User/Logout",
            {
                email:this.user_email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.$cookies.remove("user");
                window.location.assign("/login");
            })
        },

        getLGA: function () {
            var arrayOfObjects2 = [];

            //this.counter_state = this.state_index[this.state_picked];
            //alert(this.state_picked);

            for (var i = 0; i < this.results[1].attributes.states[this.state_picked].data.lgas.length; i++) {
                var obj2 = {};
                
                obj2["name"] = this.results[1].attributes.states[this.state_picked].data.lgas[i].data.name;
                obj2["code"] = this.results[1].attributes.states[this.state_picked].data.lgas[i].data.lgacode;
                //this.lga_index[this.results[1].attributes.states[i].data.lgas[i].data.name] = i;
            

                arrayOfObjects2.push(obj2);
            }
            //this.lga_picked=this.results[1].attributes.states[this.counter_state].data.lgas[0].data.name;
            this.fetch_lga = arrayOfObjects2;


            console.log('now: ', JSON.stringify(this.fetch_states, null, " "));
            //alert(this.lga_picked);
        },

        saveCharacteristics: function (event) {
            
            axios.post("/api/Section/SchoolCharacteristics",
                {
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    year_of_establishment:this.year_of_establishment,
                    shared_facilities: this.facilities_choice,
                    schools_sharingwith: this.facilities_shared,
                    location_type: this.location_of_school,
                    school_type: this.type_of_school,
                    shift:this.shifts_choice,
                    level_of_education: this.level_of_education,
                    multi_grade_teaching: this.multigrade_choice,
                    distance_from_catchment_area: this.average_distance,
                    distance_from_lga:this.school_distance_lga,
                    students_travelling_3km: this.student_distance,
                    isboarding:this.boarding_choice,
                    male_student_b: this.students_boarding_male,
                    female_student_b: this.students_boarding_female,
                    has_school_development_plan: this.sdp_choice,
                    has_sbmc: this.sbmc_choice,
                    has_pta: this.pta_choice,
                    last_inspection_date: this.date_inspection,
                    no_of_inspection:this.no_of_inspection,
                    last_inspection_authority: this.authority_choice,
                    conditional_cash_transfer: this.cash_transfer,
                    has_school_grant: this.grants_choice,
                    has_security_guard: this.guard_choice,
                    ownership: this.ownership_choice,
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert("Data Saved to DB");
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                    }
                });
                
           
        },

        checkBirthCertificates:function(){
            alert("bc:"+this.fetch_birth_certificate[0].value+" ,male:"+this.fetch_birth_certificate[0].male);
        },

        saveBirthCertificates: function (event) {
            function checkNpc(bc) {
                return bc.value == "National Population Commission";
            }
            function checkHospital(bc) {
                return bc.value == "Hospital";
            }
            function checkLga(bc) {
                return bc.value == "LGA";
            }
            function checkCourt(bc) {
                return bc.value == "Court";
            }
            function checkUn(bc) {
                return bc.value == "UN";
            }
            function checkOthers(bc) {
                return bc.value == "Others";
            }

            axios.post("/api/Section/SchoolEnrollment/BirthCertificate",{
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class: "preprimary",
                birthcertificate:this.fetch_birth_certificate,
                
            },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //saved
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else if(this.resultsSave.status == "error") {
                        // alert(response.data.message);
                        this.popAlertMsg(response.data.message);
                    }else {
                        // alert("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                    }
                })
        },

        saveEntrants: function (event) {
           //alert(JSON.stringify(this.fetch_a))
            axios.post("/api/Section/SchoolEnrollment/Entrant",{
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class: "preprimary",
                entrants:this.fetch_age,
                
            },this.config).then(response => {

                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //this.fetch_b_c = this.fetch_b_c;
                        //alert("Data Saved to DB");
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                        // console.log("Data Saved to DB");
                    } else if(this.resultsSave.status == "error") {
                        // alert(response.data.message);
                        this.popAlertMsg(response.data.message);
                    }else {
                        // alert("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                    }
                })
        },

        saveEnrollmentByAge: function (event) {
            
            //alert(this.fetch_year_by_age.find(function(age){return age.value=="12"}).Primary3_male);
            var enrollments_objs=[{value:'Below 3',kindergarten1_male:this.below3_kindergarten1_male,kindergarten1_female:this.below3_kindergarten1_female,kindergarten2_male:this.below3_kindergarten2_male,kindergarten2_female:this.below3_kindergarten2_female},{value:'3',kindergarten1_male:this.age3_kindergarten1_male,kindergarten1_female:this.age3_kindergarten1_female,kindergarten2_male:this.age3_kindergarten2_male,kindergarten2_female:this.age3_kindergarten2_female,nursery1_male:this.age3_nursery1_male,nursery1_female:this.age3_nursery1_female,nursery2_male:this.age3_nursery2_male,nursery2_female:this.age3_nursery2_female,nursery3_male:this.age3_nursery3_male,nursery3_female:this.age3_nursery3_female},{value:'4',nursery1_male:this.age4_nursery1_male,nursery1_female:this.age4_nursery1_female,nursery2_male:this.age4_nursery2_male,nursery2_female:this.age4_nursery2_female,nursery3_male:this.age4_nursery3_male,nursery3_female:this.age4_nursery3_female},{value:'5',nursery1_male:this.age5_nursery1_male,nursery1_female:this.age5_nursery1_female,nursery2_male:this.age5_nursery2_male,nursery2_female:this.age5_nursery2_female,nursery3_male:this.age5_nursery3_male,nursery3_female:this.age5_nursery3_female},{value:'Above 5 Years',nursery1_male:this.above5_nursery1_male,nursery1_female:this.above5_nursery1_female,nursery2_male:this.above5_nursery2_male,nursery2_female:this.above5_nursery2_female,nursery3_male:this.above5_nursery3_male,nursery3_female:this.above5_nursery3_female}]
             axios.post("/api/Section/SchoolEnrollment/EnrollmentByAge",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "preprimary",
                 stream_kindergarten1_stream: this.kindergarten1_streams,
                 stream_kindergarten2_stream: this.kindergarten2_streams,
                 stream_nursery1_stream: this.nursery1_streams,
                 stream_nursery2_stream: this.nursery2_streams,
                 stream_nursery3_stream: this.nursery3_streams,

                 enrollments:enrollments_objs,
                 
             },this.config).then(response => {
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                         if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        } 
                     } else if(this.resultsSave.status == "error") {
                        //  alert(response.data.message);
                        this.popAlertMsg(response.data.message);
                     }else {
                        //  alert("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }
 
                 })
         },
        
        savePrimaryEnrollmentByAge: function (event) {
            //alert(this.fetch_year_by_age.find(function(age){return age.value=="12"}).Primary3_male);
             axios.post("/api/Section/SchoolEnrollment/EnrollmentByAge",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "primary",
                 stream_primary1_stream: this.p1_stream,
                 stream_primary2_stream: this.p2_stream,
                 stream_primary3_stream: this.p3_stream,
                 stream_primary4_stream: this.p4_stream,
                 stream_primary5_stream: this.p5_stream,
                 stream_primary6_stream: this.p6_stream,

                 stream_primary1_streamwithmultigrade:  this.p1_stream_with_multigrade,
                 stream_primary2_streamwithmultigrade:  this.p2_stream_with_multigrade,
                 stream_primary3_streamwithmultigrade:  this.p3_stream_with_multigrade,
                 stream_primary4_streamwithmultigrade:  this.p4_stream_with_multigrade,
                 stream_primary5_streamwithmultigrade:  this.p5_stream_with_multigrade,
                 stream_primary6_streamwithmultigrade:  this.p6_stream_with_multigrade,

                 enrollments:this.fetch_age_primary,

                 repeater_primary1_male:  this.p1_repeaters_male,
                 repeater_primary1_female:  this.p1_repeaters_female,
                 repeater_primary2_male:  this.p2_repeaters_male,
                 repeater_primary2_female:  this.p2_repeaters_female,
                 repeater_primary3_male:  this.p3_repeaters_male,
                 repeater_primary3_female:  this.p3_repeaters_female,
                 repeater_primary4_male:  this.p4_repeaters_male,
                 repeater_primary4_female:  this.p4_repeaters_female,
                 repeater_primary5_male:  this.p5_repeaters_male,
                 repeater_primary5_female:  this.p5_repeaters_female,
                 repeater_primary6_male:  this.p6_repeaters_male,
                 repeater_primary6_female:  this.p6_repeaters_female,

                 prevyear_primary6_male:this.prevyear_primary6_male,
                 prevyear_primary6_female:this.prevyear_primary6_female
                 
             },this.config).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                         if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                        
 
                     } else if(this.resultsSave.status == "error") {
                        //  alert(response.data.message);
                        this.popAlertMsg(response.data.message);
                     }else {
                        //  alert("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }
 
                 })
         },
        
        saveSpecialNeed: function (event) {
            //console.log(JSON.stringify(this.fetch_all_special_needs, null, 2))
            
             axios.post("/api/Section/SchoolEnrollment/SpecialNeeds",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "preprimary",

                 special_needs:this.fetch_special_needs,
               
             },this.config).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                        //  console.log("Data Saved to DB");
                         //console.log("0" + this.fetch_r[0].repeaters);
                         if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
 
                     } else if(this.resultsSave.status == "error") {
                        //  alert(response.data.message);
                        this.popAlertMsg(response.data.message);
                     }else {
                        //  alert("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }
                 })
         },

         saveStudentsFlow: function (event) {
            //alert(JSON.stringify(this.fetch_drop, null, 2))
 
             axios.post("/api/Section/SchoolEnrollment/StudentFlow",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "preprimary",

                 pupil_flow:this.fetch_pupilflow,
                 
                 
                 
             },this.config).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                         if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                     } else if(this.resultsSave.status == "error") {
                        //  alert(response.data.message);
                        this.popAlertMsg(response.data.message);
                     }else {
                        //  alert("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }
 
                 })
                 
         },

         saveStudentsOrphan: function (event) {
            //alert(JSON.stringify(this.fetch_drop, null, 2))
 
             axios.post("/api/Section/SchoolEnrollment/Orphan",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "preprimary",
                 
                 orphans:this.fetch_orphans,
                 
             },this.config).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                         if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                  
 
                     } else if(this.resultsSave.status == "error") {
                        //  alert(response.data.message);
                        this.popAlertMsg(response.data.message);
                     }else {
                        //  alert("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }
 
                 })
                 
         },

        saveExamination: function () {
            if(this.registered_male >= this.took_part_male && this.took_part_male >= this.passed_male && this.registered_female >= this.took_part_female && this.took_part_female >= this.passed_female){
                axios.post("/api/Section/SchoolEnrollment/Examination",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                examination: "jsce",

                examination_jsce_registeredmale: this.registered_male,
                examination_jsce_registeredfemale: this.registered_female,
                examination_jsce_tookpartmale: this.took_part_male,
                examination_jsce_tookpartfemale: this.took_part_female,
                examination_jsce_passedmale: this.passed_male,
                examination_jsce_passedfemale: this.passed_female

            },this.config).then(response => {

                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                    } else {
                        alert(this.resultsSave.message);
                    }
                })
            }else{
                alert("male and female registered is less than the took part or passed");
                this.popAlertMsg("Correct it!");
            }
            
        },

        showRemoveDialog: function (staffindex) {
            this.removedstaff_index=staffindex;
            $("div#clearStaff").addClass("active");
        
        },

        removeStaff:function(){
            //alert(this.fetch_staffs.indexOf(this.removedstaff_index));
            axios.post("/api/Section/Staff/Remove",
            {
                school_code: schoolcode.toLowerCase(),
                transfer_school_code: this.stateschool_picked,
                year: year,
                staff_id:this.fetch_staffs[this.removedstaff_index].id,
            
                removalcategory: this.removalcat,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        this.fetch_staffs.splice(this.removedstaff_index,1);
                        this.stateschool_picked='';
                    this.removalcat='';
                        //alert(this.resultsSave.message);
                    } else {
                        alert(this.resultsSave.message);
                    }
                });
            
        },

        saveListOfStaff: function(){
            //add to the table
            //var thestaff={staff_file_no:this.staff_file_no,staff_name:this.staff_name,staff_gender:this.staff_gender,staff_type:this.staff_type,salary_source:this.salary_source,staff_yob:this.staff_yob,staff_yfa:this.staff_yfa,staff_ypa:this.staff_ypa,staff_yps:this.staff_yps,staff_level:this.staff_level,present:this.present,academic_qualification:this.academic_qualification,teaching_qualification:this.teaching_qualification,area_specialisation:this.area_specialisation,subject_taught:this.subject_taught,teaching_type:this.teaching_type,is_teaching_ss:this.is_teaching_ss,attended_training:this.attended_training};
            
            alert(this.fetch_staffs);
            axios.post("/api/Section/Staff/Addlist",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',

                stafflist:this.fetch_staffs,
                
            },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        alert(this.resultsSave.message);
                    } else {
                        alert(this.resultsSave.message);
                    }
            })

            //if successfully posted add to the table
            //this.fetch_staffs.push(listofstaffs);


        },

        saveNoOfStaff: function (event) {
            axios.post("/api/Section/Staff/NoOfStaff",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'preprimary',
                
                staff_nonteachersmale: this.non_teaching_staff_male,
                staff_nonteachersfemale: this.non_teaching_staff_female,
                staff_teachersmale: this.teaching_staff_male,
                staff_teachersfemale: this.teaching_staff_female,
                caregivers_male:this.caregivers_male,
                caregivers_female:this.caregivers_female,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert("error: "+this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                });
        },
        
        staffInformation: function (event) {
            if (event.target.id === 'next') {                
                this.popAlertMsg('Success');
                this.moveNext();
                console.log('next');
              } else if (event.target.id === 'save') {
                this.popAlertMsg('Saved');
                console.log('save');
              }
        },

        submitStaff: function(){
            if(this.staff_name != ''||this.staff_gender!=''){
                 //add to the table
            var thestaff={id:null,staff_file_no:this.staff_file_no,staff_name:this.staff_name,staff_gender:this.staff_gender,staff_type:this.staff_type,salary_source:this.salary_source,staff_yob:this.staff_yob,staff_yfa:this.staff_yfa,staff_ypa:this.staff_ypa,staff_yps:this.staff_yps,staff_level:this.staff_level,present:this.present,academic_qualification:this.academic_qualification,teaching_qualification:this.teaching_qualification,area_specialisation:this.area_specialisation,subject_taught:this.subject_taught,teaching_type:this.teaching_type,is_teaching_ss:this.is_teaching_ss,attended_training:this.attended_training};
            
            //post to the savestaff api
            axios.post("/api/Section/Staff/Addnew",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',

                staff_file_no: this.staff_file_no,
                staff_name:this.staff_name,
                gender:this.staff_gender,
                stafftype:this.staff_type,
                salary_source:this.salary_source,
                dob:this.staff_yob,
                year_of_first_appointment:this.staff_yfa,
                present_appointment_year:this.staff_ypa,
                year_of_posting:this.staff_yps,
                level:this.staff_level,
                present:this.present,
                academic_qualification:this.academic_qualification,
                teaching_qualification:this.teaching_qualification,
                specialisation:this.area_specialisation,
                mainsubject_taught:this.subject_taught,
                teaching_type:this.teaching_type,
                is_teaching_ss:this.is_teaching_ss,
                teacher_attended_training:this.attended_training,

            },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        //if successfully posted add to the table
                        thestaff.id=this.resultsSave.id;
                        this.fetch_staffs.push(thestaff);
                        this.staff_file_no='';
                        this.staff_name='';
                        this.staff_gender='';
                        this.staff_type='';
                        this.salary_source='';
                        this.staff_yob='';
                        this.staff_yfa='';
                        this.staff_ypa='';
                        this.staff_yps='';
                        this.staff_level='';
                        this.present='';
                        this.academic_qualification='';
                        this.teaching_qualification='';
                        this.area_specialisation='';
                        this.subject_taught=''
                        this.teaching_type='';
                        this.is_teaching_ss='';
                        this.attended_training='';

                    } else {
                        alert(this.resultsSave.message);
                    }
            })
            }else{
                alert("Form empty, fill the new staff entry form")
            }
           
        
        },

        submitClassroom: function(){
            //add to the table
            var theclassroom={year_of_construction:this.year_of_construction,present_condition:this.present_condition,length_in_meters:this.classroom_length,width_in_meters:this.classroom_width,floor_material:this.floor_material,roof_material:this.roof_material,wall_material:this.wall_material,seating:this.seating,good_blackboard:this.blackboard};
            
            //post to the savestaff api
            axios.post("/api/Section/Classroom/Addnew",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',

                year_of_construction: this.year_of_construction,
                present_condition:this.present_condition,
                length_in_meters:this.classroom_length,
                width_in_meters:this.classroom_width,
                floor_material:this.floor_material,
                roof_material:this.roof_material,
                wall_material:this.wall_material,
                seating:this.seating,
                good_blackboard:this.blackboard,
                
            },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.id);

                        theclassroom.id=this.resultsSave.id;

                        //alert(theclassroom.id);
                        this.fetch_classrooms.push(theclassroom);
                        this.year_of_construction='';
                        this.present_condition='';
                        this.classroom_length='';
                        this.classroom_width='';
                        this.roof_material='';
                        this.floor_material='';
                        this.wall_material='';
                        this.seating='';
                        this.blackboard='';
                    } else {
                        alert(this.resultsSave.message);
                    }
            })
        },

        submitClassrooms: function(event){
            axios.post("/api/Section/Classroom/Addlist",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                
                classrooms:this.fetch_classrooms,
                
            },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
            })
        },

        removeClassroom:function(message,ind){
            //alert(this.fetch_staffs.indexOf(this.removedstaff_index));
            axios.post("/api/Section/Classroom/Remove",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                class_id:message,
            
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        this.fetch_classrooms.splice(ind,1);
                        
                        //alert(this.resultsSave.message);
                    } else {
                        alert(this.resultsSave.message);
                    }
                });
            
        },

        saveNoOfClassrooms: function (event) {
            axios.post("/api/Section/Classroom/NoOfClassroom",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'preprimary',
                

                no_of_classrooms: this.no_of_classrooms,
                classes_held_outside: this.classes_held_outside,
                no_playrooms: this.eccd_playrooms,
                
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                })
            },
        
        saveNoOfOtherrooms: function (event) {
            axios.post("/api/Section/Room/Otherroom",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                no_of_staffrooms: this.fetch_otherRooms.find(function(room){return room.value=="Staff room"}).no_of_rooms,
                no_of_offices: this.fetch_otherRooms.find(function(room){return room.value=="Office"}).no_of_rooms,
                no_of_laboratories: this.fetch_otherRooms.find(function(room){return room.value=="Laboratories"}).no_of_rooms,
                no_of_storerooms: this.fetch_otherRooms.find(function(room){return room.value=="Store room"}).no_of_rooms,
                no_of_others: this.fetch_otherRooms.find(function(room){return room.value=="Others"}).no_of_rooms,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                })
        },

        saveDWSources: function (event) {
            axios.post("/api/Section/Facility/DrinkingWater",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                drinking_water_sources: this.sources_of_drinking_water,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                })
        },
        saveFacilitiesAvailable: function (event) {
            axios.post("/api/Section/Facility/Available",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                facilities_available:this.fetch_facilities,

                
                
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                })
        },

        saveToilet: function (event) {
            //alert(this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).male_students);
            axios.post("/api/Section/Facility/Toilet",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                toilets:this.fetch_toilet_type,

                

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                })
        },

        saveSharedFacilities: function (event) {
            axios.post("/api/Section/Facility/SharedFacilities",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                shared_facilities: this.shared_facilities,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                })
        },

        saveUndertaking:function(event){

            axios.post("/api/Section/Undertaking",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                
            
                attestation_headteacher_name: this.attestation_headteacher_name,
                attestation_headteacher_telephone: this.attestation_headteacher_telephone,
                attestation_headteacher_signdate: this.attestation_headteacher_signdate,
                attestation_enumerator_name: this.attestation_enumerator_name,
                attestation_enumerator_position: this.attestation_enumerator_position,
                attestation_enumerator_telephone: this.attestation_enumerator_telephone,
                attestation_supervisor_name: this.attestation_supervisor_name,
                attestation_supervisor_position: this.attestation_supervisor_position,
                attestation_supervisor_telephone: this.attestation_supervisor_telephone,
                
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.message);
                    }
                })
        },

        //source of power
        savePowerSource: function (event) {
            axios.post("/api/Section/Facility/PowerSource",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                power_source: this.sources_of_power,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                })
        },

        //source of power
        saveStudentbySubject: function () {
            axios.post("/api/Section/Subject/Students",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                students_by_subject: this.fetch_subjects,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                    } else {
                        alert(this.resultsSave.message);
                    }
                })
        },

        saveLearningMaterial: function (event) {
            axios.post("/api/Section/Facility/LearningMaterial",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                materials: this.learning_materials,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                });

            

        },

        savePlay: function (event) {
            axios.post("/api/Section/Facility/PlayRoom",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                playroom: this.facility_playroom,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            // this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                });

            axios.post("/api/Section/Facility/PlayFacilities",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                play_facilities: this.play_facilities,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                });
        },

        changedwsource: function(message,event){
            if(event.target.checked==false){
                axios.post("/api/Section/Remove/DrinkingSource",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        source:message
                        
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //alert("Data Saved to DB");
                        } else {
                            alert("Check the form for the required data");
                        }
                    });
            }
        },

        changesharedfacilities: function(message,event){
            if(event.target.checked==false){
                axios.post("/api/Section/Remove/SharedFacility",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        shared_facility:message
                        
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //alert("Data Saved to DB");
                        } else {
                            alert(this.resultsSave.message);
                        }
                    });
            }
            
        },

        changesourceofpower: function(message,event){
            if(event.target.checked==false){
                axios.post("/api/Section/Remove/PowerSource",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        power_source:message
                        
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //alert("Data Saved to DB");
                        } else {
                            alert("Check the form for the required data");
                        }
                    });
            }
        },

        changelearningmaterials: function(message,event){
            if(event.target.checked==false){
                axios.post("/api/Section/Remove/LearningMaterial",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        material:message
                        
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //alert("Data Saved to DB");
                        } else {
                            alert("Check the form for the required data");
                        }
                    });
            }
            
        },

        changeplayfacilities: function(message,event){
            if(event.target.checked==false){
                axios.post("/api/Section/Remove/PlayFacility",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        play_facility:message
                        
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //alert("Data Saved to DB");
                        } else {
                            alert("Check the form for the required data");
                        }
                    });
            }
            
        },

        saveHealthFence: function (event) {
            axios.post("/api/Section/Facility/HealthFacility",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                health_facility: this.health_facility,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            // this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                });

            axios.post("/api/Section/Facility/FenceFacility",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                fence_facility: this.fence_facility,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                });

        },

        saveStudents:function(event){
            axios.post("/api/Section/Book/Student",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'primary',
            
                students_book: this.fetch_mainsubjects,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                });
        },

        saveCaregiversManual:function(event){
            //alert(this.fetch_caregiversmanual[1].available);
            axios.post("/api/Section/Caregivers/Manual",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school:'primary',
            
                caregiversmanuals: this.fetch_caregiversmanual,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                });
        },

        saveSeaters:function(event){

            axios.post("/api/Section/Facility/Seater",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'primary',
            
                seaters: this.fetch_seaters,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                })
        },

        saveTeachers:function(event){
            
            axios.post("/api/Section/Book/Teacher",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'primary',
            
                teachers_book: this.fetch_mainsubjects,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                })
        },

        saveTeacherQualification:function(event){

            axios.post("/api/Section/Teacher/Qualification",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school:'primary',
            
                teacher_qualification: this.fetch_teachingqualification,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(response.data.message);
                    }
                })
        },
    },

    

});