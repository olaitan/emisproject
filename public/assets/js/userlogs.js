

//vue vm for the form

var app = new Vue({
    el: '#app',
    data: {
        results: [],
        fetch_userlogs: [],
        resultsSave: [],
        //school identification
        schoolcode: '',
        category:'',                                                
        auth_user:'',
        config:'',
    },

    computed:{
        
    },

    watch:{
        
    },

    mounted: function(){
        if(this.$cookies.isKey("user")){
            //get the access token
            this.user=this.$cookies.get("user");
            this.config = {
                headers: {
                  Accept: "application/json",
                  Authorization:"Bearer "+this.user.session,
                }
              }
//setting the theme color
var leftTab = document.querySelectorAll('.theme');
for (let i = 0; i < leftTab.length; i++) {
    leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
} 
            axios.post("/api/Details",
            {
                email:this.user.email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.auth_user=response.data;
            }).catch(error=>{
                this.$cookies.remove("user");
                window.location.assign("/login");
            })

            axios.get("/api/userlogs").then(response => {
                this.fetch_userlogs = response.data;//returns the json data
    
               
                //this.results =  JSON.parse(response.data);
                //console.log(this.results);
                console.log('r: ', JSON.stringify(response.data, null, 2));
                
                
            });

        }else{
            //this.$cookies.remove("user");
            window.location.assign("/login");
        }

        
    },

    methods:{
        //log out
        logout:function(){
            axios.post("/api/Section/User/Logout",
            {
                email:this.user_email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.$cookies.remove("user");
                window.location.assign("/login");
            })
        },
        

    },
});