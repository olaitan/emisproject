  //vue vm for the form 
  var app = new Vue({
    el: '#app',
    data: {
        fetch_roles: [],
        fetch_states: [],
        resultsSave: [],
        //school identification
        name:'',
        login_email: '',
        login_password:'',
        email: '',
        password:'',
        c_password:'',
        searchForm:'',
        createPWD: '',
        loginForm: '',
        user:'',
        auth_user:'',
        config:'',
        user_state:0,
        

    },

    mounted: function(){
        //check if an access token exist
        if(this.$cookies.isKey("appclient")){
            //get the access token
            window.location.assign("/clientAPI");
        }
        this.searchForm = document.getElementById("searchUser");
        this.createPWD = document.getElementById("createPwd");
        this.loginForm = document.getElementById("loginDetails");

        axios.get("/api/roles").then(response => {
            console.log(JSON.stringify(response.data, null, 2));
            this.fetch_roles=response.data;            
        })
//setting the theme color
var leftTab = document.querySelectorAll('.theme');
for (let i = 0; i < leftTab.length; i++) {
    leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
} 
        //get api for states
        axios.get("/api/states").then(response => {
            console.log(JSON.stringify(response.data, null, 2));
            this.fetch_states=response.data;
        })
        
    },

    methods:{

        login:function(){            
            axios.post("/api/Section/AppClient/Login",
            {
                email: this.login_email,
                password:this.login_password,
                }).then(response => {
                    
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        console.log("success");                   
                        var user = {name:this.resultsSave.success['name'],email:this.resultsSave.success['email'],session:this.resultsSave.success.token };
                        this.$cookies.set('appclient',user,"35d");
                        // print user name
                        console.log(this.$cookies.get('appclient').email);
                        window.location.assign("/clientAPI");
                    } else {
                        alert(this.resultsSave.message);
                    }
                })
        },

        signup:function(){            
            axios.post("/api/Section/AppClient/Register",
            {
                name:this.name,
                email: this.email,
                password:this.password,
                c_password:this.c_password,
                state_id:this.user_state,

                }).then(response => {
                    
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        console.log("success");                   
                        var user = {name:this.resultsSave.success['name'],email:this.resultsSave.success['email'],session:this.resultsSave.success.token };
                        this.$cookies.set('appclient',user,"35d");
                        // print user name
                        console.log(this.$cookies.get('appclient').email);
                        window.location.assign("/clientAPI");
                    } else {
                        alert(this.resultsSave.message);
                    }
                })
        },
    },

    

    
});