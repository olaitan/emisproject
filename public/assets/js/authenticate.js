  //vue vm for the form 
  var app = new Vue({
    el: '#app',
    data: {
        fetch_roles: [],
        resultsSave: [],
        //school identification
        email: '',
        password:'',
        c_password:'',
        searchForm:'',
        createPWD: '',
        loginForm: '',
        user:'',
        auth_user:'',
        config:'',
        
    },

    mounted: function(){
        //check if an access token exist
        if(this.$cookies.isKey("user")){
            //get the access token
            window.location.assign("/");
        }
        //setting the theme color
        var leftTab = document.querySelectorAll('.theme');
        for (let i = 0; i < leftTab.length; i++) {
            leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
        } 
        this.searchForm = document.getElementById("searchUser");
        this.createPWD = document.getElementById("createPwd");
        this.loginForm = document.getElementById("loginDetails");

        axios.get("/api/roles").then(response => {
            console.log(JSON.stringify(response.data, null, 2));
            this.fetch_roles=response.data;            
        })
        
    },

    methods:{

        searchEmail: function(){
            //add to the table
            
            //post to the savestaff api
            axios.post("/api/Section/User/SearchEmail",
            {
                
                email: this.email,
                
                
            }).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        console.log("success");
                        if(this.resultsSave.password_exist=="no"){
                            const spin = document.getElementById("spinner");
                            spin.classList.add("active");
                            setTimeout(() => {
                                this.searchForm.classList.remove("active");
                                this.createPWD.classList.add("active");
                            }, 2000)
        
                        }else{
                            const spin = document.getElementById("spinner");
                            spin.classList.add("active");
                            setTimeout(() => {
                                this.searchForm.classList.remove("active");
                                this.loginForm.classList.add("active");
                            }, 2000)
                        }
                        
                    } else {
                        const spin = document.getElementById("spinner")
                        spin.classList.add("active");
                        setTimeout(() => {
                            spin.classList.remove('active')
                            document.getElementById('errorEmail').classList.add('active');
                        }, 1000);
                        //alert(this.resultsSave.message);
                    }
            })
        },

        createPassword:function(){
            axios.post("/api/Section/User/CreatePassword",
            {
                email: this.email,
                password:this.password,
                c_password:this.c_password,
    
                }).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        console.log("success");                   
                        var user = {email:this.resultsSave.success['email'],session:this.resultsSave.success.token };
                        this.$cookies.set('user',user,"1d");
                        this.$cookies.set('theme',this.resultsSave.theme,"30d" );
                        // print user name
                        console.log(this.$cookies.get('user').email);
                        window.location.assign("/");
                    } else {
                        alert(this.resultsSave.message);
                    }
                })
        },

        login:function(){            
            axios.post("/api/Section/User/Login",
            {
                email: this.email,
                password:this.password,
                }).then(response => {
                    
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        console.log("success");                   
                        var user = {email:this.resultsSave.success['email'],session:this.resultsSave.success.token};
                        this.$cookies.set('user',user,"27d");
                        this.$cookies.set('theme',this.resultsSave.theme,"30d" );
                        // print user name
                        //alert(this.$cookies.get('user').session);
                        console.log(this.$cookies.get('user').email);
                        window.location.assign("/");
                    } else {
                        alert(this.resultsSave.message);
                    }
                })
        },
    },    
});