
    //var url = window.location.href;//get the school code and year by splitting the url
    //var res = url.split("/");
    var schoolcode = 'sch001';
    var year = '2016';

    //vue vm for the form

    var app = new Vue({
        el: '#app',
        data: {
            //School Identification errors
            school_identification_errors: [],
            //School Characteristics errors
            school_characteristics_errors: [],
            //jsce
            jsce_errors: [],
            //ssce
            ssce_errors: [],

            responseMsg: '',
            results: [],
            resultsSave: [],
            //select form type
            school_form_type:'',
            

            //school identification
            schoolcode: '',
            schoolname: 'SCHOOL NAME',
            xcoordinate:'',
            ycoordinate:'',
            zcoordinate:'',
            schoolstreet: '',
            schoolemail: '',
            schoollga: 'LGA',
            schooltown: '',
            schooltelephone: '',
            schoolward: '',
            schoolstate: 'STATE',
            fetch_states: [],
            state_picked: '',
            state_index: [],
            get_state_code: '',
            results_lga: [],
            fetch_lga: [],
            lga_picked: '',
            lga_index: [],
            get_lga_code: '',
            counter_state: 0,
            counter_lga: 0,
            location_type_ref: [],
            location_type_val: [],
            metadata: [],

            //metadata arrays
            fetch_location: [],
            fetch_levels: [],
            fetch_category: [],
            fetch_authority: [],
            fetch_ownership: [],
            fetch_birth_certificate: [],
            fetch_b_c: [],
            fetch_b_c_no: [],
            fetch_age: [],
            fetch_ss_age: [],
            fetch_a: [],
            fetch_a_no: [],
            fetch_streams: [],
            fetch_Ss_streams: [],
            fetch_year_by_age: [],
            fetch_ss_year_by_age: [],
            fetch_year_by_age_no: [],
            fetch_y_b_a1: [], fetch_y_b_a2: [], fetch_y_b_a3: [],
            fetch_y_b_no1: [], fetch_y_b_no2: [], fetch_y_b_no3: [],
            fetch_all_ages: [],
            fetch_repeaters: [], fetch_r:[], 
            fetch_pupilflow: [],
            fetch_dropouts: [], fetch_drop:[],
            fetch_transfer_in: [], fetch_t_in:[],
            fetch_transfer_out: [], fetch_t_out:[],
            fetch_promoted: [], fetch_pro:[],
            fetch_examination:[],
            fetch_all_special_needs: [], fetch_special_needs: [], fetch_special_needs_item: [],
            fetch_s_n1: [], fetch_s_n2: [], fetch_s_n3: [],
            fetch_s_n_no1: [], fetch_s_n_no2: [], fetch_s_n_no3: [],
            fetch_a_s_n: [], fetch_a_s_n_no: [],

            inputParams: [],
            exam_type:'',
            registered_male:'',
            registered_female:'',
            registered_total:'', 
            took_part_male:'',
            took_part_female:'',
            took_part_total:'',
            passed_male:'',
            passed_female:'',
            passed_total:'',
            
            registered_ss_male:'',
            registered_ss_female:'',
            registered_ss_total:'', 
            took_part_ss_male:'',
            took_part_ss_female:'',
            took_part_ss_total:'',
            passed_ss_male:'',
            passed_ss_female:'',
            passed_ss_total:'',

            //school characteristics
            //censusyear:year,
            year: '',
            year_of_establishment: '',
            location_of_school: '',
            level_of_education: 'LEVEL',
            type_of_school: '',
            shifts_choice: '',
            facilities_choice: '',
            boarding_choice: '',
            facilities_shared: '',
            multigrade_choice: '',
            multigrade_choice_bi: '',
            average_distance: '',
            student_distance: '',
            students_boarding_female: '',
            students_boarding_male: '',
            sdp_choice: '',
            sdp_choice_bi: '',
            sbmc_choice: '',
            sbmc_choice_bi: '',
            pta_choice: '',
            pta_choice_bi: '',
            date_inspection: '',
            no_of_inspection: '',
            authority_choice: '',
            cash_transfer: '',
            grants_choice: '',
            grants_choice_bi: '',
            guard_choice: '',
            guard_choice_bi: '',
            ownership_choice: '',

            //C4
            jss1_stream:'',
            jss2_stream:'',
            jss3_stream:'',
            jss1_stream_with_multigrade:'',
            jss2_stream_with_multigrade:'',
            jss3_stream_with_multigrade:'',
            jss1_repeaters_male: '',
            jss1_repeaters_female:'',
            jss2_repeaters_male:'',
            jss2_repeaters_female:'',
            jss3_repeaters_male:'',
            jss3_repeaters_female:'',

            //Total
            entrantmaletotal:'',
            entrantfemaletotal:'',
            jss1maletotal:'',
            jss1femaletotal:'',
            jss2maletotal:'',
            jss2femaletotal:'',
            jss3maletotal:'',
            jss3femaletotal:'',
            ss1_entrantmaletotal:'',
            ss1_entrantfemaletotal:'',
            ss1maletotal:'',
            ss1femaletotal:'',
            ss2maletotal:'',
            ss2femaletotal:'',
            ss3maletotal:'',
            ss3femaletotal:'',


            //previous year
            prevyear_jss3_male:'',
            prevyear_jss3_female:'',

            ss1_stream:'',
            ss2_stream:'',
            ss3_stream:'',
            ss1_stream_with_multigrade:'',
            ss2_stream_with_multigrade:'',
            ss3_stream_with_multigrade:'',
            ss1_repeaters_male: '',
            ss1_repeaters_female:'',
            ss2_repeaters_male:'',
            ss2_repeaters_female:'',
            ss3_repeaters_male:'',
            ss3_repeaters_female:'',
            prevyear_sss3_male:'',
            prevyear_sss3_female:'',
            //breadcrumbs href
            schoolstate_href:'',
            lga_href:'',
            level_of_education_href:'',
            school_type_href:'',

           //staff
        non_teaching_staff_male:'',
        non_teaching_staff_female:'',
        non_teaching_staff_total:'',
        teaching_staff_male:'',
        teaching_staff_female:'',
        teaching_staff_total:'',
        staff_id:'',
        staff_file_no:'',
        staff_name:'',
        staff_gender:'',
        staff_type:'',
        salary_source:'',
        staff_yob:'',
        staff_yfa:'',
        staff_ypa:'',
        staff_yps:'',
        staff_level:'',
        present:'',
        academic_qualification:'',
        teaching_qualification:'',
        area_specialisation:'',
        subject_taught:'',
        teaching_type:'',
        is_teaching_ss:'',
        attended_training:'',

        //fetch array for staff features
        fetch_teachingtype:[],
        fetch_subjecttaught:[],
        fetch_specialisation:[],
        fetch_academicqualification:[],
        fetch_teachingqualification:[],
        fetch_present:[],
        fetch_salarysource:[],
        fetch_stafftype:[],
        //fetch array for the staffs
        fetch_staffs:[],

        //Classrooms
        no_of_classrooms:'',
        classes_held_outside:'',
        fetch_otherRooms:[],
        fetch_drinkingwater_source:[],
        fetch_power_source:[],
        fetch_toilet_type:[],
        fetch_facilities:[],
        shared_facilities:[],
        

        //facilities
        sources_of_drinking_water:[],
        sources_of_power:[],
        fetch_healthfacility:[],
        health_facility:'',
        fence_facility:'',
        fetch_fence:[],
        fetch_mainsubjects:[],
        fetch_subjects:[],
        fetch_subjectsSS:[],
        fetch_seaters:[],

        //classrooms
        blackboard:'',
        seating:'',
        wall_material:'',
        floor_material:'',
        fetch_wallmaterial:[],
        fetch_classrooms:[],
        fetch_floormaterial:[],
        fetch_roofmaterial:[],
        roof_material:'',
        classroom_length:'',
        classroom_width:'',
        present_condition:'',
        fetch_presentcondition:[],
        year_of_construction:'',
        fetch_stafflist:[],

        //removal
        fetch_removalcategory:[],
        fetch_stateschools:[],
        censusyear:'2016',

        removalcat:'',
        stateschool_picked:'',
        removedstaff_index:'',

        //teachers qualification
        tq_jss_male_total:'',
        tq_jss_female_total:'',
        tq_sss_male_total:'',
        tq_sss_female_total:'',
        tq_male_total:'',
        tq_female_total:'',

        //undertaking
        attestation_headteacher_name:'',
        attestation_headteacher_telephone:'',
        attestation_headteacher_signdate:'',
        attestation_enumerator_name:'',
        attestation_enumerator_position:'',
        attestation_enumerator_telephone:'',
        attestation_supervisor_name:'',
        attestation_supervisor_position:'',
        attestation_supervisor_telephone:'',

        auth_user:'',
        config:'',
        fetch_yearslist:[],
        registered_censusyear:'',
        isjs:false,
        isss:false,
        datapreviewurl:'',

        //offline array of objects container
        saveRegistrationContainer:{},
        saveIdentificationContainer:{},
        saveCharacteristicsContainer:{},
        saveJssBirthCertificatesContainer:{},
        saveSssBirthCertificatesContainer:{},
        saveJsEntrants:{},
        saveSsEntrants:{},
        saveJsEnrollmentByAge:{},
        saveSsEnrollmentByAge:{},
        saveJsSpecialNeed:{},
        saveSsSpecialNeed:{},
        saveJsStudentsFlow:{},
        saveSsStudentsFlow:{},
        saveJsceExamination:{},
        saveSsceExamination:{},
        saveNoOfStaff:{},
        saveStaff:{},
        saveListOfStaffModel:{},
        saveClassroom:{},
        saveClassrooms:{},
        saveNoOfClassroomsModel:{},
        saveNoOfOtherroomsModel:{},
        saveDWSourcesModel:{},
        saveFacilitiesAvailableModel:{},
        saveToiletModel:{},
        saveSharedFacilitiesModel:{},
        savePowerSourceModel:{},
        saveStudentbySubjectModel:{},
        saveStudentbySubjectSSModel:{},
        saveHealth:{},
        saveFence:{},
        saveStudentsModel:{},
        saveStudentsSSModel:{},
        saveTeachersModel:{},
        saveTeachersSSModel:{},
        saveUndertakingModel:{},
        saveSeatersModel:{},
        saveTeacherQualificationModel:{}

        },

        watch:{
            facilities_choice: function() {        
                var facilitiesShared = document.getElementById("no_shared-facilities");
                if (this.facilities_choice == 0) {
                  if (facilitiesShared) {
                    facilitiesShared.required = false;
                    facilitiesShared.disabled = true;
                  }
                } else {
                  facilitiesShared.required = true;
                  facilitiesShared.disabled = false;
                }
              },
              boarding_choice: function() {        
                var boardingmale = document.getElementById("boarding_male");
                var boardingfemale = document.getElementById("boarding_female");
                if (this.boarding_choice == 0) {
                    
                    boardingmale.required = false;
                    boardingmale.disabled = true;
                    boardingfemale.required = false;
                    boardingfemale.disabled = true;
                  
                } else {
                  boardingmale.required = true;
                  boardingmale.disabled = false;
                  boardingfemale.required = true;
                  boardingfemale.disabled = false;
                }
              }
        },

        mounted: function(){
            if(this.$cookies.isKey("user")){
                //initialized the years array
                var dt=new Date();
                for(i=1860;i<dt.getFullYear();i++){
                    this.fetch_yearslist.push(i);
                }
                //get the access token
                this.user=this.$cookies.get("user");
                this.config = {
                    headers: {
                      Accept: "application/json",
                      Authorization:"Bearer "+this.user.session,
                    }
                  }
    
                axios.post("/api/Details",
                {
                    email:this.user.email,
                },this.config).then(response => {
                    console.log(JSON.stringify(response.data, null, 2));
                    this.auth_user=response.data;
                }).catch(error=>{
                    this.$cookies.remove("user");
                    window.location.assign("/login");
                })
    
                axios.get("/api/roles").then(response => {
    
                    console.log(JSON.stringify(response.data, null, 2));
        
                    this.fetch_roles=response.data;
                    
                })
    
                this.schoolcode=schoolcode;
                this.datapreviewurl="/school/" + schoolcode.toLowerCase() + "/year/" + year+"/preview";

            axios.get("/api/all/metadata",this.config).then(response => {
                this.results = response.data;//returns the json data
                console.log('r: ', JSON.stringify(response.data, null, 2));
                
                //School Characteristics
                //bind the metadata
                this.fetch_location=this.results.Secondary.data.filter(function(metadata){
                        return metadata.reference=="Location Type";
                });
                
                this.fetch_levels=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="School Level";
                });

                this.fetch_category=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="School Category";
                });

                this.fetch_authority=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Ownership - 2013";
                });

                this.fetch_ownership=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Ownership - 2013";
                });

                    //Apply the metadata to the form sections
                    //bind the states list
                    var arrayOfObjects = []

                    for (var i = 0; i < this.results.States.length; i++) {//loop the states
                        var obj = {};
                        obj["name"] = this.results.States[i].data.name;
                        obj["code"] = this.results.States[i].data.statecode;
                    
                        //save the state name as a key and the value is the index
                        if(obj["name"]==this.schoolstate){
                            this.state_picked=i;
                        }
                        arrayOfObjects.push(obj);
                    }

                    this.fetch_states = arrayOfObjects;
                    var arrayOfObjects2 = []
                    var tempIndex=0;

                    ////LGA
                    for (var i = 0; i < this.results.States.length; i++) {
                        
                        for (var j = 0; j < this.results.States[i].data.lgas.length; j++) {
                            var obj2 = {};
                            obj2["name"] = this.results.States[i].data.lgas[j].data.name;
                            obj2["code"] = this.results.States[i].data.lgas[j].data.lgacode;

                            arrayOfObjects2.push(obj2);
                            if(obj2["name"]==this.schoollga){
                                this.lga_picked=tempIndex;
                                
                            }
                            tempIndex++;
                        }
                    }
                    // this.lga_picked = this.results[1].attributes.states[0].data.lgas[0].data.name;
                    //alert(this.lga_picked);   
                    this.fetch_lga = arrayOfObjects2;


                    //SECTION C ENROLLMENT
                    //C1
                    //Bind metadata
                    this.fetch_birth_certificate=this.results.Secondary.data.filter(function(metadata){
                        return metadata.reference=="Birth Certificate Type";
                    });
                    this.fetch_birth_certificate.sort(function(a,b){return a.order-b.order});
                    
                    //C2
                    //Bind metadata for js Entrants
                    this.fetch_age=this.results.Secondary.data.filter(function(metadata){
                        return metadata.reference=="Age Category" && metadata.level=="3";
                    });
                    //alert(this.fetch_age);

                    this.fetch_age.sort(function(a,b){return a.order-b.order});
                    
                    //fectch ss entrant metadata
                    this.fetch_ss_age=this.results.Secondary.data.filter(function(metadata){
                        return metadata.reference=="Age Category" && metadata.level=="4";
                    });

                    this.fetch_ss_age.sort(function(a,b){return a.order-b.order});

                    //C3
                    //Bind metadata
                    this.fetch_year_by_age=this.results.Secondary.data.filter(function(metadata){
                        return metadata.reference=="Age Category" && metadata.level=="3";
                    });
                    
                    this.fetch_year_by_age.sort(function(a,b){return a.order-b.order});

                    //year by age for ss
                    this.fetch_ss_year_by_age=this.results.Secondary.data.filter(function(metadata){
                        return metadata.reference=="Age Category" && metadata.level=="4";
                    });
                    this.fetch_ss_year_by_age.sort(function(a,b){return a.order>b.order});

                    //C4
                    //Bind metadata
                    this.fetch_pupilflow=this.results.Secondary.data.filter(function(metadata){
                        return metadata.reference=="Enrolment Items 2016";
                    });
                    this.fetch_pupilflow.sort(function(a,b){return a.value<b.value});

                    //C5
                    //Bind metadata
                    this.fetch_special_needs=this.results.Secondary.data.filter(function(metadata){
                        return metadata.reference=="Pupil Challenges 2009";
                    });
                    this.fetch_special_needs.sort(function(a,b){return a.value>b.value});

                //still under the staff section
                //metadata
                this.fetch_teachingtype=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Teacher Appointment Type";
                });
                this.fetch_teachingtype.sort(function(a,b){return a.order>b.order});

                //Salary Source
                this.fetch_salarysource=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Salary Source";
                });
                this.fetch_salarysource.sort(function(a,b){return a.order>b.order});

                //Main Secondary Subject Taught
                this.fetch_specialisation=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Main Secondary Subject Taught";
                });
                this.fetch_specialisation.sort(function(a,b){return a.order>b.order});

                this.fetch_subjecttaught=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Main Secondary Subject Taught";
                });
                this.fetch_subjecttaught.sort(function(a,b){return a.order>b.order});

                //Present 2016
                this.fetch_present=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Present 2016";
                });
                this.fetch_present.sort(function(a,b){return a.order>b.order});

                //Staff Type
                this.fetch_stafftype=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Staff Type";
                });
                this.fetch_stafftype.sort(function(a,b){return a.order>b.order});

                //Academic Qualification 2009
                this.fetch_academicqualification=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Academic Qualification 2009";
                });
                this.fetch_academicqualification.sort(function(a,b){return a.order>b.order});

                //Teaching Qualifications
                this.fetch_teachingqualification=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Teaching Qualifications";
                });
                this.fetch_teachingqualification.sort(function(a,b){return a.order>b.order});


                //Staff Removal Category
                
                this.fetch_removalcategory=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Staff Removal Category";
                });
                this.fetch_removalcategory.sort(function(a,b){return a.order>b.order});

                //Bind metadata
                this.fetch_otherRooms=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Room Type 2016";
                });
                this.fetch_otherRooms.sort(function(a,b){return a.order>b.order});

                //present condition
                this.fetch_presentcondition=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Classroom Present Condition";
                });
                this.fetch_presentcondition.sort(function(a,b){return a.order>b.order});
                
                //floor
                this.fetch_floormaterial=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Floor Material";
                });
                this.fetch_floormaterial.sort(function(a,b){return a.order>b.order});
                
                //roof
                this.fetch_roofmaterial=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Roof Material";
                });
                this.fetch_roofmaterial.sort(function(a,b){return a.order>b.order});
                

                //wall
                this.fetch_wallmaterial=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Wall Material";
                });
                this.fetch_wallmaterial.sort(function(a,b){return a.order>b.order});
                
                //Drinking water
                //Bind metadata
                this.fetch_drinkingwater_source=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Water Supply Type";
                });
                this.fetch_drinkingwater_source.sort(function(a,b){return a.order>b.order});
                //facilities
                //Bind metadata
                this.fetch_facilities=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Useable Facility 2013";
                });
                this.fetch_facilities.sort(function(a,b){return a.order>b.order});


                //power source
                //Bind metadata
                this.fetch_power_source=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Power Facilities";
                });
                this.fetch_power_source.sort(function(a,b){return a.order>b.order});  

                //toilet type
                //Bind metadata
                this.fetch_toilet_type=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Toilet Type 2009";
                });
                this.fetch_toilet_type.sort(function(a,b){return a.order-b.order});

                //health facility
                this.fetch_healthfacility=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Available Health Facility 2009";
                });
                this.fetch_healthfacility.sort(function(a,b){return a.order>b.order});

                //seater
                this.fetch_seaters=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="School Class" && metadata.level=="3" ;
                });
                this.fetch_seaters=this.fetch_seaters.concat(this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="School Class" && metadata.level=="4" ;
                }));
                this.fetch_seaters.sort(function(a,b){return a.order>b.order});
                
                //fence
                this.fetch_fence=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Available Fence Wall 2016";
                });
                this.fetch_fence.sort(function(a,b){return a.order>b.order});

                //student by subject
                this.fetch_subjects=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Students By Subject Area JSS";
                });
                this.fetch_subjects.sort(function(a,b){return a.order>b.order});

                //ss
                this.fetch_subjectsSS=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Students By Subject Area SSS";
                });
                this.fetch_subjectsSS.sort(function(a,b){return a.order>b.order});
                

                //main subject
                this.fetch_mainsubjects=this.results.Secondary.data.filter(function(metadata){
                    return metadata.reference=="Main Secondary Subject Taught";
                });
                this.fetch_mainsubjects.sort(function(a,b){return a.order>b.order});

                        //set the spinner to stop loading
                        
                        $("div#loader").addClass("hide");
                        //set the spinner to stop loading             
                        //$("div#loader").addClass("hide");
                        //alert(this.results[1].message);
                });
                
                $("div#loader").addClass("hide");
                //setting the theme color
                var leftTab = document.querySelectorAll('.theme');
                for (let i = 0; i < leftTab.length; i++) {
                    leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
                } 
                axios.get("/api/search/state/schools",this.config).then(response => {

                    this.resultsSave = response.data;
                    //console.log(JSON.stringify(response.data, null, 2));

                    this.fetch_stateschools=response.data;

                });
                
                var previousButton = document.querySelectorAll('button#previous');
                previousButton.forEach(element => {
                    element.addEventListener('click', function () {
                    var tabP = document.querySelectorAll('div#content > div');
                    for (let index = 0; index < tabP.length; index++) {
                        if (tabP[index].className == 'show-current-tab'){
                        tabP[index].classList.remove('show-current-tab');
                        var newPrevI = index - 1;
                        tabP[newPrevI].classList.add('show-current-tab');
                        }             
                    }
                    })
                });  

                function forLoader(params) {                    
                    var clickedButton = document.querySelectorAll(params);
                    clickedButton.forEach(element => {                
                        element.addEventListener('click', function () {
                            var loader = document.getElementById('loader');
                            loader.classList.replace('hide','show');
                            console.log(loader);
                        });
                    });
                }

                forLoader('button#next');
                forLoader('button#save');

            }else{
                window.location.assign("/login");
            }
        },

        methods:{
            checkgrade:function(message,event){
                var regex = /^((\d|1[0-7])\/(\d|1[0-5]))$/gm;
                if(!regex.test(message)){
                    alert("Fix Grade level/step => ("+message+") to match [1-17]/[1-15]")
                }
                
            },
            popAlertMsg: function (message) {
                setTimeout(() => {
                    if (loader.classList.contains('show')){
                        loader.classList.replace('show', 'hide');
                    }
                }, 300);
                var alertBox = document.getElementById('alert');
                this.responseMsg = message;
                if (message != 'success'){
                    alertBox.style.backgroundColor = '#ff0000';
                }
                alertBox.classList.add('active');
                setTimeout(() => {
                    alertBox.classList.replace('active', null);
                    this.responseMsg = '';
                }, 1500);
            },
            moveNext: function () {
                // this.showLoader();
                var tab = document.querySelectorAll('div#content > div');            
                for (let index = 0; index < tab.length; index++) {
                    if (tab[index].className == 'show-current-tab'){
                        var tabContent = document.getElementById('content');
                        tabContent.classList.add('hide');
                        tab[index].classList.remove('show-current-tab');
                    // var loader = document.getElementById('loader');
                    // loader.classList.replace('hide', 'show');
                    setTimeout(() => {
                        tabContent.classList.replace('hide', null);
                        var newIndex = index + 1;
                        tab[newIndex].classList.add('show-current-tab');            
                    }, 500);
                    }        
                }
            },

            backHome:function(){
                var tab = document.querySelectorAll('div#content > div');            
                for (let index = 0; index < tab.length; index++) {
                    if (tab[index].className == 'show-current-tab'){
                        var tabContent = document.getElementById('content');
                        tabContent.classList.add('hide');
                        tab[index].classList.remove('show-current-tab');
                    // var loader = document.getElementById('loader');
                    // loader.classList.replace('hide', 'show');
                    setTimeout(() => {
                        tabContent.classList.replace('hide', null);
                        var newIndex = 0;
                        tab[newIndex].classList.add('show-current-tab');            
                    }, 500);
                    }        
                }
            },
            //tq sum
            tq_jss_male_tot: function(){
                this.tq_jss_male_total=0;
                this.tq_jss_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                    
                    return male_total + parseInt(item.jss_male); 
                },0);

                this.tq_male_total=0;
                this.tq_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                    
                    return male_total + parseInt(item.jss_male)+parseInt(item.sss_male); 
                    },0);
            },
            tq_jss_female_tot: function(){
                this.tq_jss_female_total=0;
                this.tq_jss_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                    
                    return female_total + parseInt(item.jss_female); 
                },0);
                this.tq_female_total=0;
                this.tq_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                    
                    return female_total + parseInt(item.jss_female)+parseInt(item.sss_female); 
                    },0);
            },
            tq_sss_male_tot: function(){
                this.tq_sss_male_total=0;
                this.tq_sss_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                    
                    return male_total + parseInt(item.sss_male); 
                },0);

                this.tq_male_total=0;
                this.tq_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                    
                    return male_total + parseInt(item.sss_male)+parseInt(item.jss_male); 
                    },0);
            },
            tq_sss_female_tot: function(){
                this.tq_sss_female_total=0;
                this.tq_sss_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                    
                    return female_total + parseInt(item.sss_female); 
                },0);

                this.tq_female_total=0;
                this.tq_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                    
                    return female_total + parseInt(item.sss_female)+parseInt(item.jss_female); 
                    },0);
            },
            entrantmale_total: function(){
                this.entrantmaletotal=0;
                this.entrantmaletotal=this.fetch_age.reduce(function(entrantmale_total, item){
                    return entrantmale_total + parseInt((item.male==''?0:item.male)); 
                },0);
            },
            entrantfemale_total: function(){
                this.entrantfemaletotal=0;
                this.entrantfemaletotal=this.fetch_age.reduce(function(entrantfemale_total, item){
                    
                    return entrantfemale_total + parseInt((item.female==''?0:item.female)); 
                },0);
            },
            jss1entrantmale_total: function(){
                this.jss1maletotal=0;
                this.jss1maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.jss1_male); 
                    },0);
            },
            jss1entrantfemale_total: function(){
                this.jss1femaletotal=0;
                this.jss1femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
            
                    return entrantfemale_total + parseInt(item.jss1_female); 
                    },0);
            },
            jss2entrantmale_total: function(){
                this.jss2maletotal=0;
                this.jss2maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
            
                    return entrantmale_total + parseInt(item.jss2_male); 
                    },0);
            },
            jss2entrantfemale_total: function(){
                this.jss2femaletotal=0;
                this.jss2femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
            
                    return entrantfemale_total + parseInt(item.jss2_female); 
                    },0);
            },
            jss3entrantmale_total: function(){
                this.jss3maletotal=0;
                this.jss3maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.jss3_male); 
                    },0);
            },
            jss3entrantfemale_total: function(){
                this.jss3femaletotal=0;
                this.jss3femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
            
                    return entrantfemale_total + parseInt(item.jss3_female); 
                    },0);
            },

            //ss
            ss1_entrantmale_total: function(){
                this.ss1_entrantmaletotal=0;
                this.ss1_entrantmaletotal=this.fetch_ss_age.reduce(function(entrantmale_total, item){
                    
                    return entrantmale_total + parseInt((item.male==''?0:item.male)); 
                  },0);
            },
            ss1_entrantfemale_total: function(){
                this.ss1_entrantfemaletotal=0;
                this.ss1_entrantfemaletotal=this.fetch_ss_age.reduce(function(entrantfemale_total, item){
                    
                    return entrantfemale_total + parseInt((item.female==''?0:item.female)); 
                  },0);
            },
            ss1entrantmale_total: function(){
                this.ss1maletotal=0;
                this.ss1maletotal=this.fetch_ss_year_by_age.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.sss1_male); 
                    },0);
            },
            ss1entrantfemale_total: function(){
                this.ss1femaletotal=0;
                this.ss1femaletotal=this.fetch_ss_year_by_age.reduce(function(entrantfemale_total, item){
            
                    return entrantfemale_total + parseInt(item.sss1_female); 
                    },0);
            },
            ss2entrantmale_total: function(){
                this.ss2maletotal=0;
                this.ss2maletotal=this.fetch_ss_year_by_age.reduce(function(entrantmale_total, item){
            
                    return entrantmale_total + parseInt(item.sss2_male); 
                    },0);
            },
            ss2entrantfemale_total: function(){
                this.ss2femaletotal=0;
                this.ss2femaletotal=this.fetch_ss_year_by_age.reduce(function(entrantfemale_total, item){
            
                    return entrantfemale_total + parseInt(item.sss2_female); 
                    },0);
            },
            ss3entrantmale_total: function(){
                this.ss3maletotal=0;
                this.ss3maletotal=this.fetch_ss_year_by_age.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.sss3_male); 
                    },0);
            },
            ss3entrantfemale_total: function(){
                this.ss3femaletotal=0;
                this.ss3femaletotal=this.fetch_ss_year_by_age.reduce(function(entrantfemale_total, item){
            
                    return entrantfemale_total + parseInt(item.sss3_female); 
                    },0);
            },
            //sum examination
            reg_jsce:function(){
                this.registered_total=0;
                this.registered_total=parseInt(this.registered_male)+parseInt(this.registered_female);
            },
            took_jsce:function(){
                this.took_part_total=0;
                this.took_part_total=parseInt(this.took_part_male)+parseInt(this.took_part_female);
            },
            passed_jsce:function(){
                this.passed_total=0;
                this.passed_total=parseInt(this.passed_male)+parseInt(this.passed_female);
            },
            reg_ssce:function(){
                this.registered_total=0;
                this.registered_ss_total=parseInt(this.registered_ss_male)+parseInt(this.registered_ss_female);
            },
            took_ssce:function(){
                this.took_part_total=0;
                this.took_part_ss_total=parseInt(this.took_part_ss_male)+parseInt(this.took_part_ss_female);
            },
            passed_ssce:function(){
                this.passed_total=0;
                this.passed_ss_total=parseInt(this.passed_ss_male)+parseInt(this.passed_ss_female);
            },
            //staff total
            non_teaching_staff_total_change:function(){
                this.non_teaching_staff_total=0;
                this.non_teaching_staff_total=parseInt(this.non_teaching_staff_male)+parseInt(this.non_teaching_staff_female);
            },
            teaching_staff_total_change:function(){
                this.teaching_staff_total=0;
                this.teaching_staff_total=parseInt(this.teaching_staff_male)+parseInt(this.teaching_staff_female);
            },
            //log out
            logout:function(){
                axios.post("/api/Section/User/Logout",
                {
                    email:this.user_email,
                },this.config).then(response => {
                    console.log(JSON.stringify(response.data, null, 2));
                    this.$cookies.remove("user");
                    window.location.assign("/login");
                })
            },

            setLevelBool:function(){
                if(this.level_of_education=="Junior Secondary"){
                    this.isjs=true;
                    this.isss=false;
                }else if(this.level_of_education=="Senior Secondary"){
                    this.isjs=false;
                    this.isss=true;
                }else if(this.level_of_education=="Junior and Senior Secondary"){
                    this.isss=true;
                    this.isjs=true;
                }
            },

            changecensusyear:function(){
                var previousButton = document.querySelectorAll('button#previous');
                previousButton.forEach(element => {
                    element.addEventListener('click', function () {
                    var tabP = document.querySelectorAll('div#content > div');
                    for (let index = 0; index < tabP.length; index++) {
                        if (tabP[index].className == 'show-current-tab'){
                        tabP[index].classList.remove('show-current-tab');
                        var newPrevI = index - 1;
                        tabP[newPrevI].classList.add('show-current-tab');
                        }             
                    }
                    })
                }); 
            },

            changeformtype:function(){
                
            },
            
            //save the sections
            saveCensusYear: function (event) {
                if (event.target.id === 'next') {                
                    this.popAlertMsg('Success');
                    this.moveNext();
                    console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('Saved');
                    console.log('save');
                }
            },

            validNumericOrDecimalOrNull:function(number){
                var re = /(^$)|(^[1-9]\d*(\.\d+)?$)/;
                return re.test(number);
            },

            validAlphabetOrNull:function(alphabet){
                var re = /(^$)|(^[A-Za-z]+$)/;
                return re.test(alphabet);
            },

            validNumericOrDecimal:function(number){
                var re = /(^[1-9]\d*(\.\d+)?$)/;
                return re.test(number);
            },

            validAlphabet:function(alphabet){
                var re = /(^[A-Za-z]+$)/;
                return re.test(alphabet);
            },

            validEmail:function(email){
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            },

            saveIdentification: function (event) {
                //use veevalidate later for this
                if(!this.validNumericOrDecimalOrNull(this.xcoordinate) || !this.validNumericOrDecimalOrNull(this.ycoordinate) 
                || !this.validNumericOrDecimalOrNull(this.zcoordinate) || this.validNumericOrDecimalOrNull(this.schoolname) 
                || this.validNumericOrDecimal(this.schooltown) || this.validNumericOrDecimal(this.schoolward) 
                || !this.validEmail(this.schoolemail)){

                    this.school_identification_errors=[];
                    //post errors
                    this.popAlertMsg('Check the form for the required data');
                    (!this.validNumericOrDecimalOrNull(this.xcoordinate))?this.school_identification_errors.push("x coordinate should be numeric decimal"):null;
                    (!this.validNumericOrDecimalOrNull(this.ycoordinate))?this.school_identification_errors.push("y coordinate should be numeric decimal"):null;
                    (!this.validNumericOrDecimalOrNull(this.zcoordinate))?this.school_identification_errors.push("z coordinate should be numeric decimal"):null;
                    (this.validNumericOrDecimalOrNull(this.schoolname))?this.school_identification_errors.push("School Name should be alphabets"):null;
                    (this.validNumericOrDecimal(this.schooltown))?this.school_identification_errors.push("School town should be alphabets"):null;
                    (!this.validEmail(this.schoolemail))?this.school_identification_errors.push("School email incorrect format"):null;
                    (this.validNumericOrDecimal(this.schoolward))?this.school_identification_errors.push("School ward should be alphabets"):null;
                }else{
                    this.school_identification_errors=[];
                    //send request
                    this.get_state_code = this.fetch_states[this.state_picked].name
                    
                    //alert(this.get_state_code);
                    this.get_lga_code = this.fetch_lga[this.lga_picked].name;
                    //alert(this.get_lga_code);
                    
                    //save containers
                    this.saveRegistrationContainer={
                        school_code: schoolcode.toLowerCase(),
                        register_year: this.registered_censusyear,
                        year:year
                    };
                   
                    this.saveIdentificationContainer={
                        school_code: schoolcode.toLowerCase(),
                        school_name: this.schoolname,
                        xcoordinate: this.xcoordinate,
                        ycoordinate: this.ycoordinate,
                        zcoordinate: this.zcoordinate,
                        school_address: this.schoolstreet,
                        town: this.schooltown,
                        ward: this.schoolward,
                        lga: this.get_lga_code,
                        state: this.get_state_code,
                        telephone: this.schooltelephone,
                        email: this.schoolemail
                    };

                    if (event.target.id === 'next') {                
                        this.popAlertMsg("Success");
                        this.moveNext();
                    } else if (event.target.id === 'save') {
                        this.popAlertMsg("Success");
                    } 
                }
            },

            getLGA: function () {
                var arrayOfObjects2 = [];

                //this.counter_state = this.state_index[this.state_picked];
                //alert(this.state_picked);

                for (var i = 0; i < this.results.States[this.state_picked].data.lgas.length; i++) {
                    var obj2 = {};
                    
                    obj2["name"] = this.results.States[this.state_picked].data.lgas[i].data.name;
                    obj2["code"] = this.results.States[this.state_picked].data.lgas[i].data.lgacode;
                    //this.lga_index[this.results[1].attributes.states[i].data.lgas[i].data.name] = i;

                    arrayOfObjects2.push(obj2);
                }
                //this.lga_picked=this.results[1].attributes.states[this.counter_state].data.lgas[0].data.name;
                this.fetch_lga = arrayOfObjects2;

                //console.log('now: ', JSON.stringify(this.fetch_states, null, " "));
                //alert(this.lga_picked);
            },

            saveCharacteristics: function (event) {
                //alert(this.no_of_inspection);
                //save container
                this.saveCharacteristicsContainer={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    year_of_establishment:this.year_of_establishment,
                    shared_facilities: this.facilities_choice,
                    schools_sharingwith: this.facilities_shared,
                    location_type: this.location_of_school,
                    school_type: this.type_of_school,
                    shift:this.shifts_choice,
                    level_of_education: this.level_of_education,
                    multi_grade_teaching: this.multigrade_choice,
                    distance_from_catchment_area: this.average_distance,
                    students_travelling_3km: this.student_distance,
                    isboarding:this.boarding_choice,
                    male_student_b: this.students_boarding_male,
                    female_student_b: this.students_boarding_female,
                    has_school_development_plan: this.sdp_choice,
                    has_sbmc: this.sbmc_choice,
                    has_pta: this.pta_choice,
                    last_inspection_date: this.date_inspection,
                    no_of_inspection:this.no_of_inspection,
                    last_inspection_authority: this.authority_choice,
                    conditional_cash_transfer: this.cash_transfer,
                    has_school_grant: this.grants_choice,
                    has_security_guard: this.guard_choice,
                    ownership: this.ownership_choice,
                };

                if (event.target.id === 'next') {                
                    this.popAlertMsg("Success");
                    this.moveNext();
                } else if (event.target.id === 'save') {
                    this.popAlertMsg("Success");
                } 
            },

            checkBirthCertificates:function(){
                alert("bc:"+this.fetch_birth_certificate[0].value+" ,male:"+this.fetch_birth_certificate[0].male);
            },

            saveBirthCertificates: function (event) {
                
                //save container
                this.saveJssBirthCertificatesContainer={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class: "jss",
                    birthcertificate:this.fetch_birth_certificate,
                };

                this.saveSssBirthCertificatesContainer={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class: "sss",
                    birthcertificate:this.fetch_birth_certificate,
                    };

                    if (event.target.id === 'next') {                
                        this.popAlertMsg("Success");
                        this.moveNext();
                    } else if (event.target.id === 'save') {
                        this.popAlertMsg("Success");
                    } 
            },

            saveEntrants: function (event) {
                this.saveJsEntrants={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class: "jss",
                    entrants: this.fetch_age,
                    
                };
                if (event.target.id === 'next') {                
                    this.popAlertMsg("Success");
                    this.moveNext();
                } else if (event.target.id === 'save') {
                    this.popAlertMsg("Success");
                } 
            },

            
            saveSssEntrants: function (event) {
               
                //alert(JSON.stringify(this.fetch_a))
                this.saveSsEntrants={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class: "sss",
                    entrants: this.fetch_ss_age,
                    
                }
                if (event.target.id === 'next') {                
                    this.popAlertMsg("Success");
                    this.moveNext();
                } else if (event.target.id === 'save') {
                    this.popAlertMsg("Success");
                } 
            },

            saveEnrollmentByAge: function (event) {
                this.saveJsEnrollmentByAge={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class: "jss",
                    stream_jss1_stream: this.jss1_stream,
                    stream_jss2_stream: this.jss2_stream,
                    stream_jss3_stream: this.jss3_stream,

                    stream_jss1_streamwithmultigrade:  this.jss1_stream_with_multigrade,
                    stream_jss2_streamwithmultigrade:  this.jss2_stream_with_multigrade,
                    stream_jss3_streamwithmultigrade:  this.jss3_stream_with_multigrade,

                    enrollments:this.fetch_year_by_age,
                    
                    repeater_jss1_male:  this.jss1_repeaters_male,
                    repeater_jss1_female:  this.jss1_repeaters_female,
                    repeater_jss2_male:  this.jss2_repeaters_male,
                    repeater_jss2_female:  this.jss2_repeaters_female,
                    repeater_jss3_male:  this.jss3_repeaters_male,
                    repeater_jss3_female:  this.jss3_repeaters_female,

                    prevyear_jss3_male:this.prevyear_jss3_male,
                    prevyear_jss3_female:this.prevyear_jss3_female
                };
                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
            },

            saveSssEnrollmentByAge: function (event) {
                this.saveSsEnrollmentByAge={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class: "sss",
                    stream_sss1_stream: this.ss1_stream,
                    stream_sss2_stream: this.ss2_stream,
                    stream_sss3_stream: this.ss3_stream,

                    stream_sss1_streamwithmultigrade:  this.ss1_stream_with_multigrade,
                    stream_sss2_streamwithmultigrade:  this.ss2_stream_with_multigrade,
                    stream_sss3_streamwithmultigrade:  this.ss3_stream_with_multigrade,

                    enrollments:this.fetch_ss_year_by_age,

                    repeater_sss1_male:  this.ss1_repeaters_male,
                    repeater_sss1_female:  this.ss1_repeaters_female,
                    repeater_sss2_male:  this.ss2_repeaters_male,
                    repeater_sss2_female:  this.ss2_repeaters_female,
                    repeater_sss3_male:  this.ss3_repeaters_male,
                    repeater_sss3_female:  this.ss3_repeaters_female,

                    prevyear_sss3_male:this.prevyear_sss3_male,
                    prevyear_sss3_female:this.prevyear_sss3_female
                    
                };
                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
            },
            
            saveSpecialNeed: function (event) {
                //console.log(JSON.stringify(this.fetch_all_special_needs, null, 2))
                this.saveJsSpecialNeed={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class: "jss",
                    special_needs:this.fetch_special_needs,
                };
                this.saveSsSpecialNeed={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class: "sss",
                    special_needs:this.fetch_special_needs,
                    
                };

                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
            },

            saveStudentsFlow: function (event) {
                this.saveJsStudentsFlow={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class: "jss",
                    pupil_flow:this.fetch_pupilflow,
                };

                this.saveSsStudentsFlow={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class: "sss",
                    pupil_flow:this.fetch_pupilflow,
                };

                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
            },

            saveExamination: function (event) {
                this.jsce_errors=[];
                if(this.registered_male >= this.took_part_male && this.took_part_male >= this.passed_male && this.registered_female >= this.took_part_female && this.took_part_female >= this.passed_female){
                    this.saveJsceExamination={
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        examination: "jsce",
                        examination_jsce_registeredmale: this.registered_male,
                        examination_jsce_registeredfemale: this.registered_female,
                        examination_jsce_tookpartmale: this.took_part_male,
                        examination_jsce_tookpartfemale: this.took_part_female,
                        examination_jsce_passedmale: this.passed_male,
                        examination_jsce_passedfemale: this.passed_female
                    };
                    if (event.target.id === 'next') {                
                        this.popAlertMsg('success');
                        this.moveNext();
                        // console.log('next');
                    } else if (event.target.id === 'save') {
                        this.popAlertMsg('success');
                        // console.log('save');
                    }
                }else{
                    this.jsce_errors.push("male and female registered is less than the took part or passed");
                    this.popAlertMsg("Correct it!");
                }
                
                
            },

            saveExaminationss: function (event) {
                this.ssce_errors=[];
                
                if(this.registered_ss_male >= this.took_part_ss_male && this.took_part_ss_male >= this.passed_ss_male && this.registered_ss_female >= this.took_part_ss_female && this.took_part_ss_female >= this.passed_ss_female){
                    this.saveSsceExamination={
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        examination: "ssce",
                        examination_ssce_registeredmale: this.registered_ss_male,
                        examination_ssce_registeredfemale: this.registered_ss_female,
                        examination_ssce_tookpartmale: this.took_part_ss_male,
                        examination_ssce_tookpartfemale: this.took_part_ss_female,
                        examination_ssce_passedmale: this.passed_ss_male,
                        examination_ssce_passedfemale: this.passed_ss_female
                    };
                    if (event.target.id === 'next') {                
                        this.popAlertMsg('success');
                        this.moveNext();
                        // console.log('next');
                    } else if (event.target.id === 'save') {
                        this.popAlertMsg('success');
                        // console.log('save');
                    }
                }else{
                    this.ssce_errors.push("male and female registered is less than the took part or passed");
                    this.popAlertMsg("Correct it!");
                }

                
            },

            //staff
            saveNoOfStaff_func: function (event) {
                this.saveNoOfStaff={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                    staff_nonteachersmale: this.non_teaching_staff_male,
                    staff_nonteachersfemale: this.non_teaching_staff_female,
                    staff_teachersmale: this.teaching_staff_male,
                    staff_teachersfemale: this.teaching_staff_female,
                    };

                    if (event.target.id === 'next') {                
                        this.popAlertMsg('success');
                        this.moveNext();
                        // console.log('next');
                    } else if (event.target.id === 'save') {
                        this.popAlertMsg('success');
                        // console.log('save');
                    }
            },

            staffInformation: function (event) {
                if (event.target.id === 'next') {                
                    this.popAlertMsg('Success');
                    this.moveNext();
                    console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('Saved');
                    console.log('save');
                }
            },
    
            submitStaff: function(){
                //add to the table
                //var thestaff={staff_file_no:this.staff_file_no,staff_name:this.staff_name,staff_gender:this.staff_gender,staff_type:this.staff_type,salary_source:this.salary_source,staff_yob:this.staff_yob,staff_yfa:this.staff_yfa,staff_ypa:this.staff_ypa,staff_yps:this.staff_yps,staff_level:this.staff_level,present:this.present,academic_qualification:this.academic_qualification,teaching_qualification:this.teaching_qualification,area_specialisation:this.area_specialisation,subject_taught:this.subject_taught,teaching_type:this.teaching_type,is_teaching_ss:this.is_teaching_ss,attended_training:this.attended_training};
                this.saveStaff={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
    
                    staff_file_no: this.staff_file_no,
                    staff_name:this.staff_name,
                    gender:this.staff_gender,
                    stafftype:this.staff_type,
                    salary_source:this.salary_source,
                    dob:this.staff_yob,
                    year_of_first_appointment:this.staff_yfa,
                    present_appointment_year:this.staff_ypa,
                    year_of_posting:this.staff_yps,
                    level:this.staff_level,
                    present:this.present,
                    academic_qualification:this.academic_qualification,
                    teaching_qualification:this.teaching_qualification,
                    specialisation:this.area_specialisation,
                    mainsubject_taught:this.subject_taught,
                    teaching_type:this.teaching_type,
                    is_teaching_ss:this.is_teaching_ss,
                    teacher_attended_training:this.attended_training,
                };

                this.staff_file_no='';
                this.staff_name='';
                this.staff_gender='';
                this.staff_type='';
                this.salary_source='';
                this.staff_yob='';
                this.staff_yfa='';
                this.staff_ypa='';
                this.staff_yps='';
                this.staff_level='';
                this.present='';
                this.academic_qualification='';
                this.teaching_qualification='';
                this.area_specialisation='';
                this.subject_taught=''
                this.teaching_type='';
                this.is_teaching_ss='';
                this.attended_training='';
            //if successfully posted add to the table
                this.fetch_staffs.push(this.saveStaff);

            },

            saveListOfStaff: function(){
                //add to the table
                //var thestaff={staff_file_no:this.staff_file_no,staff_name:this.staff_name,staff_gender:this.staff_gender,staff_type:this.staff_type,salary_source:this.salary_source,staff_yob:this.staff_yob,staff_yfa:this.staff_yfa,staff_ypa:this.staff_ypa,staff_yps:this.staff_yps,staff_level:this.staff_level,present:this.present,academic_qualification:this.academic_qualification,teaching_qualification:this.teaching_qualification,area_specialisation:this.area_specialisation,subject_taught:this.subject_taught,teaching_type:this.teaching_type,is_teaching_ss:this.is_teaching_ss,attended_training:this.attended_training};
                this.saveListOfStaffModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
    
                    stafflist:this.fetch_staffs,
                    
    
                };
                //does not next next action
    
            },

            submitClassroom: function(){

                this.saveClassroom={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
    
                    year_of_construction: this.year_of_construction,
                    present_condition:this.present_condition,
                    roof_material:this.roof_material,
                    length_in_meters:this.classroom_length,
                    width_in_meters:this.classroom_width,
                    floor_material:this.floor_material,
                    wall_material:this.wall_material,
                    seating:this.seating,
                    good_blackboard:this.blackboard,
                };

                this.year_of_construction='';
                this.present_condition='';
                this.classroom_length='';
                this.classroom_width='';
                this.roof_material='';
                this.floor_material='';
                this.wall_material='';
                this.seating='';
                this.blackboard='';
                this.fetch_classrooms.push(this.saveClassroom);
    
            },

            submitClassrooms: function(event){
                this.saveClassrooms={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    classrooms:this.fetch_classrooms,
                };
                if (event.target.id === 'next') {                
                    this.popAlertMsg('Success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('Success');
                    // console.log('save');
                }
            },
    
            removeClassroom:function(message,ind){
                //alert(this.fetch_staffs.indexOf(this.removedstaff_index));
                var temp={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    class_id:message,
                
                    };
                axios.post("/api/Section/Classroom/Remove",
                {
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    class_id:message,
                
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            this.fetch_classrooms.splice(ind,1);
                            
                            //alert(this.resultsSave.message);
                        } else {
                            alert(this.resultsSave.message);
                        }
                    });
                
            },

            saveNoOfClassrooms: function (event) {
                this.saveNoOfClassroomsModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                    no_of_classrooms: this.no_of_classrooms,
                    classes_held_outside: this.classes_held_outside
                    };
                if (event.target.id === 'next') {                
                    this.popAlertMsg('Success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('Success');
                    // console.log('save');
                }
            },
            
            saveNoOfOtherrooms: function (event) {
                this.saveNoOfOtherroomsModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                
                    no_of_staffrooms: this.fetch_otherRooms.find(function(room){return room.value=="Staff room"}).no_of_rooms,
                    no_of_offices: this.fetch_otherRooms.find(function(room){return room.value=="Office"}).no_of_rooms,
                    no_of_laboratories: this.fetch_otherRooms.find(function(room){return room.value=="Laboratories"}).no_of_rooms,
                    no_of_storerooms: this.fetch_otherRooms.find(function(room){return room.value=="Store room"}).no_of_rooms,
                    no_of_others: this.fetch_otherRooms.find(function(room){return room.value=="Others"}).no_of_rooms,
        
                    };

                    if (event.target.id === 'next') {                
                        this.popAlertMsg('success');
                        this.moveNext();
                        // console.log('next');
                    } else if (event.target.id === 'save') {
                        this.popAlertMsg('success');
                        // console.log('save');
                    }
            },
    
            saveDWSources: function (event) {
                this.saveDWSourcesModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                    drinking_water_sources: this.sources_of_drinking_water,
                    };
                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
            },

            saveFacilitiesAvailable: function (event) {
                this.saveFacilitiesAvailableModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                
                    facilities_available:this.fetch_facilities,
                    
                    };
                    if (event.target.id === 'next') {                
                        this.popAlertMsg('success');
                        this.moveNext();
                        // console.log('next');
                    } else if (event.target.id === 'save') {
                        this.popAlertMsg('success');
                        // console.log('save');
                    }
            },
    
            saveToilet: function (event) {
                //alert(this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).male_students);
                this.saveToiletModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                    toilets:this.fetch_toilet_type
                    };
                
                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
            },
    
            saveSharedFacilities: function (event) {
                this.saveSharedFacilitiesModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                    shared_facilities: this.shared_facilities,
                    };
                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
            },
    
            //source of power
            savePowerSource: function (event) {
                this.savePowerSourceModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                    power_source: this.sources_of_power,
                    };
                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
            },
    
            //source of power
            saveStudentbySubject: function (event) {
                this.saveStudentbySubjectModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                    students_by_subject: this.fetch_subjects
                    };

                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
                
            },

            saveStudentbySubjectSS: function (event) {
                this.saveStudentbySubjectSSModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'sss',
                    students_by_subject: this.fetch_subjectsSS
                    };
                    if (event.target.id === 'next') {                
                        this.popAlertMsg('success');
                        this.moveNext();
                        // console.log('next');
                    } else if (event.target.id === 'save') {
                        this.popAlertMsg('success');
                        // console.log('save');
                    }
            },

            showRemoveDialog: function (staffindex) {
                this.removedstaff_index=staffindex;
                $("div#clearStaff").addClass("active");
            
            },

            removeStaff:function(){
                //alert(this.fetch_staffs.indexOf(this.removedstaff_index));
                axios.post("/api/Section/Staff/Remove",
                {
                    school_code: schoolcode.toLowerCase(),
                    transfer_school_code: '',
                    year: year,
                    staff_id:this.fetch_staffs[this.removedstaff_index].id,
                    removalcategory: this.removalcat,
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            this.fetch_staffs.splice(this.removedstaff_index,1);
                            this.stateschool_picked='';
                            this.removalcat='';
                            //alert(this.resultsSave.message);
                        } else {
                            alert(this.resultsSave.message);
                        }
                    });
                
            },

            changedwsource: function(message,event){
            if(event.target.checked==false){
                axios.post("/api/Section/Remove/DrinkingSource",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        source:message
                        
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //alert("Data Saved to DB");
                        } else {
                            alert("Check the form for the required data");
                        }
                    });
            }
            
            },

            changesharedfacilities: function(message,event){
                if(event.target.checked==false){
                    axios.post("/api/Section/Remove/SharedFacility",
                        {
                            school_code: schoolcode.toLowerCase(),
                            year: year,
                            shared_facility:message
                            
                        },this.config).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert("Data Saved to DB");
                            } else {
                                alert(this.resultsSave.status);
                            }
                        });
                }
                
            },

            changesourceofpower: function(message,event){
                if(event.target.checked==false){
                    axios.post("/api/Section/Remove/PowerSource",
                        {
                            school_code: schoolcode.toLowerCase(),
                            year: year,
                            power_source:message
                            
                        },this.config).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert("Data Saved to DB");
                            } else {
                                alert(this.resultsSave.message);
                            }
                        });
                }
                
            },

            changelearningmaterials: function(message,event){
                if(event.target.checked==false){
                    axios.post("/api/Section/Remove/LearningMaterial",
                        {
                            school_code: schoolcode.toLowerCase(),
                            year: year,
                            material:message
                            
                        },this.config).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert("Data Saved to DB");
                            } else {
                                alert(this.resultsSave.message);
                            }
                        });
                }
                
            },

            changeplayfacilities: function(message,event){
                if(event.target.checked==false){
                    axios.post("/api/Section/Remove/PlayFacility",
                        {
                            school_code: schoolcode.toLowerCase(),
                            year: year,
                            facility:message
                            
                        },this.config).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert("Data Saved to DB");
                            } else {
                                alert("Check the form for the required data");
                            }
                        });
                }
                
            },
        
            saveHealthFence: function (event) {
                this.saveHealth={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                
                    health_facility: this.health_facility,
        
                    };
        
                this.saveFence={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                
                    fence_facility: this.fence_facility,
        
                    };
            
                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
        
            },
        
            saveStudents:function(event){
                this.saveStudentsModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                
                    students_book: this.fetch_mainsubjects,
        
                    };
                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
                
            },

            saveStudentsSS:function(event){
                this.saveStudentsSSModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'sss',
                    students_book: this.fetch_mainsubjects,
                    };
                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
            },
        
            saveTeachers:function(event){
                this.saveTeachersModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                    teachers_book: this.fetch_mainsubjects,
        
                    };

                    if (event.target.id === 'next') {                
                        this.popAlertMsg('success');
                        this.moveNext();
                        // console.log('next');
                    } else if (event.target.id === 'save') {
                        this.popAlertMsg('success');
                        // console.log('save');
                    }
            },

            saveTeachersSS:function(event){
                this.saveTeachersSSModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'sss',
                    teachers_book: this.fetch_mainsubjects
                    };
                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
            },

            saveUndertaking:function(event){
                this.saveUndertakingModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    attestation_headteacher_name: this.attestation_headteacher_name,
                    attestation_headteacher_telephone: this.attestation_headteacher_telephone,
                    attestation_headteacher_signdate: this.attestation_headteacher_signdate,
                    attestation_enumerator_name: this.attestation_enumerator_name,
                    attestation_enumerator_position: this.attestation_enumerator_position,
                    attestation_enumerator_telephone: this.attestation_enumerator_telephone,
                    attestation_supervisor_name: this.attestation_supervisor_name,
                    attestation_supervisor_position: this.attestation_supervisor_position,
                    attestation_supervisor_telephone: this.attestation_supervisor_telephone,
                    };
                    if (event.target.id === 'next') {                
                        this.popAlertMsg('success');
                        this.moveNext();
                        // console.log('next');
                    } else if (event.target.id === 'save') {
                        this.popAlertMsg('success');
                        // console.log('save');
                    }
            },

            saveSeaters:function(event){
                this.saveSeatersModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class:'jss',
                    seaters: this.fetch_seaters
                    };
                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
            },
            clearForm:function(){
                this.saveRegistrationContainer={}
                this.saveIdentificationContainer={}
                this.saveCharacteristicsContainer={}
                this.saveJssBirthCertificatesContainer={}
                this.saveSssBirthCertificatesContainer={}
                this.saveJsEntrants={}
                this.saveSsEntrants={}
                this.saveJsEnrollmentByAge={}
                this.saveSsEnrollmentByAge={}
                this.saveJsSpecialNeed={}
                this.saveSsSpecialNeed={}
                this.saveJsStudentsFlow={}
                this.saveSsStudentsFlow={}
                this.saveJsceExamination={}
                this.saveSsceExamination={}
                this.saveNoOfStaff={}
                this.saveStaff={}
                this.saveListOfStaffModel={}
                this.saveClassroom={}
                this.saveClassrooms={}
                this.saveNoOfClassroomsModel={}
                this.saveNoOfOtherroomsModel={}
                this.saveDWSourcesModel={}
                this.saveFacilitiesAvailableModel={}
                this.saveToiletModel={}
                this.saveSharedFacilitiesModel={}
                this.savePowerSourceModel={}
                this.saveStudentbySubjectModel={}
                this.saveStudentbySubjectSSModel={}
                this.saveHealth={}
                this.saveFence={}
                this.saveStudentsModel={}
                this.saveStudentsSSModel={}
                this.saveTeachersModel={}
                this.saveTeachersSSModel={}
                this.saveUndertakingModel={}
                this.saveSeatersModel={}
                this.saveTeacherQualificationModel={}

                //clear
            },
            exportFormData:function(event){
                var formDataJson={
                    'saveRegistrationContainer':this.saveRegistrationContainer,
                    'saveIdentificationContainer':this.saveIdentificationContainer,
                    'saveCharacteristicsContainer':this.saveCharacteristicsContainer,
                    'saveJssBirthCertificatesContainer':this.saveJssBirthCertificatesContainer,
                    'saveSssBirthCertificatesContainer':this.saveSssBirthCertificatesContainer,
                    'saveJsEntrants':this.saveJsEntrants,
                    'saveSsEntrants':this.saveSsEntrants,
                    'saveJsEnrollmentByAge':this.saveJsEnrollmentByAge,
                    'saveSsEnrollmentByAge':this.saveSsEnrollmentByAge,
                    'saveJsSpecialNeed':this.saveJsSpecialNeed,
                    'saveSsSpecialNeed':this.saveSsSpecialNeed,
                    'saveJsStudentsFlow':this.saveJsStudentsFlow,
                    'saveSsStudentsFlow':this.saveSsStudentsFlow,
                    'saveJsceExamination':this.saveJsceExamination,
                    'saveSsceExamination':this.saveSsceExamination,
                    'saveNoOfStaff':this.saveNoOfStaff,
                    'saveStaff':this.saveStaff,
                    'saveListOfStaffModel':this.saveListOfStaffModel,
                    'saveClassroom':this.saveClassroom,
                    'saveClassrooms':this.saveClassrooms,
                    'saveNoOfClassroomsModel':this.saveNoOfClassroomsModel,
                    'saveNoOfOtherroomsModel':this.saveNoOfOtherroomsModel,
                    'saveDWSourcesModel':this.saveDWSourcesModel,
                    'saveFacilitiesAvailableModel':this.saveFacilitiesAvailableModel,
                    'saveToiletModel':this.saveToiletModel,
                    'saveSharedFacilitiesModel':this.saveSharedFacilitiesModel,
                    'savePowerSourceModel':this.savePowerSourceModel,
                    'saveStudentbySubjectModel':this.saveStudentbySubjectModel,
                    'saveStudentbySubjectSSModel':this.saveStudentbySubjectSSModel,
                    'saveHealth':this.saveHealth,
                    'saveFence':this.saveFence,
                    'saveStudentsModel':this.saveStudentsModel,
                    'saveStudentsSSModel':this.saveStudentsSSModel,
                    'saveTeachersModel':this.saveTeachersModel,
                    'saveTeachersSSModel':this.saveTeachersSSModel,
                    'saveSeatersModel':this.saveSeatersModel,
                    'saveTeacherQualificationModel':this.saveTeacherQualificationModel,
                    'saveUndertakingModel':this.saveUndertakingModel
                };
                console.log(formDataJson);
                this.downloadObjectAsJson(formDataJson,this.get_state_code+"_public_secondary")
            },
            downloadObjectAsJson:function(exportObj, exportName){
                var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj));
                var downloadAnchorNode = document.createElement('a');
                downloadAnchorNode.setAttribute("href",     dataStr);
                downloadAnchorNode.setAttribute("download", exportName + ".json");
                document.body.appendChild(downloadAnchorNode); // required for firefox
                downloadAnchorNode.click();
                downloadAnchorNode.remove();
              },
            saveTeacherQualification:function(event){
                this.saveTeacherQualificationModel={
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school:'sss',
                    teacher_qualification: this.fetch_teachingqualification,
                    };
                if (event.target.id === 'next') {                
                    this.popAlertMsg('success');
                    this.moveNext();
                    // console.log('next');
                } else if (event.target.id === 'save') {
                    this.popAlertMsg('success');
                    // console.log('save');
                }
            },
        },
    });