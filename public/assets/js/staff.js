/* For Add New Staff */
var currentTabView = 0;
showTab(currentTabView);
function showTab(q){
    var tabs = document.getElementsByClassName("category");
    tabs[q].style.display = "block";

    if (q == 0){
        document.getElementById("Previous").style.display = "none";
    } else{
        document.getElementById("Previous").style.display = "block";
    }

    if (q == (tabs.length - 1)){
        document.getElementById("Next").innerHTML = "Add New Staff";
    } else{
        document.getElementById("Next").innerHTML = "Next";
    }
    //Fixing the Indicator
    stepIndicators(q)
}

function NextPrev(q){
    //alert('Works');
    var tabs = document.getElementsByClassName("category");
    tabs[currentTabView].style.display = "none";
    currentTabView = currentTabView + q;

    if (currentTabView >= tabs.length){
        document.getElementById("NewstaffForm").submit();
        return false;
    }
    showTab(currentTabView);
}

function stepIndicators(q){
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++){
        x[i].className = x[i].className.replace(" active", "");
    }
    x[q].className+= " active";
}

