
var url = window.location.href;//get the school code and year by splitting the url
var res = url.split("/");
var schoolcode = res[4];
var year = res[6];

//vue vm for the form

var app = new Vue({
    el: '#app',
    data: {
        results: [],
        resultsSave: [],
        //school identification
        schoolcode: '',
        schoolname: '',
        xcoordinate:'',
        ycoordinate:'',
        zcoordinate:'',
        schoolstreet: '',
        schoolemail: '',
        schoollga: '',
        schooltown: '',
        schooltelephone: '',
        schoolward: '',
        schoolstate: '',
        

        fetch_states: [],
        state_picked: '',
        state_index: [],
        get_state_code: '',

        results_lga: [],
        fetch_lga: [],
        lga_picked: '',
        lga_index: [],
        get_lga_code: '',

        counter_state: 0,
        counter_lga: 0,

        location_type_ref: [],
        location_type_val: [],
        metadata: [],

        //metadata arrays
        fetch_location: [],
        fetch_levels: [],
        fetch_category: [],
        fetch_authority: [],
        fetch_ownership: [],
        fetch_birth_certificate: [],
        fetch_b_c: [],
        fetch_b_c_no: [],
        fetch_age: [],
        fetch_a: [],
        fetch_a_no: [],
        fetch_streams: [],
        fetch_year_by_age: [],
        fetch_year_by_age_no: [],
        fetch_y_b_a1: [], fetch_y_b_a2: [], fetch_y_b_a3: [],
        fetch_y_b_no1: [], fetch_y_b_no2: [], fetch_y_b_no3: [],
        fetch_all_ages: [],
        fetch_repeaters: [], fetch_r:[], 
        fetch_pupilflow: [],
        fetch_dropouts: [], fetch_drop:[],
        fetch_transfer_in: [], fetch_t_in:[],
        fetch_transfer_out: [], fetch_t_out:[],
        fetch_promoted: [], fetch_pro:[],
        fetch_examination:[],
        fetch_all_special_needs: [], fetch_special_needs: [], fetch_special_needs_item: [],
        fetch_s_n1: [], fetch_s_n2: [], fetch_s_n3: [],
        fetch_s_n_no1: [], fetch_s_n_no2: [], fetch_s_n_no3: [],
        fetch_a_s_n: [], fetch_a_s_n_no: [],

        inputParams: [],

        exam_type:'',
        registered_male:'',
        registered_female:'',
        registered_total:'', 
        took_part_male:'',
        took_part_female:'',
        took_part_total:'',
        passed_male:'',
        passed_female:'',
        passed_total:'',

        //school characteristics
        year: '',
        year_of_establishment: '',
        location_of_school: '',
        level_of_education: '',
        type_of_school: '',
        shifts_choice: '',
        facilities_choice: '',
        facilities_shared: '',
        multigrade_choice: '',
        multigrade_choice_bi: '',
        average_distance: '',
        student_distance: '',
        students_boarding_female: '',
        students_boarding_male: '',
        sdp_choice: '',
        sdp_choice_bi: '',
        sbmc_choice: '',
        sbmc_choice_bi: '',
        pta_choice: '',
        pta_choice_bi: '',
        date_inspection: '',
        no_of_inspection: '',
        authority_choice: '',
        cash_transfer: '',
        grants_choice: '',
        grants_choice_bi: '',
        guard_choice: '',
        guard_choice_bi: '',
        ownership_choice: '',

        //C4
        jss1_stream:'',
        jss2_stream:'',
        jss3_stream:'',
        jss1_stream_with_multigrade:'',
        jss2_stream_with_multigrade:'',
        jss3_stream_with_multigrade:'',
        jss1_repeaters_male: '',
        jss1_repeaters_female:'',
        jss2_repeaters_male:'',
        jss2_repeaters_female:'',
        jss3_repeaters_male:'',
        jss3_repeaters_female:'',
        entrantmaletotal:'',
        entrantfemaletotal:'',
        jss1maletotal:'',
        jss1femaletotal:'',
        jss2maletotal:'',
        jss2femaletotal:'',
        jss3maletotal:'',
        jss3femaletotal:'',
        prevyear_jss3_male:'',
        prevyear_jss3_female:'',
        //breadcrumbs href
        schoolstate_href:'',
        lga_href:'',
        level_of_education_href:'',
        school_type_href:'',

        //staff
        non_teaching_staff_male:'',
        non_teaching_staff_female:'',
        non_teaching_staff_total:'',
        teaching_staff_male:'',
        teaching_staff_female:'',
        teaching_staff_total:'',
        staff_id:'',
        staff_file_no:'',
        staff_name:'',
        staff_gender:'',
        staff_type:'',
        salary_source:'',
        staff_yob:'',
        staff_yfa:'',
        staff_ypa:'',
        staff_yps:'',
        staff_level:'',
        present:'',
        academic_qualification:'',
        teaching_qualification:'',
        area_specialisation:'',
        subject_taught:'',
        teaching_type:'',
        is_teaching_ss:'',
        attended_training:'',

        //fetch array for staff features
        fetch_teachingtype:[],
        fetch_subjecttaught:[],
        fetch_specialisation:[],
        fetch_academicqualification:[],
        fetch_teachingqualification:[],
        fetch_present:[],
        fetch_salarysource:[],
        fetch_stafftype:[],
        //fetch array for the staffs
        fetch_staffs:[],

        //Classrooms
        no_of_classrooms:'',
        classes_held_outside:'',
        fetch_otherRooms:[],
        fetch_drinkingwater_source:[],
        fetch_power_source:[],
        fetch_toilet_type:[],
        fetch_facilities:[],
        shared_facilities:[],
        

        //facilities
        sources_of_drinking_water:[],
        sources_of_power:[],
        fetch_healthfacility:[],
        health_facilities:[],
        fence_facilities:[],
        fetch_fence:[],
        fetch_mainsubjects:[],
        fetch_subjects:[],
    },

    computed:{
        entrant_male_total: function(){
            
            return this.fetch_age.reduce(function(entrant_male_total, item){
                
              return entrant_male_total + item.male; 
            },0);
          },
          entrant_female_total: function(){
            
            return this.fetch_age.reduce(function(entrant_female_total, item){
                
              return entrant_female_total + item.female; 
            },0);
          }
    },

    watch:{
        
    },

    mounted: function(){
        //setting the theme color
        var leftTab = document.querySelectorAll('.theme');
        for (let i = 0; i < leftTab.length; i++) {
            leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
        } 
        this.schoolcode=schoolcode;
        axios.get("/api/school/" + schoolcode.toLowerCase() + "/year/" + year).then(response => {
            this.results = response.data;//returns the json data

            this.results_lga = response.data;
            //this.results =  JSON.parse(response.data);
            //console.log(this.results);
            console.log('r: ', JSON.stringify(response.data, null, 2));
            
            //bind the data to the inputs
            //School Identification
            this.schoolname=this.results[0].data.school_identification["school_name"];
            this.schoolstreet=this.results[0].data.school_identification["address"];
            this.xcoordinate=this.results[0].data.school_identification["xcoordinate"];
            this.ycoordinate=this.results[0].data.school_identification["ycoordinate"];
            this.zcoordinate=this.results[0].data.school_identification["zcoordinate"];
            this.schoolemail=this.results[0].data.school_identification["email_address"];
            this.schoollga=this.results[0].data.school_identification["lga"];
            this.schooltown=this.results[0].data.school_identification["town"];
            this.schooltelephone=this.results[0].data.school_identification["school_telephone"];
            this.schoolward=this.results[0].data.school_identification["ward"];
            this.schoolstate=this.results[0].data.school_identification["state"];

            //School Characteristics
            //bind the metadata
            this.fetch_location=this.results[1].data.data.filter(function(metadata){
                    return metadata.Reference=="Location Type";
            });
            //alert(this.fetch_location);
            this.fetch_levels=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="School Level";
            });
            this.fetch_category=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="School Category";
            });
            this.fetch_authority=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Ownership - 2013";
            });
            this.fetch_ownership=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Ownership - 2013";
            });

            //bind the data
            this.year_of_establishment=this.results[0].data.school_characteristics["year_of_establishment"];
            this.location_of_school=this.results[0].data.school_characteristics["location"];
            this.level_of_education=this.results[0].data.school_characteristics["levels_of_education_offered"];
            this.type_of_school=this.results[0].data.school_characteristics["type_of_school"];
            this.shifts_choice=this.results[0].data.school_characteristics["shifts"];
            this.facilities_choice=this.results[0].data.school_characteristics["shared_facilities"];
            this.facilities_shared=this.results[0].data.school_characteristics["sharing_with"];
            this.multigrade_choice=this.results[0].data.school_characteristics["multi_grade_teaching"];    
            this.average_distance=this.results[0].data.school_characteristics["school_average_distance_from_catchment_communities"];
            this.student_distance=this.results[0].data.school_characteristics["students_distance_from_school"];
            this.students_boarding_female=this.results[0].data.school_characteristics["students_boarding"].female;
            this.students_boarding_male=this.results[0].data.school_characteristics["students_boarding"].male;
            this.sdp_choice=this.results[0].data.school_characteristics["school_development_plan_sdp"];
            this.sbmc_choice=this.results[0].data.school_characteristics["school_based_management_committee_sbmc"];    
            this.pta_choice=this.results[0].data.school_characteristics["parents_teachers_association_pta"];  
            this.date_inspection=this.results[0].data.school_characteristics["date_of_last_inspection_visit"];
            this.no_of_inspection=this.results[0].data.school_characteristics["no_of_inspection"];//no of last inspection
            this.authority_choice=this.results[0].data.school_characteristics["authority_of_last_inspection"];
            this.cash_transfer=this.results[0].data.school_characteristics["conditional_cash_transfer"];
            this.grants_choice=this.results[0].data.school_characteristics["school_grants"];            
            this.guard_choice=this.results[0].data.school_characteristics["security_guard"];            
            this.ownership_choice=this.results[0].data.school_characteristics["ownership"];
            
            //bind the breadcrumbs href
            this.schoolstate_href='/generic/search?state='+encodeURI(this.schoolstate);
            this.lga_href='/generic/search?lga='+encodeURI(this.schoollga);
            this.school_type_href='/generic/search?state='+encodeURI(this.schoolstate)+'&'+'lga='+encodeURI(this.schoollga)+'&'+'schooltype='+encodeURI('public');
            this.level_of_education_href='/generic/search?state='+encodeURI(this.schoolstate)+'&'+'lga='+encodeURI(this.schoollga)+'&'+'schooltype='+encodeURI('public')+'&'+'schoollevel='+encodeURI(this.level_of_education);

            //Apply the metadata to the form sections
            //bind the states list
            var arrayOfObjects = []

            for (var i = 0; i < this.results[1].attributes.states.length; i++) {//loop the states
                var obj = {};
                obj["name"] = this.results[1].attributes.states[i].data.name;
                obj["code"] = this.results[1].attributes.states[i].data.statecode;
               
                //save the state name as a key and the value is the index
                if(obj["name"]==this.schoolstate){
                    this.state_picked=i;
                }
                arrayOfObjects.push(obj);
            }
            //this.state_picked=this.results[1].attributes.states[0].data.name;

            this.fetch_states = arrayOfObjects;
           // console.log('r: ', JSON.stringify(this.fetch_states, null, 2));
            var arrayOfObjects2 = []
            var tempIndex=0;

            ////LGA
            for (var i = 0; i < this.results[1].attributes.states.length; i++) {
                
                for (var j = 0; j < this.results[1].attributes.states[i].data.lgas.length; j++) {
                    var obj2 = {};
                    obj2["name"] = this.results[1].attributes.states[i].data.lgas[j].data.name;
                    obj2["code"] = this.results[1].attributes.states[i].data.lgas[j].data.lgacode;

                    arrayOfObjects2.push(obj2);
                    if(obj2["name"]==this.schoollga){
                        this.lga_picked=tempIndex;
                        
                    }
                    tempIndex++;
                }
            }
            // this.lga_picked = this.results[1].attributes.states[0].data.lgas[0].data.name;
            //alert(this.lga_picked);   
            this.fetch_lga = arrayOfObjects2;

            //SECTION C ENROLLMENT
            //C1
            //Bind metadata
            this.fetch_birth_certificate=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Birth Certificate Type";
            });
            this.fetch_birth_certificate.sort(function(a,b){return a.order-b.order});

            //find functions
            function checkNpc(bc) {
                return bc.value == "National Population Commission";
            }
            function checkHospital(bc) {
                return bc.value == "Hospital";
            }
            function checkLga(bc) {
                return bc.value == "LGA";
            }
            function checkCourt(bc) {
                return bc.value == "Court";
            }
            function checkUn(bc) {
                return bc.value == "UN";
            }
            function checkOthers(bc) {
                return bc.value == "Others";
            }

            //Bind data
            this.fetch_birth_certificate.find(checkNpc).male=(this.results[0].data.enrollment.birth_certificates.data.find(checkNpc)!=null)?this.results[0].data.enrollment.birth_certificates.data.find(checkNpc).male:"";
            this.fetch_birth_certificate.find(checkNpc).female=(this.results[0].data.enrollment.birth_certificates.data.find(checkNpc)!=null)?this.results[0].data.enrollment.birth_certificates.data.find(checkNpc).female:"";
            this.fetch_birth_certificate.find(checkHospital).male=(this.results[0].data.enrollment.birth_certificates.data.find(checkHospital)!=null)?this.results[0].data.enrollment.birth_certificates.data.find(checkHospital).male:"";
            this.fetch_birth_certificate.find(checkHospital).female=(this.results[0].data.enrollment.birth_certificates.data.find(checkHospital)!=null)?this.results[0].data.enrollment.birth_certificates.data.find(checkHospital).female:"";
            this.fetch_birth_certificate.find(checkLga).male=(this.results[0].data.enrollment.birth_certificates.data.find(checkLga)!=null)?this.results[0].data.enrollment.birth_certificates.data.find(checkLga).male:"";
            this.fetch_birth_certificate.find(checkLga).female=(this.results[0].data.enrollment.birth_certificates.data.find(checkLga)!=null)?this.results[0].data.enrollment.birth_certificates.data.find(checkLga).female:"";
            this.fetch_birth_certificate.find(checkCourt).male=(this.results[0].data.enrollment.birth_certificates.data.find(checkCourt)!=null)?this.results[0].data.enrollment.birth_certificates.data.find(checkCourt).male:"";
            this.fetch_birth_certificate.find(checkCourt).female=(this.results[0].data.enrollment.birth_certificates.data.find(checkCourt)!=null)?this.results[0].data.enrollment.birth_certificates.data.find(checkCourt).female:"";
            this.fetch_birth_certificate.find(checkUn).male=(this.results[0].data.enrollment.birth_certificates.data.find(checkUn)!=null)?this.results[0].data.enrollment.birth_certificates.data.find(checkUn).male:"";
            this.fetch_birth_certificate.find(checkUn).female=(this.results[0].data.enrollment.birth_certificates.data.find(checkUn)!=null)?this.results[0].data.enrollment.birth_certificates.data.find(checkUn).female:"";
            this.fetch_birth_certificate.find(checkOthers).male=(this.results[0].data.enrollment.birth_certificates.data.find(checkOthers)!=null)?this.results[0].data.enrollment.birth_certificates.data.find(checkOthers).male:"";
            this.fetch_birth_certificate.find(checkOthers).female=(this.results[0].data.enrollment.birth_certificates.data.find(checkOthers)!=null)?this.results[0].data.enrollment.birth_certificates.data.find(checkOthers).female:"";

            //C2
            //Bind metadata
            this.fetch_age=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Age Category";
            });
            this.fetch_age.sort(function(a,b){return a.order>b.order});

            //Bind data
            this.fetch_age.find(function(age){return age.value=="12"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="12"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="12"}).male:"";
            this.fetch_age.find(function(age){return age.value=="12"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="12"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="12"}).female:"";
            this.fetch_age.find(function(age){return age.value=="13"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="13"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="13"}).male:"";
            this.fetch_age.find(function(age){return age.value=="13"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="13"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="13"}).female:"";
            this.fetch_age.find(function(age){return age.value=="Above 14 Years"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 14 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 14 Years"}).male:"";
            this.fetch_age.find(function(age){return age.value=="Above 14 Years"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 14 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 14 Years"}).female:"";
            this.fetch_age.find(function(age){return age.value=="14"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="14"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="14"}).male:"";
            this.fetch_age.find(function(age){return age.value=="14"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="14"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="14"}).female:"";
            this.fetch_age.find(function(age){return age.value=="Below 12 Years"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Below 12 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Below 12 Years"}).male:"";
            this.fetch_age.find(function(age){return age.value=="Below 12 Years"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Below 12 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Below 12 Years"}).female:"";

            //C3
            //Bind metadata
            this.fetch_year_by_age=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Age Category";
            });
            this.fetch_year_by_age.sort(function(a,b){return a.order>b.order});

            //Bind data
            this.fetch_streams=this.results[0].data.enrollment.year_by_age.data.stream;//need to fix this
            this.jss1_stream=(this.fetch_streams[0])?this.fetch_streams[0].stream:"";
            this.jss2_stream=(this.fetch_streams[1])?this.fetch_streams[1].stream:"";
            this.jss3_stream=(this.fetch_streams[2])?this.fetch_streams[2].stream:"";
            this.jss1_stream_with_multigrade=(this.fetch_streams[0])?this.fetch_streams[0].stream_with_multigrade:"";
            this.jss2_stream_with_multigrade=(this.fetch_streams[1]!=null)?this.fetch_streams[1].stream_with_multigrade:"";
            this.jss3_stream_with_multigrade=(this.fetch_streams[2]!=null)?this.fetch_streams[2].stream_with_multigrade:"";

            this.fetch_repeaters=this.results[0].data.enrollment.year_by_age.data.repeater;
            this.jss1_repeaters_male=(this.fetch_repeaters[0])?this.fetch_repeaters[0].male:"";
            this.jss1_repeaters_female=(this.fetch_repeaters[0])?this.fetch_repeaters[0].female:"";
            this.jss2_repeaters_male=(this.fetch_repeaters[1]!=null)?this.fetch_repeaters[1].male:"";
            this.jss2_repeaters_female=(this.fetch_repeaters[1]!=null)?this.fetch_repeaters[1].female:"";
            this.jss3_repeaters_male=(this.fetch_repeaters[2]!=null)?this.fetch_repeaters[2].male:"";
            this.jss3_repeaters_female=(this.fetch_repeaters[2]!=null)?this.fetch_repeaters[2].female:"";

            this.prevyear_jss3_male=(this.results[0].data.enrollment.year_by_age.data.prev_year[0])?this.results[0].data.enrollment.year_by_age.data.prev_year[0].male:"";
            this.prevyear_jss3_female=(this.results[0].data.enrollment.year_by_age.data.prev_year[0])?this.results[0].data.enrollment.year_by_age.data.prev_year[0].female:"";
            
            
            this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss1"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss1"}).female:"";
            this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss2"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss2"}).female:"";
            this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss3"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss3"}).female:"";

            this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss1"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss1"}).female:"";
            this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss2"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss2"}).female:"";
            this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss3"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss3"}).female:"";

            this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss1"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss1"}).female:"";
            this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss2"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss2"}).female:"";
            this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss3"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss3"}).female:"";

            this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss1"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss1"}).female:"";
            this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss2"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss2"}).female:"";
            this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss3"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss3"}).female:"";

            this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss1"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss1"}).female:"";
            this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss2"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss2"}).female:"";
            this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss3"}).male:"";
            this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss3"}).female:"";
            
            //C4
            //Bind metadata
            this.fetch_pupilflow=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Enrolment Items 2016";
            });
            this.fetch_pupilflow.sort(function(a,b){return a.value<b.value});

            //Bind Data
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss1"}).male:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss1"}).female:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss2"}).male:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss2"}).female:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss3"}).male:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss3"}).female:"";

            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss1"}).male:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss1"}).female:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss2"}).male:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss2"}).female:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss3"}).male:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss3"}).female:"";

            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss1"}).male:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss1"}).female:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss2"}).male:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss2"}).female:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss3"}).male:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss3"}).female:"";

            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss1"}).male:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss1"}).female:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss2"}).male:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss2"}).female:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss3"}).male:"";
            this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss3"}).female:"";


            //C5
            //Bind metadata
            this.fetch_special_needs=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Pupil Challenges 2009";
            });
            this.fetch_special_needs.sort(function(a,b){return a.value>b.value});

            //Bind data
            this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss1"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss1"}).female:"";
            this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss2"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss2"}).female:"";
            this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss3"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss3"}).female:"";

            this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss1"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss1"}).female:"";
            this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss2"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss2"}).female:"";
            this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss3"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss3"}).female:"";

            this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss1"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss1"}).female:"";
            this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss2"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss2"}).female:"";
            this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss3"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss3"}).female:"";

            this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss1"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss1"}).female:"";
            this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss2"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss2"}).female:"";
            this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss3"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss3"}).female:"";

            this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss1"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss1"}).female:"";
            this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss2"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss2"}).female:"";
            this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss3"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss3"}).female:"";

            this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss1"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss1"}).female:"";
            this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss2"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss2"}).female:"";
            this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss3"}).male:"";
            this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss3"}).female:"";



            //C6
            //Bind data
            this.registered_male=(this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"})!=null)?this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).registered_male:"";
            this.registered_female=(this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"})!=null)?this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).registered_female:"";
            this.registered_total=(this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"})!=null)?this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).registered_total:"";
            this.took_part_male=(this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"})!=null)?this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).took_part_male:"";
            this.took_part_female=(this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"})!=null)?this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).took_part_female:"";
            this.took_part_total=(this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"})!=null)?this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).took_part_total:"";
            this.passed_male=(this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"})!=null)?this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).passed_male:"";
            this.passed_female=(this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"})!=null)?this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).passed_female:"";
            this.passed_total=(this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"})!=null)?this.results[0].data.enrollment.examination.data.find(function(exam){return exam.exam_type=="JSCE"}).passed_total:"";


            //Staff
            this.non_teaching_staff_male=this.results[0].data.staff.number_of_non_teaching_staffs["male"];
            this.non_teaching_staff_female=this.results[0].data.staff.number_of_non_teaching_staffs["female"];
            this.non_teaching_staff_total=this.results[0].data.staff.number_of_non_teaching_staffs["total"];

            this.teaching_staff_male=this.results[0].data.staff.number_of_teaching_staffs["male"];
            this.teaching_staff_female=this.results[0].data.staff.number_of_teaching_staffs["female"];
            this.teaching_staff_total=this.results[0].data.staff.number_of_teaching_staffs["total"];

            //Staffs
            this.fetch_staffs=this.results[0].data.staff.information_on_all_staff.data;

            //still under the staff section
            //metadata
            this.fetch_teachingtype=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Teacher Appointment Type";
            });
            this.fetch_teachingtype.sort(function(a,b){return a.order>b.order});

            //Salary Source
            this.fetch_salarysource=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Salary Source";
            });
            this.fetch_salarysource.sort(function(a,b){return a.order>b.order});

            //Main Secondary Subject Taught
            this.fetch_specialisation=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Main Secondary Subject Taught";
            });
            this.fetch_specialisation.sort(function(a,b){return a.order>b.order});

            this.fetch_subjecttaught=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Main Secondary Subject Taught";
            });
            this.fetch_subjecttaught.sort(function(a,b){return a.order>b.order});

            //Present 2016
            this.fetch_present=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Present 2016";
            });
            this.fetch_present.sort(function(a,b){return a.order>b.order});

            //Staff Type
            this.fetch_stafftype=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Staff Type";
            });
            this.fetch_stafftype.sort(function(a,b){return a.order>b.order});

            //Academic Qualification 2009
            this.fetch_academicqualification=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Academic Qualification 2009";
            });
            this.fetch_academicqualification.sort(function(a,b){return a.order>b.order});

            //Teaching Qualifications
            this.fetch_teachingqualification=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Teaching Qualifications";
            });
            this.fetch_teachingqualification.sort(function(a,b){return a.order>b.order});

            //Classroom
            this.no_of_classrooms=this.results[0].data.classrooms.number_of_classrooms;
            this.classes_held_outside=this.results[0].data.classrooms.are_classes_held_outside;
            //Bind metadata
            this.fetch_otherRooms=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Room Type 2016";
            });
            this.fetch_otherRooms.sort(function(a,b){return a.order>b.order});

            this.fetch_otherRooms.find(function(room){return room.value=="Staff room"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Staff room"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Staff room"}).number:"";
            this.fetch_otherRooms.find(function(room){return room.value=="Office"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Office"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Office"}).number:"";
            this.fetch_otherRooms.find(function(room){return room.value=="Laboratories"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Laboratories"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Laboratories"}).number:"";
            this.fetch_otherRooms.find(function(room){return room.value=="Store room"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Store room"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Store room"}).number:"";
            this.fetch_otherRooms.find(function(room){return room.value=="Others"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Others"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Others"}).number:"";
            
            //Drinking water
            //Bind metadata
            this.fetch_drinkingwater_source=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Water Supply Type";
            });
            this.fetch_drinkingwater_source.sort(function(a,b){return a.order>b.order});
            this.sources_of_drinking_water=this.results[0].data.facilities.source_of_drinking_water.data;

            //facilities
            //Bind metadata
            this.fetch_facilities=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Useable Facility 2013";
            });
            this.fetch_facilities.sort(function(a,b){return a.order>b.order});

            this.fetch_facilities.find(function(facility){return facility.value=="Toilet"}).use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Toilet"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Toilet"}).useable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Toilet"}).not_use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Toilet"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Toilet"}).notuseable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Computer"}).use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Computer"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Computer"}).useable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Computer"}).not_use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Computer"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Computer"}).notuseable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Water Source"}).use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Water Source"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Water Source"}).useable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Water Source"}).not_use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Water Source"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Water Source"}).notuseable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Laboratory"}).use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Laboratory"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Laboratory"}).useable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Laboratory"}).not_use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Laboratory"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Laboratory"}).notuseable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Classroom"}).use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Classroom"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Classroom"}).useable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Classroom"}).not_use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Classroom"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Classroom"}).notuseable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Library"}).use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Library"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Library"}).useable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Library"}).not_use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Library"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Library"}).notuseable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Play Ground"}).use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Play Ground"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Play Ground"}).useable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Play Ground"}).not_use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Play Ground"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Play Ground"}).notuseable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Others"}).use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Others"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Others"}).useable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Others"}).not_use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Others"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Others"}).notuseable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Wash hand facility"}).use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Wash hand facility"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Wash hand facility"}).useable:"";
            this.fetch_facilities.find(function(facility){return facility.value=="Wash hand facility"}).not_use=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Wash hand facility"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Wash hand facility"}).notuseable:"";


            //shared facilities
            this.shared_facilities=this.results[0].data.facilities.shared_facilities.data;


            //power source
            //Bind metadata
            this.fetch_power_source=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Power Facilities";
            });
            this.fetch_power_source.sort(function(a,b){return a.order>b.order});
            this.sources_of_power=this.results[0].data.facilities.sources_of_power.data;   

            //toilet type
            //Bind metadata
            this.fetch_toilet_type=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Toilet Type 2009";
            });
            this.fetch_toilet_type.sort(function(a,b){return a.order>b.order});

            //toilet Pit
            this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).male_students=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"}).male:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).female_students=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"}).female:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).mixed_students=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"}).mixed:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).male_teachers=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"}).male:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).female_teachers=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"}).female:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).mixed_teachers=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"}).mixed:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).male_both=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"}).male:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).female_both=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"}).female:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).mixed_both=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"}).mixed:"";

            //Bucket system
            this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).male_students=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"}).male:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).female_students=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"}).female:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).mixed_students=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"}).mixed:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).male_teachers=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"}).male:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).female_teachers=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"}).female:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).mixed_teachers=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"}).mixed:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).male_both=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"}).male:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).female_both=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"}).female:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).mixed_both=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"}).mixed:"";

            //toilet Water flush
            this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).male_students=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"}).male:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).female_students=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"}).female:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).mixed_students=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"}).mixed:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).male_teachers=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"}).male:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).female_teachers=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"}).female:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).mixed_teachers=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"}).mixed:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).male_both=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"}).male:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).female_both=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"}).female:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).mixed_both=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"}).mixed:"";

            //toilet Pit
            this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).male_students=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"}).male:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).female_students=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"}).female:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).mixed_students=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"}).mixed:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).male_teachers=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"}).male:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).female_teachers=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"}).female:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).mixed_teachers=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"}).mixed:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).male_both=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"}).male:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).female_both=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"}).female:"";
            this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).mixed_both=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"}).mixed:"";


            //health facility
            this.fetch_healthfacility=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Available Health Facility 2009";
            });
            this.fetch_healthfacility.sort(function(a,b){return a.order>b.order});
            this.health_facilities=this.results[0].data.facilities.health_facility.data;
            
            //fence
            this.fetch_fence=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Available Fence Wall 2016";
            });

            this.fetch_fence.sort(function(a,b){return a.order>b.order});
            this.fence_facilities=this.results[0].data.facilities.fence.data;

            //student by subject
            this.fetch_subjects=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Students By Subject Area JSS";
            });
            
            this.fetch_subjects.sort(function(a,b){return a.order>b.order});

            for (var i = 0; i < this.fetch_subjects.length; i++) {
            var subject=this.fetch_subjects[i].value;
            this.fetch_subjects[i].jss1_pupils_male=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss1"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss1"}).male:"";
            this.fetch_subjects[i].jss1_pupils_female=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss1"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss1"}).female:"";
            this.fetch_subjects[i].jss2_pupils_male=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss2"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss2"}).male:"";
            this.fetch_subjects[i].jss2_pupils_female=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss2"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss2"}).female:"";
            this.fetch_subjects[i].jss3_pupils_male=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss3"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss3"}).male:"";
            this.fetch_subjects[i].jss3_pupils_female=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss3"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss3"}).female:"";

            }

            //main subject
            this.fetch_mainsubjects=this.results[1].data.data.filter(function(metadata){
                return metadata.Reference=="Main Secondary Subject Taught";
            });
            this.fetch_mainsubjects.sort(function(a,b){return a.order>b.order});

            //h1
            for (var i = 0; i < this.fetch_mainsubjects.length; i++) {
                var subject=this.fetch_mainsubjects[i].value;
                this.fetch_mainsubjects[i].jss1_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Jss1"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Jss1"}).value:"";
                this.fetch_mainsubjects[i].jss2_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Jss2"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Jss2"}).value:"";
                this.fetch_mainsubjects[i].jss3_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Jss3"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Jss3"}).value:"";
                
                }
    
            //h2
            for (var i = 0; i < this.fetch_mainsubjects.length; i++) {
                var subject=this.fetch_mainsubjects[i].value;
                this.fetch_mainsubjects[i].jss1_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Jss1"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Jss1"}).value:"";
                this.fetch_mainsubjects[i].jss2_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Jss2"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Jss2"}).value:"";
                this.fetch_mainsubjects[i].jss3_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Jss3"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Jss3"}).value:"";
                this.fetch_mainsubjects[i].yes_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.subjecttaught.find(function(p){return p.subject==subject})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.subjecttaught.find(function(p){return p.subject==subject }).yes:"";
                this.fetch_mainsubjects[i].no_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.subjecttaught.find(function(p){return p.subject==subject})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.subjecttaught.find(function(p){return p.subject==subject }).no:"";
                
                }


            //summation handling when loading the values initially
            this.entrantmaletotal=this.fetch_age.reduce(function(entrantmale_total, item){
                
                return entrantmale_total + parseInt(item.male); 
              },0);
            
            this.entrantfemaletotal=this.fetch_age.reduce(function(entrantfemale_total, item){
            
            return entrantfemale_total + parseInt(item.female); 
            },0);


            this.jss1maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.jss1_male); 
                },0);
            this.jss1femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
        
                return entrantfemale_total + parseInt(item.jss1_female); 
                },0);

            this.jss2maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
        
                return entrantmale_total + parseInt(item.jss2_male); 
                },0);
            this.jss2femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
        
                return entrantfemale_total + parseInt(item.jss2_female); 
                },0);
            this.jss3maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.jss3_male); 
                },0);
            this.jss3femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
        
                return entrantfemale_total + parseInt(item.jss3_female); 
                },0);

        });
    },

    methods:{
        //staff total
        non_teaching_staff_total_change:function(){
            this.non_teaching_staff_total=0;
            this.non_teaching_staff_total=parseInt(this.non_teaching_staff_male)+parseInt(this.non_teaching_staff_female);
        },
        teaching_staff_total_change:function(){
            this.teaching_staff_total=0;
            this.teaching_staff_total=parseInt(this.teaching_staff_male)+parseInt(this.teaching_staff_female);
        },

        entrantmale_total: function(){
            this.entrantmaletotal=0;
            this.entrantmaletotal=this.fetch_age.reduce(function(entrantmale_total, item){
                
                return entrantmale_total + parseInt(item.male); 
              },0);
        },
        entrantfemale_total: function(){
            this.entrantfemaletotal=0;
            this.entrantfemaletotal=this.fetch_age.reduce(function(entrantfemale_total, item){
                
                return entrantfemale_total + parseInt(item.female); 
              },0);
        },

        jss1entrantmale_total: function(){
            this.jss1maletotal=0;
            this.jss1maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.jss1_male); 
                },0);
        },
        jss1entrantfemale_total: function(){
            this.jss1femaletotal=0;
            this.jss1femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
        
                return entrantfemale_total + parseInt(item.jss1_female); 
                },0);
        },
        jss2entrantmale_total: function(){
            this.jss2maletotal=0;
            this.jss2maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
        
                return entrantmale_total + parseInt(item.jss2_male); 
                },0);
        },
        jss2entrantfemale_total: function(){
            this.jss2femaletotal=0;
            this.jss2femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
        
                return entrantfemale_total + parseInt(item.jss2_female); 
                },0);
        },
        jss3entrantmale_total: function(){
            this.jss3maletotal=0;
            this.jss3maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.jss3_male); 
                },0);
        },
        jss3entrantfemale_total: function(){
            this.jss3femaletotal=0;
            this.jss3femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
        
                return entrantfemale_total + parseInt(item.jss3_female); 
                },0);
        },

        //sum examination
        reg_jsce:function(){
            this.registered_total=0;
            this.registered_total=parseInt(this.registered_male)+parseInt(this.registered_female);
        },

        took_jsce:function(){
            this.took_part_total=0;
            this.took_part_total=parseInt(this.took_part_male)+parseInt(this.took_part_female);
        },

        passed_jsce:function(){
            this.passed_total=0;
            this.passed_total=parseInt(this.passed_male)+parseInt(this.passed_female);
        },





        //save the sections
        saveIdentification: function () {
            //use veevalidate later for this
            if (this.schoolname != '' && this.schoolstreet != '' && this.schoollga != '' &&
                this.schoolstate != '' && this.schoolward != '' && this.schoolemail != '' &&
                this.schooltown != '' && this.schooltelephone != '') {

                this.get_state_code = this.fetch_states[this.state_picked].name
                //alert(this.get_state_code);
                this.get_lga_code = this.fetch_lga[this.lga_picked].name;
                //alert(this.get_lga_code);
                axios.post("/api/Section/SchoolIdentification",
                    {
                        school_code: schoolcode.toLowerCase(),
                        school_name: this.schoolname,
                        xcoordinate: this.xcoordinate,
                        ycoordinate: this.ycoordinate,
                        zcoordinate: this.zcoordinate,
                        school_address: this.schoolstreet,
                        town: this.schooltown,
                        ward: this.schoolward,
                        lga: this.get_lga_code,
                        state: this.get_state_code,
                        telephone: this.schooltelephone,
                        email: this.schoolemail
                    }).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //alert("Data Saved to DB");
                        } else {
                            alert("Check the form for the required data");
                        }
                    });
            }
        },

        nextCharacteristics: function () {
            console.log(this.loeo2_choice + " " + this.loeo_choice);
            alert(this.shifts_choice + " " + this.facilities_choice);
        },

        getLGA: function () {
            var arrayOfObjects2 = [];

            //this.counter_state = this.state_index[this.state_picked];
            //alert(this.state_picked);

            for (var i = 0; i < this.results[1].attributes.states[this.state_picked].data.lgas.length; i++) {
                var obj2 = {};
                
                obj2["name"] = this.results[1].attributes.states[this.state_picked].data.lgas[i].data.name;
                obj2["code"] = this.results[1].attributes.states[this.state_picked].data.lgas[i].data.lgacode;
                //this.lga_index[this.results[1].attributes.states[i].data.lgas[i].data.name] = i;
            

                arrayOfObjects2.push(obj2);
            }
            //this.lga_picked=this.results[1].attributes.states[this.counter_state].data.lgas[0].data.name;
            this.fetch_lga = arrayOfObjects2;


            console.log('now: ', JSON.stringify(this.fetch_states, null, " "));
            //alert(this.lga_picked);
        },

        saveCharacteristics: function () {

            axios.post("/api/Section/SchoolCharacteristics",
                {
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    shared_facilities: this.facilities_choice,
                    schools_sharingwith: this.facilities_shared,
                    location_type: this.location_of_school,
                    school_type: this.type_of_school,
                    shift:this.shifts_choice,
                    level_of_education: this.level_of_education,
                    multi_grade_teaching: this.multigrade_choice,
                    distance_from_catchment_area: this.average_distance,
                    students_travelling_3km: this.student_distance,
                    male_student_b: this.students_boarding_male,
                    female_student_b: this.students_boarding_female,
                    has_school_development_plan: this.sdp_choice,
                    has_sbmc: this.sbmc_choice,
                    has_pta: this.pta_choice,
                    last_inspection_date: this.date_inspection,
                    no_of_inspection:this.no_of_inspection,
                    last_inspection_authority: this.authority_choice,
                    conditional_cash_transfer: this.cash_transfer,
                    has_school_grant: this.grants_choice,
                    has_security_guard: this.guard_choice,
                    ownership: this.ownership_choice,
                }).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert("Data Saved to DB");
                    } else {
                        alert("Check the form for the required data");
                    }
                });
            axios.post("/api/Section/SchoolRegistration",
                {
                    school_code: schoolcode.toLowerCase(),
                    year: year
                }).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert("Data Saved to DB");
                    } else {
                        alert(response.data);
                    }
                });
        },

        checkBirthCertificates:function(){
            alert("bc:"+this.fetch_birth_certificate[0].value+" ,male:"+this.fetch_birth_certificate[0].male);
        },

        saveBirthCertificates: function () {
            function checkNpc(bc) {
                return bc.value == "National Population Commission";
            }
            function checkHospital(bc) {
                return bc.value == "Hospital";
            }
            function checkLga(bc) {
                return bc.value == "LGA";
            }
            function checkCourt(bc) {
                return bc.value == "Court";
            }
            function checkUn(bc) {
                return bc.value == "UN";
            }
            function checkOthers(bc) {
                return bc.value == "Others";
            }

            axios.post("/api/Section/SchoolEnrollment/BirthCertificate",{
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class: "jss",
                birthcertificate_npc_male: this.fetch_birth_certificate.find(checkNpc).male,
                birthcertificate_npc_female: this.fetch_birth_certificate.find(checkNpc).female,
                birthcertificate_hospital_male: this.fetch_birth_certificate.find(checkHospital).male,
                birthcertificate_hospital_female: this.fetch_birth_certificate.find(checkHospital).female,
                birthcertificate_lga_male: this.fetch_birth_certificate.find(checkLga).male,
                birthcertificate_lga_female: this.fetch_birth_certificate.find(checkLga).female,
                birthcertificate_court_male: this.fetch_birth_certificate.find(checkCourt).male,
                birthcertificate_court_female: this.fetch_birth_certificate.find(checkCourt).female,
                birthcertificate_un_male: this.fetch_birth_certificate.find(checkUn).male,
                birthcertificate_un_female: this.fetch_birth_certificate.find(checkUn).female,
                birthcertificate_others_male: this.fetch_birth_certificate.find(checkOthers).male,
                birthcertificate_others_female: this.fetch_birth_certificate.find(checkOthers).female
            }  
            ).then(response => {

                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //this.fetch_b_c = this.fetch_b_c;
                        //alert("Data Saved to DB");
                        //alert(JSON.stringify(this.fetch_b_c, null, 2));
                    } else if(this.resultsSave.status == "error") {
                        alert(response.data.message);
                    }else {
                        alert("Check the form for the required data");
                    }
                })
        },

        saveEntrants: function () {
           //alert(JSON.stringify(this.fetch_a))
            axios.post("/api/Section/SchoolEnrollment/Entrant",{
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class: "jss",
                enrollment_entrant_age_12_male: this.fetch_age.find(function(age){return age.value=="12"}).male,
                enrollment_entrant_age_12_female: this.fetch_age.find(function(age){return age.value=="12"}).female,
                enrollment_entrant_age_13_male: this.fetch_age.find(function(age){return age.value=="13"}).male,
                enrollment_entrant_age_13_female: this.fetch_age.find(function(age){return age.value=="13"}).female,
                enrollment_entrant_age_14_male: this.fetch_age.find(function(age){return age.value=="14"}).male,
                enrollment_entrant_age_14_female: this.fetch_age.find(function(age){return age.value=="14"}).female,
                enrollment_entrant_age_above14_male: this.fetch_age.find(function(age){return age.value=="Above 14 Years"}).male,
                enrollment_entrant_age_above14_female: this.fetch_age.find(function(age){return age.value=="Above 14 Years"}).female,
                enrollment_entrant_age_below12_male: this.fetch_age.find(function(age){return age.value=="Below 12 Years"}).male,
                enrollment_entrant_age_below12_female: this.fetch_age.find(function(age){return age.value=="Below 12 Years"}).female,
                
            }
            
            ).then(response => {

                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //this.fetch_b_c = this.fetch_b_c;
                        //alert("Data Saved to DB");
                        console.log("Data Saved to DB");
                    } else if(this.resultsSave.status == "error") {
                        alert(response.data.message);
                    }else {
                        alert("Check the form for the required data");
                    }
                })
        },

        saveEnrollmentByAge: function () {
            
            //alert(this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss3_male);
 
             axios.post("/api/Section/SchoolEnrollment/EnrollmentByAge",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "jss",
                 stream_jss1_stream: this.jss1_stream,
                 stream_jss2_stream: this.jss2_stream,
                 stream_jss3_stream: this.jss3_stream,

                 stream_jss1_streamwithmultigrade:  this.jss1_stream_with_multigrade,
                 stream_jss2_streamwithmultigrade:  this.jss2_stream_with_multigrade,
                 stream_jss3_streamwithmultigrade:  this.jss3_stream_with_multigrade,

                 enrollment_jss1_age_12_male: this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss1_male,
                 enrollment_jss1_age_12_female: this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss1_female,
                 enrollment_jss2_age_12_male: this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss2_male,
                 enrollment_jss2_age_12_female: this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss2_female,
                 enrollment_jss3_age_12_male: this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss3_male,
                 enrollment_jss3_age_12_female: this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss3_female,

                 enrollment_jss1_age_13_male: this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss1_male,
                 enrollment_jss1_age_13_female: this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss1_female,
                 enrollment_jss2_age_13_male: this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss2_male,
                 enrollment_jss2_age_13_female: this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss2_female,
                 enrollment_jss3_age_13_male: this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss3_male,
                 enrollment_jss3_age_13_female: this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss3_female,

                 enrollment_jss1_age_14_male: this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss1_male,
                 enrollment_jss1_age_14_female: this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss1_female,
                 enrollment_jss2_age_14_male: this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss2_male,
                 enrollment_jss2_age_14_female: this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss2_female,
                 enrollment_jss3_age_14_male: this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss3_male,
                 enrollment_jss3_age_14_female: this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss3_female,

                 enrollment_jss1_age_above14_male: this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss1_male,
                 enrollment_jss1_age_above14_female: this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss1_female,
                 enrollment_jss2_age_above14_male: this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss2_male,
                 enrollment_jss2_age_above14_female: this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss2_female,
                 enrollment_jss3_age_above14_male: this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss3_male,
                 enrollment_jss3_age_above14_female: this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss3_female,

                 enrollment_jss1_age_below12_male: this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss1_male,
                 enrollment_jss1_age_below12_female: this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss1_female,
                 enrollment_jss2_age_below12_male: this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss2_male,
                 enrollment_jss2_age_below12_female: this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss2_female,
                 enrollment_jss3_age_below12_male: this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss3_male,
                 enrollment_jss3_age_below12_female: this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss3_female,

                 repeater_jss1_male:  this.jss1_repeaters_male,
                 repeater_jss1_female:  this.jss1_repeaters_female,
                 repeater_jss2_male:  this.jss2_repeaters_male,
                 repeater_jss2_female:  this.jss2_repeaters_female,
                 repeater_jss3_male:  this.jss3_repeaters_male,
                 repeater_jss3_female:  this.jss3_repeaters_female,

                 prevyear_jss3_male:this.prevyear_jss3_male,
                 prevyear_jss3_female:this.prevyear_jss3_female
                 
             }
             
             ).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
 
                     } else if(this.resultsSave.status == "error") {
                         alert(response.data.message);
                     }else {
                         alert("Check the form for the required data");
                     }
 
                 })
         },
        
        saveSpecialNeed: function () {
            //console.log(JSON.stringify(this.fetch_all_special_needs, null, 2))
            
          

 
             axios.post("/api/Section/SchoolEnrollment/SpecialNeeds",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "jss",
                 
                 specialneed_jss1_altruism_male: this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss1_male,
                 specialneed_jss1_altruism_female: this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss1_female,
                 specialneed_jss2_altruism_male: this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss2_male,
                 specialneed_jss2_altruism_female: this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss2_female,
                 specialneed_jss3_altruism_male: this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss3_male,
                 specialneed_jss3_altruism_female: this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss3_female,

                 specialneed_jss1_albinism_male: this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss1_male,
                 specialneed_jss1_albinism_female: this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss1_female,
                 specialneed_jss2_albinism_male: this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss2_male,
                 specialneed_jss2_albinism_female: this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss2_female,
                 specialneed_jss3_albinism_male: this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss3_male,
                 specialneed_jss3_albinism_female: this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss3_female,

                 specialneed_jss1_mentally_male: this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss1_male,
                 specialneed_jss1_mentally_female: this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss1_female,
                 specialneed_jss2_mentally_male: this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss2_male,
                 specialneed_jss2_mentally_female: this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss2_female,
                 specialneed_jss3_mentally_male: this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss3_male,
                 specialneed_jss3_mentally_female: this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss3_female,
                 
                 specialneed_jss1_physical_male: this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss1_male,
                 specialneed_jss1_physical_female: this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss1_female,
                 specialneed_jss2_physical_male: this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss2_male,
                 specialneed_jss2_physical_female: this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss2_female,
                 specialneed_jss3_physical_male: this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss3_male,
                 specialneed_jss3_physical_female: this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss3_female,

                 specialneed_jss1_hearing_male: this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss1_male,
                 specialneed_jss1_hearing_female: this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss1_female,
                 specialneed_jss2_hearing_male: this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss2_male,
                 specialneed_jss2_hearing_female: this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss2_female,
                 specialneed_jss3_hearing_male: this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss3_male,
                 specialneed_jss3_hearing_female: this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss3_female,
                 
                 specialneed_jss1_blind_male:this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss1_male,
                 specialneed_jss1_blind_female:this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss1_female,
                 specialneed_jss2_blind_male:this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss2_male,
                 specialneed_jss2_blind_female:this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss2_female,
                 specialneed_jss3_blind_male:this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss3_male,
                 specialneed_jss3_blind_female:this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss3_female,
               
             }
             
                 
             ).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                         console.log("Data Saved to DB");
                         //console.log("0" + this.fetch_r[0].repeaters);
 
                     } else if(this.resultsSave.status == "error") {
                         alert(response.data.message);
                     }else {
                         alert("Check the form for the required data");
                     }
                 })
         },

         saveStudentsFlow: function () {
            //alert(JSON.stringify(this.fetch_drop, null, 2))
 
             axios.post("/api/Section/SchoolEnrollment/StudentFlow",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "jss",
                 
                 pupilflow_jss1_dropout_male: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss1_male,
                 pupilflow_jss1_dropout_female: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss1_female,
                 pupilflow_jss2_dropout_male: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss2_male,
                 pupilflow_jss2_dropout_female: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss2_female,
                 pupilflow_jss3_dropout_male: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss3_male,
                 pupilflow_jss3_dropout_female: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss3_female,

                 pupilflow_jss1_transferin_male: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss1_male,
                 pupilflow_jss1_transferin_female: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss1_female,
                 pupilflow_jss2_transferin_male: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss2_male,
                 pupilflow_jss2_transferin_female: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss2_female,
                 pupilflow_jss3_transferin_male: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss3_male,
                 pupilflow_jss3_transferin_female: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss3_female,

                 pupilflow_jss1_transferout_male: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss1_male,
                 pupilflow_jss1_transferout_female: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss1_female,
                 pupilflow_jss2_transferout_male: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss2_male,
                 pupilflow_jss2_transferout_female: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss2_female,
                 pupilflow_jss3_transferout_male: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss3_male,
                 pupilflow_jss3_transferout_female: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss3_female,

                 pupilflow_jss1_promoted_male: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss1_male,
                 pupilflow_jss1_promoted_female: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss1_female,
                 pupilflow_jss2_promoted_male: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss2_male,
                 pupilflow_jss2_promoted_female: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss2_female,
                 pupilflow_jss3_promoted_male: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss3_male,
                 pupilflow_jss3_promoted_female: this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss3_female,
                 
                  
             }    
             ).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                     } else if(this.resultsSave.status == "error") {
                         alert(response.data.message);
                     }else {
                         alert("Check the form for the required data");
                     }
                 })
         },

         saveExamination: function () {
                axios.post("/api/Section/SchoolEnrollment/Examination",
                {
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    examination: "jsce",

                    examination_jsce_registeredmale: this.registered_male,
                    examination_jsce_registeredfemale: this.registered_female,
                    examination_jsce_tookpartmale: this.took_part_male,
                    examination_jsce_tookpartfemale: this.took_part_female,
                    examination_jsce_passedmale: this.passed_male,
                    examination_jsce_passedfemale: this.passed_female

                }).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //alert(this.resultsSave.message);
                        } else {
                            alert(this.resultsSave.message);
                        }
                })
            },
                
            saveNoOfStaff: function () {
                    axios.post("/api/Section/Staff/NoOfStaff",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
                        
            
                        staff_nonteachersmale: this.non_teaching_staff_male,
                        staff_nonteachersfemale: this.non_teaching_staff_female,
                        staff_teachersmale: this.teaching_staff_male,
                        staff_teachersfemale: this.teaching_staff_female,
                        
            
                        }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                        })
                    },

                submitStaff: function(){
                    //add to the table
                    var thestaff={staff_file_no:this.staff_file_no,staff_name:this.staff_name,staff_gender:this.staff_gender,staff_type:this.staff_type,salary_source:this.salary_source,staff_yob:this.staff_yob,staff_yfa:this.staff_yfa,staff_ypa:this.staff_ypa,staff_yps:this.staff_yps,staff_level:this.staff_level,present:this.present,academic_qualification:this.academic_qualification,teaching_qualification:this.teaching_qualification,area_specialisation:this.area_specialisation,subject_taught:this.subject_taught,teaching_type:this.teaching_type,is_teaching_ss:this.is_teaching_ss,attended_training:this.attended_training};
                    
                    //post to the savestaff api
                    axios.post("/api/Section/Staff/Addnew",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
    
                        staff_file_no: this.staff_file_no,
                        staff_name:this.staff_name,
                        gender:this.staff_gender,
                        stafftype:this.staff_type,
                        salary_source:this.salary_source,
                        dob:this.staff_yob,
                        year_of_first_appointment:this.staff_yfa,
                        present_appointment_year:this.staff_ypa,
                        year_of_posting:this.staff_yps,
                        level:this.staff_level,
                        present:this.present,
                        academic_qualification:this.academic_qualification,
                        teaching_qualification:this.teaching_qualification,
                        specialisation:this.area_specialisation,
                        mainsubject_taught:this.subject_taught,
                        teaching_type:this.teaching_type,
                        is_teaching_ss:this.is_teaching_ss,
                        teacher_attended_training:this.attended_training,

                        
    
                    }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                    })

                    //if successfully posted add to the table
                    this.fetch_staffs.push(thestaff);


                },

                saveNoOfClassrooms: function () {
                    axios.post("/api/Section/Classroom/NoOfClassroom",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
                        
            
                        no_of_classrooms: this.no_of_classrooms,
                        classes_held_outside: this.classes_held_outside,
                        
                        
            
                        }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                        })
                    },
                
                saveNoOfOtherrooms: function () {
                    axios.post("/api/Section/Room/Otherroom",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
                    
                        no_of_staffrooms: this.fetch_otherRooms.find(function(room){return room.value=="Staff room"}).no_of_rooms,
                        no_of_offices: this.fetch_otherRooms.find(function(room){return room.value=="Office"}).no_of_rooms,
                        no_of_laboratories: this.fetch_otherRooms.find(function(room){return room.value=="Laboratories"}).no_of_rooms,
                        no_of_storerooms: this.fetch_otherRooms.find(function(room){return room.value=="Store room"}).no_of_rooms,
                        no_of_others: this.fetch_otherRooms.find(function(room){return room.value=="Others"}).no_of_rooms,
            
                        }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                        })
                },

                saveDWSources: function () {
                    axios.post("/api/Section/Facility/DrinkingWater",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
                    
                        drinking_water_sources: this.sources_of_drinking_water,
            
                        }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                        })
                },
                saveFacilitiesAvailable: function () {
                    axios.post("/api/Section/Facility/Available",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
                    
                        toilets_useable: this.fetch_facilities.find(function(facility){return facility.value=="Toilet"}).use,
                        toilets_notuseable: this.fetch_facilities.find(function(facility){return facility.value=="Toilet"}).not_use,
                        computer_useable: this.fetch_facilities.find(function(facility){return facility.value=="Computer"}).use,
                        computer_notuseable: this.fetch_facilities.find(function(facility){return facility.value=="Computer"}).not_use,
                        watersource_useable: this.fetch_facilities.find(function(facility){return facility.value=="Water Source"}).use,
                        watersource_notuseable: this.fetch_facilities.find(function(facility){return facility.value=="Water Source"}).not_use,
                        laboratory_useable: this.fetch_facilities.find(function(facility){return facility.value=="Laboratory"}).use,
                        laboratory_notuseable: this.fetch_facilities.find(function(facility){return facility.value=="Laboratory"}).not_use,
                        classrooms_useable: this.fetch_facilities.find(function(facility){return facility.value=="Classroom"}).use,
                        classrooms_notuseable: this.fetch_facilities.find(function(facility){return facility.value=="Classroom"}).not_use,
                        library_useable: this.fetch_facilities.find(function(facility){return facility.value=="Library"}).use,
                        library_notuseable: this.fetch_facilities.find(function(facility){return facility.value=="Library"}).not_use,
                        playground_useable: this.fetch_facilities.find(function(facility){return facility.value=="Play Ground"}).use,
                        playground_notuseable: this.fetch_facilities.find(function(facility){return facility.value=="Play Ground"}).not_use,
                        wash_useable: this.fetch_facilities.find(function(facility){return facility.value=="Wash hand facility"}).use,
                        wash_notuseable: this.fetch_facilities.find(function(facility){return facility.value=="Wash hand facility"}).not_use,
                        others_useable: this.fetch_facilities.find(function(facility){return facility.value=="Others"}).use,
                        others_notuseable: this.fetch_facilities.find(function(facility){return facility.value=="Others"}).not_use,
                        
                        }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                        })
                },

                saveToilet: function () {
                    //alert(this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).male_students);
                    axios.post("/api/Section/Facility/Toilet",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
                    
                        pit_students_male: this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).male_students,
                        pit_students_female: this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).female_students,
                        pit_students_mixed: this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).mixed_students,
                        pit_teachers_male: this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).male_teachers,
                        pit_teachers_female: this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).female_teachers,
                        pit_teachers_mixed: this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).mixed_teachers,
                        pit_both_male: this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).male_both,
                        pit_both_female: this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).female_both,
                        pit_both_mixed: this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).mixed_both,
                        bucket_students_male: this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).male_students,
                        bucket_students_female: this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).female_students,
                        bucket_students_mixed: this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).mixed_students,
                        bucket_teachers_male: this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).male_teachers,
                        bucket_teachers_female: this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).female_teachers,
                        bucket_teachers_mixed: this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).mixed_teachers,
                        bucket_both_male: this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).male_both,
                        bucket_both_female: this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).female_both,
                        bucket_both_mixed: this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).mixed_both,
                        
                        waterflush_students_male: this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).male_students,
                        waterflush_students_female: this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).female_students,
                        waterflush_students_mixed: this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).mixed_students,
                        waterflush_teachers_male: this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).male_teachers,
                        waterflush_teachers_female: this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).female_teachers,
                        waterflush_teachers_mixed: this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).mixed_teachers,
                        waterflush_both_male: this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).male_both,
                        waterflush_both_female: this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).female_both,
                        waterflush_both_mixed: this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).mixed_both,
                        others_students_male: this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).male_students,
                        others_students_female: this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).female_students,
                        others_students_mixed: this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).mixed_students,
                        others_teachers_male: this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).male_teachers,
                        others_teachers_female: this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).female_teachers,
                        others_teachers_mixed: this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).mixed_teachers,
                        others_both_male: this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).male_both,
                        others_both_female: this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).female_both,
                        others_both_mixed: this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).mixed_both,

                        }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                        })
                },

                saveSharedFacilities: function () {
                    axios.post("/api/Section/Facility/SharedFacilities",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
                    
                        shared_facilities: this.shared_facilities,
            
                        }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                        })
                },

                //source of power
                savePowerSource: function () {
                    axios.post("/api/Section/Facility/PowerSource",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
                    
                        power_source: this.sources_of_power,
            
                        }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                        })
                },

                //source of power
                saveStudentbySubject: function () {
                    axios.post("/api/Section/Subject/Students",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
                    
                        students_by_subject: this.fetch_subjects,
            
                        }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                        })
                },

                saveHealthFence: function () {
                    axios.post("/api/Section/Facility/HealthFacility",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
                    
                        health_facilities: this.health_facilities,
            
                        }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                        });

                    axios.post("/api/Section/Facility/FenceFacility",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
                    
                        fence_facilities: this.fence_facilities,
            
                        }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                        });

                },

                saveStudents:function(){
                    axios.post("/api/Section/Book/Student",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
                    
                        students_book: this.fetch_mainsubjects,
            
                        }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                        });

                   
                },

                saveTeachers:function(){
                    

                    axios.post("/api/Section/Book/Teacher",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        school_class:'jss',
                    
                        teachers_book: this.fetch_mainsubjects,
            
                        }).then(response => {
                            this.resultsSave = response.data;
                            if (this.resultsSave.status == "success") {
                                //alert(this.resultsSave.message);
                            } else {
                                alert(this.resultsSave.message);
                            }
                        })
                },

    },
});