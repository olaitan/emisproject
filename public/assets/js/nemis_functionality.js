var url = window.location.href;
var res = url.split("/");
var schoolcode = res[4];
var year = res[6];

// alert(JSON.stringify(res, null, 2))
//alert(JSON.stringify(schoolcode, null, 2));
//alert(JSON.stringify(year, null, 2));
//var schoolcode = window.location.href;
//schoolcode = schoolcode.substr(schoolcode.indexOf("SCH0"));
//alert("code: " + url);  

function merge(a, b) {
    for (var key in b) {
        try {
            if (b[key].constructor === Object) {
                a[key] = merge(a[key], b[key]);
            } else {
                a[key] = b[key];
            }
        } catch (e) {
            a[key] = b[key];
        }
    }
    return a;
}

function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}

var vmForm = new Vue({

    el: '#content',

    data: {
        results: [],
        resultsSave: [],
        schoolname: '',
        schoolstreet: '',
        schoolemail: '',
        schoollga: '',
        schooltown: '',
        schooltelephone: '',
        schoolward: '',
        schoolstate: '',

        fetch_states: [],
        state_picked: '',
        state_index: [],
        get_state_code: '',

        results_lga: [],
        fetch_lga: [],
        lga_picked: '',
        lga_index: [],
        get_lga_code: '',

        counter_state: 0,
        counter_lga: 0,

        location_type_ref: [],
        location_type_val: [],
        metadata: [],

        fetch_location: [],
        fetch_levels: [],
        fetch_category: [],
        fetch_authority: [],
        fetch_ownership: [],
        fetch_birth_certificate: [],
        fetch_b_c: [],
        fetch_b_c_no: [],
        fetch_age: [],
        fetch_a: [],
        fetch_a_no: [],
        fetch_streams: [],
        fetch_year_by_age: [],
        fetch_year_by_age_no: [],
        fetch_y_b_a1: [], fetch_y_b_a2: [], fetch_y_b_a3: [],
        fetch_y_b_no1: [], fetch_y_b_no2: [], fetch_y_b_no3: [],
        fetch_all_ages: [],
        fetch_repeaters: [], fetch_r:[], 
        fetch_dropouts: [], fetch_drop:[],
        fetch_transfer_in: [], fetch_t_in:[],
        fetch_transfer_out: [], fetch_t_out:[],
        fetch_promoted: [], fetch_pro:[],

        fetch_all_special_needs: [], fetch_special_needs: [], fetch_special_needs_item: [],
        fetch_s_n1: [], fetch_s_n2: [], fetch_s_n3: [],
        fetch_s_n_no1: [], fetch_s_n_no2: [], fetch_s_n_no3: [],
        fetch_a_s_n: [], fetch_a_s_n_no: [],

        inputParams: [],

        exam_type:'',
        registered_male:'',
        registered_female:'',
        registered_total:'', 
        took_part_male:'',
        took_part_female:'',
        took_part_total:'',
        passed_male:'',
        passed_female:'',
        passed_total:'',

        year: '',
        year_of_establishment: '',
        location_choice: '',
        loeo2_choice: '',
        loeo_choice: '',
        shifts_choice: '',
        shifts_choice_bi: '',
        facilities_choice: '',
        facilities_choice_bi: '',
        facilities_choice_yes: '',
        multigrade_choice: '',
        multigrade_choice_bi: '',
        average_distance: '',
        student_distance: '',
        students_boarding_female: '',
        students_boarding_male: '',
        sdp_choice: '',
        sdp_choice_bi: '',
        sbmc_choice: '',
        sbmc_choice_bi: '',
        pta_choice: '',
        pta_choice_bi: '',
        date_inspection: '',
        authority_choice: '',
        cash_transfer: '',
        grants_choice: '',
        grants_choice_bi: '',
        guard_choice: '',
        guard_choice_bi: '',
        ownership_choice: '',

        nPopC_male: '',
        nPopC_female: '',
        others_male: '',
        others_female: '',

        total_age_male: '',
        total_age_female: '',
    },

    computed: {
    },
    
    watch:{
        total_Age: function() {
            var total=0;
            for(var i=0; i<fetch_a.length; i++) {
               if(!fetch_a[i].male.isNaN()) {
                    total = total + fetch_a[i].male;
               }
            }
            this.total_age_male = total;
        },
        
    },

    mounted: function () { 

        axios.get("http://127.0.0.1:8000/api/school/" + schoolcode.toLowerCase() + "/year/" + year).then(response => {
            this.results = response.data;
            this.results_lga = response.data;
            //this.results =  JSON.parse(response.data);
            //console.log(this.results);
            console.log('r: ', JSON.stringify(response.data, null, 2));
            /*for (var prop in response.data.data) {
                this.schools.push(response.data.data.name[prop]);
            }*/

            //this.schools.push(this.results.data[0].data.name);
            //console.log(this.schools);



            var arrayOfObjects = []

            for (var i = 0; i < this.results[1].attributes.states.length; i++) {
                var obj = {};
                for (var j = 0; j < this.results[1].attributes.states.length; j++) {
                    //obj[keys[j]] = values[i][j];
                    obj["name"] = this.results[1].attributes.states[i].data.name;
                    obj["code"] = this.results[1].attributes.states[i].data.statecode;
                    this.state_index[this.results[1].attributes.states[i].data.name] = i; 
                    //save the state name as a key and the value is the index

                }

                arrayOfObjects.push(obj);
            }
            //this.state_picked=this.results[1].attributes.states[0].data.name;


            this.fetch_states = arrayOfObjects;



            //alert(this.counter_state);

            var arrayOfObjects2 = []
            ////LGA
            for (var i = 0; i < this.results[1].attributes.states[this.counter_state].data.lgas.length; i++) {
                var obj2 = {};
                for (var j = 0; j < this.results[1].attributes.states[this.counter_state].data.lgas.length; j++) {
                    //obj[keys[j]] = values[i][j];
                    obj2["name"] = this.results[1].attributes.states[this.counter_state].data.lgas[i].data.name;
                    obj2["code"] = this.results[1].attributes.states[this.counter_state].data.lgas[i].data.lgacode;
                    this.lga_index[this.results[1].attributes.states[this.counter_state].data.lgas[i].data.name] = i;

                }

                arrayOfObjects2.push(obj2);
            }
            // this.lga_picked = this.results[1].attributes.states[0].data.lgas[0].data.name;
            //alert(this.lga_picked);   

            this.fetch_lga = arrayOfObjects2;

            var arrayOfObjects3 = []

            for (var i = 0; i < this.results[1].data.data.length; i++) {
                var obj2 = {};
                for (var j = 0; j < this.results[1].data.data.length; j++) {
                    //obj[keys[j]] = values[i][j];
                    obj2["ref"] = this.results[1].data.data[i].Reference;
                    obj2["value"] = this.results[1].data.data[i].value;
                    this.location_type_ref[i] = this.results[1].data.data[i].Reference;
                    this.location_type_val[i] = this.results[1].data.data[i].value;
                }

                arrayOfObjects3.push(obj2);
            }

            this.metadata = arrayOfObjects3;
            //alert(JSON.stringify(this.location_type_ref, null, 2));

            var arrayOfObjects4 = []
            var indexes = [];
            var ind = 0;
            var loc = [];

            var indexes_levels = [];
            var ind_levels = 0;
            var loc_levels = [];

            var indexes_category = [];
            var ind_category = 0;
            var loc_category = [];

            var indexes_ownership = [];
            var ind_ownership = 0;
            var loc_ownership = [];

            var indexes_authority = [];
            var ind_authority = 0;
            var loc_authority = [];

            var indexes_birth_certificate = [];
            var ind_birth_certificate = 0;
            var loc_birth_certificate = [];

            var indexes_age = [];
            var ind_age = 0;
            var loc_age = [];

            var indexes_special_needs = [];
            var ind_special_needs = 0;
            var loc_special_needs = [];



            for (var i = 0; i < this.location_type_ref.length; i++) {
                
                 if (this.location_type_ref[i] == "School Level") {
                    indexes_levels[ind_levels] = i;
                    loc_levels[ind_levels] = this.location_type_ref[i];
                    ind_levels++;
                }
                else if (this.location_type_ref[i] == "School Category") {
                    indexes_category[ind_category] = i;
                    loc_category[ind_category] = this.location_type_ref[i];
                    ind_category++;
                }
                else if (this.location_type_ref[i] == "Ownership - 2013") {
                    indexes_ownership[ind_ownership] = i;
                    loc_ownership[ind_ownership] = this.location_type_ref[i];
                    ind_ownership++;

                    indexes_authority[ind_authority] = i;
                    loc_authority[ind_authority] = this.location_type_ref[i];
                    ind_authority++;
                }
                else if (this.location_type_ref[i] == "Birth Certificate Type") {
                    indexes_birth_certificate[ind_birth_certificate] = i;
                    loc_birth_certificate[ind_birth_certificate] = this.location_type_ref[i];
                    ind_birth_certificate++;
                }
                else if (this.location_type_ref[i] == "Age Category") {
                    indexes_age[ind_age] = i;
                    loc_age[ind_age] = this.location_type_ref[i];
                    ind_age++;
                }
                else if (this.location_type_ref[i] == "Pupil Challenges 2009") {
                    indexes_special_needs[ind_special_needs] = i;
                    loc_special_needs[ind_special_needs] = this.location_type_ref[i];
                    ind_special_needs++;
                }
                else if (this.location_type_ref[i] == "Location Type") {
                    indexes[ind] = i;
                    loc[ind] = this.location_type_ref[i];
                    ind++;
                }
            }

            var loc_result = [];
            var loc_result_location = [];
            var loc_result_levels = [];
            var loc_result_category = [];
            var loc_result_ownership = [];
            var loc_result_authority = [];
            var loc_result_birth_certificate = [];
            var loc_result_age = [];
            var loc_result_special_needs = [];

            for (var i = 0; i < loc.length; i++) {
                loc_result_location[i] = this.location_type_val[indexes[i]];
            }

            this.fetch_location = loc_result_location;

            //alert(JSON.stringify( this.fetch_location, null, 0));

            for (var i = 0; i < loc_levels.length; i++) {
                loc_result_levels[i] = this.location_type_val[indexes_levels[i]];
            }

            this.fetch_levels = loc_result_levels;

            for (var i = 0; i < loc_category.length; i++) {
                loc_result_category[i] = this.location_type_val[indexes_category[i]];
            }

            this.fetch_category = loc_result_category;

            for (var i = 0; i < loc_ownership.length; i++) {
                loc_result_ownership[i] = this.location_type_val[indexes_ownership[i]];
            }

            this.fetch_ownership = loc_result_ownership;

            for (var i = 0; i < loc_authority.length; i++) {
                loc_result_authority[i] = this.location_type_val[indexes_authority[i]];
            }

            this.fetch_authority = loc_result_authority;

            var array = [];
            array = loc_result_authority;



            var index = array.indexOf("Community");
            if (index > -1) {
                array.splice(index, 1);
            }

            this.fetch_authority = array;

            for (var i = 0; i < loc_birth_certificate.length; i++) {
                loc_result_birth_certificate[i] = this.location_type_val[indexes_birth_certificate[i]];
            }

            this.fetch_birth_certificate = loc_result_birth_certificate;

            for (var i = 0; i < loc_age.length; i++) {
                loc_result_age[i] = this.location_type_val[indexes_age[i]];
            }

            this.fetch_age = loc_result_age;

            for (var i = 0; i < loc_special_needs.length; i++) {
                loc_result_special_needs[i] = this.location_type_val[indexes_special_needs[i]];
            }

            this.fetch_special_needs_item = loc_result_special_needs;
            //alert(JSON.stringify(this.fetch_special_needs, null, 0))

            ///////OPERTATIONS FOR BIRTH////
            var array_cert = [];           

            for (var i = 0; i < this.fetch_birth_certificate.length; i++) {
                var arr = {};
                for (var j = 0; j < this.fetch_birth_certificate.length; j++) {
                    arr["certificates"] = this.fetch_birth_certificate[i];
                    arr["male"] = "";
                    arr["female"] = "";                
                }
                this.fetch_b_c.push(arr);

            }

            for (var i = 0; i < this.fetch_b_c.length; i++) {
                var arr = {};
                for (var j = 0; j < this.results[0].data.enrollment.birth_certificates.data.length; j++) {


                    if(this.fetch_b_c[i].certificates == this.results[0].data.enrollment.birth_certificates.data[j].certificate_category) {
                        this.fetch_b_c[i].male = this.results[0].data.enrollment.birth_certificates.data[j].male
                        this.fetch_b_c[i].female = this.results[0].data.enrollment.birth_certificates.data[j].female
                    }                           
                }
            }



            ///////OPERTATIONS FOR AGES////

            for (var i = 0; i < this.fetch_age.length; i++) {
                var arr = {};
                for (var j = 0; j < this.fetch_age.length; j++) {
                    arr["age"] = this.fetch_age[i];
                    arr["male"] = "";
                    arr["female"] = "";
                            
                }
                this.fetch_a.push(arr);

            }

            for (var i = 0; i < this.fetch_a.length; i++) {
                var arr = {};
                for (var j = 0; j < this.results[0].data.enrollment.entrants.data.length; j++) {


                    if(this.fetch_a[i].age == this.results[0].data.enrollment.entrants.data[j].age_category) {
                        this.fetch_a[i].male = this.results[0].data.enrollment.entrants.data[j].male;
                        this.fetch_a[i].female = this.results[0].data.enrollment.entrants.data[j].female
                    }
                   
                            
                }
                

            }

            


            ///////OPERTATIONS FOR STREAMS////
            var array_streams = [];
            for (var i = 0; i < this.results[0].data.enrollment.year_by_age.data.stream.length; i++) {
                var arr = {};
                for (var j = 0; j < this.results[0].data.enrollment.year_by_age.data.stream.length; j++) {




                    arr["class"] = this.results[0].data.enrollment.year_by_age.data.stream[i].class;
                    arr["stream"] = this.results[0].data.enrollment.year_by_age.data.stream[i].stream;
                    arr["multigrade"] = this.results[0].data.enrollment.year_by_age.data.stream[i].stream_with_multigrade;



                    //if(this.fetch_birth_certificate[i] == this.results[0].data.enrollment.birth_certificates.data[0])
                }
                array_streams.push(arr);

            }
            this.fetch_streams = array_streams;
            if(isEmpty(this.fetch_streams)) {
                if(this.loeo_choice.indexOf("Senior") === -1) 
                for (var i = 0; i < 3; i++) {
                    var arr = {};
                    for (var j = 0; j < 3; j++) {
    
    
    
    
                        arr["class"] = "SS"+(i+1);
                        arr["stream"] = "";
                        arr["multigrade"] = "";
    
    
    
                        //if(this.fetch_birth_certificate[i] == this.results[0].data.enrollment.birth_certificates.data[0])
                    }
                    array_streams.push(arr);
    
                } else {
                    for (var i = 0; i < 3; i++) {
                        var arr = {};
                        for (var j = 0; j < 3; j++) {
        
        
        
        
                            arr["class"] = "JS"+(i+1);
                            arr["stream"] = "";
                            arr["multigrade"] = "";
        
        
        
                            //if(this.fetch_birth_certificate[i] == this.results[0].data.enrollment.birth_certificates.data[0])
                        }
                        array_streams.push(arr);
                    }
                }
            }
            //alert(JSON.stringify(this.fetch_streams, null, 0));

            var classes = ['Jss1','Jss2', 'Jss3'];
            var calssesS = ['SSS1','SSS2', 'SSS3'];

            ///////OPERTATIONS FOR YEAR BY AGE////
            var array_classes = [];
            for (var i = 0; i < classes.length*3; i++) {
                var arr = {};
                for (var j = 0; j < classes.length*3; j++) {




                    arr["class"] = classes[i];
                    arr["age_category"] = "";
                    arr["male"] = "";
                    arr["female"] = "";



                    //if(this.fetch_birth_certificate[i] == this.results[0].data.enrollment.birth_certificates.data[0])
                }
                array_classes.push(arr);

            }
            //this.fetch_year_by_age = array_year_by_age;

            /*var array_year_by_age = [];
            for (var i = 0; i < array_classes.length; i++) {
                var arr = {};
                for (var j = 0; j < this.results[0].data.enrollment.year_by_age.data.age.length; j++) {


                    if(array_classes[i] == this.results[0].data.enrollment.year_by_age.data.age[j].class ) {
                        array_classes.age_category = this.results[0].data.enrollment.year_by_age.data.age[i].age_category;
                        array_classes.male = this.results[0].data.enrollment.year_by_age.data.age[i].male;
                        array_classes.female = this.results[0].data.enrollment.year_by_age.data.age[i].female;
                    }

                    /*arr["class"] = this.results[0].data.enrollment.year_by_age.data.age[i].class;
                    arr["age_category"] = this.results[0].data.enrollment.year_by_age.data.age[i].age_category;
                    arr["male"] = this.results[0].data.enrollment.year_by_age.data.age[i].male;
                    arr["female"] = this.results[0].data.enrollment.year_by_age.data.age[i].female;
                    


                    if(this.fetch_birth_certificate[i] == this.results[0].data.enrollment.birth_certificates.data[0])
                }
                array_year_by_age.push(arr);

            }
            this.fetch_year_by_age = array_year_by_age;*/

            var array_age1 = []; var array_age2 = []; var array_age3 = [];
            count = 0 ;
            for (var i = 0; i < this.fetch_age.length; i++) {
                var arr1 = {}; var arr2 = {}; var arr3 = {};
                for (var j = 0; j < 15; j++) {

                    if (j <= 4) {

                        arr1["age"] = this.fetch_age[i];
                        if(this.loeo_choice.indexOf("Senior") !== -1){
                            arr1["class"] = "Sss1";
                        } else {
                            arr1["class"] = "Jss1";
                        }
                       
                        arr1["male"] = "";
                        arr1["female"] = "";

                        
                    } else if (j<=9) {

                        arr2["age"] = this.fetch_age[i];
                        if(this.loeo_choice.indexOf("Senior") !== -1){
                            arr2["class"] = "Sss2";
                        } else {
                            arr2["class"] = "Jss2";
                        }
                        
                        arr2["male"] = ""
                        arr2["female"] = ""


                    } else if (j<=14) {

                        arr3["age"] = this.fetch_age[i];
                        if(this.loeo_choice.indexOf("Senior") !== -1){
                            arr3["class"] = "Sss3";
                        } else {
                            arr3["class"] = "Jss3";
                        }
                       
                        arr3["male"] = ""
                        arr3["female"] = ""


                    }
                    

                }
                
                array_age1.push(arr1);
                array_age2.push(arr2);
                array_age3.push(arr3);


            }

            for(var i=0; i<array_age1.length; i++) {
                for (var j = 0; j < this.results[0].data.enrollment.year_by_age.data.age.length; j++) {
                    if (array_age1[i].age == this.results[0].data.enrollment.year_by_age.data.age[j].age_category && this.results[0].data.enrollment.year_by_age.data.age[j].class == array_age1[i].class) {

                       
                        array_age1[i].male = this.results[0].data.enrollment.year_by_age.data.age[j].male;
                        array_age1[i].female = this.results[0].data.enrollment.year_by_age.data.age[j].female;


                    } 
                }
            }

            for(var i=0; i<array_age2.length; i++) {
                for (var j = 0; j < this.results[0].data.enrollment.year_by_age.data.age.length; j++) {
                    if (array_age2[i].age == this.results[0].data.enrollment.year_by_age.data.age[j].age_category && this.results[0].data.enrollment.year_by_age.data.age[j].class == array_age2[i].class) {

                       
                        array_age2[i].male = this.results[0].data.enrollment.year_by_age.data.age[j].male;
                        array_age2[i].female = this.results[0].data.enrollment.year_by_age.data.age[j].female;


                    } 
                }
            }

            for(var i=0; i<array_age3.length; i++) {
                for (var j = 0; j < this.results[0].data.enrollment.year_by_age.data.age.length; j++) {
                    if (array_age3[i].age == this.results[0].data.enrollment.year_by_age.data.age[j].age_category && this.results[0].data.enrollment.year_by_age.data.age[j].class == array_age3[i].class) {

                       ///alert(true);
                        array_age3[i].male = this.results[0].data.enrollment.year_by_age.data.age[j].male;
                        array_age3[i].female = this.results[0].data.enrollment.year_by_age.data.age[j].female;


                    } 
                }
            }
            
           
            

            //console.log(JSON.stringify(array_age3, null, 2));

            this.fetch_y_b_a1 = array_age1.filter(value => Object.keys(value).length !== 0);
            this.fetch_y_b_a2 = array_age2.filter(value => Object.keys(value).length !== 0);
            this.fetch_y_b_a3 = array_age3.filter(value => Object.keys(value).length !== 0);

            //alert(JSON.stringify(this.fetch_y_b_a1, null, 0));

            /* var array_age1 = []; var array_age2 = []; var array_age3 = [];
             for (var i = 0; i < this.results[0].data.enrollment.entrants.data.length; i++) {
                 var arr1 = {}; var arr2 = {}; var arr3 = {};
                 for (var j = 0; j < this.results[0].data.enrollment.entrants.data.length; j++) {
 
 
 
                     //alert(this.results[0].data.enrollment.entrants.data[j].age_category);
                     if (this.fetch_age[i] == this.results[0].data.enrollment.year_by_age.data.age[i].age_category) {
                         //alert("equals");
                         
                         if(this.results[0].data.enrollment.year_by_age.data.age[j].class == "Jss1" ) {
                             arr1["age"] = this.fetch_age[i];
                             arr1["class"] = this.results[0].data.enrollment.year_by_age.data.age[j].class;
                             arr1["male"] = "";
                             arr1["female"] = "";
                         } else if(this.results[0].data.enrollment.year_by_age.data.age[j].class == "Jss2" ) {
 
                         }
                         
 
                     } 
     
 
 
 
                     //if(this.fetch_birth_certificate[i] == this.results[0].data.enrollment.birth_certificates.data[0])
                 }
                 array_age1.push(arr);
 
             }
             this.fetch_y_b_a_no = array_age;*/
            //alert(JSON.stringify(this.fetch_y_b_a1, null, 0));
            //alert(JSON.stringify(this.fetch_y_b_a2, null, 0));
            //alert(JSON.stringify(this.fetch_y_b_a3, null, 0));


            var fetch_all_ages_1 = []
            for (var i = 0; i < this.fetch_age.length; i++) {
                var arr = {}
                for (var j = 0; j < this.fetch_y_b_a1.length; j++) {
                    //var inner_arr = {}
                    //inner_arr[this.fetch_age[i]] = this.fetch_age[i];
                    if (this.fetch_age[i] == this.fetch_y_b_a1[j].age) {
                        arr["ages"] = this.fetch_age[i];
                        if (this.fetch_y_b_a1[j].class = "Jss1") {
                            arr["age_male_1"] = this.fetch_y_b_a1[j].male;
                            arr["age_female_1"] = this.fetch_y_b_a1[j].female;
                        }



                    }
                }

                fetch_all_ages_1.push(arr);
            }
            var fetch_all_ages_2 = []
            for (var i = 0; i < this.fetch_age.length; i++) {
                var arr = {}
                for (var j = 0; j < this.fetch_y_b_a2.length; j++) {
                    //var inner_arr = {}
                    //inner_arr[this.fetch_age[i]] = this.fetch_age[i];
                    if (this.fetch_age[i] == this.fetch_y_b_a2[j].age) {
                        arr["ages"] = this.fetch_age[i];
                        arr["age_male_2"] = this.fetch_y_b_a2[j].male;
                        arr["age_female_2"] = this.fetch_y_b_a2[j].female;
                    }
                }

                fetch_all_ages_2.push(arr);
            }
            var fetch_all_ages_3 = []
            for (var i = 0; i < this.fetch_age.length; i++) {
                var arr = {}
                for (var j = 0; j < this.fetch_y_b_a3.length; j++) {
                    //var inner_arr = {}
                    //inner_arr[this.fetch_age[i]] = this.fetch_age[i];
                    if (this.fetch_age[i] == this.fetch_y_b_a3[j].age) {
                        arr["ages"] = this.fetch_age[i];
                        arr["age_male_3"] = this.fetch_y_b_a3[j].male;
                        arr["age_female_3"] = this.fetch_y_b_a3[j].female;
                    }
                }

                fetch_all_ages_3.push(arr);
            }
            this.fetch_all_ages = merge(fetch_all_ages_1, fetch_all_ages_2)
            this.fetch_all_ages = merge(this.fetch_all_ages, fetch_all_ages_3)

            //alert(JSON.stringify( this.fetch_all_ages, null, 0));

            ///////OPERTATIONS FOR AGES////
            var array_repeat_male = [];

            for (var i = 0; i < this.results[0].data.enrollment.year_by_age.data.repeater.length; i++) {


                if ('male' in this.results[0].data.enrollment.year_by_age.data.repeater[i]) {
                    array_repeat_male[i] = this.results[0].data.enrollment.year_by_age.data.repeater[i].male;
                }

            }
            var array_repeat_female = [];
            for (var i = 0; i < this.results[0].data.enrollment.year_by_age.data.repeater.length; i++) {


                if ('female' in this.results[0].data.enrollment.year_by_age.data.repeater[i]) {
                    array_repeat_female[i] = this.results[0].data.enrollment.year_by_age.data.repeater[i].female;
                }

            }


            var repeaters = array_repeat_male.concat(array_repeat_female);

            this.fetch_repeaters[0] = repeaters[0]
            this.fetch_repeaters[1] = repeaters[3]
            this.fetch_repeaters[2] = repeaters[1]
            this.fetch_repeaters[3] = repeaters[4]
            this.fetch_repeaters[4] = repeaters[2]
            this.fetch_repeaters[5] = repeaters[5]

            for(var i=0; i<this.fetch_repeaters.length; i++) {
                var arr={}
                for(var j=0; j<this.fetch_repeaters.length; j++) {
                    arr["repeaters"] = this.fetch_repeaters[i];
                }
                this.fetch_r.push(arr);
            }

            var array_dropout_male = [];

            for (var i = 0; i < this.results[0].data.enrollment.pupil_flow.data.length; i++) {


                if ('male' in this.results[0].data.enrollment.pupil_flow.data[i] && this.results[0].data.enrollment.pupil_flow.data[i].flow_item == "Dropout") {
                    array_dropout_male[i] = this.results[0].data.enrollment.pupil_flow.data[i].male;

                }

            }
            var array_dropout_female = [];
            for (var i = 0; i < this.results[0].data.enrollment.pupil_flow.data.length; i++) {


                if ('female' in this.results[0].data.enrollment.pupil_flow.data[i] && this.results[0].data.enrollment.pupil_flow.data[i].flow_item == "Dropout") {
                    array_dropout_female[i] = this.results[0].data.enrollment.pupil_flow.data[i].female;
                }

            }

            var dropout = array_dropout_male.concat(array_dropout_female);
            //alert(JSON.stringify( dropout, null, 0));

            this.fetch_dropouts[0] = dropout[0]
            this.fetch_dropouts[1] = dropout[1]
            this.fetch_dropouts[2] = dropout[3]
            this.fetch_dropouts[3] = dropout[4]
            this.fetch_dropouts[4] = dropout[2]
            this.fetch_dropouts[5] = dropout[5]

            for(var i=0; i<this.fetch_dropouts.length; i++) {
                var arr={}
                for(var j=0; j<this.fetch_dropouts.length; j++) {
                    arr["dropouts"] = this.fetch_dropouts[i];
                }
                this.fetch_drop.push(arr);
            }

            var array_transfer_in_male = [];

            for (var i = 0; i < this.results[0].data.enrollment.pupil_flow.data.length; i++) {


                if ('male' in this.results[0].data.enrollment.pupil_flow.data[i] && this.results[0].data.enrollment.pupil_flow.data[i].flow_item == "Transfer In") {
                    array_transfer_in_male[i] = this.results[0].data.enrollment.pupil_flow.data[i].male;

                }

            }

            var array_transfer_in_female = [];
            for (var i = 0; i < this.results[0].data.enrollment.pupil_flow.data.length; i++) {


                if ('female' in this.results[0].data.enrollment.pupil_flow.data[i] && this.results[0].data.enrollment.pupil_flow.data[i].flow_item == "Transfer In") {
                    array_transfer_in_female[i] = this.results[0].data.enrollment.pupil_flow.data[i].female;
                }

            }

            var transfer_in = array_transfer_in_male.concat(array_transfer_in_female);
           
            

            this.fetch_transfer_in[0] = array_transfer_in_male[3]
            this.fetch_transfer_in[1] = array_transfer_in_female[3]
            this.fetch_transfer_in[2] = array_transfer_in_male[4]
            this.fetch_transfer_in[3] = array_transfer_in_female[4]
            this.fetch_transfer_in[4] = array_transfer_in_male[5]
            this.fetch_transfer_in[5] = array_transfer_in_female[5]

            for(var i=0; i<this.fetch_transfer_in.length; i++) {
                var arr={}
                for(var j=0; j<this.fetch_transfer_in.length; j++) {
                    arr["transfer"] = this.fetch_transfer_in[i];
                }
                this.fetch_t_in.push(arr);
            }

            var array_transfer_out_male = [];

            for (var i = 0; i < this.results[0].data.enrollment.pupil_flow.data.length; i++) {


                if ('male' in this.results[0].data.enrollment.pupil_flow.data[i] && this.results[0].data.enrollment.pupil_flow.data[i].flow_item == "Transfer Out") {
                    array_transfer_out_male[i] = this.results[0].data.enrollment.pupil_flow.data[i].male;

                }

            }
            var array_transfer_out_female = [];
            for (var i = 0; i < this.results[0].data.enrollment.pupil_flow.data.length; i++) {


                if ('female' in this.results[0].data.enrollment.pupil_flow.data[i] && this.results[0].data.enrollment.pupil_flow.data[i].flow_item == "Transfer Out") {
                    array_transfer_out_female[i] = this.results[0].data.enrollment.pupil_flow.data[i].female;
                }

            }

            var transfer_out = array_transfer_out_male.concat(array_transfer_out_female);
            //alert(JSON.stringify( transfer_out, null, 0));

            this.fetch_transfer_out[0] = array_transfer_out_male[6]
            this.fetch_transfer_out[1] = array_transfer_out_female[6]
            this.fetch_transfer_out[2] = array_transfer_out_male[7]
            this.fetch_transfer_out[3] = array_transfer_out_female[7]
            this.fetch_transfer_out[4] = array_transfer_out_male[8]
            this.fetch_transfer_out[5] = array_transfer_out_female[8]

            
            for(var i=0; i<this.fetch_transfer_out.length; i++) {
                var arr={}
                for(var j=0; j<this.fetch_transfer_out.length; j++) {
                    arr["transfer"] = this.fetch_transfer_out[i];
                }
                this.fetch_t_out.push(arr);
            }

            var array_promoted_male = [];

            for (var i = 0; i < this.results[0].data.enrollment.pupil_flow.data.length; i++) {


                if ('male' in this.results[0].data.enrollment.pupil_flow.data[i] && this.results[0].data.enrollment.pupil_flow.data[i].flow_item == "Promoted") {
                    array_promoted_male[i] = this.results[0].data.enrollment.pupil_flow.data[i].male;

                }

            }
            var array_promoted_female = [];
            for (var i = 0; i < this.results[0].data.enrollment.pupil_flow.data.length; i++) {


                if ('female' in this.results[0].data.enrollment.pupil_flow.data[i] && this.results[0].data.enrollment.pupil_flow.data[i].flow_item == "Promoted") {
                    array_promoted_female[i] = this.results[0].data.enrollment.pupil_flow.data[i].female;
                }

            }

            var promoted = array_promoted_male.concat(array_promoted_female);
            //alert(JSON.stringify( promoted, null, 0));

            this.fetch_promoted[0] = array_promoted_male[9]
            this.fetch_promoted[1] = array_promoted_female[9]
            this.fetch_promoted[2] = array_promoted_male[10]
            this.fetch_promoted[3] = array_promoted_female[10]
            this.fetch_promoted[4] = array_promoted_male[11]
            this.fetch_promoted[5] = array_promoted_female[11]

            //alert(JSON.stringify( array_promoted_male , null, 0));
            //this.fetch_transfer = transfer_in;
            //alert(JSON.stringify(array_promoted_female , null, 0));


            for(var i=0; i<this.fetch_promoted.length; i++) {
                var arr={}
                for(var j=0; j<this.fetch_promoted.length; j++) {
                    arr["promoted"] = this.fetch_promoted[i];
                }
                this.fetch_pro.push(arr);
            }


            //this.fetch_dropouts = dropout;
            // alert(JSON.stringify( this.fetch_promoted, null, 0));

            ///FETCH SPECIAL NEEDS
            ///////OPERTATIONS FOR YEAR BY AGE////
            var array_special_needs = [];
            for (var i = 0; i < classes.length*3; i++) {
                var arr = {};
                for (var j = 0; j < classes.length*3; j++) {




                    arr["class"] = classes[i];
                    arr["special_need_item"] = "";
                    arr["male"] = "";
                    arr["female"] = "";



                    //if(this.fetch_birth_certificate[i] == this.results[0].data.enrollment.birth_certificates.data[0])
                }
                array_special_needs.push(arr);

            }
            this.fetch_special_needs = array_special_needs;
            //alert(JSON.stringify(this.fetch_special_needs, null, 0));

            var array_special_needs1 = []; var array_special_needs2 = []; var array_special_needs3 = [];
            for (var i = 0; i < this.fetch_special_needs_item.length; i++) {
                var arr1 = {}; var arr2 = {}; var arr3 = {};
                for (var j = 0; j < 15; j++) {

                    
                    if (j<=4) {

                        arr1["special_need_item"] = this.fetch_special_needs_item[i];    
                        //alert(arr1["special_need_item"]);
                        if(this.loeo_choice.indexOf("Senior") !== -1){
                            arr1["class"] = "Sss1";
                        } else {
                            arr1["class"] = "Jss1";
                        }
                        //arr1["class"] = this.results[0].data.enrollment.special_need.data[j].class;
                        arr1["male"] = "";
                        arr1["female"] = "";


                    }else if (j<=9) {

                        arr2["special_need_item"] = this.fetch_special_needs_item[i];
                        if(this.loeo_choice.indexOf("Senior") !== -1){
                            arr2["class"] = "Sss2";
                        } else {
                            arr2["class"] = "Jss2";
                        }
                        //alert(arr1["special_need_item"]);
                        //arr2["class"] = this.results[0].data.enrollment.special_need.data[j].class;
                        //;
                        arr2["male"] = "";
                        arr2["female"] = "";
                        //alert(arr2["male"] + "ma");

                    }else if (j <= 15) {

                        arr3["special_need_item"] = this.fetch_special_needs_item[i];
                        if(this.loeo_choice.indexOf("Senior") !== -1){
                            arr3["class"] = "Sss3";
                        } else {
                            arr3["class"] = "Jss3";
                        }
                        //alert(arr1["special_need_item"]);
                        //arr3["class"] = this.results[0].data.enrollment.special_need.data[j].class;
                        arr3["male"] = "";
                        arr3["female"] = "";


                    }
                }
                //alert(JSON.stringify(arr1, null, 0))
                //this.fetch_s_n1.push(arr1)
                array_special_needs1.push(arr1);
                array_special_needs2.push(arr2);
                array_special_needs3.push(arr3);



            }
            console.log(JSON.stringify(array_special_needs1, null, 2))


           
            //alert(JSON.stringify("1: " + array_special_needs1, null, 0));
            /*alert(JSON.stringify(this.fetch_s_n1, null, 0));
            alert(JSON.stringify(this.fetch_s_n2, null, 0));
            alert(JSON.stringify(this.fetch_s_n3, null, 0));*/


            /*var array_special_needs1 = []; var array_special_needs2 = []; var array_special_needs3 = [];

            for (var i = 0; i < this.results[0].data.enrollment.entrants.data.length; i++) {
                var arr1 = {}; var arr2 = {}; var arr3 = {};
                for (var j = 0; j < this.results[0].data.enrollment.entrants.data.length; j++) {
 
 
 
                    //alert(this.results[0].data.enrollment.entrants.data[j].age_category);
                    if (this.fetch_age[i] == this.results[0].data.enrollment.year_by_age.data.age[i].age_category) {
                        //alert("equals");
                        
                        if(this.results[0].data.enrollment.year_by_age.data.age[j].class == "Jss1" ) {
                            arr1["age"] = this.fetch_age[i];
                            arr1["class"] = this.results[0].data.enrollment.year_by_age.data.age[j].class;
                            arr1["male"] = "";
                            arr1["female"] = "";
                        } else if(this.results[0].data.enrollment.year_by_age.data.age[j].class == "Jss2" ) {
 
                        }
                        
 
                    } 
 
 
 
 
                    //if(this.fetch_birth_certificate[i] == this.results[0].data.enrollment.birth_certificates.data[0])
                }
                array_age1.push(arr);
 
            }
            this.fetch_y_b_a_no = array_age;*/
            //alert(JSON.stringify(this.fetch_y_b_a1, null, 0));
            //alert(JSON.stringify(this.fetch_y_b_a2, null, 0));
            //alert(JSON.stringify(this.fetch_y_b_a3, null, 0));

            for(var i=0; i<array_special_needs1.length; i++) {
                for (var j = 0; j < this.results[0].data.enrollment.special_need.data.length; j++) {
                    if (array_special_needs1[i].special_need_item == this.results[0].data.enrollment.special_need.data[j].special_need_item && this.results[0].data.enrollment.special_need.data[j].class == array_special_needs1[i].class) {

                       
                        array_special_needs1[i].male = this.results[0].data.enrollment.special_need.data[j].male;
                        array_special_needs1[i].female = this.results[0].data.enrollment.special_need.data[j].female;


                    } 
                }
            }

            for(var i=0; i<array_special_needs2.length; i++) {
                for (var j = 0; j < this.results[0].data.enrollment.special_need.data.length; j++) {
                    if (array_special_needs2[i].special_need_item == this.results[0].data.enrollment.special_need.data[j].special_need_item && this.results[0].data.enrollment.special_need.data[j].class == array_special_needs2[i].class) {

                       
                        array_special_needs2[i].male = this.results[0].data.enrollment.special_need.data[j].male;
                        array_special_needs2[i].female = this.results[0].data.enrollment.special_need.data[j].female;


                    } 
                }
            }

            for(var i=0; i<array_special_needs3.length; i++) {
                for (var j = 0; j < this.results[0].data.enrollment.special_need.data.length; j++) {
                    if (array_special_needs3[i].special_need_item == this.results[0].data.enrollment.special_need.data[j].special_need_item && this.results[0].data.enrollment.special_need.data[j].class == array_special_needs3[i].class) {

                       ///alert(true);
                       array_special_needs3[i].male = this.results[0].data.enrollment.special_need.data[j].male;
                       array_special_needs3[i].female = this.results[0].data.enrollment.special_need.data[j].female;


                    } 
                }
            }

            this.fetch_s_n1 = array_special_needs1.filter(value => Object.keys(value).length !== 0);
            this.fetch_s_n2 = array_special_needs2.filter(value => Object.keys(value).length !== 0);
            this.fetch_s_n3 = array_special_needs3.filter(value => Object.keys(value).length !== 0);

            console.log(JSON.stringify(this.fetch_s_n1, null, 2))
            


            var fetch_all_special_needs_1 = []
            for (var i = 0; i < this.fetch_special_needs_item.length; i++) {
                var arr = {}
                for (var j = 0; j < this.fetch_s_n1.length; j++) {
                    //var inner_arr = {}
                    //inner_arr[this.fetch_age[i]] = this.fetch_age[i];
                    if (this.fetch_special_needs_item[i] == this.fetch_s_n1[j].special_need_item) {
                        arr["special_need_item"] = this.fetch_special_needs_item[i];
                        if (this.fetch_s_n1[j].class = "Jss1") {
                            arr["special_need_item_male_1"] = this.fetch_s_n1[j].male;
                            arr["special_need_item_female_1"] = this.fetch_s_n1[j].female;
                        }



                    }
                }

                fetch_all_special_needs_1.push(arr);
            }

            var fetch_all_special_needs_2 = []
            for (var i = 0; i < this.fetch_special_needs_item.length; i++) {
                var arr = {}
                for (var j = 0; j < this.fetch_s_n2.length; j++) {
                    //var inner_arr = {}
                    //inner_arr[this.fetch_age[i]] = this.fetch_age[i];
                    if (this.fetch_special_needs_item[i] == this.fetch_s_n2[j].special_need_item) {
                        arr["special_need_item"] = this.fetch_special_needs_item[i];
                        if (this.fetch_s_n2[j].class = "Jss2") {
                            arr["special_need_item_male_2"] = this.fetch_s_n2[j].male;
                            arr["special_need_item_female_2"] = this.fetch_s_n2[j].female;
                        }



                    }
                }

                fetch_all_special_needs_2.push(arr);
            }
            var fetch_all_special_needs_3 = []
            for (var i = 0; i < this.fetch_special_needs_item.length; i++) {
                var arr = {}
                for (var j = 0; j < this.fetch_s_n3.length; j++) {
                    //var inner_arr = {}
                    //inner_arr[this.fetch_age[i]] = this.fetch_age[i];
                    if (this.fetch_special_needs_item[i] == this.fetch_s_n3[j].special_need_item) {
                        arr["special_need_item"] = this.fetch_special_needs_item[i];
                        if (this.fetch_s_n2[j].class = "Jss3") {
                            arr["special_need_item_male_3"] = this.fetch_s_n3[j].male;
                            arr["special_need_item_female_3"] = this.fetch_s_n3[j].female;
                        }



                    }
                }

                fetch_all_special_needs_3.push(arr);
            }
            this.fetch_all_special_needs = merge(fetch_all_special_needs_1, fetch_all_special_needs_2)
            this.fetch_all_special_needs = merge(this.fetch_all_special_needs, fetch_all_special_needs_3)

            //alert(JSON.stringify(this.fetch_all_special_needs, null, 0));









            //for (var i = 0; i < this.results.data.length; i++) {
            this.schoolname = this.results[0].data.school_identification.school_name;

            this.schoolstreet = this.results[0].data.school_identification.address;
            this.schoolemail = this.results[0].data.school_identification.email_address;
            this.schooltown = this.results[0].data.school_identification.town;
            this.schoolward = this.results[0].data.school_identification.ward;
            this.schooltelephone = this.results[0].data.school_identification.school_telephone;
            this.schoolstate = this.results[0].data.school_identification.state;
            this.state_picked = this.results[0].data.school_identification.state;

            this.schoollga = this.results[0].data.school_identification.lga;
            this.lga_picked = this.results[0].data.school_identification.lga;

            this.counter_state = this.state_index[this.state_picked];

            this.year = this.results[0].data.school_characteristics.year_of_establishment;
            this.location_choice = this.results[0].data.school_characteristics.location;
            this.loeo_choice = this.results[0].data.school_characteristics.levels_of_education_offered;
            this.loeo2_choice = this.results[0].data.school_characteristics.type_of_school;

            this.shifts_choice = this.results[0].data.school_characteristics.shifts;
            //alert(this.shifts_choice)
            if (this.shifts_choice == "1") {
                this.shifts_choice = "Yes";
                this.shifts_choice_bi = "1";
            } else {
                this.shifts_choice = "No";
                this.shifts_choice_bi = "0";
            }


            this.facilities_choice = this.results[0].data.school_characteristics.shared_facilities;
            if (this.facilities_choice == "0") {
                this.facilities_choice = "No";
                this.facilities_choice_bi = "0";
            } else {
                this.facilities_choice = "Yes";
                this.facilities_choice_bi = "1";
                //this.facilities_choice_yes = this.results[0].data.school_characteristics.shared_facilities;
            }
            this.facilities_choice_yes = this.results[0].data.school_characteristics.sharing_with;//no
            this.multigrade_choice = this.results[0].data.school_characteristics.multi_grade_teaching;//no
            if (this.multigrade_choice == "1") {
                this.multigrade_choice = "Yes";
                this.multigrade_choice_bi = "1";
            } else if (this.multigrade_choice == "0") {
                this.multigrade_choice = "No";
                this.multigrade_choice_bi = "0";
            } else {

            }

            this.average_distance = this.results[0].data.school_characteristics.school_average_distance_from_catchment_communities;
            this.student_distance = this.results[0].data.school_characteristics.students_distance_from_school;
            this.students_boarding_male = this.results[0].data.school_characteristics.students_boarding.male;
            this.students_boarding_female = this.results[0].data.school_characteristics.students_boarding.female;
            this.sdp_choice = this.results[0].data.school_characteristics.school_development_plan_sdp;
            this.year_of_establishment = this.results[0].data.school_characteristics.year_of_establishment;
            if (this.sdp_choice == "1") {
                this.sdp_choice = "Yes";
                this.sdp_choice_bi = "1";
            } else if (this.sdp_choice == "0") {
                this.sdp_choice = "No";
                this.sdp_choice_bi = "0";
            }
            this.sbmc_choice = this.results[0].data.school_characteristics.school_based_management_committee_sbmc;
            if (this.sbmc_choice == "1") {
                this.sbmc_choice = "Yes";
                this.sbmc_choice_bi = "1";
            } else if (this.sbmc_choice == "0"){
                this.sbmc_choice = "No";
                this.sbmc_choice_bi = "0";
            }
            this.pta_choice = this.results[0].data.school_characteristics.parents_teachers_association_pta;
            if (this.pta_choice == "1") {
                this.pta_choice = "Yes";
                this.pta_choice_bi = "1";
            } else if (this.pta_choice == "0") {
                this.pta_choice = "No";
                this.pta_choice_bi = "0";
            }
            this.date_inspection = this.results[0].data.school_characteristics.date_of_last_inspection_visit;
            this.authority_choice = this.results[0].data.school_characteristics.authority_of_last_inspection;
            this.cash_transfer = this.results[0].data.school_characteristics.conditional_cash_transfer;
            this.grants_choice = this.results[0].data.school_characteristics.school_grants;
            if (this.grants_choice == "1") {
                this.grants_choice = "Yes";
                this.grants_choice_bi = "1";
            } else if (this.grants_choice == "0") {
                this.grants_choice = "No";
                this.grants_choice_bi = "0";
            }

            this.guard_choice = this.results[0].data.school_characteristics.security_guard;
            if (this.guard_choice == "1") {
                this.guard_choice = "Yes";
                this.guard_choice_bi = "1";
            } else if (this.guard_choice == "0") {
                this.guard_choice = "No";
                this.guard_choice_bi = "0";
            }
            this.ownership_choice = this.results[0].data.school_characteristics.ownership;





            //}


            var namesArr = [];
            var descsArr = [];


            ///ENROLLMENT
            /**this.nPopC_male = this.results[0].data.enrollment.birth_certificates.data[0].male;
            this.nPopC_female = this.results[0].data.enrollment.birth_certificates.data[0].female;
            this.others_male = this.results[0].data.enrollment.birth_certificates.data[1].male;
            this.others_female = this.results[0].data.enrollment.birth_certificates.data[1].female;*/

            this.registered_male = this.results[0].data.enrollment.jsce.data[0].registered_male;
            this.registered_female = this.results[0].data.enrollment.jsce.data[0].registered_female;
            this.registered_total = this.results[0].data.enrollment.jsce.data[0].registered_total;
            this.took_part_male = this.results[0].data.enrollment.jsce.data[0].took_part_male;
            this.took_part_female = this.results[0].data.enrollment.jsce.data[0].took_part_female;
            this.took_part_total = this.results[0].data.enrollment.jsce.data[0].took_part_total;
            this.passed_male = this.results[0].data.enrollment.jsce.data[0].passed_male;
            this.passed_female = this.results[0].data.enrollment.jsce.data[0].passed_female;
            this.passed_total = this.results[0].data.enrollment.jsce.data[0].passed_total;

            //alert(n_male);



            if(this.loeo_choice.indexOf("Senior") === -1) {
                this.school_class = "jss"
            }


        });

    },

    methods: {
        
        saveIdentification: function () {
            if (this.schoolname != '' && this.schoolstreet != '' && this.schoollga != '' &&
                this.schoolstate != '' && this.schoolward != '' && this.schoolemail != '' &&
                this.schooltown != '' && this.schooltelephone != '') {
                this.counter_state = this.state_index[this.state_picked];
                this.get_state_code = this.fetch_states[this.counter_state].code
                //alert(this.get_state_code);
                this.counter_lga = this.lga_index[this.lga_picked];
                this.get_lga_code = this.fetch_lga[this.counter_lga].code;
                //alert(this.get_lga_code);
                axios.post("http://127.0.0.1:8000/api/Section/SchoolIdentification",
                    {
                        school_code: schoolcode.toLowerCase(),
                        school_name: this.schoolname,
                        school_address: this.schoolstreet,
                        town: this.schooltown,
                        ward: this.schoolward,
                        lga: this.get_lga_code,
                        state: this.get_state_code,
                        telephone: this.schooltelephone,
                        email: this.schoolemail
                    }).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            alert("Data Saved to DB");
                        } else {
                            alert("Check the form for the required data");
                        }
                    })
            }
        },

        nextCharacteristics: function () {
            console.log(this.loeo2_choice + " " + this.loeo_choice);
            alert(this.shifts_choice + " " + this.facilities_choice);
        },

        getLGA: function () {
            var arrayOfObjects2 = [];

            this.counter_state = this.state_index[this.state_picked];


            for (var i = 0; i < this.results[1].attributes.states[this.counter_state].data.lgas.length; i++) {
                var obj2 = {};
                for (var j = 0; j < this.results[1].attributes.states[this.counter_state].data.lgas.length; j++) {
                    //obj[keys[j]] = values[i][j];
                    obj2["name"] = this.results[1].attributes.states[this.counter_state].data.lgas[i].data.name;
                    obj2["code"] = this.results[1].attributes.states[this.counter_state].data.lgas[i].data.lgacode;
                    this.lga_index[this.results[1].attributes.states[this.counter_state].data.lgas[i].data.name] = i;
                }

                arrayOfObjects2.push(obj2);
            }
            //this.lga_picked=this.results[1].attributes.states[this.counter_state].data.lgas[0].data.name;
            this.fetch_lga = arrayOfObjects2;


            console.log('now: ', JSON.stringify(this.fetch_states, null, " "));
            //alert(this.lga_picked);
        },

        saveCharacteristics: function () {


            /* if(this.year != '' && this.facilities_choice != '' && this.facilities_choice_yes != '' &&
             this.multigrade_choice != '' && this.average_distance != '' && this.student_distance != '' &&
             this.students_boarding_female != '' && this.students_boarding_male != '' &&
             this.sdp_choice != '' && this.sbmc_choice != '' &&
             this.pta_choice != '' && this.date_inspection != '' &&
             this.authority_choice != '' && this.cash_transfer != '' &&
             this.students_grants_choice != '' && this.students_guard_choice != '' &&
             this.ownership_choice != '') {*/

            if (this.shifts_choice == "Yes") {
                this.shifts_choice_bi = "1";
            } else if (this.shifts_choice == "No") {
                this.shifts_choice_bi = "0";
            } else {

            }

            if (this.facilities_choice == "Yes") {
                this.facilities_choice_bi = "1";
            } else if (this.facilities_choice == "No") {
                this.facilities_choice_bi = "0";
                //this.facilities_choice_yes = this.results[0].data.school_characteristics.shared_facilities;
            } else {

            }

            if (this.multigrade_choice == "Yes") {
                this.multigrade_choice_bi = "1";
            } else if (this.multigrade_choice == "No") {
                this.multigrade_choice_bi = "0";
            } else {

            }

            if (this.sdp_choice == "Yes") {

                this.sdp_choice_bi = "1";
            } else if (this.sdp_choice == "No") {
                this.sdp_choice_bi = "0";
            } else {

            }

            if (this.sbmc_choice == "Yes") {
                this.sbmc_choice_bi = "1";
            } else if (this.sbmc_choice == "No") {
                this.sbmc_choice_bi = "0";
            } else {

            }

            if (this.pta_choice == "Yes") {
                this.pta_choice_bi = "1";
            } else if (this.pta_choice == "No") {
                this.pta_choice_bi = "0";
            } else {

            }

            if (this.grants_choice == "Yes") {
                this.grants_choice_bi = "1";
            } else if (this.grants_choice == "No") {
                this.grants_choice_bi = "0";
            } else {

            }

            if (this.guard_choice == "Yes") {
                this.guard_choice_bi = "1";
            } else if (this.guard_choice == "No") {
                this.guard_choice_bi = "0";
            } else {

            }

            //alert(this.student_distance);


            axios.post("http://127.0.0.1:8000/api/Section/SchoolCharacteristics",
                {
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    shared_facilities: this.facilities_choice_bi,
                    schools_sharingwith: this.facilities_choice_yes,
                    location_type: this.location_choice,
                    school_type: this.loeo2_choice,
                    level_of_education: this.loeo_choice,
                    multi_grade_teaching: this.multigrade_choice_bi,
                    distance_from_catchment_area: this.average_distance,
                    students_travelling_3km: this.student_distance,
                    male_student_b: this.students_boarding_female,
                    female_student_b: this.students_boarding_male,
                    has_school_development_plan: this.sdp_choice_bi,
                    has_sbmc: this.sbmc_choice_bi,
                    has_pta: this.pta_choice_bi,
                    last_inspection_date: this.date_inspection,
                    last_inspection_authority: this.authority_choice,
                    conditional_cash_transfer: this.cash_transfer,
                    has_school_grant: this.grants_choice_bi,
                    has_security_guard: this.guard_choice_bi,
                    ownership: this.ownership_choice,
                }).then(response => {

                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        alert("Data Saved to DB");
                    } else {
                        alert("Check the form for the required data");
                    }
                })


        },

        saveBirthCertificates: function () {
           
            var arr = JSON.parse(JSON.stringify(this.fetch_b_c)) 
            var testArray = arr;

            var vals = [];
            for (var i = 0; i < testArray.length; i++) {
                vals.push(testArray[i].val);
            }
       
            var json = "{\"birthcertificate\":{" 
            for (var i = 0; i < testArray.length; i++) {
                var j = testArray[i];
                if(j.certificates == "National Population Commission") {
                    j.certificates = "npc";
                } else {

                }
                json += "\"" + j.certificates.toLowerCase() + "\":{"
                delete j['certificates'];
                var dq = '"';
                var last = Object.keys(j).length;
                var count = 0;
                for (x in j) {
                    json += dq + x + dq + ":" + dq + j[x] + dq;
                    count++;
                    if (count < last)
                        json += ",";
                }
                if(i != testArray.length-1) {
                    json += "},";
                } else {
                    json += "}"; 
                }
                
            }
            json += "}}";
 
            var jso = JSON.parse(json);
              
          

            axios.post("http://127.0.0.1:8000/api/Section/SchoolEnrollment/BirthCertificate",{
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class: "jss",
                birthcertificate_npc_male: jso.birthcertificate.npc.male,
                birthcertificate_npc_female: jso.birthcertificate.npc.female,
                birthcertificate_hospital_male: jso.birthcertificate.hospital.male,
                birthcertificate_hospital_female: jso.birthcertificate.hospital.female,
                birthcertificate_lga_male: jso.birthcertificate.lga.male,
                birthcertificate_lga_female: jso.birthcertificate.lga.female,
                birthcertificate_court_male: jso.birthcertificate.court.male,
                birthcertificate_court_female: jso.birthcertificate.court.female,
                birthcertificate_un_male: jso.birthcertificate.un.male,
                birthcertificate_un_female: jso.birthcertificate.un.female,
                birthcertificate_others_male: jso.birthcertificate.others.male,
                birthcertificate_others_female: jso.birthcertificate.others.female,
                
                
                
                
                
              
            }
            
                
            ).then(response => {

                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //this.fetch_b_c = this.fetch_b_c;
                        alert("Data Saved to DB");
                        //alert(JSON.stringify(this.fetch_b_c, null, 2));
            

                    } else if(this.resultsSave.status == "error") {
                        alert(response.data.message);
                    }else {
                        alert("Check the form for the required data");
                    }





                })


        },

        saveEntrants: function () {
           //alert(JSON.stringify(this.fetch_a))
           
            var arr = JSON.parse(JSON.stringify(this.fetch_a)) 
            var testArray = arr;

            var vals = [];
            for (var i = 0; i < testArray.length; i++) {
                vals.push(testArray[i].val);
            }
       
            var json = "{\"enrollment_entrant\":{" 
            for (var i = 0; i < testArray.length; i++) {
                var j = testArray[i];
                if(j.age == "12") {
                    j.age = "age_12";
                } else if(j.age == "13"){
                    j.age = "age_13"
                } else if(j.age == "Above 14 Years") {
                    j.age = "age_above14"
                } else if(j.age == "Below 12 Years"){
                    j.age = "age_below12"
                } else if(j.age == "14") {
                    j.age = "age_14"
                } else if(j.age == "others") {
                    j.age = "others"
                } else if(j.age == "17") {
                    j.age = "age_17";
                } else if(j.age == "16") {
                    j.age = "age_16";
                } else if(j.age == "15") {
                    j.age = "age_15";
                } else if(j.age == "below 15") {
                    j.age = "age_below15";
                } else if(j.age == "above 17") {
                    j.age = "age_above17";
                }

                json += "\"" + j.age.toLowerCase() + "\":{"
                delete j['age'];
                var dq = '"';
                var last = Object.keys(j).length;
                var count = 0;
                for (x in j) {
                    json += dq + x + dq + ":" + dq + j[x] + dq;
                    count++;
                    if (count < last)
                        json += ",";
                }
                if(i != testArray.length-1) {
                    json += "},";
                } else {
                    json += "}"; 
                }
                
            }
            json += "}}";
 
            
            var jso = JSON.parse(json);  
            
          

            axios.post("http://127.0.0.1:8000/api/Section/SchoolEnrollment/Entrant",{
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class: "jss",
                enrollment_entrant_age_12_male: jso.enrollment_entrant.age_12.male,
                enrollment_entrant_age_12_female: jso.enrollment_entrant.age_12.female,
                enrollment_entrant_age_13_male: jso.enrollment_entrant.age_13.male,
                enrollment_entrant_age_13_female: jso.enrollment_entrant.age_13.female,
                enrollment_entrant_age_14_male: jso.enrollment_entrant.age_14.male,
                enrollment_entrant_age_14_female: jso.enrollment_entrant.age_14.female,
                enrollment_entrant_age_above14_male: jso.enrollment_entrant.age_above14.male,
                enrollment_entrant_age_above14_female: jso.enrollment_entrant.age_above14.female,
                enrollment_entrant_age_below12_male: jso.enrollment_entrant.age_below12.male,
                enrollment_entrant_age_below12_female: jso.enrollment_entrant.age_below12.female,
                
                /*enrollment_entrant_age_15_male: jso.enrollment_entrant.age_12.male,
                enrollment_entrant_age_15_female: jso.enrollment_entrant.age_12.female,
                enrollment_entrant_age_16_male: jso.enrollment_entrant.age_13.male,
                enrollment_entrant_age_16_female: jso.enrollment_entrant.age_13.female,
                enrollment_entrant_age_17_male: jso.enrollment_entrant.age_14.male,
                enrollment_entrant_age_17_female: jso.enrollment_entrant.age_14.female,
                enrollment_entrant_age_above17_male: jso.enrollment_entrant.age_above14.male,
                enrollment_entrant_age_above17_female: jso.enrollment_entrant.age_above14.female,
                enrollment_entrant_age_below15_male: jso.enrollment_entrant.age_below12.male,
                enrollment_entrant_age_below15_female: jso.enrollment_entrant.age_below12.female*/
                
                
                
              
            }
            
                
            ).then(response => {

                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //this.fetch_b_c = this.fetch_b_c;
                        alert("Data Saved to DB");
                        console.log(JSON.stringify(this.fetch_repeaters, null, 2));
                        
            

                    } else if(this.resultsSave.status == "error") {
                        alert(response.data.message);
                    }else {
                        alert("Check the form for the required data");
                    }





                })
                


        },

        saveEnrollmentByAge: function () {
            //alert(JSON.stringify(this.fetch_r, null, 2))
 
             axios.post("http://127.0.0.1:8000/api/Section/SchoolEnrollment/EnrollmentByAge",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "jss",
                 stream_jss1_stream: this.fetch_streams[0].stream,
                 stream_jss2_stream: this.fetch_streams[1].stream,
                 stream_jss3_stream: this.fetch_streams[2].stream,

                 stream_jss1_streamwithmultigrade:  this.fetch_streams[0].multigrade,
                 stream_jss2_streamwithmultigrade:  this.fetch_streams[1].multigrade,
                 stream_jss3_streamwithmultigrade:  this.fetch_streams[2].multigrade,

                 enrollment_jss1_age_12_male: this.fetch_all_ages[0].age_male_1,
                 enrollment_jss1_age_12_female: this.fetch_all_ages[0].age_female_1,
                 enrollment_jss2_age_12_male: this.fetch_all_ages[0].age_male_2,
                 enrollment_jss2_age_12_female: this.fetch_all_ages[0].age_female_2,
                 enrollment_jss3_age_12_male: this.fetch_all_ages[0].age_male_3,
                 enrollment_jss3_age_12_female: this.fetch_all_ages[0].age_female_3,

                 enrollment_jss1_age_13_male: this.fetch_all_ages[1].age_male_1,
                 enrollment_jss1_age_13_female: this.fetch_all_ages[1].age_female_1,
                 enrollment_jss2_age_13_male: this.fetch_all_ages[1].age_male_2,
                 enrollment_jss2_age_13_female: this.fetch_all_ages[1].age_female_2,
                 enrollment_jss3_age_13_male: this.fetch_all_ages[1].age_male_3,
                 enrollment_jss3_age_13_female: this.fetch_all_ages[1].age_female_3,

                 enrollment_jss1_age_14_male: this.fetch_all_ages[3].age_male_1,
                 enrollment_jss1_age_14_female: this.fetch_all_ages[3].age_female_1,
                 enrollment_jss2_age_14_male: this.fetch_all_ages[3].age_male_2,
                 enrollment_jss2_age_14_female: this.fetch_all_ages[3].age_female_2,
                 enrollment_jss3_age_14_male: this.fetch_all_ages[3].age_male_3,
                 enrollment_jss3_age_14_female: this.fetch_all_ages[3].age_female_3,

                 enrollment_jss1_age_above14_male: this.fetch_all_ages[2].age_male_1,
                 enrollment_jss1_age_above14_female: this.fetch_all_ages[2].age_female_1,
                 enrollment_jss2_age_above14_male: this.fetch_all_ages[2].age_male_2,
                 enrollment_jss2_age_above14_female: this.fetch_all_ages[2].age_female_2,
                 enrollment_jss3_age_above14_male: this.fetch_all_ages[2].age_male_3,
                 enrollment_jss3_age_above14_female: this.fetch_all_ages[2].age_female_3,

                 enrollment_jss1_age_below12_male: this.fetch_all_ages[4].age_male_1,
                 enrollment_jss1_age_below12_female: this.fetch_all_ages[4].age_female_1,
                 enrollment_jss2_age_below12_male: this.fetch_all_ages[4].age_male_2,
                 enrollment_jss2_age_below12_female: this.fetch_all_ages[4].age_female_2,
                 enrollment_jss3_age_below12_male: this.fetch_all_ages[4].age_male_3,
                 enrollment_jss3_age_below12_female: this.fetch_all_ages[4].age_female_3,

                 repeater_jss1_male:  this.fetch_r[0].repeaters,
                 repeater_jss1_female:  this.fetch_r[1].repeaters,
                 repeater_jss2_male:  this.fetch_r[2].repeaters,
                 repeater_jss2_female:  this.fetch_r[3].repeaters,
                 repeater_jss3_male:  this.fetch_r[4].repeaters,
                 repeater_jss3_female:  this.fetch_r[5].repeaters,
                 
               
             }
             
                 
             ).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         alert("Data Saved to DB");
                         console.log(JSON.stringify(this.fetch_r, null, 2));
                         console.log("0" + this.fetch_r[0].repeaters);
 
                     } else if(this.resultsSave.status == "error") {
                         alert(response.data.message);
                     }else {
                         alert("Check the form for the required data");
                     }
 
 
 
 
 
                 })
                 
 
 
         },

    
        

        saveSpecialNeed: function () {
            //console.log(JSON.stringify(this.fetch_all_special_needs, null, 2))
            
          

 
             axios.post("http://127.0.0.1:8000/api/Section/SchoolEnrollment/SpecialNeeds",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "jss",
                 
                 specialneed_jss1_altruism_male: this.fetch_all_special_needs[3].special_need_item_male_1,
                 specialneed_jss1_altruism_female: this.fetch_all_special_needs[3].special_need_item_female_1,
                 specialneed_jss2_altruism_male: this.fetch_all_special_needs[3].special_need_item_male_2,
                 specialneed_jss2_altruism_female: this.fetch_all_special_needs[3].special_need_item_female_2,
                 specialneed_jss3_altruism_male: this.fetch_all_special_needs[3].special_need_item_male_3,
                 specialneed_jss3_altruism_female: this.fetch_all_special_needs[3].special_need_item_female_3,

                 specialneed_jss1_albinism_male: this.fetch_all_special_needs[4].special_need_item_male_1,
                 specialneed_jss1_albinism_female: this.fetch_all_special_needs[4].special_need_item_female_1,
                 specialneed_jss2_albinism_male: this.fetch_all_special_needs[4].special_need_item_male_2,
                 specialneed_jss2_albinism_female: this.fetch_all_special_needs[4].special_need_item_female_2,
                 specialneed_jss3_albinism_male: this.fetch_all_special_needs[4].special_need_item_male_3,
                 specialneed_jss3_albinism_female: this.fetch_all_special_needs[4].special_need_item_female_3,

                 specialneed_jss1_mentally_male: this.fetch_all_special_needs[0].special_need_item_male_1,
                 specialneed_jss1_mentally_female: this.fetch_all_special_needs[0].special_need_item_female_1,
                 specialneed_jss2_mentally_male: this.fetch_all_special_needs[0].special_need_item_male_2,
                 specialneed_jss2_mentally_female: this.fetch_all_special_needs[0].special_need_item_female_2,
                 specialneed_jss3_mentally_male: this.fetch_all_special_needs[0].special_need_item_male_3,
                 specialneed_jss3_mentally_female: this.fetch_all_special_needs[0].special_need_item_female_3,
                 
                 specialneed_jss1_physical_male: this.fetch_all_special_needs[1].special_need_item_male_1,
                 specialneed_jss1_physical_female: this.fetch_all_special_needs[1].special_need_item_female_1,
                 specialneed_jss2_physical_male: this.fetch_all_special_needs[1].special_need_item_male_2,
                 specialneed_jss2_physical_female: this.fetch_all_special_needs[1].special_need_item_female_2,
                 specialneed_jss3_physical_male: this.fetch_all_special_needs[1].special_need_item_male_3,
                 specialneed_jss3_physical_female: this.fetch_all_special_needs[1].special_need_item_female_3,

                 specialneed_jss1_hearing_male: this.fetch_all_special_needs[2].special_need_item_male_1,
                 specialneed_jss1_hearing_female: this.fetch_all_special_needs[2].special_need_item_female_1,
                 specialneed_jss2_hearing_male: this.fetch_all_special_needs[2].special_need_item_male_2,
                 specialneed_jss2_hearing_female: this.fetch_all_special_needs[2].special_need_item_female_2,
                 specialneed_jss3_hearing_male: this.fetch_all_special_needs[2].special_need_item_male_3,
                 specialneed_jss3_hearing_female: this.fetch_all_special_needs[2].special_need_item_female_3,
                 
               
             }
             
                 
             ).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         alert("Data Saved to DB");
                         console.log(JSON.stringify(this.fetch_all_special_needs, null, 2));
                         //console.log("0" + this.fetch_r[0].repeaters);
 
                     } else if(this.resultsSave.status == "error") {
                         alert(response.data.message);
                     }else {
                         alert("Check the form for the required data");
                     }
 
 
 
 
 
                 })
                 
 
 
         },


         saveStudentsFlow: function () {
            alert(JSON.stringify(this.fetch_drop, null, 2))
 
             axios.post("http://127.0.0.1:8000/api/Section/SchoolEnrollment/StudentFlow",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "jss",
                 
                 pupilflow_jss1_dropout_male: this.fetch_drop[0].dropouts,
                 pupilflow_jss1_dropout_female: this.fetch_drop[1].dropouts,
                 pupilflow_jss2_dropout_male: this.fetch_drop[2].dropouts,
                 pupilflow_jss2_dropout_female: this.fetch_drop[3].dropouts,
                 pupilflow_jss3_dropout_male: this.fetch_drop[4].dropouts,
                 pupilflow_jss3_dropout_female: this.fetch_drop[5].dropouts,

                 pupilflow_jss1_transferin_male: this.fetch_t_in[0].transfer,
                 pupilflow_jss1_transferin_female: this.fetch_t_in[1].transfer,
                 pupilflow_jss2_transferin_male: this.fetch_t_in[2].transfer,
                 pupilflow_jss2_transferin_female: this.fetch_t_in[3].transfer,
                 pupilflow_jss3_transferin_male: this.fetch_t_in[4].transfer,
                 pupilflow_jss3_transferin_female: this.fetch_t_in[5].transfer,

                 pupilflow_jss1_transferout_male: this.fetch_t_out[0].transfer,
                 pupilflow_jss1_transferout_female: this.fetch_t_out[1].transfer,
                 pupilflow_jss2_transferout_male: this.fetch_t_out[2].transfer,
                 pupilflow_jss2_transferout_female: this.fetch_t_out[3].transfer,
                 pupilflow_jss3_transferout_male: this.fetch_t_out[4].transfer,
                 pupilflow_jss3_transferout_female: this.fetch_t_out[5].transfer,

                 pupilflow_jss1_promoted_male: this.fetch_pro[0].promoted,
                 pupilflow_jss1_promoted_female: this.fetch_pro[1].promoted,
                 pupilflow_jss2_promoted_male: this.fetch_pro[2].promoted,
                 pupilflow_jss2_promoted_female: this.fetch_pro[3].promoted,
                 pupilflow_jss3_promoted_male: this.fetch_pro[4].promoted,
                 pupilflow_jss3_promoted_female: this.fetch_pro[5].promoted,
                 
                 
               
             }
             
                 
             ).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         alert("Data Saved to DB");
                         console.log(JSON.stringify(this.fetch_drop, null, 2));
                         console.log(JSON.stringify(this.fetch_t_in, null, 2));
                         console.log(JSON.stringify(this.fetch_t_out, null, 2));
                         console.log(JSON.stringify(this.fetch_pro, null, 2));
                         //console.log("0" + this.fetch_r[0].repeaters);
 
                     } else if(this.resultsSave.status == "error") {
                         alert(response.data.message);
                     }else {
                         alert("Check the form for the required data");
                     }
 
 
 
 
 
                 })
                 
 
 
         },

         saveExamination: function () {
            

                axios.post("http://127.0.0.1:8000/api/Section/SchoolEnrollment/Examination",
                {
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    examination: "jsce",

                    examination_jsce_registeredmale: this.registered_male,
                    examination_jsce_registeredfemale: this.registered_female,
                    examination_jsce_tookpartmale: this.took_part_male,
                    examination_jsce_tookpartfemale: this.took_part_female,
                    examination_jsce_passedmale: this.passed_male,
                    examination_jsce_passedfemale: this.passed_female

                    }).then(response => {

                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            alert(this.resultsSave.message);
                        } else {
                            alert(this.resultsSave.message);
                        }





                    })

            

        },

    
    }
});

var app = new Vue({
    el: '#nemisapp',
    data:{
        results: [],
        resultsSave: [],
        schoolname: '',
        schoolstreet: '',
        schoolemail: '',
        schoollga: '',
        schooltown: '',
        schooltelephone: '',
        schoolward: '',
        schoolstate: '',

        fetch_states: [],
        state_picked: '',
        state_index: [],
        get_state_code: '',

        results_lga: [],
        fetch_lga: [],
        lga_picked: '',
        lga_index: [],
        get_lga_code: '',

        counter_state: 0,
        counter_lga: 0,

        location_type_ref: [],
        location_type_val: [],
        metadata: [],

        fetch_location: [],
        fetch_levels: [],
        fetch_category: [],
        fetch_authority: [],
        fetch_ownership: [],
        fetch_birth_certificate: [],
        fetch_b_c: [],
        fetch_b_c_no: [],
        fetch_age: [],
        fetch_a: [],
        fetch_a_no: [],
        fetch_streams: [],
        fetch_year_by_age: [],
        fetch_year_by_age_no: [],
        fetch_y_b_a1: [], fetch_y_b_a2: [], fetch_y_b_a3: [],
        fetch_y_b_no1: [], fetch_y_b_no2: [], fetch_y_b_no3: [],
        fetch_all_ages: [],
        fetch_repeaters: [], fetch_r:[], 
        fetch_dropouts: [], fetch_drop:[],
        fetch_transfer_in: [], fetch_t_in:[],
        fetch_transfer_out: [], fetch_t_out:[],
        fetch_promoted: [], fetch_pro:[],

        fetch_all_special_needs: [], fetch_special_needs: [], fetch_special_needs_item: [],
        fetch_s_n1: [], fetch_s_n2: [], fetch_s_n3: [],
        fetch_s_n_no1: [], fetch_s_n_no2: [], fetch_s_n_no3: [],
        fetch_a_s_n: [], fetch_a_s_n_no: [],

        inputParams: [],

        exam_type:'',
        registered_male:'',
        registered_female:'',
        registered_total:'', 
        took_part_male:'',
        took_part_female:'',
        took_part_total:'',
        passed_male:'',
        passed_female:'',
        passed_total:'',

        year: '',
        year_of_establishment: '',
        location_choice: '',
        loeo2_choice: '',
        loeo_choice: '',
        shifts_choice: '',
        shifts_choice_bi: '',
        facilities_choice: '',
        facilities_choice_bi: '',
        facilities_choice_yes: '',
        multigrade_choice: '',
        multigrade_choice_bi: '',
        average_distance: '',
        student_distance: '',
        students_boarding_female: '',
        students_boarding_male: '',
        sdp_choice: '',
        sdp_choice_bi: '',
        sbmc_choice: '',
        sbmc_choice_bi: '',
        pta_choice: '',
        pta_choice_bi: '',
        date_inspection: '',
        authority_choice: '',
        cash_transfer: '',
        grants_choice: '',
        grants_choice_bi: '',
        guard_choice: '',
        guard_choice_bi: '',
        ownership_choice: '',

        nPopC_male: '',
        nPopC_female: '',
        others_male: '',
        others_female: '',

        total_age_male: '',
        total_age_female: '',
    },

    computed:{

    },

    watch:{

    },

    mounted: function(){
        axios.get("http://127.0.0.1:8000/api/school/" + schoolcode.toLowerCase() + "/year/" + year).then(response => {
            this.results = response.data;//returns the json data

            this.results_lga = response.data;
            //this.results =  JSON.parse(response.data);
            //console.log(this.results);
            console.log('r: ', JSON.stringify(response.data, null, 2));
            /*for (var prop in response.data.data) {
                this.schools.push(response.data.data.name[prop]);
            }*/

            //this.schools.push(this.results.data[0].data.name);
            //console.log(this.schools);

            //Apply the metadata to the form sections
            //bind the states list
            var arrayOfObjects = []

            for (var i = 0; i < this.results[1].attributes.states.length; i++) {//loop the states
                var obj = {};
                
                //obj[keys[j]] = values[i][j];
                obj["name"] = this.results[1].attributes.states[i].data.name;
                obj["code"] = this.results[1].attributes.states[i].data.statecode;
                //this.state_index[this.results[1].attributes.states[i].data.name] = i; 
                //save the state name as a key and the value is the index

                

                arrayOfObjects.push(obj);
            }
            //this.state_picked=this.results[1].attributes.states[0].data.name;

            this.fetch_states = arrayOfObjects;

            //alert(this.counter_state);

            var arrayOfObjects2 = []
            ////LGA
            for (var i = 0; i < this.results[1].attributes.states[this.counter_state].data.lgas.length; i++) {
                var obj2 = {};
                for (var j = 0; j < this.results[1].attributes.states[this.counter_state].data.lgas.length; j++) {
                    //obj[keys[j]] = values[i][j];
                    obj2["name"] = this.results[1].attributes.states[this.counter_state].data.lgas[i].data.name;
                    obj2["code"] = this.results[1].attributes.states[this.counter_state].data.lgas[i].data.lgacode;
                    this.lga_index[this.results[1].attributes.states[this.counter_state].data.lgas[i].data.name] = i;

                }

                arrayOfObjects2.push(obj2);
            }
            // this.lga_picked = this.results[1].attributes.states[0].data.lgas[0].data.name;
            //alert(this.lga_picked);   

            this.fetch_lga = arrayOfObjects2;

            //bind the data to the inputs

        

        });

    },

    methods:{
        saveIdentification: function () {
            if (this.schoolname != '' && this.schoolstreet != '' && this.schoollga != '' &&
                this.schoolstate != '' && this.schoolward != '' && this.schoolemail != '' &&
                this.schooltown != '' && this.schooltelephone != '') {
                this.counter_state = this.state_index[this.state_picked];
                this.get_state_code = this.fetch_states[this.counter_state].code
                //alert(this.get_state_code);
                this.counter_lga = this.lga_index[this.lga_picked];
                this.get_lga_code = this.fetch_lga[this.counter_lga].code;
                //alert(this.get_lga_code);
                axios.post("http://127.0.0.1:8000/api/Section/SchoolIdentification",
                    {
                        school_code: schoolcode.toLowerCase(),
                        school_name: this.schoolname,
                        school_address: this.schoolstreet,
                        town: this.schooltown,
                        ward: this.schoolward,
                        lga: this.get_lga_code,
                        state: this.get_state_code,
                        telephone: this.schooltelephone,
                        email: this.schoolemail
                    }).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            alert("Data Saved to DB");
                        } else {
                            alert("Check the form for the required data");
                        }
                    })
            }
        },

        nextCharacteristics: function () {
            console.log(this.loeo2_choice + " " + this.loeo_choice);
            alert(this.shifts_choice + " " + this.facilities_choice);
        },

        getLGA: function () {
            var arrayOfObjects2 = [];

            //this.counter_state = this.state_index[this.state_picked];


            for (var i = 0; i < this.results[1].attributes.states[this.state_picked].data.lgas.length; i++) {
                var obj2 = {};
                
                obj2["name"] = this.results[1].attributes.states[this.counter_state].data.lgas[i].data.name;
                obj2["code"] = this.results[1].attributes.states[this.counter_state].data.lgas[i].data.lgacode;
                this.lga_index[this.results[1].attributes.states[this.counter_state].data.lgas[i].data.name] = i;
            

                arrayOfObjects2.push(obj2);
            }
            //this.lga_picked=this.results[1].attributes.states[this.counter_state].data.lgas[0].data.name;
            this.fetch_lga = arrayOfObjects2;


            console.log('now: ', JSON.stringify(this.fetch_states, null, " "));
            //alert(this.lga_picked);
        },

        saveCharacteristics: function () {


            /* if(this.year != '' && this.facilities_choice != '' && this.facilities_choice_yes != '' &&
             this.multigrade_choice != '' && this.average_distance != '' && this.student_distance != '' &&
             this.students_boarding_female != '' && this.students_boarding_male != '' &&
             this.sdp_choice != '' && this.sbmc_choice != '' &&
             this.pta_choice != '' && this.date_inspection != '' &&
             this.authority_choice != '' && this.cash_transfer != '' &&
             this.students_grants_choice != '' && this.students_guard_choice != '' &&
             this.ownership_choice != '') {*/

            if (this.shifts_choice == "Yes") {
                this.shifts_choice_bi = "1";
            } else if (this.shifts_choice == "No") {
                this.shifts_choice_bi = "0";
            } else {

            }

            if (this.facilities_choice == "Yes") {
                this.facilities_choice_bi = "1";
            } else if (this.facilities_choice == "No") {
                this.facilities_choice_bi = "0";
                //this.facilities_choice_yes = this.results[0].data.school_characteristics.shared_facilities;
            } else {

            }

            if (this.multigrade_choice == "Yes") {
                this.multigrade_choice_bi = "1";
            } else if (this.multigrade_choice == "No") {
                this.multigrade_choice_bi = "0";
            } else {

            }

            if (this.sdp_choice == "Yes") {

                this.sdp_choice_bi = "1";
            } else if (this.sdp_choice == "No") {
                this.sdp_choice_bi = "0";
            } else {

            }

            if (this.sbmc_choice == "Yes") {
                this.sbmc_choice_bi = "1";
            } else if (this.sbmc_choice == "No") {
                this.sbmc_choice_bi = "0";
            } else {

            }

            if (this.pta_choice == "Yes") {
                this.pta_choice_bi = "1";
            } else if (this.pta_choice == "No") {
                this.pta_choice_bi = "0";
            } else {

            }

            if (this.grants_choice == "Yes") {
                this.grants_choice_bi = "1";
            } else if (this.grants_choice == "No") {
                this.grants_choice_bi = "0";
            } else {

            }

            if (this.guard_choice == "Yes") {
                this.guard_choice_bi = "1";
            } else if (this.guard_choice == "No") {
                this.guard_choice_bi = "0";
            } else {

            }

            //alert(this.student_distance);


            axios.post("http://127.0.0.1:8000/api/Section/SchoolCharacteristics",
                {
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    shared_facilities: this.facilities_choice_bi,
                    schools_sharingwith: this.facilities_choice_yes,
                    location_type: this.location_choice,
                    school_type: this.loeo2_choice,
                    level_of_education: this.loeo_choice,
                    multi_grade_teaching: this.multigrade_choice_bi,
                    distance_from_catchment_area: this.average_distance,
                    students_travelling_3km: this.student_distance,
                    male_student_b: this.students_boarding_female,
                    female_student_b: this.students_boarding_male,
                    has_school_development_plan: this.sdp_choice_bi,
                    has_sbmc: this.sbmc_choice_bi,
                    has_pta: this.pta_choice_bi,
                    last_inspection_date: this.date_inspection,
                    last_inspection_authority: this.authority_choice,
                    conditional_cash_transfer: this.cash_transfer,
                    has_school_grant: this.grants_choice_bi,
                    has_security_guard: this.guard_choice_bi,
                    ownership: this.ownership_choice,
                }).then(response => {

                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        alert("Data Saved to DB");
                    } else {
                        alert("Check the form for the required data");
                    }
                })


        },

        saveBirthCertificates: function () {
           
            var arr = JSON.parse(JSON.stringify(this.fetch_b_c)) 
            var testArray = arr;

            var vals = [];
            for (var i = 0; i < testArray.length; i++) {
                vals.push(testArray[i].val);
            }
       
            var json = "{\"birthcertificate\":{" 
            for (var i = 0; i < testArray.length; i++) {
                var j = testArray[i];
                if(j.certificates == "National Population Commission") {
                    j.certificates = "npc";
                } else {

                }
                json += "\"" + j.certificates.toLowerCase() + "\":{"
                delete j['certificates'];
                var dq = '"';
                var last = Object.keys(j).length;
                var count = 0;
                for (x in j) {
                    json += dq + x + dq + ":" + dq + j[x] + dq;
                    count++;
                    if (count < last)
                        json += ",";
                }
                if(i != testArray.length-1) {
                    json += "},";
                } else {
                    json += "}"; 
                }
                
            }
            json += "}}";
 
            var jso = JSON.parse(json);
              
          

            axios.post("http://127.0.0.1:8000/api/Section/SchoolEnrollment/BirthCertificate",{
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class: "jss",
                birthcertificate_npc_male: jso.birthcertificate.npc.male,
                birthcertificate_npc_female: jso.birthcertificate.npc.female,
                birthcertificate_hospital_male: jso.birthcertificate.hospital.male,
                birthcertificate_hospital_female: jso.birthcertificate.hospital.female,
                birthcertificate_lga_male: jso.birthcertificate.lga.male,
                birthcertificate_lga_female: jso.birthcertificate.lga.female,
                birthcertificate_court_male: jso.birthcertificate.court.male,
                birthcertificate_court_female: jso.birthcertificate.court.female,
                birthcertificate_un_male: jso.birthcertificate.un.male,
                birthcertificate_un_female: jso.birthcertificate.un.female,
                birthcertificate_others_male: jso.birthcertificate.others.male,
                birthcertificate_others_female: jso.birthcertificate.others.female,
                
                
                
                
                
              
            }
            
                
            ).then(response => {

                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //this.fetch_b_c = this.fetch_b_c;
                        alert("Data Saved to DB");
                        //alert(JSON.stringify(this.fetch_b_c, null, 2));
            

                    } else if(this.resultsSave.status == "error") {
                        alert(response.data.message);
                    }else {
                        alert("Check the form for the required data");
                    }





                })


        },

        saveEntrants: function () {
           //alert(JSON.stringify(this.fetch_a))
           
            var arr = JSON.parse(JSON.stringify(this.fetch_a)) 
            var testArray = arr;

            var vals = [];
            for (var i = 0; i < testArray.length; i++) {
                vals.push(testArray[i].val);
            }
       
            var json = "{\"enrollment_entrant\":{" 
            for (var i = 0; i < testArray.length; i++) {
                var j = testArray[i];
                if(j.age == "12") {
                    j.age = "age_12";
                } else if(j.age == "13"){
                    j.age = "age_13"
                } else if(j.age == "Above 14 Years") {
                    j.age = "age_above14"
                } else if(j.age == "Below 12 Years"){
                    j.age = "age_below12"
                } else if(j.age == "14") {
                    j.age = "age_14"
                } else if(j.age == "others") {
                    j.age = "others"
                } else if(j.age == "17") {
                    j.age = "age_17";
                } else if(j.age == "16") {
                    j.age = "age_16";
                } else if(j.age == "15") {
                    j.age = "age_15";
                } else if(j.age == "below 15") {
                    j.age = "age_below15";
                } else if(j.age == "above 17") {
                    j.age = "age_above17";
                }

                json += "\"" + j.age.toLowerCase() + "\":{"
                delete j['age'];
                var dq = '"';
                var last = Object.keys(j).length;
                var count = 0;
                for (x in j) {
                    json += dq + x + dq + ":" + dq + j[x] + dq;
                    count++;
                    if (count < last)
                        json += ",";
                }
                if(i != testArray.length-1) {
                    json += "},";
                } else {
                    json += "}"; 
                }
                
            }
            json += "}}";
 
            
            var jso = JSON.parse(json);  
            
          

            axios.post("http://127.0.0.1:8000/api/Section/SchoolEnrollment/Entrant",{
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class: "jss",
                enrollment_entrant_age_12_male: jso.enrollment_entrant.age_12.male,
                enrollment_entrant_age_12_female: jso.enrollment_entrant.age_12.female,
                enrollment_entrant_age_13_male: jso.enrollment_entrant.age_13.male,
                enrollment_entrant_age_13_female: jso.enrollment_entrant.age_13.female,
                enrollment_entrant_age_14_male: jso.enrollment_entrant.age_14.male,
                enrollment_entrant_age_14_female: jso.enrollment_entrant.age_14.female,
                enrollment_entrant_age_above14_male: jso.enrollment_entrant.age_above14.male,
                enrollment_entrant_age_above14_female: jso.enrollment_entrant.age_above14.female,
                enrollment_entrant_age_below12_male: jso.enrollment_entrant.age_below12.male,
                enrollment_entrant_age_below12_female: jso.enrollment_entrant.age_below12.female,
                
                /*enrollment_entrant_age_15_male: jso.enrollment_entrant.age_12.male,
                enrollment_entrant_age_15_female: jso.enrollment_entrant.age_12.female,
                enrollment_entrant_age_16_male: jso.enrollment_entrant.age_13.male,
                enrollment_entrant_age_16_female: jso.enrollment_entrant.age_13.female,
                enrollment_entrant_age_17_male: jso.enrollment_entrant.age_14.male,
                enrollment_entrant_age_17_female: jso.enrollment_entrant.age_14.female,
                enrollment_entrant_age_above17_male: jso.enrollment_entrant.age_above14.male,
                enrollment_entrant_age_above17_female: jso.enrollment_entrant.age_above14.female,
                enrollment_entrant_age_below15_male: jso.enrollment_entrant.age_below12.male,
                enrollment_entrant_age_below15_female: jso.enrollment_entrant.age_below12.female*/
                
                
                
              
            }
            
                
            ).then(response => {

                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //this.fetch_b_c = this.fetch_b_c;
                        alert("Data Saved to DB");
                        console.log(JSON.stringify(this.fetch_repeaters, null, 2));
                        
            

                    } else if(this.resultsSave.status == "error") {
                        alert(response.data.message);
                    }else {
                        alert("Check the form for the required data");
                    }





                })
                


        },

        saveEnrollmentByAge: function () {
            //alert(JSON.stringify(this.fetch_r, null, 2))
 
             axios.post("http://127.0.0.1:8000/api/Section/SchoolEnrollment/EnrollmentByAge",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "jss",
                 stream_jss1_stream: this.fetch_streams[0].stream,
                 stream_jss2_stream: this.fetch_streams[1].stream,
                 stream_jss3_stream: this.fetch_streams[2].stream,

                 stream_jss1_streamwithmultigrade:  this.fetch_streams[0].multigrade,
                 stream_jss2_streamwithmultigrade:  this.fetch_streams[1].multigrade,
                 stream_jss3_streamwithmultigrade:  this.fetch_streams[2].multigrade,

                 enrollment_jss1_age_12_male: this.fetch_all_ages[0].age_male_1,
                 enrollment_jss1_age_12_female: this.fetch_all_ages[0].age_female_1,
                 enrollment_jss2_age_12_male: this.fetch_all_ages[0].age_male_2,
                 enrollment_jss2_age_12_female: this.fetch_all_ages[0].age_female_2,
                 enrollment_jss3_age_12_male: this.fetch_all_ages[0].age_male_3,
                 enrollment_jss3_age_12_female: this.fetch_all_ages[0].age_female_3,

                 enrollment_jss1_age_13_male: this.fetch_all_ages[1].age_male_1,
                 enrollment_jss1_age_13_female: this.fetch_all_ages[1].age_female_1,
                 enrollment_jss2_age_13_male: this.fetch_all_ages[1].age_male_2,
                 enrollment_jss2_age_13_female: this.fetch_all_ages[1].age_female_2,
                 enrollment_jss3_age_13_male: this.fetch_all_ages[1].age_male_3,
                 enrollment_jss3_age_13_female: this.fetch_all_ages[1].age_female_3,

                 enrollment_jss1_age_14_male: this.fetch_all_ages[3].age_male_1,
                 enrollment_jss1_age_14_female: this.fetch_all_ages[3].age_female_1,
                 enrollment_jss2_age_14_male: this.fetch_all_ages[3].age_male_2,
                 enrollment_jss2_age_14_female: this.fetch_all_ages[3].age_female_2,
                 enrollment_jss3_age_14_male: this.fetch_all_ages[3].age_male_3,
                 enrollment_jss3_age_14_female: this.fetch_all_ages[3].age_female_3,

                 enrollment_jss1_age_above14_male: this.fetch_all_ages[2].age_male_1,
                 enrollment_jss1_age_above14_female: this.fetch_all_ages[2].age_female_1,
                 enrollment_jss2_age_above14_male: this.fetch_all_ages[2].age_male_2,
                 enrollment_jss2_age_above14_female: this.fetch_all_ages[2].age_female_2,
                 enrollment_jss3_age_above14_male: this.fetch_all_ages[2].age_male_3,
                 enrollment_jss3_age_above14_female: this.fetch_all_ages[2].age_female_3,

                 enrollment_jss1_age_below12_male: this.fetch_all_ages[4].age_male_1,
                 enrollment_jss1_age_below12_female: this.fetch_all_ages[4].age_female_1,
                 enrollment_jss2_age_below12_male: this.fetch_all_ages[4].age_male_2,
                 enrollment_jss2_age_below12_female: this.fetch_all_ages[4].age_female_2,
                 enrollment_jss3_age_below12_male: this.fetch_all_ages[4].age_male_3,
                 enrollment_jss3_age_below12_female: this.fetch_all_ages[4].age_female_3,

                 repeater_jss1_male:  this.fetch_r[0].repeaters,
                 repeater_jss1_female:  this.fetch_r[1].repeaters,
                 repeater_jss2_male:  this.fetch_r[2].repeaters,
                 repeater_jss2_female:  this.fetch_r[3].repeaters,
                 repeater_jss3_male:  this.fetch_r[4].repeaters,
                 repeater_jss3_female:  this.fetch_r[5].repeaters,
                 
               
             }
             
                 
             ).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         alert("Data Saved to DB");
                         console.log(JSON.stringify(this.fetch_r, null, 2));
                         console.log("0" + this.fetch_r[0].repeaters);
 
                     } else if(this.resultsSave.status == "error") {
                         alert(response.data.message);
                     }else {
                         alert("Check the form for the required data");
                     }
 
 
 
 
 
                 })
                 
 
 
         },

    
        

        saveSpecialNeed: function () {
            //console.log(JSON.stringify(this.fetch_all_special_needs, null, 2))
            
          

 
             axios.post("http://127.0.0.1:8000/api/Section/SchoolEnrollment/SpecialNeeds",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "jss",
                 
                 specialneed_jss1_altruism_male: this.fetch_all_special_needs[3].special_need_item_male_1,
                 specialneed_jss1_altruism_female: this.fetch_all_special_needs[3].special_need_item_female_1,
                 specialneed_jss2_altruism_male: this.fetch_all_special_needs[3].special_need_item_male_2,
                 specialneed_jss2_altruism_female: this.fetch_all_special_needs[3].special_need_item_female_2,
                 specialneed_jss3_altruism_male: this.fetch_all_special_needs[3].special_need_item_male_3,
                 specialneed_jss3_altruism_female: this.fetch_all_special_needs[3].special_need_item_female_3,

                 specialneed_jss1_albinism_male: this.fetch_all_special_needs[4].special_need_item_male_1,
                 specialneed_jss1_albinism_female: this.fetch_all_special_needs[4].special_need_item_female_1,
                 specialneed_jss2_albinism_male: this.fetch_all_special_needs[4].special_need_item_male_2,
                 specialneed_jss2_albinism_female: this.fetch_all_special_needs[4].special_need_item_female_2,
                 specialneed_jss3_albinism_male: this.fetch_all_special_needs[4].special_need_item_male_3,
                 specialneed_jss3_albinism_female: this.fetch_all_special_needs[4].special_need_item_female_3,

                 specialneed_jss1_mentally_male: this.fetch_all_special_needs[0].special_need_item_male_1,
                 specialneed_jss1_mentally_female: this.fetch_all_special_needs[0].special_need_item_female_1,
                 specialneed_jss2_mentally_male: this.fetch_all_special_needs[0].special_need_item_male_2,
                 specialneed_jss2_mentally_female: this.fetch_all_special_needs[0].special_need_item_female_2,
                 specialneed_jss3_mentally_male: this.fetch_all_special_needs[0].special_need_item_male_3,
                 specialneed_jss3_mentally_female: this.fetch_all_special_needs[0].special_need_item_female_3,
                 
                 specialneed_jss1_physical_male: this.fetch_all_special_needs[1].special_need_item_male_1,
                 specialneed_jss1_physical_female: this.fetch_all_special_needs[1].special_need_item_female_1,
                 specialneed_jss2_physical_male: this.fetch_all_special_needs[1].special_need_item_male_2,
                 specialneed_jss2_physical_female: this.fetch_all_special_needs[1].special_need_item_female_2,
                 specialneed_jss3_physical_male: this.fetch_all_special_needs[1].special_need_item_male_3,
                 specialneed_jss3_physical_female: this.fetch_all_special_needs[1].special_need_item_female_3,

                 specialneed_jss1_hearing_male: this.fetch_all_special_needs[2].special_need_item_male_1,
                 specialneed_jss1_hearing_female: this.fetch_all_special_needs[2].special_need_item_female_1,
                 specialneed_jss2_hearing_male: this.fetch_all_special_needs[2].special_need_item_male_2,
                 specialneed_jss2_hearing_female: this.fetch_all_special_needs[2].special_need_item_female_2,
                 specialneed_jss3_hearing_male: this.fetch_all_special_needs[2].special_need_item_male_3,
                 specialneed_jss3_hearing_female: this.fetch_all_special_needs[2].special_need_item_female_3,
                 
               
             }
             
                 
             ).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         alert("Data Saved to DB");
                         console.log(JSON.stringify(this.fetch_all_special_needs, null, 2));
                         //console.log("0" + this.fetch_r[0].repeaters);
 
                     } else if(this.resultsSave.status == "error") {
                         alert(response.data.message);
                     }else {
                         alert("Check the form for the required data");
                     }
 
 
 
 
 
                 })
                 
 
 
         },


         saveStudentsFlow: function () {
            alert(JSON.stringify(this.fetch_drop, null, 2))
 
             axios.post("http://127.0.0.1:8000/api/Section/SchoolEnrollment/StudentFlow",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "jss",
                 
                 pupilflow_jss1_dropout_male: this.fetch_drop[0].dropouts,
                 pupilflow_jss1_dropout_female: this.fetch_drop[1].dropouts,
                 pupilflow_jss2_dropout_male: this.fetch_drop[2].dropouts,
                 pupilflow_jss2_dropout_female: this.fetch_drop[3].dropouts,
                 pupilflow_jss3_dropout_male: this.fetch_drop[4].dropouts,
                 pupilflow_jss3_dropout_female: this.fetch_drop[5].dropouts,

                 pupilflow_jss1_transferin_male: this.fetch_t_in[0].transfer,
                 pupilflow_jss1_transferin_female: this.fetch_t_in[1].transfer,
                 pupilflow_jss2_transferin_male: this.fetch_t_in[2].transfer,
                 pupilflow_jss2_transferin_female: this.fetch_t_in[3].transfer,
                 pupilflow_jss3_transferin_male: this.fetch_t_in[4].transfer,
                 pupilflow_jss3_transferin_female: this.fetch_t_in[5].transfer,

                 pupilflow_jss1_transferout_male: this.fetch_t_out[0].transfer,
                 pupilflow_jss1_transferout_female: this.fetch_t_out[1].transfer,
                 pupilflow_jss2_transferout_male: this.fetch_t_out[2].transfer,
                 pupilflow_jss2_transferout_female: this.fetch_t_out[3].transfer,
                 pupilflow_jss3_transferout_male: this.fetch_t_out[4].transfer,
                 pupilflow_jss3_transferout_female: this.fetch_t_out[5].transfer,

                 pupilflow_jss1_promoted_male: this.fetch_pro[0].promoted,
                 pupilflow_jss1_promoted_female: this.fetch_pro[1].promoted,
                 pupilflow_jss2_promoted_male: this.fetch_pro[2].promoted,
                 pupilflow_jss2_promoted_female: this.fetch_pro[3].promoted,
                 pupilflow_jss3_promoted_male: this.fetch_pro[4].promoted,
                 pupilflow_jss3_promoted_female: this.fetch_pro[5].promoted,
                 
                 
               
             }
             
                 
             ).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         alert("Data Saved to DB");
                         console.log(JSON.stringify(this.fetch_drop, null, 2));
                         console.log(JSON.stringify(this.fetch_t_in, null, 2));
                         console.log(JSON.stringify(this.fetch_t_out, null, 2));
                         console.log(JSON.stringify(this.fetch_pro, null, 2));
                         //console.log("0" + this.fetch_r[0].repeaters);
 
                     } else if(this.resultsSave.status == "error") {
                         alert(response.data.message);
                     }else {
                         alert("Check the form for the required data");
                     }
 
 
 
 
 
                 })
                 
 
 
         },

         saveExamination: function () {
            

                axios.post("http://127.0.0.1:8000/api/Section/SchoolEnrollment/Examination",
                {
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    examination: "jsce",

                    examination_jsce_registeredmale: this.registered_male,
                    examination_jsce_registeredfemale: this.registered_female,
                    examination_jsce_tookpartmale: this.took_part_male,
                    examination_jsce_tookpartfemale: this.took_part_female,
                    examination_jsce_passedmale: this.passed_male,
                    examination_jsce_passedfemale: this.passed_female

                    }).then(response => {

                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            alert(this.resultsSave.message);
                        } else {
                            alert(this.resultsSave.message);
                        }





                    })
                }


    }
});
