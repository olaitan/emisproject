  //vue vm for the form

  var app = new Vue({
    el: '#app',
    data: {
        
        resultsSave: [],

        //school identification
        user_email: '',
        user_fullname: '',
        user_role:'',
        find_user_email:'',
        censusyear: '',
        new_censusyear: '',
        delete_user_email: '',
        auth_user:'',
        config:'',
        fetch_yearslist:[],
        fetch_states:[],
        theme_state:''
    },

    mounted: function(){
        if(this.$cookies.isKey("user")){
            //initialized the years array
            var dt=new Date();
            for(i=1860;i<dt.getFullYear();i++){
                this.fetch_yearslist.push(i);
            }
            //get the access token
            this.user=this.$cookies.get("user");
            this.config = {
                headers: {
                  Accept: "application/json",
                  Authorization:"Bearer "+this.user.session,
                }
              }

            //alert(this.$cookies.get("theme"));
            //setting the theme color
            var leftTab = document.querySelectorAll('.theme');
            for (let i = 0; i < leftTab.length; i++) {
                leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
            } 

            axios.post("/api/Details",
            {
                email:this.user.email,
            },this.config).then(response => {
                //console.log(JSON.stringify(response.data, null, 2));
                this.auth_user=response.data;
            }).catch(error=>{
                this.$cookies.remove("user");
                window.location.assign("/login");
            })

            axios.get("/api/CensusYear/Current").then(response => {
                //console.log(JSON.stringify(response.data, null, 2));
                this.censusyear=response.data;
                this.new_censusyear=this.censusyear.Year;
            })

            //get api for states
            axios.get("/api/states").then(response => {

                console.log(JSON.stringify(response.data, null, 2));
    
                this.fetch_states=response.data;
                
            })
        }else{
            window.location.assign("/login");
        }
        
    },

    methods:{
    //log out
    logout:function(){
        axios.post("/api/Section/User/Logout",
        {
            email:this.user_email,
        },this.config).then(response => {
            console.log(JSON.stringify(response.data, null, 2));
            this.$cookies.remove("user");
            window.location.assign("/login");
        })
    },

setTheme:function(){
    //save to cookie if the state is same with the admin
    var themeColor = document.querySelector('#colorTheme');
    if(themeColor.value!=null){
        //send the color and selected state to the server
        alert("run axios");
        axios.post("/api/State/Theme",
        {
            theme:themeColor.value,
            state:this.theme_state
        },this.config).then(response => {
            this.resultsSave = response.data;
            if (this.resultsSave.status == "success") {
                var leftTab = document.querySelectorAll('.theme');
                for (let i = 0; i < leftTab.length; i++) {
                    leftTab[i].style.setProperty('--themeColor', themeColor.value);
                } 
                this.$cookies.set('theme',themeColor.value,"35d");
            }else{
                alert(this.resultsSave.message);
            }
            
        })
    }else{
        alert("Pick a theme");
    }
    
    
},
setCensusYear: function(){
    //add to the table
    //post to the savestaff api
    axios.post("/api/Section/CensusYear/Set",
    {
        old_censusyear:this.censusyear.Year,
        new_censusyear: this.new_censusyear,
        
    }).then(response => {
            this.resultsSave = response.data;
            if (this.resultsSave.status == "success") {
                console.log("success");
                configAlert = document.getElementById("alertUsers");
                configAlert.classList.add("active");
                setTimeout(() => {
                    configAlert.classList.remove('active'); 
                }, 2000);
                document.getElementById("info").innerHTML = "success";
                this.censusyear.Year=this.new_censusyear;
            } else {
                alert(this.resultsSave.message);
            }
    })
},
},

    

    
});