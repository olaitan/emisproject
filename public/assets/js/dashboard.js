  //vue vm for the form

  var app = new Vue({
    el: '#app',
    data: {
        fetch_roles: [],
        resultsSave: [],
        //school identification
        user_email: '',
        user_role:'',
        find_user_email:'',
        change_user_role: '',
        delete_user_email: '',
        auth_user:'',
        config:'',
        color: '',
        total_schools:0,
        public_schools:0,
        private_schools:0,
        states_name:[],
        states_public_sch:[],
        states_private_sch:[]
    },

    mounted: function(){
        if(this.$cookies.isKey("user")){
            //get the access token
            this.user=this.$cookies.get("user");
            this.config = {
                headers: {
                  Accept: "application/json",
                  Authorization:"Bearer "+this.user.session,
                }
              }

            axios.post("/api/Details",
            {
                email:this.user.email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.auth_user=response.data;
            }).catch(error=>{
                this.$cookies.remove("user");
                window.location.assign("/login");
            })

            //setting the theme color
            var leftTab = document.querySelectorAll('.theme');
            for (let i = 0; i < leftTab.length; i++) {
                leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
            } 

            axios.get("/api/roles").then(response => {

                console.log(JSON.stringify(response.data, null, 2));
    
                this.fetch_roles=response.data;
                
            })

            axios.post("/api/reports/overview",
            {
                census_year:'2016',
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                var chartData=response.data.data.data;
                this.total_schools=chartData["total_schools"];
                this.public_schools=chartData["public_schools"];
                this.private_schools=chartData["private_schools"];
                this.states_name=chartData.states.map(function(state){
                    return state.data.name;
                });
                this.states_public_sch=chartData.states.map(function(state){
                    return state.data.public_schools;
                });
                this.states_private_sch=chartData.states.map(function(state){
                    return state.data.private_schools;
                });

                var ctx1 = document.getElementById('pie1').getContext('2d');
                var ctx2 = document.getElementById('pie2').getContext('2d');
                var ctx3 = document.getElementById('line').getContext('2d');
        
                // And for a doughnut chart
                var myDoughnutChartPublic = new Chart(ctx1, {
                    type: 'doughnut',
                    data: {
                        labels: ['Pre-primary and primary', 'Jss and Sss'],
                        datasets: [{
                            data: [chartData["public_preprimary_primary"], chartData["public_js_ss"]],
                            backgroundColor: [
                                'rgba(255, 99, 132)',
                                'rgba(54, 162, 235)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        cutoutPercentage:30
                    }
                });
        
                var myDoughnutChartPrivate = new Chart(ctx2, {
                    type: 'doughnut',
                    data: {
                        labels: ['Pre-primary', 'Primary', 'Jss','Sss'],
                        datasets: [{
                            data: [chartData["private_preprimary"], chartData["private_primary"], chartData["private_js"],chartData["private_ss"]],
                            backgroundColor: [
                                'rgba(255, 99, 132)',
                                'rgba(54, 162, 235)',
                                'rgba(255, 206, 86)',
                                'rgba(255, 16, 106)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(255, 16, 106,1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        cutoutPercentage:30
                    }
                });
        
                var myLineChart = new Chart(ctx3, {
                    type: 'line',
                    data: {
                        labels: this.states_name,
                        datasets: [{
                            label:'Public Schools',
                            data: this.states_public_sch,
                            backgroundColor:'rgba(255, 99, 132)',
                            borderColor: 'rgba(255, 99, 132, 1)',
                            borderWidth: 1
        
                        },
                        {
                            label:'Private Schools',
                            data: this.states_private_sch,
                            backgroundColor:'rgba(54, 162, 235)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1
        
                        }]
                    },
                    options: {}
                });
            }).catch(error=>{
                
            })

        }else{
            window.location.assign("/login");
        }
        

    },

    methods:{

        //log out
        logout:function(){
            axios.post("/api/Section/User/Logout",
            {
                email:this.user_email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.$cookies.remove("user");
                window.location.assign("/login");
            })
        },
        addUser: function(){
            //add to the table
            
            //post to the savestaff api
            axios.post("/api/Section/User/Addnew",
            {
                
                email: this.user_email,
                Id_Role:this.user_role,
                
            }).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        console.log("success");
                        console.log(this.resultsSave.success.token);
                        console.log(this.resultsSave.success['email']);
        
                    } else {
                        alert(this.resultsSave.message);
                    }
            })
        },

        removeUser:function(){

            axios.post("/api/Section/User/Remove",
            {
                
            
                email: this.delete_user_email,
    
                }).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                    } else {
                        alert(this.resultsSave.message);
                    }
                })
        },

        changeUserRole:function(){

            axios.post("/api/Section/User/Change",
            {
                
            
                email: this.find_user_email,
                role:this.change_user_role,
    
                }).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        alert(this.resultsSave.message);
                    } else {
                        alert(this.resultsSave.message);
                    }
                })
        },
    },

    

    
});