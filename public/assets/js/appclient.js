  //vue vm for the form
var app = new Vue({
    el: '#app',
    data: {
        fetch_roles: [],
        resultsSave: [],
        //school identification
        user_email: '',
        user_role:'',
        find_user_email:'',
        change_user_role: '',
        delete_user_email: '',
        auth_user:'',
        config:'',
        color: '',
        token:'',


    },

    mounted: function(){
        if(this.$cookies.isKey("appclient")){
            //get the access token
            this.user=this.$cookies.get("appclient");
            this.token=this.user.session;
            this.user_email=this.user.email;
            this.config = {
                headers: {
                  Accept: "application/json",
                  Authorization:"Bearer "+this.user.session,
                }
              }
//setting the theme color
var leftTab = document.querySelectorAll('.theme');
for (let i = 0; i < leftTab.length; i++) {
    leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
} 
            axios.post("/api/Details",
            {
                email:this.user.email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.auth_user=response.data;
            }).catch(error=>{
                this.$cookies.remove("user");
                window.location.assign("/login");
            })
        }else{
            window.location.assign("/api");
        }
        
    },

    methods:{

        //log out
        logout:function(){
            //this.$cookies.remove("appclient");
            //window.location.assign("/api");

            axios.post("/api/Section/AppClient/Logout",
            {
                email:this.user_email,
            },this.config).then(response => {
                
                console.log(JSON.stringify(response.data, null, 2));
                this.$cookies.remove("appclient");
                window.location.assign("/api");
                
            })
            
        },
      
    },

    

    
});