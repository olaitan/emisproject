const buttonAddRoles = document.getElementById("addRoles");
const buttonRevoke = document.getElementById("revoke");
const buttonDelete = document.getElementById("delete");
const configAlert = document.getElementById("alertUsers");
function showAlert(){
    configAlert.classList.add("active");
    setTimeout(() => {
        configAlert.classList.remove('active'); 
    }, 2000);
}
buttonDelete.addEventListener('click', (e) =>{
    e.preventDefault();
    showAlert();
    document.getElementById("info").innerHTML = "User deleted";
})
buttonAddRoles.addEventListener('click', (e) =>{
    e.preventDefault();
    //showAlert();
    //document.getElementById("info").innerHTML = "User Created Successfully";
})
buttonRevoke.addEventListener('click', (e) =>{
    e.preventDefault();
    showAlert();
    document.getElementById("info").innerHTML = "User access revoked";
})