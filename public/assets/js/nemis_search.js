var vmForm = new Vue({
  /* el: '#content', */
  el: "#app",
  data: {
    json: null,
    show: false,
    type: 1,
    schoolcode: "",
    schoolname: "",
    state: "",
    lga: "",
    village: "",
    telephone: "",
    email: "",
    yearofest: "",
    location: "",
    loeo: "",
    typschool: "",
    ownership: "",
    results: "",
    custdet: [],
    schoolcode: "",
    arr: [],
    dis: [],
    schools: [],
    codes: [],
    locations: [],
    levels: [],
    forms: [],
    links: [],
    names: [],
    descs: [],
    fetch_year: [],
    fetch_states: [],
    fetch_lga: [],
    results: [],
    year_picked: "2016",
    state_picked: "",
    lga_picked: "",
    schoolstate: "",
    auth_user: "",
    config: "",
    fetch_yearslist:[],
    user_role:0,
    user_state:'',
    search_result:'',
  },
  mounted: function() {
    if (this.$cookies.isKey("user")) {
      var dt=new Date();
      for(i=1860;i<dt.getFullYear();i++){
          this.fetch_yearslist.push(i);
      }
      //get the access token
      this.user = this.$cookies.get("user");
      this.config = {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + this.user.session
        }
      };
      //setting the theme color
      var leftTab = document.querySelectorAll(".theme");
      for (let i = 0; i < leftTab.length; i++) {
        leftTab[i].style.setProperty(
          "--themeColor",
          this.$cookies.get("theme")
        );
      }
      axios
        .post(
          "/api/Details",
          {
            email: this.user.email
          },
          this.config
        )
        .then(response => {
          console.log(JSON.stringify(response.data, null, 2));
          this.auth_user = response.data;
          this.user_role=this.auth_user.role_id;
          this.user_state=this.auth_user.state;
        }).catch(error=>{
          this.$cookies.remove("user");
          window.location.assign("/login");
      });

      axios.get("/api/states").then(response => {
        this.fetch_states = response.data; //returns the json data

        //this.results =  JSON.parse(response.data);
        //console.log(this.results);
        console.log("r: ", JSON.stringify(response.data, null, 2));
      });
      //set the spinner to stop loading

      $("div#loader").addClass("hide");
    } else {
      //this.$cookies.remove("user");
      window.location.assign("/login");
    }
  },

  methods: {
    //log out
    logout: function() {
      axios
        .post(
          "/api/Section/User/Logout",
          {
            email: this.user_email
          },
          this.config
        )
        .then(response => {
          console.log(JSON.stringify(response.data, null, 2));
          this.$cookies.remove("user");
          window.location.assign("/login");
        });
    },
    getLgas: function() {
      var arrayOfObjects2 = [];

      for (var i = 0; i < this.fetch_states[this.state].data.lgas.length; i++) {
        var obj2 = {};
        obj2["id"] = this.fetch_states[this.state].data.lgas[i].data.id;
        obj2["name"] = this.fetch_states[this.state].data.lgas[i].data.name;
        obj2["lgacode"] = this.fetch_states[this.state].data.lgas[
          i
        ].data.lgacode;
        arrayOfObjects2.push(obj2);
      }

      this.fetch_lga = arrayOfObjects2;
    },

    showCode: function(pess) {
      console.log(pess.substr(pess.indexOf("SCH0"), pess.indexOf(" ") + 1));
      var code = pess.substr(pess.indexOf("SCH0"), pess.indexOf(" ") + 1);
      //document.cookie = "school_code=" + pess.substr(pess.indexOf("SCH0"), (pess.indexOf(" ") + 1));
      this.$cookies.set(
        "school_code",
        pess.substr(pess.indexOf("SCH0"), pess.indexOf(" ") + 1),
        Infinity
      );

      //var cookieSet = this.setCookie("SchoolCode", pess.substr(pess.indexOf("SCH0"), (pess.indexOf(" ") + 1)), 30);
      //alert(getCookie("SchoolCode"));
      //alert(code);
    },

    showCookie: function() {
      //var cookieGet = this.getCookie("SchoolCode")

      var cookieGet = this.$cookies.get("school_code");

      alert("cookie:" + this.$cookies.isKey("school_code") + " " + cookieGet);
    },

    advanceSearch: function() {
      var x = document.querySelectorAll(".hS");
      x.forEach(element => {
        element.classList.toggle("hiddenSearch");
      });
    },

    showdata: function() {
      document.querySelector("#loader").classList.remove("hide");
      document.querySelector("#loader").classList.add("show");
      var targetForm = $("#searchForms");
      var urlWithParams = targetForm.attr("action") + "?" + targetForm.serialize();
      console.log("/api/search/schools" + urlWithParams);
      axios.post("/api/search/schools" + urlWithParams,{},this.config).then(response => {
          console.log(JSON.stringify(response.data, null, 2));
          this.search_result = response.data;
         
          table.clear();
          table.rows.add(this.search_result.data);
          table.draw();
      
          setTimeout(() => {
            document.querySelector("#loader").classList.remove("show");
            document.querySelector("#loader").classList.add('hide');
            this.show = !this.show;
          }, 1000);
            });
      //table.ajax.url("/api/search/schools" + urlWithParams).load();
      
    }
  }
});
