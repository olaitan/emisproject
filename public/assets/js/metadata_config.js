

//vue vm for the form

var app = new Vue({
    el: '#app',
    data: {
        results: [],
        fetch_metadata: [],
        resultsSave: [],
        //school identification
        schoolcode: '',
        category:'',                                                
        auth_user:'',
        config:'',
        fetch_states:[]
    },

    computed:{
        
    },

    watch:{
        
    },

    mounted: function(){
        if(this.$cookies.isKey("user")){
            //get the access token
            this.user=this.$cookies.get("user");
            this.config = {
                headers: {
                  Accept: "application/json",
                  Authorization:"Bearer "+this.user.session,
                }
              }

              //setting the theme color
            var leftTab = document.querySelectorAll('.theme');
            for (let i = 0; i < leftTab.length; i++) {
                leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
            } 
            
            axios.post("/api/Details",
            {
                email:this.user.email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.auth_user=response.data;
            }).catch(error=>{
                this.$cookies.remove("user");
                window.location.assign("/login");
            })

            axios.get("/api/states").then(response => {
                this.fetch_states = response.data; //returns the json data
        
                //this.results =  JSON.parse(response.data);
                
                console.log("r: ", JSON.stringify(response.data, null, 2));
              });

            axios.get("/api/metadatas",this.config).then(response => {
                this.results = response.data;//returns the json data
    
               
                //this.results =  JSON.parse(response.data);
                //console.log(this.results);
                console.log('r: ', JSON.stringify(response.data, null, 2));
                
                
            });

        }else{
            window.location.assign("/login");
        }

        
    },

    methods:{
//log out
//log out
logout:function(){
    axios.post("/api/Section/User/Logout",
    {
        email:this.user_email,
    },this.config).then(response => {
        console.log(JSON.stringify(response.data, null, 2));
        this.$cookies.remove("user");
        window.location.assign("/login");
    })
},
        getMetadatas: function () {
            var arrayOfObjects2 = [];

            //this.counter_state = this.state_index[this.state_picked];
            //alert(this.results[this.category].data.metadatas[0].data.value);

            for (var i = 0; i < this.results[this.category].data.metadatas.length; i++) {
                var obj2 = {};
                obj2["id"] = this.results[this.category].data.metadatas[i].data.id;
                obj2["value"] = this.results[this.category].data.metadatas[i].data.value;
                obj2["display"] = this.results[this.category].data.metadatas[i].data.display;
                obj2["description"] = this.results[this.category].data.metadatas[i].data.description;
                obj2["order"] = this.results[this.category].data.metadatas[i].data.order;
                obj2["level"] = this.results[this.category].data.metadatas[i].data.level;
                obj2["state"] = this.results[this.category].data.metadatas[i].data.state;
                //this.lga_index[this.results[1].attributes.states[i].data.lgas[i].data.name] = i;
            

                arrayOfObjects2.push(obj2);
            }
            //this.lga_picked=this.results[1].attributes.states[this.counter_state].data.lgas[0].data.name;
            arrayOfObjects2.sort(function(a,b){return a.order-b.order});
            this.fetch_metadata = arrayOfObjects2;


            //console.log('now: ', JSON.stringify(this.fetch_states, null, " "));
            //alert(this.lga_picked);
        },

        saveMetadata:function(){
            console.log(this.results[this.category].data.name);
            axios.post("/api/Metadata/Save",
                        {
                            category:this.results[this.category].data.name,
                            metadatas: this.fetch_metadata,
                            
                            },this.config).then(response => {
                                this.resultsSave = response.data;
                                if (this.resultsSave.status == "success") {
                                    //alert(this.resultsSave.message);
                                } else {
                                    alert(this.resultsSave.message);
                                }
                        });
        },

        addMetadata:function(){
            //alert("here");
            var meta_obj={id:"",value:"",display:"",description:"",order:""};
            this.fetch_metadata.push(meta_obj);
        },

        deleteMetadata:function(metadataindex){
            //alert(this.fetch_metadata[metadataindex].id);
            axios.post("/api/Metadata/Delete",
                        {
                            
                            metadata_id: this.fetch_metadata[metadataindex].id,
                            
                            },this.config).then(response => {
                                this.resultsSave = response.data;
                                if (this.resultsSave.status == "success") {
                                    //alert(this.resultsSave.message);
                                    this.fetch_metadata.splice(metadataindex,1);
                                } else {
                                    alert(this.resultsSave.message);
                                }
                        })
        }

    },
});