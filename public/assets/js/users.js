const more = document.getElementById("showDetails");
if (more) {
  more.addEventListener("click", e => {
    e.preventDefault();
    const moreActs = document.getElementById("users-links");
    moreActs.classList.toggle("active");
  });
}

$("#accountsTab a").on("click", function(e) {
  e.preventDefault();
  $("#accountsTab a").removeClass("active");
  $("div.tabPane").removeClass("active");
  const nextTab = $(this).attr("href");
  $(this).addClass("active");
  $("div" + nextTab).addClass("active");
});

/* Profile Picture */
function prev() {
  var preview = document.querySelector("#userPic");
  var file = document.querySelector("input[type=file]").files[0];

  //var form_data = new FormData();
  //form_data.append('file', img.files[0]);

  if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
    var reader = new FileReader();
    reader.addEventListener(
      "load",
      function() {
        preview.src = reader.result;
      },
      false
    );

    reader.readAsDataURL(file);
  }
}

/* Edit Profile Page */
const userAlert = document.querySelector("#usealerts");
function changer() {
  userAlert.classList.add("active");
  document.getElementById("message").innerHTML = "Saving...";
}
let editButton = document.querySelector("#editProfile");
if (editButton) {
  editButton.addEventListener("click", () => {
    changer();
    setTimeout(function() {
      document.getElementById("message").innerHTML = "Changes Saved.";
      /* Tayo should change the content of message here on successful upload */
      userAlert.classList.remove("active");
    }, 2500);
  });
}

let passwordButton = document.querySelector("#savePWD");
if (passwordButton) {
  passwordButton.addEventListener("click", () => {
    changer();
    setTimeout(function() {
      document.getElementById("message").innerHTML = "Password Saved.";
      /* Tayo should change the content of message here on successful upload */
      userAlert.classList.remove("active");
    }, 2500);
  });
  document.getElementById("message").innerHTML = "";
}
