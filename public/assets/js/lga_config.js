

//vue vm for the form

var app = new Vue({
    el: '#app',
    data: {
        results: [],
        fetch_lga: [],
        resultsSave: [],
        //school identification
        schoolcode: '',
        state:'',                                                
        auth_user:'',
        config:'',
    },

    computed:{
        
    },

    watch:{
        
    },

    mounted: function(){
        if(this.$cookies.isKey("user")){
            //get the access token
            this.user=this.$cookies.get("user");
            this.config = {
                headers: {
                  Accept: "application/json",
                  Authorization:"Bearer "+this.user.session,
                }
              }

              //setting the theme color
            var leftTab = document.querySelectorAll('.theme');
            for (let i = 0; i < leftTab.length; i++) {
                leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
            } 
            
            axios.post("/api/Details",
            {
                email:this.user.email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.auth_user=response.data;
            }).catch(error=>{
                this.$cookies.remove("user");
                window.location.assign("/login");
            })

            axios.get("/api/states").then(response => {
                this.results = response.data;//returns the json data
    
                //this.results =  JSON.parse(response.data);
                //console.log(this.results);
                console.log('r: ', JSON.stringify(response.data, null, 2));
                
            });

        }else{
            window.location.assign("/login");
        }
    },

    methods:{
        //log out
        logout:function(){
            axios.post("/api/Section/User/Logout",
            {
                email:this.user_email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.$cookies.remove("user");
                window.location.assign("/login");
            })
        },
        
        getLgas: function () {
            var arrayOfObjects2 = [];

            for (var i = 0; i < this.results[this.state].data.lgas.length; i++) {
                var obj2 = {};
                obj2["id"] = this.results[this.state].data.lgas[i].data.id;
                obj2["name"] = this.results[this.state].data.lgas[i].data.name;
                obj2["lgacode"] = this.results[this.state].data.lgas[i].data.lgacode;
                arrayOfObjects2.push(obj2);
            }
            
            this.fetch_lga = arrayOfObjects2;
        },

        saveLga:function(){
            axios.post("/api/Lga/Save",
            {
                state:this.results[this.state].data.name,
                lgas: this.fetch_lga,
                
                }).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                    } else {
                        alert(this.resultsSave.message);
                    }
            })
        },

        addLga:function(){
            //alert("here");
            var lga_obj={id:"",name:"",lgacode:""};
            this.fetch_lga.push(lga_obj);
        },

        deleteMetadata:function(lgaindex){
            //alert(this.fetch_metadata[metadataindex].id);
            axios.post("/api/Lga/Delete",
            {   
                lga_id: this.fetch_lga[lgaindex].id,
                
                }).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        this.fetch_lga.splice(lgaindex,1);
                    } else {
                        alert(this.resultsSave.message);
                    }
            })
        }

    },
});