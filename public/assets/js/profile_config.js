  //vue vm for the form

  var app = new Vue({
    el: '#app',
    data: {
        fetch_roles: [],
        resultsSave: [],
        //school identification
        user_email: '',
        user_fullname: '',
        user_role:'',
        find_user_email:'',
        change_user_role: '',
        delete_user_email: '',
        auth_user:'',
        config:'',
        current_pwd:'',
        new_pwd:'',
        confirm_pwd:'',
        new_email:'',
        user_pic:''
    },

    mounted: function(){
        if(this.$cookies.isKey("user")){
            //get the access token
            this.user=this.$cookies.get("user");
            this.config = {
                headers: {
                  Accept: "application/json",
                  Authorization:"Bearer "+this.user.session,
                }
              }
//setting the theme color
var leftTab = document.querySelectorAll('.theme');
for (let i = 0; i < leftTab.length; i++) {
    leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
} 
            axios.post("/api/Details",
            {
                email:this.user.email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.auth_user=response.data;
                this.new_email=this.auth_user.email;
                
            }).catch(error=>{
                this.$cookies.remove("user");
                window.location.assign("/login");
            })

            axios.get("/api/roles").then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.fetch_roles=response.data;
            })
        }else{
            //this.$cookies.remove("user");
            window.location.assign("/login");
        }
        
    },

    methods:{
//log out
logout:function(){
    axios.post("/api/Section/User/Logout",
    {
        email:this.user_email,
    },this.config).then(response => {
        console.log(JSON.stringify(response.data, null, 2));
        this.$cookies.remove("user");
        window.location.assign("/login");
    })
},
        saveProfile: function(){
            //add to the table
            var file = document.querySelector("input[type=file]").files[0];

            var form_data = new FormData();
            form_data.append('file', file);

            console.log("sending the upload request");
            axios.post("/api/Section/User/SaveProfilePic",form_data,this.config).then(response=>{
                console.log("successfully updated image");
            });
            console.log("upload request sent")
            //post to the savestaff api
            axios.post("/api/Section/User/SaveProfile",
            {
                name:this.auth_user.name,
                email: this.new_email,
                older_email:this.auth_user.email,
                
            },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        console.log("success");
                        configAlert = document.getElementById("alertUsers");
                        configAlert.classList.add("active");
                        setTimeout(() => {
                            configAlert.classList.remove('active'); 
                        }, 2000);
                        document.getElementById("info").innerHTML = "success";
                        //console.log(this.resultsSave.success['email']);
        
                    } else {
                        console.log(this.resultsSave.message);
                    }
            })
        },

        savePwd:function(){
            //check if the new and confirm password match
            if(this.new_pwd==this.confirm_pwd){
                axios.post("/api/Section/User/SavePassword",
                {
                email: this.new_email,
                current_password: this.current_pwd,
                password:this.new_pwd,
                c_password:this.confirm_pwd,
    
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        console.log("success");
                        configAlert = document.getElementById("alertUsers");
                        configAlert.classList.add("active");
                        setTimeout(() => {
                            configAlert.classList.remove('active'); 
                        }, 2000);
                        document.getElementById("info").innerHTML = "success";
                    } else {
                        console.log(this.resultsSave.message);
                    }
                })
            }else{
                console.log("new password does not match");
            }

            
        },

    },

    

    
});