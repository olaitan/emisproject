let btnLogin = document.getElementById('login');
let spin = document.getElementById('spinner')
let text = document.getElementById('text');
btnLogin.addEventListener('click', function(e){
    e.preventDefault();
    spin.classList.add('active');
    text.innerHTML = '';
})

let btnSignUp = document.getElementById('signup');
let spinn = document.getElementById('spinn')
let stext = document.getElementById('stext');
btnSignUp.addEventListener('click', function(e){
    e.preventDefault();
    spinn.classList.add('active');
    stext.innerHTML = '';
})

var links = document.querySelectorAll('ul>li>a');
for(var i = 0; i<links.length; i++){    
    links[i].addEventListener('click', function(el){
        el.preventDefault();
        var url = this.getAttribute('href');
        if(url === '#loginForm'){
            document.querySelector('#up').classList.remove('active');
            this.classList.add('active');
            document.querySelector('#signUpForm').classList.remove('active')
            document.querySelector(url).classList.add('active');
        } else if(url === '#signUpForm'){
            document.querySelector('#log').classList.remove('active');
            this.classList.add('active');
            document.querySelector('#loginForm').classList.remove('active')
            document.querySelector(url).classList.add('active');
        }
    })
}