let saveYear = document.querySelector('#saveCensusYear');
let saveColor = document.querySelector('#saveColor');
let configsAlert = document.querySelector('#alertUsers');
let msg = document.querySelector('#info');

function alertConfig(){
    configsAlert.classList.add('active');
}

function clearAlert(){
    setTimeout(() => {
        configsAlert.classList.remove('active');
    }, 1500);
}

saveYear.addEventListener('click', ()=>{
    alertConfig();
    msg.innerHTML = 'Census Year Saved!';
    clearAlert();
})

saveColor.addEventListener('click', ()=>{
    alertConfig();
    msg.innerHTML = 'Color Saved!'
    clearAlert();
})