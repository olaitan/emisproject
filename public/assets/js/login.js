const searchButton = document.getElementById("search");
const buttonNewLogin = document.getElementById("newlogin");
const buttonLogin = document.getElementById("login");

/* Different Forms */
const searchForm = document.getElementById("searchUser");
const createPWD = document.getElementById("createPwd");
const loginForm = document.getElementById("loginDetails");

/* Change Active Form Section */
function changeForm(){
    let active = document.getElementsByClassName("active");
    active.removeClass("active");
}

//Spinner

const spinIcon = document.querySelectorAll("#spinner");
//Search Button
searchButton.addEventListener('click', (e) =>{
    e.preventDefault();
    spinIcon[0].classList.add('active');
    setTimeout(() => {
        searchForm.classList.remove("active");
        createPWD.classList.add("active");
    }, 2000)  
    
})
//New Login Button
buttonNewLogin.addEventListener('click', (e) => {
    const newPassword = document.getElementById("newpassword");
    const confirmPassword = document.getElementById("confirmpassword");
    e.preventDefault();
    if ((newPassword.value)!==(confirmPassword.value)){
        newPassword.classList.add("error");
        confirmPassword.classList.add("error");
    } else if((newPassword.value == '') && (confirmPassword.value == '')){
        newPassword.classList.add("error");
        confirmPassword.classList.add("error");
    } else{
        newPassword.classList.add("correct");
        confirmPassword.classList.add("correct");
        spinIcon[1].classList.add('active');
    }
    
})
//LoginFunction
buttonLogin.addEventListener('click', () =>{
    spinIcon[2].classList.add('active');
})