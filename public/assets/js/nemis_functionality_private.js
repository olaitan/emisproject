
var url = window.location.href;//get the school code and year by splitting the url
var res = url.split("/");
var schoolcode = res[4];
var year = res[6];

//vue vm for the form

var app = new Vue({
    el: '#app',
    data: {
        //School Identification errors
        school_identification_errors: [],

        responseMsg: '',
        results: [],
        resultsSave: [],
        //school identification
        schoolcode: '',
        primary_schoolcode: '',
        jss_schoolcode: '',
        sss_schoolcode: '',
        schoolname: '',
        xcoordinate:'',
        ycoordinate:'',
        zcoordinate:'',
        schoolstreet: '',
        schoolemail: '',
        schoollga: '',
        schooltown: '',
        schooltelephone: '',
        schoolward: '',
        schoolstate: '',
        proprietor:'',
        

        fetch_states: [],
        state_picked: '',
        state_index: [],
        get_state_code: '',

        results_lga: [],
        fetch_lga: [],
        lga_picked: '',
        lga_index: [],
        get_lga_code: '',

        counter_state: 0,
        counter_lga: 0,

        location_type_ref: [],
        location_type_val: [],
        metadata: [],

        //metadata arrays
        fetch_location: [],
        fetch_owner:[],
        fetch_recognition_status:[],
        fetch_levels: [],
        fetch_category: [],
        fetch_authority: [],
        fetch_ownership: [],
        fetch_birth_certificate: [],
        fetch_b_c: [],
        fetch_b_c_no: [],
        fetch_age: [],
        fetch_primary_age: [],
        fetch_age_primary: [],
        fetch_orphans: [],
        fetch_ss_age: [],
        fetch_a: [],
        fetch_a_no: [],
        fetch_streams: [],
        fetch_Ss_streams: [],
        fetch_year_by_age: [],
        fetch_preprimary_year_by_age: [],
        fetch_ss_year_by_age: [],
        fetch_year_by_age_no: [],
        fetch_y_b_a1: [], fetch_y_b_a2: [], fetch_y_b_a3: [],
        fetch_y_b_no1: [], fetch_y_b_no2: [], fetch_y_b_no3: [],
        fetch_all_ages: [],
        fetch_repeaters: [], fetch_r:[], 
        fetch_pupilflow: [],
        fetch_dropouts: [], fetch_drop:[],
        fetch_transfer_in: [], fetch_t_in:[],
        fetch_transfer_out: [], fetch_t_out:[],
        fetch_promoted: [], fetch_pro:[],
        fetch_examination:[],
        fetch_all_special_needs: [], fetch_special_needs: [], fetch_special_needs_item: [],
        fetch_s_n1: [], fetch_s_n2: [], fetch_s_n3: [],
        fetch_s_n_no1: [], fetch_s_n_no2: [], fetch_s_n_no3: [],
        fetch_a_s_n: [], fetch_a_s_n_no: [],

        inputParams: [],

        exam_type:'',
        registered_male:'',
        registered_female:'',
        registered_total:'', 
        took_part_male:'',
        took_part_female:'',
        took_part_total:'',
        passed_male:'',
        passed_female:'',
        passed_total:'',

        //school characteristics
        year: '',
        year_of_establishment: '',
        year_of_establishment_preprimary: '',
        year_of_establishment_jss: '',
        year_of_establishment_ss: '',
        year_of_establishment_primary: '',
        ownership:'',
        recognition_status:'',
        location_of_school: '',
        level_of_education: [],
        type_of_school: '',
        shifts_choice: '',
        facilities_choice: '',
        facilities_shared: '',
        multigrade_choice: '',
        multigrade_choice_bi: '',
        average_distance: '',
        student_distance: '',
        students_boarding_female: '',
        students_boarding_male: '',
        sdp_choice: '',
        is_psa:'',
        sdp_choice_bi: '',
        sbmc_choice: '',
        sbmc_choice_bi: '',
        boarding_choice:'',
        pta_choice: '',
        pta_choice_bi: '',
        date_inspection: '',
        no_of_inspection: '',
        authority_choice: '',
        cash_transfer: '',
        grants_choice: '',
        grants_choice_bi: '',
        guard_choice: '',
        guard_choice_bi: '',
        ownership_choice: '',

        //C4
        jss1_stream:'',
        jss2_stream:'',
        jss3_stream:'',
        jss1_stream_with_multigrade:'',
        jss2_stream_with_multigrade:'',
        jss3_stream_with_multigrade:'',
        jss1_repeaters_male: '',
        jss1_repeaters_female:'',
        jss2_repeaters_male:'',
        jss2_repeaters_female:'',
        jss3_repeaters_male:'',
        jss3_repeaters_female:'',
        
        prevyear_jss3_male:'',
        prevyear_jss3_female:'',

        //total
        entrantmaletotal:'',
        entrantfemaletotal:'',
        jss1maletotal:'',
        jss1femaletotal:'',
        jss2maletotal:'',
        jss2femaletotal:'',
        jss3maletotal:'',
        jss3femaletotal:'',
        ss1_entrantmaletotal:'',
        ss1_entrantfemaletotal:'',
        ss1maletotal:'',
        ss1femaletotal:'',
        ss2maletotal:'',
        ss2femaletotal:'',
        ss3maletotal:'',
        ss3femaletotal:'',
        kindergarten1_male_total:'',
        kindergarten1_female_total:'',
        kindergarten2_male_total:'',
        kindergarten2_female_total:'',
        nursery1_male_total:'',
        nursery1_female_total:'',
        nursery2_male_total:'',
        nursery2_female_total:'',
        nursery3_male_total:'',
        nursery3_female_total:'',

        p1_entrant_male_total:'',
        p1_entrant_female_total:'',
        p1_eccd_entrant_male_total:'',
        p1_eccd_entrant_female_total:'',

        yearbyage_p1_male_total:'',
        yearbyage_p1_female_total:'',
        yearbyage_p2_male_total:'',
        yearbyage_p2_female_total:'',
        yearbyage_p3_male_total:'',
        yearbyage_p3_female_total:'',
        yearbyage_p4_male_total:'',
        yearbyage_p4_female_total:'',
        yearbyage_p5_male_total:'',
        yearbyage_p5_female_total:'',
        yearbyage_p6_male_total:'',
        yearbyage_p6_female_total:'',

        ss1_stream:'',
        ss2_stream:'',
        ss3_stream:'',
        kindergarten1_stream:'',
        kindergarten2_stream:'',
        nursery1_stream:'',
        nursery2_stream:'',
        nursery3_stream:'',
        below3_kindergarten1_male:'',
        below3_kindergarten1_female:'',
        below3_kindergarten2_male:'',
        below3_kindergarten2_female:'',
        age3_kindergarten1_male:'',
        age3_kindergarten1_female:'',
        age3_kindergarten2_male:'',
        age3_kindergarten2_female:'',
        age3_nursery1_male:'',
        age3_nursery1_female:'',
        age3_nursery2_male:'',
        age3_nursery2_female:'',
        age3_nursery3_male:'',
        age3_nursery3_female:'',
        age4_nursery1_male:'',
        age4_nursery1_female:'',
        age4_nursery2_male:'',
        age4_nursery2_female:'',
        age4_nursery3_male:'',
        age4_nursery3_female:'',
        age5_nursery1_male:'',
        age5_nursery1_female:'',
        age5_nursery2_male:'',
        age5_nursery2_female:'',
        age5_nursery3_male:'',
        age5_nursery3_female:'',
        above5_nursery1_male:'',
        above5_nursery1_female:'',
        above5_nursery2_male:'',
        above5_nursery2_female:'',
        above5_nursery3_male:'',
        above5_nursery3_female:'',

        ss1_stream_with_multigrade:'',
        ss2_stream_with_multigrade:'',
        ss3_stream_with_multigrade:'',
        ss1_repeaters_male: '',
        ss1_repeaters_female:'',
        ss2_repeaters_male:'',
        ss2_repeaters_female:'',
        ss3_repeaters_male:'',
        ss3_repeaters_female:'',
        prevyear_sss3_male:'',
        prevyear_sss3_female:'',

        p1_stream:'',
        p2_stream:'',
        p3_stream:'',
        p4_stream:'',
        p5_stream:'',
        p6_stream:'',
        
        p1_stream_with_multigrade:'',
        p2_stream_with_multigrade:'',
        p3_stream_with_multigrade:'',
        p4_stream_with_multigrade:'',
        p5_stream_with_multigrade:'',
        p6_stream_with_multigrade:'',

        p1_repeaters_male: '',
        p1_repeaters_female:'',
        p2_repeaters_male:'',
        p2_repeaters_female:'',
        p3_repeaters_male:'',
        p3_repeaters_female:'',
        p4_repeaters_male: '',
        p4_repeaters_female:'',
        p5_repeaters_male:'',
        p5_repeaters_female:'',
        p6_repeaters_male:'',
        p6_repeaters_female:'',

        prevyear_primary6_male:'',
        prevyear_primary6_female:'',

        //breadcrumbs href
        schoolstate_href:'',
        lga_href:'',
        level_of_education_href:'',
        school_type_href:'',

        //tab
        bc_primary_tab:'tab3',
        bc_sec_tab:'tab4',
        pre_enr_tab:'tab5',
        pri_ent_tab:'tab6',
        pri_enr_tab:'tab7',
        pri_special_need_tab:'tab8',
        pri_vul_tab:'tab9',
        pri_flow_tab:'tab10',
        js_ent_tab:'tab11',
        js_enr_tab:'tab12',
        ss_ent_tab:'tab13',
        ss_enr_tab:'tab14',
        sec_flow_tab:'tab15',
        sec_special_need_tab:'tab16',

        //staff
        non_teaching_staff_male:'',
        non_teaching_staff_female:'',
        non_teaching_staff_total:'',
        teaching_staff_male:'',
        teaching_staff_female:'',
        teaching_staff_total:'',
        staff_id:'',
        staff_file_no:'',
        staff_name:'',
        staff_gender:'',
        staff_type:'',
        salary_source:'',
        staff_yob:'',
        staff_yfa:'',
        staff_ypa:'',
        staff_yps:'',
        staff_level:'',
        present:'',
        academic_qualification:'',
        teaching_qualification:'',
        area_specialisation:'',
        subject_taught:'',
        teaching_type:'',
        is_teaching_ss:'',
        attended_training:'',

        //fetch array for staff features
        fetch_teachingtype:[],
        fetch_subjecttaught:[],
        fetch_specialisation:[],
        fetch_academicqualification:[],
        fetch_teachingqualification:[],
        fetch_present:[],
        fetch_salarysource:[],
        fetch_stafftype:[],
        //fetch array for the staffs
        fetch_staffs:[],

        //Classrooms
        no_of_classrooms:'',
        classes_held_outside:'',
        fetch_otherRooms:[],
        fetch_drinkingwater_source:[],
        fetch_power_source:[],
        fetch_toilet_type:[],
        fetch_facilities:[],
        shared_facilities:[],
        

        //facilities
        sources_of_drinking_water:[],
        sources_of_power:[],
        fetch_healthfacility:[],
        health_facility:'',
        fence_facility:'',
        fetch_fence:[],
        fetch_mainsubjects:[],
        fetch_subjects:[],
        fetch_books:[],
        fetch_seaters:[],
        fetch_ownership:[],
        fetch_schoolbuilding:[],
        facility_ownership:'',
        facility_schoolbuilding:'',

        //teachers qualification
        tq_preprimary_male_total:'0',
        tq_preprimary_female_total:'0',
        tq_primary_male_total:'0',
        tq_primary_female_total:'0',
        
        tq_jss_male_total:'0',
        tq_jss_female_total:'0',
        tq_sss_male_total:'0',
        tq_sss_female_total:'0',
        tq_male_total:'0',
        tq_female_total:'0',

        ispreprimary:false,
        isprimary:false,
        isjss:false,
        issss:false,

        //undertaking
        attestation_headteacher_name:'',
        attestation_headteacher_telephone:'',
        attestation_headteacher_signdate:'',
        attestation_enumerator_name:'',
        attestation_enumerator_position:'',
        attestation_enumerator_telephone:'',
        attestation_supervisor_name:'',
        attestation_supervisor_position:'',
        attestation_supervisor_telephone:'',

        auth_user:'',
        config:'',
        fetch_yearslist:[],

        datapreviewurl:'',
        registered_censusyear:'',
    },

    mounted: function(){
        if(this.$cookies.isKey("user")){
            //initialized the years array
            var dt=new Date();
            for(i=1860;i<dt.getFullYear();i++){
                this.fetch_yearslist.push(i);
            }
            //get the access token
            this.user=this.$cookies.get("user");
            this.config = {
                headers: {
                  Accept: "application/json",
                  Authorization:"Bearer "+this.user.session,
                }
              }
            //setting the theme color
            var leftTab = document.querySelectorAll('.theme');
            for (let i = 0; i < leftTab.length; i++) {
                leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
            } 
            axios.post("/api/Details",
            {
                email:this.user.email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.auth_user=response.data;
            }).catch(error=>{
                this.$cookies.remove("user");
                window.location.assign("/login");
            })

            this.schoolcode=schoolcode;
            this.datapreviewurl="/school/" + schoolcode.toLowerCase() + "/year/" + year+"/preview";
        axios.get("/api/school/" + schoolcode.toLowerCase() + "/year/" + year,this.config).then(response => {
            this.results = response.data;//returns the json data

            
            //this.results =  JSON.parse(response.data);
            //console.log(this.results);
            if (this.results[0].type == "SchoolForm"){
                console.log('r: ', JSON.stringify(response.data, null, 2));
            
                //bind the data to the inputs
                //School Identification
                this.schoolname=this.results[0].data.school_identification["school_name"];
                this.registered_censusyear=year;
                this.schoolstreet=this.results[0].data.school_identification["address"];
                this.xcoordinate=this.results[0].data.school_identification["xcoordinate"];
                this.ycoordinate=this.results[0].data.school_identification["ycoordinate"];
                this.zcoordinate=this.results[0].data.school_identification["zcoordinate"];
                this.primary_schoolcode=this.results[0].data.school_identification["primary_school_code"];
                this.jss_schoolcode=this.results[0].data.school_identification["jss_school_code"];
                this.sss_schoolcode=this.results[0].data.school_identification["sss_school_code"];
                this.schoolemail=this.results[0].data.school_identification["email_address"];
                this.proprietor=this.results[0].data.school_identification["proprietor"];
                this.schoollga=this.results[0].data.school_identification["lga"];
                this.schooltown=this.results[0].data.school_identification["town"];
                this.schooltelephone=this.results[0].data.school_identification["school_telephone"];
                this.schoolward=this.results[0].data.school_identification["ward"];
                this.schoolstate=this.results[0].data.school_identification["state"];
    
                //School Characteristics
                //bind the metadata
                this.fetch_location=this.results[1].data.data.filter(function(metadata){
                        return metadata.reference=="Location Type" ;
                });
                //alert(this.fetch_location);
                this.fetch_levels=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="School Level";
                });
                
                this.fetch_category=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="School Category";
                });
                this.fetch_ownership=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Owner";
                });
                this.fetch_recognition_status=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Recognition Status";
                });
                this.fetch_authority=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Ownership - 2013";
                });
    
                //bind the data
                this.year_of_establishment_preprimary=this.results[0].data.school_characteristics["preprimary"];
                this.year_of_establishment_primary=this.results[0].data.school_characteristics["primary"];
                this.year_of_establishment_jss=this.results[0].data.school_characteristics["jss"];
                this.year_of_establishment_ss=this.results[0].data.school_characteristics["sss"];
                this.location_of_school=this.results[0].data.school_characteristics["location"];
                this.ownership=this.results[0].data.school_characteristics["ownership_status"];
                this.recognition_status=this.results[0].data.school_characteristics["recognition_status"];
                this.level_of_education=this.results[0].data.school_characteristics["levels_of_education_offered"];
                this.type_of_school=this.results[0].data.school_characteristics["type_of_school"];
                this.shifts_choice=this.results[0].data.school_characteristics["shifts"];
                this.facilities_choice=this.results[0].data.school_characteristics["shared_facilities"];
                this.facilities_shared=this.results[0].data.school_characteristics["sharing_with"];
                this.multigrade_choice=this.results[0].data.school_characteristics["multi_grade_teaching"];    
                this.average_distance=this.results[0].data.school_characteristics["school_average_distance_from_catchment_communities"];
                this.student_distance=this.results[0].data.school_characteristics["students_distance_from_school"];
                this.students_boarding_female=this.results[0].data.school_characteristics["students_boarding"].female;
                this.students_boarding_male=this.results[0].data.school_characteristics["students_boarding"].male;
                this.sdp_choice=this.results[0].data.school_characteristics["school_development_plan_sdp"];
                this.sbmc_choice=this.results[0].data.school_characteristics["school_based_management_committee_sbmc"];    
                this.pta_choice=this.results[0].data.school_characteristics["parents_teachers_association_pta"]; 
                this.is_psa=this.results[0].data.school_characteristics["is_psa"];  
                this.date_inspection=this.results[0].data.school_characteristics["date_of_last_inspection_visit"];
                this.no_of_inspection=this.results[0].data.school_characteristics["no_of_inspection"];//no of last inspection
                this.authority_choice=this.results[0].data.school_characteristics["authority_of_last_inspection"];
                this.cash_transfer=this.results[0].data.school_characteristics["conditional_cash_transfer"];
                this.grants_choice=this.results[0].data.school_characteristics["school_grants"];            
                this.guard_choice=this.results[0].data.school_characteristics["security_guard"];            
                this.ownership_choice=this.results[0].data.school_characteristics["ownership"];
                
    
                //bind the breadcrumbs href
                this.schoolstate_href='/generic/search?state='+encodeURI(this.schoolstate);
                this.lga_href='/generic/search?lga='+encodeURI(this.schoollga);
                this.school_type_href='/generic/search?state='+encodeURI(this.schoolstate)+'&'+'lga='+encodeURI(this.schoollga)+'&'+'schooltype='+encodeURI('private');
                //this.level_of_education_href='http://127.0.0.1:8000/generic/search?state='+encodeURI(this.schoolstate)+'&'+'lga='+encodeURI(this.schoollga)+'&'+'schooltype='+encodeURI('public')+'&'+'schoollevel='+encodeURI(this.level_of_education);
    
                //set school levels conditions
                this.ispreprimary = this.level_of_education.some(function(el){ return el === "Pre-primary"});
                this.isprimary = this.level_of_education.some(function(el){ return el === "Primary"});
                this.isjss = this.level_of_education.some(function(el){ return el === "Junior Secondary"});
                this.issss = this.level_of_education.some(function(el){ return el === "Senior Secondary"});
    
                //Apply the metadata to the form sections
                //bind the states list
                var arrayOfObjects = []
    
                for (var i = 0; i < this.results[1].attributes.states.length; i++) {//loop the states
                    var obj = {};
                    obj["name"] = this.results[1].attributes.states[i].data.name;
                    obj["code"] = this.results[1].attributes.states[i].data.statecode;
                   
                    //save the state name as a key and the value is the index
                    if(obj["name"]==this.schoolstate){
                        this.state_picked=i;
                    }
                    arrayOfObjects.push(obj);
                }
                //this.state_picked=this.results[1].attributes.states[0].data.name;
    
                this.fetch_states = arrayOfObjects;
               // console.log('r: ', JSON.stringify(this.fetch_states, null, 2));
                var arrayOfObjects2 = []
                var tempIndex=0;
    
                ////LGA
                for (var i = 0; i < this.results[1].attributes.states.length; i++) {
                    
                    for (var j = 0; j < this.results[1].attributes.states[i].data.lgas.length; j++) {
                        var obj2 = {};
                        obj2["name"] = this.results[1].attributes.states[i].data.lgas[j].data.name;
                        obj2["code"] = this.results[1].attributes.states[i].data.lgas[j].data.lgacode;
    
                        arrayOfObjects2.push(obj2);
                        if(obj2["name"]==this.schoollga){
                            this.lga_picked=tempIndex;
                            
                        }
                        tempIndex++;
                    }
                }
                // this.lga_picked = this.results[1].attributes.states[0].data.lgas[0].data.name;
                //alert(this.lga_picked);   
                this.fetch_lga = arrayOfObjects2;
    
                //SECTION C ENROLLMENT
                //C1
                //Bind metadata
                this.fetch_birth_certificate=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Birth Certificate Type";
                });
                this.fetch_birth_certificate.sort(function(a,b){return a.order-b.order});
    
                //find functions
                function checkNpc(bc) {
                    return bc.value == "National Population Commission";
                }
                function checkHospital(bc) {
                    return bc.value == "Hospital";
                }
                function checkLga(bc) {
                    return bc.value == "LGA";
                }
                function checkCourt(bc) {
                    return bc.value == "Court";
                }
                function checkUn(bc) {
                    return bc.value == "UN";
                }
                function checkOthers(bc) {
                    return bc.value == "Others";
                }
    
                //Bind data for jss
                this.fetch_birth_certificate.find(checkNpc).male=(this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "National Population Commission" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "National Population Commission" && age.class=="Jss1"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).female=(this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "National Population Commission" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "National Population Commission" && age.class=="Jss1"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).male=(this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "Others" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "Others" && age.class=="Jss1"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).female=(this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "Others" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "Others" && age.class=="Jss1"}).female:"";
                //Bind data for ss
                this.fetch_birth_certificate.find(checkNpc).sss1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "National Population Commission" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "National Population Commission" && age.class=="Sss1"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).sss1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "National Population Commission" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "National Population Commission" && age.class=="Sss1"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).sss1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "Others" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "Others" && age.class=="Sss1"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).sss1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "Others" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(age){return age.value == "Others" && age.class=="Sss1"}).female:"";
    
                //Bind data
                this.fetch_birth_certificate.find(checkNpc).kindergarten1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten1"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).kindergarten1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten1"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).kindergarten1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten1"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).kindergarten1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten1"}).female:"";
    
                this.fetch_birth_certificate.find(checkNpc).kindergarten2_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten2"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).kindergarten2_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Kindergarten2"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).kindergarten2_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten2"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).kindergarten2_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Kindergarten2"}).female:"";
    
                this.fetch_birth_certificate.find(checkNpc).nursery1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery1"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).nursery1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery1"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).nursery1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery1"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).nursery1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery1"}).female:"";
    
                this.fetch_birth_certificate.find(checkNpc).nursery2_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery2"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).nursery2_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery2"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).nursery2_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery2"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).nursery2_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery2"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery2"}).female:"";
    
                this.fetch_birth_certificate.find(checkNpc).nursery3_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery3"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery3"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).nursery3_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery3"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Nursery3"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).nursery3_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery3"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery3"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).nursery3_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery3"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Nursery3"}).female:"";
    
                this.fetch_birth_certificate.find(checkNpc).primary1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Primary1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Primary1"}).male:"";
                this.fetch_birth_certificate.find(checkNpc).primary1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Primary1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="National Population Commission" && bc.class=="Primary1"}).female:"";
                this.fetch_birth_certificate.find(checkOthers).primary1_male=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Primary1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Primary1"}).male:"";
                this.fetch_birth_certificate.find(checkOthers).primary1_female=(this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Primary1"})!=null)?this.results[0].data.enrollment.birth_certificates.data.find(function(bc){return bc.value=="Others" && bc.class=="Primary1"}).female:"";
                
    
                //C2
                this.fetch_primary_age=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Age Category" && metadata.level=="2";
                });
                this.fetch_primary_age.sort(function(a,b){return a.order-b.order});
    
                //Bind data
                this.fetch_primary_age.find(function(age){return age.value=="below 6"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"}).male:"";
                this.fetch_primary_age.find(function(age){return age.value=="below 6"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"}).female:"";
                this.fetch_primary_age.find(function(age){return age.value=="6"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"}).male:"";
                this.fetch_primary_age.find(function(age){return age.value=="6"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"}).female:"";
                this.fetch_primary_age.find(function(age){return age.value=="7"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"}).male:"";
                this.fetch_primary_age.find(function(age){return age.value=="7"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"}).female:"";
                this.fetch_primary_age.find(function(age){return age.value=="Above 11 Years"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"}).male:"";
                this.fetch_primary_age.find(function(age){return age.value=="Above 11 Years"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"}).female:"";
                this.fetch_primary_age.find(function(age){return age.value=="8"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"}).male:"";
                this.fetch_primary_age.find(function(age){return age.value=="8"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"}).female:"";
                this.fetch_primary_age.find(function(age){return age.value=="9"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"}).male:"";
                this.fetch_primary_age.find(function(age){return age.value=="9"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"}).female:"";
                this.fetch_primary_age.find(function(age){return age.value=="10"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"}).male:"";
                this.fetch_primary_age.find(function(age){return age.value=="10"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"}).female:"";
                this.fetch_primary_age.find(function(age){return age.value=="11"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"}).male:"";
                this.fetch_primary_age.find(function(age){return age.value=="11"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"}).female:"";
                //this.fetch_age.find(function(age){return age.value=="5"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="5"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="5"}).male:"";
                //this.fetch_age.find(function(age){return age.value=="5"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="5"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="5"}).female:"";
    
                //Bind data
                this.fetch_primary_age.find(function(age){return age.value=="below 6"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"}).eccd_male:"";
                this.fetch_primary_age.find(function(age){return age.value=="below 6"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 6"}).eccd_female:"";
                this.fetch_primary_age.find(function(age){return age.value=="6"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"}).eccd_male:"";
                this.fetch_primary_age.find(function(age){return age.value=="6"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="6"}).eccd_female:"";
                this.fetch_primary_age.find(function(age){return age.value=="7"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"}).eccd_male:"";
                this.fetch_primary_age.find(function(age){return age.value=="7"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="7"}).eccd_female:"";
                this.fetch_primary_age.find(function(age){return age.value=="Above 11 Years"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"}).eccd_male:"";
                this.fetch_primary_age.find(function(age){return age.value=="Above 11 Years"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 11 Years"}).eccd_female:"";
                this.fetch_primary_age.find(function(age){return age.value=="8"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"}).eccd_male:"";
                this.fetch_primary_age.find(function(age){return age.value=="8"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="8"}).eccd_female:"";
                this.fetch_primary_age.find(function(age){return age.value=="9"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"}).eccd_male:"";
                this.fetch_primary_age.find(function(age){return age.value=="9"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="9"}).eccd_female:"";
                this.fetch_primary_age.find(function(age){return age.value=="10"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"}).eccd_male:"";
                this.fetch_primary_age.find(function(age){return age.value=="10"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="10"}).eccd_female:"";
                this.fetch_primary_age.find(function(age){return age.value=="11"}).eccd_male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"}).eccd_male:"";
                this.fetch_primary_age.find(function(age){return age.value=="11"}).eccd_female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="11"}).eccd_female:"";
    
    
                //Bind metadata for js Entrants
                this.fetch_age=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Age Category" && metadata.level=="3";
                });
                this.fetch_age.sort(function(a,b){return a.order-b.order});
    
                //Bind data for js entrants
                this.fetch_age.find(function(age){return age.value=="12"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="12"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="12"}).male:"";
                this.fetch_age.find(function(age){return age.value=="12"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="12"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="12"}).female:"";
                this.fetch_age.find(function(age){return age.value=="13"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="13"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="13"}).male:"";
                this.fetch_age.find(function(age){return age.value=="13"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="13"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="13"}).female:"";
                this.fetch_age.find(function(age){return age.value=="Above 14 Years"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 14 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 14 Years"}).male:"";
                this.fetch_age.find(function(age){return age.value=="Above 14 Years"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 14 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Above 14 Years"}).female:"";
                this.fetch_age.find(function(age){return age.value=="14"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="14"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="14"}).male:"";
                this.fetch_age.find(function(age){return age.value=="14"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="14"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="14"}).female:"";
                this.fetch_age.find(function(age){return age.value=="Below 12 Years"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Below 12 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Below 12 Years"}).male:"";
                this.fetch_age.find(function(age){return age.value=="Below 12 Years"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Below 12 Years"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="Below 12 Years"}).female:"";
    
                //fectch ss entrant metadata
                this.fetch_ss_age=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Age Category" && metadata.level=="4";
                });
                this.fetch_ss_age.sort(function(a,b){return a.order-b.order});
    
                this.fetch_ss_age.find(function(age){return age.value=="above 17"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="above 17"}))?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="above 17"}).male:"";
                this.fetch_ss_age.find(function(age){return age.value=="above 17"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="above 17"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="above 17"}).female:"";
                this.fetch_ss_age.find(function(age){return age.value=="below 15"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 15"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 15"}).male:"";
                this.fetch_ss_age.find(function(age){return age.value=="below 15"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 15"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="below 15"}).female:"";
                this.fetch_ss_age.find(function(age){return age.value=="17"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="17"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="17"}).male:"";
                this.fetch_ss_age.find(function(age){return age.value=="17"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="17"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="17"}).female:"";
                this.fetch_ss_age.find(function(age){return age.value=="16"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="16"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="16"}).male:"";
                this.fetch_ss_age.find(function(age){return age.value=="16"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="16"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="16"}).female:"";
                this.fetch_ss_age.find(function(age){return age.value=="15"}).male=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="15"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="15"}).male:"";
                this.fetch_ss_age.find(function(age){return age.value=="15"}).female=(this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="15"})!=null)?this.results[0].data.enrollment.entrants.data.find(function(age){return age.age_category=="15"}).female:"";
    
    
                //C3
                //Bind metadata
                
                //Enrollment by age for preprimary
                //Bind data
                this.kindergarten1_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Kindergarten1"}).stream:"";
                this.kindergarten2_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Kindergarten2"}).stream:"";
                this.nursery1_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Nursery1"}).stream:"";
                this.nursery2_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Nursery2"}).stream:"";
                this.nursery3_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Nursery3"}).stream:"";
    
                //below3
                this.below3_kindergarten1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten1"}).male:"";
                this.below3_kindergarten1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten1"}).female:"";
                this.below3_kindergarten2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten2"}).male:"";
                this.below3_kindergarten2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 3" && age.class=="Kindergarten2"}).female:"";
                
                //3
                this.age3_kindergarten1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten1"}).male:"";
                this.age3_kindergarten1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten1"}).female:"";
                this.age3_kindergarten2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten2"}).male:"";
                this.age3_kindergarten2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Kindergarten2"}).female:"";
                this.age3_nursery1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery1"}).male:"";
                this.age3_nursery1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery1"}).female:"";
                this.age3_nursery2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery2"}).male:"";
                this.age3_nursery2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery2"}).female:"";
                this.age3_nursery3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery3"}).male:"";
                this.age3_nursery3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="3" && age.class=="Nursery3"}).female:"";
    
                //4
                this.age4_nursery1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery1"}).male:"";
                this.age4_nursery1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery1"}).female:"";
                this.age4_nursery2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery2"}).male:"";
                this.age4_nursery2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery2"}).female:"";
                this.age4_nursery3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery3"}).male:"";
                this.age4_nursery3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="4" && age.class=="Nursery3"}).female:"";
    
    
                //5
                this.age5_nursery1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery1"}).male:"";
                this.age5_nursery1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery1"}).female:"";
                this.age5_nursery2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery2"}).male:"";
                this.age5_nursery2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery2"}).female:"";
                this.age5_nursery3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery3"}).male:"";
                this.age5_nursery3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="5" && age.class=="Nursery3"}).female:"";
    
    
                //above 5
                this.above5_nursery1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery1"}).male:"";
                this.above5_nursery1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery1"}).female:"";
                this.above5_nursery2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery2"}).male:"";
                this.above5_nursery2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery2"}).female:"";
                this.above5_nursery3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery3"}).male:"";
                this.above5_nursery3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 5 Years" && age.class=="Nursery3"}).female:"";
    
                //C2
                this.fetch_age_primary=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Age Category" && metadata.level=="2";
                });
                this.fetch_age_primary.sort(function(a,b){return a.order-b.order});
    
                this.p1_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary1"}))?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary1"}).stream:"";
                this.p2_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary2"}).stream:"";
                this.p3_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary3"}).stream:"";
                this.p4_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary4"}).stream:"";
                this.p5_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary5"}).stream:"";
                this.p6_stream=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary6"}).stream:"";
    
                this.p1_stream_with_multigrade=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary1"}))?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary1"}).stream_with_multigrade:"";
                this.p2_stream_with_multigrade=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary2"}).stream_with_multigrade:"";
                this.p3_stream_with_multigrade=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary3"}).stream_with_multigrade:"";
                this.p4_stream_with_multigrade=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary4"}).stream_with_multigrade:"";
                this.p5_stream_with_multigrade=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary5"}).stream_with_multigrade:"";
                this.p6_stream_with_multigrade=(this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.stream.find(function(c){return c.class=="Primary6"}).stream_with_multigrade:"";
    
                this.p1_repeaters_male=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary1"}).male:"";
                this.p1_repeaters_female=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary1"}).female:"";
                this.p2_repeaters_male=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary2"}).male:"";
                this.p2_repeaters_female=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary2"}).female:"";
                this.p3_repeaters_male=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary3"}).male:"";
                this.p3_repeaters_female=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary3"}).female:"";
                this.p4_repeaters_male=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary4"}).male:"";
                this.p4_repeaters_female=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary4"}).female:"";
                this.p5_repeaters_male=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary5"}).male:"";
                this.p5_repeaters_female=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary5"}).female:"";
                this.p6_repeaters_male=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary6"}).male:"";
                this.p6_repeaters_female=(this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.repeater.find(function(c){return c.class=="Primary6"}).female:"";
    
    
                this.prevyear_primary6_male=(this.results[0].data.enrollment.year_by_age.data.prev_year.find(function(c){return c.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.prev_year.find(function(c){return c.class=="Primary6"}).male:"";
                this.prevyear_primary6_female=(this.results[0].data.enrollment.year_by_age.data.prev_year.find(function(c){return c.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.prev_year.find(function(c){return c.class=="Primary6"}).female:"";
                
    
                //age below 6
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary1"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary1"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary2"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary2"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary3"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary3"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary4"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary4"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary5"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary5"}).female:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary6"}).male:"";
                this.fetch_age_primary.find(function(age){return age.value=="below 6"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 6" && age.class=="Primary6"}).female:"";
    
    
                 //age 6
                 this.fetch_age_primary.find(function(age){return age.value=="6"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary1"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="6"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary1"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="6"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary2"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="6"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary2"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="6"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary3"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="6"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary3"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="6"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary4"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="6"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary4"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="6"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary5"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="6"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary5"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="6"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary6"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="6"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="6" && age.class=="Primary6"}).female:"";
     
                 //age 7
                 this.fetch_age_primary.find(function(age){return age.value=="7"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary1"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="7"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary1"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="7"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary2"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="7"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary2"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="7"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary3"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="7"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary3"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="7"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary4"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="7"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary4"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="7"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary5"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="7"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary5"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="7"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary6"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="7"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="7" && age.class=="Primary6"}).female:"";
     
                 //age 8
                 this.fetch_age_primary.find(function(age){return age.value=="8"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary1"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="8"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary1"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="8"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary2"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="8"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary2"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="8"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary3"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="8"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary3"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="8"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary4"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="8"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary4"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="8"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary5"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="8"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary5"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="8"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary6"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="8"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="8" && age.class=="Primary6"}).female:"";
     
                 //age 9
                 this.fetch_age_primary.find(function(age){return age.value=="9"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary1"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="9"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary1"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="9"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary2"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="9"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary2"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="9"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary3"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="9"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary3"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="9"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary4"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="9"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary4"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="9"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary5"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="9"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary5"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="9"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary6"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="9"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="9" && age.class=="Primary6"}).female:"";
     
                 //age 10
                 this.fetch_age_primary.find(function(age){return age.value=="10"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary1"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="10"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary1"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="10"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary2"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="10"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary2"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="10"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary3"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="10"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary3"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="10"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary4"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="10"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary4"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="10"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary5"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="10"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary5"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="10"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary6"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="10"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="10" && age.class=="Primary6"}).female:"";
     
                 //age 11
                 this.fetch_age_primary.find(function(age){return age.value=="11"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary1"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="11"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary1"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="11"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary2"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="11"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary2"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="11"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary3"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="11"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary3"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="11"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary4"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="11"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary4"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="11"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary5"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="11"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary5"}).female:"";
                 this.fetch_age_primary.find(function(age){return age.value=="11"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary6"}).male:"";
                 this.fetch_age_primary.find(function(age){return age.value=="11"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="11" && age.class=="Primary6"}).female:"";
     
     
                  //age above 11
                  this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary1"}).male:"";
                  this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary1"}).female:"";
                  this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary2"}).male:"";
                  this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary2"}).female:"";
                  this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary3"}).male:"";
                  this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary3"}).female:"";
                  this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary4_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary4"}).male:"";
                  this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary4_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary4"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary4"}).female:"";
                  this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary5_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary5"}).male:"";
                  this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary5_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary5"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary5"}).female:"";
                  this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary6_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary6"}).male:"";
                  this.fetch_age_primary.find(function(age){return age.value=="Above 11 Years"}).primary6_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary6"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 11 Years" && age.class=="Primary6"}).female:"";
      
    
                this.fetch_year_by_age=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Age Category" && metadata.level=="3";
                });
                this.fetch_year_by_age.sort(function(a,b){return a.order-b.order});
    
                //Bind data for js
                this.fetch_streams=this.results[0].data.enrollment.year_by_age.data.stream;//need to fix this
                this.jss1_stream=(this.fetch_streams.find(function(a){return a.class=="Jss1"}))?this.fetch_streams.find(function(a){return a.class=="Jss1"}).stream:"";
                this.jss2_stream=(this.fetch_streams.find(function(a){return a.class=="Jss2"}))?this.fetch_streams.find(function(a){return a.class=="Jss2"}).stream:"";
                this.jss3_stream=(this.fetch_streams.find(function(a){return a.class=="Jss3"}))?this.fetch_streams.find(function(a){return a.class=="Jss3"}).stream:"";
                this.jss1_stream_with_multigrade=(this.fetch_streams.find(function(a){return a.class=="Jss1"}))?this.fetch_streams.find(function(a){return a.class=="Jss1"}).stream_with_multigrade:"";
                this.jss2_stream_with_multigrade=(this.fetch_streams.find(function(a){return a.class=="Jss2"}))?this.fetch_streams.find(function(a){return a.class=="Jss2"}).stream_with_multigrade:"";
                this.jss3_stream_with_multigrade=(this.fetch_streams.find(function(a){return a.class=="Jss3"}))?this.fetch_streams.find(function(a){return a.class=="Jss3"}).stream_with_multigrade:"";
    
                this.fetch_repeaters=this.results[0].data.enrollment.year_by_age.data.repeater;
                this.jss1_repeaters_male=(this.fetch_repeaters.find(function(a){return a.class=="Jss1"}))?this.fetch_repeaters.find(function(a){return a.class=="Jss1"}).male:"";
                this.jss1_repeaters_female=(this.fetch_repeaters.find(function(a){return a.class=="Jss1"}))?this.fetch_repeaters.find(function(a){return a.class=="Jss1"}).female:"";
                this.jss2_repeaters_male=(this.fetch_repeaters.find(function(a){return a.class=="Jss2"}))?this.fetch_repeaters.find(function(a){return a.class=="Jss2"}).male:"";
                this.jss2_repeaters_female=(this.fetch_repeaters.find(function(a){return a.class=="Jss2"}))?this.fetch_repeaters.find(function(a){return a.class=="Jss2"}).female:"";
                this.jss3_repeaters_male=(this.fetch_repeaters.find(function(a){return a.class=="Jss3"}))?this.fetch_repeaters.find(function(a){return a.class=="Jss3"}).male:"";
                this.jss3_repeaters_female=(this.fetch_repeaters.find(function(a){return a.class=="Jss3"}))?this.fetch_repeaters.find(function(a){return a.class=="Jss3"}).female:"";
    
                this.prevyear_jss3_male=(this.results[0].data.enrollment.year_by_age.data.prev_year.find(function(a){return a.class=="Jss3"}))?this.results[0].data.enrollment.year_by_age.data.prev_year.find(function(a){return a.class=="Jss3"}).male:"";
                this.prevyear_jss3_female=(this.results[0].data.enrollment.year_by_age.data.prev_year.find(function(a){return a.class=="Jss3"}))?this.results[0].data.enrollment.year_by_age.data.prev_year.find(function(a){return a.class=="Jss3"}).female:"";
                
                
                this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss1"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss1"}).female:"";
                this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss2"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss2"}).female:"";
                this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss3"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="12" && age.class=="Jss3"}).female:"";
    
                this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss1"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss1"}).female:"";
                this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss2"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss2"}).female:"";
                this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss3"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="13"}).jss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="13" && age.class=="Jss3"}).female:"";
    
                this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss1"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss1"}).female:"";
                this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss2"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss2"}).female:"";
                this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss3"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="14"}).jss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="14" && age.class=="Jss3"}).female:"";
    
                this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss1"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss1"}).female:"";
                this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss2"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss2"}).female:"";
                this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss3"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="Above 14 Years"}).jss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Above 14 Years" && age.class=="Jss3"}).female:"";
    
                this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss1"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss1"}).female:"";
                this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss2"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss2"}).female:"";
                this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss3"}).male:"";
                this.fetch_year_by_age.find(function(age){return age.value=="Below 12 Years"}).jss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="Below 12 Years" && age.class=="Jss3"}).female:"";
                
                //year by age for ss
                this.fetch_ss_year_by_age=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Age Category" && metadata.level=="4";
                });
                this.fetch_ss_year_by_age.sort(function(a,b){return a.order-b.order});
    
                //Bind data
                this.fetch_streams=this.results[0].data.enrollment.year_by_age.data.stream;
                this.ss1_stream=(this.fetch_streams.find(function(a){return a.class=="Sss1"}))?this.fetch_streams.find(function(a){return a.class=="Sss1"}).stream:"";
                this.ss2_stream=(this.fetch_streams.find(function(a){return a.class=="Sss2"}))?this.fetch_streams.find(function(a){return a.class=="Sss2"}).stream:"";
                this.ss3_stream=(this.fetch_streams.find(function(a){return a.class=="Sss3"}))?this.fetch_streams.find(function(a){return a.class=="Sss3"}).stream:"";
                this.ss1_stream_with_multigrade=(this.fetch_streams.find(function(a){return a.class=="Sss1"}))?this.fetch_streams.find(function(a){return a.class=="Sss1"}).stream_with_multigrade:"";
                this.ss2_stream_with_multigrade=(this.fetch_streams.find(function(a){return a.class=="Sss2"}))?this.fetch_streams.find(function(a){return a.class=="Sss2"}).stream_with_multigrade:"";
                this.ss3_stream_with_multigrade=(this.fetch_streams.find(function(a){return a.class=="Sss3"}))?this.fetch_streams.find(function(a){return a.class=="Sss3"}).stream_with_multigrade:"";
    
                this.fetch_repeaters=this.results[0].data.enrollment.year_by_age.data.repeater;
                this.ss1_repeaters_male=(this.fetch_repeaters.find(function(a){return a.class=="Sss1"}))?this.fetch_repeaters.find(function(a){return a.class=="Sss1"}).male:"";
                this.ss1_repeaters_female=(this.fetch_repeaters.find(function(a){return a.class=="Sss1"}))?this.fetch_repeaters.find(function(a){return a.class=="Sss1"}).female:"";
                this.ss2_repeaters_male=(this.fetch_repeaters.find(function(a){return a.class=="Sss2"}))?this.fetch_repeaters.find(function(a){return a.class=="Sss2"}).male:"";
                this.ss2_repeaters_female=(this.fetch_repeaters.find(function(a){return a.class=="Sss2"}))?this.fetch_repeaters.find(function(a){return a.class=="Sss2"}).female:"";
                this.ss3_repeaters_male=(this.fetch_repeaters.find(function(a){return a.class=="Sss3"}))?this.fetch_repeaters.find(function(a){return a.class=="Sss3"}).male:"";
                this.ss3_repeaters_female=(this.fetch_repeaters.find(function(a){return a.class=="Sss3"}))?this.fetch_repeaters.find(function(a){return a.class=="Sss3"}).female:"";
    
                this.prevyear_sss3_male=(this.results[0].data.enrollment.year_by_age.data.prev_year.find(function(a){return a.class=="Sss3"}))?this.results[0].data.enrollment.year_by_age.data.prev_year.find(function(a){return a.class=="Sss3"}).male:"";
                this.prevyear_sss3_female=(this.results[0].data.enrollment.year_by_age.data.prev_year.find(function(a){return a.class=="Sss3"}))?this.results[0].data.enrollment.year_by_age.data.prev_year.find(function(a){return a.class=="Sss3"}).female:"";
                
    
                this.fetch_ss_year_by_age.find(function(age){return age.value=="15"}).sss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="15" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="15" && age.class=="Sss1"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="15"}).sss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="15" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="15" && age.class=="Sss1"}).female:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="15"}).sss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="15" && age.class=="Sss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="15" && age.class=="Sss2"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="15"}).sss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="15" && age.class=="Sss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="15" && age.class=="Sss2"}).female:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="15"}).sss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="15" && age.class=="Sss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="15" && age.class=="Sss3"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="15"}).sss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="15" && age.class=="Sss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="15" && age.class=="Sss3"}).female:"";
    
                this.fetch_ss_year_by_age.find(function(age){return age.value=="16"}).sss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="16" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="16" && age.class=="Sss1"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="16"}).sss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="16" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="16" && age.class=="Sss1"}).female:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="16"}).sss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="16" && age.class=="Sss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="16" && age.class=="Sss2"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="16"}).sss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="16" && age.class=="Sss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="16" && age.class=="Sss2"}).female:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="16"}).sss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="16" && age.class=="Sss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="16" && age.class=="Sss3"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="16"}).sss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="16" && age.class=="Sss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="16" && age.class=="Sss3"}).female:"";
    
                this.fetch_ss_year_by_age.find(function(age){return age.value=="17"}).sss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="17" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="17" && age.class=="Sss1"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="17"}).sss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="17" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="17" && age.class=="Sss1"}).female:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="17"}).sss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="17" && age.class=="Sss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="17" && age.class=="Sss2"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="17"}).sss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="17" && age.class=="Sss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="17" && age.class=="Sss2"}).female:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="17"}).sss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="17" && age.class=="Sss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="17" && age.class=="Sss3"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="17"}).sss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="17" && age.class=="Sss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="17" && age.class=="Sss3"}).female:"";
    
                this.fetch_ss_year_by_age.find(function(age){return age.value=="below 15"}).sss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 15" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 15" && age.class=="Sss1"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="below 15"}).sss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 15" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 15" && age.class=="Sss1"}).female:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="below 15"}).sss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 15" && age.class=="Sss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 15" && age.class=="Sss2"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="below 15"}).sss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 15" && age.class=="Sss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 15" && age.class=="Sss2"}).female:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="below 15"}).sss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 15" && age.class=="Sss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 15" && age.class=="Sss3"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="below 15"}).sss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 15" && age.class=="Sss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="below 15" && age.class=="Sss3"}).female:"";
    
                this.fetch_ss_year_by_age.find(function(age){return age.value=="above 17"}).sss1_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="above 17" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="above 17" && age.class=="Sss1"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="above 17"}).sss1_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="above 17" && age.class=="Sss1"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="above 17" && age.class=="Sss1"}).female:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="above 17"}).sss2_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="above 17" && age.class=="Sss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="above 17" && age.class=="Sss2"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="above 17"}).sss2_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="above 17" && age.class=="Sss2"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="above 17" && age.class=="Sss2"}).female:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="above 17"}).sss3_male=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="above 17" && age.class=="Sss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="above 17" && age.class=="Sss3"}).male:"";
                this.fetch_ss_year_by_age.find(function(age){return age.value=="above 17"}).sss3_female=(this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="above 17" && age.class=="Sss3"})!=null)?this.results[0].data.enrollment.year_by_age.data.age.find(function(age){return age.age_category=="above 17" && age.class=="Sss3"}).female:"";
                
    
                //C4
                //Bind metadata
                this.fetch_pupilflow=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Enrolment Items 2016";
                });
                this.fetch_pupilflow.sort(function(a,b){return a.value<b.value});
    
                //Bind Data
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary3"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary4_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary4"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary4_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary4"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary5_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary5"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary5_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary5"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary6_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary6"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).primary6_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Primary6"}).female:"";
    
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary3"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary4_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary4"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary4_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary4"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary5_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary5"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary5_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary5"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary6_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary6"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).primary6_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Primary6"}).female:"";
    
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary3"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary4_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary4"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary4_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary4"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary5_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary5"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary5_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary5"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary6_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary6"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).primary6_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Primary6"}).female:"";
    
    
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary3"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary4_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary4"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary4_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary4"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary4"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary5_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary5"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary5_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary5"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary5"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary6_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary6"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).primary6_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary6"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Primary6"}).female:"";
    
    
    
                //jss
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).jss3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Jss3"}).female:"";
    
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).jss3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Jss3"}).female:"";
    
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).jss3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Jss3"}).female:"";
    
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).jss3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Jss3"}).female:"";
    
    
                //for ss
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).sss1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Sss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Sss1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).sss1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Sss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Sss1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).sss2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Sss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Sss2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).sss2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Sss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Sss2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).sss3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Sss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Sss3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Dropout"}).sss3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Sss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Dropout" && pupil.class=="Sss3"}).female:"";
    
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).sss1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Sss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Sss1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).sss1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Sss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Sss1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).sss2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Sss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Sss2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).sss2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Sss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Sss2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).sss3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Sss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Sss3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer In"}).sss3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Sss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer In" && pupil.class=="Sss3"}).female:"";
    
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).sss1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Sss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Sss1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).sss1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Sss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Sss1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).sss2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Sss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Sss2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).sss2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Sss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Sss2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).sss3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Sss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Sss3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Transfer Out"}).sss3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Sss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Transfer Out" && pupil.class=="Sss3"}).female:"";
    
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).sss1_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Sss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Sss1"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).sss1_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Sss1"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Sss1"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).sss2_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Sss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Sss2"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).sss2_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Sss2"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Sss2"}).female:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).sss3_male=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Sss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Sss3"}).male:"";
                this.fetch_pupilflow.find(function(pupil){return pupil.value=="Promoted"}).sss3_female=(this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Sss3"})!=null)?this.results[0].data.enrollment.pupil_flow.data.find(function(pupil){return pupil.flow_item=="Promoted" && pupil.class=="Sss3"}).female:"";
    
                
    
                //C5
                //Bind metadata
                this.fetch_special_needs=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Pupil Challenges 2009";
                });
                this.fetch_special_needs.sort(function(a,b){return a.value>b.value});
    
                //Bind data for primary
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).eccd_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Eccd"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).eccd_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Eccd"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).nursery_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nurs"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).nursery_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nurs"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).nursery3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nursery3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).nursery3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Nursery3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary4_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary4"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary4_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary4"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary5_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary5"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary5_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary5"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary6_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary6"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).primary6_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Primary6"}).female:"";
    
    
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).eccd_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Eccd"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).eccd_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Eccd"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).nursery_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nurs"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).nursery_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nurs"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).nursery3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nursery3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).nursery3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Nursery3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary4_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary4"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary4_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary4"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary5_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary5"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary5_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary5"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary6_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary6"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).primary6_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Primary6"}).female:"";
    
    
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).eccd_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Eccd"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).eccd_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Eccd"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).nursery_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nurs"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).nursery_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nurs"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).nursery3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nursery3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).nursery3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Nursery3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary4_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary4"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary4_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary4"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary5_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary5"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary5_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary5"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary6_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary6"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).primary6_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Primary6"}).female:"";
    
    
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).eccd_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Eccd"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).eccd_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Eccd"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).nursery_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nurs"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).nursery_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nurs"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).nursery3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nursery3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).nursery3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Nursery3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary4_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary4"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary4_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary4"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary5_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary5"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary5_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary5"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary6_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary6"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).primary6_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Primary6"}).female:"";
    
    
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).eccd_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Eccd"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).eccd_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Eccd"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).nursery_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nurs"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).nursery_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nurs"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).nursery3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nursery3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).nursery3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Nursery3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary4_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary4"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary4_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary4"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary5_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary5"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary5_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary5"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary6_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary6"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).primary6_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Primary6"}).female:"";
    
    
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).eccd_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Eccd"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).eccd_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Eccd"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).nursery_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nurs"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).nursery_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nurs"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).nursery3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nursery3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).nursery3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Nursery3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary3"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary4_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary4"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary4_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary4"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary5_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary5"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary5_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary5"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary6_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary6"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).primary6_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Primary6"}).female:"";
    
                //Bind data
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).jss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Jss3"}).female:"";
    
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).jss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Jss3"}).female:"";
    
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).jss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Jss3"}).female:"";
    
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).jss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Jss3"}).female:"";
    
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).jss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Jss3"}).female:"";
    
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).jss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Jss3"}).female:"";
    
                //Ss
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).sss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Sss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Sss1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).sss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Sss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Sss1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).sss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Sss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Sss2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).sss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Sss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Sss2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).sss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Sss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Sss3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Blind / visually impaired"}).sss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Sss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Blind / visually impaired" && need.class=="Sss3"}).female:"";
    
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).sss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Sss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Sss1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).sss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Sss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Sss1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).sss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Sss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Sss2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).sss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Sss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Sss2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).sss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Sss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Sss3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Hearing / speech impaired"}).sss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Sss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Hearing / speech impaired" && need.class=="Sss3"}).female:"";
    
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).sss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Sss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Sss1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).sss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Sss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Sss1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).sss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Sss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Sss2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).sss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Sss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Sss2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).sss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Sss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Sss3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Physically challenged (other than visual or hearing)"}).sss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Sss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Physically challenged (other than visual or hearing)" && need.class=="Sss3"}).female:"";
    
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).sss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Sss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Sss1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).sss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Sss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Sss1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).sss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Sss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Sss2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).sss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Sss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Sss2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).sss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Sss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Sss3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Mentally challenged"}).sss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Sss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Mentally challenged" && need.class=="Sss3"}).female:"";
    
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).sss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Sss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Sss1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).sss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Sss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Sss1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).sss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Sss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Sss2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).sss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Sss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Sss2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).sss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Sss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Sss3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Albinism"}).sss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Sss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Albinism" && need.class=="Sss3"}).female:"";
    
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).sss1_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Sss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Sss1"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).sss1_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Sss1"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Sss1"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).sss2_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Sss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Sss2"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).sss2_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Sss2"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Sss2"}).female:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).sss3_male=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Sss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Sss3"}).male:"";
                this.fetch_special_needs.find(function(need){return need.value=="Autism"}).sss3_female=(this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Sss3"})!=null)?this.results[0].data.enrollment.special_need.data.find(function(need){return need.special_need_item=="Autism" && need.class=="Sss3"}).female:"";
    
                //Bind data
                this.fetch_orphans=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Vulnerability Type";
                });
                this.fetch_orphans.sort(function(a,b){return a.order>b.order});
    
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).eccd_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Eccd"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).eccd_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Eccd"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).nursery_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nurs"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).nursery_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nurs"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).nursery3_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nursery3"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).nursery3_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Nursery3"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary1_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary1"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary1_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary1"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary2_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary2"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary2_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary2"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary3_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary3"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary3_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary3"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary4_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary4"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary4_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary4"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary5_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary5"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary5_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary5"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary6_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary6"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Father"}).primary6_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Father" && need.class=="Primary6"}).female:"";
    
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).eccd_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Eccd"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).eccd_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Eccd"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).nursery_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nurs"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).nursery_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nurs"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).nursery3_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nursery3"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).nursery3_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Nursery3"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary1_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary1"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary1_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary1"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary2_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary2"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary2_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary2"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary3_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary3"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary3_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary3"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary4_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary4"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary4_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary4"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary5_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary5"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary5_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary5"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary6_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary6"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Mother"}).primary6_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Mother" && need.class=="Primary6"}).female:"";
    
    
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).eccd_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Eccd"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).eccd_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Eccd"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Eccd"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).nursery_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nurs"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).nursery_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nurs"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nurs"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).nursery3_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nursery3"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).nursery3_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nursery3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Nursery3"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary1_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary1"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary1_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary1"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary1"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary2_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary2"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary2_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary2"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary2"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary3_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary3"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary3_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary3"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary3"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary4_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary4"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary4_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary4"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary4"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary5_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary5"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary5_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary5"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary5"}).female:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary6_male=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary6"}).male:"";
                this.fetch_orphans.find(function(need){return need.value=="Lost Both"}).primary6_female=(this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary6"})!=null)?this.results[0].data.enrollment.orphans.data.find(function(need){return need.orphan_item=="Lost Both" && need.class=="Primary6"}).female:"";
    
    
                //Staff
                this.non_teaching_staff_male=this.results[0].data.staff.number_of_non_teaching_staffs["male"];
                this.non_teaching_staff_female=this.results[0].data.staff.number_of_non_teaching_staffs["female"];
                this.non_teaching_staff_total=this.results[0].data.staff.number_of_non_teaching_staffs["total"];
    
                this.teaching_staff_male=this.results[0].data.staff.number_of_teaching_staffs["male"];
                this.teaching_staff_female=this.results[0].data.staff.number_of_teaching_staffs["female"];
                this.teaching_staff_total=this.results[0].data.staff.number_of_teaching_staffs["total"];
    
                //Staffs
                //this.fetch_staffs=this.results[0].data.staff.information_on_all_staff.data;
    
                //still under the staff section
                //metadata
                this.fetch_teachingtype=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Teacher Appointment Type";
                });
                this.fetch_teachingtype.sort(function(a,b){return a.order>b.order});
    
                //Salary Source
                this.fetch_salarysource=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Salary Source";
                });
                this.fetch_salarysource.sort(function(a,b){return a.order>b.order});
    
                //Main Secondary Subject Taught
                this.fetch_specialisation=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Main Secondary Subject Taught";
                });
                this.fetch_specialisation.sort(function(a,b){return a.order>b.order});
    
                this.fetch_subjecttaught=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Main Secondary Subject Taught";
                });
                this.fetch_subjecttaught.sort(function(a,b){return a.order>b.order});
    
                //Present 2016
                this.fetch_present=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Present 2016";
                });
                this.fetch_present.sort(function(a,b){return a.order>b.order});
    
                //Staff Type
                this.fetch_stafftype=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Staff Type";
                });
                this.fetch_stafftype.sort(function(a,b){return a.order>b.order});
    
                //Academic Qualification 2009
                this.fetch_academicqualification=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Academic Qualification 2009";
                });
                this.fetch_academicqualification.sort(function(a,b){return a.order>b.order});
    
                //Teaching Qualifications
                this.fetch_teachingqualification=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Teaching Qualifications";
                });
                this.fetch_teachingqualification.sort(function(a,b){return a.order>b.order});
    
                //Classroom
                this.no_of_classrooms=this.results[0].data.classrooms.number_of_classrooms;
                this.classes_held_outside=this.results[0].data.classrooms.are_classes_held_outside;
                //Bind metadata
                this.fetch_otherRooms=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Room Type 2016";
                });
                this.fetch_otherRooms.sort(function(a,b){return a.order>b.order});
    
                this.fetch_otherRooms.find(function(room){return room.value=="Staff room"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Staff room"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Staff room"}).number:"";
                this.fetch_otherRooms.find(function(room){return room.value=="Office"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Office"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Office"}).number:"";
                this.fetch_otherRooms.find(function(room){return room.value=="Laboratories"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Laboratories"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Laboratories"}).number:"";
                this.fetch_otherRooms.find(function(room){return room.value=="Store room"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Store room"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Store room"}).number:"";
                this.fetch_otherRooms.find(function(room){return room.value=="Others"}).no_of_rooms=(this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Others"})!=null)?this.results[0].data.classrooms.rooms_other_than_classrooms.data.find(function(room){return room.roomtype=="Others"}).number:"";
                
                //Drinking water
                //Bind metadata
                this.fetch_drinkingwater_source=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Water Supply Type";
                });
                this.fetch_drinkingwater_source.sort(function(a,b){return a.order>b.order});
                this.sources_of_drinking_water=this.results[0].data.facilities.source_of_drinking_water.data;
    
                //facilities
                //Bind metadata
                this.fetch_facilities=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Useable Facility 2013";
                });
                this.fetch_facilities.sort(function(a,b){return a.order>b.order});
    
                this.fetch_facilities.find(function(facility){return facility.value=="Toilet"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Toilet"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Toilet"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Toilet"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Toilet"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Toilet"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Computer"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Computer"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Computer"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Computer"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Computer"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Computer"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Water Source"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Water Source"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Water Source"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Water Source"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Water Source"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Water Source"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Laboratory"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Laboratory"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Laboratory"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Laboratory"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Laboratory"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Laboratory"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Classroom"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Classroom"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Classroom"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Classroom"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Classroom"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Classroom"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Library"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Library"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Library"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Library"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Library"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Library"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Play Ground"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Play Ground"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Play Ground"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Play Ground"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Play Ground"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Play Ground"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Others"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Others"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Others"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Others"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Others"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Others"}).notuseable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Wash hand facility"}).useable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Wash hand facility"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Wash hand facility"}).useable:"";
                this.fetch_facilities.find(function(facility){return facility.value=="Wash hand facility"}).notuseable=(this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Wash hand facility"})!=null)?this.results[0].data.facilities.facilities_available.data.find(function(room){return room.facility=="Wash hand facility"}).notuseable:"";
    
    
                //shared facilities
                this.shared_facilities=this.results[0].data.facilities.shared_facilities.data;
    
    
                //power source
                //Bind metadata
                this.fetch_power_source=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Power Facilities";
                });
                this.fetch_power_source.sort(function(a,b){return a.order>b.order});
                this.sources_of_power=this.results[0].data.facilities.sources_of_power.data;   
    
                //toilet type
                //Bind metadata
                this.fetch_toilet_type=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Toilet Type 2009";
                });
                this.fetch_toilet_type.sort(function(a,b){return a.order>b.order});
    
                //toilet Pit
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).students_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).students_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).students_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by students"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).teachers_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).teachers_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).teachers_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used only by teachers"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).both_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).both_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).both_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Pit" && room.usertype=="Used by students and teachers"}).mixed:"";
    
                //Bucket system
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).students_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).students_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).students_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by students"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).teachers_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).teachers_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).teachers_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used only by teachers"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).both_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).both_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Bucket system"}).both_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Bucket system" && room.usertype=="Used by students and teachers"}).mixed:"";
    
                //toilet Water flush
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).students_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).students_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).students_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by students"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).teachers_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).teachers_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).teachers_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used only by teachers"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).both_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).both_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Water flush"}).both_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Water flush" && room.usertype=="Used by students and teachers"}).mixed:"";
    
                //toilet Pit
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).students_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).students_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).students_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by students"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).teachers_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).teachers_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).teachers_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used only by teachers"}).mixed:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).both_male=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"}).male:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).both_female=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"}).female:"";
                this.fetch_toilet_type.find(function(facility){return facility.value=="Others"}).both_mixed=(this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"})!=null)?this.results[0].data.facilities.toilet.data.find(function(room){return room.toilet=="Others" && room.usertype=="Used by students and teachers"}).mixed:"";
    
    
                //health facility
                this.fetch_healthfacility=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Available Health Facility 2009";
                });
                this.fetch_healthfacility.sort(function(a,b){return a.order>b.order});
                this.health_facility=this.results[0].data.facilities.health_facility.data;
                
                //fence
                this.fetch_fence=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Available Fence Wall 2016";
                });
                this.fetch_fence.sort(function(a,b){return a.order>b.order});
                this.fence_facility=this.results[0].data.facilities.fence.data;
    
                //ownership
                this.fetch_ownership=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Ownership Status";
                });
                this.fetch_ownership.sort(function(a,b){return a.order>b.order});
                this.facility_ownership=this.results[0].data.facilities.ownership.data[0];
    
                //schoolbuilding
                this.fetch_schoolbuilding=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Type of school building";
                });
                this.fetch_schoolbuilding.sort(function(a,b){return a.order>b.order});
                this.facility_schoolbuilding=this.results[0].data.facilities.schoolbuilding.data[0];
    
                //undertaking
                this.attestation_headteacher_name=this.results[0].data.undertaking.attestation_headteacher_name;
                this.attestation_headteacher_telephone=this.results[0].data.undertaking.attestation_headteacher_telephone;
                this.attestation_headteacher_signdate=this.results[0].data.undertaking.attestation_headteacher_signdate;
                this.attestation_enumerator_name=this.results[0].data.undertaking.attestation_enumerator_name;
                this.attestation_enumerator_position=this.results[0].data.undertaking.attestation_enumerator_position;
                this.attestation_enumerator_telephone=this.results[0].data.undertaking.attestation_enumerator_telephone;
                this.attestation_supervisor_name=this.results[0].data.undertaking.attestation_supervisor_name;
                this.attestation_supervisor_position=this.results[0].data.undertaking.attestation_supervisor_position;
                this.attestation_supervisor_telephone=this.results[0].data.undertaking.attestation_supervisor_telephone;
    
    
                 //seater
                 this.fetch_seaters=[{"value":"ECCD","order":"1"},{"value":"Nurs","order":"2"},{"value":"Primary1","order":"3"},{"value":"Primary2","order":"4"},{"value":"Primary3","order":"5"},{"value":"Primary4","order":"6"},{"value":"Primary5","order":"7"},{"value":"Primary6","order":"8"},{"value":"Jss1","order":"9"},{"value":"Jss2","order":"10"},{"value":"Jss3","order":"11"},{"value":"Sss1","order":"12"},{"value":"Sss2","order":"13"},{"value":"Sss3","order":"14"}];
    
    
                this.fetch_seaters.sort(function(a,b){return a.order>b.order});
                for (var i = 0; i < this.fetch_seaters.length; i++) {
                    var Tclass=this.fetch_seaters[i].value;
                    this.fetch_seaters[i].seater1=(this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="1 Seater"})!=null)?this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="1 Seater"}).capacity:"";
                    this.fetch_seaters[i].seater2=(this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="2 Seater"})!=null)?this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="2 Seater"}).capacity:"";
                    this.fetch_seaters[i].seater3=(this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="3 Seater"})!=null)?this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="3 Seater"}).capacity:"";
                    this.fetch_seaters[i].seater4=(this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="4 Seater"})!=null)?this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="4 Seater"}).capacity:"";
                    this.fetch_seaters[i].seater5=(this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="5 Seater"})!=null)?this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="5 Seater"}).capacity:"";
                    this.fetch_seaters[i].seater6=(this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="6 Seater"})!=null)?this.results[0].data.facilities.seater.data.find(function(p){return p.class==Tclass && p.seattype=="6 Seater"}).capacity:"";
                    
                    }
    
                //student by subject
                this.fetch_subjects=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Students By Subject Area JSS";
                });
                this.fetch_subjects.sort(function(a,b){return a.order>b.order});
    
                for (var i = 0; i < this.fetch_subjects.length; i++) {
                var subject=this.fetch_subjects[i].value;
                this.fetch_subjects[i].jss1_pupils_male=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss1"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss1"}).male:"";
                this.fetch_subjects[i].jss1_pupils_female=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss1"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss1"}).female:"";
                this.fetch_subjects[i].jss2_pupils_male=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss2"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss2"}).male:"";
                this.fetch_subjects[i].jss2_pupils_female=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss2"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss2"}).female:"";
                this.fetch_subjects[i].jss3_pupils_male=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss3"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss3"}).male:"";
                this.fetch_subjects[i].jss3_pupils_female=(this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss3"})!=null)?this.results[0].data.number_of_student_by_subject.data.find(function(p){return p.subject==subject && p.class=="Jss3"}).female:"";
    
                }
    
                //teaching qualification
                for(var i=0; i< this.fetch_teachingqualification.length; i++){
                    var tq=this.fetch_teachingqualification[i].value;
                    this.fetch_teachingqualification[i].preprimary_male=(this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Pre-primary" && p.qualification==tq})!=null)?this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Pre-primary" && p.qualification==tq}).male:"";
                    this.fetch_teachingqualification[i].preprimary_female=(this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Pre-primary" && p.qualification==tq})!=null)?this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Pre-primary" && p.qualification==tq}).female:"";
                    this.fetch_teachingqualification[i].primary_male=(this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Primary" && p.qualification==tq})!=null)?this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Primary" && p.qualification==tq}).male:"";
                    this.fetch_teachingqualification[i].primary_female=(this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Primary" && p.qualification==tq})!=null)?this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Primary" && p.qualification==tq}).female:"";
                    this.fetch_teachingqualification[i].jss_male=(this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Junior Secondary" && p.qualification==tq})!=null)?this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Junior Secondary" && p.qualification==tq}).male:"";
                    this.fetch_teachingqualification[i].jss_female=(this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Junior Secondary" && p.qualification==tq})!=null)?this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Junior Secondary" && p.qualification==tq}).female:"";
                    this.fetch_teachingqualification[i].sss_male=(this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Senior Secondary" && p.qualification==tq})!=null)?this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Senior Secondary" && p.qualification==tq}).male:"";
                    this.fetch_teachingqualification[i].sss_female=(this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Senior Secondary" && p.qualification==tq})!=null)?this.results[0].data.teacher_qualification.data.find(function(p){return p.level=="Senior Secondary" && p.qualification==tq}).female:"";
                }
                
    
                
                //main subject
                this.fetch_mainsubjects=this.results[1].data.data.filter(function(metadata){
                    return metadata.reference=="Main Secondary Subject Taught";
                });
                this.fetch_mainsubjects.sort(function(a,b){return a.order>b.order});
    
    
                //books
                this.fetch_books=[{value:"Number"}];
    
                //h1
                for (var i = 0; i < this.fetch_books.length; i++) {
                    var subject=this.fetch_books[i].value;
                    this.fetch_books[i].prm1_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary1"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary1"}).value:"";
                    this.fetch_books[i].prm2_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary2"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary2"}).value:"";
                    this.fetch_books[i].prm3_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary3"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary3"}).value:"";
                    this.fetch_books[i].prm4_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary4"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary4"}).value:"";
                    this.fetch_books[i].prm5_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary5"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary5"}).value:"";
                    this.fetch_books[i].prm6_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary6"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Primary6"}).value:"";
                    this.fetch_books[i].jss1_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Jss1"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Jss1"}).value:"";
                    this.fetch_books[i].jss2_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Jss2"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Jss2"}).value:"";
                    this.fetch_books[i].jss3_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Jss3"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Jss3"}).value:"";
                    this.fetch_books[i].sss1_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Sss1"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Sss1"}).value:"";
                    this.fetch_books[i].sss2_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Sss2"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Sss2"}).value:"";
                    this.fetch_books[i].sss3_pupils_books=(this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Sss3"})!=null)?this.results[0].data.student_teacher_book.subject_textbooks_available.data.find(function(p){return p.subject==subject && p.class=="Sss3"}).value:"";
                    
                    }
        
                //h2
                for (var i = 0; i < this.fetch_books.length; i++) {
                    var subject=this.fetch_books[i].value;
                    this.fetch_books[i].prm1_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary1"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary1"}).value:"";
                    this.fetch_books[i].prm2_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary2"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary2"}).value:"";
                    this.fetch_books[i].prm3_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary3"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary3"}).value:"";
                    this.fetch_books[i].prm4_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary4"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary4"}).value:"";
                    this.fetch_books[i].prm5_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary5"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary5"}).value:"";
                    this.fetch_books[i].prm6_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary6"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Primary6"}).value:"";
                    this.fetch_books[i].jss1_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Jss1"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Jss1"}).value:"";
                    this.fetch_books[i].jss2_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Jss2"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Jss2"}).value:"";
                    this.fetch_books[i].jss3_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Jss3"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Jss3"}).value:"";
                    
                    this.fetch_books[i].sss1_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Sss1"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Sss1"}).value:"";
                    this.fetch_books[i].sss2_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Sss2"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Sss2"}).value:"";
                    this.fetch_books[i].sss3_teachers_books=(this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Sss3"})!=null)?this.results[0].data.student_teacher_book.subject_teachers_textbooks_available.teacherbook.find(function(p){return p.subject==subject && p.class=="Sss3"}).value:"";
                    
                    }
                
                //summation for preprimary
                this.kindergarten1_male_total=parseInt(this.below3_kindergarten1_male)+parseInt(this.age3_kindergarten1_male);
                this.kindergarten1_female_total=parseInt(this.below3_kindergarten1_female)+parseInt(this.age3_kindergarten1_female);
                this.kindergarten2_male_total=parseInt(this.below3_kindergarten2_male)+parseInt(this.age3_kindergarten2_male);
                this.kindergarten2_female_total=parseInt(this.below3_kindergarten2_female)+parseInt(this.age3_kindergarten2_female);
                this.nursery1_male_total=parseInt(this.age3_nursery1_male)+parseInt(this.age4_nursery1_male)+parseInt(this.age5_nursery1_male)+parseInt(this.above5_nursery1_male);
                this.nursery1_female_total=parseInt(this.age3_nursery1_female)+parseInt(this.age4_nursery1_female)+parseInt(this.age5_nursery1_female)+parseInt(this.above5_nursery1_female);
                this.nursery2_male_total=parseInt(this.age3_nursery2_male)+parseInt(this.age4_nursery2_male)+parseInt(this.age5_nursery2_male)+parseInt(this.above5_nursery2_male);
                this.nursery2_female_total=parseInt(this.age3_nursery2_female)+parseInt(this.age4_nursery2_female)+parseInt(this.age5_nursery2_female)+parseInt(this.above5_nursery2_female);
                this.nursery3_male_total=parseInt(this.age3_nursery3_male)+parseInt(this.age4_nursery3_male)+parseInt(this.age5_nursery3_male)+parseInt(this.above5_nursery3_male);
                this.nursery3_female_total=parseInt(this.age3_nursery3_female)+parseInt(this.age4_nursery3_female)+parseInt(this.age5_nursery3_female)+parseInt(this.above5_nursery3_female);
    
                //sum total for entrant
                this.p1_entrant_male_total=this.fetch_primary_age.reduce(function(entrantmale_total, item){
                    
                    return entrantmale_total + parseInt(item.male); 
                  },0);
                this.p1_entrant_female_total=this.fetch_primary_age.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt(item.female); 
                    },0);
                this.p1_eccd_entrant_male_total=this.fetch_primary_age.reduce(function(entrantmale_total, item){
                    
                    return entrantmale_total + parseInt(item.eccd_male); 
                  },0);
                this.p1_eccd_entrant_female_total=this.fetch_primary_age.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt(item.eccd_female); 
                    },0);
                
                //sum total for year by age
                this.yearbyage_p1_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.primary1_male); 
                    },0);
                this.yearbyage_p1_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt(item.primary1_female); 
                    },0);
                this.yearbyage_p2_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.primary2_male); 
                    },0);
                this.yearbyage_p2_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt(item.primary2_female); 
                    },0);
                this.yearbyage_p3_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.primary3_male); 
                    },0);
                this.yearbyage_p3_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt(item.primary3_female); 
                    },0);
                this.yearbyage_p4_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.primary4_male); 
                    },0);
                this.yearbyage_p4_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt(item.primary4_female); 
                    },0);
                this.yearbyage_p5_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.primary5_male); 
                    },0);
                this.yearbyage_p5_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt(item.primary5_female); 
                    },0);
                this.yearbyage_p6_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.primary6_male); 
                    },0);
                this.yearbyage_p6_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
                
                    return entrantfemale_total + parseInt(item.primary6_female); 
                    },0);
    
    
    
                //summation for jss
                this.entrantmaletotal=this.fetch_age.reduce(function(entrantmale_total, item){
                    
                    return entrantmale_total + parseInt(item.male); 
                  },0);
                
                this.entrantfemaletotal=this.fetch_age.reduce(function(entrantfemale_total, item){
                
                return entrantfemale_total + parseInt(item.female); 
                },0);
    
    
                this.jss1maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.jss1_male); 
                    },0);
                this.jss1femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
            
                    return entrantfemale_total + parseInt(item.jss1_female); 
                    },0);
    
                this.jss2maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
            
                    return entrantmale_total + parseInt(item.jss2_male); 
                    },0);
                this.jss2femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
            
                    return entrantfemale_total + parseInt(item.jss2_female); 
                    },0);
                this.jss3maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.jss3_male); 
                    },0);
                this.jss3femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
            
                    return entrantfemale_total + parseInt(item.jss3_female); 
                    },0);
    
    
                //summation for ss
                //ss entrant
                this.ss1_entrantmaletotal=this.fetch_ss_age.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.male); 
                    },0);
                
                this.ss1_entrantfemaletotal=this.fetch_ss_age.reduce(function(entrantfemale_total, item){
                
                return entrantfemale_total + parseInt(item.female); 
                },0);
    
                this.ss1maletotal=this.fetch_ss_year_by_age.reduce(function(entrantmale_total, item){
        
                    return entrantmale_total + parseInt(item.sss1_male); 
                    },0);
                this.ss1femaletotal=this.fetch_ss_year_by_age.reduce(function(entrantfemale_total, item){
            
                    return entrantfemale_total + parseInt(item.sss1_female); 
                    },0);
    
                this.ss2maletotal=this.fetch_ss_year_by_age.reduce(function(entrantmale_total, item){
            
                    return entrantmale_total + parseInt(item.sss2_male); 
                    },0);
                this.ss2femaletotal=this.fetch_ss_year_by_age.reduce(function(entrantfemale_total, item){
            
                    return entrantfemale_total + parseInt(item.sss2_female); 
                    },0);
                this.ss3maletotal=this.fetch_ss_year_by_age.reduce(function(entrantmale_total, item){
                
                    return entrantmale_total + parseInt(item.sss3_male); 
                    },0);
                this.ss3femaletotal=this.fetch_ss_year_by_age.reduce(function(entrantfemale_total, item){
            
                    return entrantfemale_total + parseInt(item.sss3_female); 
                    },0);
    
                this.tq_preprimary_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                
                    return male_total + parseInt(item.preprimary_male); 
                    },0);
                    this.tq_preprimary_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                    
                        return female_total + parseInt(item.preprimary_female); 
                      },0);
                      this.tq_primary_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                    
                        return male_total + parseInt(item.primary_male); 
                      },0);
                      this.tq_primary_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                    
                        return female_total + parseInt(item.primary_female); 
                      },0);
                      this.tq_jss_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                    
                        return male_total + parseInt(item.jss_male); 
                      },0);
                      this.tq_jss_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                    
                        return female_total + parseInt(item.jss_female); 
                      },0);
                      this.tq_sss_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                    
                        return male_total + parseInt(item.sss_male); 
                      },0);
                      this.tq_sss_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                    
                        return female_total + parseInt(item.sss_female); 
                      },0);
                      this.tq_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                  
                        return male_total + parseInt(item.sss_male)+parseInt(item.jss_male)+ parseInt(item.primary_female)+parseInt(item.preprimary_female); 
                      },0);
                      this.tq_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                  
                        return female_total + parseInt(item.sss_female)+parseInt(item.jss_female)+ parseInt(item.primary_female)+parseInt(item.preprimary_female); 
                      },0);
                //set the spinner to stop loading             
                $("div#loader").addClass("hide");      
            }else{
                //set the spinner to stop loading             
                $("div#loader").addClass("hide");
                alert(this.results[1].message);
            }
        });




        var previousButton = document.querySelectorAll('button#previous');
        previousButton.forEach(element => {
          element.addEventListener('click', function () {
            var tabP = document.querySelectorAll('div#content > div');
            for (let index = 0; index < tabP.length; index++) {
              if (tabP[index].className == 'show-current-tab'){
                tabP[index].classList.remove('show-current-tab');
                var newPrevI = index - 1;
                tabP[newPrevI].classList.add('show-current-tab');
              }             
            }
          })
        });  

        function forLoader(params) {            
            var clickedButton = document.querySelectorAll(params);
            clickedButton.forEach(element => {                
                element.addEventListener('click', function () {
                    var loader = document.getElementById('loader');
                    loader.classList.replace('hide','show');
                    console.log(loader);
                });
            });
        }

        forLoader('button#next');
        forLoader('button#save');
        }else{
            window.location.assign("/login");
        }

        
    },

    watch:{
        boarding_choice: function() {
            var boardingmale = document.getElementById("boarding_male");
            var boardingfemale = document.getElementById("boarding_female");
            if (this.boarding_choice == 0) {
                
                boardingmale.required = false;
                boardingmale.disabled = true;
                boardingfemale.required = false;
                boardingfemale.disabled = true;
                
            } else {
                boardingmale.required = true;
                boardingmale.disabled = false;
                boardingfemale.required = true;
                boardingfemale.disabled = false;
            }
        }
    },

    methods:{
        print_form:function(){
            window.print();
        },
        checkgrade:function(message,event){
            var regex = /^((\d|1[0-7])\/(\d|1[0-5]))$/gm;
            if(!regex.test(message)){
                alert("Fix Grade level/step => ("+message+") to match [1-17]/[1-15]")
            }
            
        },
        popAlertMsg: function (message) {
            setTimeout(() => {
                if (loader.classList.contains('show')){
                    loader.classList.replace('show', 'hide');
                }
            }, 300);
            var alertBox = document.getElementById('alert');
            this.responseMsg = message;
            if (message.indexOf("Error") >= 0 ){
              alertBox.style.backgroundColor = '#ff0000';
            }
            alertBox.classList.add('active');
            setTimeout(() => {
              alertBox.classList.replace('active', null);
              this.responseMsg = '';
            }, 1500);
        },
        moveNext: function () {
            // this.showLoader();
            var tab = document.querySelectorAll('div#content > div');            
            for (let index = 0; index < tab.length; index++) {
              if (tab[index].className == 'show-current-tab'){
                  var tabContent = document.getElementById('content');
                  tabContent.classList.add('hide');
                  tab[index].classList.remove('show-current-tab');
                // var loader = document.getElementById('loader');
                // loader.classList.replace('hide', 'show');
                setTimeout(() => {
                  tabContent.classList.replace('hide', null);
                  var newIndex = index + 1;
                  tab[newIndex].classList.add('show-current-tab');            
                }, 500);
              }        
            }
        },
        //tq sum
        tq_preprimary_male_tot: function(){
            this.tq_preprimary_male_total=0;
            this.tq_preprimary_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                
                return male_total + parseInt(item.preprimary_male); 
              },0);

              this.tq_male_total=0;
              this.tq_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                  
                  return male_total + parseInt(item.primary_male)+parseInt(item.preprimary_male)+ parseInt(item.jss_male)+parseInt(item.sss_male); 
                },0);
        },

        tq_preprimary_female_tot: function(){
            this.tq_preprimary_female_total=0;
            this.tq_preprimary_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                
                return female_total + parseInt(item.preprimary_female); 
              },0);
              this.tq_female_total=0;
              this.tq_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                  
                  return female_total + parseInt(item.primary_female)+parseInt(item.preprimary_female)+ parseInt(item.jss_female)+parseInt(item.sss_female); 
                },0);
        },

        tq_primary_male_tot: function(){
            this.tq_primary_male_total=0;
            this.tq_primary_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                
                return male_total + parseInt(item.primary_male); 
              },0);

              this.tq_male_total=0;
              this.tq_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                  
                  return male_total + parseInt(item.primary_male)+parseInt(item.preprimary_male)+ parseInt(item.jss_male)+parseInt(item.sss_male); 
                },0);
        },

        tq_primary_female_tot: function(){
            this.tq_primary_female_total=0;
            this.tq_primary_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                
                return female_total + parseInt(item.primary_female); 
              },0);

              this.tq_female_total=0;
              this.tq_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                  
                  return female_total + parseInt(item.primary_female)+parseInt(item.preprimary_female)+ parseInt(item.jss_female)+parseInt(item.sss_female); 
                },0);
        },

        tq_jss_male_tot: function(){
            this.tq_jss_male_total=0;
            this.tq_jss_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                
                return male_total + parseInt(item.jss_male); 
              },0);

              this.tq_male_total=0;
              this.tq_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                  
                  return male_total + parseInt(item.jss_male)+parseInt(item.sss_male)+ parseInt(item.primary_male)+parseInt(item.preprimary_male); 
                },0);
        },

        tq_jss_female_tot: function(){
            this.tq_jss_female_total=0;
            this.tq_jss_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                
                return female_total + parseInt(item.jss_female); 
              },0);
              this.tq_female_total=0;
              this.tq_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                  
                  return female_total + parseInt(item.jss_female)+parseInt(item.sss_female)+ parseInt(item.primary_female)+parseInt(item.preprimary_female); 
                },0);
        },

        tq_sss_male_tot: function(){
            this.tq_sss_male_total=0;
            this.tq_sss_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                
                return male_total + parseInt(item.sss_male); 
              },0);

              this.tq_male_total=0;
              this.tq_male_total=this.fetch_teachingqualification.reduce(function(male_total, item){
                  
                  return male_total + parseInt(item.sss_male)+parseInt(item.jss_male)+ parseInt(item.primary_female)+parseInt(item.preprimary_female); 
                },0);
        },

        tq_sss_female_tot: function(){
            this.tq_sss_female_total=0;
            this.tq_sss_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                
                return female_total + parseInt(item.sss_female); 
              },0);

              this.tq_female_total=this.fetch_teachingqualification.reduce(function(female_total, item){
                  
                  return female_total + parseInt(item.sss_female)+parseInt(item.jss_female)+ parseInt(item.primary_female)+parseInt(item.preprimary_female); 
                },0);
        },

        //summation methods for preprimary
        kindergarten1_male_tot: function(){
            this.kindergarten1_male_total=0;
            this.kindergarten1_male_total=parseInt(this.below3_kindergarten1_male)+parseInt(this.age3_kindergarten1_male);
        },

        kindergarten1_female_tot:function(){
            this.kindergarten1_female_total=0;
            this.kindergarten1_female_total=parseInt(this.below3_kindergarten1_female)+parseInt(this.age3_kindergarten1_female);

        },

        kindergarten2_male_tot:function(){
            this.kindergarten2_male_total=0;
            this.kindergarten2_male_total=parseInt(this.below3_kindergarten2_male)+parseInt(this.age3_kindergarten2_male);

        },

        kindergarten2_female_tot:function(){
            this.kindergarten2_female_total=0;
            this.kindergarten2_female_total=parseInt(this.below3_kindergarten2_female)+parseInt(this.age3_kindergarten2_female);

        },

        nursery1_male_tot:function(){
            this.nursery1_male_total=0;
            this.nursery1_male_total=parseInt(this.age3_nursery1_male)+parseInt(this.age4_nursery1_male)+parseInt(this.age5_nursery1_male)+parseInt(this.above5_nursery1_male);

        },

        nursery1_female_tot:function(){
            this.nursery1_female_total=0;
            this.nursery1_female_total=parseInt(this.age3_nursery1_female)+parseInt(this.age4_nursery1_female)+parseInt(this.age5_nursery1_female)+parseInt(this.above5_nursery1_female);

        },

        nursery2_male_tot:function(){
            this.nursery2_male_total=0;
            this.nursery2_male_total=parseInt(this.age3_nursery2_male)+parseInt(this.age4_nursery2_male)+parseInt(this.age5_nursery2_male)+parseInt(this.above5_nursery2_male);

        },

        nursery2_female_tot:function(){
            this.nursery2_female_total=0;
            this.nursery2_female_total=parseInt(this.age3_nursery2_female)+parseInt(this.age4_nursery2_female)+parseInt(this.age5_nursery2_female)+parseInt(this.above5_nursery2_female);

        },

        nursery3_male_tot:function(){
            this.nursery3_male_total=0;
            this.nursery3_male_total=parseInt(this.age3_nursery3_male)+parseInt(this.age4_nursery3_male)+parseInt(this.age5_nursery3_male)+parseInt(this.above5_nursery3_male);

        },
        
        nursery3_female_tot:function(){
            this.nursery3_female_total=0;
            this.nursery3_female_total=parseInt(this.age3_nursery3_female)+parseInt(this.age4_nursery3_female)+parseInt(this.age5_nursery3_female)+parseInt(this.above5_nursery3_female);

        },

        //sum total methods
        pp_entrantmale_total: function(){
            this.p1_entrant_male_total=0;
            this.p1_entrant_male_total=this.fetch_primary_age.reduce(function(entrantmale_total, item){
                
                return entrantmale_total + parseInt(item.male); 
              },0);
        },

        pp_entrantfemale_total:function(){
            this.p1_entrant_female_total=0;
            this.p1_entrant_female_total=this.fetch_primary_age.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt(item.female); 
                },0);
        },

        eccd_entrantmale_total:function(){
            this.p1_eccd_entrant_male_total=0;
            this.p1_eccd_entrant_male_total=this.fetch_primary_age.reduce(function(entrantmale_total, item){
                
                return entrantmale_total + parseInt(item.eccd_male); 
              },0);
        },

        eccd_entrantfemale_total:function(){
            this.p1_eccd_entrant_female_total=0;
            this.p1_eccd_entrant_female_total=this.fetch_primary_age.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt(item.eccd_female); 
                },0);
        },


        yearbyage_p1_male_tot:function(){
            this.yearbyage_p1_male_total=0;
            this.yearbyage_p1_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.primary1_male); 
                },0);
        },

        yearbyage_p1_female_tot:function(){
            this.yearbyage_p1_female_total=0;
            this.yearbyage_p1_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt(item.primary1_female); 
                },0);
        },

        yearbyage_p2_male_tot:function(){
            this.yearbyage_p2_male_total=0;
            this.yearbyage_p2_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.primary2_male); 
                },0);
        },

        yearbyage_p2_female_tot:function(){
            this.yearbyage_p2_female_total=0;
            this.yearbyage_p2_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt(item.primary2_female); 
                },0);
        },

        yearbyage_p3_male_tot:function(){
            this.yearbyage_p3_male_total=0;
            this.yearbyage_p3_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.primary3_male); 
                },0);
        },

        yearbyage_p3_female_tot:function(){
            this.yearbyage_p3_female_total=0;
            this.yearbyage_p3_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt(item.primary3_male); 
                },0);
        },

        yearbyage_p4_male_tot:function(){
            this.yearbyage_p4_male_total=0;
            this.yearbyage_p4_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.primary4_male); 
                },0);
        },

        yearbyage_p4_female_tot:function(){
            this.yearbyage_p4_female_total=0;
            this.yearbyage_p4_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt(item.primary4_female); 
                },0);
        },

        yearbyage_p5_male_tot:function(){
            this.yearbyage_p5_male_total=0;
            this.yearbyage_p5_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.primary5_male); 
                },0);
        },

        yearbyage_p5_female_tot:function(){
            this.yearbyage_p5_female_total=0;
            this.yearbyage_p5_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt(item.primary5_female); 
                },0);
        },

        yearbyage_p6_male_tot:function(){
            this.yearbyage_p6_male_total=0;
            this.yearbyage_p6_male_total=this.fetch_age_primary.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.primary6_male); 
                },0);
        },

        yearbyage_p6_female_tot:function(){
            this.yearbyage_p6_female_total=0;
            this.yearbyage_p6_female_total=this.fetch_age_primary.reduce(function(entrantfemale_total, item){
            
                return entrantfemale_total + parseInt(item.primary6_female); 
                },0);
        },

        entrantmale_total: function(){
            this.entrantmaletotal=0;
            this.entrantmaletotal=this.fetch_age.reduce(function(entrantmale_total, item){
                
                return entrantmale_total + parseInt((item.male==''?0:item.male)); 
              },0);
        },
        
        entrantfemale_total: function(){
            this.entrantfemaletotal=0;
            this.entrantfemaletotal=this.fetch_age.reduce(function(entrantfemale_total, item){
                
                return entrantfemale_total + parseInt((item.female==''?0:item.female)); 
              },0);
        },

        jss1entrantmale_total: function(){
            this.jss1maletotal=0;
            this.jss1maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.jss1_male); 
                },0);
        },
        jss1entrantfemale_total: function(){
            this.jss1femaletotal=0;
            this.jss1femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
        
                return entrantfemale_total + parseInt(item.jss1_female); 
                },0);
        },
        jss2entrantmale_total: function(){
            this.jss2maletotal=0;
            this.jss2maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
        
                return entrantmale_total + parseInt(item.jss2_male); 
                },0);
        },
        jss2entrantfemale_total: function(){
            this.jss2femaletotal=0;
            this.jss2femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
        
                return entrantfemale_total + parseInt(item.jss2_female); 
                },0);
        },
        jss3entrantmale_total: function(){
            this.jss3maletotal=0;
            this.jss3maletotal=this.fetch_year_by_age.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.jss3_male); 
                },0);
        },
        jss3entrantfemale_total: function(){
            this.jss3femaletotal=0;
            this.jss3femaletotal=this.fetch_year_by_age.reduce(function(entrantfemale_total, item){
        
                return entrantfemale_total + parseInt(item.jss3_female); 
                },0);
        },

        //ss

        ss1_entrantmale_total: function(){
            this.ss1_entrantmaletotal=0;
            this.ss1_entrantmaletotal=this.fetch_ss_age.reduce(function(entrantmale_total, item){
                
                return entrantmale_total + parseInt((item.male==''?0:item.male)); 
              },0);
        },
        ss1_entrantfemale_total: function(){
            this.ss1_entrantfemaletotal=0;
            this.ss1_entrantfemaletotal=this.fetch_ss_age.reduce(function(entrantfemale_total, item){
                
                return entrantfemale_total + parseInt((item.female==''?0:item.female)); 
              },0);
        },

        ss1entrantmale_total: function(){
            this.ss1maletotal=0;
            this.ss1maletotal=this.fetch_ss_year_by_age.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.sss1_male); 
                },0);
        },
        ss1entrantfemale_total: function(){
            this.ss1femaletotal=0;
            this.ss1femaletotal=this.fetch_ss_year_by_age.reduce(function(entrantfemale_total, item){
        
                return entrantfemale_total + parseInt(item.sss1_female); 
                },0);
        },
        ss2entrantmale_total: function(){
            this.ss2maletotal=0;
            this.ss2maletotal=this.fetch_ss_year_by_age.reduce(function(entrantmale_total, item){
        
                return entrantmale_total + parseInt(item.sss2_male); 
                },0);
        },
        ss2entrantfemale_total: function(){
            this.ss2femaletotal=0;
            this.ss2femaletotal=this.fetch_ss_year_by_age.reduce(function(entrantfemale_total, item){
        
                return entrantfemale_total + parseInt(item.sss2_female); 
                },0);
        },
        ss3entrantmale_total: function(){
            this.ss3maletotal=0;
            this.ss3maletotal=this.fetch_ss_year_by_age.reduce(function(entrantmale_total, item){
            
                return entrantmale_total + parseInt(item.sss3_male); 
                },0);
        },
        ss3entrantfemale_total: function(){
            this.ss3femaletotal=0;
            this.ss3femaletotal=this.fetch_ss_year_by_age.reduce(function(entrantfemale_total, item){
        
                return entrantfemale_total + parseInt(item.sss3_female); 
                },0);
        },

        //staff total
        non_teaching_staff_total_change:function(){
            this.non_teaching_staff_total=0;
            this.non_teaching_staff_total=parseInt(this.non_teaching_staff_male)+parseInt(this.non_teaching_staff_female);
        },
        teaching_staff_total_change:function(){
            this.teaching_staff_total=0;
            this.teaching_staff_total=parseInt(this.teaching_staff_male)+parseInt(this.teaching_staff_female);
        },

        //save the sections
        saveCensusYear: function (event) {
            if (event.target.id === 'next') {                
              this.popAlertMsg('Success');
              this.moveNext();
              console.log('next');
            } else if (event.target.id === 'save') {
              this.popAlertMsg('Saved');
              console.log('save');
            }
        },

        validNumericOrDecimalOrNull:function(number){
            var re = /(^$)|(^[1-9]\d*(\.\d+)?$)/;
            return re.test(number);
        },
      
        validAlphabetOrNull:function(alphabet){
            var re = /(^$)|(^[A-Za-z]+$)/;
            return re.test(alphabet);
        },
      
        validNumericOrDecimal:function(number){
            var re = /(^[1-9]\d*(\.\d+)?$)/;
            return re.test(number);
        },
      
        validAlphabet:function(alphabet){
            var re = /(^[A-Za-z]+$)/;
            return re.test(alphabet);
        },
      
        validEmail:function(email){
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },

        saveIdentification: function (event) {
            this.school_identification_errors=[];
            if(!this.validNumericOrDecimalOrNull(this.xcoordinate) || !this.validNumericOrDecimalOrNull(this.ycoordinate) 
            || !this.validNumericOrDecimalOrNull(this.zcoordinate) || this.validNumericOrDecimalOrNull(this.schoolname) 
            || this.validNumericOrDecimal(this.schooltown) || this.validNumericOrDecimal(this.schoolward) 
            || !this.validEmail(this.schoolemail)){
                
                //post errors
                this.popAlertMsg('Check the form for the required data');

                (!this.validNumericOrDecimalOrNull(this.xcoordinate))?this.school_identification_errors.push("x coordinate should be numeric decimal"):null;
                (!this.validNumericOrDecimalOrNull(this.ycoordinate))?this.school_identification_errors.push("y coordinate should be numeric decimal"):null;
                (!this.validNumericOrDecimalOrNull(this.zcoordinate))?this.school_identification_errors.push("z coordinate should be numeric decimal"):null;
                (this.validNumericOrDecimalOrNull(this.schoolname))?this.school_identification_errors.push("School Name should be alphabets"):null;
                (this.validNumericOrDecimal(this.schooltown))?this.school_identification_errors.push("School town should be alphabets"):null;
                (!this.validEmail(this.schoolemail))?this.school_identification_errors.push("School email incorrect format"):null;
                (this.validNumericOrDecimal(this.schoolward))?this.school_identification_errors.push("School ward should be alphabets"):null;

            }else{
                
                this.get_state_code = this.fetch_states[this.state_picked].name
                //alert(this.get_state_code);
                this.get_lga_code = this.fetch_lga[this.lga_picked].name;
                //alert(this.config);
                //get the access token
            
                axios.post("/api/Section/SchoolRegistration",
                {
                    school_code: schoolcode.toLowerCase(),
                    register_year: this.registered_censusyear,
                    year:year
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert("Data Saved to DB");
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            // this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(response.data.message);
                        //alert("Data Saved to DB");
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            // this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                        //console.log(response.data);
                    }
                });

                axios.post("/api/Section/SchoolIdentification",
                {
                    school_code: schoolcode.toLowerCase(),
                    school_name: this.schoolname,
                    xcoordinate: this.xcoordinate,
                    ycoordinate: this.ycoordinate,
                    zcoordinate: this.zcoordinate,
                    school_address: this.schoolstreet,
                    proprietor:this.proprietor,
                    jss_schoolcode:this.jss_schoolcode,
                    sss_schoolcode:this.sss_schoolcode,
                    primary_schoolcode:this.primary_schoolcode,
                    town: this.schooltown,
                    ward: this.schoolward,
                    lga: this.get_lga_code,
                    state: this.get_state_code,
                    telephone: this.schooltelephone,
                    email: this.schoolemail
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert("Data Saved to DB");
                        //alert("Data Saved to DB");
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        this.popAlertMsg("Check the form for the required data");
                        // console.log("Check the form for the required data");
                    }
                });
            }
        },
        
        //log out
        logout:function(){
            axios.post("/api/Section/User/Logout",
            {
                email:this.user_email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.$cookies.remove("user");
                window.location.assign("/login");
            })
        },

        changelevel: function(message,event){
            //this.ispreprimary = this.level_of_education.some(function(el){ return el === "Pre-primary"});
            //alert(this.ispreprimary);
            if(event.target.checked==false){
                axios.post("/api/Section/Remove/SchoolLevel",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        level:message
                        
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            console.log("Data Saved to DB");
                            //set school levels conditions
                        this.ispreprimary = this.level_of_education.some(function(el){ return el === "Pre-primary"});
                        this.isprimary = this.level_of_education.some(function(el){ return el === "Primary"});
                        this.isjss = this.level_of_education.some(function(el){ return el === "Junior Secondary"});
                        this.issss = this.level_of_education.some(function(el){ return el === "Senior Secondary"});
                            //location.reload();
                        } else {
                            console.log("Check the form for the required data");
                        }
                    });
            }else{
                //set school levels conditions
            this.ispreprimary = this.level_of_education.some(function(el){ return el === "Pre-primary"});
            this.isprimary = this.level_of_education.some(function(el){ return el === "Primary"});
            this.isjss = this.level_of_education.some(function(el){ return el === "Junior Secondary"});
            this.issss = this.level_of_education.some(function(el){ return el === "Senior Secondary"});
            }
            
        },

        getLGA: function () {
            var arrayOfObjects2 = [];

            //this.counter_state = this.state_index[this.state_picked];
            //alert(this.state_picked);

            for (var i = 0; i < this.results[1].attributes.states[this.state_picked].data.lgas.length; i++) {
                var obj2 = {};
                
                obj2["name"] = this.results[1].attributes.states[this.state_picked].data.lgas[i].data.name;
                obj2["code"] = this.results[1].attributes.states[this.state_picked].data.lgas[i].data.lgacode;
                //this.lga_index[this.results[1].attributes.states[i].data.lgas[i].data.name] = i;
            

                arrayOfObjects2.push(obj2);
            }
            //this.lga_picked=this.results[1].attributes.states[this.counter_state].data.lgas[0].data.name;
            this.fetch_lga = arrayOfObjects2;


            console.log('now: ', JSON.stringify(this.fetch_states, null, " "));
            //alert(this.lga_picked);
        },

        saveCharacteristics: function (event) {
            //alert(this.no_of_inspection);
            if(this.level_of_education.length){
                axios.post("/api/Section/SchoolCharacteristics",
                {
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    year_of_establishment_preprimary:this.year_of_establishment_preprimary,
                    year_of_establishment_primary:this.year_of_establishment_primary,
                    year_of_establishment_jss:this.year_of_establishment_jss,
                    year_of_establishment_ss:this.year_of_establishment_ss,
                    shared_facilities: this.facilities_choice,
                    schools_sharingwith: this.facilities_shared,
                    location_type: this.location_of_school,
                    school_type: this.type_of_school,
                    owner:this.ownership,
                    recognition_status:this.recognition_status,
                    shift:this.shifts_choice,
                    level_of_educations: this.level_of_education,
                    multi_grade_teaching: this.multigrade_choice,
                    distance_from_catchment_area: this.average_distance,
                    students_travelling_3km: this.student_distance,
                    isboarding:this.boarding_choice,
                    male_student_b: this.students_boarding_male,
                    female_student_b: this.students_boarding_female,
                    has_school_development_plan: this.sdp_choice,
                    has_sbmc: this.sbmc_choice,
                    has_pta: this.pta_choice,
                    is_psa:this.is_psa,
                    last_inspection_date: this.date_inspection,
                    no_of_inspection:this.no_of_inspection,
                    last_inspection_authority: this.authority_choice,
                    conditional_cash_transfer: this.cash_transfer,
                    has_school_grant: this.grants_choice,
                    has_security_guard: this.guard_choice,
                    ownership: this.ownership_choice,
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                        // console.log("Data Saved to DB");
                        // "Check the form for the required data"
                    } else {
                        this.popAlertMsg(response.data);
                        // console.log(response.data);
                    }
                }).catch(error => {
                    this.popAlertMsg(error.message);
                    alert(error.message)
                });
            }else{
                this.popAlertMsg("School level is required");
                alert("School level is required")
            }
            
            
                
        },

        checkBirthCertificates:function(){
            console.log("bc:"+this.fetch_birth_certificate[0].value+" ,male:"+this.fetch_birth_certificate[0].male);
        },

        saveBirthCertificates: function (event) {
            function checkNpc(bc) {
                return bc.value == "National Population Commission";
            }
            function checkHospital(bc) {
                return bc.value == "Hospital";
            }
            function checkLga(bc) {
                return bc.value == "LGA";
            }
            function checkCourt(bc) {
                return bc.value == "Court";
            }
            function checkUn(bc) {
                return bc.value == "UN";
            }
            function checkOthers(bc) {
                return bc.value == "Others";
            }

            axios.post("/api/Section/SchoolEnrollment/BirthCertificate",{
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class: "preprimary",
                birthcertificate:this.fetch_birth_certificate,
                
            },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //saved
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                        
                    } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(response.data.message);
                    }else {
                        console.log("Check the form for the required data");
                    }
                })
        },

        saveSecondaryBirthCertificates: function (event) {
            function checkNpc(bc) {
                return bc.value == "National Population Commission";
            }
            function checkHospital(bc) {
                return bc.value == "Hospital";
            }
            function checkLga(bc) {
                return bc.value == "LGA";
            }
            function checkCourt(bc) {
                return bc.value == "Court";
            }
            function checkUn(bc) {
                return bc.value == "UN";
            }
            function checkOthers(bc) {
                return bc.value == "Others";
            }

            axios.post("/api/Section/SchoolEnrollment/BirthCertificate",{
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class: "jss",
                birthcertificate:this.fetch_birth_certificate,
                
            },this.config).then(response => {

                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //this.fetch_b_c = this.fetch_b_c;
                        //alert("Data Saved to DB");
                        //alert(JSON.stringify(this.fetch_b_c, null, 2));
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            // this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(response.data.message);
                    }else {
                        console.log("Check the form for the required data");
                    }
                });

            axios.post("/api/Section/SchoolEnrollment/BirthCertificate",{
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class: "sss",
                    birthcertificate:this.fetch_birth_certificate,
                    
                },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //this.fetch_b_c = this.fetch_b_c;
                            //alert("Data Saved to DB");
                            //alert(JSON.stringify(this.fetch_b_c, null, 2));
                            if (event.target.id === 'next') {                
                                this.popAlertMsg(this.resultsSave.status);
                                this.moveNext();
                                // console.log('next');
                            } else if (event.target.id === 'save') {
                                this.popAlertMsg(this.resultsSave.status);
                                // console.log('save');
                            }
                        } else if(this.resultsSave.status == "error") {
                            // console.log(response.data.message);
                            this.popAlertMsg(response.data.message);
                        }else {
                            console.log("Check the form for the required data");
                        }
                    });
        },

        savePreprimaryEntrants: function (event) {
            //alert(JSON.stringify(this.fetch_a))
             axios.post("/api/Section/SchoolEnrollment/Entrant",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "preprimary",
                 entrants:this.fetch_primary_age,
                 
             },this.config).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                        //  console.log("Data Saved to DB");
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                     } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(this.resultsSave.status);
                     }else {
                        // console.log("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data')
                     }
                 })
         },

        saveEntrants: function (event) {
           //alert(JSON.stringify(this.fetch_a))
            axios.post("/api/Section/SchoolEnrollment/Entrant",{
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class: "jss",
                entrants:this.fetch_age,
                
            },this.config).then(response => {

                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //this.fetch_b_c = this.fetch_b_c;
                        //alert("Data Saved to DB");
                        // console.log("Data Saved to DB");
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }else {
                        // console.log("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                    }
                })
        },

        saveSsEntrants: function (event) {
            //alert(JSON.stringify(this.fetch_a))
             axios.post("/api/Section/SchoolEnrollment/Entrant",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "sss",
                 entrants:this.fetch_ss_age,
                 
             },this.config).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                        
                        //  console.log("Data Saved to DB");
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                     } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(this.resultsSave.status);
                     }else {
                        // console.log("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }
                 })
         },

        savePreprimaryEnrollmentByAge: function (event) {
            
            //alert(this.fetch_year_by_age.find(function(age){return age.value=="12"}).Primary3_male);
            var enrollments_objs=[{value:'Below 3',kindergarten1_male:this.below3_kindergarten1_male,kindergarten1_female:this.below3_kindergarten1_female,kindergarten2_male:this.below3_kindergarten2_male,kindergarten2_female:this.below3_kindergarten2_female},{value:'3',kindergarten1_male:this.age3_kindergarten1_male,kindergarten1_female:this.age3_kindergarten1_female,kindergarten2_male:this.age3_kindergarten2_male,kindergarten2_female:this.age3_kindergarten2_female,nursery1_male:this.age3_nursery1_male,nursery1_female:this.age3_nursery1_female,nursery2_male:this.age3_nursery2_male,nursery2_female:this.age3_nursery2_female,nursery3_male:this.age3_nursery3_male,nursery3_female:this.age3_nursery3_female},{value:'4',nursery1_male:this.age4_nursery1_male,nursery1_female:this.age4_nursery1_female,nursery2_male:this.age4_nursery2_male,nursery2_female:this.age4_nursery2_female,nursery3_male:this.age4_nursery3_male,nursery3_female:this.age4_nursery3_female},{value:'5',nursery1_male:this.age5_nursery1_male,nursery1_female:this.age5_nursery1_female,nursery2_male:this.age5_nursery2_male,nursery2_female:this.age5_nursery2_female,nursery3_male:this.age5_nursery3_male,nursery3_female:this.age5_nursery3_female},{value:'Above 5 Years',nursery1_male:this.above5_nursery1_male,nursery1_female:this.above5_nursery1_female,nursery2_male:this.above5_nursery2_male,nursery2_female:this.above5_nursery2_female,nursery3_male:this.above5_nursery3_male,nursery3_female:this.above5_nursery3_female}]

             axios.post("/api/Section/SchoolEnrollment/EnrollmentByAge",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "preprimary",
                 stream_kindergarten1_stream: this.kindergarten1_stream,
                 stream_kindergarten2_stream: this.kindergarten2_stream,
                 stream_nursery1_stream: this.nursery1_stream,
                 stream_nursery2_stream: this.nursery2_stream,
                 stream_nursery3_stream: this.nursery3_stream,

                 enrollments:enrollments_objs,

             },this.config).then(response => { 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                         if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                     } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(response.data.message)
                     }else {
                        // console.log("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data')
                     }
 
                 })
         },

        savePrimaryEnrollmentByAge: function (event) {
            //alert(this.fetch_year_by_age.find(function(age){return age.value=="12"}).Primary3_male);
             axios.post("/api/Section/SchoolEnrollment/EnrollmentByAge",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "primary",
                 stream_primary1_stream: this.p1_stream,
                 stream_primary2_stream: this.p2_stream,
                 stream_primary3_stream: this.p3_stream,
                 stream_primary4_stream: this.p4_stream,
                 stream_primary5_stream: this.p5_stream,
                 stream_primary6_stream: this.p6_stream,

                 stream_primary1_streamwithmultigrade:  this.p1_stream_with_multigrade,
                 stream_primary2_streamwithmultigrade:  this.p2_stream_with_multigrade,
                 stream_primary3_streamwithmultigrade:  this.p3_stream_with_multigrade,
                 stream_primary4_streamwithmultigrade:  this.p4_stream_with_multigrade,
                 stream_primary5_streamwithmultigrade:  this.p5_stream_with_multigrade,
                 stream_primary6_streamwithmultigrade:  this.p6_stream_with_multigrade,

                 enrollments:this.fetch_age_primary,

                 repeater_primary1_male:  this.p1_repeaters_male,
                 repeater_primary1_female:  this.p1_repeaters_female,
                 repeater_primary2_male:  this.p2_repeaters_male,
                 repeater_primary2_female:  this.p2_repeaters_female,
                 repeater_primary3_male:  this.p3_repeaters_male,
                 repeater_primary3_female:  this.p3_repeaters_female,
                 repeater_primary4_male:  this.p4_repeaters_male,
                 repeater_primary4_female:  this.p4_repeaters_female,
                 repeater_primary5_male:  this.p5_repeaters_male,
                 repeater_primary5_female:  this.p5_repeaters_female,
                 repeater_primary6_male:  this.p6_repeaters_male,
                 repeater_primary6_female:  this.p6_repeaters_female,

                 prevyear_primary6_male:this.prevyear_primary6_male,
                 prevyear_primary6_female:this.prevyear_primary6_female
                 
             },this.config).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                         if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        } 
                     } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(this.resultsSave.status);
                     }else {
                        // console.log("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }
 
                 })
         },
        
        saveEnrollmentByAge: function (event) {
            //alert(this.fetch_year_by_age.find(function(age){return age.value=="12"}).jss3_male);
             axios.post("/api/Section/SchoolEnrollment/EnrollmentByAge",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "jss",
                 stream_jss1_stream: this.jss1_stream,
                 stream_jss2_stream: this.jss2_stream,
                 stream_jss3_stream: this.jss3_stream,

                 stream_jss1_streamwithmultigrade:  this.jss1_stream_with_multigrade,
                 stream_jss2_streamwithmultigrade:  this.jss2_stream_with_multigrade,
                 stream_jss3_streamwithmultigrade:  this.jss3_stream_with_multigrade,

                 enrollments:this.fetch_year_by_age,
                 
                 repeater_jss1_male:  this.jss1_repeaters_male,
                 repeater_jss1_female:  this.jss1_repeaters_female,
                 repeater_jss2_male:  this.jss2_repeaters_male,
                 repeater_jss2_female:  this.jss2_repeaters_female,
                 repeater_jss3_male:  this.jss3_repeaters_male,
                 repeater_jss3_female:  this.jss3_repeaters_female,

                 prevyear_jss3_male:this.prevyear_jss3_male,
                 prevyear_jss3_female:this.prevyear_jss3_female
                 
             },this.config).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                         if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
 
                     } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(this.resultsSave.status);
                     }else {
                        // console.log("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }
 
                 })
         },

        saveSsEnrollmentByAge: function (event) {
            
            //alert(this.fetch_year_by_age.find(function(age){return age.value=="15"}).ss3_male);
 
             axios.post("/api/Section/SchoolEnrollment/EnrollmentByAge",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "sss",
                 stream_sss1_stream: this.ss1_stream,
                 stream_sss2_stream: this.ss2_stream,
                 stream_sss3_stream: this.ss3_stream,

                 stream_sss1_streamwithmultigrade:  this.ss1_stream_with_multigrade,
                 stream_sss2_streamwithmultigrade:  this.ss2_stream_with_multigrade,
                 stream_sss3_streamwithmultigrade:  this.ss3_stream_with_multigrade,

                 enrollments:this.fetch_ss_year_by_age,

                 repeater_sss1_male:  this.ss1_repeaters_male,
                 repeater_sss1_female:  this.ss1_repeaters_female,
                 repeater_sss2_male:  this.ss2_repeaters_male,
                 repeater_sss2_female:  this.ss2_repeaters_female,
                 repeater_sss3_male:  this.ss3_repeaters_male,
                 repeater_sss3_female:  this.ss3_repeaters_female,

                 prevyear_sss3_male:this.prevyear_sss3_male,
                 prevyear_sss3_female:this.prevyear_sss3_female
                 
             },this.config).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                         if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        } 
                     } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(this.resultsSave.status);
                     }else {
                        // console.log("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }
 
                 })
         },
        
        savePreprimarySpecialNeed: function (event) {
            //console.log(JSON.stringify(this.fetch_all_special_needs, null, 2))
            
             axios.post("/api/Section/SchoolEnrollment/SpecialNeeds",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "preprimary",

                 special_needs:this.fetch_special_needs,
               
             },this.config).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                        //  console.log("Data Saved to DB");
                         //console.log("0" + this.fetch_r[0].repeaters);
                         if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
 
                     } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(this.resultsSave.status);
                     }else {
                        // console.log("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }
                 })
         },

        saveStudentsOrphan: function (event) {
            //console.log(this.fetch_orphans.find(function(pupil){return pupil.value=="Lost Mother"}).eccd_male)
            
             axios.post("/api/Section/SchoolEnrollment/Orphan",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "preprimary",
                 orphans:this.fetch_orphans,
                 
             },this.config).then(response => {
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                         if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                     } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(this.resultsSave.status);
                     }else {
                        // console.log("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }
                 })
         },
        
        saveSpecialNeed: function (event) {
            //console.log(JSON.stringify(this.fetch_all_special_needs, null, 2))           
            //Post request for the jss
             axios.post("/api/Section/SchoolEnrollment/SpecialNeeds",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "jss",
                 special_needs:this.fetch_special_needs,

                 
             },this.config).then(response => {
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                        //  console.log("Data Saved to DB");
                        //  console.log("0" + this.fetch_r[0].repeaters);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            // this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                     } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(this.resultsSave.status);
                     }else {
                        // console.log("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }
                 });
            
                 //Post request for secondary
            axios.post("/api/Section/SchoolEnrollment/SpecialNeeds",{
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class: "sss",
                    
                    special_needs:this.fetch_special_needs,
                    
                },this.config).then(response => {
                   this.resultsSave = response.data;
                   if (this.resultsSave.status == "success") {
                    //    console.log("Data Saved to DB");        
                    if (event.target.id === 'next') {                
                        this.popAlertMsg(this.resultsSave.status);
                        this.moveNext();
                        // console.log('next');
                    } else if (event.target.id === 'save') {
                        this.popAlertMsg(this.resultsSave.status);
                        // console.log('save');
                    }             
                   } else if(this.resultsSave.status == "error") {
                    // console.log(response.data.message);
                    this.popAlertMsg(this.resultsSave.status);
                   }else {
                    // console.log("Check the form for the required data");
                    this.popAlertMsg('Check the form for the required data');
                   }
               })
         },

        saveStudentsFlow: function (event) {
            //alert(JSON.stringify(this.fetch_drop, null, 2))
 
             axios.post("/api/Section/SchoolEnrollment/StudentFlow",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "jss",
                 pupil_flow:this.fetch_pupilflow,
                 
             },this.config).then(response => {
 
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                         if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            // this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }           
                     } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(this.resultsSave.status);
                     }else {
                        // console.log("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }
                 });
            
            axios.post("/api/Section/SchoolEnrollment/StudentFlow",{
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    school_class: "sss",
                    pupil_flow:this.fetch_pupilflow,
                    

                },this.config).then(response => {
    
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //this.fetch_b_c = this.fetch_b_c;
                            //alert("Data Saved to DB");
                            if (event.target.id === 'next') {                
                                this.popAlertMsg(this.resultsSave.status);
                                this.moveNext();
                                // console.log('next');
                            } else if (event.target.id === 'save') {
                                this.popAlertMsg(this.resultsSave.status);
                                // console.log('save');
                            }                            
                        } else if(this.resultsSave.status == "error") {
                            // console.log(response.data.message);
                            this.popAlertMsg(this.resultsSave.status);
                        }else {
                            // console.log("Check the form for the required data");
                            this.popAlertMsg('Check the form for the required data');
                        }

                    });
         },

        savePreprimaryStudentsFlow: function (event) {
            //alert(JSON.stringify(this.fetch_drop, null, 2))
 
             axios.post("/api/Section/SchoolEnrollment/StudentFlow",{
                 school_code: schoolcode.toLowerCase(),
                 year: year,
                 school_class: "preprimary",
                 pupil_flow:this.fetch_pupilflow,
                 
                 
             },this.config).then(response => {
                     this.resultsSave = response.data;
                     if (this.resultsSave.status == "success") {
                         //this.fetch_b_c = this.fetch_b_c;
                         //alert("Data Saved to DB");
                         if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                     } else if(this.resultsSave.status == "error") {
                        // console.log(response.data.message);
                        this.popAlertMsg(this.resultsSave.status);
                     }else {
                        // console.log("Check the form for the required data");
                        this.popAlertMsg('Check the form for the required data');
                     }

                 }) 
         },

        saveExamination: function () {
            if(this.registered_male >= this.took_part_male && this.took_part_male >= this.passed_male && this.registered_female >= this.took_part_female && this.took_part_female >= this.passed_female){
                axios.post("/api/Section/SchoolEnrollment/Examination",
                {
                    school_code: schoolcode.toLowerCase(),
                    year: year,
                    examination: "nateb",

                    examination_nateb_registeredmale: this.registered_male,
                    examination_nateb_registeredfemale: this.registered_female,
                    examination_nateb_tookpartmale: this.took_part_male,
                    examination_nateb_tookpartfemale: this.took_part_female,
                    examination_nateb_passedmale: this.passed_male,
                    examination_nateb_passedfemale: this.passed_female

                },this.config).then(response => {

                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //alert(this.resultsSave.message);
                        } else {
                            console.log(this.resultsSave.message);
                        }
                    })
            }else{
                alert("male and female registered is less than the took part or passed");
                this.popAlertMsg("Correct it!");
            }
                
                },

        
        saveNoOfStaff: function () {
            axios.post("/api/Section/Staff/NoOfStaff",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
                

                staff_nonteachersmale: this.non_teaching_staff_male,
                staff_nonteachersfemale: this.non_teaching_staff_female,
                staff_teachersmale: this.teaching_staff_male,
                staff_teachersfemale: this.teaching_staff_female,
                

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                    } else {
                        console.log(this.resultsSave.message);
                    }
                })
            },

        submitStaff: function(){
            //add to the table
            var thestaff={staff_file_no:this.staff_file_no,staff_name:this.staff_name,staff_gender:this.staff_gender,staff_type:this.staff_type,salary_source:this.salary_source,staff_yob:this.staff_yob,staff_yfa:this.staff_yfa,staff_ypa:this.staff_ypa,staff_yps:this.staff_yps,staff_level:this.staff_level,present:this.present,academic_qualification:this.academic_qualification,teaching_qualification:this.teaching_qualification,area_specialisation:this.area_specialisation,subject_taught:this.subject_taught,teaching_type:this.teaching_type,is_teaching_ss:this.is_teaching_ss,attended_training:this.attended_training};
            
            //post to the savestaff api
            axios.post("/api/Section/Staff/Addnew",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',

                staff_file_no: this.staff_file_no,
                staff_name:this.staff_name,
                gender:this.staff_gender,
                stafftype:this.staff_type,
                salary_source:this.salary_source,
                dob:this.staff_yob,
                year_of_first_appointment:this.staff_yfa,
                present_appointment_year:this.staff_ypa,
                year_of_posting:this.staff_yps,
                level:this.staff_level,
                present:this.present,
                academic_qualification:this.academic_qualification,
                teaching_qualification:this.teaching_qualification,
                specialisation:this.area_specialisation,
                mainsubject_taught:this.subject_taught,
                teaching_type:this.teaching_type,
                is_teaching_ss:this.is_teaching_ss,
                teacher_attended_training:this.attended_training,

            },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                    } else {
                        console.log(this.resultsSave.message);
                    }
            })

            //if successfully posted add to the table
            this.fetch_staffs.push(thestaff);


        },

        saveNoOfClassrooms: function () {
            axios.post("/api/Section/Classroom/NoOfClassroom",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
                

                no_of_classrooms: this.no_of_classrooms,
                classes_held_outside: this.classes_held_outside,
                
                

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                    } else {
                        console.log(this.resultsSave.message);
                    }
                })
            },
        
        saveNoOfOtherrooms: function () {
            axios.post("/api/Section/Room/Otherroom",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                no_of_staffrooms: this.fetch_otherRooms.find(function(room){return room.value=="Staff room"}).no_of_rooms,
                no_of_offices: this.fetch_otherRooms.find(function(room){return room.value=="Office"}).no_of_rooms,
                no_of_laboratories: this.fetch_otherRooms.find(function(room){return room.value=="Laboratories"}).no_of_rooms,
                no_of_storerooms: this.fetch_otherRooms.find(function(room){return room.value=="Store room"}).no_of_rooms,
                no_of_others: this.fetch_otherRooms.find(function(room){return room.value=="Others"}).no_of_rooms,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                    } else {
                        console.log(this.resultsSave.message);
                    }
                })
        },

        changedwsource: function(message,event){
            if(event.target.checked==false){
                axios.post("/api/Section/Remove/DrinkingSource",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        source:message
                        
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            // alert("Data Saved to DB");
                        } else {
                            console.log("Check the form for the required data");
                        }
                    });
            }
            
        },

        changesharedfacilities: function(message,event){
            if(event.target.checked==false){
                axios.post("/api/Section/Remove/SharedFacility",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        shared_facility:message
                        
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //alert("Data Saved to DB");
                        } else {
                            console.log("Check the form for the required data");
                        }
                    });
            }
            
        },

        changesourceofpower: function(message,event){
            if(event.target.checked==false){
                axios.post("/api/Section/Remove/PowerSource",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        power_source:message
                        
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //alert("Data Saved to DB");
                        } else {
                            console.log("Check the form for the required data");
                        }
                    });
            }
            
        },

        changelearningmaterials: function(message,event){
            if(event.target.checked==false){
                axios.post("/api/Section/Remove/LearningMaterial",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        material:message
                        
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            // alert("Data Saved to DB");
                        } else {
                            console.log("Check the form for the required data");
                        }
                    });
            }
            
        },

        changeplayfacilities: function(message,event){
            if(event.target.checked==false){
                axios.post("/api/Section/Remove/PlayFacility",
                    {
                        school_code: schoolcode.toLowerCase(),
                        year: year,
                        facility:message
                        
                    },this.config).then(response => {
                        this.resultsSave = response.data;
                        if (this.resultsSave.status == "success") {
                            //alert("Data Saved to DB");
                        } else {
                            console.log("Check the form for the required data");
                        }
                    });
            }
            
        },

        saveDWSources: function (event) {
            axios.post("/api/Section/Facility/DrinkingWater",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                drinking_water_sources: this.sources_of_drinking_water,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // console.log(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                })
        },


        saveFacilitiesAvailable: function (event) {
            axios.post("/api/Section/Facility/Available",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',

                facilities_available:this.fetch_facilities,
            
                
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // console.log(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                })
        },

        saveToilet: function (event) {
            //alert(this.fetch_toilet_type.find(function(facility){return facility.value=="Pit"}).male_students);
            axios.post("/api/Section/Facility/Toilet",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
                toilets:this.fetch_toilet_type,
                
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // console.log(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                })
        },

        saveSharedFacilities: function (event) {
            axios.post("/api/Section/Facility/SharedFacilities",
            {
                school_code: schoolcode.toLowerCase(),
                year: year, 
                school_class:'jss',
            
                shared_facilities: this.shared_facilities,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // console.log(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                })
        },

        //source of power
        savePowerSource: function (event) {
            axios.post("/api/Section/Facility/PowerSource",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                power_source: this.sources_of_power,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // console.log(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                })
        },

        //source of power
        saveStudentbySubject: function () {
            axios.post("/api/Section/Subject/Students",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                students_by_subject: this.fetch_subjects,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                    } else {
                        console.log(this.resultsSave.message);
                    }
                })
        },

        saveHealthFence: function (event) {
            axios.post("/api/Section/Facility/HealthFacility",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                health_facility: this.health_facility,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // console.log(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                });

        },

        saveSchoolBuilding: function (event) {
            axios.post("/api/Section/Facility/SchoolBuilding",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                school_building: this.facility_schoolbuilding,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // console.log(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                });
        },

        saveOwnership: function (event) {
            axios.post("/api/Section/Facility/Ownership",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'jss',
            
                ownership: this.facility_ownership,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // console.log(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                });

            

        },

        saveStudents:function(event){
            axios.post("/api/Section/Book/Student",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'private',
            
                students_book: this.fetch_books,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // console.log(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                });

            
        },

        saveTeachers:function(event){
            

            axios.post("/api/Section/Book/Teacher",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'private',
            
                teachers_book: this.fetch_books,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                })
        },

        saveSeaters:function(event){

            axios.post("/api/Section/Facility/Seater",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school_class:'primary',
            
                seaters: this.fetch_seaters,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // console.log(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                })
        },

        saveUndertaking:function(event){

            axios.post("/api/Section/Undertaking",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                
            
                attestation_headteacher_name: this.attestation_headteacher_name,
                attestation_headteacher_telephone: this.attestation_headteacher_telephone,
                attestation_headteacher_signdate: this.attestation_headteacher_signdate,
                attestation_enumerator_name: this.attestation_enumerator_name,
                attestation_enumerator_position: this.attestation_enumerator_position,
                attestation_enumerator_telephone: this.attestation_enumerator_telephone,
                attestation_supervisor_name: this.attestation_supervisor_name,
                attestation_supervisor_position: this.attestation_supervisor_position,
                attestation_supervisor_telephone: this.attestation_supervisor_telephone,
                

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        // alert(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                })
        },

        saveTeacherQualification:function(event){

            axios.post("/api/Section/Teacher/Qualification",
            {
                school_code: schoolcode.toLowerCase(),
                year: year,
                school:'private',
            
                teacher_qualification: this.fetch_teachingqualification,

                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        if (event.target.id === 'next') {                
                            this.popAlertMsg(this.resultsSave.status);
                            this.moveNext();
                            // console.log('next');
                        } else if (event.target.id === 'save') {
                            this.popAlertMsg(this.resultsSave.status);
                            // console.log('save');
                        }
                    } else {
                        console.log(this.resultsSave.message);
                        this.popAlertMsg(this.resultsSave.status);
                    }
                })
        },
    },

});