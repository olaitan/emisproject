  //vue vm for the form

  var app = new Vue({
    el: '#app',
    data: {
        fetch_roles: [],
        fetch_states: [],
        resultsSave: [],
        //school identification
        user_email: '',
        user_fullname: '',
        user_role:'',
        user_state:0,
        find_user_email:'',
        change_user_role: '',
        delete_user_email: '',
        auth_user:'',
        config:'',
    },

    mounted: function(){
        if(this.$cookies.isKey("user")){
            //get the access token
            this.user=this.$cookies.get("user");
            this.config = {
                headers: {
                  Accept: "application/json",
                  Authorization:"Bearer "+this.user.session,
                }
              }
            //setting the theme color
            var leftTab = document.querySelectorAll('.theme');
            for (let i = 0; i < leftTab.length; i++) {
                leftTab[i].style.setProperty('--themeColor', this.$cookies.get("theme"));
            } 
            axios.post("/api/Details",
            {
                email:this.user.email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.auth_user=response.data;
            }).catch(error=>{
                this.$cookies.remove("user");
                window.location.assign("/login");
            })

            axios.get("/api/roles").then(response => {

                console.log(JSON.stringify(response.data, null, 2));
    
                this.fetch_roles=response.data;
                
            })

            //get api for states
            axios.get("/api/states").then(response => {

                console.log(JSON.stringify(response.data, null, 2));
    
                this.fetch_states=response.data;
                
            })

        }else{
            window.location.assign("/login");
        }
        

    },

    methods:{
        //log out
        logout:function(){
            axios.post("/api/Section/User/Logout",
            {
                email:this.user_email,
            },this.config).then(response => {
                console.log(JSON.stringify(response.data, null, 2));
                this.$cookies.remove("user");
                window.location.assign("/login");
            })
        },
        addUser: function(){
            //add to the table
            //alert(" state: "+this.user_state);
            //post to the savestaff api
            axios.post("/api/Section/User/Addnew",
            {
                name:this.user_fullname,
                email: this.user_email,
                Id_Role:this.user_role,
                state_id:this.user_state,
                
            },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        configAlert.classList.add("active");
                        setTimeout(() => {
                            configAlert.classList.remove('active'); 
                        }, 2000);
                        document.getElementById("info").innerHTML = "User Created Successfully";
                        alert("success");
                        console.log(this.resultsSave.success.token);
                        console.log(this.resultsSave.success['email']);
                        console.log(this.resultsSave.state);
                        this.user_fullname='';
                        this.user_email='';
                        this.user_role='';
                        this.user_state='';
                    } else {
                        alert(this.resultsSave.message);
                    }
            })
        },

        removeUser:function(){

            axios.post("/api/Section/User/Remove",
            {
                
            
                email: this.delete_user_email,
    
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        //alert(this.resultsSave.message);
                        configAlert.classList.add("active");
                        setTimeout(() => {
                            configAlert.classList.remove('active'); 
                        }, 2000);
                        document.getElementById("info").innerHTML = "User deleted";
                    } else {
                        alert(this.resultsSave.message);
                    }
                })
        },

        changeUserRole:function(){

            axios.post("/api/Section/User/Change",
            {
                
                email: this.find_user_email,
                Id_Role:this.change_user_role,
    
                },this.config).then(response => {
                    this.resultsSave = response.data;
                    if (this.resultsSave.status == "success") {
                        configAlert.classList.add("active");
                        setTimeout(() => {
                            configAlert.classList.remove('active'); 
                        }, 2000);
                        document.getElementById("info").innerHTML = "User revoked";
                    } else {
                        alert(this.resultsSave.message);
                    }
                })
        },
    },

    

    
});