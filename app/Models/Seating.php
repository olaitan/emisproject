<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seating extends Model
{
    //The model associated with the tblvar_availablefacilities table
    protected $table='tblvar_seating';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','Id_SchoolClass','CensusYear','Id_SeatType','SeatingCapacity','DateLastModified','ClientAppID'];

     //One to many(inverse) relationship School
   public function school()
   {
       return $this->belongsTo('App\Models\School','Id_School','Id_School');
   }

   //One to many(inverse) relationship with SlaveReference
   public function schoolclass()
   {
       return $this->belongsTo('App\Models\SlaveReference','Id_SchoolClass','Id_SlaveReference');
   }

   //One to many(inverse) relationship with SlaveReference
   public function censusyear()
   {
       return $this->belongsTo('App\Models\CensusYear','CensusYear','Year');
   }

   //One to many(inverse) relationship with SlaveReference
   public function seattype()
   {
       return $this->belongsTo('App\Models\SlaveReference','Id_SeatType','Id_SlaveReference');
   }

}
