<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolCharacteristic extends Model
{
    //The model associated with the tblvar_schoolcharacteristics table
    protected $table='tblvar_schoolcharacteristics';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','Year','SharedFacilities','SchoolsSharingWith','MultiGradeTeaching','DistanceFromCatchmentArea','DistanceFromLGA','StudentsTravelling3km','IsBoarding','MaleStudentsBoardingAtSchoolPremises','FemaleStudentsBoardingAtSchoolPremises','HasSchoolDevelopmentPlan','HasSBMC','HasPTA','IsPSA','LastInspectionDate','LastInspectionAuthority','ConditionalCashTransfer','HasSchoolGrant','HasSecurityGuard','Ownership','OwnershipStatus','DateLastModified'];


     //One to many(inverse) relationship School
    public function school()
    {
    	return $this->belongsTo('App\Models\School','Id_School','Id_School');
    }

    //One to many(inverse) relationship with SlaveReference
    public function lastinspectionauthority()
    {
    	return $this->belongsTo('App\Models\SlaveReference','LastInspectionAuthority','Id_SlaveReference');
    }

    //One to many(inverse) relationship with SlaveReference
    public function ownership()
    {
    	return $this->belongsTo('App\Models\SlaveReference','Ownership','Id_SlaveReference');
    }

    //One to many(inverse) relationship with SlaveReference
    public function ownershipstatus()
    {
    	return $this->belongsTo('App\Models\SlaveReference','OwnershipStatus','Id_SlaveReference');
    }
}
