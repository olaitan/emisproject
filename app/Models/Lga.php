<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lga extends Model
{
    //The model associated with the tblmtd_lga table
    protected $table='tblmtd_lga';
    public $timestamps=false;
    protected $primaryKey='id_LGA';
    protected $fillable = ['Id_State','LGACode','LGA','isdeleted'];

    //One to many(inverse) relationship with State
    public function state()
    {
    	return $this->belongsTo('App\Models\State','Id_State','Id_State');
    }

    //One to many relationship with school table
    public function schools()
    {
    	return $this->hasMany('App\Models\School','Id_LGA','Id_LGA');
    }
}
