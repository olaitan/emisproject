<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompletedClass extends Model
{
    //The model associated with the tblvar_availablefacilities table
    protected $table='tblvar_completedclasses';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','Year','Id_SchoolClass','MaleEnrol','FemaleEnrol'];

   //One to many(inverse) relationship School
   public function school()
   {
       return $this->belongsTo('App\Models\School','Id_School','Id_School');
   }

   //One to many(inverse) relationship with SlaveReference
   public function censusyear()
   {
       return $this->belongsTo('App\Models\CensusYear','Year','Year');
   }

   //One to many(inverse) relationship with SlaveReference
   public function schoolclass()
   {
       return $this->belongsTo('App\Models\SlaveReference','Id_SchoolClass','Id_SlaveReference');
   }

   
}
