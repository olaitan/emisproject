<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Undertaking extends Model
{
    //The model associated with the tblvar_availablefacilities table
    protected $table='tblvar_undertaking';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','HeadTeacherName','CensusYear','HeadTeacherPhone','HeadTeacherSignDate','EnumeratorName','EnumeratorPosition','EnumeratorPhone','SupervisorName','SupervisorPosition','SupervisorPhone','FieldCoordinatorCheckDate','PreDataEntryCheck','DataEntryCompleted','VerificationCheck'];

    //One to many(inverse) relationship School
   public function school()
   {
       return $this->belongsTo('App\Models\School','Id_School','Id_School');
   }

   //One to many(inverse) relationship with SlaveReference
   public function censusyear()
   {
       return $this->belongsTo('App\Models\CensusYear','CensusYear','Year');
   }
}
