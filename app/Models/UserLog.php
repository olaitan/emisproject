<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
      //The model associated with the tblvar_availablefacilities table
      protected $table='tblmtd_userlog';
      public $timestamps=false;
      protected $primaryKey='id_userlog';
      protected $fillable = ['id_user','id_school','censusyear','category','user_activity','machine_name','machine_ip','notes','created_at'];
  
       //One to many(inverse) relationship School
    public function school()
    {
    	return $this->belongsTo('App\Models\School','id_school','Id_School');
    }

     //One to many(inverse) relationship School
     public function user()
     {
         return $this->belongsTo('App\User','id_user','id');
     }

     
}
