<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CensusYear extends Model
{
    //The model associated with the tblvar_caregivermanual table
    protected $table='tblsys_censusyear';
    public $timestamps=false;
    protected $primaryKey='Year';
    protected $fillable = ['Year','Session','OrderNo','Active'];

     //One to many relationship with enrolmentage table
     public function birthcertificates()
     {
         return $this->hasMany('App\Models\BirthCertificates','Year','CensusYear');
     }
     //One to many relationship with enrolmentage table
     public function caregivermanuals()
     {
         return $this->hasMany('App\Models\CaregiverManual','Year','CensusYear');
     }
    
    //One to many relationship with enrolmentage table
    public function enrolmentages()
    {
    	return $this->hasMany('App\Models\EnrolmentAge','Year','CensusYear');
    }
    //One to many relationship with enrolmentage table
    public function enrolmententrants()
    {
    	return $this->hasMany('App\Models\EnrolmentEntrant','Year','CensusYear');
    }
    //One to many relationship with enrolmentage table
    public function enrolmentorphans()
    {
    	return $this->hasMany('App\Models\EnrolmentOrphan','Year','CensusYear');
    }
    //One to many relationship with enrolmentage table
    public function enrolmentpupilflows()
    {
    	return $this->hasMany('App\Models\EnrolmentPupilFlow','Year','CensusYear');
    }
    //One to many relationship with enrolmentage table
    public function enrolmentspecialneeds()
    {
    	return $this->hasMany('App\Models\EnrolmentSpecialNeed','Year','CensusYear');
    }
    //One to many relationship with enrolmentage table
    public function streams()
    {
    	return $this->hasMany('App\Models\Stream','Year','CensusYear');
    }
}
