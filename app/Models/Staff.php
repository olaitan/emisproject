<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    //The model associated with the tblvar_availablefacilities table
    protected $table='tblbse_staff';
    public $timestamps=false;
    protected $primaryKey='Id_Staff';
    protected $fillable = ['Id_Staff','IsRemoved','Id_RemovalReason','Id_School','StaffFileNo','StaffName','Gender','DateOfBirth','YearOfFirstAppointment','Id_AcademicQualification','Id_TeachingQualification','Created','DateLastModified','ClientAppID'];

    //One to many(inverse) relationship School
    public function academicqualification()
    {
        return $this->belongsTo('App\Models\SlaveReference','Id_AcademicQualification','Id_SlaveReference');
    }

    //One to many(inverse) relationship with SlaveReference
    public function teachingqualification()
    {
       return $this->belongsTo('App\Models\SlaveReference','Id_TeachingQualification','Id_SlaveReference');
    }

    //One to many(inverse) relationship with SlaveReference
    public function removalreason()
    {
       return $this->belongsTo('App\Models\SlaveReference','Id_RemovalReason','Id_SlaveReference');
    }

    //One to many(inverse) relationship School
    public function school()
    {
       return $this->belongsTo('App\Models\School','Id_School','Id_School');
    }

}
