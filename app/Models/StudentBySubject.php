<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentBySubject extends Model
{
     //The model associated with the tblvar_availablefacilities table
     protected $table='tblvar_studentbysubject';
     public $timestamps=false;
     protected $primaryKey='Id_School';
     protected $fillable = ['Id_School','CensusYear','Id_Class','Id_Subject','Male','Female','DateLastModified','ClientAppID'];
 
     //One to many(inverse) relationship School
     public function class()
     {
         return $this->belongsTo('App\Models\SlaveReference','Id_Class','Id_SlaveReference');
     }
 
     //One to many(inverse) relationship School
     public function subject()
     {
         return $this->belongsTo('App\Models\SlaveReference','Id_Subject','Id_SlaveReference');
     }
 
     
    //One to many(inverse) relationship School
    public function school()
    {
        return $this->belongsTo('App\Models\School','Id_School','Id_School');
    }
 
    //One to many(inverse) relationship with SlaveReference
    public function censusyear()
    {
        return $this->belongsTo('App\Models\CensusYear','CensusYear','Year');
    }
}
