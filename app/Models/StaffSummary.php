<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaffSummary extends Model
{
    //The model associated with the tblvar_availablefacilities table
    protected $table='tblvar_staffsummary';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','CensusYear','NonTeachersMale','NonTeachersFemale','NonTeachersTotal','TeachersMale','TeachersFemale','TeachersTotal','LastYearTeachersMale','LastYearTeachersFemale','LastYearTeachersTotal','CareGiversMale','CareGiversFemale','TrainedCareGiversMale','TrainedCareGiversFemale','SupportStaffMale','SupportStaffFemale','FullTimeMale','FullTimeFemale','PartTimeMale','PartTimeFemale'];

    //One to many(inverse) relationship School
   public function school()
   {
       return $this->belongsTo('App\Models\School','Id_School','Id_School');
   }

   //One to many(inverse) relationship with SlaveReference
   public function censusyear()
   {
       return $this->belongsTo('App\Models\CensusYear','CensusYear','Year');
   }


}
