<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Repeater extends Model
{
    //The model associated with the tblvar_enrolmentage table
    protected $table='tblvar_repeaters';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','Id_SchoolClass','Year','Male','Female','DateLastModified','ClientAppID'];

    //One to many(inverse) relationship School
    public function school()
    {
    	return $this->belongsTo('App\Models\School','Id_School','Id_School');
    }

    //One to many(inverse) relationship with SlaveReference
    public function schoolclass()
    {
    	return $this->belongsTo('App\Models\SlaveReference','Id_SchoolClass','Id_SlaveReference');
    }

    //One to many(inverse) relationship with SlaveReference
    public function censusyear()
    {
    	return $this->belongsTo('App\Models\CensusYear','CensusYear','Year');
    }
}
