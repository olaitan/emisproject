<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    //The model associated with the tblbse_school table
    protected $table='tblbse_school';
    protected $primaryKey='Id_School';
    protected $fillable = ['SchoolCode','Id_School'];
    public $timestamps=false;

    //One to many relationship with birthcertificate table
    public function birthcertificates()
    {
    	return $this->hasMany('App\Models\BirthCertificate','Id_School','Id_School');
    }
    //One to many relationship with schoolcharacteristics table
    public function schoolcharacteristics()
    {
    	return $this->hasMany('App\Models\SchoolCharacteristic','Id_School','Id_School');
    }
    //One to many relationship with privateschoolcharacteristics table
    public function privateschoolcharacteristics()
    {
    	return $this->hasMany('App\Models\PrivateSchoolCharacteristics','Id_School','Id_School');
    }

    //One to many relationship with enrolmentage table
    public function enrolmentages()
    {
    	return $this->hasMany('App\Models\EnrolmentAge','Id_School','Id_School');
    }
    //One to many relationship with enrolmentage table
    public function enrolmententrants()
    {
    	return $this->hasMany('App\Models\EnrolmentEntrant','Id_School','Id_School');
    }
    //One to many relationship with enrolmentage table
    public function enrolmentorphans()
    {
    	return $this->hasMany('App\Models\EnrolmentOrphan','Id_School','Id_School');
    }
    //One to many relationship with enrolmentage table
    public function enrolmentpupilflows()
    {
    	return $this->hasMany('App\Models\EnrolmentPupilFlow','Id_School','Id_School');
    }
    //One to many relationship with enrolmentage table
    public function enrolmentspecialneeds()
    {
    	return $this->hasMany('App\Models\EnrolmentSpecialNeed','Id_School','Id_School');
    }
    //One to many relationship with enrolmentage table
    public function streams()
    {
    	return $this->hasMany('App\Models\Stream','Id_School','Id_School');
    }
    //One to many relationship with enrolmentage table
    public function repeaters()
    {
        return $this->hasMany('App\Models\Repeater','Id_School','Id_School');
    }
    //One to many relationship with caregivermanual table
    public function caregivermanuals()
    {
    	return $this->hasMany('App\Models\CaregiverManual','Id_School','Id_School');
    }
    //One to many relationship with caregivermanual table
    public function staffs()
    {
        return $this->hasMany('App\Models\Staff','Id_School','Id_School');
    }
     //One to many relationship with caregivermanual table
     public function availablefacilities()
     {
         return $this->hasMany('App\Models\AvailableFacility','Id_School','Id_School');
     }
      //One to many relationship with caregivermanual table
      public function powersources()
      {
          return $this->hasMany('App\Models\PowerSource','Id_School','Id_School');
      }

      //One to many relationship with caregivermanual table
      public function classroomsummaries()
      {
          return $this->hasMany('App\Models\ClassroomSummary','Id_School','Id_School');
      }

    //One to many(inverse) relationship with LGA
    public function lga()
    {
    	return $this->belongsTo('App\Models\Lga','Id_LGA','Id_LGA');
    }
    //One to many(inverse) relationship with SlaveReference
    public function state()
    {
    	return $this->belongsTo('App\Models\State','Id_State','Id_State');
    }
    //One to many(inverse) relationship with SlaveReference
    public function locationtype()
    {
    	return $this->belongsTo('App\Models\SlaveReference','Id_LocationType','Id_SlaveReference');
    }
    //One to many(inverse) relationship with SlaveReference
    public function schooltype()
    {
    	return $this->belongsTo('App\Models\SlaveReference','Id_SchoolType','Id_SlaveReference');
    }

     //One to many(inverse) relationship with SlaveReference
    public function levelofeducation()
    {
        return $this->belongsTo('App\Models\SlaveReference','LevelofEducation','Id_SlaveReference');
    }

    //One to many(inverse) relationship with SlaveReference
    public function sectorcode()
    {
    	return $this->belongsTo('App\Models\SlaveReference','Id_SectorCode','Id_SlaveReference');
    }

    //One to many(inverse) relationship with SlaveReference
    public function status()
    {
    	return $this->belongsTo('App\Models\SlaveReference','Status','Id_SlaveReference');
    }

      //One to many(inverse) relationship with SlaveReference
      public function recognitionstatus()
      {
          return $this->belongsTo('App\Models\SlaveReference','RecognitionStatus','Id_SlaveReference');
      }
}
