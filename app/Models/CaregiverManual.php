<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CaregiverManual extends Model
{
    //The model associated with the tblvar_caregivermanual table
    protected $table='tblvar_caregivermanual';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','Year','Id_ManualCategory','Available'];

     //One to many(inverse) relationship School
    public function school()
    {
    	return $this->belongsTo('App\Models\School','Id_School','Id_School');
    }

    //One to many(inverse) relationship with SlaveReference
    public function manualcategory()
    {
    	return $this->belongsTo('App\Models\SlaveReference','Id_ManualCategory','Id_SlaveReference');
    }

    //One to many(inverse) relationship with SlaveReference
    public function censusyear()
    {
    	return $this->belongsTo('App\Models\CensusYear','CensusYear','Year');
    }
}
