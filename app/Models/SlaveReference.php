<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SlaveReference extends Model
{
    //The model associated with the tblmtd_slavereference table
    protected $primaryKey='Id_SlaveReference';
    protected $table='tblmtd_slavereference';
    public $timestamps=false;
    protected $fillable = ['Id_MasterReference','Value','DisplayValue','Description','MetadataOrder','IsDeleted'];



    //One to many relationship with enrolmentage table
    public function schoolclasses()
    {
    	return $this->hasMany('App\Models\EnrolmentAge','Id_SchoolClass','Id_SlaveReference');
    }
    //One to many relationship with enrolmentage table
    public function agecategories()
    {
    	return $this->hasMany('App\Models\EnrolmentAge','Id_Agecategory','Id_SlaveReference');
    }
    //One to many relationship with caregivermanual table
    public function caregivermanuals()
    {
    	return $this->hasMany('App\Models\CaregiverManual','Id_ManualCategory','Id_SlaveReference');
    }
    //One to many relationship with school table
    public function towns()
    {
    	return $this->hasMany('App\Models\School','Id_Town','Id_SlaveReference');
    }
    //One to many relationship with school table
    public function locationtypes()
    {
    	return $this->hasMany('App\Models\School','Id_LocationType','Id_SlaveReference');
    }
    //One to many relationship with school table
    public function schooltypes()
    {
    	return $this->hasMany('App\Models\School','Id_SchoolType','Id_SlaveReference');
    }
    //One to many relationship with school table
    public function sectorcodes()
    {
    	return $this->hasMany('App\Models\School','Id_SectorCode','Id_SlaveReference');
    }
    //One to many relationship with school table
    public function lastinspectionauthoritys()
    {
    	return $this->hasMany('App\Models\SchoolCharacteristics','LastInspectionAuthority','Id_SlaveReference');
    }
    //One to many relationship with school table
    public function ownerships()
    {
    	return $this->hasMany('App\Models\SchoolCharacteristics','Ownership','Id_SlaveReference');
    }


    //One to many(inverse) relationship with MasterReference
    public function masterreference()
    {
    	return $this->belongsTo('App\Models\MasterReference','Id_MasterReference','Id_MasterReference');
    }

    //One to many(inverse) relationship with State
    public function state()
    {
    	return $this->belongsTo('App\Models\State','Id_State','Id_State');
    }
}
