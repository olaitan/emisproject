<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workshop extends Model
{
    //The model associated with the tblvar_availablefacilities table
    protected $table='tblvar_workshop';
    public $timestamps=false;
    protected $primaryKey='Id';
    protected $fillable = ['Id','Id_School','CensusYear','Id_WorkshopType','Shared','YearOfConstruction','Id_PresentCondition','Length','Width','Id_FloorMaterial','Id_WallMaterial','Id_RoofMaterial','Seating','GoodBlackboard'];

    //One to many(inverse) relationship School
   public function school()
   {
       return $this->belongsTo('App\Models\School','Id_School','Id_School');
   }

   //One to many(inverse) relationship with SlaveReference
   public function censusyear()
   {
       return $this->belongsTo('App\Models\CensusYear','CensusYear','Year');
   }

   //One to many(inverse) relationship School
   public function presentcondition()
   {
       return $this->belongsTo('App\Models\SlaveReference','Id_PresentCondition','Id_SlaveReference');
   }

    //One to many(inverse) relationship School
    public function workshoptype()
    {
        return $this->belongsTo('App\Models\SlaveReference','Id_WorkshopType','Id_SlaveReference');
    }

   //One to many(inverse) relationship with SlaveReference
  public function floormaterial()
  {
      return $this->belongsTo('App\Models\SlaveReference','Id_FloorMaterial','Id_SlaveReference');
  }

  //One to many(inverse) relationship School
  public function wallmaterial()
  {
      return $this->belongsTo('App\Models\SlaveReference','Id_WallMaterial','Id_SlaveReference');
  }

  //One to many(inverse) relationship with SlaveReference
 public function roofmaterial()
 {
     return $this->belongsTo('App\Models\SlaveReference','Id_RoofMaterial','Id_SlaveReference');
 }
}
