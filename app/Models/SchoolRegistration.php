<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolRegistration extends Model
{
    //The model associated with the tblvar_caregivermanual table
    protected $table='tblvar_schoolregistration';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','CensusYear','Registered'];

    //One to many(inverse) relationship School
    public function school()
    {
    	return $this->belongsTo('App\Models\School','Id_School','Id_School');
    }

  

}
