<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaffTransfer extends Model
{
    //The model associated with the tblvar_availablefacilities table
    protected $table='tblvar_stafftransfers';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['FromSchool','ToSchool','CensusYear','Id_Staff'];

    //One to many(inverse) relationship with SlaveReference
   public function staff()
   {
       return $this->belongsTo('App\Models\Staff','Id_Staff','Id_Staff');
   }

   //One to many(inverse) relationship School
   public function fromschool()
   {
       return $this->belongsTo('App\Models\School','FromSchool','Id_School');
   }

   //One to many(inverse) relationship School
   public function toschool()
   {
       return $this->belongsTo('App\Models\School','ToSchool','Id_School');
   }

   //One to many(inverse) relationship with SlaveReference
   public function censusyear()
   {
       return $this->belongsTo('App\Models\CensusYear','CensusYear','Year');
   }

}
