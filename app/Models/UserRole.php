<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    //The model associated with the tblvar_availablefacilities table
    protected $table='tblsys_userrole';
    public $timestamps=false;
    protected $primaryKey='Id';
    protected $fillable = ['Id','Role'];

}
