<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //The model associated with the tblmtd_country table
    protected $table='tblmtd_country';
    public $timestamps=false;
    protected $primaryKey='CountryCode';

    //One to many relationship with state table
    public function states()
    {
    	return $this->hasMany('App\Models\State','CountryCode','CountryCode');
    }
}
