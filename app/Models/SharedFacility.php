<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedFacility extends Model
{
    //The model associated with the tblvar_availablefacilities table
    protected $table='tblvar_sharedfacilities';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','Id_SharedFacility','CensusYear','DateLastModified','ClientAppID'];

    //One to many(inverse) relationship School
   public function school()
   {
       return $this->belongsTo('App\Models\School','Id_School','Id_School');
   }

   //One to many(inverse) relationship with SlaveReference
   public function censusyear()
   {
       return $this->belongsTo('App\Models\CensusYear','CensusYear','Year');
   }

   //One to many(inverse) relationship with SlaveReference
   public function sharedfacility()
   {
           return $this->belongsTo('App\Models\SlaveReference','Id_SharedFacility','Id_SlaveReference');
   }
}
