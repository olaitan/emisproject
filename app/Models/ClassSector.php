<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassSector extends Model
{
    //The model associated with the tblvar_caregivermanual table
    protected $table='tblsys_classsector';
    public $timestamps=false;
    protected $primaryKey='Id_SchoolClass';
    protected $fillable = ['Id_SchoolClass','Id_Sector'];
}
