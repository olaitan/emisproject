<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaffSchool extends Model
{
    //The model associated with the tblvar_availablefacilities table
    protected $table='tblvar_staffschool';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','Id_Staff','CensusYear','Id_StaffType','Id_SalarySource','PresentAppointmentYear','YearOfPosting','GradeLevelStep','Present','Id_MainSubjectTaught','Id_AreaOfSpecialisation','Id_TeachingType','TeacherAttendedTraining','DateLastModified','ClientAppID'];

    //One to many(inverse) relationship School
    public function stafftype()
    {
        return $this->belongsTo('App\Models\SlaveReference','Id_StaffType','Id_SlaveReference');
    }

    //One to many(inverse) relationship School
    public function present()
    {
        return $this->belongsTo('App\Models\SlaveReference','Present','Id_SlaveReference');
    }

    

    //One to many(inverse) relationship School
    public function salarysource()
    {
        return $this->belongsTo('App\Models\SlaveReference','Id_SalarySource','Id_SlaveReference');
    }

    //One to many(inverse) relationship School
    public function mainsubjecttaught()
    {
        return $this->belongsTo('App\Models\SlaveReference','Id_MainSubjectTaught','Id_SlaveReference');
    }

    //One to many(inverse) relationship School
    public function areaofspecialisation()
    {
        return $this->belongsTo('App\Models\SlaveReference','Id_AreaOfSpecialisation','Id_SlaveReference');
    }
    
    //One to many(inverse) relationship School
    public function teachingtype()
    {
        return $this->belongsTo('App\Models\SlaveReference','Id_TeachingType','Id_SlaveReference');
    }

    //One to many(inverse) relationship with SlaveReference
   public function staff()
   {
       return $this->belongsTo('App\Models\Staff','Id_Staff','Id_Staff');
   }

   //One to many(inverse) relationship School
   public function school()
   {
       return $this->belongsTo('App\Models\School','Id_School','Id_School');
   }

   //One to many(inverse) relationship with SlaveReference
   public function censusyear()
   {
       return $this->belongsTo('App\Models\CensusYear','CensusYear','Year');
   }

}
