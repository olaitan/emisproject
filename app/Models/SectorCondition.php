<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SectorCondition extends Model
{
    //The model associated with the tblvar_caregivermanual table
    protected $table='tblsys_sectorcondition';
    public $timestamps=false;
    protected $primaryKey='Id';
    protected $fillable = ['Id','SectorCondition'];
}
