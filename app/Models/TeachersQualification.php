<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeachersQualification extends Model
{
    //The model associated with the tblvar_availablefacilities table
    protected $table='tblvar_teachersqualification';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','Id_HighestQualification','CensusYear','Id_Level','Male','Female','DateLastModified','ClientAppID'];

     //One to many(inverse) relationship School
   public function school()
   {
       return $this->belongsTo('App\Models\School','Id_School','Id_School');
   }

   //One to many(inverse) relationship with SlaveReference
   public function censusyear()
   {
       return $this->belongsTo('App\Models\CensusYear','CensusYear','Year');
   }

   //One to many(inverse) relationship with SlaveReference
   public function highestqualification()
   {
       return $this->belongsTo('App\Models\SlaveReference','Id_HighestQualification','Id_SlaveReference');
   }

   //One to many(inverse) relationship with SlaveReference
   public function level()
   {
       return $this->belongsTo('App\Models\SlaveReference','Id_Level','Id_SlaveReference');
   }
}
