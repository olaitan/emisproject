<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterReference extends Model
{
    //The model associated with the tblmtd_masterreference table
    protected $table='tblmtd_masterreference';
    public $timestamps=false;
    protected $primaryKey='Id_MasterReference';


    //One to many relationship with lga table
    public function metadatas()
    {
        return $this->hasMany('App\Models\SlaveReference','Id_MasterReference','Id_MasterReference');
    }

    //One to many(inverse) relationship with SlaveReference
   public function sectorcondition()
   {
       return $this->belongsTo('App\Models\SectorCondition','Sector_Condition','Id');
   }
}
