<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassroomSummary extends Model
{
    //The model associated with the tblvar_availablefacilities table
    protected $table='tblvar_classroomsummary';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','Year','NoOfRooms','NoOfClassrooms','ClassesHeldOutside','NoOfPlayRooms'];


    //One to many(inverse) relationship School
   public function school()
   {
       return $this->belongsTo('App\Models\School','Id_School','Id_School');
   }

   //One to many(inverse) relationship with SlaveReference
   public function year()
   {
       return $this->belongsTo('App\Models\CensusYear','Year','Year');
   }

   
}
