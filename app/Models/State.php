<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    //The model associated with the tblmtd_state table
    protected $table='tblmtd_state';
    public $timestamps=false;
    protected $primaryKey='Id_State';
    protected $fillable = ['Id_State','State','StateCode','Theme','Capital','CountryCode'];
    //One to many relationship with lga table
    public function lgas()
    {
    	return $this->hasMany('App\Models\Lga','Id_State','Id_State');
    }
    //One to many relationship with school table
    public function schools()
    {
    	return $this->hasMany('App\Models\School','Id_State','Id_State');
    }
    //One to many relationship with SlaveReference table
    public function slavereferences()
    {
    	return $this->hasMany('App\Models\SlaveReference','Id_State','Id_State');
    }

    //One to many(inverse) relationship with Country
    public function country()
    {
    	return $this->belongsTo('App\Models\Country','CountryCode','CountryCode');
    }
}
