<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivateSchoolCharacteristic extends Model
{
    //The model associated with the tblvar_schoolcharacteristics table
    protected $table='tblbse_privateschoolcharacteristics';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','SchoolCode','SchoolLevel','YearEstablished','IsActive'];
    protected $guarded=[];
    //One to many(inverse) relationship with School
    public function school()
    {
    	return $this->belongsTo('App\Models\School','Id_School','Id_School');
    }

    //One to many(inverse) relationship with SlaveReference
    public function levelofeducation()
    {
        return $this->belongsTo('App\Models\SlaveReference','SchoolLevel','Id_SlaveReference');
    }
}
