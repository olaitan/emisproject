<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolBuilding extends Model
{
    //The model associated with the tblvar_healthfacility table
    protected $table='tblvar_schoolbuilding';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','Id_SchoolBuilding','CensusYear','DateLastModified','ClientAppID'];

    //One to many(inverse) relationship School
   public function school()
   {
       return $this->belongsTo('App\Models\School','Id_School','Id_School');
   }

   //One to many(inverse) relationship with SlaveReference
   public function censusyear()
   {
       return $this->belongsTo('App\Models\CensusYear','CensusYear','Year');
   }

   //One to many(inverse) relationship with SlaveReference
   public function schoolbuilding()
   {
           return $this->belongsTo('App\Models\SlaveReference','Id_SchoolBuilding','Id_SlaveReference');
   }
}
