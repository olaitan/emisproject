<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Toilet extends Model
{
    //The model associated with the tblvar_availablefacilities table
    protected $table='tblvar_toilets';
    public $timestamps=false;
    protected $primaryKey='Id_School';
    protected $fillable = ['Id_School','Id_UserType','CensusYear','Id_ToiletType','Male','Female','Mixed','DateLastModified','ClientAppID'];

   //One to many(inverse) relationship School
   public function school()
   {
       return $this->belongsTo('App\Models\School','Id_School','Id_School');
   }

   //One to many(inverse) relationship with SlaveReference
   public function censusyear()
   {
       return $this->belongsTo('App\Models\CensusYear','CensusYear','Year');
   }

   //One to many(inverse) relationship with SlaveReference
   public function usertype()
   {
       return $this->belongsTo('App\Models\SlaveReference','Id_UserType','Id_SlaveReference');
   }

   //One to many(inverse) relationship with SlaveReference
   public function toilettype()
   {
       return $this->belongsTo('App\Models\SlaveReference','Id_ToiletType','Id_SlaveReference');
   }
}
