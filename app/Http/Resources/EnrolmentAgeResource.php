<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class EnrolmentAgeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'class'=>$this->schoolclass->Value,
            'age_category'=>$this->agecategory->Value,
            'male'=>$this->MaleEnrol,
            'female'=>$this->FemaleEnrol,
        ];
    }
}
