<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReportKeyIndicatorPrimaryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'enrolment_primary'=>$this->schools->Value,
            //'enrolment_preprimary'=>$this->certificatecategory->Value,
            //'no_primary_schools'=>$this->MaleEnrol,
            //'no_teachers_primary_schools'=>$this->FemaleEnrol,
            //'no_useable_classrooms'=>$this->FemaleEnrol,
            //'pupils_classroom_ratio'=>$this->FemaleEnrol,
            //'percentage_with_nowater'=>$this->FemaleEnrol,
            //'public_toilet_ratio'=>$this->FemaleEnrol,
            //'percentage_with_no_healthfacilities'=>$this->FemaleEnrol,
            //'percentage_with_no_good_blackboard'=>$this->FemaleEnrol,
        ];
    }
}
