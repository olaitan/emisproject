<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class StaffResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->Id_Staff,
            'staff_file'=>$this->StaffFileNo,
            'staff_name'=>$this->StaffName,
            'gender'=>$this->Gender,
            'yofa'=>$this->YearOfFirstAppointment,
            'academic_qualification'=>$this->academicqualification->Value,
            'teaching_qualification'=>$this->teachingqualification->Value,
        ];
    }
}
