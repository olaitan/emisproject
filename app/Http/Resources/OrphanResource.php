<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class OrphanResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'class'=>$this->schoolclass->Value,
            'orphan_item'=>$this->orphanitem->Value,
            'male'=>$this->MaleEnrol,
            'female'=>$this->FemaleEnrol,
        ];
    }
}
