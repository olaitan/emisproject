<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SchoolTeachersReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'SchoolTeachersReport',
            'data'=>[
                'state'=>($this->state)?$this->state->State:"",
                'lga'=>($this->lga)?$this->lga->LGA:"",
                'name'=>$this->SchoolName,
                'schoolcode'=>$this->SchoolCode,
                'total_teachers_m'=>$this->staffs->where("Gender","M")->count(),
                'total_teachers_f'=>$this->staffs->where("Gender","F")->count()
            ]
        ];
    }
}
