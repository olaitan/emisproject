<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RepeatersReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'RepeatersReport',
            'data'=>[
                'state'=>($this->state)?$this->state->State:"",
                'lga'=>($this->lga)?$this->lga->LGA:"",
                'name'=>$this->SchoolName,
                'schoolcode'=>$this->SchoolCode,
                'kg1_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","881")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","881")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'kg1_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","881")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","881")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'kg2_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","882")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","882")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'kg2_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","882")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","882")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'ns1_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","883")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","883")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'ns1_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","883")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","883")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'ns2_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","884")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","884")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'ns2_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","884")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","884")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'ns3_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","885")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","885")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'ns3_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","885")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","885")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'pr1_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","886")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","886")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'pr1_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","886")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","886")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'pr2_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","887")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","887")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'pr2_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","887")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","887")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'pr3_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","888")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","888")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'pr3_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","888")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","888")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'pr4_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","889")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","889")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'pr4_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","889")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","889")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'pr5_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","890")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","890")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'pr5_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","890")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","890")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'pr6_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","891")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","891")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'pr6_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","891")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","891")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'js1_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","894")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","894")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'js1_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","894")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","894")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'js2_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","895")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","895")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'js2_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","895")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","895")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'js3_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","896")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","896")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'js3_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","896")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","896")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'ss1_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","897")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","897")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'ss1_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","897")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","897")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'ss2_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","898")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","898")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'ss2_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","898")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","898")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'ss3_m_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","899")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","899")->where("Year",$request->census_year)->first()->Male:"null"):"null",
                'ss3_f_rpt'=>($this->repeaters)?(($this->repeaters->where("Id_SchoolClass","899")->where("Year",$request->census_year)->first()!=null)?$this->repeaters->where("Id_SchoolClass","899")->where("Year",$request->census_year)->first()->Female:"null"):"null",
                'total_m_rpt'=>($this->repeaters)?(($this->repeaters->where('Year',$request->census_year)->first()!=null)?$this->repeaters->where('Year',$request->census_year)->sum('Male'):"null"):"null",
                'total_f_rpt'=>($this->repeaters)?(($this->repeaters->where('Year',$request->census_year)->first()!=null)?$this->repeaters->where('Year',$request->census_year)->sum('Female'):"null"):"null",
                'total_t_rpt'=>($this->repeaters)?(($this->repeaters->where('Year',$request->census_year)->first()!=null)?$this->repeaters->where('Year',$request->census_year)->sum('Male')+$this->repeaters->where('Year',$request->census_year)->sum('Female'):"null"):"null"
                
            ]
        ];
    }
}
