<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'state'=>$this->state->State,
            'email'=>$this->email,
            'profile_pic'=>$this->profile_pic,
            'role_id'=>$this->Id_Role,
            'role'=>$this->userrole->Role,
            'created_at'=>$this->created_at,
            ];
    }
}
