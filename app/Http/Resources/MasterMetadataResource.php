<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class MasterMetadataResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'metadata'=>$this->ReferenceType,
            'school'=>$this->sectorcondition->SectorCondition,
            'data'=>MetadataResource::collection($this->metadatas()->where('IsDeleted','<>','1')->get()),
        ];
    }
}
