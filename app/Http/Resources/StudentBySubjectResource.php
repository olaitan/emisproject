<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class StudentBySubjectResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'class'=>$this->class->Value,
            'subject'=>$this->subject->Value,
            'male'=>$this->Male,
            'female'=>$this->Female,
        ];
    }
}
