<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\PrivateSchoolCharacteristic;
use App\Models\State;

class ReportOverviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'OverviewReport',
            'data'=>[
                'total_schools'=>$this->count(),
                'public_schools'=>$this->where('Id_SectorCode','22')->count(),
                'private_schools'=>$this->where('Id_SectorCode','23')->count(),
                'public_preprimary_primary'=>$this->where('Id_SectorCode','22')->whereIn('LevelofEducation',[903,905,906])->count(),
                'public_js_ss'=>$this->where('Id_SectorCode','22')->whereIn('LevelofEducation',[904,907,908])->count(),
                //'public_science_vocational'=>$this->where('FormType','sciencevocational')->count(),
                'private_preprimary'=>PrivateSchoolCharacteristic::where('SchoolLevel',905)->count(),
                'private_primary'=>PrivateSchoolCharacteristic::where('SchoolLevel',906)->count(),
                'private_js'=>PrivateSchoolCharacteristic::where('SchoolLevel',907)->count(),
                'private_ss'=>PrivateSchoolCharacteristic::where('SchoolLevel',908)->count(),
                'states'=>StateReportOverviewResource::collection(State::all())
            ]
        ];
    }
}
