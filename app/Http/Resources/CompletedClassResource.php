<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CompletedClassResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'class'=>$this->schoolclass->Value,
            'male'=>$this->MaleEnrol,
            'female'=>$this->FemaleEnrol
            ];
    }
}
