<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'=>($this->user)?$this->user->name:'--',
            'school'=>($this->school)?$this->school->SchoolCode:'',
            'census'=>$this->censusyear,
            'category'=>$this->category,
            'activity'=>$this->user_activity,
            'notes'=>$this->notes,
            ];
    }
}
