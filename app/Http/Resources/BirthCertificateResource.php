<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class BirthCertificateResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'class'=>$this->schoolclass->Value,
            'value'=>$this->certificatecategory->Value,
            'male'=>$this->MaleEnrol,
            'female'=>$this->FemaleEnrol,
        ];
    }
}
