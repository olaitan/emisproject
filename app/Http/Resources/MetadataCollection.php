<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\StateResource;
use App\Http\Resources\MetadataResource;
use App\Http\Resources\MasterMetadataResource;
use App\Models\State;

class MetadataCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            //'data'=>MasterMetadataResource::collection($this->collection),
            'data'=>MetadataResource::collection($this->collection),
        ];
    }

    public function with($request)
    {
        return [
            'states' => StateResource::collection(State::all()),
        ];
    }
}
