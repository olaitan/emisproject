<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StateReportOverviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'StateOverviewReport',
            'data'=>[
                'name'=>$this->State,
                'public_schools'=>($this->schools)?$this->schools->where('Id_SectorCode','22')->count():null,
                'private_schools'=>($this->schools)?$this->schools->where('Id_SectorCode','23')->count():null,
            ]
        ];
    }
}
