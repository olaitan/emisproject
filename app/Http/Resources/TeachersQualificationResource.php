<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TeachersQualificationResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'qualification'=>$this->highestqualification->Value,
            'level'=>$this->level->Value,
            'male'=>$this->Male,
            'female'=>$this->Female,   
        ];
    }
}
