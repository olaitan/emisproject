<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\CensusYear;
class SchoolResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'School',
            'data'=>[
                'name'=>$this->SchoolName,
                'schoolcode'=>$this->SchoolCode,
                'town'=>$this->Town,
                'lga'=>($this->Id_LGA!=0)?$this->lga->LGA:"",
                'state'=>($this->Id_State!=0)?$this->state->State:"",
                'level'=>($this->LevelofEducation!=0)?$this->levelofeducation->Value:"",
                // 'sdp'=>($this->schoolcharacteristics()->count()!=0)?$this->schoolcharacteristics()->first()->HasSchoolDevelopmentPlan:"",
                // 'sbmc'=>($this->schoolcharacteristics()->count()!=0)?$this->schoolcharacteristics()->first()->HasSBMC:"",
                // 'pta'=>($this->schoolcharacteristics()->count()!=0)?$this->schoolcharacteristics()->first()->HasPTA:"",
                // 'grants'=>($this->schoolcharacteristics()->count()!=0)?$this->schoolcharacteristics()->first()->HasSchoolGrant:"",
                // 'guard'=>($this->schoolcharacteristics()->count()!=0)?$this->schoolcharacteristics()->first()->HasSecurityGuard:"",
                // 'ownership'=>($this->schoolcharacteristics()->count()!=0)?(($this->schoolcharacteristics()->first()->Ownership)?$this->schoolcharacteristics()->first()->ownership->Value:""):""
            ],
            'link'=>route('schoolform', ['schoolcode' => $this->SchoolCode,'year'=>CensusYear::where('Active','1')->first()]),//change later to school_form
        ];
    }
}
