<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SpecialNeedResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'class'=>$this->schoolclass->Value,
            'special_need_item'=>$this->specialneeditem->Value,
            'male'=>$this->MaleEnrol,
            'female'=>$this->FemaleEnrol,
        ];
    }
}
