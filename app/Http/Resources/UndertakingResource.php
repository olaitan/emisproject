<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UndertakingResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'attestation_headteacher_name'=>$this->HeadTeacherName,
            'attestation_headteacher_telephone'=>$this->HeadTeacherPhone,
            'attestation_headteacher_signdate'=>$this->HeadTeacherSignDate,
            'attestation_enumerator_name'=>$this->EnumeratorName,
            'attestation_enumerator_position'=>$this->EnumeratorPosition,
            'attestation_enumerator_telephone'=>$this->EnumeratorPhone,
            'attestation_supervisor_name'=>$this->SupervisorName,
            'attestation_supervisor_position'=>$this->SupervisorPosition,
            'attestation_supervisor_telephone'=>$this->SupervisorPhone,
        ];
    }
}
