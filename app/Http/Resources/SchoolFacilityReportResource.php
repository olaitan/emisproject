<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SchoolFacilityReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'SchoolFacilityReport',
            'data'=>[
                'state'=>($this->state)?$this->state->State:"",
                'lga'=>($this->lga)?$this->lga->LGA:"",
                'name'=>$this->SchoolName,
                'schoolcode'=>$this->SchoolCode,
                'usablerooms'=>($this->availablefacilities)?(($this->availablefacilities->where("CensusYear",$request->census_year)->first()!=null)?$this->availablefacilities->where("CensusYear",$request->census_year)->sum('Useable'):"null"):"null",
                'classesheldoutside'=>($this->classroomsummaries)?(($this->classroomsummaries->where("Year",$request->census_year)->first()!=null)?($this->classroomsummaries->where("Year",$request->census_year)->first()->ClassesHeldOutside==1)?'Yes':'No':"null"):"null",
                'powersource'=>($this->powersources)?(($this->powersources->where("CensusYear",$request->census_year)->first()!=null)?$this->powersources->where("CensusYear",$request->census_year)->first()->powersource->Value:"null"):"null"

            ]
        ];
    }
}
