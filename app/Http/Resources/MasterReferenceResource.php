<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class MasterReferenceResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'metadata category',
            'data'=>[
                'name'=>$this->ReferenceType,
                'description'=>$this->Description,
                'metadatas'=>SlaveReferenceResource::collection($this->metadatas()->where('IsDeleted','<>','1')->get()),
            ],
        ];
    }
}
