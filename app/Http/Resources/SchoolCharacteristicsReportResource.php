<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class SchoolCharacteristicsReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'SchoolCharacteristicReport',
            'data'=>[
                'state'=>($this->school && $this->school->state)?$this->school->state->State:"",
                'lga'=>($this->school && $this->school->lga)?$this->school->lga->LGA:"",
                'name'=>$this->school["SchoolName"],
                'schoolcode'=>$this->school["SchoolCode"],
                'location'=>($this->school && $this->school->locationtype)?$this->school->locationtype->Value:"null",
                'multigrade'=>($this->MultiGradeTeaching!=0)?'Yes':'No',
                'ownership'=>($this->Ownership!=0)?$this->ownership->Value:"null",
                'sharedfacilities'=>($this->SharedFacilities!=0)?'Yes':'No',
                'operationalmode'=>($this->school)?(($this->school->OperateShift)?'Yes':'No'):'null',
                'schooltype'=>($this->school && $this->school->schooltype)?$this->school->schooltype->Value:"null",
                'sector'=>($this->school && $this->school->sectorcode)?$this->school->sectorcode->Value:"null",
                'schoollevel'=>($this->school && $this->school->LevelofEducation!=0)?$this->school->levelofeducation->Value:"",
                'census_year'=>$request->census_year,
                'reported_in_census'=>'Yes'
            ]
        ];
    }
}
