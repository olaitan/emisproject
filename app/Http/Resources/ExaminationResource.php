<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ExaminationResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'exam_type'=>$this->examtype->Value,
            'registered_male'=>$this->RegisteredMale,
            'registered_female'=>$this->RegisteredFemale,
            'registered_total'=>$this->RegisteredTotal,
            'took_part_male'=>$this->TookPartMale,
            'took_part_female'=>$this->TookPartFemale,
            'took_part_total'=>$this->TookPartTotal,
            'passed_male'=>$this->PassedMale,
            'passed_female'=>$this->PassedFemale,
            'passed_total'=>$this->PassedTotal,
        ];
    }
}
