<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ToiletResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'censusyear'=>$this->CensusYear,
            'usertype'=>$this->usertype->Value,
            'toilet'=>$this->toilettype->Value,
            'male'=>$this->Male,
            'female'=>$this->Female,
            'mixed'=>$this->Mixed,
        ];
    }
}
