<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SchoolStreamsReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'StreamReport',
            'data'=>[
                'state'=>($this->state)?$this->state->State:"",
                'lga'=>($this->lga)?$this->lga->LGA:"",
                'name'=>$this->SchoolName,
                'schoolcode'=>$this->SchoolCode,
                'kg1_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","881")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","881")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'kg2_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","882")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","882")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'ns1_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","883")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","883")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'ns2_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","884")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","884")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'ns3_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","885")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","885")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'pr1_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","886")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","886")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'pr2_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","887")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","887")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'pr3_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","888")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","888")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'pr4_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","889")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","889")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'pr5_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","890")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","890")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'pr6_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","891")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","891")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'js1_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","894")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","894")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'js2_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","895")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","895")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'js3_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","896")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","896")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'ss1_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","897")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","897")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'ss2_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","898")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","898")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'ss3_streams'=>($this->streams)?(($this->streams->where("Id_SchoolClass","899")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","899")->where("Year",$request->census_year)->first()->Stream:"null"):"null",
                'total_t_streams'=>($this->streams)?(($this->streams->where('Year',$request->census_year)->first()!=null)?$this->streams->where('Year',$request->census_year)->sum('Stream'):"null"):"null",

                'kg1_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","881")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","881")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'kg2_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","882")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","882")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'ns1_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","883")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","883")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'ns2_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","884")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","884")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'ns3_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","885")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","885")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'pr1_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","886")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","886")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'pr2_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","887")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","887")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'pr3_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","888")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","888")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'pr4_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","889")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","889")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'pr5_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","890")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","890")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'pr6_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","891")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","891")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'js1_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","894")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","894")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'js2_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","895")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","895")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'js3_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","896")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","896")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'ss1_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","897")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","897")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'ss2_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","898")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","898")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'ss3_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Id_SchoolClass","899")->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Id_SchoolClass","899")->where("Year",$request->census_year)->first()->StreamWithMultiGrade:"null"):"null",
                'total_t_streamswithmultigrade'=>($this->streams)?(($this->streams->where("Year",$request->census_year)->first()!=null)?$this->streams->where("Year",$request->census_year)->sum('StreamWithMultiGrade'):"null"):"null"
                
            ]
        ];
    }
}
