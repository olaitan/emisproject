<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RepeaterResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'class'=>$this->schoolclass->Value,
            'male'=>$this->Male,
            'female'=>$this->Female,
        ];
    }
}
