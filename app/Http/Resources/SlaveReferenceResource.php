<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SlaveReferenceResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'metadata',
            'data'=>[
                'id'=>$this->Id_SlaveReference,
                'value'=>$this->Value,
                'display'=>$this->DisplayValue,
                'description'=>$this->Description,
                'order'=>$this->MetadataOrder,
                'level'=>$this->Level,
                'state'=>($this->Id_State==null || $this->Id_State==0)?0:$this->Id_State
            ],
        ];
    }
}
