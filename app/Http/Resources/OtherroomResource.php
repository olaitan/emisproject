<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class OtherroomResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'roomtype'=>$this->roomtype->Value,
            'number'=>$this->Number,
            ];
    }
}
