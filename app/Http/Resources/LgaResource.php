<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class LgaResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'lga',
            'data'=>[
                'id'=>$this->id_LGA,
                'lgacode'=>$this->LGACode,
                'name'=>$this->LGA,
            ],
        ];
    }
}
