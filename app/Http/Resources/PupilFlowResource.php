<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PupilFlowResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'class'=>$this->schoolclass->Value,
            'flow_item'=>($this->flowitem)?$this->flowitem->Value:0,
            'male'=>$this->MaleEnrol,
            'female'=>$this->FemaleEnrol,
        ];
    }
}
