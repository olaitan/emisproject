<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReportMainEntryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        //check if it is a state or all states

        //a state
        return[
            'key_indicators_public_primary'=>ReportKeyIndicatorPrimaryResource::collection($this->lgas),
            //'key_indicators_public_secondary'=>[],
            //'key_indicators_public_science_vocational'=>[],
            //'key_indicators_private'=>[]
        ];
    }
}
