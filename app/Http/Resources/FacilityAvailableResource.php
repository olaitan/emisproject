<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class FacilityAvailableResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'censusyear'=>$this->CensusYear,
            'facility'=>$this->facilitytype->Value,
            'useable'=>$this->Useable,
            'notuseable'=>$this->NotUseable
        ];
    }
}
