<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class WorkshopResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->Id,
            'workshop_type'=>$this->workshoptype->Value,
            'year_of_construction'=>$this->YearOfConstruction,
            'present_condition'=>$this->presentcondition->Value,
            'length_in_meters'=>$this->Length,
            'width_in_meters'=>$this->Width,
            'floor_material'=>$this->floormaterial->Value,
            'wall_material'=>$this->wallmaterial->Value,
            'roof_material'=>$this->roofmaterial->Value,
            'shared'=>$this->Shared,
            'seating'=>$this->Seating,
            'good_blackboard'=>$this->GoodBlackboard
            ];
    }
}
