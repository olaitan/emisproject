<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EnrolmentReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'EnrolmentReport',
            'data'=>[
                'state'=>($this->state)?$this->state->State:"",
                'lga'=>($this->lga)?$this->lga->LGA:"",
                'name'=>$this->SchoolName,
                'schoolcode'=>$this->SchoolCode,
                'kg1_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","881")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","881")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'kg1_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","881")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","881")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'kg2_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","882")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","882")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'kg2_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","882")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","882")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'ns1_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","883")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","883")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'ns1_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","883")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","883")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'ns2_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","884")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","884")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'ns2_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","884")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","884")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'ns3_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","885")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","885")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'ns3_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","885")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","885")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'pr1_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","886")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","886")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'pr1_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","886")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","886")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'pr2_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","887")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","887")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'pr2_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","887")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","887")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'pr3_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","888")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","888")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'pr3_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","888")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","888")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'pr4_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","889")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","889")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'pr4_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","889")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","889")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'pr5_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","890")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","890")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'pr5_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","890")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","890")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'pr6_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","891")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","891")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'pr6_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","891")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","891")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'js1_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","894")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","894")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'js1_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","894")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","894")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'js2_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","895")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","895")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'js2_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","895")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","895")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'js3_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","896")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","896")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'js3_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","896")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","896")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'ss1_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","897")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","897")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'ss1_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","897")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","897")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'ss2_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","898")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","898")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'ss2_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","898")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","898")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'ss3_m'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","899")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","899")->where("Year",$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'ss3_f'=>($this->enrolmentages)?(($this->enrolmentages->where("Id_SchoolClass","899")->where("Year",$request->census_year)->first()!=null)?$this->enrolmentages->where("Id_SchoolClass","899")->where("Year",$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'total_m'=>($this->enrolmentages)?(($this->enrolmentages->where('Year',$request->census_year)->first()!=null)?$this->enrolmentages->where('Year',$request->census_year)->sum('MaleEnrol'):"null"):"null",
                'total_f'=>($this->enrolmentages)?(($this->enrolmentages->where('Year',$request->census_year)->first()!=null)?$this->enrolmentages->where('Year',$request->census_year)->sum('FemaleEnrol'):"null"):"null",
                'total_t'=>($this->enrolmentages)?(($this->enrolmentages->where('Year',$request->census_year)->first()!=null)?$this->enrolmentages->where('Year',$request->census_year)->sum('MaleEnrol')+$this->enrolmentages->where('Year',$request->census_year)->sum('FemaleEnrol'):"null"):"null",

            ]
        ];
    }
}
