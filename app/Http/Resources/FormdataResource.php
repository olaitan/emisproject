<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\SchoolCharacteristic;
use App\Models\BirthCertificate;
use App\Models\EnrolmentAge;
use App\Models\EnrolmentEntrant;
use App\Models\SchoolRegistration;
use App\Models\EnrolmentOrphan;
use App\Models\Stream;
use App\Models\Workshop;
use App\Models\Repeater;
use App\Models\EnrolmentPupilFlow;
use App\Models\Examination;
use App\Models\CompletedClass;
use App\Models\EnrolmentSpecialNeed;
use App\Models\PrivateSchoolCharacteristic;
use App\Models\SlaveReference;
use App\Models\MasterReference;
use App\Models\CaregiverManual;
use App\Http\Resources\MetadataResource;
use App\Http\Resources\WorkshopResource;
use App\Http\Resources\CaregiverManualResource;
use App\Http\Resources\DrinkingWaterResource;
use App\Http\Resources\BirthCertificateResource;
use App\Http\Resources\EntrantResource;
use App\Http\Resources\UndertakingResource;
use App\Http\Resources\StreamResource;
use App\Http\Resources\RepeaterResource;
use App\Http\Resources\CompletedClassResource;
use App\Http\Resources\EnrolmentAgeResource;
use App\Http\Resources\PupilFlowResource;
use App\Http\Resources\SpecialNeedResource;
use App\Http\Resources\ExaminationResource;
use App\Http\Resources\StaffSchoolResource;
use App\Http\Resources\ClassroomResource;
use App\Http\Resources\OrphanResource;
use App\Http\Resources\StudentBookResource;
use App\Http\Resources\TeacherBookResource;
use App\Http\Resources\SubjectTaughtResource;
use App\Http\Resources\OtherroomResource;
use App\Http\Resources\PowerSourceResource;
use App\Http\Resources\ToiletResource;
use App\Http\Resources\TeachersQualificationResource;
use App\Http\Resources\SeaterResource;
use App\Http\Resources\SharedFacilityResource;
use App\Http\Resources\FacilityAvailableResource;
use App\Http\Resources\HealthFacilityResource;
use App\Http\Resources\FenceResource;
use App\Http\Resources\StudentBySubjectResource;
use App\Http\Resources\OwnershipResource;
use App\Http\Resources\BuildingResource;
use App\Http\Resources\MaterialResource;
use App\Http\Resources\PlayFacilityResource;
use App\Http\Resources\PlayRoomResource;
use App\Http\Resources\PrivateLevelOfEducation;
use App\Models\StaffSummary;
use App\Models\Ownership;
use App\Models\SchoolBuilding;
use App\Models\PlayRoom;
use App\Models\PlayFacility;
use App\Models\LearningMaterial;
use App\Models\ClassroomSummary;
use App\Models\OtherRoom;
use App\Models\Classroom;
use App\Models\Staff;
use App\Models\StaffSchool;
use App\Models\AvailableFacility;
use App\Models\HealthFacility;
use App\Models\FenceFacility;
use App\Models\StudentBySubject;
use App\Models\TeachersQualification;
use App\Models\StudentBook;
use App\Models\SubjectTaught;
use App\Models\TeacherBook;
use App\Models\Toilet;
use App\Models\Seating;
use App\Models\SharedFacility;
use App\Models\SafeDrinkingWater;
use App\Models\Undertaking;
use App\Models\PowerSource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class FormdataResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $tcoll=array();
        $tcoll[]=array('Id_School','=',$this->Id_School);
        $tcoll[]=array('Year','=',$request->year);
        $levelofeducation_coll='';
        $form_metadata='';
        $locationtype_metadata='';
        $schoollevel_metadata='';
        $schoolcategory_metadata='';
        $ownership_metadata='';
        $sc=new SchoolCharacteristic;
        $staffsummary_model=new StaffSummary;
        $classroomsummary_model=new ClassroomSummary;
        $safedrinkingwater_model=new SafeDrinkingWater;
        $healthfacility_model=new HealthFacility;
        $fencefacility_model=new FenceFacility;
        $undertaking_model=new Undertaking;
        $schoolregistration_model=new SchoolRegistration;

        try{
            $sc=SchoolCharacteristic::where($tcoll)->firstorfail();
        }catch(ModelNotFoundException $ex){
            $sc->LastInspectionAuthority=0;
            $sc->Ownership=0;
            $sc->OwnershipStatus=0;
        }
        try{
            $staffsummary_model=StaffSummary::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->firstorfail();
        }catch(ModelNotFoundException $ex){
           
        }
        try{
            $classroomsummary_model=ClassroomSummary::where(['Id_School'=>$this->Id_School,'Year'=>$request->year])->firstorfail();
           
            
        }catch(ModelNotFoundException $ex){
            
        }
        try{
            $safedrinkingwater_model=SafeDrinkingWater::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
        }catch(ModelNotFoundException $ex){
            
        }
        try{
            $healthfacility_model=HealthFacility::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->firstorfail();
            $fencefacility_model=FenceFacility::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->firstorfail();
            $undertaking_model=Undertaking::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->firstorfail();
        }catch(ModelNotFoundException $ex){
            
        }

        if($this->FormType=='private'){
                //get the collection of privateschoolcharacteristic level of education with school id and census year
                $prcoll=array();
                $prcoll[]=array('Id_School','=',$this->Id_School);
                //$prcoll[]=array('SchoolCode','=',$this->SchoolCode);
                $prcoll[]=array('IsActive','=','1');
                $levelofeducation_coll=PrivateSchoolCharacteristic::where($prcoll)->get();

                $birthcertificate_model=new BirthCertificate;
                $entrant_model=new EnrolmentEntrant;
                $age_model=new EnrolmentAge;
                $stream_model=new Stream;
                $repeater_model=new Repeater;
                $specialneed_model=new EnrolmentSpecialNeed;
                $pupilflow_model=new EnrolmentPupilFlow;
                $completedclasses_model=new CompletedClass;
                $orphan=new EnrolmentOrphan;
                $staffschool_model=new StaffSchool;
                $classroom_model=new Classroom;
                $otherroom_model=new OtherRoom;
                $availablefacilities_model=new AvailableFacility;
                $sharedfacilities_model=new SharedFacility;
                $toilet_model=new Toilet;
                $sourceofpower_model=new PowerSource;
                $completedclasses_model=new CompletedClass;
                $studentbysubject_model=new StudentBySubject;
                $studentbook_model=new StudentBook;
                $teacherbook_model=new TeacherBook;
                $subjecttaught_model=new SubjectTaught;
                $seater_model=new Seating;
                $teachersqualification_model=new TeachersQualification;
                $ownership_model=new Ownership;
                $building_model=new SchoolBuilding;
                


                try{
                    $birthcertificate_model=BirthCertificate::where($tcoll)->get();
                    $entrant_model=EnrolmentEntrant::where($tcoll)->get();
                    $age_model=EnrolmentAge::where($tcoll)->get();
                    $stream_model=Stream::where($tcoll)->get();
                    $repeater_model=Repeater::where($tcoll)->get();
                    $specialneed_model=EnrolmentSpecialNeed::where($tcoll)->get();
                    $pupilflow_model=EnrolmentPupilFlow::where($tcoll)->get();
                    $completedclasses_model=CompletedClass::where($tcoll)->get();
                    $orphan=EnrolmentOrphan::where($tcoll)->get();
                    $staffschool_model=StaffSchool::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $classroom_model=Classroom::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $otherroom_model=OtherRoom::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $availablefacilities_model=AvailableFacility::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $sharedfacilities_model=SharedFacility::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $toilet_model=Toilet::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $sourceofpower_model=PowerSource::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $completedclasses_model=CompletedClass::where($tcoll)->get();
                    $studentbysubject_model=StudentBySubject::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $studentbook_model=StudentBook::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $teacherbook_model=TeacherBook::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $subjecttaught_model=SubjectTaught::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $seater_model=Seating::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $teachersqualification_model=TeachersQualification::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $ownership_model=Ownership::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $building_model=SchoolBuilding::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $schoolregistration_model=SchoolRegistration::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year,'Registered'=>'1'])->first();

                }catch(ModelNotFoundException $ex){
                    //catch later
                }

                //$form_metadata=SlaveReference::whereIn('Id_SlaveReference',[33,34,906,907,908,905,19,276,554,21,685,686,687,671,672,673,674,675,676,542,543,689,690,691,692,693,880,894,895,896,897,898,899,881,882,883,884,885,886,887,888,889,890,891,892,893,759,760,761,762,495,496,497,498,715,716,12,280,909,13,559,910,14,8,9,17,11,15,560,16,24,135,561,562,911,100,101,102,912,694,695,696,495,496,497,498,715,716,918,919,920,921,648,649,650,651,652,667,668,669,670,666,665,664,766,767,768,769,770,69,68,70,511,512,513,514,52,913,516,518,520,515,519,517,720,721,722,723,922,88,89,90,91,92,120,229,706,707,708,709,710,711,712,713,778,172,173,174,175,176,339,487,488,489,779,490,491,492,484,485,486,780,781,782,783,830,831,832,833,834,835,836,837,838,839,840,841,842,843,844,845,846,664,665,666,670,669,668,667,861,862,863,864,865,866,867,868,869,870,871,860,859,849,850,851,852,853,854,855,856,857,858,872,879,877,873,874,875,876,878,640,641,642,644,643,645,646,647])->orderBy('Id_MasterReference')->get();
                //$form_metadata=MasterReference::whereIn('Sector_Condition',[1,2,3,4,5,6])->orderBy('Id_MasterReference')->get();
                $form_metadata=SlaveReference::whereIn('Level',[1,2,3,4,0])->where(['IsDeleted'=>'0'])->orderBy('Id_MasterReference')->get();
                return [
                    [
                    'type'=>'SchoolForm',
                    'data'=>[
                        'school_identification'=>[
                            'school_registration'=>($schoolregistration_model)?$schoolregistration_model->CensusYear:'',
                            'school_code'=>(string)$this->SchoolCode,
                            'primary_school_code'=>(string)$this->Primary_Schoolcode,
                            'jss_school_code'=>(string)$this->Jss_Schoolcode,
                            'sss_school_code'=>(string)$this->Sss_Schoolcode,
                            'xcoordinate'=>($this->XCoordinate)?(string)$this->XCoordinate:'',
                            'ycoordinate'=>($this->YCoordinate)?(string)$this->YCoordinate:'',
                            'zcoordinate'=>($this->ZCoordinate)?(string)$this->ZCoordinate:'',
                            'school_name'=>(string)$this->SchoolName,
                            'proprietor'=>(string)$this->Proprietor,
                            'address'=>(string)$this->SchoolAddress,
                            'town'=>(string)$this->Town,
                            'ward'=>(string)$this->Ward,
                            'lga'=>$this->lga->LGA,
                            'state'=>$this->lga->state->State,
                            'school_telephone'=>(string)$this->Telephone,
                            'email_address'=>(string)$this->Email,
                        ],
                        'school_characteristics'=>[
                            'preprimary'=>($levelofeducation_coll->where('Id_School',$this->Id_School)->where('SchoolLevel','905')->where('IsActive','1')->first())?($levelofeducation_coll->where('Id_School',$this->Id_School)->where('SchoolLevel','905')->first())->YearEstablished:"",
                            'primary'=>($levelofeducation_coll->where('Id_School',$this->Id_School)->where('SchoolLevel','906')->where('IsActive','1')->first())?($levelofeducation_coll->where('Id_School',$this->Id_School)->where('SchoolLevel','906')->first())->YearEstablished:"",
                            'jss'=>($levelofeducation_coll->where('Id_School',$this->Id_School)->where('SchoolLevel','907')->where('IsActive','1')->first())?($levelofeducation_coll->where('Id_School',$this->Id_School)->where('SchoolLevel','907')->first())->YearEstablished:"",
                            'sss'=>($levelofeducation_coll->where('Id_School',$this->Id_School)->where('SchoolLevel','908')->where('IsActive','1')->first())?($levelofeducation_coll->where('Id_School',$this->Id_School)->where('SchoolLevel','908')->first())->YearEstablished:"",
                            'location'=>($this->locationtype)?(string)$this->locationtype->Value:'',
                            'ownership_status'=>($sc->OwnershipStatus)?(string)$sc->ownershipstatus->Value:'',
                            'recognition_status'=>($this->RecognitionStatus==0)?'':(string)$this->recognitionstatus->Value,
                            'levels_of_education_offered'=>PrivateLevelOfEducation::collection($levelofeducation_coll),
                            'shifts'=>(string)$this->OperateShift,
                            'shared_facilities'=>(string)$sc->SharedFacilities,
                            'type_of_school'=>($this->schooltype)?(string)$this->schooltype->Value:'',
                            'is_psa'=>(string)$sc->IsPSA,
                            'school_average_distance_from_catchment_communities'=>(string)$sc->DistanceFromCatchmentArea,
                            'students_boarding'=>[
                                'male'=>(string)$sc->MaleStudentsBoardingAtSchoolPremises,
                                'female'=>(string)$sc->FemaleStudentsBoardingAtSchoolPremises,
                            ],
                            'school_development_plan_sdp'=>(string)$sc->HasSchoolDevelopmentPlan,
                            'school_based_management_committee_sbmc'=>(string)$sc->HasSBMC,
                            'parents_teachers_association_pta'=>(string)$sc->HasPTA,
                            'date_of_last_inspection_visit'=>(string)$sc->LastInspectionDate,
                            'no_of_inspection'=>(string)$sc->InspectionNo,
                            'authority_of_last_inspection'=>($sc->LastInspectionAuthority==0)?'':$sc->lastinspectionauthority->Value,
                            'security_guard'=>(string)$sc->HasSecurityGuard,
                            
                        ],
                        'enrollment'=>[
                            'birth_certificates'=>[
                                'data'=>BirthCertificateResource::collection($birthcertificate_model),
                            ],
                            'entrants'=>[
                                'data'=>EntrantResource::collection($entrant_model),
                            ],
                            'year_by_age'=>[
                                'data'=>[
                                    'stream'=>StreamResource::collection($stream_model),
                                    'age'=>EnrolmentAgeResource::collection($age_model),
                                    'repeater'=>RepeaterResource::collection($repeater_model),
                                    'prev_year'=>CompletedClassResource::collection($completedclasses_model),
                                ],
                            ],
                            'pupil_flow'=>[
                                'data'=>PupilFlowResource::collection($pupilflow_model),
                            ],
                            'special_need'=>[
                                'data'=>SpecialNeedResource::collection($specialneed_model),
                            ],
                            'orphans'=>[
                                'data'=>OrphanResource::collection($orphan),
                            ],
                            
                        ],
                        
                        'staff'=>[
                            'number_of_non_teaching_staffs'=>[
                                'male'=>(string)$staffsummary_model->NonTeachersMale,
                                'female'=>(string)$staffsummary_model->NonTeachersFemale,
                                'total'=>(string)$staffsummary_model->NonTeachersTotal,
                            ],
                            'number_of_teaching_staffs'=>[
                                'male'=>(string)$staffsummary_model->TeachersMale,
                                'female'=>(string)$staffsummary_model->TeachersFemale,
                                'total'=>(string)$staffsummary_model->TeachersTotal,
                            ],
                            //'information_on_all_staff'=>[
                                //'data'=>StaffSchoolResource::collection($staffschool_model),
                        // ],
                        ],
                        'classrooms'=>[
                            'number_of_classrooms'=>(string)$classroomsummary_model->NoOfClassrooms,
                            'are_classes_held_outside'=>(string)$classroomsummary_model->ClassesHeldOutside,
                            //'information_on_all_classrooms'=>[
                                //'data'=>ClassroomResource::collection($classroom_model),
                            //],
                            'rooms_other_than_classrooms'=>[
                                'data'=>OtherroomResource::collection($otherroom_model),
                            ],
                        ],
                        'facilities'=>[
                            'source_of_drinking_water'=>[
                                'data'=>DrinkingWaterResource::collection($safedrinkingwater_model),
                            ],
                            'facilities_available'=>[
                                'data'=>FacilityAvailableResource::collection($availablefacilities_model),
                            ],
                            'shared_facilities'=>[
                                'data'=>SharedFacilityResource::collection($sharedfacilities_model),
                            ],
                            'toilet'=>[
                                'data'=>ToiletResource::collection($toilet_model),
                            ],
                            'sources_of_power'=>[
                                'data'=>PowerSourceResource::collection($sourceofpower_model),
                            ],
                            'health_facility'=>[
                                'data'=>($healthfacility_model->Id_HealthFacility!=0)?$healthfacility_model->healthfacility->Value:'',
                            ],
                            'fence'=>[
                                'data'=>($fencefacility_model->Id_FenceFacility!=0)?$fencefacility_model->fencefacility->Value:'',
                            ],
                            'seater'=>[
                                'data'=>SeaterResource::collection($seater_model),
                            ],
                            'ownership'=>[
                                'data'=>OwnershipResource::collection($ownership_model),
                            ],
                            'schoolbuilding'=>[
                                'data'=>BuildingResource::collection($building_model),
                            ]
                        ],
                        'number_of_student_by_subject'=>[
                            'data'=>StudentBySubjectResource::collection($studentbysubject_model),
                        ],
                        'student_teacher_book'=>[
                            'subject_textbooks_available'=>[
                                'data'=>StudentBookResource::collection($studentbook_model),
                            ],
                            'subject_teachers_textbooks_available'=>[
                                'subjecttaught'=>SubjectTaughtResource::collection($subjecttaught_model),
                                'teacherbook'=>TeacherBookResource::collection($teacherbook_model),
                            ],
                        ],
                        'teacher_qualification'=>[
                            'data'=>TeachersQualificationResource::collection($teachersqualification_model)
                        ],
                        'undertaking'=>[
                            'attestation_headteacher_name'=>$undertaking_model->HeadTeacherName,
                            'attestation_headteacher_telephone'=>$undertaking_model->HeadTeacherPhone,
                            'attestation_headteacher_signdate'=>$undertaking_model->HeadTeacherSignDate,
                            'attestation_enumerator_name'=>$undertaking_model->EnumeratorName,
                            'attestation_enumerator_position'=>$undertaking_model->EnumeratorPosition,
                            'attestation_enumerator_telephone'=>$undertaking_model->EnumeratorPhone,
                            'attestation_supervisor_name'=>$undertaking_model->SupervisorName,
                            'attestation_supervisor_position'=>$undertaking_model->SupervisorPosition,
                            'attestation_supervisor_telephone'=>$undertaking_model->SupervisorPhone,
                        ]
                    ],
                ],[
                    'type'=>'Metadata',
                    'data'=>(new MetadataCollection($form_metadata)),
                    'attributes'=>(new MetadataCollection($form_metadata))->with($request),
                ]
            ];
        }else if($this->FormType=='sciencevocational'){
            $birthcertificate_model=new BirthCertificate;
                $entrant_model=new EnrolmentEntrant;
                $age_model=new EnrolmentAge;
                $stream_model=new Stream;
                $repeater_model=new Repeater;
                $specialneed_model=new EnrolmentSpecialNeed;
                $pupilflow_model=new EnrolmentPupilFlow;
                $completedclasses_model=new CompletedClass;
                $examination_model=new Examination;
                $orphan=new EnrolmentOrphan;
                $staffschool_model=new StaffSchool;
                $classroom_model=new Classroom;
                $workshop_model=new Workshop;
                $otherroom_model=new OtherRoom;
                $availablefacilities_model=new AvailableFacility;
                $sharedfacilities_model=new SharedFacility;
                $toilet_model=new Toilet;
                $sourceofpower_model=new PowerSource;
                $completedclasses_model=new CompletedClass;
                $studentbysubject_model=new StudentBySubject;
                $studentbook_model=new StudentBook;
                $teacherbook_model=new TeacherBook;
                $subjecttaught_model=new SubjectTaught;
                $seater_model=new Seating;
                $teachersqualification_model=new TeachersQualification;
                try{
                    $birthcertificate_model=BirthCertificate::where($tcoll)->get();
                    $entrant_model=EnrolmentEntrant::where($tcoll)->get();
                    $age_model=EnrolmentAge::where($tcoll)->get();
                    $stream_model=Stream::where($tcoll)->get();
                    $repeater_model=Repeater::where($tcoll)->get();
                    $specialneed_model=EnrolmentSpecialNeed::where($tcoll)->get();
                    $pupilflow_model=EnrolmentPupilFlow::where($tcoll)->get();
                    $completedclasses_model=CompletedClass::where($tcoll)->get();
                    $orphan=EnrolmentOrphan::where($tcoll)->get();
                    $examination_model=Examination::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $staffschool_model=StaffSchool::where(['Id_School'=>$this->Id_School])->whereHas('staff',function($q){
                        $q->where('IsRemoved','0');
                    })->groupBy('Id_Staff')->get();                    
                    $classroom_model=Classroom::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $workshop_model=Workshop::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $otherroom_model=OtherRoom::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $availablefacilities_model=AvailableFacility::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $sharedfacilities_model=SharedFacility::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $toilet_model=Toilet::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $sourceofpower_model=PowerSource::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $completedclasses_model=CompletedClass::where($tcoll)->get();
                    $studentbysubject_model=StudentBySubject::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $studentbook_model=StudentBook::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $teacherbook_model=TeacherBook::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $subjecttaught_model=SubjectTaught::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $seater_model=Seating::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $teachersqualification_model=TeachersQualification::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $schoolregistration_model=SchoolRegistration::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year,'Registered'=>'1'])->first();

                }catch(ModelNotFoundException $ex){
                    //catch later
                }
                //$form_metadata=SlaveReference::whereIn('Id_SlaveReference',[33,34,904,48,19,276,685,686,687,685,686,687,688,689,690,691,692,693,880,894,895,896,897,898,899,561,16,24,135,562,911,100,101,102,912,495,496,497,498,715,716,759,760,761,762,495,496,497,498,715,716,918,919,920,921,648,649,650,651,652,667,668,669,670,666,665,664,766,767,768,769,770,69,68,70,511,512,513,514,52,913,516,518,520,515,519,517,720,721,722,723,922,88,89,90,91,92,120,229,706,707,708,709,710,711,712,713,778,172,173,174,175,176,339,487,488,489,779,490,491,492,484,485,486,780,781,782,783,830,831,832,833,834,835,836,837,838,839,840,841,842,843,844,845,846,664,665,666,670,669,668,667,861,862,863,864,865,866,867,868,869,870,871,860,859,849,850,851,852,853,854,855,856,857,858,872,879,877,873,874,875,876,878,456,457,458,459,460,461,462,463,464,470,466,467,468,469,465,788,789,471,472,473,474,475,936,937,938,939,955,956,957,958,959,960,961,962,963,964,965,966,954,953,942,943,944,945,946,947,948,949,950,951,952,941])->orderBy('Id_MasterReference')->get();
                //$form_metadata=MasterReference::whereIn('Sector_Condition',[1,2,3,7])->orderBy('Id_MasterReference')->get();
                $form_metadata=SlaveReference::whereIn('Level',[3,4,6,0])->where(['IsDeleted'=>'0'])->orderBy('Id_MasterReference')->get();
                return [
                    [
                    'type'=>'SchoolForm',
                    'data'=>[
                        'school_identification'=>[
                            'school_registration'=>($schoolregistration_model)?$schoolregistration_model->CensusYear:'',
                            'school_code'=>(string)$this->SchoolCode,
                            'school_name'=>(string)$this->SchoolName,
                            'xcoordinate'=>($this->XCoordinate)?(string)$this->XCoordinate:'',
                            'ycoordinate'=>($this->YCoordinate)?(string)$this->YCoordinate:'',
                            'zcoordinate'=>($this->ZCoordinate)?(string)$this->ZCoordinate:'',
                            'address'=>(string)$this->SchoolAddress,
                            'town'=>(string)$this->Town,
                            'ward'=>(string)$this->Ward,
                            'lga'=>$this->lga->LGA,
                            'state'=>$this->lga->state->State,
                            'school_telephone'=>(string)$this->Telephone,
                            'email_address'=>(string)$this->Email,
                        ],
                        'school_characteristics'=>[
                            'year_of_establishment'=>(string)$this->YearFounded,
                            'location'=>($this->locationtype)?(string)$this->locationtype->Value:'',
                            'levels_of_education_offered'=>($this->levelofeducation)?(string)$this->levelofeducation->Value:'',
                            'type_of_school'=>($this->schooltype)?(string)$this->schooltype->Value:'',
                            'shifts'=>(string)$this->OperateShift,
                            'shared_facilities'=>(string)$sc->SharedFacilities,
                            'sharing_with'=>(string)$sc->SchoolsSharingWith,
                            'multi_grade_teaching'=>(string)$sc->MultiGradeTeaching,
                            'school_average_distance_from_catchment_communities'=>(string)$sc->DistanceFromCatchmentArea,
                            'students_distance_from_school'=>(string)$sc->StudentsTravelling3km,
                            'students_boarding'=>[
                                'male'=>(string)$sc->MaleStudentsBoardingAtSchoolPremises,
                                'female'=>(string)$sc->FemaleStudentsBoardingAtSchoolPremises,
                            ],
                            'school_development_plan_sdp'=>(string)$sc->HasSchoolDevelopmentPlan,
                            'school_based_management_committee_sbmc'=>(string)$sc->HasSBMC,
                            'parents_teachers_association_pta'=>(string)$sc->HasPTA,
                            'date_of_last_inspection_visit'=>(string)$sc->LastInspectionDate,
                            'authority_of_last_inspection'=>($sc->LastInspectionAuthority==0)?'':$sc->lastinspectionauthority->Value,
                            'no_of_inspection'=>(string)$sc->InspectionNo,
                            'conditional_cash_transfer'=>(string)$sc->ConditionalCashTransfer,
                            'school_grants'=>(string)$sc->HasSchoolGrant,
                            'security_guard'=>(string)$sc->HasSecurityGuard,
                            'ownership'=>($sc->Ownership==0)?'':(string)$sc->ownership->Value,
                        ],
                        'enrollment'=>[
                            'birth_certificates'=>[
                                'data'=>BirthCertificateResource::collection($birthcertificate_model),
                            ],
                            'entrants'=>[
                                'data'=>EntrantResource::collection($entrant_model),
                            ],
                            'year_by_age'=>[
                                'data'=>[
                                    'stream'=>StreamResource::collection($stream_model),
                                    'age'=>EnrolmentAgeResource::collection($age_model),
                                    'repeater'=>RepeaterResource::collection($repeater_model),
                                    'prev_year'=>CompletedClassResource::collection($completedclasses_model),
                                ],
                            ],
                            'pupil_flow'=>[
                                'data'=>PupilFlowResource::collection($pupilflow_model),
                            ],
                            'special_need'=>[
                                'data'=>SpecialNeedResource::collection($specialneed_model),
                            ],
                            'orphans'=>[
                                'data'=>OrphanResource::collection($orphan),
                            ],
                            'examination'=>[
                                'data'=>ExaminationResource::collection($examination_model),
                            ],
                        ],
                        
                        'staff'=>[
                            'number_of_non_teaching_staffs'=>[
                                'male'=>(string)$staffsummary_model->NonTeachersMale,
                                'female'=>(string)$staffsummary_model->NonTeachersFemale,
                                'total'=>(string)$staffsummary_model->NonTeachersTotal,
                            ],
                            'number_of_teaching_staffs'=>[
                                'male'=>(string)$staffsummary_model->TeachersMale,
                                'female'=>(string)$staffsummary_model->TeachersFemale,
                                'total'=>(string)$staffsummary_model->TeachersTotal,
                            ],
                            'information_on_all_staff'=>[
                                'data'=>StaffSchoolResource::collection($staffschool_model),
                            ],
                        ],
                        'classrooms'=>[
                            'number_of_classrooms'=>(string)$classroomsummary_model->NoOfClassrooms,
                            'are_classes_held_outside'=>(string)$classroomsummary_model->ClassesHeldOutside,
                            'information_on_all_classrooms'=>[
                                'data'=>ClassroomResource::collection($classroom_model),
                            ],
                            'rooms_other_than_classrooms'=>[
                                'data'=>OtherroomResource::collection($otherroom_model),
                            ],
                        ],
                        'workshops'=>[
                            'data'=>WorkshopResource::collection($workshop_model)
                        ],
                        'facilities'=>[
                            'source_of_drinking_water'=>[
                                'data'=>DrinkingWaterResource::collection($safedrinkingwater_model),
                            ],
                            'facilities_available'=>[
                                'data'=>FacilityAvailableResource::collection($availablefacilities_model),
                            ],
                            'shared_facilities'=>[
                                'data'=>SharedFacilityResource::collection($sharedfacilities_model),
                            ],
                            'toilet'=>[
                                'data'=>ToiletResource::collection($toilet_model),
                            ],
                            'sources_of_power'=>[
                                'data'=>PowerSourceResource::collection($sourceofpower_model),
                            ],
                            'health_facility'=>[
                                'data'=>($healthfacility_model->Id_HealthFacility!=0)?$healthfacility_model->healthfacility->Value:'',
                            ],
                            'fence'=>[
                                'data'=>($fencefacility_model->Id_FenceFacility!=0)?$fencefacility_model->fencefacility->Value:'',
                            ],
                            'seater'=>[
                                'data'=>SeaterResource::collection($seater_model),
                            ]
                        ],
                        'number_of_student_by_subject'=>[
                            'data'=>StudentBySubjectResource::collection($studentbysubject_model),
                        ],
                        'student_teacher_book'=>[
                            'subject_textbooks_available'=>[
                                'data'=>StudentBookResource::collection($studentbook_model),
                            ],
                            'subject_teachers_textbooks_available'=>[
                                'subjecttaught'=>SubjectTaughtResource::collection($subjecttaught_model),
                                'teacherbook'=>TeacherBookResource::collection($teacherbook_model),
                            ],
                        ],
                        'teacher_qualification'=>[
                            'data'=>TeachersQualificationResource::collection($teachersqualification_model)
                        ],
                        'undertaking'=>[
                            'attestation_headteacher_name'=>$undertaking_model->HeadTeacherName,
                            'attestation_headteacher_telephone'=>$undertaking_model->HeadTeacherPhone,
                            'attestation_headteacher_signdate'=>$undertaking_model->HeadTeacherSignDate,
                            'attestation_enumerator_name'=>$undertaking_model->EnumeratorName,
                            'attestation_enumerator_position'=>$undertaking_model->EnumeratorPosition,
                            'attestation_enumerator_telephone'=>$undertaking_model->EnumeratorPhone,
                            'attestation_supervisor_name'=>$undertaking_model->SupervisorName,
                            'attestation_supervisor_position'=>$undertaking_model->SupervisorPosition,
                            'attestation_supervisor_telephone'=>$undertaking_model->SupervisorPhone,
                        ]
                    ],
                ],[
                    'type'=>'Metadata',
                    'data'=>(new MetadataCollection($form_metadata)),
                    'attributes'=>(new MetadataCollection($form_metadata))->with($request),
                ]
            ];
        }else if( $this->levelofeducation->Value=="Pre-primary and primary" || $this->levelofeducation->Value=="Primary" || $this->levelofeducation->Value=="Pre-primary"){
            $birthcertificate_model=new BirthCertificate;
                $entrant_model=new EnrolmentEntrant;
                $age_model=new EnrolmentAge;
                $stream_model=new Stream;
                $repeater_model=new Repeater;
                $specialneed_model=new EnrolmentSpecialNeed;
                $pupilflow_model=new EnrolmentPupilFlow;
                $orphan=new EnrolmentOrphan;
                $completedclasses_model=new CompletedClass;
                $staffschool_model=new StaffSchool;
                $classroom_model=new Classroom;
                $otherroom_model=new OtherRoom;
                $availablefacilities_model=new AvailableFacility;
                $sharedfacilities_model=new SharedFacility;
                $toilet_model=new Toilet;
                $sourceofpower_model=new PowerSource;
                $completedclasses_model=new CompletedClass;
                $studentbysubject_model=new StudentBySubject;
                $studentbook_model=new StudentBook;
                $teacherbook_model=new TeacherBook;
                $subjecttaught_model=new SubjectTaught;
                $seater_model=new Seating;
                $playroom_model=new PlayRoom;
                $playfacility_model=new PlayFacility;
                $material_model=new LearningMaterial;
                $teachersqualification_model=new TeachersQualification;
                $caregivermanual_model=new CaregiverManual;
                
                try{
                    $birthcertificate_model=BirthCertificate::where($tcoll)->get();
                    $entrant_model=EnrolmentEntrant::where($tcoll)->get();
                    $age_model=EnrolmentAge::where($tcoll)->get();
                    $stream_model=Stream::where($tcoll)->get();
                    $repeater_model=Repeater::where($tcoll)->get();
                    $specialneed_model=EnrolmentSpecialNeed::where($tcoll)->get();
                    $pupilflow_model=EnrolmentPupilFlow::where($tcoll)->get();
                    $orphan=EnrolmentOrphan::where($tcoll)->get();
                    $completedclasses_model=CompletedClass::where($tcoll)->get();
                    $staffschool_model=StaffSchool::where(['Id_School'=>$this->Id_School])->whereHas('staff',function($q){
                        $q->where('IsRemoved','0');
                    })->groupBy('Id_Staff')->get();                    
                    $classroom_model=Classroom::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $otherroom_model=OtherRoom::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $availablefacilities_model=AvailableFacility::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $sharedfacilities_model=SharedFacility::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $toilet_model=Toilet::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $sourceofpower_model=PowerSource::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $completedclasses_model=CompletedClass::where($tcoll)->get();
                    $studentbysubject_model=StudentBySubject::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $studentbook_model=StudentBook::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $teacherbook_model=TeacherBook::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $subjecttaught_model=SubjectTaught::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $seater_model=Seating::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $playroom_model=PlayRoom::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $playfacility_model=PlayFacility::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $material_model=LearningMaterial::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $teachersqualification_model=TeachersQualification::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $caregivermanual_model=CaregiverManual::where(['Id_School'=>$this->Id_School,'Year'=>$request->year])->get();
                    $schoolregistration_model=SchoolRegistration::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year,'Registered'=>'1'])->first();

                }catch(ModelNotFoundException $ex){
                    //catch later
                }
                //$form_metadata=SlaveReference::whereIn('Id_SlaveReference',[914,915,916,33,34,46,47,903,19,21,276,685,686,687,685,686,687,688,689,690,691,692,693,880,881,882,883,884,885,886,887,888,889,890,891,892,893,759,760,761,762,495,496,497,498,715,716,14,8,9,17,11,15,560,926,927,928,929,930,931,648,649,650,651,652,667,668,669,670,666,665,664,766,767,768,769,770,69,68,70,511,512,513,514,52,913,516,518,520,515,519,517,720,721,722,723,922,88,89,90,91,92,120,229,706,707,708,709,710,711,712,713,778,172,173,174,175,176,339,487,488,489,779,490,491,492,484,485,486,780,781,782,783,724,725,726,727,728,729,730,731,732,733,734,735,736,737,738,456,457,458,459,460,461,462,463,464,470,466,467,468,469,465,788,789,471,472,473,474,475,717,714,718,936,937,938,939])->orderBy('Id_MasterReference')->get();
                //$form_metadata=MasterReference::whereIn('Sector_Condition',[1,4,5,8])->orderBy('Id_MasterReference')->get();
                $form_metadata=SlaveReference::whereIn('Level',[1,2,5,0])->where(['IsDeleted'=>'0'])->orderBy('Id_MasterReference')->get();
                return [
                    [
                    'type'=>'SchoolForm',
                    'data'=>[
                        'school_identification'=>[
                            'school_registration'=>($schoolregistration_model)?$schoolregistration_model->CensusYear:'',
                            'school_code'=>(string)$this->SchoolCode,
                            'school_name'=>(string)$this->SchoolName,
                            'xcoordinate'=>($this->XCoordinate)?(string)$this->XCoordinate:'',
                            'ycoordinate'=>($this->YCoordinate)?(string)$this->YCoordinate:'',
                            'zcoordinate'=>($this->ZCoordinate)?(string)$this->ZCoordinate:'',
                            'address'=>(string)$this->SchoolAddress,
                            'town'=>(string)$this->Town,
                            'ward'=>(string)$this->Ward,
                            'lga'=>$this->lga->LGA,
                            'state'=>$this->lga->state->State,
                            'school_telephone'=>(string)$this->Telephone,
                            'email_address'=>(string)$this->Email,
                        ],
                        'school_characteristics'=>[
                            'year_of_establishment'=>(string)$this->YearFounded,
                            'location'=>($this->locationtype)?(string)$this->locationtype->Value:'',
                            'levels_of_education_offered'=>($this->levelofeducation)?(string)$this->levelofeducation->Value:'',
                            'type_of_school'=>($this->schooltype)?(string)$this->schooltype->Value:'',
                            'shifts'=>(string)$this->OperateShift,
                            'shared_facilities'=>(string)$sc->SharedFacilities,
                            'sharing_with'=>(string)$sc->SchoolsSharingWith,
                            'multi_grade_teaching'=>(string)$sc->MultiGradeTeaching,
                            'school_average_distance_from_catchment_communities'=>(string)$sc->DistanceFromCatchmentArea,
                            'distance_from_lga'=>(string)$sc->DistanceFromLGA,
                            'students_distance_from_school'=>(string)$sc->StudentsTravelling3km,
                            'students_boarding'=>[
                                'male'=>(string)$sc->MaleStudentsBoardingAtSchoolPremises,
                                'female'=>(string)$sc->FemaleStudentsBoardingAtSchoolPremises,
                            ],
                            'school_development_plan_sdp'=>(string)$sc->HasSchoolDevelopmentPlan,
                            'school_based_management_committee_sbmc'=>(string)$sc->HasSBMC,
                            'parents_teachers_association_pta'=>(string)$sc->HasPTA,
                            'date_of_last_inspection_visit'=>(string)$sc->LastInspectionDate,
                            'no_of_inspection'=>(string)$sc->InspectionNo,
                            'authority_of_last_inspection'=>($sc->LastInspectionAuthority==0)?'':$sc->lastinspectionauthority->Value,
                            'conditional_cash_transfer'=>(string)$sc->ConditionalCashTransfer,
                            'school_grants'=>(string)$sc->HasSchoolGrant,
                            'security_guard'=>(string)$sc->HasSecurityGuard,
                            'ownership'=>($sc->Ownership==0)?'':(string)$sc->ownership->Value,
                        ],
                        'enrollment'=>[
                            'birth_certificates'=>[
                                'data'=>BirthCertificateResource::collection($birthcertificate_model),
                            ],
                            'entrants'=>[
                                'data'=>EntrantResource::collection($entrant_model),
                            ],
                            'year_by_age'=>[
                                'data'=>[
                                    'stream'=>StreamResource::collection($stream_model),
                                    'age'=>EnrolmentAgeResource::collection($age_model),
                                    'repeater'=>RepeaterResource::collection($repeater_model),
                                    'prev_year'=>CompletedClassResource::collection($completedclasses_model),
                                ],
                            ],
                            'pupil_flow'=>[
                                'data'=>PupilFlowResource::collection($pupilflow_model),
                            ],
                            'special_need'=>[
                                'data'=>SpecialNeedResource::collection($specialneed_model),
                            ],
                            'orphans'=>[
                                'data'=>OrphanResource::collection($orphan),
                            ],
                            
                        ],
                        'staff'=>[
                            'number_of_non_teaching_staffs'=>[
                                'male'=>(string)$staffsummary_model->NonTeachersMale,
                                'female'=>(string)$staffsummary_model->NonTeachersFemale,
                                'total'=>(string)$staffsummary_model->NonTeachersTotal,
                            ],
                            'number_of_teaching_staffs'=>[
                                'male'=>(string)$staffsummary_model->TeachersMale,
                                'female'=>(string)$staffsummary_model->TeachersFemale,
                                'total'=>(string)$staffsummary_model->TeachersTotal,
                            ],
                            'number_of_caregivers'=>[
                                'male'=>(string)$staffsummary_model->CareGiversMale,
                                'female'=>(string)$staffsummary_model->CareGiversFemale,
                                
                            ],
                            'information_on_all_staff'=>[
                                'data'=>StaffSchoolResource::collection($staffschool_model),
                            ],
                        ],
                        'classrooms'=>[
                            'number_of_classrooms'=>(string)$classroomsummary_model->NoOfClassrooms,
                            'are_classes_held_outside'=>(string)$classroomsummary_model->ClassesHeldOutside,
                            'eccd_playrooms'=>(string)$classroomsummary_model->NoOfPlayRooms,
                            'information_on_all_classrooms'=>[
                                'data'=>ClassroomResource::collection($classroom_model),
                            ],
                            'rooms_other_than_classrooms'=>[
                                'data'=>OtherroomResource::collection($otherroom_model),
                            ],
                        ],
                        'facilities'=>[
                            'source_of_drinking_water'=>[
                                'data'=>DrinkingWaterResource::collection($safedrinkingwater_model),
                            ],
                            'facilities_available'=>[
                                'data'=>FacilityAvailableResource::collection($availablefacilities_model),
                            ],
                            'shared_facilities'=>[
                                'data'=>SharedFacilityResource::collection($sharedfacilities_model),
                            ],
                            'toilet'=>[
                                'data'=>ToiletResource::collection($toilet_model),
                            ],
                            'sources_of_power'=>[
                                'data'=>PowerSourceResource::collection($sourceofpower_model),
                            ],
                            'health_facility'=>[
                                'data'=>($healthfacility_model->Id_HealthFacility!=0)?$healthfacility_model->healthfacility->Value:'',
                            ],
                            'fence'=>[
                                'data'=>($fencefacility_model->Id_FenceFacility!=0)?$fencefacility_model->fencefacility->Value:'',
                            ],
                            'seater'=>[
                                'data'=>SeaterResource::collection($seater_model),
                            ],
                            'playroom'=>[
                                'data'=>PlayRoomResource::collection($playroom_model),
                            ],
                            'playfacilities'=>[
                                'data'=>PlayFacilityResource::collection($playfacility_model),
                            ],
                            'materials'=>[
                                'data'=>MaterialResource::collection($material_model),
                            ]
                        ],
                        'number_of_student_by_subject'=>[
                            'data'=>StudentBySubjectResource::collection($studentbysubject_model),
                        ],
                        'student_teacher_book'=>[
                            'subject_textbooks_available'=>[
                                'data'=>StudentBookResource::collection($studentbook_model),
                            ],
                            'subject_teachers_textbooks_available'=>[
                                'subjecttaught'=>SubjectTaughtResource::collection($subjecttaught_model),
                                'teacherbook'=>TeacherBookResource::collection($teacherbook_model),
                            ],
                        ],
                        'caregiver_manual'=>[
                            'data'=>CaregiverManualResource::collection($caregivermanual_model)
                        ],
                        'teacher_qualification'=>[
                            'data'=>TeachersQualificationResource::collection($teachersqualification_model)
                        ],
                        'undertaking'=>[
                            'attestation_headteacher_name'=>$undertaking_model->HeadTeacherName,
                            'attestation_headteacher_telephone'=>$undertaking_model->HeadTeacherPhone,
                            'attestation_headteacher_signdate'=>$undertaking_model->HeadTeacherSignDate,
                            'attestation_enumerator_name'=>$undertaking_model->EnumeratorName,
                            'attestation_enumerator_position'=>$undertaking_model->EnumeratorPosition,
                            'attestation_enumerator_telephone'=>$undertaking_model->EnumeratorPhone,
                            'attestation_supervisor_name'=>$undertaking_model->SupervisorName,
                            'attestation_supervisor_position'=>$undertaking_model->SupervisorPosition,
                            'attestation_supervisor_telephone'=>$undertaking_model->SupervisorPhone,
                        ]
                    ],
                ],[
                    'type'=>'Metadata',
                    'data'=>(new MetadataCollection($form_metadata)),
                    'attributes'=>(new MetadataCollection($form_metadata))->with($request),
                ]
            ];
        }else if(($this->FormType=='public' && $this->levelofeducation->Value=="Junior and Senior Secondary")||($this->FormType=='public' && $this->levelofeducation->Value=="Junior Secondary") || ($this->FormType=='public' && $this->levelofeducation->Value=="Senior Secondary")){
            $birthcertificate_model=new BirthCertificate;
                $entrant_model=new EnrolmentEntrant;
                $age_model=new EnrolmentAge;
                $stream_model=new Stream;
                $repeater_model=new Repeater;
                $specialneed_model=new EnrolmentSpecialNeed;
                $pupilflow_model=new EnrolmentPupilFlow;
                $completedclasses_model=new CompletedClass;
                $examination_model=new Examination;
                $orphan=new EnrolmentOrphan;
                $staffschool_model=new StaffSchool;
                $classroom_model=new Classroom;
                $otherroom_model=new OtherRoom;
                $availablefacilities_model=new AvailableFacility;
                $sharedfacilities_model=new SharedFacility;
                $toilet_model=new Toilet;
                $sourceofpower_model=new PowerSource;
                $completedclasses_model=new CompletedClass;
                $studentbysubject_model=new StudentBySubject;
                $studentbook_model=new StudentBook;
                $teacherbook_model=new TeacherBook;
                $subjecttaught_model=new SubjectTaught;
                $seater_model=new Seating;
                $teachersqualification_model=new TeachersQualification;
                try{
                    $birthcertificate_model=BirthCertificate::where($tcoll)->get();
                    $entrant_model=EnrolmentEntrant::where($tcoll)->get();
                    $age_model=EnrolmentAge::where($tcoll)->get();
                    $stream_model=Stream::where($tcoll)->get();
                    $repeater_model=Repeater::where($tcoll)->get();
                    $specialneed_model=EnrolmentSpecialNeed::where($tcoll)->get();
                    $pupilflow_model=EnrolmentPupilFlow::where($tcoll)->get();
                    $completedclasses_model=CompletedClass::where($tcoll)->get();
                    $orphan=EnrolmentOrphan::where($tcoll)->get();
                    $examination_model=Examination::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $staffschool_model=StaffSchool::where(['Id_School'=>$this->Id_School])->whereHas('staff',function($q){
                        $q->where('IsRemoved','0');
                    })->groupBy('Id_Staff')->get();
                    
                    $classroom_model=Classroom::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $otherroom_model=OtherRoom::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $availablefacilities_model=AvailableFacility::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $sharedfacilities_model=SharedFacility::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $toilet_model=Toilet::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $sourceofpower_model=PowerSource::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $completedclasses_model=CompletedClass::where($tcoll)->get();
                    $studentbysubject_model=StudentBySubject::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $studentbook_model=StudentBook::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $teacherbook_model=TeacherBook::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $subjecttaught_model=SubjectTaught::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $seater_model=Seating::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $teachersqualification_model=TeachersQualification::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year])->get();
                    $schoolregistration_model=SchoolRegistration::where(['Id_School'=>$this->Id_School,'CensusYear'=>$request->year,'Registered'=>'1'])->first();

                }catch(ModelNotFoundException $ex){
                    //catch later
                }
                //$form_metadata=SlaveReference::whereIn('Id_SlaveReference',[33,34,904,48,19,276,685,686,687,685,686,687,688,689,690,691,692,693,880,894,895,896,897,898,899,561,16,24,135,562,911,100,101,102,912,495,496,497,498,715,716,759,760,761,762,918,919,920,921,648,649,650,651,652,667,668,669,670,666,665,664,766,767,768,769,770,69,68,70,511,512,513,514,52,913,516,518,520,515,519,517,720,721,722,723,922,88,89,90,91,92,120,229,706,707,708,709,710,711,712,713,778,172,173,174,175,176,339,487,488,489,779,490,491,492,484,485,486,780,781,782,783,830,831,832,833,834,835,836,837,838,839,840,841,842,843,844,845,846,664,665,666,670,669,668,667,861,862,863,864,865,866,867,868,869,870,871,860,859,849,850,851,852,853,854,855,856,857,858,872,879,877,873,874,875,876,878,456,457,458,459,460,461,462,463,464,470,466,467,468,469,465,788,789,471,472,473,474,475,936,937,938,939])->orderBy('Id_MasterReference')->get();
                //$form_metadata=MasterReference::whereIn('Sector_Condition',[1,2,3,8])->orderBy('Id_MasterReference')->get();
                $form_metadata=SlaveReference::whereIn('Level',[3,4,6,0])->where(['IsDeleted'=>'0'])->orderBy('Id_MasterReference')->get();
                //$form_metadata=$form_m->where(['IsDeleted'=>'0']);
                return [
                    [
                    'type'=>'SchoolForm',
                    'data'=>[
                        'school_identification'=>[
                            'school_registration'=>($schoolregistration_model)?$schoolregistration_model->CensusYear:'',
                            'school_code'=>(string)$this->SchoolCode,
                            'school_name'=>(string)$this->SchoolName,
                            'xcoordinate'=>($this->XCoordinate)?(string)$this->XCoordinate:'',
                            'ycoordinate'=>($this->YCoordinate)?(string)$this->YCoordinate:'',
                            'zcoordinate'=>($this->ZCoordinate)?(string)$this->ZCoordinate:'',
                            'address'=>(string)$this->SchoolAddress,
                            'town'=>(string)$this->Town,
                            'ward'=>(string)$this->Ward,
                            'lga'=>($this->Id_LGA!=0)?$this->lga->LGA:"",
                            'state'=>($this->Id_State!=0)?$this->state->State:"",
                            'school_telephone'=>(string)$this->Telephone,
                            'email_address'=>(string)$this->Email,
                        ],
                        'school_characteristics'=>[
                            'year_of_establishment'=>(string)$this->YearFounded,
                            'location'=>($this->locationtype)?(string)$this->locationtype->Value:'',
                            'levels_of_education_offered'=>($this->levelofeducation)?(string)$this->levelofeducation->Value:'',
                            'type_of_school'=>($this->schooltype)?(string)$this->schooltype->Value:'',
                            'shifts'=>(string)$this->OperateShift,
                            'shared_facilities'=>(string)$sc->SharedFacilities,
                            'sharing_with'=>(string)$sc->SchoolsSharingWith,
                            'multi_grade_teaching'=>(string)$sc->MultiGradeTeaching,
                            'school_average_distance_from_catchment_communities'=>(string)$sc->DistanceFromCatchmentArea,
                            'students_distance_from_school'=>(string)$sc->StudentsTravelling3km,
                            'students_boarding'=>[
                                'male'=>(string)$sc->MaleStudentsBoardingAtSchoolPremises,
                                'female'=>(string)$sc->FemaleStudentsBoardingAtSchoolPremises,
                            ],
                            'school_development_plan_sdp'=>(string)$sc->HasSchoolDevelopmentPlan,
                            'school_based_management_committee_sbmc'=>(string)$sc->HasSBMC,
                            'parents_teachers_association_pta'=>(string)$sc->HasPTA,
                            'date_of_last_inspection_visit'=>(string)$sc->LastInspectionDate,
                            'authority_of_last_inspection'=>($sc->LastInspectionAuthority==0)?'':$sc->lastinspectionauthority->Value,
                            'no_of_inspection'=>(string)$sc->InspectionNo,
                            'conditional_cash_transfer'=>(string)$sc->ConditionalCashTransfer,
                            'school_grants'=>(string)$sc->HasSchoolGrant,
                            'security_guard'=>(string)$sc->HasSecurityGuard,
                            'ownership'=>($sc->Ownership==0)?'':(string)$sc->ownership->Value,
                        ],
                        'enrollment'=>[
                            'birth_certificates'=>[
                                'data'=>BirthCertificateResource::collection($birthcertificate_model),
                            ],
                            'entrants'=>[
                                'data'=>EntrantResource::collection($entrant_model),
                            ],
                            'year_by_age'=>[
                                'data'=>[
                                    'stream'=>StreamResource::collection($stream_model),
                                    'age'=>EnrolmentAgeResource::collection($age_model),
                                    'repeater'=>RepeaterResource::collection($repeater_model),
                                    'prev_year'=>CompletedClassResource::collection($completedclasses_model),
                                ],
                            ],
                            'pupil_flow'=>[
                                'data'=>PupilFlowResource::collection($pupilflow_model),
                            ],
                            'special_need'=>[
                                'data'=>SpecialNeedResource::collection($specialneed_model),
                            ],
                            'orphans'=>[
                                'data'=>OrphanResource::collection($orphan),
                            ],
                            'examination'=>[
                                'data'=>ExaminationResource::collection($examination_model),
                            ],
                        ],
                        'staff'=>[
                            'number_of_non_teaching_staffs'=>[
                                'male'=>(string)$staffsummary_model->NonTeachersMale,
                                'female'=>(string)$staffsummary_model->NonTeachersFemale,
                                'total'=>(string)$staffsummary_model->NonTeachersTotal,
                            ],
                            'number_of_teaching_staffs'=>[
                                'male'=>(string)$staffsummary_model->TeachersMale,
                                'female'=>(string)$staffsummary_model->TeachersFemale,
                                'total'=>(string)$staffsummary_model->TeachersTotal,
                            ],
                            'information_on_all_staff'=>[
                                'data'=>StaffSchoolResource::collection($staffschool_model),
                            ],
                        ],
                        'classrooms'=>[
                            'number_of_classrooms'=>(string)$classroomsummary_model->NoOfClassrooms,
                            'are_classes_held_outside'=>(string)$classroomsummary_model->ClassesHeldOutside,
                            'information_on_all_classrooms'=>[
                                'data'=>ClassroomResource::collection($classroom_model),
                            ],
                            'rooms_other_than_classrooms'=>[
                                'data'=>OtherroomResource::collection($otherroom_model),
                            ],
                        ],
                        'facilities'=>[
                            'source_of_drinking_water'=>[
                                'data'=>DrinkingWaterResource::collection($safedrinkingwater_model),
                            ],
                            'facilities_available'=>[
                                'data'=>FacilityAvailableResource::collection($availablefacilities_model),
                            ],
                            'shared_facilities'=>[
                                'data'=>SharedFacilityResource::collection($sharedfacilities_model),
                            ],
                            'toilet'=>[
                                'data'=>ToiletResource::collection($toilet_model),
                            ],
                            'sources_of_power'=>[
                                'data'=>PowerSourceResource::collection($sourceofpower_model),
                            ],
                            'health_facility'=>[
                                'data'=>($healthfacility_model->Id_HealthFacility!=0)?$healthfacility_model->healthfacility->Value:'',
                            ],
                            'fence'=>[
                                'data'=>($fencefacility_model->Id_FenceFacility!=0)?$fencefacility_model->fencefacility->Value:'',
                            ],
                            'seater'=>[
                                'data'=>SeaterResource::collection($seater_model),
                            ]
                        ],
                        'number_of_student_by_subject'=>[
                            'data'=>StudentBySubjectResource::collection($studentbysubject_model),
                        ],
                        'student_teacher_book'=>[
                            'subject_textbooks_available'=>[
                                'data'=>StudentBookResource::collection($studentbook_model),
                            ],
                            'subject_teachers_textbooks_available'=>[
                                'subjecttaught'=>SubjectTaughtResource::collection($subjecttaught_model),
                                'teacherbook'=>TeacherBookResource::collection($teacherbook_model),
                            ],
                        ],
                        'teacher_qualification'=>[
                            'data'=>TeachersQualificationResource::collection($teachersqualification_model)
                        ],
                        'undertaking'=>[
                            'attestation_headteacher_name'=>$undertaking_model->HeadTeacherName,
                            'attestation_headteacher_telephone'=>$undertaking_model->HeadTeacherPhone,
                            'attestation_headteacher_signdate'=>$undertaking_model->HeadTeacherSignDate,
                            'attestation_enumerator_name'=>$undertaking_model->EnumeratorName,
                            'attestation_enumerator_position'=>$undertaking_model->EnumeratorPosition,
                            'attestation_enumerator_telephone'=>$undertaking_model->EnumeratorPhone,
                            'attestation_supervisor_name'=>$undertaking_model->SupervisorName,
                            'attestation_supervisor_position'=>$undertaking_model->SupervisorPosition,
                            'attestation_supervisor_telephone'=>$undertaking_model->SupervisorPhone,
                        ]
                    ],
                ],[
                    'type'=>'Metadata',
                    'data'=>(new MetadataCollection($form_metadata)),
                    'attributes'=>(new MetadataCollection($form_metadata))->with($request),
                ]
            ];
        }

    }
}
