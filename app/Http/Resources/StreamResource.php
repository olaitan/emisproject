<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class StreamResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'class'=>$this->schoolclass->Value,
            'stream'=>$this->Stream,
            'stream_with_multigrade'=>$this->StreamWithMultiGrade,
        ];
    }
}
