<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class StateResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'state',
            'data'=>[
                'id'=>$this->Id_State,
                'statecode'=>$this->StateCode,
                'name'=>$this->State,
                'lgas'=>LgaResource::collection($this->lgas()->where('isdeleted','<>','1')->get()),
            ],
        ];
    }
}
