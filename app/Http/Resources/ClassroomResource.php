<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ClassroomResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->Id,
            'year_of_construction'=>$this->YearOfConstruction,
            'present_condition'=>$this->presentcondition->Value,
            'length_in_meters'=>$this->LengthInMeters,
            'width_in_meters'=>$this->WidthInMeters,
            'floor_material'=>$this->floormaterial->Value,
            'wall_material'=>$this->wallmaterial->Value,
            'roof_material'=>$this->roofmaterial->Value,
            'seating'=>$this->Seating,
            'good_blackboard'=>$this->GoodBlackboard
            ];
    }
}
