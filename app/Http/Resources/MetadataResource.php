<?php

namespace App\Http\Resources;
use App\Models\SlaveReference;
use Illuminate\Http\Resources\Json\Resource;

class MetadataResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'Metadata',
            'reference'=>($this->masterreference)?(string)$this->masterreference->ReferenceType:"",
            'description'=>(string)$this->Description,
            'value'=>(string)$this->Value,
            'display'=>(string)$this->DisplayValue,
            'order'=>$this->MetadataOrder,
            'level'=>$this->Level,
        ];
    }
}
