<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class StaffSchoolResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->staff->Id_Staff,
            'staff_file_no'=>$this->staff->StaffFileNo,
            'staff_name'=>$this->staff->StaffName,
            'staff_gender'=>$this->staff->Gender,
            'staff_type'=>$this->stafftype->Value,
            'salary_source'=>$this->salarysource->Value,
            'staff_yob'=>$this->staff->DateOfBirth,
            'staff_yfa'=>$this->staff->YearOfFirstAppointment,
            'staff_ypa'=>$this->PresentAppointmentYear,
            'staff_yps'=>$this->YearOfPosting,
            'staff_level'=>$this->GradeLevelStep,
            'present'=>$this->present->Value,
            'academic_qualification'=>$this->staff->academicqualification->Value,
            'teaching_qualification'=>$this->staff->teachingqualification->Value,
            'area_specialisation'=>$this->areaofspecialisation->Value,
            'subject_taught'=>$this->mainsubjecttaught->Value,
            'teaching_type'=>$this->teachingtype->Value,
            'teaches_seniorsecondary'=>'',
            'attended_training'=>$this->TeacherAttendedTraining,
        ];    }
}
