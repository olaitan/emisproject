<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\School;
use App\Models\TeachersQualification;
use App\Models\SlaveReference;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;

class TeacherQualificationController extends Controller
{
    public function saveteachersqualification(Request $request){
        
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school')){
                    //source of 
                    //remember to check if input value maps with the items in the array
                    if($request->filled('teacher_qualification')){
                        $teacher_qualification=$request->input('teacher_qualification.*');
                        foreach($teacher_qualification as $name){
                            if($name!='' && $request->school=='private'){
                                
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $q_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array

                                //preprimary
                                if($flag=DB::table('tblvar_teachersqualification')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'905','Id_HighestQualification'=>$q_ref])->exists()){
                                    DB::table('tblvar_teachersqualification')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'905','Id_HighestQualification'=>$q_ref])
                                    ->update(['Male'=>$name["preprimary_male"],'Female'=>$name["preprimary_female"]]);
                                }else{
                                    $sbs_model=TeachersQualification::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'905','Id_HighestQualification'=>$q_ref,'Male'=>$name["preprimary_male"],'Female'=>$name["preprimary_female"]]);
                                }

                                //primary
                                if($flag=DB::table('tblvar_teachersqualification')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'906','Id_HighestQualification'=>$q_ref])->exists()){
                                    DB::table('tblvar_teachersqualification')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'906','Id_HighestQualification'=>$q_ref])
                                    ->update(['Male'=>$name["primary_male"],'Female'=>$name["primary_female"]]);
                                }else{
                                    $sbs_model=TeachersQualification::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'906','Id_HighestQualification'=>$q_ref,'Male'=>$name["primary_male"],'Female'=>$name["primary_female"]]);
                                }

                                //jss
                                if($flag=DB::table('tblvar_teachersqualification')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'907','Id_HighestQualification'=>$q_ref])->exists()){
                                    DB::table('tblvar_teachersqualification')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'907','Id_HighestQualification'=>$q_ref])
                                    ->update(['Male'=>$name["jss_male"],'Female'=>$name["jss_female"]]);
                                }else{
                                    $sbs_model=TeachersQualification::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'907','Id_HighestQualification'=>$q_ref,'Male'=>$name["jss_male"],'Female'=>$name["jss_female"]]);
                                }

                                //sss
                                if($flag=DB::table('tblvar_teachersqualification')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'908','Id_HighestQualification'=>$q_ref])->exists()){
                                    DB::table('tblvar_teachersqualification')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'908','Id_HighestQualification'=>$q_ref])
                                    ->update(['Male'=>$name["sss_male"],'Female'=>$name["sss_female"]]);
                                }else{
                                    $sbs_model=TeachersQualification::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'908','Id_HighestQualification'=>$q_ref,'Male'=>$name["sss_male"],'Female'=>$name["sss_female"]]);
                                }

                                
                            }else if($name!='' && $request->school=='primary'){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $q_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array

                                //preprimary
                                if($flag=DB::table('tblvar_teachersqualification')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'905','Id_HighestQualification'=>$q_ref])->exists()){
                                    DB::table('tblvar_teachersqualification')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'905','Id_HighestQualification'=>$q_ref])
                                    ->update(['Male'=>$name["preprimary_male"],'Female'=>$name["preprimary_female"]]);
                                }else{
                                    $sbs_model=TeachersQualification::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'905','Id_HighestQualification'=>$q_ref,'Male'=>$name["preprimary_male"],'Female'=>$name["preprimary_female"]]);
                                }

                                //primary
                                if($flag=DB::table('tblvar_teachersqualification')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'906','Id_HighestQualification'=>$q_ref])->exists()){
                                    DB::table('tblvar_teachersqualification')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'906','Id_HighestQualification'=>$q_ref])
                                    ->update(['Male'=>$name["primary_male"],'Female'=>$name["primary_female"]]);
                                }else{
                                    $sbs_model=TeachersQualification::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'906','Id_HighestQualification'=>$q_ref,'Male'=>$name["primary_male"],'Female'=>$name["primary_female"]]);
                                }

                            }else if($name!='' && $request->school=='sss'){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $q_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array

                                
                                //jss
                                if($flag=DB::table('tblvar_teachersqualification')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'907','Id_HighestQualification'=>$q_ref])->exists()){
                                    DB::table('tblvar_teachersqualification')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'907','Id_HighestQualification'=>$q_ref])
                                    ->update(['Male'=>$name["jss_male"],'Female'=>$name["jss_female"]]);
                                }else{
                                    $sbs_model=TeachersQualification::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'907','Id_HighestQualification'=>$q_ref,'Male'=>$name["jss_male"],'Female'=>$name["jss_female"]]);
                                }

                                //sss
                                if($flag=DB::table('tblvar_teachersqualification')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'908','Id_HighestQualification'=>$q_ref])->exists()){
                                    DB::table('tblvar_teachersqualification')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'908','Id_HighestQualification'=>$q_ref])
                                    ->update(['Male'=>$name["sss_male"],'Female'=>$name["sss_female"]]);
                                }else{
                                    $sbs_model=TeachersQualification::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Level'=>'908','Id_HighestQualification'=>$q_ref,'Male'=>$name["sss_male"],'Female'=>$name["sss_female"]]);
                                }

                            }
                        }
                    }else{
                        return response()->json(['status'=>'error','message'=>'The array was not provided']);
                    }
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                    $authuser=Auth::user();
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save teacher qualification","notes"=>"Transaction successful"]);

                    return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }
}
