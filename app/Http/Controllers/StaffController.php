<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\School;
use App\Models\Staff;
use App\Models\StaffSchool;
use App\Models\StaffSummary;
use App\Models\SlaveReference;
use App\Models\MasterReference;
use App\Http\Resources\StaffSchoolResource;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;
use Illuminate\Support\Facades\DB;

class StaffController extends Controller
{
    //save the number of staffs
    public function savenumberofstaffs(Request $request){
        
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
           $sch_model=School::where('SchoolCode',$request->school_code)->first();
           $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                 //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')&&$request->input('school_class')=='preprimary'){
                    //saving number of staffs for jss
                //save for d1 and d2
                if($flag=DB::table('tblvar_staffsummary')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])->exists()){
                    DB::table('tblvar_staffsummary')
                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])
                    ->update(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'NonTeachersMale'=>($request->filled('staff_nonteachersmale'))?$request->input('staff_nonteachersmale'):null,'NonTeachersFemale'=>($request->filled('staff_nonteachersfemale'))?$request->input('staff_nonteachersfemale'):null,'NonTeachersTotal'=>($request->filled('staff_nonteachersfemale'))?$request->input('staff_nonteachersmale')+$request->input('staff_nonteachersfemale'):null,'TeachersMale'=>($request->filled('staff_teachersmale'))?$request->input('staff_teachersmale'):null,'TeachersFemale'=>($request->filled('staff_teachersfemale'))?$request->input('staff_teachersfemale'):null,'TeachersTotal'=>($request->filled('staff_teachersmale'))?$request->input('staff_teachersmale')+$request->input('staff_teachersfemale'):null,'CareGiversMale'=>($request->filled('caregivers_male'))?$request->input('caregivers_male'):null,'CareGiversFemale'=>($request->filled('caregivers_female'))?$request->input('caregivers_female'):null]);
                    }else{
                        $numberofstaffs=StaffSummary::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year],['NonTeachersMale'=>($request->filled('staff_nonteachersmale'))?$request->input('staff_nonteachersmale'):null,'NonTeachersFemale'=>($request->filled('staff_nonteachersfemale'))?$request->input('staff_nonteachersfemale'):null,'NonTeachersTotal'=>($request->filled('staff_nonteachersfemale'))?$request->input('staff_nonteachersmale')+$request->input('staff_nonteachersfemale'):null,'TeachersMale'=>($request->filled('staff_teachersmale'))?$request->input('staff_teachersmale'):null,'TeachersFemale'=>($request->filled('staff_teachersfemale'))?$request->input('staff_teachersfemale'):null,'TeachersTotal'=>($request->filled('staff_teachersmale'))?$request->input('staff_teachersmale')+$request->input('staff_teachersfemale'):null,'CareGiversMale'=>($request->filled('caregivers_male'))?$request->input('caregivers_male'):null,'CareGiversFemale'=>($request->filled('caregivers_female'))?$request->input('caregivers_female'):null]);
                    }
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save number of staff","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);
            }else if($request->filled('school_class')&&$request->input('school_class')=='jss'){
                //saving number of staffs for jss
                //save for d1 and d2
                if($flag=DB::table('tblvar_staffsummary')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])->exists()){
                    DB::table('tblvar_staffsummary')
                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])
                    ->update(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'NonTeachersMale'=>($request->filled('staff_nonteachersmale'))?$request->input('staff_nonteachersmale'):null,'NonTeachersFemale'=>($request->filled('staff_nonteachersfemale'))?$request->input('staff_nonteachersfemale'):null,'NonTeachersTotal'=>($request->filled('staff_nonteachersfemale'))?$request->input('staff_nonteachersmale')+$request->input('staff_nonteachersfemale'):null,'TeachersMale'=>($request->filled('staff_teachersmale'))?$request->input('staff_teachersmale'):null,'TeachersFemale'=>($request->filled('staff_teachersfemale'))?$request->input('staff_teachersfemale'):null,'TeachersTotal'=>($request->filled('staff_teachersmale'))?$request->input('staff_teachersmale')+$request->input('staff_teachersfemale'):null]);
                }else{
                    $numberofstaffs=StaffSummary::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year],['NonTeachersMale'=>($request->filled('staff_nonteachersmale'))?$request->input('staff_nonteachersmale'):null,'NonTeachersFemale'=>($request->filled('staff_nonteachersfemale'))?$request->input('staff_nonteachersfemale'):null,'NonTeachersTotal'=>($request->filled('staff_nonteachersfemale'))?$request->input('staff_nonteachersmale')+$request->input('staff_nonteachersfemale'):null,'TeachersMale'=>($request->filled('staff_teachersmale'))?$request->input('staff_teachersmale'):null,'TeachersFemale'=>($request->filled('staff_teachersfemale'))?$request->input('staff_teachersfemale'):null,'TeachersTotal'=>($request->filled('staff_teachersmale'))?$request->input('staff_teachersmale')+$request->input('staff_teachersfemale'):null]);
                }
            //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
            
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save number of staff","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','message'=>'The Post was successful']);

            }else{
                return response()->json(['status'=>'error','message'=>'The school class was not provided']);
            }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
       }else{
           return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
       } 
    }

    //save the staff
    public function savestaff(Request $request){
        //arrays to hold ids
        // $academic_qualification_ids=array(['1'=>'','2'=>'','3'=>'','4'=>'','5'=>'','6'=>'']);
        // $teaching_qualification_ids=array(['1'=>'','2'=>'','3'=>'','4'=>'','5'=>'','6'=>'']);
        // $subject_of_qualification_ids=array(['1'=>'','2'=>'','3'=>'','4'=>'','5'=>'','6'=>'','7'=>'']);
        // $main_subject_taught_ids=array(['1'=>'','2'=>'','3'=>'','4'=>'','5'=>'','6'=>'','7'=>'']);
        // $area_of_specialization_ids=array(['1'=>'','2'=>'','3'=>'','4'=>'','5'=>'','6'=>'','7'=>'']);
        // $teaching_type_ids=array(['1'=>'','2'=>'']);

        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
           $sch_model=School::where('SchoolCode',$request->school_code)->first();
           $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //get the staff
                $thestaff=new Staff;
                
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')&&$request->input('school_class')=='preprimary'){

                }else if($request->filled('school_class')&&$request->input('school_class')=='jss'){
                    //saving staff for jss

                    //get the academic qualification id
                    $academic_qualification_id=0;
                    if ($request->filled('academic_qualification')) {
                            //get the id of the status with the statuscode
                            $slaveref=SlaveReference::where('Value',$request->academic_qualification)->first();
                            $academic_qualification_id=$slaveref->Id_SlaveReference;
                        }
                    //get the teaching qualification id
                    $teaching_qualification_id=0;
                    if ($request->filled('teaching_qualification')) {
                            //get the id of the status with the statuscode
                            $slaveref=SlaveReference::where('Value',$request->teaching_qualification)->first();
                            $teaching_qualification_id=$slaveref->Id_SlaveReference;
                        }
                    
                    //save staff 
                    
                        $thestaff=Staff::create(['Id_School'=>$sch_model->Id_School,'StaffFileNo'=>$request->input('staff_file_no'),'StaffName'=>($request->filled('staff_name'))?$request->input('staff_name'):null,'Gender'=>($request->filled('gender'))?$request->input('gender'):null,'DateOfBirth'=>($request->filled('dob'))?$request->input('dob'):null,'YearOfFirstAppointment'=>($request->filled('year_of_first_appointment'))?$request->input('year_of_first_appointment'):null,'Id_AcademicQualification'=>($academic_qualification_id!=0)?$academic_qualification_id:null,'Id_TeachingQualification'=>($teaching_qualification_id!=0)?$teaching_qualification_id:null]);
                    

                    //get the stafftype id
                    $stafftype_id=0;
                    if ($request->filled('stafftype')) {
                            //get the id of the status with the statuscode
                            $slaveref=SlaveReference::where('Value',$request->stafftype)->first();
                            $stafftype_id=$slaveref->Id_SlaveReference;
                        }

                    //get the salary source id
                    $salary_source_id=0;
                    if ($request->filled('salary_source')) {
                            //get the id of the status with the statuscode
                            $slaveref=SlaveReference::where('Value',$request->salary_source)->first();
                            $salary_source_id=$slaveref->Id_SlaveReference;
                        }
                    //get the present id
                    $present_id=0;
                    if ($request->filled('present')) {
                            //get the id of the status with the statuscode
                            $slaveref=SlaveReference::where('Value',$request->present)->first();
                            $present_id=$slaveref->Id_SlaveReference;
                        }
                    //get the mainsubject taught id
                    $mainsubject_id=0;
                    if ($request->filled('mainsubject_taught')) {
                            //get the id of the status with the statuscode
                            $slaveref=SlaveReference::where('Value',$request->mainsubject_taught)->first();
                            $mainsubject_id=$slaveref->Id_SlaveReference;
                        }
                    //get the area of specialisation id
                    $specialisation_id=0;
                    if ($request->filled('specialisation')) {
                            //get the id of the status with the statuscode
                            $slaveref=SlaveReference::where('Value',$request->specialisation)->first();
                            $specialisation_id=$slaveref->Id_SlaveReference;
                        }
                    //get the teaching type id
                    $teachingtype_id=0;
                    if ($request->filled('teaching_type')) {
                            //get the id of the status with the statuscode
                            $slaveref=SlaveReference::where('Value',$request->teaching_type)->first();
                            $teachingtype_id=$slaveref->Id_SlaveReference;
                        }

                    //save staffschool
                    if($flag=DB::table('tblvar_staffschool')->where(['Id_Staff'=>$thestaff->Id_Staff,'Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])->exists()){
                        DB::table('tblvar_staffschool')
                        ->where(['Id_Staff'=>$thestaff->Id_Staff,'Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])
                        ->update(['Id_StaffType'=>($stafftype_id!=0)?$stafftype_id:null,'Id_SalarySource'=>($salary_source_id!=0)?$salary_source_id:null,'PresentAppointmentYear'=>($request->filled('present_appointment_year'))?$request->input('present_appointment_year'):null,'YearOfPosting'=>($request->filled('year_of_posting'))?$request->input('year_of_posting'):null,'GradeLevelStep'=>($request->filled('level'))?$request->input('level'):null,'Present'=>($present_id!=0)?$present_id:null,'Id_MainSubjectTaught'=>($mainsubject_id!=0)?$mainsubject_id:null,'Id_AreaOfSpecialisation'=>($specialisation_id!=0)?$specialisation_id:null,'Id_TeachingType'=>($teachingtype_id!=0)?$teachingtype_id:null,'TeacherAttendedTraining'=>($request->filled('teacher_attended_training'))?$request->input('teacher_attended_training'):null]);
                    }else{
                        $newstaff=StaffSchool::updateOrCreate(['Id_Staff'=>$thestaff->Id_Staff,'Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_StaffType'=>($stafftype_id!=0)?$stafftype_id:null,'Id_SalarySource'=>($salary_source_id!=0)?$salary_source_id:null,'PresentAppointmentYear'=>($request->filled('present_appointment_year'))?$request->input('present_appointment_year'):null,'YearOfPosting'=>($request->filled('year_of_posting'))?$request->input('year_of_posting'):null,'GradeLevelStep'=>($request->filled('level'))?$request->input('level'):null,'Present'=>($present_id!=0)?$present_id:null,'Id_MainSubjectTaught'=>($mainsubject_id!=0)?$mainsubject_id:null,'Id_AreaOfSpecialisation'=>($specialisation_id!=0)?$specialisation_id:null,'Id_TeachingType'=>($teachingtype_id!=0)?$teachingtype_id:null,'TeacherAttendedTraining'=>($request->filled('teacher_attended_training'))?$request->input('teacher_attended_training'):null]);
                    }
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                    
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save staff","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful','id'=>$thestaff->Id_Staff]);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }

            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
           
       }else{
           return response()->json(['status'=>'error','message'=>'The staff identity was not sent here'.$request->id]);
       }
    }

    //get staffs
    public function staffs($schoolcode,$year){
        
        $sch_model=School::where('SchoolCode',$schoolcode)->first();
        $authuser=Auth::user();
        if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
            $staffschool_model=StaffSchool::where('Id_School',$sch_model->Id_School)->get();

            //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
            
            $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"read","user_activity"=>"get staffs for the school","notes"=>"Transaction successful"]);
            return StaffSchoolResource::collection($staffschool_model);
        }else{
            return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
        }
        

    }

    //post staffs
    public function savestaffs(Request $request){
        
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                    //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of safe drinking water
                    //remember to check if input value maps with the items in the array
                    if($request->filled('stafflist')){
                        $stafflist=$request->input('stafflist.*');
                        //$sch_model=School::where('SchoolCode',$request->school_code)->first();
                        $tempno=0;
                        foreach($stafflist as $name){
                                $tempno=$tempno+1;
                                if($name){
                                    //get the staff
                                    $thestaff=Staff::where('Id_Staff',$name["id"])->first();

                                    //get the academic qualification id
                                    $academic_qualification_id=0;
                                    if ($name["academic_qualification"]) {
                                            //get the id of the status with the statuscode
                                            $slaveref=SlaveReference::where('Value',$name["academic_qualification"])->first();
                                            $academic_qualification_id=$slaveref->Id_SlaveReference;
                                        }
                                    //get the teaching qualification id
                                    $teaching_qualification_id=0;
                                    if ($name["teaching_qualification"]) {
                                            //get the id of the status with the statuscode
                                            $slaveref=SlaveReference::where('Value',$name["teaching_qualification"])->first();
                                            $teaching_qualification_id=$slaveref->Id_SlaveReference;
                                        }
                                    
                                    //save staff 
                                    if($flag=DB::table('tblbse_staff')->where(['Id_Staff'=>$name["id"]])->exists()){
                                        DB::table('tblbse_staff')
                                        ->where(['Id_Staff'=>$name["id"]])
                                        ->update(['StaffName'=>($name["staff_name"])?$name["staff_name"]:null,'Gender'=>($name["staff_gender"])?$name["staff_gender"]:null,'DateOfBirth'=>($name["staff_yob"])?$name["staff_yob"]:null,'YearOfFirstAppointment'=>($name["staff_yfa"])?$name["staff_yfa"]:null,'Id_AcademicQualification'=>($academic_qualification_id!=0)?$academic_qualification_id:null,'Id_TeachingQualification'=>($teaching_qualification_id!=0)?$teaching_qualification_id:null]);
                                    }else{
                                        $thestaff=Staff::create(['StaffFileNo'=>$name["staff_file_no"],'StaffName'=>($name["staff_name"])?$name["staff_name"]:null,'Gender'=>($name["staff_gender"])?$name["staff_gender"]:null,'DateOfBirth'=>($name["staff_yob"])?$name["staff_yob"]:null,'YearOfFirstAppointment'=>($name["staff_yfa"])?$name["staff_yfa"]:null,'Id_AcademicQualification'=>($academic_qualification_id!=0)?$academic_qualification_id:null,'Id_TeachingQualification'=>($teaching_qualification_id!=0)?$teaching_qualification_id:null]);
                                    }

                                    //get the stafftype id
                                    $stafftype_id=0;
                                    if ($name["staff_type"]) {
                                            //get the id of the status with the statuscode
                                            $slaveref=SlaveReference::where('Value',$name["staff_type"])->first();
                                            $stafftype_id=$slaveref->Id_SlaveReference;
                                        }

                                    //get the salary source id
                                    $salary_source_id=0;
                                    if ($name["salary_source"]) {
                                            //get the id of the status with the statuscode
                                            $slaveref=SlaveReference::where('Value',$name["salary_source"])->first();
                                            $salary_source_id=$slaveref->Id_SlaveReference;
                                        }
                                    //get the present id
                                    $present_id=0;
                                    if ($name["present"]) {
                                            //get the id of the status with the statuscode
                                            $slaveref=SlaveReference::where('Value',$name["present"])->first();
                                            $present_id=$slaveref->Id_SlaveReference;
                                        }
                                    //get the mainsubject taught id
                                    $mainsubject_id=0;
                                    if ($name["subject_taught"]) {
                                            //get the id of the status with the statuscode
                                            $slaveref=SlaveReference::where('Value',$name["subject_taught"])->first();
                                            $mainsubject_id=$slaveref->Id_SlaveReference;
                                        }
                                    //get the area of specialisation id
                                    $specialisation_id=0;
                                    if ($name["area_specialisation"]) {
                                            //get the id of the status with the statuscode
                                            $slaveref=SlaveReference::where('Value',$name["area_specialisation"])->first();
                                            $specialisation_id=$slaveref->Id_SlaveReference;
                                        }
                                    //get the teaching type id
                                    $teachingtype_id=0;
                                    if ($name["teaching_type"]) {
                                            //get the id of the status with the statuscode
                                            $slaveref=SlaveReference::where('Value',$name["teaching_type"])->first();
                                            $teachingtype_id=$slaveref->Id_SlaveReference;
                                        }

                                    //save staffschool
                                    if($flag=DB::table('tblvar_staffschool')->where(['Id_Staff'=>$thestaff->Id_Staff,'Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])->exists()){
                                        DB::table('tblvar_staffschool')
                                        ->where(['Id_Staff'=>$thestaff->Id_Staff,'Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])
                                        ->update(['Id_StaffType'=>($stafftype_id!=0)?$stafftype_id:null,'Id_SalarySource'=>($salary_source_id!=0)?$salary_source_id:null,'PresentAppointmentYear'=>($name["staff_ypa"])?$name["staff_ypa"]:null,'YearOfPosting'=>($name["staff_yps"])?$name["staff_yps"]:null,'GradeLevelStep'=>($name["staff_level"])?$name["staff_level"]:null,'Present'=>($present_id!=0)?$present_id:null,'Id_MainSubjectTaught'=>($mainsubject_id!=0)?$mainsubject_id:null,'Id_AreaOfSpecialisation'=>($specialisation_id!=0)?$specialisation_id:null,'Id_TeachingType'=>($teachingtype_id!=0)?$teachingtype_id:null,'TeacherAttendedTraining'=>($name["attended_training"])?$name["attended_training"]:null]);
                                            }else{
                                                $newstaff=StaffSchool::updateOrCreate(['Id_Staff'=>$thestaff->Id_Staff,'Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_StaffType'=>($stafftype_id!=0)?$stafftype_id:null,'Id_SalarySource'=>($salary_source_id!=0)?$salary_source_id:null,'PresentAppointmentYear'=>($name["staff_ypa"])?$name["staff_ypa"]:null,'YearOfPosting'=>($name["staff_yps"])?$name["staff_yps"]:null,'GradeLevelStep'=>($name["staff_level"])?$name["staff_level"]:null,'Present'=>($present_id!=0)?$present_id:null,'Id_MainSubjectTaught'=>($mainsubject_id!=0)?$mainsubject_id:null,'Id_AreaOfSpecialisation'=>($specialisation_id!=0)?$specialisation_id:null,'Id_TeachingType'=>($teachingtype_id!=0)?$teachingtype_id:null,'TeacherAttendedTraining'=>($name["attended_training"])?$name["attended_training"]:null]);
                                            }
                                        //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
 
                                }else{
                                    return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
                                }
                            
                        }
                    }else{
                        return response()->json(['status'=>'error','message'=>'The health facilities was not provided']);
                    }
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                   
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save staffs","notes"=>"Transaction successful"]);

                    return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }
}
