<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SchoolCharacteristic;
use App\Models\School;
use App\Models\SlaveReference;

class SchoolCharacteristicsController extends Controller
{
    public function store(Request $request){
        if($request->filled('school_code')&&$request->filled('year')){
                $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $sch=SchoolCharacteristics::firstOrNew(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year]);
            $sch->SharedFacilities=($request->filled('shared_facilities'))?$request->shared_facilities:null;
            $sch->SchoolsSharingWith=($request->filled('schools_sharingwith'))?$request->schools_sharingwith:null;
            $sch->MultiGradeTeaching=($request->filled('multi_grade_teaching'))?$request->multi_grade_teaching:null;
            $sch->DistanceFromCatchmentArea=($request->filled('distance_from_catchment_area'))?$request->distance_from_catchment_area:null;
            $sch->DistanceFromLGA=($request->filled('distance_from_lga'))?$request->distance_from_lga:null;
            $sch->StudentsTravelling3km=($request->filled('students_travelling_3km'))?$request->students_travelling_3km:null;
            $sch->MaleStudentsBoardingAtSchoolPremises=($request->filled('male_student_b'))?$request->male_student_b:null;
            $sch->FemaleStudentsBoardingAtSchoolPremises=($request->filled('female_student_b'))?$request->female_student_b:null;
            $sch->HasSchoolDevelopmentPlan=($request->filled('has_school_development_plan'))?$request->has_school_development_plan:null;
            $sch->HasSBMC=($request->filled('has_sbmc'))?$request->has_sbmc:null;
            $sch->HasPTA=($request->filled('has_pta'))?$request->has_pta:null;
            $sch->IsPSA=($request->filled('is_psa'))?$request->is_psa:null;
            $sch->LastInspectionDate=($request->filled('last_inspection_date'))?$request->last_inspection_date:null;
            $sch->InspectionNo=($request->filled('no_of_inspection'))?$request->no_of_inspection:null;


            //sort the code out first
        if ($request->filled('recognition_status')) {
            $recognitionstatus=$request->recognition_status;
            //get the id of the status with the statuscode
            $slaveref=SlaveReference::where('Value',$recognitionstatus)->first();
            $sch_model->Status=$slaveref->Id_SlaveReference;
        }

           //sort the code out first
           if ($request->filled('location_type')) {
            $locationtype=$request->location_type;
            //get the id of the status with the statuscode
            $slaveref=SlaveReference::where('Value',$locationtype)->first();
            $sch_model->Id_LocationType=$slaveref->Id_SlaveReference;
        }

           //sort the code out first
           if ($request->filled('level_of_education')) {
            $levelofeducation=$request->level_of_education;
            //get the id of the status with the statuscode
            $slaveref=SlaveReference::where('Value',$levelofeducation)->first();
            $sch_model->LevelofEducation=$slaveref->Id_SlaveReference;
        }

           //sort the code out first
           if ($request->filled('school_type')) {
            $schooltype=$request->school_type;
            //get the id of the status with the statuscode
            $slaveref=SlaveReference::where('Value',$schooltype)->first();
            $sch_model->Id_SchoolType=$slaveref->Id_SlaveReference;
        }

        //sort the code out first
        if ($request->filled('last_inspection_authority')) {
            $lastinspectionauthoritycode=$request->last_inspection_authority;
            //get the id of the lga with the lgacode
            $slaveref=SlaveReference::where('Value',$lastinspectionauthoritycode)->first();
            $sch->LastInspectionAuthority=$slaveref->Id_SlaveReference;
        }else{
            $sch->LastInspectionAuthority=null;
        }
        
        $sch->ConditionalCashTransfer=($request->filled('conditional_cash_transfer'))?$request->conditional_cash_transfer:null;
        $sch->HasSchoolGrant=($request->filled('has_school_grant'))?$request->has_school_grant:null;
        $sch->HasSecurityGuard=($request->filled('has_security_guard'))?$request->has_security_guard:null;

        //sort the code out first
        if ($request->filled('ownership')) {
            $ownershipcode=$request->ownership;
            //get the id of the lga with the lgacode
            $slaveref=SlaveReference::where('Value',$ownershipcode)->first();
            $sch->Ownership=$slaveref->Id_SlaveReference;
        }else{
            $sch->Ownership=null;
        }

        //save school shifts
        $sch_model->OperateShift=($request->filled('shift'))?$request->shift:null;

        $sch->save();
        $sch_model->save();
        return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
        
    }
}
