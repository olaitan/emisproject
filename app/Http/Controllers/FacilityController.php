<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AvailableFacility;
use App\Models\FenceFacility;
use App\Models\SafeDrinkingWater;
use App\Models\SlaveReference;
use App\Models\SharedFacility;
use App\Models\Toilet;
use App\Models\PowerSource;
use App\Models\HealthFacility;
use App\Models\Ownership;
use App\Models\SchoolBuilding;
use App\Models\PlayRoom;
use App\Models\PlayFacility;
use App\Models\LearningMaterial;
use App\Models\School;
use App\Models\Seating;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;

class FacilityController extends Controller
{

    public function savewatersource(Request $request){
        
        //an array of water source id
        $water_source_ids=array(['Borehole'=>'89','Well'=>'90','None'=>'229','Stream'=>'91','Tankers'=>'92','Rain Water'=>'120','Pipe Borne Water'=>'88']);
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
                //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
            if($request->filled('school_class')){
                //source of safe drinking water
                //loop through the array
                if($request->filled('drinking_water_sources')){
                    $sources=$request->input('drinking_water_sources.*');
                   // $sources_ids=array('Pre-primary'=>'905','Primary'=>'906','Junior Secondary'=>'907','Senior Secondary'=>'908');
                    foreach($sources as $name){
                        if($name!=''){
                            //save or update
                            $slaveref=SlaveReference::where('Value',$name)->first();
                            $level_ref=$slaveref->Id_SlaveReference;
                            //remember to check if input value maps with the items in the array
                            
                            $safedrinkingwater=SafeDrinkingWater::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_WaterSource'=>($level_ref!=null)?$level_ref:null]);
                            
                        }
                    }
                }
            //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
            
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save drinking water sources","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','message'=>'The Post was successful']);

            }else{
                return response()->json(['status'=>'error','message'=>'The school class was not provided']);
            }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            

        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        } 
    }

    public function savefacilitiesavailable(Request $request){
        
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                if($request->filled('facilities_available')){
                    $facilities_available=$request->input('facilities_available.*');
                    foreach($facilities_available as $name){
                        if($name!='' ){
                            $slaveref=SlaveReference::where('Value',$name["value"])->first();
                            $meta_data=$slaveref->Id_SlaveReference;
                           
                            if($flag=DB::table('tblvar_availablefacilities')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_FacilityType'=>$meta_data])->exists()){
                                DB::table('tblvar_availablefacilities')
                                ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_FacilityType'=>$meta_data])
                                ->update(['Useable'=>(array_key_exists('useable', $name))?$name["useable"]:null,'NotUseable'=>(array_key_exists('notuseable', $name))?$name["notuseable"]:null]);
                            }else{
                                $toilet_model=AvailableFacility::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_FacilityType'=>$meta_data],['Useable'=>(array_key_exists('useable', $name))?$name["useable"]:null,'NotUseable'=>(array_key_exists('notuseable', $name))?$name["notuseable"]:null]);
                            }
                        }
                    }
                }
    
                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save facilities available","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','message'=>'The Post was successful']);
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    public function savesharedfacilities(Request $request){
        
        //an array of water source id
        $shared_facilities_ids=array(['Toilet'=>'706','Computer'=>'707','Water Source'=>'708','Laboratory'=>'709','Classroom'=>'710','Library'=>'711','Play Ground'=>'712','Others'=>'713','Wash hand facility'=>'778']);
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
                //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of safe drinking water
                    //check if shared_facilities is filled
                    if($request->filled('shared_facilities')){
                        $shared_facilities=$request->input('shared_facilities.*');
                        foreach($shared_facilities as $name){
                            if($name!=''){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name)->first();
                                $facility_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array
                                
                                $sharedfacility_model=SharedFacility::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SharedFacility'=>($facility_ref!=null)?$facility_ref:null]);
                                
                            }
                        }
                    }else{
                        return response()->json(['status'=>'error','message'=>'The shared facilities was not provided']);
                    }
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                    
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save shared facilities","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);
                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    public function savetoilettype(Request $request){
        
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                if($request->filled('toilets')){
                    $toilets=$request->input('toilets.*');
                    foreach($toilets as $name){
                        if($name!='' ){
                            $slaveref=SlaveReference::where('Value',$name["value"])->first();
                            $meta_data=$slaveref->Id_SlaveReference;
        
                            //check and get the schoolclass from the schoolclass field
                            if($request->filled('school_class')){
        
                                //used only by students
                                if($flag=DB::table('tblvar_toilets')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_UserType'=>'490','Id_ToiletType'=>$meta_data])->exists()){
                                    DB::table('tblvar_toilets')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_UserType'=>'490','Id_ToiletType'=>$meta_data])
                                    ->update(['Male'=>(array_key_exists('students_male', $name))?$name["students_male"]:null,'Female'=>(array_key_exists('students_female', $name))?$name["students_female"]:null,'Mixed'=>(array_key_exists('students_mixed', $name))?$name["students_mixed"]:null]);
                                }else{
                                    $pitt_students_model=Toilet::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_UserType'=>'490','Id_ToiletType'=>$meta_data,'Male'=>(array_key_exists('students_male', $name))?$name["students_male"]:null,'Female'=>(array_key_exists('students_female', $name))?$name["students_female"]:null,'Mixed'=>(array_key_exists('students_mixed', $name))?$name["students_mixed"]:null]);
                                }
        
                                //used only by teachers
                                if($flag=DB::table('tblvar_toilets')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_UserType'=>'491','Id_ToiletType'=>$meta_data])->exists()){
                                    DB::table('tblvar_toilets')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_UserType'=>'491','Id_ToiletType'=>$meta_data])
                                    ->update(['Male'=>(array_key_exists('teachers_male', $name))?$name["teachers_male"]:null,'Female'=>(array_key_exists('teachers_female', $name))?$name["teachers_female"]:null,'Mixed'=>(array_key_exists('teachers_mixed', $name))?$name["teachers_mixed"]:null]);
                                }else{
                                    $pitt_teachers_model=Toilet::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_UserType'=>'491','Id_ToiletType'=>$meta_data,'Male'=>(array_key_exists('teachers_male', $name))?$name["teachers_male"]:null,'Female'=>(array_key_exists('teachers_female', $name))?$name["teachers_female"]:null,'Mixed'=>(array_key_exists('teachers_mixed', $name))?$name["teachers_mixed"]:null]);
                                }
        
                                //used by students and teachers
                                if($flag=DB::table('tblvar_toilets')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_UserType'=>'492','Id_ToiletType'=>$meta_data])->exists()){
                                    DB::table('tblvar_toilets')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_UserType'=>'492','Id_ToiletType'=>$meta_data])
                                    ->update(['Male'=>(array_key_exists('both_male', $name))?$name["both_male"]:null,'Female'=>(array_key_exists('both_female', $name))?$name["both_female"]:null,'Mixed'=>(array_key_exists('both_mixed', $name))?$name["both_mixed"]:null]);
                                }else{
                                    $pitt_both_model=Toilet::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_UserType'=>'492','Id_ToiletType'=>$meta_data,'Male'=>(array_key_exists('both_male', $name))?$name["both_male"]:null,'Female'=>(array_key_exists('both_female', $name))?$name["both_female"]:null,'Mixed'=>(array_key_exists('both_mixed', $name))?$name["both_mixed"]:null]);
                                }
        
                            }else{
                                return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                            }
                        }
                    }
                    
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                    $authuser=Auth::user();
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save toilet type","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);
        
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    public function savesourceofpower(Request $request){
        //an array of power source id
        $power_source_ids=array(['Generator'=>'172','PHCN(NEPA)'=>'173','Solar'=>'174','Battery'=>'175','Others'=>'176','Gas Turbine'=>'339']);
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
                //get the school object
                $sch_model=School::where('SchoolCode',$request->school_code)->first();
                $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of safe drinking water
                    //remember to check if input value maps with the items in the array
                    if($request->filled('power_source')){
                        $power_sources=$request->input('power_source.*');
                        foreach($power_sources as $name){
                            if($name!=''){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name)->first();
                                $facility_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array
                                
                                $powersource_model=PowerSource::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_PowerSource'=>($facility_ref!=null)?$facility_ref:null]);
                                
                            }
                        }
                    }else{
                        return response()->json(['status'=>'error','message'=>'The power source  was not provided']);
                    }
                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
            
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save source of power","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    public function savehealthfacility(Request $request){
        $health_facility_ids=array(['No'=>'486','Yes, first aid kit'=>'485','Yes, health clinic'=>'484']);
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of safe drinking water
                    //remember to check if input value maps with the items in the array
                    if($request->filled('health_facility')){
                    
                        //save or update
                        $slaveref=SlaveReference::where('Value',$request->health_facility)->first();
                        $facility_ref=$slaveref->Id_SlaveReference;
                        //remember to check if input value maps with the items in the array
                        if($flag=DB::table('tblvar_healthfacility')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])->exists()){
                            DB::table('tblvar_healthfacility')
                            ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])
                            ->update(['Id_HealthFacility'=>($facility_ref!=null)?$facility_ref:null]);
                        }else{
                            $healthfacility_model=HealthFacility::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_HealthFacility'=>($facility_ref!=null)?$facility_ref:null]);
                        }
                        
                    }else{
                        return response()->json(['status'=>'error','message'=>'The health facilities was not provided']);
                    }
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save health facility","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    public function savefencefacility(Request $request){
        
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of safe drinking water
                    //remember to check if input value maps with the items in the array
                    if($request->filled('fence_facility')){
                        //save or update
                        $slaveref=SlaveReference::where('Value',$request->fence_facility)->first();
                        $facility_ref=$slaveref->Id_SlaveReference;
                        //remember to check if input value maps with the items in the array
                        if($flag=DB::table('tblvar_fencefacility')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])->exists()){
                            DB::table('tblvar_fencefacility')
                            ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])
                            ->update(['Id_FenceFacility'=>($facility_ref!=null)?$facility_ref:null]);
                        }else{
                            $fencefacility_model=FenceFacility::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_FenceFacility'=>($facility_ref!=null)?$facility_ref:null]);
                        }
                        
                    }else{
                        return response()->json(['status'=>'error','message'=>'The fence facility was not provided']);
                    }
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                    
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save fence facility","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    public function saveseaters(Request $request){
       
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of safe drinking water
                    //remember to check if input value maps with the items in the array
                    if($request->filled('seaters')){
                        $seaters=$request->input('seaters.*');
                        foreach($seaters as $name){
                            if($name!='' && $request->school_class!="primary"){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $class_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array

                                //1 Seater
                                if($flag=DB::table('tblvar_seating')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'923'])->exists()){
                                    DB::table('tblvar_seating')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'923'])
                                    ->update(['SeatingCapacity'=>(array_key_exists('seater1', $name))?$name["seater1"]:null]);
                                }else{
                                    $sbs_model=Seating::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'923','SeatingCapacity'=>(array_key_exists('seater1', $name))?$name["seater1"]:null]);
                                }

                                //2 Seater
                                if($flag=DB::table('tblvar_seating')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'924'])->exists()){
                                    DB::table('tblvar_seating')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'924'])
                                    ->update(['SeatingCapacity'=>(array_key_exists('seater2', $name))?$name["seater2"]:null]);
                                }else{
                                    $sbs_model=Seating::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'924','SeatingCapacity'=>(array_key_exists('seater2', $name))?$name["seater2"]:null]);
                                }

                                //3 Seater
                                if($flag=DB::table('tblvar_seating')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'925'])->exists()){
                                    DB::table('tblvar_seating')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'925'])
                                    ->update(['SeatingCapacity'=>(array_key_exists('seater3', $name))?$name["seater3"]:null]);
                                }else{
                                    $sbs_model=Seating::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'925','SeatingCapacity'=>(array_key_exists('seater3', $name))?$name["seater3"]:null]);
                                }
                                
                            }else{
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $class_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array

                                //1 Seater
                                if($flag=DB::table('tblvar_seating')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'923'])->exists()){
                                    DB::table('tblvar_seating')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'923'])
                                    ->update(['SeatingCapacity'=>(array_key_exists('seater1', $name))?$name["seater1"]:null]);
                                }else{
                                    $sbs_model=Seating::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'923','SeatingCapacity'=>(array_key_exists('seater1', $name))?$name["seater1"]:null]);
                                }

                                //2 Seater
                                if($flag=DB::table('tblvar_seating')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'924'])->exists()){
                                    DB::table('tblvar_seating')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'924'])
                                    ->update(['SeatingCapacity'=>(array_key_exists('seater2', $name))?$name["seater2"]:null]);
                                }else{
                                    $sbs_model=Seating::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'924','SeatingCapacity'=>(array_key_exists('seater2', $name))?$name["seater2"]:null]);
                                }

                                //3 Seater
                                if($flag=DB::table('tblvar_seating')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'925'])->exists()){
                                    DB::table('tblvar_seating')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'925'])
                                    ->update(['SeatingCapacity'=>(array_key_exists('seater3', $name))?$name["seater3"]:null]);
                                }else{
                                    $sbs_model=Seating::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'925','SeatingCapacity'=>(array_key_exists('seater3', $name))?$name["seater3"]:null]);
                                }

                                //4 Seater
                                if($flag=DB::table('tblvar_seating')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'932'])->exists()){
                                    DB::table('tblvar_seating')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'932'])
                                    ->update(['SeatingCapacity'=>(array_key_exists('seater4', $name))?$name["seater4"]:null]);
                                }else{
                                    $sbs_model=Seating::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'932','SeatingCapacity'=>(array_key_exists('seater4', $name))?$name["seater4"]:null]);
                                }

                                //5 Seater
                                if($flag=DB::table('tblvar_seating')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'933'])->exists()){
                                    DB::table('tblvar_seating')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'933'])
                                    ->update(['SeatingCapacity'=>(array_key_exists('seater5', $name))?$name["seater5"]:null]);
                                }else{
                                    $sbs_model=Seating::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'933','SeatingCapacity'=>(array_key_exists('seater5', $name))?$name["seater5"]:null]);
                                }

                                //6 Seater
                                if($flag=DB::table('tblvar_seating')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'934'])->exists()){
                                    DB::table('tblvar_seating')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'934'])
                                    ->update(['SeatingCapacity'=>(array_key_exists('seater6', $name))?$name["seater6"]:null]);
                                }else{
                                    $sbs_model=Seating::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolClass'=>$class_ref,'Id_SeatType'=>'934','SeatingCapacity'=>(array_key_exists('seater6', $name))?$name["seater6"]:null]);
                                }
                            }
                        }
                    }else{
                        return response()->json(['status'=>'error','message'=>'The fence facilities was not provided']);
                    }
                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
            
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save seaters","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    public function saveownership(Request $request){
        
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of safe drinking water
                    //remember to check if input value maps with the items in the array
                    if($request->filled('ownership')){
                        $ownership=$request->input('ownership');

                        //save or update
                        $slaveref=SlaveReference::where('Value',$ownership)->first();
                        $ownership_ref=$slaveref->Id_SlaveReference;
                        //remember to check if input value maps with the items in the array
                        if($flag=DB::table('tblvar_ownership')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])->exists()){
                            DB::table('tblvar_ownership')
                            ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])
                            ->update(['Id_Ownership'=>($ownership_ref!=null)?$ownership_ref:null]);
                        }else{
                            $healthfacility_model=Ownership::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Ownership'=>($ownership_ref!=null)?$ownership_ref:null]);
                        }
                    }else{
                        return response()->json(['status'=>'error','message'=>'The ownership was not provided']);
                    }
                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
            
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save ownership","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    public function saveschoolbuilding(Request $request){
        
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of safe drinking water
                    //remember to check if input value maps with the items in the array
                    if($request->filled('school_building')){
                        $building=$request->input('school_building');

                        //save or update
                        $slaveref=SlaveReference::where('Value',$building)->first();
                        $building_ref=$slaveref->Id_SlaveReference;
                        //remember to check if input value maps with the items in the array
                        if($flag=DB::table('tblvar_schoolbuilding')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])->exists()){
                            DB::table('tblvar_schoolbuilding')
                            ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])
                            ->update(['Id_SchoolBuilding'=>($building_ref!=null)?$building_ref:null]);
                        }else{
                            $healthfacility_model=SchoolBuilding::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_SchoolBuilding'=>($building_ref!=null)?$building_ref:null]);
                        }
                    }else{
                        return response()->json(['status'=>'error','message'=>'The ownership was not provided']);
                    }
                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
            
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save school building","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    public function saveplayroom(Request $request){
        
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of safe drinking water
                    //remember to check if input value maps with the items in the array
                    if($request->filled('playroom')){
                        $playroom=$request->input('playroom');
                        
                        if($playroom!=''){
                            //save or update
                            $slaveref=SlaveReference::where('Value',$playroom)->first();
                            $playroom_ref=$slaveref->Id_SlaveReference;
                            //remember to check if input value maps with the items in the array
                            
                            $playroom_model=PlayRoom::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year],['Id_PlayRoom'=>($playroom_ref!=null)?$playroom_ref:null]);
                            
                        }
                        //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                        
                        $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save playroom","notes"=>"Transaction successful"]);
                        return response()->json(['status'=>'success','message'=>'The Post was successful']);
                    }else{
                        return response()->json(['status'=>'error','message'=>'The ownership was not provided']);
                    }
                    

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    public function saveplayfacility(Request $request){
        
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                 //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of safe drinking water
                    //remember to check if input value maps with the items in the array
                    if($request->filled('play_facilities')){
                        $play_facilities=$request->input('play_facilities.*');
                        foreach($play_facilities as $name){
                            if($name!=''){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name)->first();
                                $play_facilities_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array
                                
                                $healthfacility_model=PlayFacility::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_PlayFacility'=>($play_facilities_ref!=null)?$play_facilities_ref:null]);
                                
                            }
                        }
                    }else{
                        return response()->json(['status'=>'error','message'=>'The health facilities was not provided']);
                    }
                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save play facility","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
           
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    public function savelearningmaterial(Request $request){
       
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of safe drinking water
                    //remember to check if input value maps with the items in the array
                    if($request->filled('materials')){
                        $materials=$request->input('materials.*');
                        foreach($materials as $name){
                            if($name!=''){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name)->first();
                                $materials_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array
                                
                                $healthfacility_model=LearningMaterial::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_LearningMaterial'=>($materials_ref!=null)?$materials_ref:null]);
                                
                            }
                        }
                    }else{
                        return response()->json(['status'=>'error','message'=>'The health facilities was not provided']);
                    }
                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save learning material","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    public function removedwsource(Request $request){
        // $authuser=Auth::user();
        // if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){

                
            
        // }else{
        //     return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
        // }
        if($request->filled('school_code')&& $request->filled('year')&& $request->filled('source')){
            $sch_model=School::where('SchoolCode',$request->school_code)->first();

            $slaveref=SlaveReference::where('Value',$request->source)->first();
            $level_ref=$slaveref->Id_SlaveReference;

            $dwsource=SafeDrinkingWater::where([['Id_School','=',$sch_model->Id_School],['CensusYear','=',$request->year],['Id_WaterSource','=',$level_ref]])->delete();
            if($dwsource){
                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                $authuser=Auth::user();
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"delete","user_activity"=>"remove drinking water resource","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=>'The source was not deleted, does not exist']);

            }
            
            
            
        }
    }

    public function removesharedfacility(Request $request){
        if($request->filled('school_code')&& $request->filled('year')&& $request->filled('shared_facility')){
            $sch_model=School::where('SchoolCode',$request->school_code)->first();

            $slaveref=SlaveReference::where('Value',$request->shared_facility)->first();
            $sf_ref=$slaveref->Id_SlaveReference;

            $sf=SharedFacility::where([['Id_School','=',$sch_model->Id_School],['CensusYear','=',$request->year],['Id_SharedFacility','=',$sf_ref]])->delete();
            if($sf){
                $authuser=Auth::user();
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"delete","user_activity"=>"remove shared facility","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=>'The source was not deleted, does not exist']);

            }
            
            
            
        }
    }

    public function removepowersource(Request $request){
        if($request->filled('school_code')&& $request->filled('year')&& $request->filled('power_source')){
            $sch_model=School::where('SchoolCode',$request->school_code)->first();

            $slaveref=SlaveReference::where('Value',$request->power_source)->first();
            $obj_ref=$slaveref->Id_SlaveReference;

            $pwsource=PowerSource::where([['Id_School','=',$sch_model->Id_School],['CensusYear','=',$request->year],['Id_PowerSource','=',$obj_ref]])->delete();
            if($pwsource){
                $authuser=Auth::user();
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"delete","user_activity"=>"remove power source","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=>'The source was not deleted, does not exist']);

            }
            
            
            
        }
    }

    public function removelearningmaterial(Request $request){
        if($request->filled('school_code')&& $request->filled('year')&& $request->filled('material')){
            $sch_model=School::where('SchoolCode',$request->school_code)->first();

            $slaveref=SlaveReference::where('Value',$request->material)->first();
            $lm_ref=$slaveref->Id_SlaveReference;

            $lm=LearningMaterial::where([['Id_School','=',$sch_model->Id_School],['CensusYear','=',$request->year],['Id_LearningMaterial','=',$lm_ref]])->delete();
            if($lm){
                $authuser=Auth::user();
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"delete","user_activity"=>"remove learning material","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=>'The source was not deleted, does not exist']);

            }
        }
    }

    public function removeplayfacility(Request $request){
        if($request->filled('school_code')&& $request->filled('year')&& $request->filled('play_facility')){
            $sch_model=School::where('SchoolCode',$request->school_code)->first();

            $slaveref=SlaveReference::where('Value',$request->play_facility)->first();
            $obj_ref=$slaveref->Id_SlaveReference;

            $pr=PlayFacility::where([['Id_School','=',$sch_model->Id_School],['CensusYear','=',$request->year],['Id_PlayFacility','=',$obj_ref]])->delete();
            if($pr){
                $authuser=Auth::user();
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"delete","user_activity"=>"remove play facility","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=>'The source was not deleted, does not exist']);

            }
            
        }
    }
}