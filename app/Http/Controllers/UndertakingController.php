<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Undertaking;
use App\Http\Resources\UndertakingResource;
use App\Models\School;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;

class UndertakingController extends Controller
{
    public function saveundertaking(Request $request){
        
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                if($flag=DB::table('tblvar_undertaking')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])->exists()){
                    DB::table('tblvar_undertaking')
                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])
                    ->update(['HeadTeacherName'=>$request->attestation_headteacher_name,'HeadTeacherPhone'=>$request->attestation_headteacher_telephone,'HeadTeacherSignDate'=>$request->attestation_headteacher_signdate,'EnumeratorName'=>$request->attestation_enumerator_name,'EnumeratorPosition'=>$request->attestation_enumerator_position,'EnumeratorPhone'=>$request->attestation_enumerator_telephone,'SupervisorName'=>$request->attestation_supervisor_name,'SupervisorPosition'=>$request->attestation_supervisor_position,'SupervisorPhone'=>$request->attestation_supervisor_telephone]);
                }else{
                    $sbs_model=Undertaking::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'HeadTeacherName'=>$request->attestation_headteacher_name,'HeadTeacherPhone'=>$request->attestation_headteacher_telephone,'HeadTeacherSignDate'=>$request->attestation_headteacher_signdate,'EnumeratorName'=>$request->attestation_enumerator_name,'EnumeratorPosition'=>$request->attestation_enumerator_position,'EnumeratorPhone'=>$request->attestation_enumerator_telephone,'SupervisorName'=>$request->attestation_supervisor_name,'SupervisorPosition'=>$request->attestation_supervisor_position,'SupervisorPhone'=>$request->attestation_supervisor_telephone]);
                }
                
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save undertaking","notes"=>"Transaction successful"]);
    
                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                return response()->json(['status'=>'success','message'=>'The Post was successful']);
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
           
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }
}
