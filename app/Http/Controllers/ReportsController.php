<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\School;
use App\Models\SchoolRegistration;
use App\Models\State;
use App\Models\Lga;
use App\Models\CensusYear;
use App\Models\SchoolCharacteristic;
use App\Models\PrivateSchoolCharacteristic;
use App\Models\SlaveReference;
use App\Models\MasterReference;
use App\Http\Resources\FormdataResource;
use App\Http\Resources\SchoolCharacteristicsReportResource;
use App\Http\Resources\SchoolTeachersReportResource;
use App\Http\Resources\SchoolFacilityReportResource;
use App\Http\Resources\SchoolStreamsReportResource;
use App\Http\Resources\RepeatersReportResource;
use App\Http\Resources\EnrolmentReportResource;
use App\Http\Resources\ReportOverviewResource;
use App\Http\Resources\UserResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;
use App\User; 
use Validator;

class ReportsController extends Controller
{
    protected $Sdp=null;
    protected $Sbmc=null;
    protected $Pta=null;
    protected $Grants=null;
    protected $Guard=null;
    protected $Ownership=null;
    protected $CensusYear=null;
    

    public function overview(Request $request){
 
        $schs=School::all();

        return response()->json(['data'=>new ReportOverviewResource($schs)]);
    }


    public function characteristics_search(Request $request){
        $tcoll=array();

        
        //$authuser=Auth::user();
        //Prepare the search variables
        
        //manages the strict access control for users 
       

            if ($request->input('state')!='') {
                $statename=$request->input('state');
                //get the id of the state with the statecode
                $state=State::where('State',$statename)->first();

                //This is a patch find a lasting solution to it later
                //$qpatch=$statecode+116;
                $state = array('Id_State','=',$state->Id_State);
                //$tcoll[]=$state;
            }
      
      

        //lga
        if ($request->input('lga')!='') {
            $lgacode=$request->input('lga');
            //get the id of the lga with the lgacode
            $lga=Lga::where('LGA',$lgacode)->first();
            $lga = array('Id_LGA','=',$lga->id_LGA);
            //$tcoll[]=$lga;
        }

        //census year
        if ($request->input('census_year')!='') {
            $censusyear=$request->input('census_year');
            $tcoll[]=array('Year','=',$censusyear);
        }
 
        $schs=SchoolCharacteristic::where($tcoll)->get();

        return response()->json(['data'=>SchoolCharacteristicsReportResource::collection($schs)]);
    }

    public function enrolment_search(Request $request){
        $tcoll=array();
        
        //$authuser=Auth::user();
        //Prepare the search variables
        
        //manages the strict access control for users 
        //if($authuser->Id_Role==1){

            if ($request->input('state')!='') {
                $statename=$request->input('state');
                //get the id of the state with the statecode
                $state=State::where('State',$statename)->first();

                //This is a patch find a lasting solution to it later
                //$qpatch=$statecode+116;
                $state = array('Id_State','=',$state->Id_State);
                //$tcoll[]=$state;
            }
        //}else{
            
            //$state = array('Id_State','=',$authuser->state_id);
            //$tcoll[]=$state;
        //}

        //lga
        if ($request->input('lga')!='') {
            $lgacode=$request->input('lga');
            //get the id of the lga with the lgacode
            $lga=Lga::where('LGA',$lgacode)->first();
            $lga = array('Id_LGA','=',$lga->id_LGA);
            //$tcoll[]=$lga;
        }

        $schs=School::where($tcoll)->get();

        return response()->json(['data'=>EnrolmentReportResource::collection($schs)]);
    }

    public function repeaters_search(Request $request){
        $tcoll=array();
        
        //$authuser=Auth::user();
        //Prepare the search variables
        
        //manages the strict access control for users 
        //if($authuser->Id_Role==1){

            if ($request->input('state')!='') {
                $statename=$request->input('state');
                //get the id of the state with the statecode
                $state=State::where('State',$statename)->first();

                //This is a patch find a lasting solution to it later
                //$qpatch=$statecode+116;
                $state = array('Id_State','=',$state->Id_State);
                //$tcoll[]=$state;
            }
        //}else{
            
            //$state = array('Id_State','=',$authuser->state_id);
            //$tcoll[]=$state;
        //}

        //lga
        if ($request->input('lga')!='') {
            $lgacode=$request->input('lga');
            //get the id of the lga with the lgacode
            $lga=Lga::where('LGA',$lgacode)->first();
            $lga = array('Id_LGA','=',$lga->id_LGA);
            //$tcoll[]=$lga;
        }


        //$schs=School::where($tcoll)->paginate(100);
        $schs=School::where($tcoll)->get();

        return response()->json(['data'=>RepeatersReportResource::collection($schs)]);
    }

    public function facility_search(Request $request){
        $tcoll=array();
        
        //$authuser=Auth::user();
        //Prepare the search variables
        
        //manages the strict access control for users 
        //if($authuser->Id_Role==1){

            if ($request->input('state')!='') {
                $statename=$request->input('state');
                //get the id of the state with the statecode
                $state=State::where('State',$statename)->first();

                //This is a patch find a lasting solution to it later
                //$qpatch=$statecode+116;
                $state = array('Id_State','=',$state->Id_State);
                //$tcoll[]=$state;
            }
        //}else{
            
            //$state = array('Id_State','=',$authuser->state_id);
            //$tcoll[]=$state;
        //}

        //lga
        if ($request->input('lga')!='') {
            $lgacode=$request->input('lga');
            //get the id of the lga with the lgacode
            $lga=Lga::where('LGA',$lgacode)->first();
            $lga = array('Id_LGA','=',$lga->id_LGA);
            //$tcoll[]=$lga;
        }
        
        
 

        //$schs=School::where($tcoll)->paginate(100);
        $schs=School::where($tcoll)->get();



        return response()->json(['data'=>SchoolFacilityReportResource::collection($schs)]);
    }

    public function streams_search(Request $request){
        $tcoll=array();
        
        //$authuser=Auth::user();
        //Prepare the search variables
        
        //manages the strict access control for users 
        //if($authuser->Id_Role==1){

            if ($request->input('state')!='') {
                $statename=$request->input('state');
                //get the id of the state with the statecode
                $state=State::where('State',$statename)->first();

                //This is a patch find a lasting solution to it later
                //$qpatch=$statecode+116;
                $state = array('Id_State','=',$state->Id_State);
                //$tcoll[]=$state;
            }
        //}else{
            
            //$state = array('Id_State','=',$authuser->state_id);
            //$tcoll[]=$state;
        //}

        //lga
        if ($request->input('lga')!='') {
            $lgacode=$request->input('lga');
            //get the id of the lga with the lgacode
            $lga=Lga::where('LGA',$lgacode)->first();
            $lga = array('Id_LGA','=',$lga->id_LGA);
            //$tcoll[]=$lga;
        }

        //$schs=School::where($tcoll)->paginate(100);
        $schs=School::where($tcoll)->get();
        //$schs=School::all();

        return response()->json(['data'=>SchoolStreamsReportResource::collection($schs)]);
    }

    public function teachers_search(Request $request){
        $tcoll=array();
        
        //$authuser=Auth::user();
        //Prepare the search variables
        
        //manages the strict access control for users 
        //if($authuser->Id_Role==1){

            if ($request->input('state')!='') {
                $statename=$request->input('state');
                //get the id of the state with the statecode
                $state=State::where('State',$statename)->first();

                //This is a patch find a lasting solution to it later
                //$qpatch=$statecode+116;
                $state = array('Id_State','=',$state->Id_State);
                //$tcoll[]=$state;
            }
        //}else{
            
            //$state = array('Id_State','=',$authuser->state_id);
            //$tcoll[]=$state;
        //}

        //lga
        if ($request->input('lga')!='') {
            $lgacode=$request->input('lga');
            //get the id of the lga with the lgacode
            $lga=Lga::where('LGA',$lgacode)->first();
            $lga = array('Id_LGA','=',$lga->id_LGA);
            //$tcoll[]=$lga;
        }

        //$schs=School::where($tcoll)->paginate(100);
        $schs=School::all();

        return response()->json(['data'=>SchoolTeachersReportResource::collection($schs)]);
    }
}
