<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SlaveReference;
use App\Models\Workshop;
use App\Models\School;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;

class WorkshopController extends Controller
{
    //save the classroom
    public function saveworkshop(Request $request){

        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
           $sch_model=School::where('SchoolCode',$request->school_code)->first();
           $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')&&$request->input('school_class')=='jss'){
                    //saving staff for jss

                    //get the floor material id
                    $floormaterial_id=0;
                    if ($request->filled('floor_material')) {
                        //get the id of the status with the statuscode
                        $slaveref=SlaveReference::where('Value',$request->floor_material)->first();
                        $floormaterial_id=$slaveref->Id_SlaveReference;
                    }
                    //get the teaching qualification id
                    $wallmaterial_id=0;
                    if ($request->filled('wall_material')) {
                        //get the id of the status with the statuscode
                        $slaveref=SlaveReference::where('Value',$request->wall_material)->first();
                        $wallmaterial_id=$slaveref->Id_SlaveReference;
                    }

                    //get the teaching qualification id
                    $roofmaterial_id=0;
                    if ($request->filled('roof_material')) {
                        //get the id of the status with the statuscode
                        $slaveref=SlaveReference::where('Value',$request->roof_material)->first();
                        $roofmaterial_id=$slaveref->Id_SlaveReference;
                    }

                    $presentcondition_id=0;
                    if ($request->filled('present_condition')) {
                        //get the id of the status with the statuscode
                        $slaveref=SlaveReference::where('Value',$request->present_condition)->first();
                        $presentcondition_id=$slaveref->Id_SlaveReference;
                    }

                    $workshoptype_id=0;
                    if ($request->filled('workshop_type')) {
                        //get the id of the status with the statuscode
                        $slaveref=SlaveReference::where('Value',$request->workshop_type)->first();
                        $workshoptype_id=$slaveref->Id_SlaveReference;
                    }
                    
                    //save classroom         
                    $obj=Workshop::create(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_WorkshopType'=>($workshoptype_id!=0)?$workshoptype_id:null,'YearOfConstruction'=>$request->input('year_of_construction'),'Length'=>($request->filled('length_in_meters'))?$request->input('length_in_meters'):null,'Width'=>($request->filled('width_in_meters'))?$request->input('width_in_meters'):null,'Shared'=>($request->filled('shared'))?$request->input('shared'):null,'Seating'=>($request->filled('seating'))?$request->input('seating'):null,'GoodBlackboard'=>($request->filled('good_blackboard'))?$request->input('good_blackboard'):null,'Id_PresentCondition'=>($presentcondition_id!=0)?$presentcondition_id:null,'Id_FloorMaterial'=>($floormaterial_id!=0)?$floormaterial_id:null,'Id_RoofMaterial'=>($roofmaterial_id!=0)?$roofmaterial_id:null,'Id_WallMaterial'=>($wallmaterial_id!=0)?$wallmaterial_id:null]);
                    
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save workshop","notes"=>"Transaction successful"]);

                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                return response()->json(['status'=>'success','message'=>'The Post was successful','id'=>$obj->Id]);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
           
       }else{
           return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
       }
    }

    //save classrooms
    public function saveworkshops(Request $request){
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //remember to check if input value maps with the items in the array
                if($request->filled('workshops')){
                    $workshops=$request->input('workshops.*');
                    foreach($workshops as $name){

                        if($name){  
                            //get the floor material id
                            $floormaterial_id=0;
                            if ($name['floor_material']) {
                                    //get the id of the status with the statuscode
                                    $slaveref=SlaveReference::where('Value',$name['floor_material'])->first();
                                    $floormaterial_id=$slaveref->Id_SlaveReference;
                                }
                            //get the teaching qualification id
                            $wallmaterial_id=0;
                            if ($name['wall_material']) {
                                    //get the id of the status with the statuscode
                                    $slaveref=SlaveReference::where('Value',$name['wall_material'])->first();
                                    $wallmaterial_id=$slaveref->Id_SlaveReference;
                                }

                                //get the teaching qualification id
                            $roofmaterial_id=0;
                            if ($name['roof_material']) {
                                    //get the id of the status with the statuscode
                                    $slaveref=SlaveReference::where('Value',$name['roof_material'])->first();
                                    $roofmaterial_id=$slaveref->Id_SlaveReference;
                                }

                                $presentcondition_id=0;
                            if ($name['present_condition']) {
                                    //get the id of the status with the statuscode
                                    $slaveref=SlaveReference::where('Value',$name['present_condition'])->first();
                                    $presentcondition_id=$slaveref->Id_SlaveReference;
                                }

                            $workshoptype_id=0;
                            if ($name['workshop_type']) {
                                //get the id of the status with the statuscode
                                $slaveref=SlaveReference::where('Value',$name['workshop_type'])->first();
                                $workshoptype_id=$slaveref->Id_SlaveReference;
                            }
                            if($name['id']){
                                //update
                                DB::table('tblvar_workshop')
                                ->where(['Id'=>$name['id']])
                                ->update(['Id_WorkshopType'=>($workshoptype_id!=0)?$workshoptype_id:null,'YearOfConstruction'=>($name['year_of_construction'])?$name['year_of_construction']:null,'Length'=>($name['length_in_meters'])?$name['length_in_meters']:null,'Width'=>($name['width_in_meters'])?$name['width_in_meters']:null,'Shared'=>$name['shared'],'Seating'=>$name['seating'],'GoodBlackboard'=>$name['good_blackboard'],'Id_PresentCondition'=>($presentcondition_id!=0)?$presentcondition_id:null,'Id_FloorMaterial'=>($floormaterial_id!=0)?$floormaterial_id:null,'Id_RoofMaterial'=>($roofmaterial_id!=0)?$roofmaterial_id:null,'Id_WallMaterial'=>($wallmaterial_id!=0)?$wallmaterial_id:null]);
                            }else{
                                //create
                                $obj=Workshop::create(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_WorkshopType'=>($workshoptype_id!=0)?$workshoptype_id:null,'YearOfConstruction'=>($name['year_of_construction'])?$name['year_of_construction']:null,'Length'=>($name['length_in_meters'])?$name['length_in_meters']:null,'Width'=>($name['width_in_meters'])?$name['width_in_meters']:null,'Shared'=>$name['shared'],'Seating'=>$name['seating'],'GoodBlackboard'=>$name['good_blackboard'],'Id_PresentCondition'=>($presentcondition_id!=0)?$presentcondition_id:null,'Id_FloorMaterial'=>($floormaterial_id!=0)?$floormaterial_id:null,'Id_RoofMaterial'=>($roofmaterial_id!=0)?$roofmaterial_id:null,'Id_WallMaterial'=>($wallmaterial_id!=0)?$wallmaterial_id:null]);
                            }
                            
                        }
                    }
                }else{
                    return response()->json(['status'=>'error','message'=>'The health facilities was not provided']);
                }
                
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save workshops","notes"=>"Transaction successful"]);

                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                return response()->json(['status'=>'success','message'=>'The Post was successful']);
        
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
    
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    public function removeworkshop(Request $request){
        if($request->filled('school_code')&& $request->filled('year')&& $request->filled('workshop_id')){
            $pr=Workshop::where('Id',$request->workshop_id)->delete();
            if($pr){
                $authuser=Auth::user();
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"delete","user_activity"=>"remove workshop","notes"=>"Transaction successful"]);

                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=>'The source was not deleted, does not exist']);
            }
        }
    }
}
