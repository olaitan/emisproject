<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\School;
use App\Models\Lga;
use App\Models\State;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;

class SchoolIdentificationController extends Controller
{
    public function store(Request $request){
        if($request->filled('school_code')){
        $sch=School::firstOrNew(['SchoolCode'=>$request->school_code]);
        $authuser=Auth::user();
        if($authuser->state_id==$sch->Id_State || $authuser->Id_Role==1){
            $sch->SchoolName=($request->filled('school_name'))?$request->school_name:$sch->SchoolName;
            $sch->XCoordinate=($request->filled('xcoordinate'))?$request->xcoordinate:$sch->Xcoordinate;
            $sch->YCoordinate=($request->filled('ycoordinate'))?$request->ycoordinate:$sch->Ycoordinate;
            $sch->ZCoordinate=($request->filled('zcoordinate'))?$request->zcoordinate:$sch->Zcoordinate;
            $sch->SchoolAddress=($request->filled('school_address'))?$request->school_address:$sch->SchoolAddress;
            $sch->Town=($request->filled('town'))?$request->town:$sch->Town;
            $sch->Ward=($request->filled('ward'))?$request->ward:$sch->Ward;

            //sort the lga code out
        if ($request->filled('lga')) {
            $lgacode=$request->lga;
            //get the id of the lga with the lgacode
            $lga=Lga::where('LGA',$lgacode)->first();
            $sch->Id_LGA=$lga->id_LGA;
        }

        //sort the state code out
        if ($request->filled('state')) {
            $statecode=$request->state;
            //get the id of the state with the lgacode
            $state=State::where('State',$statecode)->first();
            $sch->Id_State=$state->Id_State;
        }
            $sch->Telephone=($request->filled('telephone'))?$request->telephone:$sch->Telephone;
            $sch->Email=($request->filled('email'))?$request->email:$sch->Email;

            //check if private
            if($sch->FormType=="private"){
                $sch->Proprietor=($request->filled('proprietor'))?$request->proprietor:$sch->proprietor;//save the proprietor
                $sch->Primary_Schoolcode=($request->filled('primary_schoolcode'))?$request->primary_schoolcode:$sch->Primary_Schoolcode;//save the primary school code
                $sch->Jss_Schoolcode=($request->filled('jss_schoolcode'))?$request->jss_schoolcode:$sch->Jss_Schoolcode;//save the jss school code
                $sch->Sss_Schoolcode=($request->filled('sss_schoolcode'))?$request->sss_schoolcode:$sch->Sss_Schoolcode;//save the sss school code

            }
            $sch->save();
            //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
            
            $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save school identification","notes"=>"Transaction successful"]);
            return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=>'The school code was not sent']);
            }
        }else{
            return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
        }
        
        
    }
}
