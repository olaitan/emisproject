<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SlaveReference;
use App\Models\School;
use App\Models\StaffTransfer;
use App\Models\Staff;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;
class StaffRemovalController extends Controller
{
    public function removestaff(Request $request){

        //check slave for type of removal
           //get the academic qualification id
           $removalcategory_id=0;
           if ($request->filled('removalcategory')) {
                   //get the id of the status with the statuscode
                   $slaveref=SlaveReference::where('Value',$request->removalcategory)->first();
                   $removalcategory_id=$slaveref->Id_SlaveReference;

            if($removalcategory_id=="936"){
                //transfer

                
                if($request->filled('transfer_school_code')&&$request->filled('year')&&$request->filled('staff_id')){
                    //get the school object
                   $sch_model=School::where('SchoolCode',$request->transfer_school_code)->first();
                   if($sch_model->FormType=='private'){
                    return response()->json(['status'=>'error','message'=>'Cannot transfer to a private school']);

                   }else{
                    $staff_model=Staff::where('Id_Staff',$request->staff_id)->first();
                   
                    //record the transfer in the transferstaff table
                 $numberofstaffs=StaffTransfer::create(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'FromSchool'=>$staff_model->Id_School,'ToSchool'=>$sch_model->Id_School,'Id_Staff'=>$staff_model->Id_Staff]);
 
                    //record it in the staff table
                     DB::table('tblbse_staff')
                     ->where(['Id_Staff'=>$staff_model->Id_Staff])
                     ->update(['Id_School'=>$sch_model->Id_School]);
 
                     DB::table('tblvar_staffschool')
                     ->where(['Id_Staff'=>$staff_model->Id_Staff])
                     ->update(['Id_School'=>$sch_model->Id_School]);
                     //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                     $authuser=Auth::user();
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"update","user_activity"=>"transfer staff","notes"=>"Transaction successful"]);

                     return response()->json(['status'=>'success','message'=>'Post Successful']);
             
                   }
                      }else{
                   return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
               } 
                

            }else{
                if($request->filled('school_code')&&$request->filled('year')&&$request->filled('staff_id')){
                    //get the school object
                   $sch_model=School::where('SchoolCode',$request->school_code)->first();
                   $staff_model=Staff::where('Id_Staff',$request->staff_id)->first();
                   
                   //other removals
                //update the staff
                DB::table('tblbse_staff')
                ->where(['Id_Staff'=>$staff_model->Id_Staff])
                ->update(['IsRemoved'=>"1",'Id_RemovalReason'=>$removalcategory_id]);
                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                $authuser=Auth::user();
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"update","user_activity"=>"set cause of staff absence","notes"=>"Transaction successful"]);

                return response()->json(['status'=>'success','message'=>'Post Successful']);
               }else{
                   return response()->json(['status'=>'error','message'=>'The school code/year was not sent for others'.$request->staff_no]);
               } 
                
            }
        }
        //checks for the school_code and the year
        
    }
}
