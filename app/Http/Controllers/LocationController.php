<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\State;
use App\Models\Lga;
use App\Http\Resources\StateResource;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;
class LocationController extends Controller
{
    public function states(){
        $states=State::all();
        return StateResource::collection($states); 
    }

    public function savelgas(Request $request){
		if($request->filled('lgas') && $request->filled('state')){
			$lgas=$request->input('lgas.*');
			$mref=State::where('State',$request->state)->first();
            $state_ref=$mref->Id_State;
			foreach($lgas as $name){
				if($name["id"]!=''){
					
					if($flag=DB::table('tblmtd_lga')->where(['id_LGA'=>$name["id"],'Id_State'=>$state_ref])->exists()){
                        DB::table('tblmtd_lga')
                        ->where(['id_LGA'=>$name["id"],'Id_State'=>$state_ref])
                        ->update(['Id_State'=>$state_ref,'LGA'=>($name["name"]!=null)?$name["name"]:null,'LGACode'=>($name["lgacode"]!=null)?$name["lgacode"]:null]);
                    }else{
                        $_model=Lga::create(['Id_State'=>$state_ref,'LGA'=>($name["name"]!=null)?$name["name"]:null,'LGACode'=>($name["lgacode"]!=null)?$name["lgacode"]:null]);
                    }
				}else{
					
                        $_model=Lga::create(['Id_State'=>$state_ref,'LGA'=>($name["name"]!=null)?$name["name"]:null,'LGACode'=>($name["lgacode"]!=null)?$name["lgacode"]:null]);
                    
				}
			}
		}else{
			return response()->json(['status'=>'error','message'=>'The metadatas was not provided']);
		}
		//$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
	return response()->json(['status'=>'success','message'=>'The Post was successful']);
	}
}
