<?php

namespace App\Http\Controllers\ApiAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User; 
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;
use Validator;
use App\Models\UserRole;
use App\Http\Resources\UserRoleResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserLogResource;

class UserController extends Controller
{
    //
    public $successStatus = 200;


    public function logoutApi()
    { 
        if (Auth::check()) 
        {
            //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
            Auth::user()->AauthAcessToken()->delete();
            return response()->json(['status'=>'success','message'=>'successfully logged out']);
        }
    }

    public function logoutApiAppclient()
    { 
        if (Auth::check()) 
        {
            //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
            //Auth::user()->AauthAcessToken()->delete();
            return response()->json(['status'=>'success','message'=>'successfully logged out']);
        }
    }

    public function getuserlogs(){
        $authuser=Auth::user();
        $userlogs=UserLog::all();
        //$create_log=UserLog::create(["id_user"=>$authuser->id,"category"=>"read","user_activity"=>"get user logs","notes"=>"Transaction successful"]);
        //return UserLogResource::collection($userlogs);
        return response()->json(['data'=>UserLogResource::collection($userlogs)]);

    }

    public function getroles(){
        $roles=UserRole::all();
        //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
        return UserRoleResource::collection($roles);
    }
/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(Request $request){ 
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email',
            'password' => 'required',
            
        ]);
        if ($validator->fails()) { 
                    return response()->json(['status'=>'error','message'=>$validator->errors()]);            
                }

        $user=User::where('email',request('email'))->first();
        if(!$user || $user->isdeleted=="1"){
            return response()->json(['status'=>'error','message'=>'Email is deleted from the system, contact admin']); 
        }
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            $success['email']=$user->email;
            $create_log=UserLog::create(["id_user"=>$user->id,"category"=>"read","user_activity"=>"login successfully","notes"=>"login by ".$user->name]);
            return response()->json(['status'=>'success','success' => $success,'theme'=>$user->state->Theme], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['status'=>'error','message'=>'Password Incorrect']); 
        } 
    }

    public function login_appclient(Request $request){ 
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email',
            'password' => 'required',
            
        ]);
        if ($validator->fails()) { 
                    return response()->json(['error'=>$validator->errors()], 401);            
                }

        $user=User::where('email',request('email'))->first();
        if(!$user || $user->isdeleted=="1"){
            return response()->json(['status'=>'error','message'=>'Email is deleted from the system, contact admin']); 
        }
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            $success['email']= $user->email;
            $success['name']= $user->name;
            $create_log=UserLog::create(["id_user"=>$user->id,"category"=>"read","user_activity"=>"login successfully","notes"=>"login by ".$user->name]);
            return response()->json(['status'=>'success','success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['message'=>'Unauthorised'], 401); 
        } 
    }
/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
        if ($validator->fails()) { 
                    return response()->json(['error'=>$validator->errors()], 401);            
                }
        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['name'] =  $user->name;
        $create_log=UserLog::create(["id_user"=>$user->id,"category"=>"create","user_activity"=>"user registration","notes"=>""]);
        return response()->json(['success'=>$success], $this-> successStatus); 
    }

    public function register_appclient(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
            'state_id'=>'required',
        ]);
        if ($validator->fails()) { 
                    return response()->json(['error'=>$validator->errors()], 401);            
                }
        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['name'] =  $user->name;
        $success['email'] =$user->email;
        $create_log=UserLog::create(["id_user"=>$user->id,"category"=>"create","user_activity"=>"user registration","notes"=>""]);
        return response()->json(['status'=>'success','success'=>$success], $this-> successStatus); 
    }

/** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        $user = Auth::user(); 
        $create_log=UserLog::create(["id_user"=>$user->id,"category"=>"read","user_activity"=>"request user details","notes"=>"user details successful transaction"]);
        return new UserResource($user);
    } 

    public function picUpload(Request $request)
    {
        $authuser=Auth::user();

        $validator = Validator::make($request->all(), [
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2084kb'
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $image = $request->file('file');
        $input['file'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['file']);

        //post
        $authuser->profile_pic='/images/'.$input['file'];
        $authuser->save();
        
        //$this->postImage->add($input);


        return response()->json(['status'=>'success']);
    }

    public function remove(Request $request){
        $authuser=Auth::user();
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email', 
        ]);
        if ($validator->fails()) { 
                    return response()->json(['error'=>$validator->errors()], 401);            
            }

                //get the user and change the role
        $user=User::where('email',$request->email)->first();
        if($user){
            $user->isdeleted = 1;
            $user->save();
            $create_log=UserLog::create(["id_user"=>$authuser->id,"category"=>"delete","user_activity"=>"deleting a user from the system(soft delete)","notes"=>"".$user->email." successfully deleted"]);
            return response()->json(['status'=>'success','email'=>$user->email]);
        }else{
            $create_log=UserLog::create(["id_user"=>$authuser->id,"category"=>"delete","user_activity"=>"deleting a user from the system(soft delete)","notes"=>"Transaction failed, email not registered (".$request->email.")"]);
            return response()->json(['status'=>'error','message'=>'email not registered, kindly inform your administrator']);
        }
    }

    public function change(Request $request){
        $authuser=Auth::user();
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email',
            'Id_Role'=>'required',  
        ]);
        if ($validator->fails()) { 
                    return response()->json(['error'=>$validator->errors()], 401);            
                }

        //get the user and change the role
        $user=User::where('email',$request->email)->first();
        if($user){
            $user->Id_Role = $request->Id_Role;
            $user->save();
            $create_log=UserLog::create(["id_user"=>$authuser->id,"category"=>"update","user_activity"=>"change user role","notes"=>"Transaction successful,".$user->email." To:".$user->userrole->Role]);
            return response()->json(['status'=>'success','email'=>$user->email]);
        }else{
            $create_log=UserLog::create(["id_user"=>$authuser->id,"category"=>"update","user_activity"=>"change user role","notes"=>"Transaction unsuccessful,email not registered"]);
            return response()->json(['status'=>'error','message'=>'email not registered, kindly inform your administrator']);
        }
    }

    public function saveprofile(Request $request){
        $authuser=Auth::user();
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email',
            'older_email' => 'required|email',
            'name'=>'required',  
            
        ]);
        if ($validator->fails()) { 
                    return response()->json(['error'=>$validator->errors()], 401);            
                }

        //get the user and set the new email and name
        $user=User::where('email',$request->older_email)->first();
        if($user){
            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();

            // $image = $request->user_pic;
            // $input['user_pic'] = time().'.'.$image->getClientOriginalExtension();
            // $destinationPath = public_path('/images');
            // $image->move($destinationPath, $input['user_pic']);

            $create_log=UserLog::create(["id_user"=>$authuser->id,"category"=>"update","user_activity"=>"save profile","notes"=>"Tansaction successful"]);
            return response()->json(['status'=>'success','email'=>$user->email]);
        }else{
            $create_log=UserLog::create(["id_user"=>$authuser->id,"category"=>"update","user_activity"=>"save profile","notes"=>"Transaction unsuccessful"]);
            return response()->json(['status'=>'error','message'=>'email not registered, kindly inform your administrator']);
        }
    }

    public function savepassword(Request $request){
        $authuser=Auth::user();
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email',
            'current_password' => 'required',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) { 
                    return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        $user=User::where('email',$request->email)->first();
        //get the user and change the role
        if(Auth::attempt(['email' => request('email'), 'password' => request('current_password')])){
            $user->password =bcrypt($request->password);
            $user->save();
            $create_log=UserLog::create(["id_user"=>$authuser->id,"category"=>"update","user_activity"=>"change password","notes"=>"Transaction successful,".$authuser->email]);
            return response()->json(['status'=>'success']);
        }else{
            $create_log=UserLog::create(["id_user"=>$authuser->id,"category"=>"update","user_activity"=>"change password","notes"=>"Transaction unsuccessful,".$authuser->email]);
            return response()->json(['status'=>'error','message'=>'password incorrect, enter the current password again']);
        }
    }

    public function addnew(Request $request) 
    { 
        //check and make sure role id is not dataentry, for security
        $authuser = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'name'=>'required', 
            'email' => 'required|email',
            'Id_Role'=>'required',
        ]);  

        if($authuser->Id_Role==1){
            $validator = Validator::make($request->all(), [ 
                'name'=>'required', 
                'email' => 'required|email',
                'Id_Role'=>'required',
                'state_id'=>'required',
            ]);
        }
        
        if ($validator->fails()) { 
                    return response()->json(['error'=>$validator->errors()], 401);            
                }
        $input = $request->all();
        
        $user = new User; 
        $user->name=$input['name'];
        $user->email=$input['email'];
        $user->Id_Role=$input['Id_Role'];
        $user->state_id=($authuser->Id_Role==1)?$input['state_id']:$authuser->state_id;
        
        $user->save();

        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['email'] =  $user->email;
        $create_log=UserLog::create(["id_user"=>$authuser,"category"=>"create","user_activity"=>"add new user","notes"=>"Transaction successful,".$user->email]);
        return response()->json(['status'=>'success','success'=>$success,'state'=>$input['state_id']]); 
    }

    public function createpassword(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
        if ($validator->fails()) { 
                    return response()->json(['error'=>$validator->errors()], 401);            
                }
        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user=User::where('email',$request->email)->first();
        if(!$user || $user->isdeleted=="1"){
            return response()->json(['status'=>'error','message'=>'Email is deleted from the system, contact admin']); 
        }
        if($user){
            $user->password=$input['password'];
            $user->save();
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
                $user = Auth::user(); 
                $success['token'] =  $user->createToken('MyApp')->accessToken; 
                $success['email'] =  $user->email;
                $create_log=UserLog::create(["id_user"=>$user->id,"category"=>"create","user_activity"=>"create password","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','success' => $success,'theme'=>$user->state->Theme]); 
            } 
            else{ 
                return response()->json(['status'=>'error','message'=>'Unauthorised'], 401); 
            }

        }else{
            return response()->json(['status'=>'error','message'=>'email not registered, kindly inform your administrator']);
        }
       
        
    }

    public function searchemail(Request $request) 
    { 
        try {
            $validator = Validator::make($request->all(), [  
                'email' => 'required|email', 
            ]);
            if ($validator->fails()) { 
                        return response()->json(['error'=>$validator->errors()], 401);            
                    }
            $user=User::where('email',$request->email)->first();
            if($user){
                $create_log=UserLog::create(["id_user"=>$user->id,"category"=>"read","user_activity"=>"search email","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','email'=>$user->email,'password_exist'=>($user->password!=null)?'yes':'no']);
            }else{
                return response()->json(['status'=>'error','message'=>'email not registered, kindly inform your administrator']);
            }
          }
          
          //catch exception
          catch(Exception $e) {
            return response()->json(['status'=>'error','message'=>'Message: ' .$e->getMessage()]);
          }
        
         
    }

}
