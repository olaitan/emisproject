<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ClassroomSummary;
use App\Models\SlaveReference;
use App\Models\Classroom;
use App\Models\School;
use App\Models\OtherRoom;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;
class ClassroomsController extends Controller
{
    //save the number of classroom
    public function savenumberofclassrooms(Request $request){
        
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
           $sch_model=School::where('SchoolCode',$request->school_code)->first();
           $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')&&$request->input('school_class')=='preprimary'){

                    //saving number of staffs for jss
                    //save for d1 and d2
                    if($flag=DB::table('tblvar_classroomsummary')->where(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year])->exists()){
                        DB::table('tblvar_classroomsummary')
                        ->where(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year])
                        ->update(['NoOfClassrooms'=>($request->filled('no_of_classrooms'))?$request->input('no_of_classrooms'):null,'ClassesHeldOutside'=>($request->filled('classes_held_outside'))?$request->input('classes_held_outside'):null,'NoOfPlayRooms'=>($request->filled('no_playrooms'))?$request->input('no_playrooms'):null]);
                        }else{
                            $classroomsummary=ClassroomSummary::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year],['NoOfClassrooms'=>($request->filled('no_of_classrooms'))?$request->input('no_of_classrooms'):null,'ClassesHeldOutside'=>($request->filled('classes_held_outside'))?$request->input('classes_held_outside'):null,'NoOfPlayRooms'=>($request->filled('no_playrooms'))?$request->input('no_playrooms'):null]);
                        }
                        $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save number of classrooms","notes"=>"Transaction successful"]);
                        return response()->json(['status'=>'success','message'=>'The Post was successful']);
                }else if($request->filled('school_class')&&$request->input('school_class')=='jss'){
                    //saving number of staffs for jss
                    //save for d1 and d2
                    if($flag=DB::table('tblvar_classroomsummary')->where(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year])->exists()){
                        DB::table('tblvar_classroomsummary')
                        ->where(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year])
                        ->update(['NoOfClassrooms'=>($request->filled('no_of_classrooms'))?$request->input('no_of_classrooms'):null,'ClassesHeldOutside'=>($request->filled('classes_held_outside'))?$request->input('classes_held_outside'):null]);
                    }else{
                        $classroomsummary=ClassroomSummary::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year],['NoOfClassrooms'=>($request->filled('no_of_classrooms'))?$request->input('no_of_classrooms'):null,'ClassesHeldOutside'=>($request->filled('classes_held_outside'))?$request->input('classes_held_outside'):null]);
                    }
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save number of classrooms","notes"=>"Transaction successful"]);
                        return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        } 
    }

    //save the classroom
    public function saveclassroom(Request $request){

        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
           $sch_model=School::where('SchoolCode',$request->school_code)->first();
           $authuser=Auth::user();
           
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //get the floor material id
                $floormaterial_id=0;
                if ($request->filled('floor_material')) {
                        //get the id of the status with the statuscode
                        $slaveref=SlaveReference::where('Value',$request->floor_material)->first();
                        $floormaterial_id=$slaveref->Id_SlaveReference;
                    }
                //get the teaching qualification id
                $wallmaterial_id=0;
                if ($request->filled('wall_material')) {
                        //get the id of the status with the statuscode
                        $slaveref=SlaveReference::where('Value',$request->wall_material)->first();
                        $wallmaterial_id=$slaveref->Id_SlaveReference;
                    }

                    //get the teaching qualification id
                $roofmaterial_id=0;
                if ($request->filled('roof_material')) {
                        //get the id of the status with the statuscode
                        $slaveref=SlaveReference::where('Value',$request->roof_material)->first();
                        $roofmaterial_id=$slaveref->Id_SlaveReference;
                    }

                    $presentcondition_id=0;
                if ($request->filled('present_condition')) {
                        //get the id of the status with the statuscode
                        $slaveref=SlaveReference::where('Value',$request->present_condition)->first();
                        $presentcondition_id=$slaveref->Id_SlaveReference;
                    }
                
                //save classroom
                
                $classroom=Classroom::create(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'YearOfConstruction'=>$request->input('year_of_construction'),'LengthInMeters'=>($request->filled('length_in_meters'))?$request->input('length_in_meters'):null,'WidthInMeters'=>($request->filled('width_in_meters'))?$request->input('width_in_meters'):null,'Seating'=>($request->filled('seating'))?$request->input('seating'):null,'GoodBlackboard'=>($request->filled('good_blackboard'))?$request->input('good_blackboard'):null,'Id_PresentCondition'=>($presentcondition_id!=0)?$presentcondition_id:null,'Id_FloorMaterial'=>($floormaterial_id!=0)?$floormaterial_id:null,'Id_RoofMaterial'=>($roofmaterial_id!=0)?$roofmaterial_id:null,'Id_WallMaterial'=>($wallmaterial_id!=0)?$wallmaterial_id:null]);
                

                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save classroom","notes"=>"Transaction successful"]);
            return response()->json(['status'=>'success','message'=>'The Post was successful','id'=>$classroom->Id]);

            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
       }else{
           return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
       }
    }

    public function saveclassrooms(Request $request){
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                    //source of safe drinking water
                    //remember to check if input value maps with the items in the array
                    if($request->filled('classrooms')){
                        $classrooms=$request->input('classrooms.*');
                        foreach($classrooms as $name){
                            if($name){
                                    
                    //get the floor material id
                $floormaterial_id=0;
                if ($name['floor_material']) {
                        //get the id of the status with the statuscode
                        $slaveref=SlaveReference::where('Value',$name['floor_material'])->first();
                        $floormaterial_id=$slaveref->Id_SlaveReference;
                    }
                //get the teaching qualification id
                $wallmaterial_id=0;
                if ($name['wall_material']) {
                        //get the id of the status with the statuscode
                        $slaveref=SlaveReference::where('Value',$name['wall_material'])->first();
                        $wallmaterial_id=$slaveref->Id_SlaveReference;
                    }

                    //get the teaching qualification id
                $roofmaterial_id=0;
                if ($name['roof_material']) {
                        //get the id of the status with the statuscode
                        $slaveref=SlaveReference::where('Value',$name['roof_material'])->first();
                        $roofmaterial_id=$slaveref->Id_SlaveReference;
                    }

                    $presentcondition_id=0;
                if ($name['present_condition']) {
                        //get the id of the status with the statuscode
                        $slaveref=SlaveReference::where('Value',$name['present_condition'])->first();
                        $presentcondition_id=$slaveref->Id_SlaveReference;
                    }
                
                    if($name['id']){
                        //update
                        DB::table('tblvar_classroom')
                        ->where(['Id'=>$name['id']])
                        ->update(['YearOfConstruction'=>($name['year_of_construction'])?$name['year_of_construction']:null,'LengthInMeters'=>($name['length_in_meters'])?$name['length_in_meters']:null,'WidthInMeters'=>($name['width_in_meters'])?$name['width_in_meters']:null,'Seating'=>$name['seating'],'GoodBlackboard'=>$name['good_blackboard'],'Id_PresentCondition'=>($presentcondition_id!=0)?$presentcondition_id:null,'Id_FloorMaterial'=>($floormaterial_id!=0)?$floormaterial_id:null,'Id_RoofMaterial'=>($roofmaterial_id!=0)?$roofmaterial_id:null,'Id_WallMaterial'=>($wallmaterial_id!=0)?$wallmaterial_id:null]);
                    }else{
                        //create
                        $thestaff=Classroom::create(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'YearOfConstruction'=>($name['year_of_construction'])?$name['year_of_construction']:null,'LengthInMeters'=>($name['length_in_meters'])?$name['length_in_meters']:null,'WidthInMeters'=>($name['width_in_meters'])?$name['width_in_meters']:null,'Seating'=>$name['seating'],'GoodBlackboard'=>$name['good_blackboard'],'Id_PresentCondition'=>($presentcondition_id!=0)?$presentcondition_id:null,'Id_FloorMaterial'=>($floormaterial_id!=0)?$floormaterial_id:null,'Id_RoofMaterial'=>($roofmaterial_id!=0)?$roofmaterial_id:null,'Id_WallMaterial'=>($wallmaterial_id!=0)?$wallmaterial_id:null]);
                    }
                
                        }
                    }
                    }else{
                        return response()->json(['status'=>'error','message'=>'The health facilities was not provided']);
                    }
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save classrooms","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','message'=>'The Post was successful']);
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    //save other rooms
    public function saveotherrooms(Request $request){
        //checks for the school_code and the year
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
           $sch_model=School::where('SchoolCode',$request->school_code)->first();
           $authuser=Auth::user();
           if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')&&$request->input('school_class')=='jss'){
                    //save staff room
                    if($flag=DB::table('tblvar_otherrooms')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'720'])->exists()){
                        DB::table('tblvar_otherrooms')
                        ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'720'])
                        ->update(['Number'=>($request->filled('no_of_staffrooms'))?$request->input('no_of_staffrooms'):null]);
                    }else{
                        $otherroom=OtherRoom::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'720'],['Number'=>($request->filled('no_of_staffrooms'))?$request->input('no_of_staffrooms'):null]);
                    }

                    //save office
                    if($flag=DB::table('tblvar_otherrooms')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'721'])->exists()){
                    DB::table('tblvar_otherrooms')
                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'721'])
                    ->update(['Number'=>($request->filled('no_of_offices'))?$request->input('no_of_offices'):null]);
                    }else{
                        $otherroom=OtherRoom::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'721'],['Number'=>($request->filled('no_of_offices'))?$request->input('no_of_offices'):null]);
                    }

                    //save laboratories
                    if($flag=DB::table('tblvar_otherrooms')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'722'])->exists()){
                        DB::table('tblvar_otherrooms')
                        ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'722'])
                        ->update(['Number'=>($request->filled('no_of_laboratories'))?$request->input('no_of_laboratories'):null]);
                    }else{
                        $otherroom=OtherRoom::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'722'],['Number'=>($request->filled('no_of_laboratories'))?$request->input('no_of_laboratories'):null]);
                    }

                    //save store room
                    if($flag=DB::table('tblvar_otherrooms')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'723'])->exists()){
                        DB::table('tblvar_otherrooms')
                        ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'723'])
                        ->update(['Number'=>($request->filled('no_of_storerooms'))?$request->input('no_of_storerooms'):null]);
                    }else{
                        $otherroom=OtherRoom::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'723'],['Number'=>($request->filled('no_of_storerooms'))?$request->input('no_of_storerooms'):null]);
                    }

                    //save others
                    if($flag=DB::table('tblvar_otherrooms')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'922'])->exists()){
                        DB::table('tblvar_otherrooms')
                        ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'922'])
                        ->update(['Number'=>($request->filled('no_of_others'))?$request->input('no_of_others'):null]);
                    }else{
                        $otherroom=OtherRoom::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_RoomType'=>'922'],['Number'=>($request->filled('no_of_others'))?$request->input('no_of_others'):null]);
                    }
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save other rooms","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','message'=>'The Post was successful']);

            }else{
                return response()->json(['status'=>'error','message'=>'The school class was not provided']);
            }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
       }else{
           return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
       } 
    }

    public function removeclassroom(Request $request){
        if($request->filled('school_code')&& $request->filled('year')&& $request->filled('class_id')){
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $pr=Classroom::where('Id',$request->class_id)->delete();
            if($pr){
                $authuser=Auth::user();
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"delete","user_activity"=>"delete classrooms","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=>'The source was not deleted, does not exist='.$request->class_id]);
            }
        }
    }
}
