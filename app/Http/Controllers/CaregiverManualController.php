<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SlaveReference;
use App\Models\School;
use App\Models\CaregiverManual;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;

class CaregiverManualController extends Controller
{
    public function savecgmanual(Request $request){
        
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){

                //check and get the schoolclass from the schoolclass field
                if($request->filled('school')){
                    //source of 
                    //remember to check if input value maps with the items in the array
                    if($request->filled('caregiversmanuals')){
                        $caregiversmanuals=$request->input('caregiversmanuals.*');
                        foreach($caregiversmanuals as $name){
                            if($name!='' && $request->school=='primary'){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $q_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array

                                //preprimary
                                if($flag=DB::table('tblvar_caregivermanual')->where(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_ManualCategory'=>$q_ref])->exists()){
                                    DB::table('tblvar_caregivermanual')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_ManualCategory'=>$q_ref])
                                    ->update(['Available'=>$name["available"]]);
                                }else{
                                    $sbs_model=CaregiverManual::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_ManualCategory'=>$q_ref,'Available'=>$name["available"]]);
                                }
                            }
                        }
                    
                        $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"saving care giver manual data, Transaction successful","notes"=>""]);
                        return response()->json(['status'=>'success','message'=>'Posted Successfully']);
                    }else{
                        return response()->json(['status'=>'error','message'=>'The array was not provided']);
                    }
                    
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }
}
