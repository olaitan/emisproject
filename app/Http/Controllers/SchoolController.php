<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\School;
use App\Models\SchoolRegistration;
use App\Models\State;
use App\Models\Lga;
use App\Models\CensusYear;
use App\Models\SchoolCharacteristic;
use App\Models\PrivateSchoolCharacteristic;
use App\Models\SlaveReference;
use App\Models\MasterReference;
use App\Http\Resources\FormdataResource;
use App\Http\Resources\SchoolResource;
use App\Http\Resources\UserResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;
use App\User; 
use Validator;

class SchoolController extends Controller
{
    protected $Sdp=null;
    protected $Sbmc=null;
    protected $Pta=null;
    protected $Grants=null;
    protected $Guard=null;
    protected $Ownership=null;
    /*Search Action
    Search variables 
    <variable:>schoolcode</variable:>
    <variable:>schoolname</variable:>
    <variable:>stateschoolcode</variable:>
    <variable:>email</variable:>
    <variable:>state</variable:>
    <variable:>lga</variable:>
    <variable:>yearfounded</variable:>
    <variable:>schooltype</variable:>
     */

     public function savetheme(Request $request){
        $validator = Validator::make($request->all(), [ 
            'state' => 'required', 
            'theme' => 'required', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['status'=>'error','message'=>$validator->errors()], 401);            
        }
        $state=State::where('Id_State',$request->state)->first();

        if($state!=null){
            $state->Theme=$request->theme;
            $state->save();
            return response()->json(['status'=>'success','message'=>'successfully updated']);
        }else{
            return response()->json(['status'=>'error','message'=>'no state with that id found']);
        }
        return response()->json(['status'=>'success','message'=>'successfully updated']);
        
     }

    public function showschoolform(Request $request){
        $sch=School::where('SchoolCode',$request->schoolcode)->first();
        
        if($sch->FormType=='private'){
            return view('currentFormPs');
        }else if($sch->FormType=='sciencevocational'){
            return view('currentFormVs');
        }else if( $sch->levelofeducation->Value=="Pre-primary and primary" || $sch->levelofeducation->Value=="Primary" || $sch->levelofeducation->Value=="Pre-primary"){
            return view('currentFormPP');
        }else if(($sch->FormType=='public' && $sch->levelofeducation->Value=="Junior and Senior Secondary")||($sch->FormType=='public' && $sch->levelofeducation->Value=="Junior Secondary") || ($sch->FormType=='public' && $sch->levelofeducation->Value=="Senior Secondary")){
            return view('currentFormPublic');
        }
        

    }

    public function showschoolformpreview(Request $request){
        $sch=School::where('SchoolCode',$request->schoolcode)->first();
        
        if($sch->FormType=='private'){
            return view('previewFormPs');
        }else if($sch->FormType=='sciencevocational'){
            return view('previewFormVs');
        }else if( $sch->levelofeducation->Value=="Pre-primary and primary" || $sch->levelofeducation->Value=="Primary" || $sch->levelofeducation->Value=="Pre-primary"){
            return view('previewFormPP');
        }else if(($sch->FormType=='public' && $sch->levelofeducation->Value=="Junior and Senior Secondary")||($sch->FormType=='public' && $sch->levelofeducation->Value=="Junior Secondary") || ($sch->FormType=='public' && $sch->levelofeducation->Value=="Senior Secondary")){
            return view('previewFormPublic');
        }
        

    }

    public function search(Request $request){
        $tcoll=array();
        $ctcoll=array();
        
        $authuser=Auth::user();
        //Prepare the search variables
        if ($request->input('schoolcode')!='') {
            $schoolcode = array('SchoolCode','LIKE',"%{$request->input('schoolcode')}%");
            $tcoll[]=$schoolcode;
        }
        if ($request->input('schoolname')!='') {
            $schoolname = array('SchoolName','LIKE',"%{$request->input('schoolname')}%");
            $tcoll[]=$schoolname;
            }
        if ($request->input('stateschoolcode')!='') {
            $stateschoolcode = array('StateSchoolCode','=',$request->input('stateschoolcode'));
            $tcoll[]=$stateschoolcode;
        }
        if ($request->input('email')!='') {
            $email = array('Email','LIKE',"%{$request->input('email')}%");
            $tcoll[]=$email;
        }
        
        //manages the strict access control for users 
        if($authuser->Id_Role==1){

            if ($request->input('state')!='') {
                $statename=$request->input('state');
                //get the id of the state with the statecode
                $state=State::where('State',$statename)->first();

                //This is a patch find a lasting solution to it later
                //$qpatch=$statecode+116;
                $state = array('Id_State','=',$state->Id_State);
                $tcoll[]=$state;
            }
        }else{
            
            $state = array('Id_State','=',$authuser->state_id);
            $tcoll[]=$state;
        }

        //lga
        if ($request->input('lga')!='') {
            $lgacode=$request->input('lga');
            //get the id of the lga with the lgacode
            $lga=Lga::where('LGA',$lgacode)->first();
            $lga = array('Id_LGA','=',$lga->id_LGA);
            $tcoll[]=$lga;
        }
        if ($request->input('schooltype')!='') {
            $schooltype = array('formtype','=',$request->input('schooltype'));
            $tcoll[]=$schooltype;
        }
        if ($request->input('schoollevel')!='') {
            $schoollevelcode=$request->input('schoollevel');
            //get the id of the lga with the lgacode
            $schoollevel=SlaveReference::where('Value',$schoollevelcode)->first();
            $schoollevel = array('LevelofEducation','=',$schoollevel->Id_SlaveReference);
            $tcoll[]=$schoollevel;
        }
        if ($request->input('yearfounded')!='') {
            $yearfounded = array('YearFounded','=',$request->input('yearfounded'));
            $tcoll[]=$yearfounded;
        }
        //ownership
        if ($request->input('operateshift')!='') {
            $schoollevel = array('OperateShift','=',$request->input('operateshift'));
            $tcoll[]=$schoollevel;
        }
        //authority of last inspection
        if ($request->input('locationofschool')!='') {
            $schoollevelcode=$request->input('locationofschool');
            //get the id of the lga with the lgacode
            $schoollevel=SlaveReference::where('Value',$schoollevelcode)->first();
            $schoollevel = array('Id_LocationType','=',$schoollevel->Id_SlaveReference);
            $tcoll[]=$schoollevel;
        }
        //types of school
        if ($request->input('typesofschool')!='') {
            $schoollevelcode=$request->input('typesofschool');
            //get the id of the lga with the lgacode
            $schoollevel=SlaveReference::where('Value',$schoollevelcode)->first();
            $schoollevel = array('Id_SchoolType','=',$schoollevel->Id_SlaveReference);
            $tcoll[]=$schoollevel;
        }
        
        $this->Sdp=$request->input('sdp');
        $this->Sbmc=$request->input('sbmc');
        $this->Pta=$request->input('pta');
        $this->Grants=$request->input('grants');
        $this->Guard=$request->input('guard');
        $this->Ownership=$request->input('ownership');

        //$schs=School::where($tcoll)->paginate(100);
        $schs=School::where($tcoll)->get();
        //get the schools and run and intersect
        $schools=$schs->reject(function($school){
            //check the school last year SC table
            $SdpBool=(($this->Sdp==null)?false:(($school->schoolcharacteristics()->count()!=0)?$school->schoolcharacteristics()->first()->HasSchoolDevelopmentPlan!=$this->Sdp:1));
            $SbmcBool=(($this->Sbmc==null)?false:(($school->schoolcharacteristics()->count()!=0)?$school->schoolcharacteristics()->first()->HasSchoolDevelopmentPlan!=$this->Sbmc:1));
            $PtaBool=(($this->Pta==null)?false:(($school->schoolcharacteristics()->count()!=0)?$school->schoolcharacteristics()->first()->HasSchoolDevelopmentPlan!=$this->Pta:1));
            $GrantsBool=(($this->Grants==null)?false:(($school->schoolcharacteristics()->count()!=0)?$school->schoolcharacteristics()->first()->HasSchoolDevelopmentPlan!=$this->Grants:1));
            $GuardBool=(($this->Guard==null)?false:(($school->schoolcharacteristics()->count()!=0)?$school->schoolcharacteristics()->first()->HasSchoolDevelopmentPlan!=$this->Guard:1));
            $OwnershipBool=(($this->Ownership==null)?false:(($school->schoolcharacteristics()->count()!=0)?(($school->schoolcharacteristics()->first()->Ownership)?$school->schoolcharacteristics()->first()->ownership->Value!=$this->Ownership:1):1));
            return $SdpBool || $SbmcBool || $PtaBool || $GrantsBool || $GuardBool || $OwnershipBool;
        });

        // $schs->load(['schoolcharacteristics'=>function($query){
        //     $query->where('HasSchoolGrant','0');
        // }]);


        //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
        //$authuser=Auth::user();
        //$create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>"","censusyear"=>"","category"=>"read","user_activity"=>"search school","notes"=>"Transaction successful"]);
        //return SchoolResource::collection($schools);
        //return response()->json(['data'=>SchoolResource::collection($schools),'links'=>json_encode(['first'=>'http://127.0.0.1:8000/api/search/schools?page=1','last'=>'http://127.0.0.1:8000/api/search/schools?page=1','prev'=>'null','next'=>'null']),'meta'=>json_encode(['current_page'=>1,'from'=>1,'last_page'=>1,'path'=>'http://127.0.0.1:8000/api/search/schools','per_page'=>100,'to'=>$schools->count(),'total'=>$schools->count()])]);
        return response()->json(['data'=>SchoolResource::collection($schools)]);
    }

    public function stateschools(Request $request){
        $tcoll=array();
    
        //the dataentry state
        $schs=School::where([['FormType','<>','private']])->get();
        //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
        $authuser=Auth::user();
        $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>"","censusyear"=>"","category"=>"read","user_activity"=>"search state schools","notes"=>"Transaction successful"]);
        return SchoolResource::collection($schs);
    }

    public function gsearch(Request $request){
        $tcoll=array();
        
        
        //Prepare the search variables
        if ($request->input('schoolcode')!='') {
            $schoolcode = array('SchoolCode','=',$request->input('schoolcode'));
            $tcoll[]=$schoolcode;
        }
        if ($request->input('schoolname')!='') {
            $schoolname = array('SchoolName','=',$request->input('schoolname'));
            $tcoll[]=$schoolname;
            }
        if ($request->input('stateschoolcode')!='') {
            $stateschoolcode = array('StateSchoolCode','=',$request->input('stateschoolcode'));
            $tcoll[]=$stateschoolcode;
        }
        if ($request->input('email')!='') {
            $email = array('Email','=',$request->input('email'));
            $tcoll[]=$email;
        }
        if ($request->input('state')!='') {
            $statecode=$request->input('state');
            //get the id of the state with the statecode
            $state=State::where('State',$statecode)->first();
            $state = array('Id_State','=',$state->Id_State);
            $tcoll[]=$state;
        }
        if ($request->input('lga')!='') {
            $lgacode=$request->input('lga');
            //get the id of the lga with the lgacode
            $lga=Lga::where('LGA',$lgacode)->first();
            $lga = array('Id_LGA','=',$lga->id_LGA);
            $tcoll[]=$lga;
        }
        if ($request->input('schooltype')!='') {
            $schooltype = array('formtype','=',$request->input('schooltype'));
            $tcoll[]=$schooltype;
        }
        if ($request->input('schoollevel')!='') {
            $schoollevelcode=$request->input('schoollevel');
            //get the id of the lga with the lgacode
            $schoollevel=SlaveReference::where('Value',$schoollevelcode)->first();
            $schoollevel = array('LevelofEducation','=',$schoollevel->Id_SlaveReference);
            $tcoll[]=$schoollevel;
            
        }
        if ($request->input('yearfounded')!='') {
            $yearfounded = array('YearFounded','=',$request->input('yearfounded'));
            $tcoll[]=$yearfounded;
        }
        
        $schs=School::where($tcoll)->paginate();
        //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
        //$authuser=Auth::user();
        //$create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>"","censusyear"=>"","category"=>"read","user_activity"=>"generic search of schools","notes"=>"Transaction successful"]);
        return SchoolResource::collection($schs);
    }

    public function genericsearch(Request $request){
        $tcoll=array();
        
        
        //Prepare the search variables
        if ($request->input('schoolcode')!='') {
            $schoolcode = array('SchoolCode','=',$request->input('schoolcode'));
            $tcoll[]=$schoolcode;
        }
        if ($request->input('schoolname')!='') {
            $schoolname = array('SchoolName','=',$request->input('schoolname'));
            $tcoll[]=$schoolname;
            }
        if ($request->input('stateschoolcode')!='') {
            $stateschoolcode = array('StateSchoolCode','=',$request->input('stateschoolcode'));
            $tcoll[]=$stateschoolcode;
        }
        if ($request->input('email')!='') {
            $email = array('Email','=',$request->input('email'));
            $tcoll[]=$email;
        }
        if ($request->input('state')!='') {
            $statecode=$request->input('state');
            //get the id of the state with the statecode
            $state=State::where('State',$statecode)->first();
            $state = array('Id_State','=',$state->Id_State);
            $tcoll[]=$state;
        }
        if ($request->input('lga')!='') {
            $lgacode=$request->input('lga');
            //get the id of the lga with the lgacode
            $lga=Lga::where('LGA',$lgacode)->first();
            $lga = array('Id_LGA','=',$lga->id_LGA);
            $tcoll[]=$lga;
        }
        if ($request->input('schooltype')!='') {
            $schooltype = array('formtype','=',$request->input('schooltype'));
            $tcoll[]=$schooltype;
        }
        if ($request->input('schoollevel')!='') {
            $schoollevelcode=$request->input('schoollevel');
            //get the id of the lga with the lgacode
            $schoollevel=SlaveReference::where('Value',$schoollevelcode)->first();
            $schoollevel = array('LevelofEducation','=',$schoollevel->Id_SlaveReference);
            $tcoll[]=$schoollevel;
            
        }
        if ($request->input('yearfounded')!='') {
            $yearfounded = array('YearFounded','=',$request->input('yearfounded'));
            $tcoll[]=$yearfounded;
        }
        
        $schs=School::where($tcoll)->paginate();
        //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
        //$authuser=Auth::user();
        //$create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"read","user_activity"=>"search schools","notes"=>"Transaction successful"]);
        return View('genericSearch',['request'=>$request]);
    }

    //Get school
    public function schoolform($schoolcode,$year){
        try{
            $sch=School::where('SchoolCode',$schoolcode)->firstorfail();
            
            //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
            $authuser=Auth::user();
            if($authuser->state_id==$sch->Id_State || $authuser->Id_Role==1){
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch->Id_School,"censusyear"=>$year,"category"=>"read","user_activity"=>"get school ".$sch->SchoolCode." data","notes"=>"Transaction successful"]);
                return new FormdataResource($sch);
            }else{
                return response()->json([['type'=>'error'],['message'=>'You are not authorized to view this school']]);
            }
            
        }catch(ModelNotFoundException $ex){
            return response()->json(['type'=>'error','message'=>'no school with that schoolcode found']);
        }        
    	//return 'gets the data if already entered for the year before or just the base data for the school that does not change and the meta data for the form';
    }

    public function registerschool(Request $request){
        if($request->filled('school_code')&&$request->filled('year')){
            $sch_model=School::where('SchoolCode',$request->school_code)->first();

            if($flag=DB::table('tblvar_schoolregistration')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])->exists()){
                //do nothing
                if(!$request->filled('register_year')&&$request->year!==$request->register_year){
                    //$school_privatecharacteristics=SchoolRegistration::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year],['Registered'=>'0']);                   
                    DB::table('tblvar_schoolregistration')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])
                ->update(['Registered'=>0]);
                }else{
                    DB::table('tblvar_schoolregistration')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year])
                    ->update(['Registered'=>1]);
                }
            }else{
                if($request->filled('register_year')){
                    $school_privatecharacteristics=SchoolRegistration::Create(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->register_year,'Registered'=>'1']);                   

                }else{
                    return response()->json(['status'=>'error','message'=>'The school is not registered for this census year, please register the school']);

                }
            }
            //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
            $authuser=Auth::user();
            $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"register school for census year","notes"=>"Transaction successful"]);
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=>'The school is not registered for this census year, please register the school']);
        }
    }

    public function removeschoollevel(Request $request){
        // $authuser=Auth::user();
        // if($authuser->state_id==$sch->Id_State || $authuser->Id_Role==1){

            
        
        // }else{
        //     return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
        // }
        if($request->filled('school_code')&& $request->filled('year')){

            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $level_ids=array('Pre-primary'=>'905','Primary'=>'906','Junior Secondary'=>'907','Senior Secondary'=>'908');
            $level_ref=$level_ids[$request->level];
            if($flag=DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])->exists()){
                DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])
                ->update(['IsActive'=>0]);
            }else{
                //$school_privatecharacteristics=PrivateSchoolCharacteristic::Create(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref,'YearEstablished'=>$request->year_of_establishment_preprimary]);                   
            }
            //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
            $authuser=Auth::user();
            $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"delete","user_activity"=>"remove school level","notes"=>"Transaction successful"]);
            return response()->json(['status'=>'success']);
        }
    }

    public function addschool(Request $request){
        $authuser=Auth::user();
        
        if($request->filled('school_name')&& $request->filled('school_type')&& $request->filled('state')){
            if($authuser->state->State==$request->filled('state') || $authuser->Id_Role==1){
                $sch_model=new School;
                //generete schoolcode
                $schoolcode='';
                $statecode='';
                $levelcode='';
                $serialno='';
    
                  //sort the state code out
                if ($request->filled('state')) {
                    
                    //get the id of the state with the lgacode
                    $state=State::where('State',$request->state)->first();
                    $statecode=$state->StateCode;
                    $sch_model->Id_State=$state->Id_State;
                }
    
                if ($request->input('lga')!='') {
                    $lgacode=$request->input('lga');
                    //get the id of the lga with the lgacode
                    $lga=Lga::where('LGA',$lgacode)->first();
                    $sch_model->Id_LGA=$lga->id_LGA;
                    
                }
                $sch_model->SchoolName=($request->filled('school_name'))?$request->school_name:$sch_model->SchoolName;
                $sch_model->SchoolAddress=($request->filled('school_address'))?$request->school_address:$sch_model->SchoolAddress;
                $sch_model->Town=($request->filled('town'))?$request->town:$sch_model->Town;
                $sch_model->Telephone=($request->filled('telephone'))?$request->telephone:$sch_model->Telephone;
                $sch_model->Ward=($request->filled('ward'))?$request->ward:$sch_model->Ward;
                $sch_model->YearFounded=($request->filled('year'))?$request->year:$sch_model->YearFounded;
                $sch_model->Email=($request->filled('email'))?$request->email:$sch_model->Email;

                if($request->school_type=='private'){
                    //$sch->LevelofEducation=903;
                    $slaveref=SlaveReference::where('Value','Private School')->where('Id_MasterReference','5')->first();
                    $sch_model->LevelofEducation=$slaveref->Id_SlaveReference;
                    //add to the school private characteristics
                    $sch_model->FormType='private';

                    //hardcoded, should be changed
                    $sch_model->Id_SectorCode=23;
                }elseif($request->school_type=='sciencevocational'){
                    //$sch->LevelofEducation=904;
                    $slaveref=SlaveReference::where('Value',$request->schoollevel)->where('Id_MasterReference','5')->first();
                    $sch_model->LevelofEducation=$slaveref->Id_SlaveReference;
                    $sch_model->FormType='sciencevocational';
                     //hardcoded, should be changed
                     $sch_model->Id_SectorCode=22;
                }elseif($request->school_type=='public'){
                    //$sch->LevelofEducation=903;
                    $slaveref=SlaveReference::where('Value',$request->schoollevel)->where('Id_MasterReference','5')->first();
                    $sch_model->LevelofEducation=$slaveref->Id_SlaveReference;
                    $sch_model->FormType='public';
                    //hardcoded, should be changed
                    $sch_model->Id_SectorCode=22;
                }
    
                //sort the levelcode
                $level_ids=array('Pre-primary'=>'PP','Primary'=>'P','Pre-primary and primary'=>'PPP','Junior Secondary'=>'J','Senior Secondary'=>'S','Junior and Senior Secondary'=>'JS');
                $levelcode='';
                if($request->school_type=='private'){
                    $levelcode='PRV';
                }else{
                    $levelcode=$level_ids[$request->schoollevel];
                }

                //get the serial no by no of state and level
                $mCount=School::where('Id_State',$sch_model->Id_State)->where('LevelofEducation',$sch_model->LevelofEducation)->count();
                $serialno=$mCount+1;
    
                if($serialno<10){
                    $schoolcode=$statecode.$levelcode.'000'.$serialno;
                }elseif($serialno<100){
                    $schoolcode=$statecode.$levelcode.'00'.$serialno;
                }elseif($serialno<1000){
                    $schoolcode=$statecode.$levelcode.'0'.$serialno;
                }
                $sch_model->SchoolCode=$schoolcode;
    
                //create a new school
                $sch_model->save();
                //$sch->SchoolName=$request->schoolname;
                //$sch->SchoolAddress=$request->schooladdress;
                if($request->school_type=='private'){
                       //loop through the array
                       if($request->filled('level_of_educations')){
                        $level_names=$request->input('level_of_educations.*');
                        $level_ids=array('Pre-primary'=>'905','Primary'=>'906','Junior Secondary'=>'907','Senior Secondary'=>'908');
                    foreach($level_names as $name){
                        if($name!=''){
                            //save or update
                            //$slaveref=SlaveReference::where('Value',$name)->first();
                            //$level_ref=$level_ids[$name];
                            $slaveref=SlaveReference::where('Value',$name)->where('Id_MasterReference','5')->first();
                            $level_ref=$slaveref->Id_SlaveReference;
                            if($name=='Pre-primary'){
                                if($flag=DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])->exists()){
                                    DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])
                                    ->update(['IsActive'=>'1']);
                                }else{
                                    $school_privatecharacteristics=PrivateSchoolCharacteristic::Create(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref,'IsActive'=>1]);                   
                                }
                            }elseif($name=='Primary'){
                                if($flag=DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])->exists()){
                                    DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])
                                    ->update(['IsActive'=>'1']);
                                }else{
                                    $school_privatecharacteristics=PrivateSchoolCharacteristic::Create(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref,'IsActive'=>1]);                   
                                }
                            }elseif($name=='Junior Secondary'){
                                if($flag=DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])->exists()){
                                    DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])
                                    ->update(['IsActive'=>'1']);
                                }else{
                                    $school_privatecharacteristics=PrivateSchoolCharacteristic::Create(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref,'IsActive'=>1]);                   
                                }
                            }elseif($name=='Senior Secondary'){
                                if($flag=DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])->exists()){
                                    DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])
                                    ->update(['IsActive'=>'1']);
                                }else{
                                    $school_privatecharacteristics=PrivateSchoolCharacteristic::Create(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref,'IsActive'=>1]);                   
                                }
                            }
                            
                        }
                    }
                }
                }
                
                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                //$authuser=Auth::user();
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>"","category"=>"write","user_activity"=>"add new school","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success','schoolcode'=>$schoolcode]);
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }

        }else{
            return response()->json(['type'=>'error','message'=>'required fields are missing']);
        }
    }

    public function saveschoolcharacteristics(Request $request){
        //$authuser=Auth::user();
        // if($authuser->state_id==$sch->Id_State || $authuser->Id_Role==1){

        // }else{
        //     return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
        // }
        if($request->filled('school_code')&&$request->filled('year')){
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            //check if private
            if($sch_model->FormType=='private'){
                //sort the code out first
                if ($request->filled('recognition_status')) {
                    $recognitionstatus=$request->recognition_status;
                    //get the id of the status with the statuscode
                    $slaveref=SlaveReference::where('Value',$recognitionstatus)->first();
                    
                    $sch_model->RecognitionStatus=$slaveref->Id_SlaveReference;
                }

                //sort the code out first
                if ($request->filled('location_type')) {
                    $locationtype=$request->location_type;
                    //get the id of the status with the statuscode
                    $slaveref=SlaveReference::where('Value',$locationtype)->first();
                    $sch_model->Id_LocationType=$slaveref->Id_SlaveReference;
                }

                //loop through the array
                if($request->filled('level_of_educations')){
                    $level_names=$request->input('level_of_educations.*');
                    $level_ids=array('Pre-primary'=>'905','Primary'=>'906','Junior Secondary'=>'907','Senior Secondary'=>'908');
                foreach($level_names as $name){
                    if($name!=''){
                        //save or update
                        //$slaveref=SlaveReference::where('Value',$name)->first();
                        $level_ref=$level_ids[$name];
                        if($name=='Pre-primary'){
                            if($flag=DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])->exists()){
                                DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])
                                ->update(['YearEstablished'=>$request->year_of_establishment_preprimary,'IsActive'=>'1']);
                            }else{
                                $school_privatecharacteristics=PrivateSchoolCharacteristic::Create(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref,'YearEstablished'=>$request->year_of_establishment_preprimary,'IsActive'=>1]);                   
                            }
                        }elseif($name=='Primary'){
                            if($flag=DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])->exists()){
                                DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])
                                ->update(['YearEstablished'=>$request->year_of_establishment_primary,'IsActive'=>'1']);
                            }else{
                                $school_privatecharacteristics=PrivateSchoolCharacteristic::Create(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref,'YearEstablished'=>$request->year_of_establishment_primary,'IsActive'=>1]);                   
                            }
                        }elseif($name=='Junior Secondary'){
                            if($flag=DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])->exists()){
                                DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])
                                ->update(['YearEstablished'=>$request->year_of_establishment_jss,'IsActive'=>'1']);
                            }else{
                                $school_privatecharacteristics=PrivateSchoolCharacteristic::Create(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref,'YearEstablished'=>$request->year_of_establishment_jss,'IsActive'=>1]);                   
                            }
                        }elseif($name=='Senior Secondary'){
                            if($flag=DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])->exists()){
                                DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref])
                                ->update(['YearEstablished'=>$request->year_of_establishment_ss,'IsActive'=>'1']);
                            }else{
                                $school_privatecharacteristics=PrivateSchoolCharacteristic::Create(['Id_School'=>$sch_model->Id_School,'SchoolCode'=>$sch_model->SchoolCode,'SchoolLevel'=>$level_ref,'YearEstablished'=>$request->year_of_establishment_ss,'IsActive'=>1]);                   
                            }
                        }
                        
                    }
                }
            }
                //sort the code out first
                if ($request->filled('school_type')) {
                    $schooltype=$request->school_type;
                    //get the id of the status with the statuscode
                    $slaveref=SlaveReference::where('Value',$schooltype)->first();
                    $sch_model->Id_SchoolType=$slaveref->Id_SlaveReference;
                }
        
                
                //save to the school characteristics table
                if($flag=DB::table('tblvar_schoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year])->exists()){
                    
                    DB::table('tblvar_schoolcharacteristics')
                    ->where(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year])
                    ->update(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'InspectionNo'=>($request->filled('no_of_inspection'))?$request->no_of_inspection:null,'OwnershipStatus'=>($request->filled('owner'))?SlaveReference::where('Value',$request->owner)->first()->Id_SlaveReference:null,'SharedFacilities'=>($request->filled('shared_facilities'))?$request->shared_facilities:null,'SchoolsSharingWith'=>($request->filled('schools_sharingwith'))?$request->schools_sharingwith:null,'MultiGradeTeaching'=>($request->filled('multi_grade_teaching'))?$request->multi_grade_teaching:null,'DistanceFromCatchmentArea'=>($request->filled('distance_from_catchment_area'))?$request->distance_from_catchment_area:null,'DistanceFromLGA'=>($request->filled('distance_from_lga'))?$request->distance_from_lga:null,'StudentsTravelling3km'=>($request->filled('students_travelling_3km'))?$request->students_travelling_3km:null,'IsBoarding'=>($request->filled('isboarding'))?$request->isboarding:null,'MaleStudentsBoardingAtSchoolPremises'=>($request->filled('male_student_b'))?$request->male_student_b:null,'FemaleStudentsBoardingAtSchoolPremises'=>($request->filled('female_student_b'))?$request->female_student_b:null,'HasSchoolDevelopmentPlan'=>($request->filled('has_school_development_plan'))?$request->has_school_development_plan:null,'HasSBMC'=>($request->filled('has_sbmc'))?$request->has_sbmc:null,'HasPTA'=>($request->filled('has_pta'))?$request->has_pta:null,'IsPSA'=>($request->filled('is_psa'))?$request->is_psa:null,'LastInspectionDate'=>($request->filled('last_inspection_date'))?$request->last_inspection_date:null,'ConditionalCashTransfer'=>($request->filled('conditional_cash_transfer'))?$request->conditional_cash_transfer:null,'HasSchoolGrant'=>($request->filled('has_school_grant'))?$request->has_school_grant:null,'HasSecurityGuard'=>($request->filled('has_security_guard'))?$request->has_security_guard:null,'LastInspectionAuthority'=>($request->filled('last_inspection_authority'))?(SlaveReference::where('Value',$request->last_inspection_authority)->first())->Id_SlaveReference:null,'Ownership'=>($request->filled('ownership'))?(SlaveReference::where('Value',$request->ownership)->first())->Id_SlaveReference:null]);
                }else{
                    $school_characteristics=SchoolCharacteristic::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year],['InspectionNo'=>($request->filled('no_of_inspection'))?$request->no_of_inspection:null,'SharedFacilities'=>($request->filled('shared_facilities'))?$request->shared_facilities:null,'SchoolsSharingWith'=>($request->filled('schools_sharingwith'))?$request->schools_sharingwith:null,'MultiGradeTeaching'=>($request->filled('multi_grade_teaching'))?$request->multi_grade_teaching:null,'DistanceFromCatchmentArea'=>($request->filled('distance_from_catchment_area'))?$request->distance_from_catchment_area:null,'DistanceFromLGA'=>($request->filled('distance_from_lga'))?$request->distance_from_lga:null,'StudentsTravelling3km'=>($request->filled('students_travelling_3km'))?$request->students_travelling_3km:null,'IsBoarding'=>($request->filled('isboarding'))?$request->isboarding:null,'MaleStudentsBoardingAtSchoolPremises'=>($request->filled('male_student_b'))?$request->male_student_b:null,'FemaleStudentsBoardingAtSchoolPremises'=>($request->filled('female_student_b'))?$request->female_student_b:null,'HasSchoolDevelopmentPlan'=>($request->filled('has_school_development_plan'))?$request->has_school_development_plan:null,'HasSBMC'=>($request->filled('has_sbmc'))?$request->has_sbmc:null,'HasPTA'=>($request->filled('has_pta'))?$request->has_pta:null,'IsPSA'=>($request->filled('is_psa'))?$request->is_psa:null,'LastInspectionDate'=>($request->filled('last_inspection_date'))?$request->last_inspection_date:null,'ConditionalCashTransfer'=>($request->filled('conditional_cash_transfer'))?$request->conditional_cash_transfer:null,'HasSchoolGrant'=>($request->filled('has_school_grant'))?$request->has_school_grant:null,'HasSecurityGuard'=>($request->filled('has_security_guard'))?$request->has_security_guard:null,'LastInspectionAuthority'=>($request->filled('last_inspection_authority'))?(SlaveReference::where('Value',$request->last_inspection_authority)->first())->Id_SlaveReference:null,'Ownership'=>($request->filled('ownership'))?(SlaveReference::where('Value',$request->ownership)->first())->Id_SlaveReference:null]);
                }

                //save for pre primary
                if($request->filled('preprimary_year_established')){
                    if($flag=DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'SchoolLevel'=>'46'])->exists()){
                        DB::table('tblbse_privateschoolcharacteristics')
                        ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'SchoolLevel'=>'46'])
                        ->update(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'YearEstablished'=>$request->preprimary_year_established,'SchoolLevel'=>'46']);
                    }else{
                        $school_privatecharacteristics=PrivateSchoolCharacteristic::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year],['YearEstablished'=>$request->preprimary_year_established,'SchoolLevel'=>'46']);}
                }
                
                //save for primary
                if($request->filled('primary_year_established')){
                    if($flag=DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'SchoolLevel'=>'47'])->exists()){
                        DB::table('tblbse_privateschoolcharacteristics')
                        ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'SchoolLevel'=>'47'])
                        ->update(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'YearEstablished'=>$request->primary_year_established,'SchoolLevel'=>'47']);
                    }else{
                        $school_privatecharacteristics=PrivateSchoolCharacteristic::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year],['YearEstablished'=>$request->primary_year_established,'SchoolLevel'=>'47']);
                    }
                }
                //save for junior
                if($request->filled('js_year_established')){
                    if($flag=DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'SchoolLevel'=>'98'])->exists()){
                        DB::table('tblbse_privateschoolcharacteristics')
                        ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'SchoolLevel'=>'98'])
                        ->update(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'YearEstablished'=>$request->js_year_established,'SchoolLevel'=>'98']);
                    }else{
                        $school_privatecharacteristics=PrivateSchoolCharacteristic::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year],['YearEstablished'=>$request->js_year_established,'SchoolLevel'=>'98']);
                    }
                }
                //save for secondary
                if($request->filled('ss_year_established')){
                    if($flag=DB::table('tblbse_privateschoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'SchoolLevel'=>'48'])->exists()){
                        DB::table('tblbse_privateschoolcharacteristics')
                        ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'SchoolLevel'=>'48'])
                        ->update(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'YearEstablished'=>$request->ss_year_established,'SchoolLevel'=>'48']);
                    }else{
                        $school_privatecharacteristics=PrivateSchoolCharacteristic::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year],['YearEstablished'=>$request->ss_year_established,'SchoolLevel'=>'48']);
                    }
                }
                $sch_model->save();
                $authuser=Auth::user();
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save school characteristics","notes"=>"Transaction successful"]);
                return response()->json(['status'=>'success']);
            }
            else{
                //public 
                //sort the code out first
                if ($request->filled('recognition_status')) {
                    $recognitionstatus=$request->recognition_status;
                    //get the id of the status with the statuscode
                    $slaveref=SlaveReference::where('Value',$recognitionstatus)->first();
                    $sch_model->Status=$slaveref->Id_SlaveReference;
                }

                //sort the code out first
                if ($request->filled('location_type')) {
                    $locationtype=$request->location_type;
                    //get the id of the status with the statuscode
                    $slaveref=SlaveReference::where('Value',$locationtype)->first();
                    $sch_model->Id_LocationType=$slaveref->Id_SlaveReference;
                }

                //sort the code out first
                if ($request->filled('level_of_education')) {
                    $levelofeducation=$request->level_of_education;
                    //get the id of the status with the statuscode
                    $slaveref=SlaveReference::where('Value',$levelofeducation)->where('Id_MasterReference','5')->first();
                    $sch_model->LevelofEducation=$slaveref->Id_SlaveReference;
                }

                //sort the code out first
                if ($request->filled('school_type')) {
                    $schooltype=$request->school_type;
                    //get the id of the status with the statuscode
                    $slaveref=SlaveReference::where('Value',$schooltype)->first();
                    $sch_model->Id_SchoolType=$slaveref->Id_SlaveReference;
                }
        
                //sort the code out first
                if ($request->filled('year_of_establishment')) {
                    $sch_model->YearFounded=$request->year_of_establishment;
                }

                if($flag=DB::table('tblvar_schoolcharacteristics')->where(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year])->exists()){
                    DB::table('tblvar_schoolcharacteristics')
                    ->where(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year])
                    ->update(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'InspectionNo'=>($request->filled('no_of_inspection'))?$request->no_of_inspection:null,'HasSecurityGuard'=>($request->filled('has_security_guard'))?$request->has_security_guard:null,'IsPSA'=>($request->filled('is_psa'))?$request->is_psa:null,'SharedFacilities'=>($request->filled('shared_facilities'))?$request->shared_facilities:null,'SchoolsSharingWith'=>($request->filled('schools_sharingwith'))?$request->schools_sharingwith:null,'MultiGradeTeaching'=>($request->filled('multi_grade_teaching'))?$request->multi_grade_teaching:null,'DistanceFromCatchmentArea'=>($request->filled('distance_from_catchment_area'))?$request->distance_from_catchment_area:null,'DistanceFromLGA'=>($request->filled('distance_from_lga'))?$request->distance_from_lga:null,'StudentsTravelling3km'=>($request->filled('students_travelling_3km'))?$request->students_travelling_3km:null,'IsBoarding'=>($request->filled('isboarding'))?$request->isboarding:null,'MaleStudentsBoardingAtSchoolPremises'=>($request->filled('male_student_b'))?$request->male_student_b:null,'FemaleStudentsBoardingAtSchoolPremises'=>($request->filled('female_student_b'))?$request->female_student_b:null,'HasSchoolDevelopmentPlan'=>($request->filled('has_school_development_plan'))?$request->has_school_development_plan:null,'HasSBMC'=>($request->filled('has_sbmc'))?$request->has_sbmc:null,'HasPTA'=>($request->filled('has_pta'))?$request->has_pta:null,'IsPSA'=>($request->filled('is_psa'))?$request->is_psa:null,'LastInspectionDate'=>($request->filled('last_inspection_date'))?$request->last_inspection_date:null,'InspectionNo'=>($request->filled('no_of_inspection'))?$request->no_of_inspection:null,'ConditionalCashTransfer'=>($request->filled('conditional_cash_transfer'))?$request->conditional_cash_transfer:null,'HasSchoolGrant'=>($request->filled('has_school_grant'))?$request->has_school_grant:null,'HasSecurityGuard'=>($request->filled('has_security_guard'))?$request->has_security_guard:null,'LastInspectionAuthority'=>($request->filled('last_inspection_authority'))?(SlaveReference::where('Value',$request->last_inspection_authority)->first())->Id_SlaveReference:null,'Ownership'=>($request->filled('ownership'))?(SlaveReference::where('Value',$request->ownership)->first())->Id_SlaveReference:null]);
                }else{
                    $school_characteristics=SchoolCharacteristic::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year],['InspectionNo'=>($request->filled('no_of_inspection'))?$request->no_of_inspection:null,'HasSecurityGuard'=>($request->filled('has_security_guard'))?$request->has_security_guard:null,'IsPSA'=>($request->filled('is_psa'))?$request->is_psa:null,'SharedFacilities'=>($request->filled('shared_facilities'))?$request->shared_facilities:null,'SchoolsSharingWith'=>($request->filled('schools_sharingwith'))?$request->schools_sharingwith:null,'MultiGradeTeaching'=>($request->filled('multi_grade_teaching'))?$request->multi_grade_teaching:null,'DistanceFromCatchmentArea'=>($request->filled('distance_from_catchment_area'))?$request->distance_from_catchment_area:null,'DistanceFromLGA'=>($request->filled('distance_from_lga'))?$request->distance_from_lga:null,'StudentsTravelling3km'=>($request->filled('students_travelling_3km'))?$request->students_travelling_3km:null,'IsBoarding'=>($request->filled('isboarding'))?$request->isboarding:null,'MaleStudentsBoardingAtSchoolPremises'=>($request->filled('male_student_b'))?$request->male_student_b:null,'FemaleStudentsBoardingAtSchoolPremises'=>($request->filled('female_student_b'))?$request->female_student_b:null,'HasSchoolDevelopmentPlan'=>($request->filled('has_school_development_plan'))?$request->has_school_development_plan:null,'HasSBMC'=>($request->filled('has_sbmc'))?$request->has_sbmc:null,'HasPTA'=>($request->filled('has_pta'))?$request->has_pta:null,'IsPSA'=>($request->filled('is_psa'))?$request->is_psa:null,'LastInspectionDate'=>($request->filled('last_inspection_date'))?$request->last_inspection_date:null,'InspectionNo'=>($request->filled('no_of_inspection'))?$request->no_of_inspection:null,'ConditionalCashTransfer'=>($request->filled('conditional_cash_transfer'))?$request->conditional_cash_transfer:null,'HasSchoolGrant'=>($request->filled('has_school_grant'))?$request->has_school_grant:null,'HasSecurityGuard'=>($request->filled('has_security_guard'))?$request->has_security_guard:null,'LastInspectionAuthority'=>($request->filled('last_inspection_authority'))?(SlaveReference::where('Value',$request->last_inspection_authority)->first())->Id_SlaveReference:null,'Ownership'=>($request->filled('ownership'))?(SlaveReference::where('Value',$request->ownership)->first())->Id_SlaveReference:null]);
                }
                
                //save school shifts
                $sch_model->OperateShift=($request->filled('shift'))?$request->shift:null;
                $sch_model->YearFounded=$request->year_of_establishment;
                $sch_model->save();
                $authuser=Auth::user();
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save school characteristics","notes"=>"Transaction successful"]);
                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                return response()->json(['status'=>'success']);
            }
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
        
    }

    public function setcensusyear(Request $request){
        //validate
        $validator = Validator::make($request->all(), [ 
            'old_censusyear' => 'required',
            'new_censusyear' => 'required',
        ]);
        if ($validator->fails()) { 
                    return response()->json(['error'=>$validator->errors()], 401);            
        }
        $oldcensusyear=CensusYear::where('Year',$request->old_censusyear)->first();
        $oldcensusyear->Active=0;
        $oldcensusyear->save();

        $newcensusyear=CensusYear::updateOrCreate(['Year'=>$request->new_censusyear],['Active'=>1]);
        //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
        $authuser=Auth::user();
        $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>"","censusyear"=>"","category"=>"write","user_activity"=>"set census year","notes"=>"Transaction successful"]);
        return response()->json(['status'=>'success']);
    }

    public function getcensusyear(){
        $censusyear=CensusYear::where('Active','1')->first();
        //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
        //$authuser=Auth::user();
        //$create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>"","censusyear"=>"","category"=>"read","user_activity"=>"get census year","notes"=>"Transaction successful"]);
        return $censusyear;
    }

}