<?php

namespace App\Http\Controllers;
use App\Models\School;
use App\Models\StudentBySubject;
use App\Models\SlaveReference;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;
use Illuminate\Http\Request;

class StudentBySubjectController extends Controller
{
    public function savesubjects(Request $request){
        
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of 
                    //remember to check if input value maps with the items in the array
                    if($request->filled('students_by_subject')){
                        $students_by_subject=$request->input('students_by_subject.*');
                        foreach($students_by_subject as $name){
                            if($name!='' && $request->school_class=='jss'){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $subject_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array

                                //jss1
                                if($flag=DB::table('tblvar_studentbysubject')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbysubject')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref])
                                    ->update(['Male'=>(array_key_exists('jss1_pupils_male', $name))?$name["jss1_pupils_male"]:null,'Female'=>$name["jss1_pupils_female"]]);
                                }else{
                                    $sbs_model=StudentBySubject::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref,'Male'=>$name["jss1_pupils_male"],'Female'=>$name["jss1_pupils_female"]]);
                                }

                                //jss2
                                if($flag=DB::table('tblvar_studentbysubject')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbysubject')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref])
                                    ->update(['Male'=>$name["jss2_pupils_male"],'Female'=>$name["jss2_pupils_female"]]);
                                }else{
                                    $sbs_model=StudentBySubject::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref,'Male'=>$name["jss2_pupils_male"],'Female'=>$name["jss2_pupils_female"]]);
                                }

                                //jss3
                                if($flag=DB::table('tblvar_studentbysubject')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbysubject')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref])
                                    ->update(['Male'=>$name["jss3_pupils_male"],'Female'=>$name["jss3_pupils_female"]]);
                                }else{
                                    $sbs_model=StudentBySubject::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref,'Male'=>$name["jss3_pupils_male"],'Female'=>$name["jss3_pupils_female"]]);
                                }

                                
                            }else if($name!='' && $request->school_class=='sss'){

                                //save or update
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $subject_ref=$slaveref->Id_SlaveReference;

                                
                                //sss1
                                if($flag=DB::table('tblvar_studentbysubject')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbysubject')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref])
                                    ->update(['Male'=>(array_key_exists('sss1_pupils_male', $name))?$name["sss1_pupils_male"]:null,'Female'=>$name["sss1_pupils_female"]]);
                                }else{
                                    $sbs_model=StudentBySubject::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref,'Male'=>$name["sss1_pupils_male"],'Female'=>$name["sss1_pupils_female"]]);
                                }

                                //sss2
                                if($flag=DB::table('tblvar_studentbysubject')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbysubject')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref])
                                    ->update(['Male'=>$name["sss2_pupils_male"],'Female'=>$name["sss2_pupils_female"]]);
                                }else{
                                    $sbs_model=StudentBySubject::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref,'Male'=>$name["sss2_pupils_male"],'Female'=>$name["sss2_pupils_female"]]);
                                }

                                //sss3
                                if($flag=DB::table('tblvar_studentbysubject')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbysubject')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref])
                                    ->update(['Male'=>$name["sss3_pupils_male"],'Female'=>$name["sss3_pupils_female"]]);
                                }else{
                                    $sbs_model=StudentBySubject::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref,'Male'=>$name["sss3_pupils_male"],'Female'=>$name["sss3_pupils_female"]]);
                                }
                            }
                        }
                    }else{
                        return response()->json(['status'=>'error','message'=>'The health facilities was not provided']);
                    }
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                    
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save student by subjects","notes"=>"Transaction successful"]);

                    return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }
}
