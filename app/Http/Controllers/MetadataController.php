<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SlaveReference;
use App\Models\MasterReference;
use App\Http\Resources\MetadataResource;
use App\Http\Resources\MasterReferenceResource;
use App\Http\Resources\MetadataCollection;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;
use App\Http\Resources\StateResource;
use App\Models\State;

class MetadataController extends Controller
{
    //get search metadata
    public function searchmetadata(){
		$search_metadata=SlaveReference::whereIn('Id_MasterReference',[5,6])->orderBy('Id_MasterReference')->get();
		//$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
        return new MetadataCollection($search_metadata);
	}
	
    //get school characteristics metadata for the form specified
    public function schoolcharacteristicsmetadata($formtype){
    	switch ($formtype) {
    		case 'jss':
                $form_metadata=SlaveReference::find([33,34,98,48,19,276,685,686,687,685,686,687,688,689,690,691,692,693,880]);
                MetadataResource::withoutWrapping();
                return MetadataResource::collection($form_metadata);
    			break;
    		case 'preprimary':
                $form_metadata=SlaveReference::find([33,34,46,47,19,21,276,685,686,687,685,686,687,688,689,690,691,692,693,880]);
                MetadataResource::withoutWrapping();
                return MetadataResource::collection($form_metadata);
    			break;
    		case 'private':
    			$form_metadata=SlaveReference::find([33,34,46,47,98,48,19,276,554,21,685,686,687,671,672,673,674,675,676,689,690,691,692,693,880]);
    			MetadataResource::withoutWrapping();
                return MetadataResource::collection($form_metadata);
    			break;
    		case 'sciencevocational':
    			$form_metadata=SlaveReference::find([33,34,98,48,19,276,685,686,687,685,686,687,688,689,690,691,692,693,880]);
    			MetadataResource::withoutWrapping();
                return MetadataResource::collection($form_metadata);
    			break;
    		case 'sss':
    			$form_metadata=SlaveReference::find([33,34,98,48,19,276,685,686,687,688,689,690,691,692,693,880]);
    			MetadataResource::withoutWrapping();
                return MetadataResource::collection($form_metadata);
    			break;
    		default:
    			return "Wrong form selection";
    			break;
    	}

	}

	//metadata config
	public function getmetadatas(){
		$metadatas=MasterReference::all();
		//$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
		//$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
		$authuser=Auth::user();
		$create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>"","censusyear"=>"","category"=>"read","user_activity"=>"get metadata","notes"=>"Transaction successful"]);
		return MasterReferenceResource::collection($metadatas);
	}
	
	public function savemetadatas(Request $request){
		if($request->filled('metadatas') && $request->filled('category')){
			$metadatas=$request->input('metadatas.*');
			$mref=MasterReference::where('ReferenceType',$request->category)->first();
            $master_ref=$mref->Id_MasterReference;
			foreach($metadatas as $name){
				if($name["id"]!=''){
					
					if($flag=DB::table('tblmtd_slavereference')->where(['Id_SlaveReference'=>$name["id"],'Id_MasterReference'=>$master_ref])->exists()){
                        DB::table('tblmtd_slavereference')
                        ->where(['Id_SlaveReference'=>$name["id"],'Id_MasterReference'=>$master_ref])
                        ->update(['Id_MasterReference'=>$master_ref,'Value'=>($name["value"]!=null)?$name["value"]:null,'DisplayValue'=>($name["display"]!=null)?$name["display"]:null,'Description'=>($name["description"]!=null)?$name["description"]:null,'MetadataOrder'=>($name["order"]!=null)?$name["order"]:null,'Level'=>($name["level"]!=null)?$name["level"]:null]);
                    }else{
                        $_model=SlaveReference::create(['Id_MasterReference'=>$master_ref,'Value'=>($name["value"]!=null)?$name["value"]:null,'DisplayValue'=>($name["display"]!=null)?$name["display"]:null,'Description'=>($name["description"]!=null)?$name["description"]:null,'MetadataOrder'=>($name["order"]!=null)?$name["order"]:null,'Level'=>($name["level"]!=null)?$name["level"]:null]);
                    }
				}else{
					
                        $_model=SlaveReference::create(['Id_MasterReference'=>$master_ref,'Value'=>($name["value"]!=null)?$name["value"]:null,'DisplayValue'=>($name["display"]!=null)?$name["display"]:null,'Description'=>($name["description"]!=null)?$name["description"]:null,'MetadataOrder'=>($name["order"]!=null)?$name["order"]:null,'Level'=>($name["level"]!=null)?$name["level"]:null]);
				}
			}
		}else{
			return response()->json(['status'=>'error','message'=>'The metadatas was not provided']);
		}
		//$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
		$authuser=Auth::user();
		$create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>"","censusyear"=>"","category"=>"write","user_activity"=>"save metadata","notes"=>"Transaction successful"]);
		return response()->json(['status'=>'success','message'=>'The Post was successful']);
	}
	
	public function deletemetadatas(Request $request){
        if($request->filled('metadata_id')){
			//soft delete
			//SlaveReference::destroy($request->metadata_id);
			$sref=SlaveReference::find($request->metadata_id);
			$sref->IsDeleted=1;
			$sref->save();
			//$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);        
			$authuser=Auth::user();
            $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"delete","user_activity"=>"delete metadata","notes"=>"Transaction successful"]);
			return response()->json(['status'=>'success','message'=>'The Post was successful']);
		}else{
			return response()->json(['status'=>'error','message'=>'The metadata_id was not provided']);
		}
    }
	
	public function all_metadata(){
		$metadata_coll=array();
		$form_preprimary_primary_metadata=(new MetadataCollection(SlaveReference::whereIn('Level',[1,2,5,0])->where(['IsDeleted'=>'0'])->orderBy('Id_MasterReference')->get()));
		$form_secondary_metadata=(new MetadataCollection(SlaveReference::whereIn('Level',[3,4,6,0])->where(['IsDeleted'=>'0'])->orderBy('Id_MasterReference')->get()));
		$form_private_metadata=(new MetadataCollection(SlaveReference::whereIn('Level',[1,2,3,4,0])->where(['IsDeleted'=>'0'])->orderBy('Id_MasterReference')->get()));
		$states=StateResource::collection(State::all());
		return response()->json(['Pp'=>$form_preprimary_primary_metadata,'Secondary'=>$form_secondary_metadata,'Private'=>$form_private_metadata,'States'=>$states]);
    }
}
