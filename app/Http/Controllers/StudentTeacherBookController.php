<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\School;
use App\Models\StudentBook;
use App\Models\SubjectTaught;
use App\Models\TeacherBook;
use App\Models\SlaveReference;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;

class StudentTeacherBookController extends Controller
{
    public function savestudents(Request $request){
        
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of 
                    //remember to check if input value maps with the items in the array
                    if($request->filled('students_book')){
                        $students_book=$request->input('students_book.*');
                        foreach($students_book as $name){
                            if($name!='' && $request->school_class=='jss'){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $subject_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array

                                //jss1
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["jss1_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref,'Value'=>$name["jss1_pupils_books"]]);
                                }

                                //jss2
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["jss2_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref,'Value'=>$name["jss2_pupils_books"]]);
                                }

                                //jss3
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["jss3_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref,'Value'=>$name["jss3_pupils_books"]]);
                                }
                            }else if($name!='' && $request->school_class=='sss'){
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $subject_ref=$slaveref->Id_SlaveReference;

                                //ss1
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["sss1_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref,'Value'=>$name["sss1_pupils_books"]]);
                                }

                                //ss2
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["sss2_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref,'Value'=>$name["sss2_pupils_books"]]);
                                }

                                //ss3
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["sss3_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref,'Value'=>$name["sss3_pupils_books"]]);
                                }
                            }else if($name!='' && $request->school_class=='primary'){
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $subject_ref=$slaveref->Id_SlaveReference;

                                //nurs
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'893','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'893','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["preprimary_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'893','Id_Subject'=>$subject_ref,'Value'=>$name["preprimary_pupils_books"]]);
                                }

                                //p1
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'886','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'886','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm1_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'886','Id_Subject'=>$subject_ref,'Value'=>$name["prm1_pupils_books"]]);
                                }

                                //p2
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'887','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'887','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm2_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'887','Id_Subject'=>$subject_ref,'Value'=>$name["prm2_pupils_books"]]);
                                }

                                //p3
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'888','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'888','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm3_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'888','Id_Subject'=>$subject_ref,'Value'=>$name["prm3_pupils_books"]]);
                                }

                                //p4
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'889','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'889','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm4_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'889','Id_Subject'=>$subject_ref,'Value'=>$name["prm4_pupils_books"]]);
                                }

                                //p5
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'890','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'890','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm5_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'890','Id_Subject'=>$subject_ref,'Value'=>$name["prm5_pupils_books"]]);
                                }

                                //p6
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'891','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'891','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm6_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'891','Id_Subject'=>$subject_ref,'Value'=>$name["prm6_pupils_books"]]);
                                }
                            }else if($name!='' && $request->school_class=='private'){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $subject_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array

                                //p1
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'886','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'886','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm1_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'886','Id_Subject'=>$subject_ref,'Value'=>$name["prm1_pupils_books"]]);
                                }

                                //p2
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'887','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'887','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm2_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'887','Id_Subject'=>$subject_ref,'Value'=>$name["prm2_pupils_books"]]);
                                }

                                //p3
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'888','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'888','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm3_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'888','Id_Subject'=>$subject_ref,'Value'=>$name["prm3_pupils_books"]]);
                                }

                                //p4
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'889','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'889','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm4_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'889','Id_Subject'=>$subject_ref,'Value'=>$name["prm4_pupils_books"]]);
                                }

                                //p5
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'890','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'890','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm5_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'890','Id_Subject'=>$subject_ref,'Value'=>$name["prm5_pupils_books"]]);
                                }

                                //p6
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'891','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'891','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm6_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'891','Id_Subject'=>$subject_ref,'Value'=>$name["prm6_pupils_books"]]);
                                }

                                //jss1
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["jss1_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref,'Value'=>$name["jss1_pupils_books"]]);
                                }

                                //jss2
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["jss2_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref,'Value'=>$name["jss2_pupils_books"]]);
                                }

                                //jss3
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["jss3_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref,'Value'=>$name["jss3_pupils_books"]]);
                                }

                                //ss1
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["sss1_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref,'Value'=>$name["sss1_pupils_books"]]);
                                }

                                //ss2
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["sss2_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref,'Value'=>$name["sss2_pupils_books"]]);
                                }

                                //ss3
                                if($flag=DB::table('tblvar_studentbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_studentbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["sss3_pupils_books"]]);
                                }else{
                                    $sbs_model=StudentBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref,'Value'=>$name["sss3_pupils_books"]]);
                                }

                            }

                        }
                    }else{
                        return response()->json(['status'=>'error','message'=>'The array was not provided']);
                    }
                
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save students book","notes"=>"Transaction successful"]);

                //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]); 
                return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
        
    }

    public function saveteachers(Request $request){
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')){
                    //source of 
                    //remember to check if input value maps with the items in the array
                    if($request->filled('teachers_book')){
                        $teachers_book=$request->input('teachers_book.*');
                        foreach($teachers_book as $name){
                            if($name!='' && $request->school_class=='jss'){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $subject_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array

                                

                                //jss1
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["jss1_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref,'Value'=>$name["jss1_teachers_books"]]);
                                }

                                //jss2
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["jss2_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref,'Value'=>$name["jss2_teachers_books"]]);
                                }

                                //jss3
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["jss3_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref,'Value'=>$name["jss3_teachers_books"]]);
                                }
                            }else if($name!='' && $request->school_class=='sss'){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $subject_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array

                                

                                //ss1
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["sss1_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref,'Value'=>$name["sss1_teachers_books"]]);
                                }

                                //ss2
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["sss2_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref,'Value'=>$name["sss2_teachers_books"]]);
                                }

                                //ss3
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["sss3_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref,'Value'=>$name["sss3_teachers_books"]]);
                                }
                            }else if ($name!='' && $request->school_class=='primary'){
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $subject_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array

                                //nurs
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'893','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'893','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["preprimary_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'893','Id_Subject'=>$subject_ref,'Value'=>$name["preprimary_teachers_books"]]);
                                }

                                //p1
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'886','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'886','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm1_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'886','Id_Subject'=>$subject_ref,'Value'=>$name["prm1_teachers_books"]]);
                                }

                                //p2
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'887','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'887','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm2_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'887','Id_Subject'=>$subject_ref,'Value'=>$name["prm2_teachers_books"]]);
                                }

                                //p3
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'888','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'888','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm3_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'888','Id_Subject'=>$subject_ref,'Value'=>$name["prm3_teachers_books"]]);
                                }

                                //p4
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'889','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'889','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm4_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'889','Id_Subject'=>$subject_ref,'Value'=>$name["prm4_teachers_books"]]);
                                }

                                //p5
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'890','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'890','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm5_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'890','Id_Subject'=>$subject_ref,'Value'=>$name["prm5_teachers_books"]]);
                                }

                                //p6
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'891','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'891','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm6_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'891','Id_Subject'=>$subject_ref,'Value'=>$name["prm6_teachers_books"]]);
                                }
                            }else if ($name!='' && $request->school_class=='private'){
                                //save or update
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $subject_ref=$slaveref->Id_SlaveReference;
                                //remember to check if input value maps with the items in the array

                                //p1
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'886','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'886','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm1_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'886','Id_Subject'=>$subject_ref,'Value'=>$name["prm1_teachers_books"]]);
                                }

                                //p2
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'887','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'887','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm2_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'887','Id_Subject'=>$subject_ref,'Value'=>$name["prm2_teachers_books"]]);
                                }

                                //p3
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'888','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'888','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm3_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'888','Id_Subject'=>$subject_ref,'Value'=>$name["prm3_teachers_books"]]);
                                }

                                //p4
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'889','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'889','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm4_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'889','Id_Subject'=>$subject_ref,'Value'=>$name["prm4_teachers_books"]]);
                                }

                                //p5
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'890','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'890','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm5_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'890','Id_Subject'=>$subject_ref,'Value'=>$name["prm5_teachers_books"]]);
                                }

                                //p6
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'891','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'891','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["prm6_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'891','Id_Subject'=>$subject_ref,'Value'=>$name["prm6_teachers_books"]]);
                                }

                                //jss1
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["jss1_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'894','Id_Subject'=>$subject_ref,'Value'=>$name["jss1_teachers_books"]]);
                                }

                                //jss2
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["jss2_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'895','Id_Subject'=>$subject_ref,'Value'=>$name["jss2_teachers_books"]]);
                                }

                                //jss3
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["jss3_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'896','Id_Subject'=>$subject_ref,'Value'=>$name["jss3_teachers_books"]]);
                                }

                                //ss1
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["sss1_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'897','Id_Subject'=>$subject_ref,'Value'=>$name["sss1_teachers_books"]]);
                                }

                                //ss2
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["sss2_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'898','Id_Subject'=>$subject_ref,'Value'=>$name["sss2_teachers_books"]]);
                                }

                                //ss3
                                if($flag=DB::table('tblvar_teacherbook')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref])->exists()){
                                    DB::table('tblvar_teacherbook')
                                    ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref])
                                    ->update(['Value'=>$name["sss3_teachers_books"]]);
                                }else{
                                    $sbs_model=TeacherBook::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_Class'=>'899','Id_Subject'=>$subject_ref,'Value'=>$name["sss3_teachers_books"]]);
                                }


                            }
                        }
                        }else{
                            return response()->json(['status'=>'error','message'=>'The array was not provided']);
                        }
            
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save teacher book","notes"=>"Transaction successful"]);

                    return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }
}
