<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BirthCertificate;
use App\Models\EnrolmentAge;
use App\Models\EnrolmentEntrant;
use App\Models\EnrolmentOrphan;
use App\Models\EnrolmentPupilFlow;
use App\Models\EnrolmentSpecialNeed;
use App\Models\Repeater;
use App\Models\Stream;
use App\Models\SlaveReference;
use App\Models\School;
use App\Models\CompletedClass;
use App\Models\Examination;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Models\UserLog;

class EnrollmentController extends Controller
{
    //saves the birthcertificate section for enrollment
    public function savebirthcertificate(Request $request){
        
        //checks for the school_code and the year
         if($request->filled('school_code')&&$request->filled('year')){
             //get the school object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
            if($request->filled('school_class')&&$request->input('school_class')=='preprimary'){

                if($request->filled('birthcertificate')){
                    $birthcertificate=$request->input('birthcertificate.*');
                    foreach($birthcertificate as $name){
                        if($name!='' ){
                            $slaveref=SlaveReference::where('Value',$name["value"])->first();
                            $meta_data=$slaveref->Id_SlaveReference;

                            //kindergarten 1
                            if($flag=DB::table('tblvar_birthcertificate')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'881','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])->exists()){
                                DB::table('tblvar_birthcertificate')
                                ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'881','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])
                                ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'881','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('kindergarten1_male', $name))?$name["kindergarten1_male"]:null,'FemaleEnrol'=>(array_key_exists('kindergarten1_female', $name))?$name["kindergarten1_female"]:null]);
                            }else{
                                $birthcertificate_model_npc=BirthCertificate::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'881','Id_CertificateCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('kindergarten1_male', $name))?$name["kindergarten1_male"]:null,'FemaleEnrol'=>(array_key_exists('kindergarten1_female', $name))?$name["kindergarten1_female"]:null]);
                            }

                            //kindergarten 2
                            if($flag=DB::table('tblvar_birthcertificate')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'882','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])->exists()){
                                DB::table('tblvar_birthcertificate')
                                ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'882','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])
                                ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'882','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('kindergarten2_male', $name))?$name["kindergarten2_male"]:null,'FemaleEnrol'=>(array_key_exists('kindergarten2_female', $name))?$name["kindergarten2_female"]:null]);
                            }else{
                                $birthcertificate_model_npc=BirthCertificate::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'882','Id_CertificateCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('kindergarten2_male', $name))?$name["kindergarten2_male"]:null,'FemaleEnrol'=>(array_key_exists('kindergarten2_female', $name))?$name["kindergarten2_female"]:null]);
                            }

                            //nursery 1
                            if($flag=DB::table('tblvar_birthcertificate')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'883','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])->exists()){
                                DB::table('tblvar_birthcertificate')
                                ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'883','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])
                                ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'883','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('nursery1_male', $name))?$name["nursery1_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery1_female', $name))?$name["nursery1_female"]:null]);
                            }else{
                                $birthcertificate_model_npc=BirthCertificate::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'883','Id_CertificateCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('nursery1_male', $name))?$name["nursery1_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery1_female', $name))?$name["nursery1_female"]:null]);
                            }

                            //nursery 2
                            if($flag=DB::table('tblvar_birthcertificate')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'884','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])->exists()){
                                DB::table('tblvar_birthcertificate')
                                ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'884','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])
                                ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'884','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('nursery2_male', $name))?$name["nursery2_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery2_female', $name))?$name["nursery2_female"]:null]);
                            }else{
                                $birthcertificate_model_npc=BirthCertificate::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'884','Id_CertificateCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('nursery2_male', $name))?$name["nursery2_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery2_female', $name))?$name["nursery2_female"]:null]);
                            }

                             //nursery 3
                            if($flag=DB::table('tblvar_birthcertificate')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])->exists()){
                                DB::table('tblvar_birthcertificate')
                                ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])
                                ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('nursery3_male', $name))?$name["nursery3_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery3_female', $name))?$name["nursery3_female"]:null]);
                            }else{
                                $birthcertificate_model_npc=BirthCertificate::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'885','Id_CertificateCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('nursery3_male', $name))?$name["nursery3_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery3_female', $name))?$name["nursery3_female"]:null]);
                            }
                              //primary 1
                            if($flag=DB::table('tblvar_birthcertificate')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])->exists()){
                                DB::table('tblvar_birthcertificate')
                                ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])
                                ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary1_male', $name))?$name["primary1_male"]:null,'FemaleEnrol'=>(array_key_exists('primary1_female', $name))?$name["primary1_female"]:null]);
                            }else{
                                $birthcertificate_model_npc=BirthCertificate::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'886','Id_CertificateCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary1_male', $name))?$name["primary1_male"]:null,'FemaleEnrol'=>(array_key_exists('primary1_female', $name))?$name["primary1_female"]:null]);
                            }
                        }
                    }
                }
                
                $authuser=Auth::user();
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save birth certificate","notes"=>"Transaction successful"]);
            return response()->json(['status'=>'success','message'=>'The Post was successful']);

            }else if($request->filled('school_class')&&$request->input('school_class')=='jss'){
                if($request->filled('birthcertificate')){
                    $birthcertificate=$request->input('birthcertificate.*');
                    foreach($birthcertificate as $name){
                        if($name!='' ){
                            $slaveref=SlaveReference::where('Value',$name["value"])->first();
                            $meta_data=$slaveref->Id_SlaveReference;

                            if($flag=DB::table('tblvar_birthcertificate')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])->exists()){
                                DB::table('tblvar_birthcertificate')
                                ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])
                                ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('male', $name))?$name["male"]:null,'FemaleEnrol'=>(array_key_exists('female', $name))?$name["female"]:null]);
                            }else{
                                $birthcertificate_model_npc=BirthCertificate::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'894','Id_CertificateCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('male', $name))?$name["male"]:null,'FemaleEnrol'=>(array_key_exists('female', $name))?$name["female"]:null]);
                            }
                        }
                    }
                }
                //saving birth certificate for jss
                
                $authuser=Auth::user();
                $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save birth certificate","notes"=>"Transaction successful"]);
            return response()->json(['status'=>'success','message'=>'The Post was successful']);

            }else if($request->filled('school_class')&&$request->input('school_class')=='sss'){

                if($request->filled('birthcertificate')){
                    $birthcertificate=$request->input('birthcertificate.*');
                    foreach($birthcertificate as $name){
                        if($name!='' ){
                            $slaveref=SlaveReference::where('Value',$name["value"])->first();
                            $meta_data=$slaveref->Id_SlaveReference;

                            if($flag=DB::table('tblvar_birthcertificate')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])->exists()){
                                DB::table('tblvar_birthcertificate')
                                ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data])
                                ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_CertificateCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('sss1_male', $name))?$name["sss1_male"]:null,'FemaleEnrol'=>(array_key_exists('sss1_female', $name))?$name["sss1_female"]:null]);
                            }else{
                                $birthcertificate_model_npc=BirthCertificate::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'897','Id_CertificateCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('sss1_male', $name))?$name["sss1_male"]:null,'FemaleEnrol'=>(array_key_exists('sss1_female', $name))?$name["sss1_female"]:null]);
                            }

                        }
                    }
                }
                
            
            $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save birth certificate","notes"=>"Transaction successful"]);
            return response()->json(['status'=>'success','message'=>'The Post was successful']);

            }else{
                return response()->json(['status'=>'error','message'=>'The school class was not provided']);
            }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            

        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        } 
    }

    //saves the entrant section for enrollment
    public function saveentrant(Request $request){
        //check if the school code and year was sent
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school model instance of the object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                if($request->filled('entrants')){
                    $entrants=$request->input('entrants.*');
    
                    foreach($entrants as $name){
                        if($name!='' ){
                            $slaveref=SlaveReference::where('Value',$name["value"])->first();
                            $meta_data=$slaveref->Id_SlaveReference;
    
                            //check and get the schoolclass from the schoolclass field
                            if($request->filled('school_class')&&$request->input('school_class')=='preprimary'){
    
                                //save for age value
                                if($flag=DB::table('tblvar_enrolmententrant')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmententrant')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['MaleEntrant'=>(array_key_exists('male', $name))?$name["male"]:null,'FemaleEntrant'=>(array_key_exists('female', $name))?$name["female"]:null,'MaleECCD'=>(array_key_exists('eccd_male', $name))?$name["eccd_male"]:null,'FemaleECCD'=>(array_key_exists('eccd_female', $name))?$name["eccd_female"]:null]);
                                }else{
                                    $entrant=EnrolmentEntrant::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'886','Id_AgeCategory'=>$meta_data],['MaleEntrant'=>(array_key_exists('male', $name))?$name["male"]:null,'FemaleEntrant'=>(array_key_exists('female', $name))?$name["female"]:null,'MaleECCD'=>(array_key_exists('eccd_male', $name))?$name["eccd_male"]:null,'FemaleECCD'=>(array_key_exists('eccd_female', $name))?$name["eccd_female"]:null]);
                                }
    
                            }else if($request->filled('school_class')&&$request->input('school_class')=='jss'){
                                
                                //saving for jss
                                //save for 12
                                if($flag=DB::table('tblvar_enrolmententrant')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmententrant')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['MaleEntrant'=>(array_key_exists('male', $name))?$name["male"]:null,'FemaleEntrant'=>(array_key_exists('female', $name))?$name["female"]:null]);
                                }else{
                                    $entrant_12=EnrolmentEntrant::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'894','Id_AgeCategory'=>$meta_data],['MaleEntrant'=>(array_key_exists('male', $name))?$name["male"]:null,'FemaleEntrant'=>(array_key_exists('female', $name))?$name["female"]:null]);
                                }
                                
                            }
                            else if($request->filled('school_class')&&$request->input('school_class')=='sss'){
                                //save for ss1
                                //save for below 15
                                if($flag=DB::table('tblvar_enrolmententrant')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmententrant')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['MaleEntrant'=>(array_key_exists('male', $name))?$name["male"]:null,'FemaleEntrant'=>(array_key_exists('female', $name))?$name["female"]:null]);
                                }else{
                                    $entrant_below15=EnrolmentEntrant::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'897','Id_AgeCategory'=>$meta_data],['MaleEntrant'=>(array_key_exists('male', $name))?$name["male"]:null,'FemaleEntrant'=>(array_key_exists('female', $name))?$name["female"]:null]);
                                }
                                        
                            }else{
                                return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                            }                        
                        }
                    }
    
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save entrant","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    //saves the enrollment by age section
    public function saveenrollmentbyage(Request $request){  
        //check if the school code and year was sent
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school model instance of the object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('school_class')&&$request->input('school_class')=='preprimary'){
                    //save for stream model
                    //kindergarten 1
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'881','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'881','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'881','Year'=>$request->year,'Stream'=>($request->filled('stream_kindergarten1_stream'))?$request->input('stream_kindergarten1_stream'):null]);
                    }else{
                        $stream_kindergarten1=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'881'],['Stream'=>($request->filled('stream_kindergarten1_stream'))?$request->input('stream_kindergarten1_stream'):null]);
                    }

                    //kindergarten 2
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'882','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'882','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'882','Year'=>$request->year,'Stream'=>($request->filled('stream_kindergarten2_stream'))?$request->input('stream_kindergarten2_stream'):null]);
                    }else{
                        $stream_kindergarten2=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'882'],['Stream'=>($request->filled('stream_kindergarten2_stream'))?$request->input('stream_kindergarten2_stream'):null]);
                    }

                    //nursery 1
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'883','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'883','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'883','Year'=>$request->year,'Stream'=>($request->filled('stream_nursery1_stream'))?$request->input('stream_nursery1_stream'):null]);
                    }else{
                        $stream_nursery1=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'883'],['Stream'=>($request->filled('stream_nursery1_stream'))?$request->input('stream_nursery1_stream'):null]);
                    }

                    //nursery 2
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'884','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'884','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'884','Year'=>$request->year,'Stream'=>($request->filled('stream_nursery2_stream'))?$request->input('stream_nursery2_stream'):null]);
                    }else{
                        $stream_nursery2=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'884'],['Stream'=>($request->filled('stream_nursery2_stream'))?$request->input('stream_nursery2_stream'):null]);
                    }

                    //nursery 3
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year,'Stream'=>($request->filled('stream_nursery3_stream'))?$request->input('stream_nursery3_stream'):null]);
                    }else{
                        $stream_nursery3=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'885'],['Stream'=>($request->filled('stream_nursery3_stream'))?$request->input('stream_nursery3_stream'):null]);
                    }
                    

                    if($request->filled('enrollments')){
                        $enrollments=$request->input('enrollments.*');
                        foreach($enrollments as $name){
                            if($name!='' ){
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $meta_data=$slaveref->Id_SlaveReference;
        
                                //use the create or update method for each enrollment entrant type
                                //kindergarten 1
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'881','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'881','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'881','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('kindergarten1_male', $name))?$name["kindergarten1_male"]:null,'FemaleEnrol'=>(array_key_exists('kindergarten1_female', $name))?$name["kindergarten1_female"]:null]);
                                }else{
                                    $entrant_below3=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'881','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('kindergarten1_male', $name))?$name["kindergarten1_male"]:null,'FemaleEnrol'=>(array_key_exists('kindergarten1_female', $name))?$name["kindergarten1_female"]:null]);
                                }

                                //kindergarten 2
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'882','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'882','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'882','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('kindergarten2_male', $name))?$name["kindergarten2_male"]:null,'FemaleEnrol'=>(array_key_exists('kindergarten2_female', $name))?$name["kindergarten2_female"]:null]);
                                }else{
                                    $entrant_below3=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'882','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('kindergarten2_male', $name))?$name["kindergarten2_male"]:null,'FemaleEnrol'=>(array_key_exists('kindergarten2_female', $name))?$name["kindergarten2_female"]:null]);
                                }

                                //nursery 1
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'883','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'883','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'883','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('nursery1_male', $name))?$name["nursery1_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery1_female', $name))?$name["nursery1_female"]:null]);
                                }else{
                                    $entrant_below3=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'883','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('nursery1_male', $name))?$name["nursery1_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery1_female', $name))?$name["nursery1_female"]:null]);
                                }

                                //nursery 2
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'884','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'884','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'884','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('nursery2_male', $name))?$name["nursery2_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery2_female', $name))?$name["nursery2_female"]:null]);
                                }else{
                                    $entrant_below3=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'884','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('nursery2_male', $name))?$name["nursery2_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery2_female', $name))?$name["nursery2_female"]:null]);
                                }

                                //nursery 3
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('nursery3_male', $name))?$name["nursery3_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery3_female', $name))?$name["nursery3_female"]:null]);
                                }else{
                                    $entrant_below3=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'885','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('nursery3_male', $name))?$name["nursery3_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery3_female', $name))?$name["nursery3_female"]:null]);
                                }

                            }
                        }
                    }
                    
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                    $authuser=Auth::user();
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save enrollment by age","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);

                }else if($request->filled('school_class')&&$request->input('school_class')=='jss'){

                    //save for stream model
                    //jss1
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Stream'=>($request->filled('stream_jss1_stream'))?$request->input('stream_jss1_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_jss1_streamwithmultigrade'))?$request->input('stream_jss1_streamwithmultigrade'):null]);
                    }else{
                        $stream_jss1=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'894'],['Stream'=>($request->filled('stream_jss1_stream'))?$request->input('stream_jss1_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_jss1_streamwithmultigrade'))?$request->input('stream_jss1_streamwithmultigrade'):null]);
                    }
                    //jss2
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year,'Stream'=>($request->filled('stream_jss2_stream'))?$request->input('stream_jss2_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_jss2_streamwithmultigrade'))?$request->input('stream_jss2_streamwithmultigrade'):null]);
                    }else{
                        $stream_jss2=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'895'],['Stream'=>($request->filled('stream_jss2_stream'))?$request->input('stream_jss2_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_jss2_streamwithmultigrade'))?$request->input('stream_jss2_streamwithmultigrade'):null]);
                    }
                    //jss3
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year,'Stream'=>($request->filled('stream_jss3_stream'))?$request->input('stream_jss3_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_jss3_streamwithmultigrade'))?$request->input('stream_jss3_streamwithmultigrade'):null]);
                    }else{
                        $stream_jss3=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'896'],['Stream'=>($request->filled('stream_jss3_stream'))?$request->input('stream_jss3_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_jss3_streamwithmultigrade'))?$request->input('stream_jss3_streamwithmultigrade'):null]);
                    }

                    if($request->filled('enrollments')){
                        $enrollments=$request->input('enrollments.*');
                        foreach($enrollments as $name){
                            if($name!='' ){
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $meta_data=$slaveref->Id_SlaveReference;
        
                                //use the create or update method for each enrollment entrant type
                                //JSS1
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('jss1_male', $name))?$name["jss1_male"]:null,'FemaleEnrol'=>(array_key_exists('jss1_female', $name))?$name["jss1_female"]:null]);
                                }else{
                                    $entrant_12=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'894','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('jss1_male', $name))?$name["jss1_male"]:null,'FemaleEnrol'=>(array_key_exists('jss1_female', $name))?$name["jss1_female"]:null]);
                                }

                                //JSS2
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('jss2_male', $name))?$name["jss2_male"]:null,'FemaleEnrol'=>(array_key_exists('jss2_female', $name))?$name["jss2_female"]:null]);
                                }else{
                                    $entrant_12=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'895','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('jss2_male', $name))?$name["jss2_male"]:null,'FemaleEnrol'=>(array_key_exists('jss2_female', $name))?$name["jss2_female"]:null]);
                                }

                                //JSS3
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('jss3_male', $name))?$name["jss3_male"]:null,'FemaleEnrol'=>(array_key_exists('jss3_female', $name))?$name["jss3_female"]:null]);
                                }else{
                                    $entrant_12=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'896','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('jss3_male', $name))?$name["jss3_male"]:null,'FemaleEnrol'=>(array_key_exists('jss3_female', $name))?$name["jss3_female"]:null]);
                                }
        
                            }
                        }
                    }

                    //save for the repeaters model
                    //jss 1
                    if(DB::table('tblvar_repeaters')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year])->exists()){
                        DB::table('tblvar_repeaters')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Male'=>($request->filled('repeater_jss1_male'))?$request->input('repeater_jss1_male'):null,'Female'=>($request->filled('repeater_jss1_female'))?$request->input('repeater_jss1_female'):null]);
                    }else{
                        $repeater_model=Repeater::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'894'],['Male'=>($request->filled('repeater_jss1_male'))?$request->input('repeater_jss1_male'):null,'Female'=>($request->filled('repeater_jss1_female'))?$request->input('repeater_jss1_female'):null]);
                    }
                    //for jss2 
                    if(DB::table('tblvar_repeaters')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year])->exists()){
                        DB::table('tblvar_repeaters')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year,'Male'=>($request->filled('repeater_jss2_male'))?$request->input('repeater_jss2_male'):null,'Female'=>($request->filled('repeater_jss2_female'))?$request->input('repeater_jss2_female'):null]);
                    }else{
                        $repeater_model=Repeater::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'895'],['Male'=>($request->filled('repeater_jss2_male'))?$request->input('repeater_jss2_male'):null,'Female'=>($request->filled('repeater_jss2_female'))?$request->input('repeater_jss2_female'):null]);
                    }
                    //jss3
                    if(DB::table('tblvar_repeaters')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year])->exists()){
                        DB::table('tblvar_repeaters')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year,'Male'=>($request->filled('repeater_jss3_male'))?$request->input('repeater_jss3_male'):null,'Female'=>($request->filled('repeater_jss3_female'))?$request->input('repeater_jss3_female'):null]);
                    }else{
                        $repeater_model=Repeater::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'896'],['Male'=>($request->filled('repeater_jss3_male'))?$request->input('repeater_jss3_male'):null,'Female'=>($request->filled('repeater_jss3_female'))?$request->input('repeater_jss3_female'):null]);
                    }

                    //completed jss3 previous year
                    if(DB::table('tblvar_completedclasses')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year])->exists()){
                        DB::table('tblvar_completedclasses')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year,'MaleEnrol'=>($request->filled('prevyear_jss3_male'))?$request->input('prevyear_jss3_male'):null,'FemaleEnrol'=>($request->filled('prevyear_jss3_female'))?$request->input('prevyear_jss3_female'):null]);
                    }else{
                        $cC_model=CompletedClass::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'896'],['MaleEnrol'=>($request->filled('prevyear_jss3_male'))?$request->input('prevyear_jss3_male'):null,'FemaleEnrol'=>($request->filled('prevyear_jss3_female'))?$request->input('prevyear_jss3_female'):null]);
                    }

                    $authuser=Auth::user();
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save enrollment by age","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);
                }
                else if($request->filled('school_class')&&$request->input('school_class')=='sss'){
                    
                    //save for stream model
                    //ss1
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Stream'=>($request->filled('stream_sss1_stream'))?$request->input('stream_sss1_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_sss1_streamwithmultigrade'))?$request->input('stream_sss1_streamwithmultigrade'):null]);
                    }else{
                        $stream_jss1=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'897'],['Stream'=>($request->filled('stream_sss1_stream'))?$request->input('stream_sss1_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_sss1_streamwithmultigrade'))?$request->input('stream_sss1_streamwithmultigrade'):null]);
                    }

                    //ss2
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year,'Stream'=>($request->filled('stream_sss2_stream'))?$request->input('stream_sss2_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_sss2_streamwithmultigrade'))?$request->input('stream_sss2_streamwithmultigrade'):null]);
                    }else{
                        $stream_jss2=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'898'],['Stream'=>($request->filled('stream_sss2_stream'))?$request->input('stream_sss2_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_sss2_streamwithmultigrade'))?$request->input('stream_sss2_streamwithmultigrade'):null]);
                    }

                    //ss3
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year,'Stream'=>($request->filled('stream_sss3_stream'))?$request->input('stream_sss3_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_sss3_streamwithmultigrade'))?$request->input('stream_sss3_streamwithmultigrade'):null]);
                    }else{
                        $stream_jss3=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'899'],['Stream'=>($request->filled('stream_sss3_stream'))?$request->input('stream_sss3_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_sss3_streamwithmultigrade'))?$request->input('stream_sss3_streamwithmultigrade'):null]);
                    }

                    if($request->filled('enrollments')){
                        $enrollments=$request->input('enrollments.*');
                        foreach($enrollments as $name){
                            if($name!='' ){
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $meta_data=$slaveref->Id_SlaveReference;
        
                                //use the create or update method for each enrollment entrant type
                                //SS1
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('sss1_male', $name))?$name["sss1_male"]:null,'FemaleEnrol'=>(array_key_exists('sss1_female', $name))?$name["sss1_female"]:null]);
                                }else{
                                    $entrant_15=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'897','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('sss1_male', $name))?$name["sss1_male"]:null,'FemaleEnrol'=>(array_key_exists('sss1_female', $name))?$name["sss1_female"]:null]);
                                }

                                //SS2
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('sss2_male', $name))?$name["sss2_male"]:null,'FemaleEnrol'=>(array_key_exists('sss2_female', $name))?$name["sss2_female"]:null]);
                                }else{
                                    $entrant_15=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'898','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('sss2_male', $name))?$name["sss2_male"]:null,'FemaleEnrol'=>(array_key_exists('sss2_female', $name))?$name["sss2_female"]:null]);
                                }

                                //SS3
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('sss3_male', $name))?$name["sss3_male"]:null,'FemaleEnrol'=>(array_key_exists('sss3_female', $name))?$name["sss3_female"]:null]);
                                }else{
                                    $entrant_15=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'899','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('sss3_male', $name))?$name["sss3_male"]:null,'FemaleEnrol'=>(array_key_exists('sss3_female', $name))?$name["sss3_female"]:null]);
                                }
        
                            }
                        }
                    }

                    //save for the repeaters model
                    //ss 1
                    if(DB::table('tblvar_repeaters')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year])->exists()){
                        DB::table('tblvar_repeaters')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Male'=>($request->filled('repeater_sss1_male'))?$request->input('repeater_sss1_male'):null,'Female'=>($request->filled('repeater_sss1_female'))?$request->input('repeater_sss1_female'):null]);
                    }else{
                        $repeater_model=Repeater::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'897'],['Male'=>($request->filled('repeater_sss1_male'))?$request->input('repeater_sss1_male'):null,'Female'=>($request->filled('repeater_sss1_female'))?$request->input('repeater_sss1_female'):null]);
                    }
                    //for ss2 
                    if(DB::table('tblvar_repeaters')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year])->exists()){
                        DB::table('tblvar_repeaters')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year,'Male'=>($request->filled('repeater_sss2_male'))?$request->input('repeater_sss2_male'):null,'Female'=>($request->filled('repeater_sss2_female'))?$request->input('repeater_sss2_female'):null]);
                    }else{
                        $repeater_model=Repeater::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'898'],['Male'=>($request->filled('repeater_sss2_male'))?$request->input('repeater_sss2_male'):null,'Female'=>($request->filled('repeater_sss2_female'))?$request->input('repeater_sss2_female'):null]);
                    }
                    //ss3
                    if(DB::table('tblvar_repeaters')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year])->exists()){
                        DB::table('tblvar_repeaters')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year,'Male'=>($request->filled('repeater_sss3_male'))?$request->input('repeater_sss3_male'):null,'Female'=>($request->filled('repeater_sss3_female'))?$request->input('repeater_sss3_female'):null]);
                    }else{
                        $repeater_model=Repeater::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'899'],['Male'=>($request->filled('repeater_sss3_male'))?$request->input('repeater_sss3_male'):null,'Female'=>($request->filled('repeater_sss3_female'))?$request->input('repeater_sss3_female'):null]);
                    }

                    //completed ss3 previous year
                    if(DB::table('tblvar_completedclasses')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year])->exists()){
                        DB::table('tblvar_completedclasses')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year,'MaleEnrol'=>($request->filled('prevyear_sss3_male'))?$request->input('prevyear_sss3_male'):null,'FemaleEnrol'=>($request->filled('prevyear_sss3_female'))?$request->input('prevyear_sss3_female'):null]);
                    }else{
                        $cC_model=CompletedClass::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'899'],['MaleEnrol'=>($request->filled('prevyear_sss3_male'))?$request->input('prevyear_sss3_male'):null,'FemaleEnrol'=>($request->filled('prevyear_sss3_female'))?$request->input('prevyear_sss3_female'):null]);
                    }

                    $authuser=Auth::user();
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save enrollment by age","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);
                }
                else if($request->filled('school_class')&&$request->input('school_class')=='primary'){
                    //save for stream model
                    //primary 1
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Stream'=>($request->filled('stream_primary1_stream'))?$request->input('stream_primary1_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_primary1_streamwithmultigrade'))?$request->input('stream_primary1_streamwithmultigrade'):null]);
                    }else{
                        $stream_primary1=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'886'],['Stream'=>($request->filled('stream_primary1_stream'))?$request->input('stream_primary1_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_primary1_streamwithmultigrade'))?$request->input('stream_primary1_streamwithmultigrade'):null]);
                    }
                    //primary 2
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Stream'=>($request->filled('stream_primary2_stream'))?$request->input('stream_primary2_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_primary2_streamwithmultigrade'))?$request->input('stream_primary2_streamwithmultigrade'):null]);
                    }else{
                        $stream_primary2=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'887'],['Stream'=>($request->filled('stream_primary2_stream'))?$request->input('stream_primary2_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_primary2_streamwithmultigrade'))?$request->input('stream_primary2_streamwithmultigrade'):null]);
                    }
                    //primary 3
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Stream'=>($request->filled('stream_primary3_stream'))?$request->input('stream_primary3_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_primary3_streamwithmultigrade'))?$request->input('stream_primary3_streamwithmultigrade'):null]);
                    }else{
                        $stream_primary3=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'888'],['Stream'=>($request->filled('stream_primary3_stream'))?$request->input('stream_primary3_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_primary3_streamwithmultigrade'))?$request->input('stream_primary3_streamwithmultigrade'):null]);
                    }
                    //primary 4
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Stream'=>($request->filled('stream_primary4_stream'))?$request->input('stream_primary4_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_primary4_streamwithmultigrade'))?$request->input('stream_primary4_streamwithmultigrade'):null]);
                    }else{
                        $stream_primary4=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'889'],['Stream'=>($request->filled('stream_primary4_stream'))?$request->input('stream_primary4_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_primary4_streamwithmultigrade'))?$request->input('stream_primary4_streamwithmultigrade'):null]);
                    }
                    //primary 5
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Stream'=>($request->filled('stream_primary5_stream'))?$request->input('stream_primary5_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_primary5_streamwithmultigrade'))?$request->input('stream_primary5_streamwithmultigrade'):null]);
                    }else{
                        $stream_primary5=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'890'],['Stream'=>($request->filled('stream_primary5_stream'))?$request->input('stream_primary5_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_primary5_streamwithmultigrade'))?$request->input('stream_primary5_streamwithmultigrade'):null]);
                    }
                    //primary 6
                    if($flag=DB::table('tblvar_stream')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year])->exists()){
                        DB::table('tblvar_stream')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Stream'=>($request->filled('stream_primary6_stream'))?$request->input('stream_primary6_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_primary6_streamwithmultigrade'))?$request->input('stream_primary6_streamwithmultigrade'):null]);
                    }else{
                        $stream_primary6=Stream::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'891'],['Stream'=>($request->filled('stream_primary6_stream'))?$request->input('stream_primary6_stream'):null,'StreamWithMultiGrade'=>($request->filled('stream_primary6_streamwithmultigrade'))?$request->input('stream_primary6_streamwithmultigrade'):null]);
                    }

                    if($request->filled('enrollments')){
                        $enrollments=$request->input('enrollments.*');
                        foreach($enrollments as $name){
                            if($name!='' ){
                                $slaveref=SlaveReference::where('Value',$name["value"])->first();
                                $meta_data=$slaveref->Id_SlaveReference;
        
                                //use the create or update method for each enrollment entrant type
                                //primary 1
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary1_male', $name))?$name["primary1_male"]:null,'FemaleEnrol'=>(array_key_exists('primary1_female', $name))?$name["primary1_female"]:null]);
                                }else{
                                    $entrant_below6=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'886','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary1_male', $name))?$name["primary1_male"]:null,'FemaleEnrol'=>(array_key_exists('primary1_female', $name))?$name["primary1_female"]:null]);
                                }

                                //primary 2
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary2_male', $name))?$name["primary2_male"]:null,'FemaleEnrol'=>(array_key_exists('primary2_female', $name))?$name["primary2_female"]:null]);
                                }else{
                                    $entrant_below6=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'887','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary2_male', $name))?$name["primary2_male"]:null,'FemaleEnrol'=>(array_key_exists('primary2_female', $name))?$name["primary2_female"]:null]);
                                }

                                //primary 3
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary3_male', $name))?$name["primary3_male"]:null,'FemaleEnrol'=>(array_key_exists('primary3_female', $name))?$name["primary3_female"]:null]);
                                }else{
                                    $entrant_below6=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'888','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary3_male', $name))?$name["primary3_male"]:null,'FemaleEnrol'=>(array_key_exists('primary3_female', $name))?$name["primary3_female"]:null]);
                                }

                                //primary 4
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary4_male', $name))?$name["primary4_male"]:null,'FemaleEnrol'=>(array_key_exists('primary4_female', $name))?$name["primary4_female"]:null]);
                                }else{
                                    $entrant_below6=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'889','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary4_male', $name))?$name["primary4_male"]:null,'FemaleEnrol'=>(array_key_exists('primary4_female', $name))?$name["primary4_female"]:null]);
                                }

                                //primary 5
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary5_male', $name))?$name["primary5_male"]:null,'FemaleEnrol'=>(array_key_exists('primary5_female', $name))?$name["primary5_female"]:null]);
                                }else{
                                    $entrant_below6=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'890','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary5_male', $name))?$name["primary5_male"]:null,'FemaleEnrol'=>(array_key_exists('primary5_female', $name))?$name["primary5_female"]:null]);
                                }

                                //primary 6
                                if($flag=DB::table('tblvar_enrolmentage')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentage')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Id_AgeCategory'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Id_AgeCategory'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary6_male', $name))?$name["primary6_male"]:null,'FemaleEnrol'=>(array_key_exists('primary6_female', $name))?$name["primary6_female"]:null]);
                                }else{
                                    $entrant_below6=EnrolmentAge::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'891','Id_AgeCategory'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary6_male', $name))?$name["primary6_male"]:null,'FemaleEnrol'=>(array_key_exists('primary6_female', $name))?$name["primary6_female"]:null]);
                                }

                            }
                        }
                    }

                            
                    //save for the repeaters model
                    //primary 1
                    if(DB::table('tblvar_repeaters')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year])->exists()){
                        DB::table('tblvar_repeaters')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Male'=>($request->filled('repeater_primary1_male'))?$request->input('repeater_primary1_male'):null,'Female'=>($request->filled('repeater_primary1_female'))?$request->input('repeater_primary1_female'):null]);
                    }else{
                        $repeater_model=Repeater::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'886'],['Male'=>($request->filled('repeater_primary1_male'))?$request->input('repeater_primary1_male'):null,'Female'=>($request->filled('repeater_primary1_female'))?$request->input('repeater_primary1_female'):null]);
                    }

                    //for primary 2 
                    if(DB::table('tblvar_repeaters')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year])->exists()){
                        DB::table('tblvar_repeaters')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Male'=>($request->filled('repeater_primary2_male'))?$request->input('repeater_primary2_male'):null,'Female'=>($request->filled('repeater_primary2_female'))?$request->input('repeater_primary2_female'):null]);
                    }else{
                        $repeater_model=Repeater::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'887'],['Male'=>($request->filled('repeater_primary2_male'))?$request->input('repeater_primary2_male'):null,'Female'=>($request->filled('repeater_primary2_female'))?$request->input('repeater_primary2_female'):null]);
                    }

                    //primary 3
                    if(DB::table('tblvar_repeaters')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year])->exists()){
                        DB::table('tblvar_repeaters')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Male'=>($request->filled('repeater_primary3_male'))?$request->input('repeater_primary3_male'):null,'Female'=>($request->filled('repeater_primary3_female'))?$request->input('repeater_primary3_female'):null]);
                    }else{
                        $repeater_model=Repeater::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'888'],['Male'=>($request->filled('repeater_primary3_male'))?$request->input('repeater_primary3_male'):null,'Female'=>($request->filled('repeater_primary3_female'))?$request->input('repeater_primary3_female'):null]);
                    }

                    //primary 4
                    if(DB::table('tblvar_repeaters')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year])->exists()){
                        DB::table('tblvar_repeaters')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Male'=>($request->filled('repeater_primary4_male'))?$request->input('repeater_primary4_male'):null,'Female'=>($request->filled('repeater_primary4_female'))?$request->input('repeater_primary4_female'):null]);
                    }else{
                        $repeater_model=Repeater::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'889'],['Male'=>($request->filled('repeater_primary4_male'))?$request->input('repeater_primary4_male'):null,'Female'=>($request->filled('repeater_primary4_female'))?$request->input('repeater_primary4_female'):null]);
                    }

                    //primary 5
                    if(DB::table('tblvar_repeaters')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year])->exists()){
                        DB::table('tblvar_repeaters')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Male'=>($request->filled('repeater_primary5_male'))?$request->input('repeater_primary5_male'):null,'Female'=>($request->filled('repeater_primary4_female'))?$request->input('repeater_primary4_female'):null]);
                    }else{
                        $repeater_model=Repeater::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'890'],['Male'=>($request->filled('repeater_primary5_male'))?$request->input('repeater_primary5_male'):null,'Female'=>($request->filled('repeater_primary4_female'))?$request->input('repeater_primary4_female'):null]);
                    }

                    //primary 6
                    if(DB::table('tblvar_repeaters')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year])->exists()){
                        DB::table('tblvar_repeaters')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Male'=>($request->filled('repeater_primary6_male'))?$request->input('repeater_primary6_male'):null,'Female'=>($request->filled('repeater_primary6_female'))?$request->input('repeater_primary6_female'):null]);
                    }else{
                        $repeater_model=Repeater::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'891'],['Male'=>($request->filled('repeater_primary6_male'))?$request->input('repeater_primary6_male'):null,'Female'=>($request->filled('repeater_primary6_female'))?$request->input('repeater_primary6_female'):null]);
                    }

                    //completed primary6 previous year
                    if(DB::table('tblvar_completedclasses')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year])->exists()){
                        DB::table('tblvar_completedclasses')
                        ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year])
                        ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'MaleEnrol'=>($request->filled('prevyear_primary6_male'))?$request->input('prevyear_primary6_male'):null,'FemaleEnrol'=>($request->filled('prevyear_primary6_female'))?$request->input('prevyear_primary6_female'):null]);
                    }else{
                        $cC_model=CompletedClass::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'891'],['MaleEnrol'=>($request->filled('prevyear_primary6_male'))?$request->input('prevyear_primary6_male'):null,'FemaleEnrol'=>($request->filled('prevyear_primary6_female'))?$request->input('prevyear_primary6_female'):null]);
                    }
                    
                
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save enrollment by age","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);
                }
                else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }

        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    //saves the student flow section
    public function savestudentflow(Request $request){
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school model instance of the object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                if($request->filled('pupil_flow')){
                    $pupil_flow=$request->input('pupil_flow.*');
                    foreach($pupil_flow as $name){
                        if($name!='' ){
                            $slaveref=SlaveReference::where('Value',$name["value"])->first();
                            $meta_data=$slaveref->Id_SlaveReference;
    
                            //check and get the schoolclass from the schoolclass field
                            if($request->filled('school_class')&&$request->input('school_class')=='preprimary'){
                                
                                //saving for primary 1
                                if($flag=DB::table('tblvar_enrolmentpupilflow')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_FlowItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentpupilflow')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_FlowItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_FlowItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary1_male', $name))?$name["primary1_male"]:null,'FemaleEnrol'=>(array_key_exists('primary1_female', $name))?$name["primary1_female"]:null]);
                                }else{
                                    $dropout_pupilflow=EnrolmentPupilFlow::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'886','Id_FlowItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary1_male', $name))?$name["primary1_male"]:null,'FemaleEnrol'=>(array_key_exists('primary1_female', $name))?$name["primary1_female"]:null]);
                                }
                                
                                //for primary 2
                                if($flag=DB::table('tblvar_enrolmentpupilflow')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Id_FlowItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentpupilflow')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Id_FlowItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Id_FlowItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary2_male', $name))?$name["primary2_male"]:null,'FemaleEnrol'=>(array_key_exists('primary2_female', $name))?$name["primary2_female"]:null]);
                                }else{
                                    $dropout_pupilflow=EnrolmentPupilFlow::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'887','Id_FlowItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary2_male', $name))?$name["primary2_male"]:null,'FemaleEnrol'=>(array_key_exists('primary2_female', $name))?$name["primary2_female"]:null]);
                                }
                                
                                //for primary 3
                                if($flag=DB::table('tblvar_enrolmentpupilflow')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Id_FlowItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentpupilflow')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Id_FlowItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Id_FlowItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary3_male', $name))?$name["primary3_male"]:null,'FemaleEnrol'=>(array_key_exists('primary3_female', $name))?$name["primary3_female"]:null]);
                                }else{
                                    $dropout_pupilflow=EnrolmentPupilFlow::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'888','Id_FlowItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary3_male', $name))?$name["primary3_male"]:null,'FemaleEnrol'=>(array_key_exists('primary3_female', $name))?$name["primary3_female"]:null]);
                                }
    
                                
                                //for primary 4
                                if($flag=DB::table('tblvar_enrolmentpupilflow')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Id_FlowItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentpupilflow')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Id_FlowItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Id_FlowItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary4_male', $name))?$name["primary4_male"]:null,'FemaleEnrol'=>(array_key_exists('primary4_female', $name))?$name["primary4_female"]:null]);
                                }else{
                                    $dropout_pupilflow=EnrolmentPupilFlow::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'889','Id_FlowItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary4_male', $name))?$name["primary4_male"]:null,'FemaleEnrol'=>(array_key_exists('primary4_female', $name))?$name["primary4_female"]:null]);
                                }
    
                                
                                //for primary 5
                                if($flag=DB::table('tblvar_enrolmentpupilflow')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Id_FlowItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentpupilflow')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Id_FlowItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Id_FlowItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary5_male', $name))?$name["primary5_male"]:null,'FemaleEnrol'=>(array_key_exists('primary5_female', $name))?$name["primary5_female"]:null]);
                                }else{
                                    $dropout_pupilflow=EnrolmentPupilFlow::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'890','Id_FlowItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary5_male', $name))?$name["primary5_male"]:null,'FemaleEnrol'=>(array_key_exists('primary5_female', $name))?$name["primary5_female"]:null]);
                                }
    
                                
                                //for primary 6
                                if($flag=DB::table('tblvar_enrolmentpupilflow')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Id_FlowItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentpupilflow')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Id_FlowItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Id_FlowItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary6_male', $name))?$name["primary6_male"]:null,'FemaleEnrol'=>(array_key_exists('primary6_female', $name))?$name["primary6_female"]:null]);
                                }else{
                                    $dropout_pupilflow=EnrolmentPupilFlow::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'891','Id_FlowItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary6_male', $name))?$name["primary6_male"]:null,'FemaleEnrol'=>(array_key_exists('primary6_female', $name))?$name["primary6_female"]:null]);
                                }
    
                               
                            }else if($request->filled('school_class')&&$request->input('school_class')=='jss'){
    
                                //saving for jss 1
                                
                                if($flag=DB::table('tblvar_enrolmentpupilflow')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_FlowItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentpupilflow')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_FlowItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_FlowItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('jss1_male', $name))?$name["jss1_male"]:null,'FemaleEnrol'=>(array_key_exists('jss1_female', $name))?$name["jss1_female"]:null]);
                                }else{
                                    $dropout_pupilflow=EnrolmentPupilFlow::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'894','Id_FlowItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('jss1_male', $name))?$name["jss1_male"]:null,'FemaleEnrol'=>(array_key_exists('jss1_female', $name))?$name["jss1_female"]:null]);
                                }
                                
                                //for jss2
                                if($flag=DB::table('tblvar_enrolmentpupilflow')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year,'Id_FlowItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentpupilflow')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year,'Id_FlowItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year,'Id_FlowItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('jss2_male', $name))?$name["jss2_male"]:null,'FemaleEnrol'=>(array_key_exists('jss2_female', $name))?$name["jss2_female"]:null]);
                                }else{
                                    $dropout_pupilflow=EnrolmentPupilFlow::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'895','Id_FlowItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('jss2_male', $name))?$name["jss2_male"]:null,'FemaleEnrol'=>(array_key_exists('jss2_female', $name))?$name["jss2_female"]:null]);
                                }
    
                                
                                //jss3
                                if($flag=DB::table('tblvar_enrolmentpupilflow')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year,'Id_FlowItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentpupilflow')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year,'Id_FlowItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year,'Id_FlowItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('jss3_male', $name))?$name["jss3_male"]:null,'FemaleEnrol'=>(array_key_exists('jss3_female', $name))?$name["jss3_female"]:null]);
                                }else{
                                    $dropout_pupilflow=EnrolmentPupilFlow::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'896','Id_FlowItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('jss3_male', $name))?$name["jss3_male"]:null,'FemaleEnrol'=>(array_key_exists('jss3_female', $name))?$name["jss3_female"]:null]);
                                }
                                
                                
                            }
                            else if($request->filled('school_class')&&$request->input('school_class')=='sss'){
                                
                                //saving for sss 1
                                if($flag=DB::table('tblvar_enrolmentpupilflow')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_FlowItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentpupilflow')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_FlowItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_FlowItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('sss1_male', $name))?$name["sss1_male"]:null,'FemaleEnrol'=>(array_key_exists('sss1_female', $name))?$name["sss1_female"]:null]);
                                }else{
                                    $dropout_pupilflow=EnrolmentPupilFlow::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'897','Id_FlowItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('sss1_male', $name))?$name["sss1_male"]:null,'FemaleEnrol'=>(array_key_exists('sss1_female', $name))?$name["sss1_female"]:null]);
                                }
                                
                                //for sss2
                                if($flag=DB::table('tblvar_enrolmentpupilflow')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year,'Id_FlowItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentpupilflow')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year,'Id_FlowItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year,'Id_FlowItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('sss2_male', $name))?$name["sss2_male"]:null,'FemaleEnrol'=>(array_key_exists('sss2_female', $name))?$name["sss2_female"]:null]);
                                }else{
                                    $dropout_pupilflow=EnrolmentPupilFlow::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'898','Id_FlowItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('sss2_male', $name))?$name["sss2_male"]:null,'FemaleEnrol'=>(array_key_exists('sss2_female', $name))?$name["sss2_female"]:null]);
                                }
    
                                //sss3
                                if($flag=DB::table('tblvar_enrolmentpupilflow')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year,'Id_FlowItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentpupilflow')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year,'Id_FlowItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year,'Id_FlowItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('sss3_male', $name))?$name["sss3_male"]:null,'FemaleEnrol'=>(array_key_exists('sss3_female', $name))?$name["sss3_female"]:null]);
                                }else{
                                    $dropout_pupilflow=EnrolmentPupilFlow::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'899','Id_FlowItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('sss3_male', $name))?$name["sss3_male"]:null,'FemaleEnrol'=>(array_key_exists('sss3_female', $name))?$name["sss3_female"]:null]);
                                }
    
                                
                            }
                            else{
                                return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                            }
                        }
                    }
                     //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                     
                     $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save student flow","notes"=>"Transaction successful"]);
                     return response()->json(['status'=>'success','message'=>'The Post was successful']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

     //saves the student special needs
     public function savespecialneeds(Request $request){
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school model instance of the object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                if($request->filled('special_needs')){
                    $special_needs=$request->input('special_needs.*');
                    foreach($special_needs as $name){
                        if($name!='' ){
                            $slaveref=SlaveReference::where('Value','LIKE',$name["value"])->first();
                            $meta_data=$slaveref->Id_SlaveReference;
                            
                            //check and get the schoolclass from the schoolclass field
                            if($request->filled('school_class')&&$request->input('school_class')=='preprimary'){
                                //use the create or update method for each enrollment special needs type
                                //ECCD
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'892','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'892','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'892','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('eccd_male', $name))?$name["eccd_male"]:null,'FemaleEnrol'=>(array_key_exists('eccd_female', $name))?$name["eccd_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'892','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('eccd_male', $name))?$name["eccd_male"]:null,'FemaleEnrol'=>(array_key_exists('eccd_female', $name))?$name["eccd_female"]:null]);
                                }
    
                                
                                //NURS
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'893','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'893','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'893','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('nursery_male', $name))?$name["nursery_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery_female', $name))?$name["nursery_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'893','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('nursery_male', $name))?$name["nursery_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery_female', $name))?$name["nursery_female"]:null]);
                                }
    
                                //NUR3
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('nursery3_male', $name))?$name["nursery3_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery3_female', $name))?$name["nursery3_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'885','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('nursery3_male', $name))?$name["nursery3_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery3_female', $name))?$name["nursery3_female"]:null]);
                                }
    
                                //PRI 1
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary1_male', $name))?$name["primary1_male"]:null,'FemaleEnrol'=>(array_key_exists('primary1_female', $name))?$name["primary1_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'886','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary1_male', $name))?$name["primary1_male"]:null,'FemaleEnrol'=>(array_key_exists('primary1_female', $name))?$name["primary1_female"]:null]);
                                }
    
                                
                                //PRI 2
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary2_male', $name))?$name["primary2_male"]:null,'FemaleEnrol'=>(array_key_exists('primary2_female', $name))?$name["primary2_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'887','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary2_male', $name))?$name["primary2_male"]:null,'FemaleEnrol'=>(array_key_exists('primary2_female', $name))?$name["primary2_female"]:null]);
                                }
    
                                
                                //PRI 3
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary3_male', $name))?$name["primary3_male"]:null,'FemaleEnrol'=>(array_key_exists('primary3_female', $name))?$name["primary3_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'888','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary3_male', $name))?$name["primary3_male"]:null,'FemaleEnrol'=>(array_key_exists('primary3_female', $name))?$name["primary3_female"]:null]);
                                }
    
                                
                                //PRI 4
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary4_male', $name))?$name["primary4_male"]:null,'FemaleEnrol'=>(array_key_exists('primary4_female', $name))?$name["primary4_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'889','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary4_male', $name))?$name["primary4_male"]:null,'FemaleEnrol'=>(array_key_exists('primary4_female', $name))?$name["primary4_female"]:null]);
                                }
    
                                //PRI 5
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary5_male', $name))?$name["primary5_male"]:null,'FemaleEnrol'=>(array_key_exists('primary5_female', $name))?$name["primary5_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'890','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary5_male', $name))?$name["primary5_male"]:null,'FemaleEnrol'=>(array_key_exists('primary5_female', $name))?$name["primary5_female"]:null]);
                                }
    
                                //PRI 6
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary6_male', $name))?$name["primary6_male"]:null,'FemaleEnrol'=>(array_key_exists('primary6_female', $name))?$name["primary6_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'891','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary6_male', $name))?$name["primary6_male"]:null,'FemaleEnrol'=>(array_key_exists('primary6_female', $name))?$name["primary6_female"]:null]);
                                }
    
                            }
                            else if($request->filled('school_class')&&$request->input('school_class')=='jss'){
    
                                
                                //use the create or update method for each enrollment special needs type
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'894','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('jss1_male', $name))?$name["jss1_male"]:null,'FemaleEnrol'=>(array_key_exists('jss1_female', $name))?$name["jss1_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'894','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('jss1_male', $name))?$name["jss1_male"]:null,'FemaleEnrol'=>(array_key_exists('jss1_female', $name))?$name["jss1_female"]:null]);
                                }
    
                                
                                //jss2
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'895','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('jss2_male', $name))?$name["jss2_male"]:null,'FemaleEnrol'=>(array_key_exists('jss2_female', $name))?$name["jss2_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'895','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('jss2_male', $name))?$name["jss2_male"]:null,'FemaleEnrol'=>(array_key_exists('jss2_female', $name))?$name["jss2_female"]:null]);
                                }
    
                                
                                //jss3
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'896','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('jss3_male', $name))?$name["jss3_male"]:null,'FemaleEnrol'=>(array_key_exists('jss3_female', $name))?$name["jss3_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'896','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('jss3_male', $name))?$name["jss3_male"]:null,'FemaleEnrol'=>(array_key_exists('jss3_female', $name))?$name["jss3_female"]:null]);
                                }
    
                            }
                            else if($request->filled('school_class')&&$request->input('school_class')=='sss'){
                                //use the create or update method for each enrollment special needs type
                                //ss1
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'897','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('sss1_male', $name))?$name["sss1_male"]:null,'FemaleEnrol'=>(array_key_exists('sss1_female', $name))?$name["sss1_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'897','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('sss1_male', $name))?$name["sss1_male"]:null,'FemaleEnrol'=>(array_key_exists('sss1_female', $name))?$name["sss1_female"]:null]);
                                }
    
                                //ss2
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'898','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('sss2_male', $name))?$name["sss2_male"]:null,'FemaleEnrol'=>(array_key_exists('sss2_female', $name))?$name["sss2_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'898','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('sss2_male', $name))?$name["sss2_male"]:null,'FemaleEnrol'=>(array_key_exists('sss2_female', $name))?$name["sss2_female"]:null]);
                                }
    
                                //ss3
                                if($flag=DB::table('tblvar_enrolmentspecialneeds')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentspecialneeds')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'899','Year'=>$request->year,'Id_SpecialNeedItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('sss3_male', $name))?$name["sss3_male"]:null,'FemaleEnrol'=>(array_key_exists('sss3_female', $name))?$name["sss3_female"]:null]);
                                }else{
                                    $specialneed_hearing=EnrolmentSpecialNeed::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'899','Id_SpecialNeedItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('sss3_male', $name))?$name["sss3_male"]:null,'FemaleEnrol'=>(array_key_exists('sss3_female', $name))?$name["sss3_female"]:null]);
                                }
                                
                            }
                            else{
                                return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                            }
    
                           
                        }
                    }
                     //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                     
                     $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save special needs","notes"=>"Transaction successful"]);
                     return response()->json(['status'=>'success','message'=>'The Post was successful']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
            
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    //saves the student orphan
    public function saveorphan(Request $request){
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school model instance of the object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                if($request->filled('orphans')){
                    $orphans=$request->input('orphans.*');
                    foreach($orphans as $name){
                        if($name!='' ){
                            $slaveref=SlaveReference::where('Value',$name["value"])->first();
                            $meta_data=$slaveref->Id_SlaveReference;
                        
                            //check and get the schoolclass from the schoolclass field
                            if($request->filled('school_class')&&$request->input('school_class')=='preprimary'){
                                //saving for eccd
                                //lost mother
                                if($flag=DB::table('tblvar_enrolmentorphans')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'892','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentorphans')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'892','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'892','Year'=>$request->year,'Id_OrphanItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('eccd_male', $name))?$name["eccd_male"]:null,'FemaleEnrol'=>(array_key_exists('eccd_female', $name))?$name["eccd_female"]:null]);
                                }else{
                                    $lost_mother=EnrolmentOrphan::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'892','Id_OrphanItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('eccd_male', $name))?$name["eccd_male"]:null,'FemaleEnrol'=>(array_key_exists('eccd_female', $name))?$name["eccd_female"]:null]);
                                }
                                
                                //saving for nurs
                                //lost mother
                                if($flag=DB::table('tblvar_enrolmentorphans')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'893','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentorphans')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'893','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'893','Year'=>$request->year,'Id_OrphanItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('nursery_male', $name))?$name["nursery_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery_female', $name))?$name["nursery_female"]:null]);
                                }else{
                                    $lost_mother=EnrolmentOrphan::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'893','Id_OrphanItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('nursery_male', $name))?$name["nursery_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery_female', $name))?$name["nursery_female"]:null]);
                                }
                                
    
                                //saving for nur3
                                //lost mother
                                if($flag=DB::table('tblvar_enrolmentorphans')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentorphans')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'885','Year'=>$request->year,'Id_OrphanItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('nursery3_male', $name))?$name["nursery3_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery3_female', $name))?$name["nursery3_female"]:null]);
                                }else{
                                    $lost_mother=EnrolmentOrphan::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'885','Id_OrphanItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('nursery3_male', $name))?$name["nursery3_male"]:null,'FemaleEnrol'=>(array_key_exists('nursery3_female', $name))?$name["nursery3_female"]:null]);
                                }
                                
                                //saving for primary 1
                                //lost mother
                                if($flag=DB::table('tblvar_enrolmentorphans')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentorphans')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'886','Year'=>$request->year,'Id_OrphanItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary1_male', $name))?$name["primary1_male"]:null,'FemaleEnrol'=>(array_key_exists('primary1_female', $name))?$name["primary1_female"]:null]);
                                }else{
                                    $lost_mother=EnrolmentOrphan::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'886','Id_OrphanItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary1_male', $name))?$name["primary1_male"]:null,'FemaleEnrol'=>(array_key_exists('primary1_female', $name))?$name["primary1_female"]:null]);
                                }
                                
                                //saving for primary 2
                                //lost mother
                                if($flag=DB::table('tblvar_enrolmentorphans')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentorphans')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'887','Year'=>$request->year,'Id_OrphanItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary2_male', $name))?$name["primary2_male"]:null,'FemaleEnrol'=>(array_key_exists('primary2_female', $name))?$name["primary2_female"]:null]);
                                }else{
                                    $lost_mother=EnrolmentOrphan::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'887','Id_OrphanItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary2_male', $name))?$name["primary2_male"]:null,'FemaleEnrol'=>(array_key_exists('primary2_female', $name))?$name["primary2_female"]:null]);
                                }
                                
                                //saving for primary 3
                                //lost mother
                                if($flag=DB::table('tblvar_enrolmentorphans')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentorphans')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'888','Year'=>$request->year,'Id_OrphanItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary3_male', $name))?$name["primary3_male"]:null,'FemaleEnrol'=>(array_key_exists('primary3_female', $name))?$name["primary3_female"]:null]);
                                }else{
                                    $lost_mother=EnrolmentOrphan::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'888','Id_OrphanItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary3_male', $name))?$name["primary3_male"]:null,'FemaleEnrol'=>(array_key_exists('primary3_female', $name))?$name["primary3_female"]:null]);
                                }
                                
    
                                //saving for primary 4
                                //lost mother
                                if($flag=DB::table('tblvar_enrolmentorphans')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentorphans')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'889','Year'=>$request->year,'Id_OrphanItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary4_male', $name))?$name["primary4_male"]:null,'FemaleEnrol'=>(array_key_exists('primary4_female', $name))?$name["primary4_female"]:null]);
                                }else{
                                    $lost_mother=EnrolmentOrphan::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'889','Id_OrphanItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary4_male', $name))?$name["primary4_male"]:null,'FemaleEnrol'=>(array_key_exists('primary4_female', $name))?$name["primary4_female"]:null]);
                                }
                                
                                //saving for primary 5
                                //lost mother
                                if($flag=DB::table('tblvar_enrolmentorphans')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentorphans')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'890','Year'=>$request->year,'Id_OrphanItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary5_male', $name))?$name["primary5_male"]:null,'FemaleEnrol'=>(array_key_exists('primary5_female', $name))?$name["primary5_female"]:null]);
                                }else{
                                    $lost_mother=EnrolmentOrphan::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'890','Id_OrphanItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary5_male', $name))?$name["primary5_male"]:null,'FemaleEnrol'=>(array_key_exists('primary5_female', $name))?$name["primary5_female"]:null]);
                                }
                                
                                //saving for primary 6
                                //lost mother
                                if($flag=DB::table('tblvar_enrolmentorphans')->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])->exists()){
                                    DB::table('tblvar_enrolmentorphans')
                                    ->where(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Id_OrphanItem'=>$meta_data])
                                    ->update(['Id_School'=>$sch_model->Id_School,'Id_SchoolClass'=>'891','Year'=>$request->year,'Id_OrphanItem'=>$meta_data,'MaleEnrol'=>(array_key_exists('primary6_male', $name))?$name["primary6_male"]:null,'FemaleEnrol'=>(array_key_exists('primary6_female', $name))?$name["primary6_female"]:null]);
                                }else{
                                    $lost_mother=EnrolmentOrphan::updateOrCreate(['Id_School'=>$sch_model->Id_School,'Year'=>$request->year,'Id_SchoolClass'=>'891','Id_OrphanItem'=>$meta_data],['MaleEnrol'=>(array_key_exists('primary6_male', $name))?$name["primary6_male"]:null,'FemaleEnrol'=>(array_key_exists('primary6_female', $name))?$name["primary6_female"]:null]);
                                }
    
                            }else{
                                return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                            }
    
                        }
                    }
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                    
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save orphans","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }

    //saves the student examination
    public function saveexamination(Request $request){
        if($request->filled('school_code')&&$request->filled('year')){
            //get the school model instance of the object
            $sch_model=School::where('SchoolCode',$request->school_code)->first();
            $authuser=Auth::user();
            if($authuser->state_id==$sch_model->Id_State || $authuser->Id_Role==1){
                //check and get the schoolclass from the schoolclass field
                if($request->filled('examination')&&$request->input('examination')=='ssce'){
                    if($flag=DB::table('tblvar_examination')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'901'])->exists()){
                        DB::table('tblvar_examination')
                        ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'901'])
                        ->update(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'901','RegisteredMale'=>($request->filled('examination_ssce_registeredmale'))?$request->input('examination_ssce_registeredmale'):null,'RegisteredFemale'=>($request->filled('examination_ssce_registeredfemale'))?$request->input('examination_ssce_registeredfemale'):null,'RegisteredTotal'=>($request->filled('examination_ssce_registeredmale'))?$request->input('examination_ssce_registeredmale')+$request->input('examination_ssce_registeredfemale'):null,'TookPartMale'=>($request->filled('examination_ssce_tookpartmale'))?$request->input('examination_ssce_tookpartmale'):null,'TookPartFemale'=>($request->filled('examination_ssce_tookpartfemale'))?$request->input('examination_ssce_tookpartfemale'):null,'TookPartTotal'=>($request->filled('examination_ssce_tookpartmale'))?$request->input('examination_ssce_tookpartmale')+$request->input('examination_ssce_tookpartfemale'):null,'PassedMale'=>($request->filled('examination_ssce_passedmale'))?$request->input('examination_ssce_passedmale'):null,'PassedFemale'=>($request->filled('examination_ssce_passedfemale'))?$request->input('examination_ssce_passedfemale'):null,'PassedTotal'=>($request->filled('examination_ssce_passedmale'))?$request->input('examination_ssce_passedmale')+$request->input('examination_ssce_passedfemale'):null]);
                    }else{
                        $jsce=Examination::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'901'],['RegisteredMale'=>($request->filled('examination_ssce_registeredmale'))?$request->input('examination_ssce_registeredmale'):null,'RegisteredFemale'=>($request->filled('examination_ssce_registeredfemale'))?$request->input('examination_ssce_registeredfemale'):null,'RegisteredTotal'=>($request->filled('examination_ssce_registeredmale'))?$request->input('examination_ssce_registeredmale')+$request->input('examination_ssce_registeredfemale'):null,'TookPartMale'=>($request->filled('examination_ssce_tookpartmale'))?$request->input('examination_ssce_tookpartmale'):null,'TookPartFemale'=>($request->filled('examination_ssce_tookpartfemale'))?$request->input('examination_ssce_tookpartfemale'):null,'TookPartTotal'=>($request->filled('examination_ssce_tookpartmale'))?$request->input('examination_ssce_tookpartmale')+$request->input('examination_ssce_tookpartfemale'):null,'PassedMale'=>($request->filled('examination_ssce_passedmale'))?$request->input('examination_ssce_passedmale'):null,'PassedFemale'=>($request->filled('examination_ssce_passedfemale'))?$request->input('examination_ssce_passedfemale'):null,'PassedTotal'=>($request->filled('examination_ssce_passedmale'))?$request->input('examination_ssce_passedmale')+$request->input('examination_ssce_passedfemale'):null]);
                    }
                
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save ssce examination","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);
                }else if($request->filled('examination')&&$request->input('examination')=='jsce'){
                    //use the create or update method for each enrollment special needs type
                    if($flag=DB::table('tblvar_examination')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'900'])->exists()){
                        DB::table('tblvar_examination')
                        ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'900'])
                        ->update(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'900','RegisteredMale'=>($request->filled('examination_jsce_registeredmale'))?$request->input('examination_jsce_registeredmale'):null,'RegisteredFemale'=>($request->filled('examination_jsce_registeredfemale'))?$request->input('examination_jsce_registeredfemale'):null,'RegisteredTotal'=>($request->filled('examination_jsce_registeredmale'))?$request->input('examination_jsce_registeredmale')+$request->input('examination_jsce_registeredfemale'):null,'TookPartMale'=>($request->filled('examination_jsce_tookpartmale'))?$request->input('examination_jsce_tookpartmale'):null,'TookPartFemale'=>($request->filled('examination_jsce_tookpartfemale'))?$request->input('examination_jsce_tookpartfemale'):null,'TookPartTotal'=>($request->filled('examination_jsce_tookpartmale'))?$request->input('examination_jsce_tookpartmale')+$request->input('examination_jsce_tookpartfemale'):null,'PassedMale'=>($request->filled('examination_jsce_passedmale'))?$request->input('examination_jsce_passedmale'):null,'PassedFemale'=>($request->filled('examination_jsce_passedfemale'))?$request->input('examination_jsce_passedfemale'):null,'PassedTotal'=>($request->filled('examination_jsce_passedmale'))?$request->input('examination_jsce_passedmale')+$request->input('examination_jsce_passedfemale'):null]);
                    }else{
                        $jsce=Examination::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'900'],['RegisteredMale'=>($request->filled('examination_jsce_registeredmale'))?$request->input('examination_jsce_registeredmale'):null,'RegisteredFemale'=>($request->filled('examination_jsce_registeredfemale'))?$request->input('examination_jsce_registeredfemale'):null,'RegisteredTotal'=>($request->filled('examination_jsce_registeredmale'))?$request->input('examination_jsce_registeredmale')+$request->input('examination_jsce_registeredfemale'):null,'TookPartMale'=>($request->filled('examination_jsce_tookpartmale'))?$request->input('examination_jsce_tookpartmale'):null,'TookPartFemale'=>($request->filled('examination_jsce_tookpartfemale'))?$request->input('examination_jsce_tookpartfemale'):null,'TookPartTotal'=>($request->filled('examination_jsce_tookpartmale'))?$request->input('examination_jsce_tookpartmale')+$request->input('examination_jsce_tookpartfemale'):null,'PassedMale'=>($request->filled('examination_jsce_passedmale'))?$request->input('examination_jsce_passedmale'):null,'PassedFemale'=>($request->filled('examination_jsce_passedfemale'))?$request->input('examination_jsce_passedfemale'):null,'PassedTotal'=>($request->filled('examination_jsce_passedmale'))?$request->input('examination_jsce_passedmale')+$request->input('examination_jsce_passedfemale'):null]);
                    }
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                    
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save jsce examination","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);
                }
                else if($request->filled('examination')&&$request->input('examination')=='neco'){
                    if($flag=DB::table('tblvar_examination')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'902'])->exists()){
                        DB::table('tblvar_examination')
                        ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'902'])
                        ->update(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'902','RegisteredMale'=>($request->filled('examination_neco_registeredmale'))?$request->input('examination_neco_registeredmale'):null,'RegisteredFemale'=>($request->filled('examination_neco_registeredfemale'))?$request->input('examination_neco_registeredfemale'):null,'RegisteredTotal'=>($request->filled('examination_neco_registeredmale'))?$request->input('examination_neco_registeredmale')+$request->input('examination_neco_registeredfemale'):null,'TookPartMale'=>($request->filled('examination_neco_tookpartmale'))?$request->input('examination_neco_tookpartmale'):null,'TookPartFemale'=>($request->filled('examination_neco_tookpartfemale'))?$request->input('examination_neco_tookpartfemale'):null,'TookPartTotal'=>($request->filled('examination_neco_tookpartmale'))?$request->input('examination_neco_tookpartmale')+$request->input('examination_neco_tookpartfemale'):null,'PassedMale'=>($request->filled('examination_neco_passedmale'))?$request->input('examination_neco_passedmale'):null,'PassedFemale'=>($request->filled('examination_neco_passedfemale'))?$request->input('examination_neco_passedfemale'):null,'PassedTotal'=>($request->filled('examination_neco_passedmale'))?$request->input('examination_neco_passedmale')+$request->input('examination_neco_passedfemale'):null]);
                    }else{
                        $jsce=Examination::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'902'],['RegisteredMale'=>($request->filled('examination_neco_registeredmale'))?$request->input('examination_neco_registeredmale'):null,'RegisteredFemale'=>($request->filled('examination_neco_registeredfemale'))?$request->input('examination_neco_registeredfemale'):null,'RegisteredTotal'=>($request->filled('examination_neco_registeredmale'))?$request->input('examination_neco_registeredmale')+$request->input('examination_neco_registeredfemale'):null,'TookPartMale'=>($request->filled('examination_neco_tookpartmale'))?$request->input('examination_neco_tookpartmale'):null,'TookPartFemale'=>($request->filled('examination_neco_tookpartfemale'))?$request->input('examination_neco_tookpartfemale'):null,'TookPartTotal'=>($request->filled('examination_neco_tookpartmale'))?$request->input('examination_neco_tookpartmale')+$request->input('examination_neco_tookpartfemale'):null,'PassedMale'=>($request->filled('examination_neco_passedmale'))?$request->input('examination_neco_passedmale'):null,'PassedFemale'=>($request->filled('examination_neco_passedfemale'))?$request->input('examination_neco_passedfemale'):null,'PassedTotal'=>($request->filled('examination_neco_passedmale'))?$request->input('examination_neco_passedmale')+$request->input('examination_neco_passedfemale'):null]);
                    }
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                    
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save neco examination","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);
                }
                else if($request->filled('examination')&&$request->input('examination')=='nateb'){
                    if($flag=DB::table('tblvar_examination')->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'917'])->exists()){
                        DB::table('tblvar_examination')
                        ->where(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'917'])
                        ->update(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'917','RegisteredMale'=>($request->filled('examination_nateb_registeredmale'))?$request->input('examination_nateb_registeredmale'):null,'RegisteredFemale'=>($request->filled('examination_nateb_registeredfemale'))?$request->input('examination_nateb_registeredfemale'):null,'RegisteredTotal'=>($request->filled('examination_nateb_registeredmale'))?$request->input('examination_nateb_registeredmale')+$request->input('examination_nateb_registeredfemale'):null,'TookPartMale'=>($request->filled('examination_nateb_tookpartmale'))?$request->input('examination_nateb_tookpartmale'):null,'TookPartFemale'=>($request->filled('examination_nateb_tookpartfemale'))?$request->input('examination_nateb_tookpartfemale'):null,'TookPartTotal'=>($request->filled('examination_nateb_tookpartmale'))?$request->input('examination_nateb_tookpartmale')+$request->input('examination_nateb_tookpartfemale'):null,'PassedMale'=>($request->filled('examination_nateb_passedmale'))?$request->input('examination_nateb_passedmale'):null,'PassedFemale'=>($request->filled('examination_nateb_passedfemale'))?$request->input('examination_nateb_passedfemale'):null,'PassedTotal'=>($request->filled('examination_nateb_passedmale'))?$request->input('examination_nateb_passedmale')+$request->input('examination_nateb_passedfemale'):null]);
                    }else{
                        $jsce=Examination::updateOrCreate(['Id_School'=>$sch_model->Id_School,'CensusYear'=>$request->year,'Id_ExamType'=>'917'],['RegisteredMale'=>($request->filled('examination_nateb_registeredmale'))?$request->input('examination_nateb_registeredmale'):null,'RegisteredFemale'=>($request->filled('examination_nateb_registeredfemale'))?$request->input('examination_nateb_registeredfemale'):null,'RegisteredTotal'=>($request->filled('examination_nateb_registeredmale'))?$request->input('examination_nateb_registeredmale')+$request->input('examination_nateb_registeredfemale'):null,'TookPartMale'=>($request->filled('examination_nateb_tookpartmale'))?$request->input('examination_nateb_tookpartmale'):null,'TookPartFemale'=>($request->filled('examination_nateb_tookpartfemale'))?$request->input('examination_nateb_tookpartfemale'):null,'TookPartTotal'=>($request->filled('examination_nateb_tookpartmale'))?$request->input('examination_nateb_tookpartmale')+$request->input('examination_nateb_tookpartfemale'):null,'PassedMale'=>($request->filled('examination_nateb_passedmale'))?$request->input('examination_nateb_passedmale'):null,'PassedFemale'=>($request->filled('examination_nateb_passedfemale'))?$request->input('examination_nateb_passedfemale'):null,'PassedTotal'=>($request->filled('examination_nateb_passedmale'))?$request->input('examination_nateb_passedmale')+$request->input('examination_nateb_passedfemale'):null]);
                    }
                    //$create_log=UserLog::create(["id_user"=>"","id_school"=>"","censusyear"=>"","category"=>"","user_activity"=>"","notes"=>""]);
                    
                    $create_log=UserLog::create(["id_user"=>$authuser->id,"id_school"=>$sch_model->Id_School,"censusyear"=>$request->year,"category"=>"write","user_activity"=>"save nateb","notes"=>"Transaction successful"]);
                    return response()->json(['status'=>'success','message'=>'The Post was successful']);
                }
                else{
                    return response()->json(['status'=>'error','message'=>'The school class was not provided']);
                }
            }else{
                return response()->json(['type'=>'error','message'=>'You are not authorized to make this post']);
            }
        }else{
            return response()->json(['status'=>'error','message'=>'The school code/year was not sent']);
        }
    }
}