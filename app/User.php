<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','profile_pic','Id_Role','state_id', 'isdeleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //One to many(inverse) relationship with SlaveReference
   public function userrole()
   {
       return $this->belongsTo('App\Models\UserRole','Id_Role','Id');
   }

   public function state()
   {
       return $this->belongsTo('App\Models\State','state_id','Id_State');
   }

   public function AauthAcessToken()
   {
    return $this->hasMany('App\Models\OauthAccessToken');
   }
}
