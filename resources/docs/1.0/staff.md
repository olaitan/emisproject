# Staff

---

- [DATA](#data)
- [NO OF STAFF](#noofstaff)
- [ADD NEW](#addnew)
- [ADD LIST](#addlist)
- [REMOVE](#remove)
- [RETURNS](#returns)


<a name="data"></a>
## Data


```json 
        "staff": {
        "number_of_non_teaching_staffs": {
          "male": "7",
          "female": "7",
          "total": "14"
        },
        "number_of_teaching_staffs": {
          "male": "5",
          "female": "8",
          "total": "13"
        },
        "number_of_caregivers": {
          "male": "1",
          "female": "2"
        },
        "information_on_all_staff": {
          "data": [
            {
              "id": 19,
              "staff_file_no": null,
              "staff_name": "Ajayi John",
              "staff_gender": "m",
              "staff_type": "Head Teacher",
              "salary_source": "Federal Government",
              "staff_yob": 2000,
              "staff_yfa": 2000,
              "staff_ypa": 2000,
              "staff_yps": 2000,
              "staff_level": "2",
              "present": "Absent for more than 1 month - Maternity leave",
              "academic_qualification": "SSCE / WASC",
              "teaching_qualification": "PGDE",
              "area_specialisation": "Mathematics",
              "subject_taught": "Mathematics",
              "teaching_type": "Full Time",
              "teaches_seniorsecondary": "",
              "attended_training": 1
            }
          ]
        }
      },
```
---------------------------------------------------------------

<a name="noofstaff"></a>
## Post - Section/Staff/NoOfStaff

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required |string  |
| school_class | required |string  |
| staff_nonteachersmale | optional |string  |
| staff_nonteachersfemale | optional |string  |
| staff_teachersmale | optional |string  |
| staff_teachersfemale | optional |string  |
| caregivers_male | optional |string  |
| caregivers_female | optional |string  |

---------------------------------------------------------------

<a name="addnew"></a>
## Post - Section/Staff/Addnew

| Name  | Status | Info |
| : | : | :  |
| school_code | Cannot be changed |The school code  |
| year | required |string  |
| school_class | required |string  |
| staff_file_no | optional |string  |
| staff_name | required |string  |
| gender | required |string(male,female)  |
| salary_source | required |string  |
| dob | required |string  |
| year_of_first_appointment | optional |string  |
| present_appointment_year | optional |string  |
| year_of_posting | optional |string  |
| level | optional |string  |
| present | optional |string  |
| academic_qualification | required |string  |
| teaching_qualification | required |string  |
| specialisation | optional |string  |
| mainsubject_taught | optional |string  |
| teaching_type | optional |string  |
| is_teaching_ss | optional |boolean  |
| teacher_attended_training | optional |boolean  |

---------------------------------------------------------------

<a name="addlist"></a>
## Post - Section/Staff/Addlist

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required |string  |
| school_class | required |string  |
| stafflist | optional |Array of staff objects  |

Staff Object Sample
```json 
        {
            staff_file_no:"",
            staff_name:"",
            staff_gender:"",
            staff_type:"",
            salary_source:"",
            staff_yob:"",
            staff_yfa:"",
            staff_ypa:"",
            staff_yps:"",
            staff_level:"",
            present:"",
            academic_qualification:"",
            teaching_qualification:"",
            area_specialisation:"",
            subject_taught:"",
            teaching_type:"",
            is_teaching_ss:"",
            attended_training:""
        } 
```

---------------------------------------------------------------

<a name="remove"></a>
## Post - Section/Staff/Remove

| Name  | Status | Info |
| : | : | :  |
| school_code | Cannot be changed |The school code  |
| year | required |string  |
| transfer_school_code | required |string  |
| staff_id | required |string  |
| removalcategory | optional |string (get string value from the metadata)  |


<a name="returns"></a>
## Returns

Returns Json object containing the school data for the census year

```json 
        {
            'status':'success'
        } 
```

if an error occurred

```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```



