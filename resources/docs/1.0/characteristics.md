# Characteristics

---

- [DATA](#data)
- [POST](#post)
- [RETURNS](#returns)


<a name="data"></a>
## Data

Returns the school census form data for the year 

```json 
        "school_characteristics": {
        "year_of_establishment": "1996",
        "location": "Rural",
        "levels_of_education_offered": "Junior and Senior Secondary",
        "type_of_school": "Islamiyya Integrated",
        "shifts": "1",
        "shared_facilities": "1",
        "sharing_with": "5",
        "multi_grade_teaching": "1",
        "school_average_distance_from_catchment_communities": "6.00",
        "students_distance_from_school": "2",
        "students_boarding": {
          "male": "5",
          "female": "5"
        },
        "school_development_plan_sdp": "1",
        "school_based_management_committee_sbmc": "1",
        "parents_teachers_association_pta": "1",
        "date_of_last_inspection_visit": "2018-09-02",
        "authority_of_last_inspection": "State",
        "no_of_inspection": "8",
        "conditional_cash_transfer": "8",
        "school_grants": "0",
        "security_guard": "1",
        "ownership": "State"
      }
```
---------------------------------------------------------------

<a name="post"></a>
## Post - Section/SchoolIdentification

| Name  | Status | Info |
| : | : | :  |
| year_of_establishment | required |The school code  |
| location | required |string  |
| levels_of_education_offered | required |string  |
| type_of_school | required |string |
| shifts | required |boolean(0,1)  |
| shared_facilities | required |boolean(0,1) |
| sharing_with | required |string  |
| multi_grade_teaching | required |boolean(0,1)  |
| school_average_distance_from_catchment_communities | required |string  |
| students_distance_from_school | required |string  |
| students_boarding_male | required |string  |
| students_boarding_female | required |string  |
| school_development_plan_sdp | required |boolean(0,1)  |
| school_based_management_committee_sbmc | required |boolean(0,1)  |
| parents_teachers_association_pta | required |boolean(0,1)  |
| date_of_last_inspection_visit | required |string  |
| authority_of_last_inspection | required |string  |
| no_of_inspection | required |string|
| conditional_cash_transfer | required |string|
| school_grants | required |boolean(0,1)|
| security_guard | required |boolean(0,1)|
| ownership | required |string|



> {info}  0|1 for the booleans, the string value expected are in the metadata


<a name="returns"></a>
## Returns

Returns Json object containing the school data for the census year

```json 
        {
            'status':'success'
        } 
```

if an error occurred
```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```



