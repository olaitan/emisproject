# Workshop

---

- [DATA](#data)
- [ADD NEW](#addnew)
- [ADD LIST](#addlist)
- [REMOVE](#remove)
- [RETURNS](#returns)


<a name="data"></a>
## Data


```json 
        "workshops": {
        "data": [
          {
            "id": 2,
            "workshop_type": "Clothing & Textile Design",
            "year_of_construction": 1999,
            "present_condition": "Good",
            "length_in_meters": 1,
            "width_in_meters": 1,
            "floor_material": "Mud/Earth",
            "wall_material": "Mud",
            "roof_material": "Mud",
            "shared": 1,
            "seating": 1,
            "good_blackboard": 1
          }
        ]
      },
```
---------------------------------------------------------------

<a name="addnew"></a>
## Post - Section/Workshop/Addnew

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required |string  |
| school_class | required |string  |
| year_of_construction | optional |string  |
| present_condition | optional |string  |
| length_in_meters | optional |string  |
| width_in_meters | optional |string  |
| roof_material | optional |string  |
| floor_material | optional |string  |
| wall_material | optional |string  |
| seating | optional |string  |
| shared | optional |string  |
| workshop_type | optional |string  |
| good_blackboard | optional |string  |

---------------------------------------------------------------

<a name="addlist"></a>
## Post - Section/Workshop/Addlist

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required |string  |
| workshops | required |Array of workshop objects  |


---------------------------------------------------------------

<a name="remove"></a>
## Post - Section/Workshop/Remove

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required |string  |
| workshop_id | required |string  |


<a name="returns"></a>
## Returns

Returns Json object 

```json 
        {
            'status':'success'
        } 
```

if an error occurred
```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```



