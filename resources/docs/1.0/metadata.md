# Metadata

Nemis API provides you with the forms required metadata.

---

- [Metadata](#metadata)
- [Data](#data)
- [Attributes](#attributes)

<a name="metadata"></a>
## Metadata

```json 
        [
            'type':'Metadata',
            'data':[metadata object array],
            'attributes':[the states and lga],
        ] 
```

---------------------------------------------------------------

<a name="data"></a>
#### Data

The metadata for the differenct schools come with school's form data for a census year request.
Sending a get request to `/school/{schoolcode}/year/{year}` will return the data and metadata in json


```json 
        {
          "type": "Metadata",
          "reference": "Age Category",
          "description": "Ss",
          "value": "above 17",
          "display": "above 17",
          "order": 0,
          "level": 4
        } 
```

> {primary} The order is used to arrange the metadata while the level signifies the school level the metadata belongs

| Level | Value |
| : |   :-   |
| Pre primary | 1 |
| Primary | 2 |
| Junior Secondary | 3  |
| Senior Secondary | 4  |
| Pre primary and primary | 5  |
| Junior and Senior Secondary | 6  |
---------------------------------------------------------------

<a name="attributes"></a>
#### Attributes


```json 
      
        'attributes':{
            'states':[array of state objects]
        }
        
```

The state object has withing it the array of lgas belonging to it
```json 
      
        {
          "type": "state",
          "data": {
            "statecode": "AB",
            "name": "Abia",
            "lgas": [array of lga objects]
            }
        }
        
```

Here is the lga object
```json 
      
        {
            "type": "lga",
            "data": {
                "lgacode": "ABN",
                "name": "ABA NORTH"
            }
        }
        
```