# Search

Search Api that returns Schools based on the search parameter given

---

- [POST](#post)
- [ARGUMENTS](#arguments)
- [RETURNS](#returns)


<a name="post"></a>
## POST - /search/schools

Searches for the school/s based on the value of the parameters given 

---------------------------------------------------------------

<a name="arguments"></a>
#### Arguments

| Name | Status | Info |
| : |   :-   |  :  |
| schoolname | Optional | The school name of the school to be retrieved  |
| schoolcode | Optional | The school code of the school to be retrieved  |
| state | Optional  | The state name of the school to be retrieved  |
| lga | Optional | The school lga name to be retrieved  |
| schooltype | Optional | The school type of the school to be retrieved `[public,private,sciencevocational]` |

> {info}  Get the state and lga name from the metadata `GET /states`


<a name="returns"></a>
#### Returns

Returns Json object collection of the schools if matching any or empty

```json 
        {
            'type'=>'School',
            'data'=>[
                'name':school name,
                'schoolcode':school code,
                'town':town,
                'lga':lga,
                'state':state,
                'level':school level,
            ],
            
        
        } 
```



