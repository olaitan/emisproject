# Identification

---

- [DATA](#data)
- [POST](#post)
- [RETURNS](#returns)


<a name="data"></a>
## Data

Returns the school census form data for the year 

```json 
        "school_identification": {
            "school_registration": 2016,
            "school_code": "SCH006", 
            "school_name": "ISHERI GRAMMAR SCHOOL",
            "xcoordinate": "1",
            "ycoordinate": "2",
            "zcoordinate": "3",
            "address": "24, CLINTON STR, ISHERI",
            "town": "Afue",
            "ward": "Olaitan",
            "lga": "Katsina Ala",
            "state": "Benue",
            "school_telephone": "02154789",
            "email_address": "egba@gmail.com"
      }
```
---------------------------------------------------------------

<a name="post"></a>
## Post - Section/SchoolIdentification

| Name  | Status | Info |
| : | : | :  |
| school_code | Cannot be changed |The school code  |
| school_name | required |string  |
| xcoordinate | optional |string  |
| ycoordinate | optional |string  |
| zcoordinate | optional |string  |
| address | required |string  |
| town | required |string  |
| ward | required |string  |
| lga | required |int, the id of lga from the metadata  |
| state | required |int, the id of state from the metadata  |
| school_telephone | required |string  |
| email_address | required |string  |



> {info}  All the fields are required


<a name="returns"></a>
## Returns

Returns Json object containing the school data for the census year

```json 
        {
            'status':'success'
        } 
```

if an error occurred
```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```



