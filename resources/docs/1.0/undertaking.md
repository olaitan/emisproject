# Undertaking

---

- [DATA](#data)
- [POST](#post)
- [RETURNS](#returns)


<a name="data"></a>
## Data



```json 
        "undertaking": {
            "attestation_headteacher_name": null,
            "attestation_headteacher_telephone": null,
            "attestation_headteacher_signdate": null,
            "attestation_enumerator_name": null,
            "attestation_enumerator_position": null,
            "attestation_enumerator_telephone": null,
            "attestation_supervisor_name": null,
            "attestation_supervisor_position": null,
            "attestation_supervisor_telephone": null
      }
```
---------------------------------------------------------------

<a name="post"></a>
## Post - Section/Undertaking

| Name  | Status | Info |
| : | : | :  |
| school_code | required | The school code |
| year | required |string |
| attestation_headteacher_name | optional |string |
| attestation_headteacher_telephone | optional |string |
| attestation_headteacher_signdate | optional |date |
| attestation_enumerator_name | optional |string |
| attestation_enumerator_position | optional |string |
| attestation_enumerator_telephone | optional |string |
| attestation_supervisor_name | optional |string |
| attestation_supervisor_position | optional | string |
| attestation_supervisor_telephone | optional |string |



<a name="returns"></a>
## Returns

Returns Json object 

```json 
        {
            'status':'success'
        } 
```

if an error occurred
```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```



