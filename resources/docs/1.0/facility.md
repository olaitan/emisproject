# Facility

---

- [DATA](#data)
- [DRINKING WATER](#drinkingwater)
- [FACILITY AVAILABLE](#available)
- [SHARED FACILITIES](#sharedfacilities)
- [TOILET](#toilet)
- [POWER SOURCE](#powersource)
- [HEALTH FACILITY](#healthfacility)
- [FENCE FACILITY](#fencefacility)
- [SEATER](#seater)
- [OWNERSHIP](#ownership)
- [SCHOOL BUILDING](#schoolbuilding)
- [RETURNS](#returns)


<a name="data"></a>
## Data

```json 
        "facilities": {
        "source_of_drinking_water": {
          "data": [
            "Well",
            "Tankers"
          ]
        },
        "facilities_available": {
          "data": [
            {
              "censusyear": 2016,
              "facility": "Toilet",
              "useable": 3,
              "notuseable": 1
            },
            {
              "censusyear": 2016,
              "facility": "Computer",
              "useable": 2,
              "notuseable": 4
            },...
          ]
        },
        "shared_facilities": {
          "data": [
            "Computer",
            "Toilet",
            "Water Source",
            "Laboratory",
            "Play Ground",
            "Wash hand facility"
          ]
        },
        "toilet": {
          "data": [
            {
              "censusyear": 2016,
              "usertype": "Used only by students",
              "toilet": "Pit",
              "male": 1,
              "female": 2,
              "mixed": 3
            },...
          ]
        },
        "sources_of_power": {
          "data": [
            "Generator",
            "PHCN (NEPA)",
            "Gas Turbine"
          ]
        },
        "health_facility": {
          "data": "Yes, first aid kit"
        },
        "fence": {
          "data": "Needs Minor Repair"
        },
        "seater": {
          "data": [
            {
              "class": "ECCD",
              "seattype": "1 Seater",
              "capacity": 1
            },
            {
              "class": "ECCD",
              "seattype": "2 Seater",
              "capacity": 2
            },....
          ]
        },
        "playroom": {
          "data": [
            "Yes with Play Rugs"
          ]
        },
        "playfacilities": {
          "data": [
            "Swing",
            "Merry Go Round"
          ]
        },
        "materials": {
          "data": [
            "Posters",
            "Audio/Visual (Radio, TV, DVD)"
          ]
        }
      },
```
---------------------------------------------------------------

<a name="drinkingwater"></a>
## Post - Section/Facility/DrinkingWater

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| school_class | required | string  |
| drinking_water_sources | optional | Array of drinking water sources string( get value from metadata)  |

---------------------------------------------------------------

<a name="available"></a>
## Post - Section/Facility/Available

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| school_class | required | string  |
| facilities_available | optional | array of facility available objects  |


Facility Available object sample
```json 

        [
             {
                "value": "Wash",
                "useable":12,
                "notuseable":12,
                
            },....
        ]
```
---------------------------------------------------------------

<a name="sharedfacilities"></a>
## Post - Section/Facility/SharedFacilities

| Name  | Status | Info |
| : | : | :  |
| school_code | Cannot be changed |The school code  |
| year | required | string  |
| school_class | optional | string  |
| shared_facilities | optional | string  |

---------------------------------------------------------------

<a name="toilet"></a>
## Post - Section/Facility/Toilet

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| school_class | required | string  |
| toilets | required | array of toilet object  |

Toilet object sample
```json 

        [
             {
                "value": "Pit",
                "students_male":12,
                "students_female":12,
                "students_mixed":12,
                "teachers_male":12,
                "teachers_female":12,
                "teachers_mixed":12,
                "both_male":12,
                "both_female":12,
                "both_mixed":12,
            },....
        ]
```
---------------------------------------------------------------

<a name="powersource"></a>
## Post - Section/Facility/PowerSource

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| school_class | required | string  |
| power_source | optional | string  |

---------------------------------------------------------------

<a name="healthfacility"></a>
## Post - Section/Facility/HealthFacility

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| school_class | required | string  |
| health_facility | optional | string  |

---------------------------------------------------------------

<a name="fencefacility"></a>
## Post - Section/Facility/FenceFacility

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| school_class | required | string  |
| fence_facility | optional | string  |

---------------------------------------------------------------

<a name="seater"></a>
## Post - Section/Facility/Seater

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| school_class | required | string  |
| seaters | optional | array of seater objects  |

Seater object sample
```json 

        [
             {
                "value": "Jss1",
                "seater1":12,
                "seater2":2,
                "seater3":1,
                "seater4":12,
                "seater5":1,
                "seater6":2,
            },....
        ]
```
---------------------------------------------------------------

<a name="ownership"></a>
## Post - Section/Facility/Ownership

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| school_class | required | string  |
| ownership | optional | string  |

---------------------------------------------------------------

<a name="schoolbuilding"></a>
## Post - Section/Facility/SchoolBuilding

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| school_class | required | string  |
| school_building | optional | string  |

---------------------------------------------------------------



<a name="returns"></a>
## Returns

Returns Json object 

```json 
        {
            'status':'success'
        } 
```

if an error occurred
```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```



