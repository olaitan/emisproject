# School

---

- [GET](#get)
- [ARGUMENTS](#arguments)
- [RETURNS](#returns)


<a name="get"></a>
## GET - /school/{schoolcode}/year/{year}

Returns the school census form data for the year 

---------------------------------------------------------------

<a name="arguments"></a>
## Arguments

| Name  | Info |
| : | :  |
| year | The census year  |
| schoolcode | The school code of the school to be retrieved  |


> {info}  The year and schoolcode should not be empty/null


<a name="returns"></a>
## Returns

Returns Json object containing the school data for the census year

```json 
        {
            "school_identification":{school identification data},
            "school_characteristics":{school characteristics data},
            "enrollment":{enrollment data},
            "staff":{staff data},
            "classrooms":{classrooms data},
            "facilities":{facilities data},
            "number_of_student_by_subject":{ section data},
            "student_teacher_book":{section data},
            "caregiver_manual":{ section data},
            "teacher_qualification":{ section data},
            "undertaking":{section data},
            
        } 
```



