# Enrollment

---

- [BIRTH CERTIFICATES](#birthcertificates)
- [ENROLLMENT FOR CURRENT SCHOOL YEAR BY AGE](#yearbyage)
- [ENTRANTS](#entrants)
- [PUPIL FLOW](#pupilflow)
- [SPECIAL NEEDS](#specialneeds)
- [ORPHANS BY GRADE](#orphans)
- [EXAMINATION](#examination)



<a name="birthcertificates"></a>
## Birth Certificates

This section of the enrollment is for the birth certificates and you can find the metadata for birth certificates in the metadata.

#### Data

```json 
        "enrollment": {
            "birth_certificates":{
                "data":[
                    {
                        "class": "Jss1",
                        "value": "Others",
                        "male": 7,
                        "female": 7
                    },....
                ]
            }
      }
```
----------------------------------------------------------------------------------------------------
#### Metadata

```json 
        {
          "type": "Metadata",
          "reference": "Birth Certificate Type 2016",
          "description": "National Population Commission",
          "value": "National Population Commission",
          "display": "National Population Commission",
          "order": 1,
          "level": 3
        },....
```
----------------------------------------------------------------------------------------------------
#### Post - Section/SchoolEnrollment/BirthCertificate
| Name  | Status | Info |
| : | : | :  |
| school_code | required | string |
| year | required | string |
| school_class | required |string (preprimary, jss, sss) |
| birth_certificate | optional | Array of birth certificate objects  |

Birth Certificate expected objects for different classes:
```json 

     for jss   {
          "description": "National Population Commission",
          "value": "National Population Commission",
          "display": "National Population Commission",
          "male": "1",
          "female":"5",
          "order": 1,
          "level": 3
        },....

        for sss   {
          "description": "National Population Commission",
          "value": "National Population Commission",
          "display": "National Population Commission",
          "sss1_male": "1",
          "sss1_female":"5",
          "order": 1,
          "level": 3
        },....

        for kindergarten1   {
          "description": "National Population Commission",
          "value": "National Population Commission",
          "display": "National Population Commission",
          "kinder1_male": "1",
          "kinder1_female":"5",
          "order": 1,
          "level": 3
        },....

         for kindergarten2   {
          "description": "National Population Commission",
          "value": "National Population Commission",
          "display": "National Population Commission",
          "kinder2_male": "1",
          "kinder2_female":"5",
          "order": 1,
          "level": 3
        },....

         for nursery1   {
          "description": "National Population Commission",
          "value": "National Population Commission",
          "display": "National Population Commission",
          "nursery1_male": "1",
          "nursery1_female":"5",
          "order": 1,
          "level": 3
        },....

         for nursery2   {
          "description": "National Population Commission",
          "value": "National Population Commission",
          "display": "National Population Commission",
          "nursery2_male": "1",
          "nursery2_female":"5",
          "order": 1,
          "level": 3
        },....

         for nursery3   {
          "description": "National Population Commission",
          "value": "National Population Commission",
          "display": "National Population Commission",
          "nursery3_male": "1",
          "nursery3_female":"5",
          "order": 1,
          "level": 3
        },....

        for primary1   {
          "description": "National Population Commission",
          "value": "National Population Commission",
          "display": "National Population Commission",
          "primary1_male": "1",
          "primary1_female":"5",
          "order": 1,
          "level": 3
        },....

```
> {info}  There are only three School classes which could be preprimary, jss, sss



<a name="yearbyage"></a>
## Enrollment For Current School Year By Age

#### Data

```json 
        "enrollment": {
            "year_by_age":{
                "data":{
                    "stream":[
                      {
                        "class": "Kindergarten1",
                        "stream": 1,
                        "stream_with_multigrade": null
                      },....
                    ],
                    "age": [
                      {
                        "class": "Kindergarten1",
                        "age_category": "Below 3",
                        "male": 6,
                        "female": 7
                      },.....
                    ],
                    "repeater": [
                      {
                        "class": "Jss1",
                        "male": 2,
                        "female": 5
                      },
                      {
                        "class": "Jss2",
                        "male": 2,
                        "female": 2
                      },.....
                  ]
                }
            }
      }
```
----------------------------------------------------------------------------------------------------
#### Metadata

```json 
        {
          "type": "Metadata",
          "reference": "Age Category",
          "description": "Primary",
          "value": "6",
          "display": "6",
          "order": 2,
          "level": 2
        },....
```
----------------------------------------------------------------------------------------------------
#### Post - Section/SchoolEnrollment/EnrollmentByAge
| Name  | Status | Info |
| : | : | :  |
| school_code | required | string |
| year | required | string |
| school_class | required |string (preprimary, primary, jss, sss) |
| stream_jss1_stream | optional | string |
| stream_jss1_streamwithmultigrade | optional | string |
| repeater_jss1_male | optional | string |
| enrollments | optional | Array of enrollment objects  |

Enrollment Object Sample
```json 

       {
          
          "value": "6",//this is the age, to be gotten from the metadata
          "jss1_male": "1",
          "jss1_female":"5",
          "jss2_male": "1",
          "jss2_female":"5",
          "jss3_male": "1",
          "jss3_female":"5",
        },....
        
```
Level and Classes (nursery1_male,nursery1_female)
| Level  |
| : | : |
| preprimary | kindergarten1,kindergarten2,nursery1,nursery2,nursery3 |
| primary | primary1-primary6 |
| jss | jss1,jss2,jss3 |
| sss | sss1,sss2,sss3 |




<a name="entrants"></a>
## Entrants

#### Data

```json 
         "entrants": {
          "data": [
            {
              "class": "Jss1",
              "age_category": "12",
              "male": 1,
              "female": 2,
              "eccd_male": null,
              "eccd_female": null
            },
            {
              "class": "Jss1",
              "age_category": "13",
              "male": 3,
              "female": 1,
              "eccd_male": null,
              "eccd_female": null
            },...
          ]
         }
```
----------------------------------------------------------------------------------------------------
#### Metadata

```json 
        {
          "type": "Metadata",
          "reference": "Age Category",
          "description": "Primary",
          "value": "6",
          "display": "6",
          "order": 2,
          "level": 2
        },....
```
----------------------------------------------------------------------------------------------------
#### Post - Section/SchoolEnrollment/Entrant
| Name  | Status | Info |
| : | : | :  |
| school_code | required | string |
| year | required | string |
| school_class | required |string (preprimary, primary, jss, sss) |
| entrants | optional | Array of entrants objects  |

Entrant Object Sample
```json 

       {
          "value": "6",
          "display": "6",
          "male": "1",
          "female":"5",
        },....

        
```

<a name="pupilflow"></a>
## Pupil Flow

#### Data

```json 
        "enrollment": {
            "pupil_flow": {
          "data": [
              {
                "class": "Primary1",
                "flow_item": "Dropout",
                "male": 1,
                "female": 4
              },....
            ]
          }
      }
```
----------------------------------------------------------------------------------------------------
#### Metadata

```json 
        {
          "type": "Metadata",
          "reference": "Enrolment Items 2013",
          "description": "Transfer In",
          "value": "Transfer In",
          "display": "Transfer In",
          "order": 2,
          "level": 0
        },....
```
----------------------------------------------------------------------------------------------------
#### Post - Section/SchoolEnrollment/StudentFlow
| Name  | Status | Info |
| : | : | :  |
| school_code | required | string |
| year | required | string |
| school_class | required | string (preprimary, jss, sss) |
| pupil_flow | optional | Array of pupil flow objects  |

Pupil flow expected objects for different classes:
```json 

     for jss   {
          "value": "Transfer In",//get this from the metadata
          "jss1_male": "1",
          "jss1_female":"5",
          "jss2_male": "1",
          "jss2_female":"5",
          "jss3_male": "1",
          "jss3_female":"5",
          "order": 1,
          "level": 3
        },....

        for sss   {
          "value": "Transfer In",
          "sss1_male": "1",
          "sss1_female":"5",
          "sss2_male": "1",
          "sss2_female":"5",
          "sss3_male": "1",
          "sss3_female":"5",
          "order": 1,
          "level": 3
        },....

        for primary   {
          "value": "Transfer In",
          "eccd_male": "1",
          "eccd_female":"5",
          "nurs_male": "1",
          "nurs_female":"5",
          "nurs3_male": "1",
          "nurs3_female":"5",
          "primary1_male": "1",
          "primary1_female":"5",
          "primary2_male": "1",
          "primary2_female":"5",
          "primary3_male": "1",
          "primary3_female":"5",
          "primary4_male": "1",
          "primary4_female":"5",
          "primary5_male": "1",
          "primary5_female":"5",
          "primary6_male": "1",
          "primary6_female":"5",
          "order": 1,
          "level": 3
        },....

```


<a name="specialneeds"></a>
## Special Needs
#### Data

```json 
        "special_need": {
          "data": [
            {
              "class": "Jss1",
              "special_need_item": "Blind / visually impaired",
              "male": 6,
              "female": 9
            },
            {
              "class": "Jss2",
              "special_need_item": "Blind / visually impaired",
              "male": 8,
              "female": 7
            },.....
          ]
        }
```
----------------------------------------------------------------------------------------------------
#### Metadata

```json 
        {
          "type": "Metadata",
          "reference": "Pupil Challenges",
          "description": "Blind and Visually Impaired",
          "value": "Blind and Visually Impaired",
          "display": "Blind and Visually Impaired",
          "order": 1,
          "level": 3
        },....
```
----------------------------------------------------------------------------------------------------
#### Post - Section/SchoolEnrollment/SpecialNeeds
| Name  | Status | Info |
| : | : | :  |
| school_code | required | string |
| year | required | string |
| school_class | required |string (preprimary, jss, sss) |
| special_needs | optional | Array of special need objects  |

Special need expected objects for different classes:
```json 

     for jss   {
          "value": "Blind and Visually Impaired",
          "jss1_male": "1",
          "jss1_female":"5",
          "jss2_male": "1",
          "jss2_female":"5",
          "jss3_male": "1",
          "jss3_female":"5",
          "order": 1,
          "level": 3
        },....

        for sss   {
          "value": "Blind and Visually Impaired",
          "sss1_male": "1",
          "sss1_female":"5",
          "sss2_male": "1",
          "sss2_female":"5",
          "sss3_male": "1",
          "sss3_female":"5",
          "order": 1,
          "level": 3
        },....

        for primary   {
          "value": "Blind and Visually Impaired",
          "eccd_male": "1",
          "eccd_female":"5",
          "nurs_male": "1",
          "nurs_female":"5",
          "nurs3_male": "1",
          "nurs3_female":"5",
          "primary1_male": "1",
          "primary1_female":"5",
          "primary2_male": "1",
          "primary2_female":"5",
          "primary3_male": "1",
          "primary3_female":"5",
          "primary4_male": "1",
          "primary4_female":"5",
          "primary5_male": "1",
          "primary5_female":"5",
          "primary6_male": "1",
          "primary6_female":"5",
          "order": 1,
          "level": 3
        },....

```

<a name="orphans"></a>
## Orphans

#### Data

```json 
        "enrollment": {
            "birth_certificates":{
                "data":[
                    {
                        "class": "Jss1",
                        "value": "Others",
                        "male": 7,
                        "female": 7
                    },....
                ]
            }
      }
```
----------------------------------------------------------------------------------------------------
#### Metadata

```json 
        {
          "type": "Metadata",
          "reference": "Birth Certificate Type 2016",
          "description": "National Population Commission",
          "value": "National Population Commission",
          "display": "National Population Commission",
          "order": 1,
          "level": 3
        },....
```
----------------------------------------------------------------------------------------------------
#### Post - Section/SchoolEnrollment/Orphan
| Name  | Status | Info |
| : | : | :  |
| school_code | required | string |
| year | required | string |
| school_class | required |string (preprimary, jss, sss) |
| orphans | optional | Array of orphan objects  |

Orphan expected objects for different classes:

```json 

     for jss   {
          "value": "National Population Commission",
          "jss1_male": "1",
          "jss1_female":"5",
          "jss2_male": "1",
          "jss2_female":"5",
          "jss3_male": "1",
          "jss3_female":"5",
        },....

        for sss   {
          "value": "National Population Commission",
          "sss1_male": "1",
          "sss1_female":"5",
          "sss2_male": "1",
          "sss2_female":"5",
          "sss3_male": "1",
          "sss3_female":"5",
        },....

        for primary   {
          "value": "National Population Commission",
          "eccd_male": "1",
          "eccd_female":"5",
          "nurs_male": "1",
          "nurs_female":"5",
          "nurs3_male": "1",
          "nurs3_female":"5",
          "primary1_male": "1",
          "primary1_female":"5",
          "primary2_male": "1",
          "primary2_female":"5",
          "primary3_male": "1",
          "primary3_female":"5",
          "primary4_male": "1",
          "primary4_female":"5",
          "primary5_male": "1",
          "primary5_female":"5",
          "primary6_male": "1",
          "primary6_female":"5",
        },....

```

<a name="examination"></a>
## Examination

#### Data

```json 
        "examination": {
          "data": [
            {
              "exam_type": "NATEB",
              "registered_male": 1,
              "registered_female": 2,
              "registered_total": 3,
              "took_part_male": 4,
              "took_part_female": 5,
              "took_part_total": 9,
              "passed_male": 6,
              "passed_female": 6,
              "passed_total": 12
            },
            {
              "exam_type": "JSCE",
              "registered_male": 5,
              "registered_female": 8,
              "registered_total": 13,
              "took_part_male": 5,
              "took_part_female": 10,
              "took_part_total": 15,
              "passed_male": 19,
              "passed_female": 2,
              "passed_total": 21
            },
            {
              "exam_type": "SSCE",
              "registered_male": 1,
              "registered_female": 4,
              "registered_total": 5,
              "took_part_male": 2,
              "took_part_female": 5,
              "took_part_total": 7,
              "passed_male": 2,
              "passed_female": 3,
              "passed_total": 5
            }
          ]
        }
      },
      
```
----------------------------------------------------------------------------------------------------
#### Post - Section/SchoolEnrollment/Examination
| Name  | Status | Info |
| : | : | :  |
| school_code | required | string |
| year | required | string |
| examination | required | string (jsce, ssce, nateb) |
| examination_jsce_registeredmale | optional | string |
| examination_jsce_registeredfemale | optional | string |
| examination_jsce_tookpartmale | optional | string |
| examination_jsce_tookpartfemale | optional | string |
| examination_jsce_passedmale | optional | string |
| examination_jsce_passedfemale | optional | string |


## Returns

Returns Json object containing the school data for the census year

```json 
        {
            'status':'success'
        } 
```

if an error occurred
```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```