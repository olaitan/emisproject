# Teachers Textbooks

---

- [DATA](#data)
- [POST](#post)
- [RETURNS](#returns)


<a name="data"></a>
## Data

```json 
        "teacherbook": [
            {
              "class": "Primary1",
              "subject": "Mathematics",
              "value": 9
            },
            {
              "class": "Primary1",
              "subject": "English",
              "value": 2
            },
            {
              "class": "Primary1",
              "subject": "Other",
              "value": 6
            },
            {
              "class": "Primary1",
              "subject": "Science",
              "value": 2
            },......
        ]
```
----------------------------------------------------------------------------------------------------
#### Metadata

```json 
          {
          "type": "Metadata",
          "reference": "Subject of Qualification Secondary",
          "description": "Humanities",
          "value": "Humanities",
          "display": "5 - Humanities",
          "order": 5,
          "level": 2
        },....
```
----------------------------------------------------------------------------------------------------

<a name="post"></a>
## Post - Section/Book/Teacher

| Name  | Status | Info |
| : | : | :  |
| school_code | Cannot be changed |The school code  |
| year | required |string  |
| school_class | required |string  |
| teachers_book | required |Array of teachers book objects  |


Teacher Book object sample
```json 

        [
             {
                "value": "Humanities",
                "jss1_teachers_books":12,
                "jss2_teachers_books":12,
                "jss3_teachers_books":12,
            },....
        ]
```
| Level  | variable |
| : | : | 
| preprimary | preprimary_teachers_books | 
| primary | prm1_teachers_books - prm6_teachers_books |
| jss | jss1_teachers_books,jss2_teachers_books,jss3_teachers_books |
| sss | sss1_teachers_books,sss2_teachers_books,sss3_teachers_books |

<a name="returns"></a>
## Returns

Returns Json object

```json 
        {
            'status':'success'
        } 
```

if an error occurred
```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```



