# Care Giver Manuals

---

- [DATA](#data)
- [POST](#post)
- [RETURNS](#returns)


<a name="data"></a>
## Data


```json 
        "caregiver_manual": {
            "data": [
            {
                "category": "National Curriculum",
                "available": null
            },
            {
                "category": "Care Giver Guide / Hand Book",
                "available": "0"
            },
            {
                "category": "National Policy on ECCDE / Pre-primary School",
                "available": "1"
            }
            ]
        },
```
----------------------------------------------------------------------------------------------------
#### Metadata

```json 
          {
          "type": "Metadata",
          "reference": "Caregiver Manual Type",
          "description": "Care Giver Guide / Hand Book",
          "value": "Care Giver Guide / Hand Book",
          "display": "Care Giver Guide / Hand Book",
          "order": 2,
          "level": 1
        },
        {
          "type": "Metadata",
          "reference": "Caregiver Manual Type",
          "description": "National Curriculum",
          "value": "National Curriculum",
          "display": "National Curriculum",
          "order": 4,
          "level": 1
        },....
```
----------------------------------------------------------------------------------------------------
<a name="post"></a>
## Post - Section/Caregivers/Manual

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required |string  |
| school | required |string(preprimary,primary,jss,sss)  |
| caregiversmanuals | required | Array of care giver objects |
> {info}  All the fields are required

Care giver object sample:
```json 

        [
            {
            "value": "National Curriculum",
            "available":1
            },....
        ]
```


<a name="returns"></a>
## Returns

Returns Json object

```json 
        {
            'status':'success'
        } 
```

if an error occurred
```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```



