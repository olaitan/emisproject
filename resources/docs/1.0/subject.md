# Subject Textbooks for Pupils

---

- [DATA](#data)
- [POST](#post)
- [RETURNS](#returns)


<a name="data"></a>
## Data


```json 
        "subject_textbooks_available": {
          "data": [
            {
              "class": "Primary1",
              "subject": "Mathematics",
              "value": 9
            },
            {
              "class": "Primary1",
              "subject": "English",
              "value": 2
            },
            {
              "class": "Primary1",
              "subject": "Other",
              "value": 2
            },
            {
              "class": "Primary1",
              "subject": "Science",
              "value": 2
            },....
            ]
        }
```
----------------------------------------------------------------------------------------------------
#### Metadata

```json 
          {
          "type": "Metadata",
          "reference": "Subject of Qualification Secondary",
          "description": "Humanities",
          "value": "Humanities",
          "display": "5 - Humanities",
          "order": 5,
          "level": 2
        },....
```
----------------------------------------------------------------------------------------------------

<a name="post"></a>
#### Post - Section/Book/Student

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required |string  |
| school_class | required |string  |
| students_book | optional |Array of students book objects  |


Teacher Book expected objects for different classes:
```json 

        [
             {
                "value": "Humanities",
                "jss1_students_books":12,
                "jss2_students_books":8,
                "jss3_students_books":10,
            },....
        ]
```
| Level  | variable |
| : | : | 
| Pre Primary | preprimary_pupils_books | 
| Primary | prm1_pupils_books - prm6_pupils_books |
| Jss | jss1_pupils_books,jss2_pupils_books,jss3_pupils_books |
| Sss | sss1_pupils_books,sss2_pupils_books,sss3_pupils_books |

<a name="returns"></a>
#### Returns

Returns Json object 

```json 
        {
            'status':'success'
        } 
```

if an error occurred
```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```



