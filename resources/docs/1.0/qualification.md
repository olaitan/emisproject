# Teacher Qualification

---

- [DATA](#data)
- [POST](#post)
- [RETURNS](#returns)


<a name="data"></a>
## Data

```json 
        "teacher_qualification": {
        "data": [
          {
            "qualification": "None",
            "level": "Pre-primary",
            "male": 5,
            "female": 8
          },........
          ]
        }
```
----------------------------------------------------------------------------------------------------
#### Metadata

```json 
        {
          "type": "Metadata",
          "reference": "Teaching Qualifications",
          "description": "PGDE",
          "value": "PGDE",
          "display": "2 - PGDE",
          "order": 2,
          "level": 0
        },....
```
----------------------------------------------------------------------------------------------------

<a name="post"></a>
#### Post - Section/Teacher/Qualification

| Name  | Status | Info |
| : | : | :  |
| school_code | required | string |
| year | required |string  |
| school | required |string (private,primary,sss)  |
| teacher_qualification | required | Array of teacher qualification objects |
> {info}  All the fields are required

Teacher Qualification object sample:
```json 

        [{
          "value": "PGDE",
          "preprimary_male": "1",
          "preprimary_female":"5",
          "primary_male": "1",
          "primary_female":"5",
        },....
        ]
     
```
| Level  | variable |
| : | : | 
| Pre Primary | preprimary_male,preprimary_female | 
| Primary | primary_male,primary_female |
| Jss | jss_male,jss_female |
| Sss | sss_male,sss_female |

<a name="returns"></a>
## Returns

Returns Json object

```json 
        {
            'status':'success'
        } 
```

if an error occurred
```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```



