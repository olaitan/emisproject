# Classroom

---

- [DATA](#data)
- [NO OF CLASSROOMS](#noofclassrooms)
- [ADD NEW](#addnew)
- [REMOVE](#remove)
- [OTHER ROOM](#otherroom)
- [ADD LIST](#addlist)
- [RETURNS](#returns)


<a name="data"></a>
## Data

Returns the school census form data for the year 

```json 
       "classrooms": {
        "number_of_classrooms": "1",
        "are_classes_held_outside": "1",
        "eccd_playrooms": "5",
        "information_on_all_classrooms": {
          "data": [
            {
              "id": 3,
              "year_of_construction": 1995,
              "present_condition": "Good",
              "length_in_meters": 8,
              "width_in_meters": 9,
              "floor_material": "Mud/Earth",
              "wall_material": "Mud",
              "roof_material": "Mud",
              "seating": "1",
              "good_blackboard": "1"
            }
          ]
        },
        "rooms_other_than_classrooms": {
          "data": [
            {
              "roomtype": "Staff room",
              "number": 5
            },
            {
              "roomtype": "Office",
              "number": 2
            },
            {
              "roomtype": "Laboratories",
              "number": 3
            },
            {
              "roomtype": "Store room",
              "number": 4
            },
            {
              "roomtype": "Others",
              "number": 8
            }
          ]
        }
      },
```
---------------------------------------------------------------

<a name="noofclassrooms"></a>
## Post - Section/Classroom/NoOfClassroom

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required |string  |
| school_class | required |string  |
| no_of_classrooms | optional |string  |
| classes_held_outside | optional |string  |
| no_playrooms | optional |string  |


---------------------------------------------------------------

<a name="addnew"></a>
## Post - Section/Classroom/Addnew

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required |string  |
| school_class | required |string  |
| year_of_construction | required |string  |
| present_condition | required |string  |
| length_in_meters | required |string  |
| width_in_meters | required |string  |
| floor_material | required |string  |
| roof_material | required |string  |
| wall_material | required |string  |
| seating | required |string  |
| good_blackboard | required |string  |

---------------------------------------------------------------

<a name="remove"></a>
## Post - Section/Classroom/Remove

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required |string  |
| class_id | required |string  |

---------------------------------------------------------------

<a name="otherroom"></a>
## Post - Section/Room/Otherroom

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required |string  |
| school_class | required |string  |
| no_of_staffrooms | optional |string  |
| no_of_offices | optional |string  |
| no_of_laboratories | optional |string  |
| no_of_storerooms | optional |string  |
| no_of_others | optional |string  |

---------------------------------------------------------------

<a name="addlist"></a>
## Post - Section/Classroom/Addlist

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required |string  |
| classrooms | required |Array of classroom objects  |

```json 
        {
            year_of_construction:this.year_of_construction,
            present_condition:this.present_condition,
            length_in_meters:this.classroom_length,
            width_in_meters:this.classroom_width,
            floor_material:this.floor_material,
            roof_material:this.roof_material,
            wall_material:this.wall_material,
            seating:this.seating,
            good_blackboard:this.blackboard
        }
```
<a name="returns"></a>
## Returns

Returns Json object containing the school data for the census year

```json 
        {
            'status':'success'
        } 
```

if an error occurred
```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```



