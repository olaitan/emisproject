# Registration

---

- [POST](#post)
- [ARGUMENTS](#arguments)
- [RETURNS](#returns)


<a name="post"></a>
## POST - Section/SchoolRegistration

You need to register a school for a new census year before inputing the data 

---------------------------------------------------------------

<a name="arguments"></a>
## Arguments

| Name  | Info |
| : | :  |
| year | The census year  |
| school_code | The school code of the school to be retrieved  |
| register_year | The census year to register  |


> {info}  The year and schoolcode should not be empty/null

<a name="returns"></a>
## Returns

Returns Json object containing the school data for the census year

```json 
        {
            'status':'success'
        } 
```

if an error occurred
```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```



