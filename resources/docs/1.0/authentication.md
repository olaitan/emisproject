# Authentication

---

- [Overview](#overview)

<a name="overview"></a>
## Overview

Authenticate your API calls by including your secret key in the Authorization header of every request you make. You can manage your API keys from the dashboard.

Generally, we provide both public and secret keys. If for any reason you believe your secret key has been compromised or you wish to reset them, you can do so from the dashboard.

> {danger} Do not commit your secret keys to git, or use them in client-side code.

Authorization headers should be in the following format: `Authorization: Bearer SECRET_KEY`

> {primary} `Authorization: Bearer sk_test_shdjkhdj827391nV4Lid` 