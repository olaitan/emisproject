# Nemis Documentation

Nemis API to access public data in the Nigerian Education System

---

- [Welcome](#welcome)

<a name="welcome"></a>
## Welcome

Before you can start using the Nemis API, you will need a Nemis developers account. Create a free account now if you haven't already done so.It strives to be RESTful and is organized around the main resources you would be interacting with - with a few notable exceptions. 

Here you will learn how to integrate the Nemis API into your application / product.

Points to remember when using our APIs

* Our base URL  is `http://theschoollight.com/api/` and all our endpoints are relative to the base url

* All request should include the headers

```json 
headers: {
            Accept: "application/json",
            Authorization:"Bearer "+theauthorizedtoken,
         } 
```