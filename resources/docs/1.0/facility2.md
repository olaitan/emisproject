# Facility

---

- [PLAYROOM](#playroom)
- [PLAY FACILITIES](#playfacilities)
- [LEARNING MATERIAL](#learningmaterial)
- [REMOVE DRINKING SOURCE](#removedrinkingsource)
- [REMOVE SHAREDFACILITY](#removesharedfacility)
- [REMOVE POWERSOURCE](#removepowersource)
- [REMOVE LEARNING MATERIAL](#removelearningmaterial)
- [REMOVE PLAYFACILITY](#removeplayfacility)
- [RETURNS](#returns)


<a name="playroom"></a>
## Post - Section/Facility/PlayRoom

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| school_class | required | string  |
| playroom | optional | string  |

---------------------------------------------------------------

<a name="playfacilities"></a>
## Post - Section/Facility/PlayFacilities

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| school_class | required | string  |
| play_facilities | optional | string  |

---------------------------------------------------------------

<a name="learningmaterial"></a>
## Post - Section/Facility/LearningMaterial

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| school_class | required | string  |
| materials | optional | string  |

---------------------------------------------------------------

<a name="removedrinkingsource"></a>
## Post - Section/Remove/DrinkingSource

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| source | required | string  |

---------------------------------------------------------------

<a name="removesharedfacility"></a>
## Post - Section/Remove/SharedFacility

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| shared_facility | required | string  |

---------------------------------------------------------------

<a name="removepowersource"></a>
## Post - Section/Remove/PowerSource

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| power_source | required | string  |

---------------------------------------------------------------

<a name="removelearningmaterial"></a>
## Post - Section/Remove/LearningMaterial

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| material | required | string  |

---------------------------------------------------------------

<a name="removeplayfacility"></a>
## Post - Section/Remove/PlayFacility

| Name  | Status | Info |
| : | : | :  |
| school_code | required |The school code  |
| year | required | string  |
| play_facility | required | string  |

---------------------------------------------------------------



<a name="returns"></a>
## Returns

Returns Json object 

```json 
        {
            'status':'success'
        } 
```

if an error occurred
```json 
        {
            'status':'error',
            'message':'reason for error'
        } 
```



