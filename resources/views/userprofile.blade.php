<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin | Dashboard</title>
    <!-- StyleSheets -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/customized.css">
    <link rel="stylesheet" href="assets/css/users.css">
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
</head>
<body>
    <div id="app">
        <!-- Main Container -->
        <div id="main-container">
            <div id="alertUsers">
                <p id="info"></p>
            </div>
            <div id="left-tab" class="theme">
                <div class="logo">                
                    <span id="collapse"></span>
                    <a href="/"><p>NEMIS</p></a>
                </div>           
                <div class="links">
                    <ul>
                        <li><a href="/"><span class="overview"></span><p>Overview</p></a></li>
                        <li><a href="/" id="moreReportLinks"><span class="reports"></span><p>Reports</p></a>
                            <ul id="extraReportLinks">
                            <li v-if="auth_user.role_id==1"><a href="/school_characteristics_report">School Characteristics</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/enrolment_report">Enrolment</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/repeaters_report">Repeaters</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_facility_report">School_Facility</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_streams_report">School_Streams</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_teachers_report">School_Teachers</a></li>                                 
                            </ul>
                        </li>
                        <li><a href="/search"><span class="search"></span><p>Search</p></a></li>
                        <li><a href="/new"><span class="addSchool"></span><p>Add a school</p></a></li>
                        <li v-if="auth_user.role_id==1"><a href="/" id="moreLinks" class="moreLinks"><span class="settings"></span><p>Settings</p></a>
                            <ul id="extraLinks">
                                <li><a href="/metadatas">Configure Metadatas</a></li>
                                <!-- <li><a href="/stateMetadatas">Configure States</a></li> -->
                                <li><a href="/lgaMetadatas">Configure LGA</a></li>
                                <li><a href="/userlogs">User Logs</a></li>
                            </ul>
                        </li>
                    </ul>
                </div> 
                <div class="users">
                    <div class="user-avatar">
                        <img v-bind:src="auth_user.profile_pic" alt="IA" srcset="" >
                    </div>
                    <h2 class="name"><a href="" id="showDetails">@{{auth_user.name}}</a></h2>
                    <p class="role">@{{auth_user.role}}</p>

                    <div class="users-links" id="users-links">
                        <ul>
                            <li><a href="/userID">View Profile</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/configure">Configure</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/edituser">Accounts</a></li>
                            <li v-on:click="logout"><a href="/login">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div id="right-tab">
                <div id="usealerts" class="">
                    <p id="message">Saving changes</p>
                </div>
                <div class="user-full">
                    <div class="user-image" >                        
                        <img v-bind:src="auth_user.profile_pic" id="userPic" alt="userID">
                        <div id="editImage" class="cameraIcon">
                            <input type="file" class="theme" name="" onchange = 'prev()' id='uploadImage'>
                        </div>
                    </div>
                    <div class="user-details">
                        <p class="name theme">@{{auth_user.name}}</p>
                        <p class="user-role">@{{auth_user.role}}</p>
                    </div>
                    <!-- <input type="file" name="" id="">
                    <img src="" height="200"  alt=""> -->
                </div>
                <div class="user-summary">
                    <ul class="seed">
                        <li><a href="#profile" class="active theme">Summary</a></li>
                        <li><a href="#changepassword" class="theme">Change Password</a></li>
                    </ul>

                    <div class="tabContent">                    
                        <div id="profile" class="active">
                            <form action="" name="" id="profileForm">
                                <div>
                                    <label for="name">Full name</label>
                                    <input type="text" v-model="auth_user.name" class="fullname" name="" id="name">
                                </div>
                                <div>
                                    <label for="email">Email</label>
                                    <input type="email" v-model="new_email" class="email" name="" id="email">
                                </div>
                                
                            </form>
                            <div class="buttons">
                                <button id="editProfile" class='theme' v-on:click="saveProfile">Save Profile</button>
                            </div>
                        </div>
                        <div id="changepassword">
                            <div>
                                <label for="currentPWD">Enter current password</label>
                                <input type="password" v-model="current_pwd" name="" id="currentPWD">
                            </div>
                            <div>
                                <label for="newPWD">Enter New password</label>
                                <input type="password" v-model="new_pwd" name="" id="newPWD">
                            </div>
                            <div>
                                <label for="confirmPWD">Confirm Password</label>
                                <input type="password" v-model="confirm_pwd" name="" id="confirmPWD">
                            </div>
                            <div class="buttons">
                                <button id="savePWD" v-on:click="savePwd">Save Password</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="assets/js/jquery-1.11.2.min.js"></script> 
    <script src="{{asset('assets/js/profile_config.js')}}"></script>
    <script src="assets/js/functions.js"></script>
    <script src="{{asset('assets/js/users.js')}}"></script>
    
</body>
</html>