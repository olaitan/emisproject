<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin | Dashboard</title>
    <!-- StyleSheets -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/customized.css">
    <link rel="stylesheet" href="assets/css/users.css">
    <link rel="stylesheet" href="assets/css/Chart.min.css">
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
    <script src="{{asset('assets/js/Chart.min.js')}}"></script>
    <script>
        function getCookie(name) {
            var dc = document.cookie;
            var prefix = name + "=";
            var begin = dc.indexOf("; " + prefix);
            if (begin == -1) {
                begin = dc.indexOf(prefix);
                if (begin != 0) return null;
            }
            else
            {
                begin += 2;
                var end = document.cookie.indexOf(";", begin);
                if (end == -1) {
                end = dc.length;
                }
            }
            // because unescape has been deprecated, replaced with decodeURI
            //return unescape(dc.substring(begin + prefix.length, end));
            return decodeURI(dc.substring(begin + prefix.length, end));
        }

        var myCookie = getCookie("user");

        if (myCookie == null) {
            // do cookie doesn't exist stuff;
            window.location.assign("/login");
        }
    </script>
</head>
<body>
    <div id="app">
        <!-- Main Container -->
        <div id="main-container">
            <div id="left-tab" class="theme">
                <div class="logo">                
                    <span id="collapse"></span>
                    <a href="/"><p>NEMIS</p></a>
                </div>           
                <div class="links">
                    <ul>
                        <li><a href=""><span class="overview"></span><p>Overview</p></a></li>
                        <li><a href="/" id="moreReportLinks"><span class="reports"></span><p>Reports</p></a>
                            <ul id="extraReportLinks">
                                <li v-if="auth_user.role_id==1"><a href="/school_characteristics_report">School Characteristics</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/enrolment_report">Enrolment</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/repeaters_report">Repeaters</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_facility_report">School_Facility</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_streams_report">School_Streams</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_teachers_report">School_Teachers</a></li>                                
                            </ul>
                        </li>
                        
                        <li><a href="/search"><span class="search"></span><p>Search</p></a></li>
                        <li><a href="/new"><span class="addSchool"></span><p>Add a school</p></a></li>
                        <li ><a href="/" id="moreLinks" ><span class="settings"></span><p>Settings</p></a>
                            <ul id="extraLinks">
                                <li v-if="auth_user.role_id==1"><a href="/metadatas">Configure Metadatas</a></li>                                
                                <li v-if="auth_user.role_id==1"><a href="/lgaMetadatas">Configure LGA</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/userlogs">User Logs</a></li>
                            </ul>
                        </li>
                        <li><a href="/" id="moreOfflineLinks"><span class="offline"></span><p>Offline(Alpha)</p></a>
                            <ul id="extraOfflineLinks">
                                <li ><a href="/offline_data_entry">Form Entry</a></li>
                                <li ><a href="/offline_batch_upload">Batch Upload</a></li>  
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="users">
                    <div class="user-avatar">
                        <img v-bind:src="auth_user.profile_pic" alt="IA" srcset="" >
                    </div>
                    <h2 class="name"><a href="" id="showDetails">@{{auth_user.name}}</a></h2>
                    <p class="role">@{{auth_user.role}}</p>

                    <div class="users-links" id="users-links">
                        <ul>
                            <li><a href="/userID">View Profile</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/configure">Configure</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/edituser">Accounts</a></li>
                            <li v-on:click="logout"><a href="/login">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="right-tab">
                <div id="grid">
                    <div id="one">
                        <p class="heading">@{{total_schools}}</p>
                        <div class="heading">
                            <p>Total Schools</p>
                        </div>
                    </div>
                    <div id="two">
                        <p class="heading">@{{public_schools}}</p>
                        <div class="heading">
                            <p>Public Schools</p>
                        </div>
                    </div>
                    <div id="three">
                        <p class="heading">@{{private_schools}}</p>
                        <div class="heading">
                            <p>Private Schools</p>
                        </div>
                    </div>
                </div>
                <div id="pie_grid">
                    <div>
                        <p>Public Schools Data</p>
                        <canvas id="pie1" height="200">
                            <p>Public School Pie Chart</p>
                        </canvas>
                    </div>
                    
                    <div>
                    <p>Private Schools Data</p>
                    <canvas id="pie2" height="200">
                        <p>Private School Pie Chart</p>
                    </canvas>
                    </div>

                </div>

                <div id="line_chart">
                <p>States Data</p>
                    <canvas id="line" height="100">
                        <p>Hello Fallback World</p>
                    </canvas>
                </div>
            </div> 
        </div>
    </div>

    <script src="assets/js/jquery-1.11.2.min.js"></script> 
    <script src="{{asset('assets/js/dashboard.js')}}"></script>
    <script src="{{asset('assets/js/functions.js')}}"></script>
    <script src="{{asset('assets/js/users.js')}}"></script>
</body>
</html>