<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin | Dashboard</title>
    <!-- StyleSheets -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/customized.css">
    <link rel="stylesheet" href="assets/css/users.css">
    <!-- scripting -->
    <script src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
</head>

<body>
    <!-- Main Container -->
    <div id="app">
        <div id="main-container">
            <div id="left-tab" class="theme">
                <div class="logo">
                    <span id="collapse"></span>
                    <a href="/">
                        <p>NEMIS</p>
                    </a>
                </div>
                <div class="links">
                    <ul>
                        <li><a href="/"><span class="overview"></span>
                                <p>Overview</p>
                            </a></li>
                        <li><a href="/" id="moreReportLinks"><span class="reports"></span><p>Reports</p></a>
                            <ul id="extraReportLinks">
                            <li v-if="auth_user.role_id==1"><a href="/school_characteristics_report">School Characteristics</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/enrolment_report">Enrolment</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/repeaters_report">Repeaters</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_facility_report">School_Facility</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_streams_report">School_Streams</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_teachers_report">School_Teachers</a></li>                                
                            </ul>
                        </li>

                        <li><a href="/search"><span class="search"></span>
                                <p>Search</p>
                            </a></li>
                        <li><a href="/new"><span class="addSchool"></span>
                                <p>Add a school</p>
                            </a></li>
                        <li ><a href="/" id="moreLinks" class="moreLinks"><span class="settings"></span>
                                <p>Settings</p>
                            </a>
                            <ul id="extraLinks">
                                <li v-if="auth_user.role_id==1"><a href="/metadatas">Configure Metadatas</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/lgaMetadatas">Configure LGA</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/userlogs">User Logs</a></li>
                            </ul>
                        </li>
                        <li><a href="/" id="moreOfflineLinks"><span class="offline"></span><p>Offline(Alpha)</p></a>
                            <ul id="extraOfflineLinks">
                                <li ><a href="/offline_data_entry">Form Entry</a></li>
                                <li ><a href="/offline_batch_upload">Batch Upload</a></li>  
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="users">
                    <div class="user-avatar">
                        <img v-bind:src="auth_user.profile_pic" alt="IA" srcset="">
                    </div>
                    <h2 class="name"><a href="" id="showDetails">@{{auth_user.name}}</a></h2>
                    <p class="role">@{{auth_user.role}}</p>

                    <div class="users-links" id="users-links">
                        <ul>
                            <li><a href="/userID">View Profile</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/configure">Configure</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/edituser">Accounts</a></li>
                            <li v-on:click="logout"><a href="/login">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="right-tab">
                <div id="alert">
                    <p>Saved Successfully...</p>
                    <div class="close"></div>
                </div>
                <div id="addSchools">
                    <p class="addSchool theme">Create a New School</p>
                    <div class="wrappingDiv">
                        <div class="school_questions xtra">
                            <p class="question">School Name</p>
                            <input v-model="schoolname" class="theme" type="text" required="required" name="" id="">
                        </div> 
                    </div>
                    <div class="wrappingDiv">
                        <div class="school_questions">
                            <p class="question">Category</p>
                            <select  v-model="schooltype" required="required" v-on:change="changelevel()" class="theme" name="schooltype" id="">
                                <option value="" disabled>Choose type</option>
                                <option value="public" >Public</option>
                                <option value="private">Private School</option>
                                <option value="sciencevocational">Science and Vocational</option>
                            </select>
                        </div>                        
                        <div class="school_questions">
                            <p class="question">Year Founded</p>
                            <select v-model="year" required="required" class="theme" name="year" id="">
                                <option value="" disabled>Choose Year</option>
                                <option v-for="n in fetch_yearslist"  v-bind:value="n">@{{n}}</option>
                            </select>
                        </div>                        
                    </div>
                    <div class="wrappingDiv">
                        <div v-if="IsPublic" class="school_questions">
                            <p class="question">Level</p>
                            <select v-model="schoollevel"  class="theme" name="schoollevel" id="">
                                <option value="" disabled>Choose type</option>
                                <option value="Pre-primary">Pre-primary</option>
                                <option value="Primary">Primary</option>
                                <option value="Pre-primary and primary">Pre-primary and primary</option>
                                <option value="Junior Secondary">Junior Secondary</option>
                                <option value="Senior Secondary">Senior Secondary</option>
                                <option value="Junior and Senior Secondary">Junior and Senior Secondary</option>
                            </select>
                        </div>
                        <div v-if="IsVS" class="school_questions">
                            <p class="question">Level</p>
                            <select v-model="schoollevel"  class="theme" name="schooltype" id="">
                                <option value="" disabled>Choose type</option>
                                <option value="Junior Secondary">Junior Secondary</option>
                                <option value="Senior Secondary">Senior Secondary</option>
                                <option value="Junior and Senior Secondary">Junior and Senior Secondary</option>

                            </select>
                        </div>
                        <div v-if="IsPrivate" class="school_questions forPrivate">
                            <p class="question">Level</p>
                            <div class="private">
                                <div>
                                    <label for="pre-primary">Pre-primary</label>
                                    <input id="pre-primary" type="checkbox" value="Pre-primary" v-model="level_of_educations" name="level-of-edu">
                                </div>
                                <div>
                                    <label for="primary">Primary</label>
                                    <input id="primary" type="checkbox" value="Primary" v-model="level_of_educations" name="level-of-edu">
                                </div>
                                <div>
                                    <label for="junior">Junior Secondary</label>
                                    <input id="junior" type="checkbox" value="Junior Secondary" v-model="level_of_educations" name="level-of-edu">
                                </div>
                                <div>
                                    <label for="senior">Senior Secondary</label>
                                    <input id="senior" type="checkbox" value="Senior Secondary" v-model="level_of_educations" name="level-of-edu">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wrappingDiv">
                        <div class="school_questions">
                            <p class="question">Telephone</p>
                            <input v-model="schooltelephone" class="theme" required="required" type="tel" name="" id="" placeholder="+234 (0) 8012345678">
                        </div>
                        <div class="school_questions">
                            <p class="question">Ward</p>
                            <input v-model="schoolward" class="theme" required="required" type="text" name="" id="" placeholder="School Ward">
                        </div>
                        <div class="school_questions">
                            <p class="question">Email Address</p>
                            <input v-model="schoolemail" class="theme" required="required" type="email" name="" id="" placeholder="someone@example.com"> 
                        </div> 
                    </div>
                    <div class="wrappingDiv">
                        <div class="school_questions xtra fly">
                            <p class="question">School Address</p>
                            <input v-model="schoolstreet" class="theme" required="required" type="text" name="" id="" placeholder="Lakinode Layout, Beside Central Mosque, Idanre">
                        </div>                                
                        <div class="school_questions">
                            <p class="question">State</p>
                            <select  v-model="state_picked" class="theme" required="required" v-on:change="getLGA" name="state" id="state">
                                <option value="" disabled="disabled">Choose State</option>
                                <option  v-for="(state,index) in fetch_states" v-bind:value="index" v-if="user_role<2 || state.data.name==user_state">@{{state.data.name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="wrappingDiv">
                        <div class="school_questions">
                            <p class="question">Local Government Area</p>
                            <select v-model="lga_picked"class="theme" required="required" name="lga" id="lga">
                                <option value="" disabled="disabled">Choose LGA</option>
                                <option v-for="(lga,index) in fetch_lga" v-bind:value="index">(@{{lga.lgacode}}) - @{{lga.name}}</option>
                            </select>
                        </div>
                        <div class="school_questions">
                            <p class="question">Town</p>
                            <input v-model="schooltown" class="theme" required="required" type="town" name="" id="">
                        </div> 
                    </div>                                        
                    <div class="wrappingDiv">
                        <div id="schoolNo" class="school_questions">
                            <p class="question">School Code<span> *Would be automatically generated</span></p>
                            <input v-model="schoolcode" type="text" name="" id="" disabled="disabled">
                        </div>
                    </div>

                    <div class="specialDiv">
                        <div class="BtnAddSchool">
                            <input type="button" class="theme" v-on:click="addschool" id="addSchool" value="Add School">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="assets/js/jquery-1.11.2.min.js"></script>
    <script src="{{asset('assets/js/add_newschool.js')}}"></script>
    <script src="assets/js/functions.js"></script>
    <script src="{{asset('assets/js/users.js')}}"></script>
</body>

</html> 