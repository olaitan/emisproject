<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin | Dashboard</title>
    <!-- StyleSheets -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/assets/datatables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/users.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/forms.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/customized.css')}}">
    <!-- scripting -->
    <script src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
</head>
<body>
<div id="app">
    <!-- Main Container -->
    <div id="main-container">
        <div id="left-tab" class="theme">
            <div class="logo">
                <span id="collapse"></span>
                <a href="/"><p>NEMIS</p></a>
            </div>           
            <div class="links">
                <ul>
                    <li><a href="/"><span class="overview"></span><p>Overview</p></a></li>
                    <li><a href="/" id="moreReportLinks"><span class="reports"></span><p>Reports</p></a>
                            <ul id="extraReportLinks">
                                <li v-if="auth_user.role_id==1"><a href="/general_report">General</a></li>                                
                            </ul>
                        </li>
                    <li><a href="/search"><span class="search"></span><p>Search</p></a></li>
                    <li><a href="/new"><span class="addSchool"></span><p>Add a school</p></a></li>
                    <li ><a href="/" id="moreLinks"><span class="settings"></span><p>Settings</p></a>
                        <ul id="extraLinks">
                            <li v-if="auth_user.role_id==1"><a href="/metadatas">Configure Metadatas</a></li>
                            <li v-if="auth_user.role_id==1"><a href="/lgaMetadatas">Configure LGA</a></li>
                            <li v-if="auth_user.role_id==1"><a href="/userlogs">User Activity Logs</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="users">
                <div class="user-avatar">
                    <img v-bind:src="auth_user.profile_pic" alt="IA" srcset="" >
                </div>
                <h2 class="name"><a href="" id="showDetails">@{{auth_user.name}}</a></h2>
                <p class="role">@{{auth_user.role}}</p>

                <div class="users-links" id="users-links">
                    <ul>
                        <li><a href="/userID">View Profile</a></li>
                        <li v-if="auth_user.role_id!=4"><a href="/configure">Configure</a></li>
                        <li v-if="auth_user.role_id!=4"><a href="/edituser">Accounts</a></li>
                        <li v-on:click="logout"><a href="/login">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="right-tab" class="contents">                   
            <div id="loader" class='theme'></div>
            <div class="search-form">
                <p class="theme">Search </p>
                <div class="form">
                    <form action="" id="searchForms">
                        <div class='basicForm'>
                            <label for="">state</label>
                            <select v-model="state" v-on:change="getLgas" name="state" id="state" class="theme">
                                <option v-if="user_role==1" value="" >Select state</option>
                                <option v-for="(source,index) in fetch_states" v-if="user_role<2 || source.data.name==user_state" v-bind:value="source.data.name">@{{source.data.name}}</option>
                            </select>
                        </div>
                        <div class='basicForm'>
                            <label for="">LGA</label>
                            <select v-model="lga_picked" name="lga" id="lga" class="theme">
                                <option value="" selected="true" disabled="disabled">Select LGA</option>
                                <option v-for="(lga,index) in fetch_lga" v-bind:value="lga.name">@{{lga.name}}</option>
                            </select>
                        </div>
                        <div class='basicForm'>
                            <label for="">school category</label>
                            <select name="schooltype" id="" class="theme">
                                <option value="">Select type</option>
                                <option value="public">Public</option>
                                <option value="private">Private</option>
                                <option value="sciencevocational">Science Vocational</option>
                            </select>
                        </div>
                        <div class='basicForm'>
                            <label for="">Levels of Education offered</label>
                            <select name="schoollevel" class='theme' id="">                                      
                                <option value="">Choose Level</option>
                                <option value="Pre-primary">Pre-primary</option>
                                <option value="Primary">Primary</option>
                                <option value="Pre-primary and primary">Pre-primary and Primary</option>
                                <option value="Junior Secondary">Junior secondary</option>
                                <option value="Senior Secondary">Senior secondary</option>
                                <option value="Junior and Senior Secondary">Junior and Senior Secondary</option>
                                
                            </select>
                        </div> 
                        <div class='basicForm'>
                            <label for="">Census Year</label>
                            <select class="theme" name="census_year" id="">
                                <option value="" selected="selected" >Choose Year</option>
                                <option v-for="n in fetch_yearslist"  v-bind:value="n">@{{n}}</option>
                            </select>
                        </div>                       
                        <div class='hS hiddenSearch'>
                            <label for="">Location Of School</label>  
                            <select name="locationofschool" class='theme' id="">                                           
                                <option value="">Choose</option>
                                <option value="Rural">Rural</option>
                                <option value="Urban">Urban</option>
                            </select>               
                        </div>
                        <div class='hS hiddenSearch'>
                            <label for="">Types of school</label>
                            <select name="typesofschool" class='theme' id="">
                                <option value="">Choose</option>
                                <option value="Regular">Regular</option>
                                <option value="Qur'anic Islamiyya">Qur'anic Islamiyya</option>
                                <option value="Islamiyya Integrated">Islamiyya Integrated</option>
                                <option value="Vocational Training Centre">Vocational Training Centre</option>
                                <option value="Nomadic">Nomadic</option>
                            </select>
                        </div>
                        <!-- <div class='hS hiddenSearch'>
                            <label for="">School Grants</label>
                            <select name="grants" class='theme' id="">
                                <option value="">Choose</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class='hS hiddenSearch'>
                            <label for="">Security Guards</label>
                            <select name="guards" class='theme' id="">
                                <option value="">Choose</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div> -->
                        <div class='hS hiddenSearch'>
                            <label for="">Operate Shift</label>
                            <select name="operateshift" class='theme' id="">
                                <option value="">Choose</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class='hS hiddenSearch'>
                            <label for="">School Development Plan</label>
                            <select name="sdp" class='theme' id="">
                                <option value="">Choose</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        
                        <div class='hS hiddenSearch'>
                            <label for="">PTA</label>
                            <select name="pta" class='theme' id="">
                                <option value="">Choose</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class='hS hiddenSearch'>
                            <label for="">School Grants</label>
                            <select name="grants" class='theme' id="">
                                <option value="">Choose</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class='hS hiddenSearch'>
                            <label for="">School Guard</label>
                            <select name="guard" class='theme' id="">
                                <option value="">Choose</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class='hS hiddenSearch'>
                            <label for="">Ownership</label>
                            <select name="ownership" class='theme' id="">
                                <option value="">Choose</option>
                                <option value="State">State</option>
                                <option value="LGEA">LGEA</option>
                                <option value="Community">Community</option>
                                <option value="Federal">Federal</option>
                            </select>
                        </div>
                        <div class='hS hiddenSearch'>
                            <label for="">School Based Management Committee</label>
                            <select name="sbmc" class='theme' id="">
                                <option value="">Choose</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </form>
                    <div id="search-button">
                    <button type="submit" id="searchBTN" class="theme" v-on:click="showdata">Search</button>
                    <button v-on:click='advanceSearch' class='advanceSearch theme'>Advance Search</button>
                    </div>
                </div>
            </div>
            <div class="search-result table-responsive">
                <p class="theme">Reports result</p>
                <table class="table table-hover" id="school_search_results">
                    <thead>
                        <tr>
                            <th>State</th>
                            <th>LGA</th>
                            <th>School</th>
                            <th>SchoolCode</th>
                            <th>Location</th>
                            <th>Multigrade</th>
                            <th>Ownership</th>
                            <th>SharedFacilities</th>
                            <th>OperationalMode</th>
                            <th>SchoolType</th>
                            <th>Sector</th>
                            <th>SchoolLevel</th>
                            <th>ReportedInCensus</th>
                            <th>Census Year</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
    

<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/nemis_general_report.js')}}"></script>
<script src="{{asset('assets/assets/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/functions.js')}}"></script>
<script src="{{asset('assets/js/users.js')}}"></script>
<script>
    var table = $('#school_search_results').DataTable( {
        "dataSrc":"",
        "columns": [
            { "data": "data.state" },
            { "data": "data.lga" },
            { "data": "data.name" },
            { "data": "data.schoolcode" },
            { "data": "data.location" },
            { "data": "data.multigrade" },
            { "data": "data.ownership" },
            { "data": "data.sharedfacilities" },
            { "data": "data.operationalmode" },
            { "data": "data.schooltype" },
            { "data": "data.sector" },
            { "data": "data.schoollevel" },
            { "data": "data.reported_in_census" },
            { "data": "data.census_year" }
        ]
    } );
</script>
</body>
</html>