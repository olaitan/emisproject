<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NEMIS - App Client</title>    
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/customized.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <!-- scripting -->
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
</head>
<body>
<div id="app">
    <div id="loginApp">
        <div class="header">
            <h2><a href="/">NEMIS</a></h2>
            <div class="nav">
                <ul>
                    <li><a href="/docs">Documentation</a></li>
                </ul>
            </div>
            <div class="logs">
                <button v-on:click="logout">Logout</button>
            </div>
        </div>

        <div class="mainWrap caseID">
            <div class="contentWrap">
                <p class="p_Caption">APP CLIENT</p>
                <div class="appID"><p>@{{user_email}} </p></div>
            </div>
            <div class="contentWrap">
                <p class="p_Caption">SECRET TOKEN</p>
                <div class="appID"><p>@{{token}}</p></div>
            </div>
        </div>

        <div class="footer">
            <p>Copyright &copy 2018</p>
        </div>
    </div>
</div>
<script src="{{asset('assets/js/appclient.js')}}"></script>
</body>
</html>
