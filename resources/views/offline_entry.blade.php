<!--THIS IS PUBLIC FORM-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Offline Data Entry</title>
    <!-- StyleSheets -->
    <link rel="shortcut icon" href="{{asset('assets/icons/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/parsley.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/customized.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/forms.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/users.css')}}">
    <!-- scripting -->
    <script src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
</head>

<body>
    <div id="app">
        <!-- Main Container -->
        <div id="main-container">

            <!-- Start Dialog Box -->
            <div id="coverLayer"></div>

            <!-- For the staff -->
            <div id="dialog-form">
                <div id="cancel" class="closeme">
                    <p class="dialog-label"></p>
                </div>
                <!-- Removing of Staff -->
                <div class="removeStaff" id="clearStaff">
                    <div class="exit">
                        <p id="closers">X</p>
                    </div>
                    <div class="form-Box">
                        <p class="dialog-label">Remove Staff</p>
                        <form action="" id="RemoveStaffForm">
                            <div class="">
                                <div class="formRow">
                                    <label for="">Category</label>
                                    <select v-model="removalcat" name="cat-option" required="required" id="">
                                        <option v-for="(source,index) in fetch_removalcategory" v-bind:value="source.value"> @{{source.value}}</option>
                                    </select>
                                </div>
                                <div v-if="removalcat==='Transferred'" class="formRow">
                                    <label for="">School</label>
                                    <select v-model="stateschool_picked" name="school" required="required">
                                        <option v-for="(source,index) in fetch_stateschools" v-bind:value="source.data.schoolcode"> @{{source.data.name}}</option>4
                                    </select>
                                </div>
                                <div class="formRow">
                                    <label for="staff-year">year</label>
                                    <input type="text" v-model="censusyear" class="StaffNum" name="" id="" placeholder="2002">
                                </div>
                            </div>
                        </form>
                        <div id="Buttons">
                            <button id="Remove" v-on:click="removeStaff">Remove Staff</button>
                        </div>
                    </div>
                </div>
                <!-- Navigation Tabs -->
                <nav>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="Staff-List" data-toggle="tab" href="#StaffList" role="tab" aria-controls="home" aria-selected="true">Existing Staff List</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Add-Staff" data-toggle="tab" href="#AddStaff" role="tab" aria-controls="profile" aria-selected="false">Add New Staff</a>
                        </li>
                    </ul>
                </nav>

                <!-- Tab panes -->
                <div class="tab-content" id="allowFlow">
                    <div class="tab-pane active" id="StaffList" role="tabpanel" aria-labelledby="Staff-List">
                        <p class="dialog-label">Current Staff List</p>
                        <div class="table-responsive mainCover">
                            <table class="table table-hover" id="StaffList">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Actions</th>
                                        <th scope="col">Staff file no</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Gender</th>
                                        <th scope="col">Type of staff</th>
                                        <th scope="col">Source of Salary</th>
                                        <th scope="col">Year of Birth</th>
                                        <th scope="col">Year of First Appointment</th>
                                        <th scope="col">Year of Present Appointment</th>
                                        <th scope="col">Year of Posting to this School</th>
                                        <th scope="col">Grade level/Step</th>
                                        <th scope="col">Present</th>
                                        <th scope="col">Highest Academic Qualification</th>
                                        <th scope="col">Teaching Qualification</th>
                                        <th scope="col">Area of Specialisation</th>
                                        <th scope="col">Main Subbject Taught</th>
                                        <th scope="col">Teaching type</th>
                                        <th scope="col">Teaches both junior & senior secondary classes</th>
                                        <th scope="col">Teacher attended training workshop / seminar</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(source,index) in fetch_staffs">
                                        <td>@{{index+1}}</td>
                                        <td id="actionBtn" v-on:click="showRemoveDialog(index)">
                                            <p id="more"></p>
                                        </td>
                                        <td><input type="text" v-model="source.staff_file_no" class="StaffNum" name="" id="" placeholder="P456"></td>
                                        <td><input type="text" v-model="source.staff_name" class="StaffName" name="" id="" placeholder="Fred Abdul"></td>
                                        <td>
                                            <select v-model="source.staff_gender" name="gender" id="">
                                                <option value="" disabled="disabled"></option>
                                                <option value="f">F</option>
                                                <option value="m">M</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select v-model="source.staff_type" name="type-of-staff" id="">
                                                <option v-for="(source,index) in fetch_stafftype" v-bind:value="source.value"> @{{source.value}}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select v-model="source.salary_source" name="source-of-salary" id="">
                                                <option v-for="(source,index) in fetch_salarysource" v-bind:value="source.value"> @{{source.value}}</option>
                                            </select>
                                        </td>
                                        <td><input type="number" min="0" v-model="source.staff_yob" class="yob" name="" id="" placeholder=""></td>
                                        <td><input type="number" min="0" class="yofa" v-model="source.staff_yfa" name="" id="" placeholder=""></td>
                                        <td><input type="number" min="0" class="yopa" name="" id="" v-model="source.staff_ypa" placeholder=""></td>
                                        <td><input type="number" min="0" class="yoposting" name="" v-model="source.staff_yps" id="" placeholder=""></td>
                                        <td><input class="Grade" v-model="source.staff_level" v-on:change="checkgrade(source.staff_level,$event)" type="text" name="" id="" placeholder="7/2"></td>
                                        <td>
                                            <select v-model="source.present" name="present-academic-qualification" id="">
                                                <option v-for="(source,index) in fetch_present" v-bind:value="source.value"> @{{source.value}}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select v-model="source.academic_qualification" name="teaching-qualification" id="">
                                                <option v-for="(source,index) in fetch_academicqualification" v-bind:value="source.value"> @{{source.value}}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select v-model="source.teaching_qualification" name="subject-qualification" id="">
                                                <option v-for="(source,index) in fetch_teachingqualification" v-bind:value="source.value"> @{{source.value}}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select v-model="source.area_specialisation" name="area-of-specialization" id="">
                                                <option v-for="(source,index) in fetch_specialisation" v-bind:value="source.value"> @{{source.value}}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select v-model="source.subject_taught" name="main-subject-taught" id="">
                                                <option v-for="(source,index) in fetch_subjecttaught" v-bind:value="source.value"> @{{source.value}}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select v-model="source.teaching_type" name="teaching-type" id="">
                                                <option v-for="(source,index) in fetch_teachingtype" v-bind:value="source.value"> @{{source.value}}</option>
                                            </select>
                                        </td>
                                        <td><input type="checkbox" name="teach-both-jss-sss" id=""></td>
                                        <td><input type="checkbox" name="teach-both-jss-sss" id=""></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <button id="addStaff" v-on:click="saveListOfStaff">Save Changes</button>
                        </div>
                    </div>
                    <div class="tab-pane" id="AddStaff" role="tabpanel" aria-labelledby="Add-Staff">
                        <div class="addNewStaff" id="sizingStaff">
                            <div class="form-Box">
                                <p class="dialog-label">New Staff Entry</p>
                                <form action="" id="NewStaffForm">
                                    <div class="category">
                                        <div class="formRow">
                                            <label for="staff_id">staff file no.</label>
                                            <input v-model="staff_file_no" type="text" id="staff_id">
                                        </div>
                                        <div class="formRow">
                                            <label for="staff_name">name of staff</label>
                                            <input v-model="staff_name" type="text" id="staff_name">
                                        </div>
                                        <div class="formRow specials">
                                            <div>
                                                <label for="staff-gender">gender</label>
                                                <select v-model="staff_gender" name="gender" required="required" id="">
                                                    <option value="" disabled></option>
                                                    <option value="m">Male</option>
                                                    <option value="f">Female</option>
                                                </select>
                                            </div>
                                            <div>
                                                <label for="type-of-staff">type of staff</label>
                                                <select v-model="staff_type" name="source-of-salary" id="type-of-staff">
                                                    <option v-for="(source,index) in fetch_stafftype" v-bind:value="source.value"> @{{source.display}}</option>
                                                </select>
                                            </div>
                                            <div>
                                                <label for="staff_id">source of salary</label>
                                                <select v-model="salary_source" name="source-of-salary" id="type-of-staff">
                                                    <option v-for="(source,index) in fetch_salarysource" v-bind:value="source.value"> @{{source.display}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="formRow specials">
                                            <div>
                                                <label for="staff-year">year of birth</label>
                                                <select name="year" v-model="staff_yob" required="required" id="staff-year">
                                                    <option value="" disabled>Choose Year</option>
                                                    <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                                </select>
                                            </div>
                                            <div>
                                                <label for="staff-gender">year of first appointment</label>
                                                <select name="year-appoint" v-model="staff_yfa" required="required" id="year-appoint">
                                                    <option value="" disabled>Choose Year</option>
                                                    <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                                </select>
                                            </div>
                                            <div>
                                                <label for="type-of-staff">year of present appointment</label>
                                                <select name="" v-model="staff_ypa" required="required" id="">
                                                    <option value="" disabled>Choose Year</option>
                                                    <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="formRow specials">
                                            <div>
                                                <label for="">year of posting to school</label>
                                                <select name="" v-model="staff_yps" required="required" id="">
                                                    <option value="" disabled>Choose Year</option>
                                                    <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                                </select>
                                            </div>
                                            <div>
                                                <label for="">Grade level/Step</label>
                                                <select v-model="staff_level" name="" required="required" id="">
                                                    <option value=""></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                            <div>
                                                <label for="">present</label>
                                                <select v-model="present" name="present-academic-qualification" id="">
                                                    <option v-for="(source,index) in fetch_present" v-bind:value="source.value"> @{{source.display}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="formRow specials">
                                            <div>
                                                <label for="">academic qualification</label>
                                                <select v-model="academic_qualification" name="teaching-qualification" id="">
                                                    <option v-for="(source,index) in fetch_academicqualification" v-bind:value="source.value"> @{{source.display}}</option>
                                                </select>
                                            </div>
                                            <div>
                                                <label for="">teaching qualification</label>
                                                <select v-model="teaching_qualification" name="subject-qualification" id="">
                                                    <option v-for="(source,index) in fetch_teachingqualification" v-bind:value="source.value"> @{{source.display}}</option>
                                                </select>
                                            </div>
                                            <div>
                                                <label for="">area of specialisation</label>
                                                <select v-model="area_specialisation" name="area-of-specialization" id="">
                                                    <option v-for="(source,index) in fetch_specialisation" v-bind:value="source.value"> @{{source.display}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="formRow specials">
                                            <div>
                                                <label for="">main subject taught</label>
                                                <select v-model="subject_taught" name="main-subject-taught" id="">
                                                    <option v-for="(source,index) in fetch_subjecttaught" v-bind:value="source.value"> @{{source.display}}</option>
                                                </select>
                                            </div>
                                            <div>
                                                <label for="">teaching type</label>
                                                <select v-model="teaching_type" name="teaching-type" id="">
                                                    <option v-for="(source,index) in fetch_teachingtype" v-bind:value="source.value"> @{{source.display}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="formRow">
                                            <label for="">Tick box if teacher also teaches senior secondary classes in this school</label>
                                            <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="formRow">
                                            <label for="">Tick box if teacher attended training</label>
                                            <input type="checkbox" v-model="attended_training" value="1" name="" id="">
                                        </div>
                                    </div>
                                </form>
                                <div id="Buttons">
                                    <button id="Next" class="theme" v-on:click="submitStaff">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- For Classrooms -->
            <div id="addClassRooms">
                <div class="classes">
                    <div class="form-Box">
                        <p class="dialog-label">Information on new Classrooms</p>
                        <form action="" id="StaffForm">
                            <div>
                                <div class="formRow">
                                    <label for="staff_id">Year of construction</label>
                                    <select v-model="year_of_construction" name="" id="">
                                        <option value="" disabled>Choose Year</option>
                                        <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                    </select>
                                </div>
                                <div class="formRow">
                                    <label for="">Present condition</label>
                                    <select v-model="present_condition" name="source-of-salary" id="type-of-staff">
                                        <option v-for="(source,index) in fetch_presentcondition" v-bind:value="source.value"> @{{source.display}}</option>
                                    </select>
                                </div>
                                <div class="formRow special">
                                    <div class="left">
                                        <label for="">Length(metre)</label>
                                        <input type="number" min="1" max="10" v-model="classroom_length" name="" id="">
                                    </div>
                                    <div class="right">
                                        <label for="">Width(metre)</label>
                                        <input type="number" min="1" max="10" v-model="classroom_width" name="" id="">
                                    </div>
                                </div>
                                <div class="formRow special">
                                    <div>
                                        <label for="">Floor material</label>
                                        <select v-model="floor_material" name="source-of-salary" id="type-of-staff">
                                            <option v-for="(source,index) in fetch_floormaterial" v-bind:value="source.value"> @{{source.display}}</option>
                                        </select>
                                    </div>
                                    <div>
                                        <label for="year-appoint">Wall Materials</label>
                                        <select v-model="wall_material" name="source-of-salary" id="type-of-staff">
                                            <option v-for="(source,index) in fetch_wallmaterial" v-bind:value="source.value"> @{{source.display}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="formRow special">
                                    <div>
                                        <label for="">Roof Material</label>
                                        <select v-model="roof_material" name="source-of-salary" id="type-of-staff">
                                            <option v-for="(source,index) in fetch_roofmaterial" v-bind:value="source.value"> @{{source.display}}</option>
                                        </select>
                                    </div>
                                    <div>
                                        <label for="">Seating</label>
                                        <select v-model="seating" name="" required="required" id="">
                                            <option value="" disabled></option>
                                            <option value="1">1-Yes</option>
                                            <option value="0">2-No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="formRow">
                                    <label for="">Good black board</label>
                                    <select v-model="blackboard" name="" required="required" id="">
                                        <option value="" disabled></option>
                                        <option value="1">1-Yes</option>
                                        <option value="0">2-No</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div id="Buttons">
                            <button id="submitClassRoom" type="submit" v-on:click="submitClassroom">Submit</button>
                            <button id="cancelClassRoom">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- For Workshops -->
            <div id="addWorkShop">
                <div class="classes">
                    <div class="form-Box">
                        <p class="dialog-label">Information on new Workshops</p>
                        <form action="">
                            <div class="formRow special">
                                <div>
                                    <label for="">Type of workshop</label>
                                    <select name="" id="">
                                        <option value="" disabled>Choose Year</option>
                                        <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                    </select>
                                </div>
                                <div>
                                    <label for="">Shared</label>
                                    <select name="" id="">
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                        <option value="">4</option>
                                        <option value="">5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="formRow special">
                                <div>
                                    <label for="">Year of Construction</label>
                                    <select name="" id="">
                                        <option value="" disabled>Choose Year</option>
                                        <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                    </select>
                                </div>
                                <div>
                                    <label for="">Present Condition</label>
                                    <select name="" id="">
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                        <option value="">4</option>
                                        <option value="">5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="formRow special">
                                <div>
                                    <label for="">Length(metres)</label>
                                    <input type="number" min="0" name="" id="">
                                </div>
                                <div>
                                    <label for="">Width(metres)</label>
                                    <input type="number" min="0" name="" id="">
                                </div>
                            </div>
                            <div class="formRow special">
                                <div>
                                    <label for="">Floor material</label>
                                    <select name="" id="">
                                        <option value="" disabled>Choose Year</option>
                                        <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                    </select>
                                </div>
                                <div>
                                    <label for="">Walls material</label>
                                    <select name="" id="">
                                        <option value="" disabled>Choose Year</option>
                                        <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="formRow special">
                                <div>
                                    <label for="">Roof Material</label>
                                    <select name="" id="">
                                        <option value="" disabled>Choose Year</option>
                                        <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                    </select>
                                </div>
                                <div>
                                    <label for="">Seating</label>
                                    <select name="" id="">
                                        <option value="" disabled>Choose Year</option>
                                        <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="formRow">
                                <div>
                                    <label for="">Good blackboard</label>
                                    <select name="" id="">
                                        <option value="" disabled>Choose Year</option>
                                        <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div id="dialog-btn">
                            <ul>
                                <li><button id="submit-workshop" type="submit">Submit</button></li>
                                <li><button id="cancelWks">Cancel</button></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <div id="left-tab" class="theme">
                <div class="logo">
                    <span id="collapse"></span>
                    <a href="/">
                        <p>NEMIS</p>
                    </a>
                </div>
                <div class="links">
                    <ul>
                        <li><a href="/"><span class="overview"></span>
                                <p>Overview</p>
                            </a></li>
                        <li><a href="/" id="moreReportLinks"><span class="reports"></span><p>Reports</p></a>
                            <ul id="extraReportLinks">
                            <li v-if="auth_user.role_id==1"><a href="/school_characteristics_report">School Characteristics</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/enrolment_report">Enrolment</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/repeaters_report">Repeaters</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_facility_report">School_Facility</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_streams_report">School_Streams</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_teachers_report">School_Teachers</a></li>                                
                            </ul>
                        </li>
                        <li><a href="/search"><span class="search"></span>
                                <p>Search</p>
                            </a></li>
                        <li><a href="/new"><span class="addSchool"></span>
                                <p>Add a school</p>
                            </a></li>
                        <li ><a href="/" id="moreLinks" class="moreLinks"><span class="settings"></span>
                                <p>Settings</p>
                            </a>
                            <ul id="extraLinks">
                                <li v-if="auth_user.role_id==1"><a href="/metadatas">Configure Metadatas</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/lgaMetadatas">Configure LGA</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/userlogs">User Logs</a></li>
                            </ul>
                        </li>
                        <li><a href="/" id="moreOfflineLinks"><span class="overview"></span><p>Offline(Alpha)</p></a>
                            <ul id="extraOfflineLinks">
                                <li ><a href="/offline_data_entry">Form Entry</a></li>
                                <li ><a href="/offline_batch_upload">Batch Upload</a></li>  
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="users">
                    <div class="user-avatar">
                        <img v-bind:src="auth_user.profile_pic" alt="IA" srcset="">
                    </div>
                    <h2 class="name"><a href="" id="showDetails">@{{auth_user.name}}</a></h2>
                    <p class="role">@{{auth_user.role}}</p>
                    <div class="users-links" id="users-links">
                        <ul>
                            <li><a href="/userID">View Profile</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/configure">Configure</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/edituser">Accounts</a></li>
                            <li v-on:click="logout"><a href="/login">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            
            <div id="right-tab">
                <div id="alert">
                    <p v-text="responseMsg"></p>
                    <div class="close"></div>
                </div>
                <div id="loader" class="theme"></div>
            
                <!-- Breadcrumb Div -->
                <div>
                    <ul class="breadcrumb alert-success">
                        <li class="breadcrumb-item"><a >Home</a></li>
                        <li class="breadcrumb-item"><a >@{{schoolstate}}</a></li>
                        <li class="breadcrumb-item"><a >@{{schoollga}}</a></li>
                        <li class="breadcrumb-item"><a >Public </a></li>
                        <li class="breadcrumb-item"><a >@{{level_of_education}}</a></li>
                        <li class="breadcrumb-item active">@{{schoolname}}</li>
                    </ul>
                    <a style="color:blue" v-on:click='backHome' v-if='school_form_type' id=''> << Back Home </a>
                    <a style="color:green;float:right;" v-on:click='exportFormData' v-if='registered_censusyear' id=''> (- Export Json-) </a>
                </div>
            
                <!-- Forms and Navigation -->
                <div>
                    <!-- Div containing the various tabs -->
                    <div class="tab-contents" id="content">
                        <div id="tab1" class="show-current-tab">
                            <p class="label">Choose Form Type</p>
                            <form action="">                                
                                <div class="row">
                                    <div class="question">
                                        <p>Form Type</p>
                                    </div>
                                    <div class="answer">
                                        <select v-model="school_form_type" v-on:change="changeformtype" required class="theme" name="" id="">
                                            <option value="" >Choose Type</option>
                                            <option disabled value="1">Private</option>
                                            <option disabled value="2">Public Pre-primary and Primary</option>
                                            <option value="3">Public Secondary</option>
                                            <option disabled value="4">Science and Vocational</option>
                                        </select>
                                        </div>
                                    </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    
                                    <li><button id="next" v-if='school_form_type' v-on:click="saveCensusYear" class="theme">next</button></li>
                                </ul>
                            </div>
                        </div>


                        <!--Public Secondary-->
                        <div id="tab2" v-if="school_form_type==3">
                            <p class="label">Choose Census Year</p>
                            <form action="">                                
                                <div class="row">
                                    <div class="question">
                                        <p>Census Year</p>
                                    </div>
                                    <div class="answer">
                                        <select v-model="registered_censusyear" v-on:change="changecensusyear" required class="theme" name="" id="">
                                            <option value="" >Choose Year</option>
                                            <option v-for="n in fetch_yearslist"   v-bind:value="n">@{{n}}</option>
                                        </select>
                                        </div>
                                    </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveCensusYear">save</button></li>
                                    <li><button id="next" v-on:click="saveCensusYear" class="theme">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab3" v-if="school_form_type==3">
                            <p class="label">A. School Identification</p>
                            <form action="">
                            <p v-if="school_identification_errors.length" style="color:red">
                                <b>Please correct the following error(s):</b>
                                <ul style="list-style-type: circle !important">
                                    <li style="color:red" v-for="error in school_identification_errors">-> @{{ error }}</li>
                                </ul>
                            </p>
                                <div class="row">
                                    <div class="question">
                                        <p>School Code</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" v-model="schoolcode" class="theme" name="schoolcode" id="schoolcode" required="required" placeholder="School Code">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>School Coordinates</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" v-model="xcoordinate" name="school_elevation" class="theme" id="elevation" required="required" placeholder="Elevation (Meter)">
                                        <input type="text" v-model="ycoordinate" name="school_latitude" class="theme" id="latitude_north" required="required" placeholder="Latitude  North">
                                        <input type="text" v-model="zcoordinate" name="school_longitude" class="theme" id="longitude_east" required="required" placeholder="Longitude East">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.1 school name</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" v-model="schoolname" name="schoolname" class="theme" id="schoolname" required="required" placeholder="School Name">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.2 Number and Street Address</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" name="schoolstreet" v-model="schoolstreet" class="theme" id="schoolstreet" required="required" placeholder="Number and Street">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.3 state</p>
                                    </div>
                                    <div class="answer">
                                        <select v-model="state_picked" class="theme" v-on:change="getLGA" name="year" id="">
                                            <option v-for="(state,index) in fetch_states" v-bind:value="index">(@{{state.code}}) - @{{state.name}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.4 LGA</p>
                                    </div>
                                    <div class="answer">
                                        <select v-model="lga_picked" class="theme" name="year" id="">
                                            <option v-for="(lga,index) in fetch_lga" v-bind:value="index">(@{{lga.code}}) - @{{lga.name}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.5 Name of Village or Town</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" v-model="schooltown" name="schooltown" id="schooltown" class="theme" required="required" placeholder="Village or Town">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.6 ward</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" v-model="schoolward" name="schoolward" class="theme" id="schoolward" required="required" placeholder="School Ward">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.7 School Telephone</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" v-model="schooltelephone" name="schooltelephone" class="theme" id="schooltelephone" required="required" placeholder="School Telephone">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.8 E-Mail Address</p>
                                    </div>
                                    <div class="answer">
                                        <input type="email" v-model="schoolemail" name="schoolemail" id="schoolemail" class="theme" required="required" placeholder="School Email Address">
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveIdentification">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveIdentification">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab4" v-if="school_form_type==3">
                            <p class="label">B. School Characteristics</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>B.1 Year of establishment</p>
                                    </div>
                                    <div class="answer">
                                        <select v-model="year_of_establishment" class="theme" name="" id="">
                                            <option value="" disabled>Choose Year</option>
                                            <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.2 Location</p>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li v-for="(location,index) in fetch_location"><input type="radio" v-bind:value="location.value" v-model="location_of_school" name="location" required="required" id="urban"><label for="urban">@{{location.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.3 Levels of education offered</p>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li v-for="(level,index) in fetch_levels"><input type="radio" v-bind:value="level.value" v-on:change="setLevelBool" v-model="level_of_education" name="level-of-edu" required="required" id="level-of-edu-js"><label for="level-of-edu-js">@{{level.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.4 Type of school</p>
                                        <em>Tick only one to describe school</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li v-for="(category,index) in fetch_category"><input type="radio" v-bind:value="category.value" v-model="type_of_school" name="type-of-school" required="required" id="type-of-school-reg"><label for="">@{{category.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.5 Shifts</p>
                                        <em>Does the School operate shift system?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" name="shifts" v-model="shifts_choice" value="1" id="shifts_yes"><label for="shifts_yes">Yes</label></li>
                                            <li><input type="radio" name="shifts" v-model="shifts_choice" value="0" id="shifts_no"><label for="shifts_no">No</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.6 Shared facilities</p>
                                        <em>Does the school share facilities/Teachers/premises with any other school?</em>
                                        <p>If Yes . How many Schools are sharing facilities: </p>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" name="shared-facilities" v-model="facilities_choice" value="1" required="required" id="shared-facilities_yes"><label for="shared-facilities_yes">Yes</label></li>
                                            <li><input type="radio" name="shared-facilities" v-model="facilities_choice" value="0" required="required" id="shared-facilities_no"><label for="shared-facilities_no">No</label></li>
                                        </ul>
                                        <input type="number" min="0" name="" v-model="facilities_shared" required="required" id="no_shared-facilities">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.7 Multi-grade teaching</p>
                                        <em>Does any teacher teach more than one class at the same time?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" name="multi-grade" v-model="multigrade_choice" value="1" required="required" id="multi-grade_yes"><label for="multi-grade_yes">Yes</label></li>
                                            <li><input type="radio" name="multi-grade" v-model="multigrade_choice" value="0" required="required" id="multi-grade_no"><label for="multi-grade_no">No</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.8 School: Average Distance from Catchment communities</p>
                                        <em>What is average distance of school from its catchment areas</em>
                                    </div>
                                    <div class="answer helper">
                                        <input type="number" min="0" name="" v-model="average_distance" class="theme" required="required" id="avg-distance"><label for="avg-distance">kilometres (Enter 0 if within 1 km)</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.9 Students: Distance from School</p>
                                        <em>How many students live further than 3km from the school?</em>
                                    </div>
                                    <div class="answer helper">
                                        <input type="number" min="0" name="" class="theme" v-model="student_distance" required="required" id="student-distance"><label for="student-distance">students</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.10 Students: Boarding</p>
                                        <em>How many students board at the school premises?</em>
                                    </div>
                                    <div class="answer helper">
                                        <ul>
                                            <li><input type="radio" name="isboarding" v-model="boarding_choice" value="1" required="required" id="shared-facilities_yes"><label for="shared-facilities_yes">Yes</label></li>
                                            <li><input type="radio" name="isboarding" v-model="boarding_choice" value="0" required="required" id="shared-facilities_no"><label for="shared-facilities_no">No</label></li>
                                        </ul>
                                        <input type="number" min="0" class="theme" name="boarding" v-model="students_boarding_male" required="required" id="boarding_male"><label for="boarding_male">male</label>
                                        <input type="number" min="0" class="theme" name="boarding" v-model="students_boarding_female" required="required" id="boarding_female"><label for="boarding_female">female</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.11 School Development Plan (SDP)</p>
                                        <em>Did the school prepare SDP in the last school year?</em>
                                    </div>
                                    <div class="answer">
                                    
                                        <ul>
                                            <li><input type="radio" name="sdp" v-model="sdp_choice" value="1" required="required" id="sdp_yes"><label for="sdp_yes">Yes</label></li>
                                            <li><input type="radio" name="sdp" v-model="sdp_choice" value="0" required="required" id="sdp_no"><label for="sdp_no">No</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.12 School Based Management Committee (SBMC)</p>
                                        <em>Does the school have SBMC, which met at least once last year?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" name="sbmc" v-model="sbmc_choice" value="1" required="required" id="sbmc_yes"><label for="sbmc_yes">Yes</label></li>
                                            <li><input type="radio" name="sbmc" v-model="sbmc_choice" value="0" required="required" id="sbmc_no"><label for="sbmc_no">No</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.13 Parents’-Teachers’ Association (PTA) / Parents Forum (PF)</p>
                                        <em>Does the school have PTA / PF, which met at least once last year?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" name="pta" v-model="pta_choice" value="1" required="required" id="pta_yes"><label for="pta_yes">Yes</label></li>
                                            <li><input type="radio" name="pta" v-model="pta_choice" value="0" required="required" id="pta_no"><label for="pta_no">No</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.14 Date of Last Inspection Visit</p>
                                        <em>When was the school last inspected?</em>
                                        <p>Number of inspection Visit in last academic year</p>
                                    </div>
                                    <div class="answer helper">
                                        <input type="date" class="theme" name="" v-model="date_inspection" required="required" id="">
                                        <input type="number" min="0" class="theme" name="" v-model="no_of_inspection" id=""><label for="">No.</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.15 Authority of Last Inspection</p>
                                        <em>Which authority conducted the last inspection visit?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li v-for="(authority,index) in fetch_authority"><input type="radio" v-bind:value="authority.value" v-model="authority_choice" name="authority-inspection" required="required" id="authority-inspection_fed"><label for="authority-inspection_fed">@{{authority.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.16 Conditional Cash Transfer</p>
                                        <em>How many pupils benefitted from Conditional Cash Transfer?</em>
                                    </div>
                                    <div class="answer helper">
                                        <input type="number" class="theme" name="" v-model="cash_transfer" required="required" id=""><label for="">No</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.17 School Grants</p>
                                        <em>Has your school ever received grants in the last 2 academic sessions?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" name="school_grants" v-model="grants_choice" value="1" required="required" id="school_grants_yes"><label for="school_grants_yes">yes</label></li>
                                            <li><input type="radio" name="school_grants" v-model="grants_choice" value="0" required="required" id="school_grants_no"><label for="school_grants_no">no</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.18 Security Guard</p>
                                        <em>Does the school have a security guard?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" name="security_guard" v-model="guard_choice" value="1" required="required" id="security_guard_yes"><label for="security_guard_yes">yes</label></li>
                                            <li><input type="radio" name="security_guard" v-model="guard_choice" value="0" required="required" id="security_guard_no"><label for="security_guard_no">no</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.19 Ownership</p>
                                        <em>Which of the tier of Govt. owned this school</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li v-for="(owner,index) in fetch_ownership"><input type="radio" v-bind:value="owner.value" v-model="ownership_choice" name="ownership" required="required" id="ownership_comm"><label for="ownership_comm">@{{owner.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveCharacteristics">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveCharacteristics">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab5" v-if="school_form_type==3">
                            <p class="label">C.1 Number of Children with Birth Certificates</p>
                            <form action="" class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td class="effectGray text label-effect" rowspan="3"><label for="">How many children were enrolled with Birth certificates</label></td>
                                    </tr>
                                    <tr>
                                        <td class="effectGray label-effect" v-if="isjs" colspan="2"><label for="">JSS 1</label></td>
                                        <td class="effectGray label-effect" v-if="isss" colspan="2"><label for="">SSS 1</label></td>
                                    </tr>
                                    <tr>
                                        <td v-if="isjs" class="effectGray label-effect"><label>Male<label></td>
                                        <td v-if="isjs" class="effectGray label-effect"><label>Female<label></td>
                                        <td v-if="isss" class="effectGray label-effect"><label>Male<label></td>
                                        <td v-if="isss" class="effectGray label-effect"><label>Female<label></td>
                                    </tr>
                                    <tr v-for="(birth_certificate,index) in fetch_birth_certificate">
                                        <td><label for="">@{{birth_certificate.value}}</label></td>
                                        <td v-if="isjs"><input class="theme"  v-model="birth_certificate.male" min="0" type="number"></td>
                                        <td v-if="isjs"><input class="theme"  v-model="birth_certificate.female" min="0" type="number"></td>
                                        <td v-if="isss"><input class="theme"  v-model="birth_certificate.sss1_male" min="0" type="number"></td>
                                        <td v-if="isss"><input class="theme"  v-model="birth_certificate.sss1_female" min="0" type="number"></td>
                                    </tr>
                                </table>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveBirthCertificates">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveBirthCertificates">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab6" v-if="school_form_type==3">
                            <p class="label">C.2 New entrants in JSS 1</p>
                            <div v-if="!isjs">
                                 <h2>Not a Jss, move to the next section</h2>
                            </div>
                            <div v-if="isjs">
                                <form action="" class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray"></td>
                                            <td colspan="2" class="effectGray label-effect"><label>New Entrants in JSS 1</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray text label-effect"><label>Student Age</label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                        </tr>
                                        <tr v-for="(age,index) in fetch_age">
                                            <td class="text label-effect"><label for="">@{{age.value}}</label></td>
                                            <td><input class="theme" v-model="age.male" name="" v-on:change="entrantmale_total" min="0" type="number"></td>
                                            <td><input class="theme" v-model="age.female" name="" v-on:change="entrantfemale_total" min="0" type="number"></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">Total</label></td>
                                            <td><input v-bind:value="entrantmaletotal" disabled type="number" min="0" name="" id="" placeholder="0"></td>
                                            <td><input v-bind:value="entrantfemaletotal" disabled type="number" min="0" name="" id="" placeholder="0"></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveEntrants">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveEntrants">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab7" v-if="school_form_type==3">
                            <p class="label">C.3 Junior Secondary Enrolment for the Current Academic Year by age</p>
                            <div v-if="!isjs">
                                 <h2>Not a Jss, move to the next section</h2>
                            </div>
                            <div v-if="isjs">
                                <form action="" class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray"></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 3</label></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">No. of stream</label></td>
                                            <td colspan="2"><input v-model="jss1_stream" type="number" min="0" class="theme" name="" id=""></td>
                                            <td colspan="2"><input v-model="jss2_stream" type="number" min="0" class="theme" name="" id=""></td>
                                            <td colspan="2"><input v-model="jss3_stream" type="number" min="0" class="theme" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">No of streams with Multigrade teaching</label></td>
                                            <td colspan="2"><input v-model="jss1_stream_with_multigrade" class="theme" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input v-model="jss2_stream_with_multigrade" class="theme" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input v-model="jss3_stream_with_multigrade" class="theme" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>Age</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                        </tr>
                                        <tr v-for="(age,index) in fetch_year_by_age">
                                            <td class="text label-effect"><label>@{{age.value}}</label></td>
                                            <td><input v-model="age.jss1_male" class="theme" v-on:change="jss1entrantmale_total" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="age.jss1_female" class="theme" v-on:change="jss1entrantfemale_total" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="age.jss2_male" class="theme" v-on:change="jss2entrantmale_total" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="age.jss2_female" class="theme" v-on:change="jss2entrantfemale_total" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="age.jss3_male" class="theme" v-on:change="jss3entrantmale_total" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="age.jss3_female" class="theme" v-on:change="jss3entrantfemale_total" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>Total</label></td>
                                            <td><input v-model="jss1maletotal" disabled="disabled" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="jss1femaletotal" disabled="disabled" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="jss2maletotal" disabled="disabled" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="jss2femaletotal" disabled="disabled" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="jss3maletotal" disabled="disabled" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="jss3femaletotal" disabled="disabled" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>Repeaters</label></td>
                                            <td><input v-model="jss1_repeaters_male" class="theme" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="jss1_repeaters_female" class="theme" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="jss2_repeaters_male" class="theme" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="jss2_repeaters_female" class="theme" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="jss3_repeaters_male" class="theme" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="jss3_repeaters_female" class="theme" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>No. Completed JSS 3 for previous year</label></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input type="number" min="0" name="" class="theme" v-model="prevyear_jss3_male" id=""></td>
                                            <td><input type="number" min="0" name="" class="theme" v-model="prevyear_jss3_female" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveEnrollmentByAge">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveEnrollmentByAge">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab8" v-if="school_form_type==3">
                            <p class="label">C.4 New entrants in SSS 1</p>
                            <div v-if="!isss">
                                 <h2>Not a Sss, move to the next section</h2>
                            </div>
                            <div v-if="isss">
                                <form action="" class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray"></td>
                                            <td colspan="2" class="effectGray label-effect"><label>New Entrants in SSS 1</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray text label-effect"><label>Student Age</label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                        </tr>
                                        <tr v-for="(age,index) in fetch_ss_age">
                                            <td class="text label-effect"><label for="">@{{age.value}}</label></td>
                                            <td><input v-model="age.male" class="theme" v-on:change="ss1_entrantmale_total" name="" type="number" min="0"></td>
                                            <td><input v-model="age.female" class="theme" v-on:change="ss1_entrantfemale_total" name="" type="number" min="0"></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">Total</label></td>
                                            <td><input disabled v-bind:value="ss1_entrantmaletotal" type="number" min="0" name="" id="" placeholder="0"></td>
                                            <td><input disabled v-bind:value="ss1_entrantfemaletotal" type="number" min="0" name="" id="" placeholder="0"></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveSssEntrants">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveSssEntrants">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab9" v-if="school_form_type==3">
                            <p class="label">C.5 Senior Secondary Enrolment for the Current Academic Year by age</p>
                            <div v-if="!isss">
                                 <h2>Not a Sss, move to the next section</h2>
                            </div>
                            <div v-if="isss">
                                <form action="" class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray"></td>
                                            <td class="effectGray label-effect" colspan="2"><label>SSS 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>SSS 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>SSS 3</label></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">No. of stream</label></td>
                                            <td colspan="2"><input v-model="ss1_stream" class="theme" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input v-model="ss2_stream" class="theme" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input v-model="ss3_stream" class="theme" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">No of streams with Multigrade teaching</label></td>
                                            <td colspan="2"><input v-model="ss1_stream_with_multigrade" class="theme" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input v-model="ss2_stream_with_multigrade" class="theme" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input v-model="ss3_stream_with_multigrade" class="theme" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>Age</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                        </tr>
                                        <tr v-for="(age,index) in fetch_ss_year_by_age">
                                            <td class="text label-effect"><label>@{{age.value}}</label></td>
                                            <td><input v-model="age.sss1_male" class="theme" v-on:change="ss1entrantmale_total" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="age.sss1_female" class="theme" v-on:change="ss1entrantfemale_total" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="age.sss2_male" class="theme" v-on:change="ss2entrantmale_total" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="age.sss2_female" class="theme" v-on:change="ss2entrantfemale_total" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="age.sss3_male" class="theme" v-on:change="ss3entrantmale_total" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="age.sss3_female" class="theme" v-on:change="ss3entrantfemale_total" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>Total</label></td>
                                            <td><input v-model="ss1maletotal" disabled="disabled" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="ss1femaletotal" disabled="disabled" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="ss2maletotal" disabled="disabled" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="ss2femaletotal" disabled="disabled" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="ss3maletotal" disabled="disabled" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="ss3femaletotal" disabled="disabled" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>Repeaters</label></td>
                                            <td><input v-model="ss1_repeaters_male" class="theme" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="ss1_repeaters_female" class="theme" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="ss2_repeaters_male" class="theme" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="ss2_repeaters_female" class="theme" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="ss3_repeaters_male" class="theme" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="ss3_repeaters_female" class="theme" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>No. Completed SSS 3 for previous year</label></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input v-model="prevyear_sss3_male" class="theme" type="number" min="0" name="" id=""></td>
                                            <td><input v-model="prevyear_sss3_female" class="theme" type="number" min="0" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveSssEnrollmentByAge">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveSssEnrollmentByAge">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab10" v-if="school_form_type==3">
                            <p class="label">C.6 Students Flow for the Current Academic Year Junior Secondary School</p>
                            <form action="" class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td class="effectGray text label-effect" rowspan="2"><label>Student Flow</label></td>
                                        <td class="effectGray label-effect" v-if="isjs" colspan="2"><label>JSS 1</label></td>
                                        <td class="effectGray label-effect" v-if="isjs" colspan="2"><label>JSS 2</label></td>
                                        <td class="effectGray label-effect" v-if="isjs" colspan="2"><label>JSS 3</label></td>
                                        <td class="effectGray label-effect" v-if="isss" colspan="2"><label>SSS 1</label></td>
                                        <td class="effectGray label-effect" v-if="isss" colspan="2"><label>SSS 2</label></td>
                                        <td class="effectGray label-effect" v-if="isss" colspan="2"><label>SSS 3</label></td>
                                    </tr>
                                    <tr>
                                        <td class="effectGray label-effect" v-if="isjs"><label>Male</label></td>
                                        <td class="effectGray label-effect" v-if="isjs"><label>Female</label></td>
                                        <td class="effectGray label-effect" v-if="isjs"><label>Male</label></td>
                                        <td class="effectGray label-effect" v-if="isjs"><label>Female</label></td>
                                        <td class="effectGray label-effect" v-if="isjs"><label>Male</label></td>
                                        <td class="effectGray label-effect" v-if="isjs"><label>Female</label></td>
                                        <td class="effectGray label-effect" v-if="isss"><label>Male</label></td>
                                        <td class="effectGray label-effect" v-if="isss"><label>Female</label></td>
                                        <td class="effectGray label-effect" v-if="isss"><label>Male</label></td>
                                        <td class="effectGray label-effect" v-if="isss"><label>Female</label></td>
                                        <td class="effectGray label-effect" v-if="isss"><label>Male</label></td>
                                        <td class="effectGray label-effect" v-if="isss"><label>Female</label></td>
                                    </tr>
                                    <tr v-for="(pupil,index) in fetch_pupilflow">
                                        <td class="text label-effect"><label>@{{pupil.value}}</label></td>
                                        <td v-if="isjs"><input class="theme" v-model="pupil.jss1_male" type="number" min="0" name="a" id=""></td>
                                        <td v-if="isjs"><input class="theme" v-model="pupil.jss1_female" type="number" min="0" name="a" id=""></td>
                                        <td v-if="isjs"><input class="theme" v-model="pupil.jss2_male" type="number" min="0" name="a" id=""></td>
                                        <td v-if="isjs"><input class="theme" v-model="pupil.jss2_female" type="number" min="0" name="a" id=""></td>
                                        <td v-if="isjs"><input class="theme" v-model="pupil.jss3_male" type="number" min="0" name="a" id=""></td>
                                        <td v-if="isjs"><input class="theme" v-model="pupil.jss3_female" type="number" min="0" name="a" id=""></td>
                                        <td v-if="isss"><input class="theme" v-model="pupil.sss1_male" type="number" min="0" name="a" id=""></td>
                                        <td v-if="isss"><input class="theme" v-model="pupil.sss1_female" type="number" min="0" name="a" id=""></td>
                                        <td v-if="isss"><input class="theme" v-model="pupil.sss2_male" type="number" min="0" name="a" id=""></td>
                                        <td v-if="isss"><input class="theme" v-model="pupil.sss2_female" type="number" min="0" name="a" id=""></td>
                                        <td v-if="isss"><input class="theme" v-model="pupil.sss3_male" type="number" min="0" name="a" id=""></td>
                                        <td v-if="isss"><input class="theme" v-model="pupil.sss3_female" type="number" min="0" name="a" id=""></td>
                                    </tr>
                                </table>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveStudentsFlow">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveStudentsFlow">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab11" v-if="school_form_type==3">
                            <p class="label">C.7 Students with Special Needs for the Current Academic Year</p>
                            <form action="" class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td class="effectGray text label-effect" rowspan="2"><label>Challenge that impacts the ability to learn</label></td>
                                        <td v-if="isjs" class="effectGray label-effect" colspan="2"><label>JSS 1</label></td>
                                        <td v-if="isjs" class="effectGray label-effect" colspan="2"><label>JSS 2</label></td>
                                        <td v-if="isjs" class="effectGray label-effect" colspan="2"><label>JSS 3</label></td>
                                        <td v-if="isss" class="effectGray label-effect" colspan="2"><label>SSS 1</label></td>
                                        <td v-if="isss" class="effectGray label-effect" colspan="2"><label>SSS 2</label></td>
                                        <td v-if="isss" class="effectGray label-effect" colspan="2"><label>SSS 3</label></td>
                                    </tr>
                                    <tr>
                                        <td v-if="isjs" class="effectGray label-effect"><label>Male</label></td>
                                        <td v-if="isjs" class="effectGray label-effect"><label>Female</label></td>
                                        <td v-if="isjs" class="effectGray label-effect"><label>Male</label></td>
                                        <td v-if="isjs" class="effectGray label-effect"><label>Female</label></td>
                                        <td v-if="isjs" class="effectGray label-effect"><label>Male</label></td>
                                        <td v-if="isjs" class="effectGray label-effect"><label>Female</label></td>
                                        <td v-if="isss" class="effectGray label-effect"><label>Male</label></td>
                                        <td v-if="isss" class="effectGray label-effect"><label>Female</label></td>
                                        <td v-if="isss" class="effectGray label-effect"><label>Male</label></td>
                                        <td v-if="isss" class="effectGray label-effect"><label>Female</label></td>
                                        <td v-if="isss" class="effectGray label-effect"><label>Male</label></td>
                                        <td v-if="isss" class="effectGray label-effect"><label>Female</label></td>
                                    </tr>
                                    <tr v-for="(need,index) in fetch_special_needs">
                                        <td class="text label-effect"><label>@{{need.value}}</label></td>
                                        <td v-if="isjs"><input class="theme" type="number" min="0" v-model="need.jss1_male" name="" id=""></td>
                                        <td v-if="isjs"><input class="theme" type="number" min="0" v-model="need.jss1_female" name="" id=""></td>
                                        <td v-if="isjs"><input class="theme" type="number" min="0" v-model="need.jss2_male" name="" id=""></td>
                                        <td v-if="isjs"><input class="theme" type="number" min="0" v-model="need.jss2_female" name="" id=""></td>
                                        <td v-if="isjs"><input class="theme" type="number" min="0" v-model="need.jss3_male" name="" id=""></td>
                                        <td v-if="isjs"><input class="theme" type="number" min="0" v-model="need.jss3_female" name="" id=""></td>
                                        <td v-if="isss"><input class="theme" type="number" min="0" v-model="need.sss1_male" name="" id=""></td>
                                        <td v-if="isss"><input class="theme" type="number" min="0" v-model="need.sss1_female" name="" id=""></td>
                                        <td v-if="isss"><input class="theme" type="number" min="0" v-model="need.sss2_male" name="" id=""></td>
                                        <td v-if="isss"><input class="theme" type="number" min="0" v-model="need.sss2_female" name="" id=""></td>
                                        <td v-if="isss"><input class="theme" type="number" min="0" v-model="need.sss3_male" name="" id=""></td>
                                        <td v-if="isss"><input class="theme" type="number" min="0" v-model="need.sss3_female" name="" id=""></td>
                                    </tr>
                                </table>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveSpecialNeed">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveSpecialNeed">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab12" v-if="school_form_type==3">
                            <p class="label">C.8 JSCE examination for the previous Academic Year {Show previous year of current year}</p>
                            <div v-if="!isjs">
                                 <h2>Not a Jss, move to the next section</h2>
                            </div>
                            <div v-if="isjs">
                                <form action="" class="table-responsive">
                                    <p v-if="jsce_errors.length" style="color:red">
                                        <b>Please correct the following error(s):</b>
                                        <ul style="list-style-type: circle !important">
                                            <li style="color:red" v-for="error in jsce_errors">-> @{{ error }}</li>
                                        </ul>
                                    </p>
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect"><label for=""></label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                            <td class="effectGray label-effect"><label for="">Total</label></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">How many students were registered for JSCE?</label></td>
                                            <td><input class="theme" type="number" min="0" v-model="registered_male" v-on:change="reg_jsce" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="registered_female" v-on:change="reg_jsce" name="" id=""></td>
                                            <td><input type="number" min="0" disabled v-model="registered_total" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">How many students took part in the JSCE?</label></td>
                                            <td><input class="theme" type="number" min="0" v-model="took_part_male" v-on:change="took_jsce" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="took_part_female" v-on:change="took_jsce" name="" id=""></td>
                                            <td><input type="number" min="0" disabled v-model="took_part_total" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">How many students passed JSCE?</label></td>
                                            <td><input class="theme" type="number" min="0" v-model="passed_male" v-on:change="passed_jsce" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="passed_female" v-on:change="passed_jsce" name="" id=""></td>
                                            <td><input type="number" min="0" disabled v-model="passed_total" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveExamination">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveExamination">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab13" v-if="school_form_type==3">
                            <p class="label">C.9 SSCE examination for the previous Academic Year {Show previous year of current year}</p>
                            <div v-if="!isss">
                                 <h2>Not a Sss, move to the next section</h2>
                            </div>
                            <div v-if="isss">
                                <form action="" class="table-responsive">
                                    <p v-if="ssce_errors.length" style="color:red">
                                        <b>Please correct the following error(s):</b>
                                        <ul style="list-style-type: circle !important">
                                            <li style="color:red" v-for="error in ssce_errors">-> @{{ error }}</li>
                                        </ul>
                                    </p>
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect"><label for=""></label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                            <td class="effectGray label-effect"><label for="">Total</label></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">How many students were registered for SSCE?</label></td>
                                            <td><input class="theme" type="number" min="0" v-model="registered_ss_male" v-on:change="reg_ssce" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="registered_ss_female" v-on:change="reg_ssce" name="" id=""></td>
                                            <td><input type="number" min="0" disabled v-model="registered_ss_total" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">How many students took part in the SSCE?</label></td>
                                            <td><input class="theme" type="number" min="0" v-model="took_part_ss_male" v-on:change="took_ssce" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="took_part_ss_female" v-on:change="took_ssce" name="" id=""></td>
                                            <td><input type="number" min="0" disabled v-model="took_part_ss_total" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">How many students passed SSCE?</label></td>
                                            <td><input class="theme" type="number" min="0" v-model="passed_ss_male" v-on:change="passed_ssce" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="passed_ss_female" v-on:change="passed_ssce" name="" id=""></td>
                                            <td><input type="number" min="0" disabled v-model="passed_ss_total" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveExaminationss">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveExaminationss">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab14" v-if="school_form_type==3">
                            <p class="label">D. Staff</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>D.1 How many non-teaching staff are working at the school?</p>
                                    </div>
                                    <div class="answer helper">
                                        <input class="theme" type="number" min="0" name="" v-model="non_teaching_staff_male" v-on:change="non_teaching_staff_total_change" id=""><label for="">male</label>
                                        <input class="theme" type="number" min="0" name="" v-model="non_teaching_staff_female" v-on:change="non_teaching_staff_total_change" id=""><label for="">female</label>
                                        <input type="number" min="0" disabled name="" v-model="non_teaching_staff_total" id=""><label for="">Total</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>D.2 How many teachers are working at the school regardless of whether they are currently present or on course or absent</p>
                                    </div>
                                    <div class="answer helper">
                                        <input class="theme" type="number" min="0" name="" v-model="teaching_staff_male" v-on:change="teaching_staff_total_change" id=""><label for="">male</label>
                                        <input class="theme" type="number" min="0" name="" v-model="teaching_staff_female" v-on:change="teaching_staff_total_change" id=""><label for="">female</label>
                                        <input type="number" min="0" name="" disabled v-model="teaching_staff_total" id=""><label for="">Total</label>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveNoOfStaff_func">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveNoOfStaff_func">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab15" v-if="school_form_type==3">
                            <p class="label">D.3 Information on all staff during the school year</p>
                            <div class="row">
                                <form action="" class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <th>No.</th>
                                            <th>Staff File No</th>
                                            <th>Name of staff</th>
                                            <th>Gender</th>
                                            <th>Type of staff</th>
                                            <th>Source of salary</th>
                                            <th>Year of Birth</th>
                                            <th>Year of first appointment</th>
                                            <th>Year of present appointment</th>
                                            <th>Year of posting to the school</th>
                                            <th>Grade level / Step</th>
                                            <th>Present</th>
                                            <th>Academic Qualification</th>
                                            <th>Teaching Qualification</th>
                                            <th>Area of specialization</th>
                                            <th>Main subject taught</th>
                                            <th>Teaching type</th>
                                            <th>Tick box if teacher also teaches senior secondary classes in this school</th>
                                            <th>Tick box if teacher attended training workshop / seminar in last 12 months</th>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(source,index) in fetch_staffs">
                                                <td>@{{index+1}}</td>
                                                <td>@{{source.staff_file_no}}</td>
                                                <td>@{{source.staff_name}}</td>
                                                <td>@{{source.staff_gender}}</td>
                                                <td>@{{source.staff_type}}</td>
                                                <td>@{{source.salary_source}}</td>
                                                <td>@{{source.staff_yob}}</td>
                                                <td>@{{source.staff_yfa}}</td>
                                                <td>@{{source.staff_ypa}}</td>
                                                <td>@{{source.staff_yps}}</td>
                                                <td>@{{source.staff_level}}</td>
                                                <td>@{{source.present}}</td>
                                                <td>@{{source.academic_qualification}}</td>
                                                <td>@{{source.teaching_qualification}}</td>
                                                <td>@{{source.area_specialisation}}</td>
                                                <td>@{{source.subject_taught}}</td>
                                                <td>@{{source.teaching_type}}</td>
                                                <td><input type="checkbox" name="" id=""></td>
                                                <td><input type="checkbox" v-model="attended_training" value="1" name="" id=""></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                            <div class="addBtn">
                                <ul>
                                    <li><button class="btn-success" id="addBtn"></button></li>
                                </ul>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="staffInformation">save</button></li>
                                    <li><button id="next" v-on:click="staffInformation" class="theme">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab16" v-if="school_form_type==3">
                            <p class="label">E. Classrooms</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>E.1 How many classrooms are there in the school?</p>
                                    </div>
                                    <div class="answer helper">
                                        <input class="theme" type="number" min="0" v-model="no_of_classrooms" name="" id=""><label for="">Number</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>E.2 Are any classes held outside (because classrooms are unusable or insufficient)?</p>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" v-model="classes_held_outside" name="sufficient" value="1" id="sufficient_yes"><label for="sufficient_yes">Yes</label></li>
                                            <li><input type="radio" v-model="classes_held_outside" name="sufficient" value="0" id="sufficient_no"><label for="sufficient_no">No</label></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button v-on:click="saveNoOfClassrooms" id="save">save</button></li>
                                    <li><button v-on:click="saveNoOfClassrooms" id="next" class="theme">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <!-- E3 -->
                        <div id="tab17" v-if="school_form_type==3">
                            <p class="label">E.3 Information on all classrooms</p>
                            <div class="row ">
                                <form action="" class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <th>No.</th>
                                            <th></th>
                                            <th>Year of construction</th>
                                            <th>Present condition</th>
                                            <th>Length in metres</th>
                                            <th>Width in metres</th>
                                            <th>Floor material</th>
                                            <th>Walls material</th>
                                            <th>Roof material</th>
                                            <th>Seating</th>
                                            <th>Good blackboard</th>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(source,index) in fetch_classrooms">
                                                <td>@{{index+1}}</td>
                                                <td>
                                                    <p v-on:click="removeClassroom(source.id,index)" id="more"></p>
                                                </td>
                                                <td>
                                                    <select v-model="source.year_of_construction" class="theme" name="" id="">
                                                        <option value="" disabled>Choose Year</option>
                                                        <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select v-model="source.present_condition" class="theme" name="source-of-salary" id="type-of-staff">
                                                        <option v-for="(source,index) in fetch_presentcondition" v-bind:value="source.value"> @{{source.display}}</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="number" min="0" v-model="source.length_in_meters" class="theme" name="" id="">
                                                </td>
                                                <td><input type="number" min="0" v-model="source.width_in_meters" class="theme" name="" id=""></td>
                                                <td>
                                                    <select v-model="source.floor_material" class="theme" name="source-of-salary" id="type-of-staff">
                                                        <option v-for="(source,index) in fetch_floormaterial" v-bind:value="source.value"> @{{source.display}}</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select v-model="source.wall_material" class="theme" name="source-of-salary" id="type-of-staff">
                                                        <option v-for="(source,index) in fetch_wallmaterial" v-bind:value="source.value"> @{{source.display}}</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select v-model="source.roof_material" class="theme" name="source-of-salary" id="type-of-staff">
                                                        <option v-for="(source,index) in fetch_roofmaterial" v-bind:value="source.value"> @{{source.display}}</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select v-model="source.seating" class="theme" name="" required="required" id="">
                                                        <option value="" disabled></option>
                                                        <option value="1">1-Yes</option>
                                                        <option value="0">2-No</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select v-model="source.good_blackboard" class="theme" name="" required="required" id="">
                                                        <option value="" disabled></option>
                                                        <option value="1">1-Yes</option>
                                                        <option value="0">2-No</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                            <div class="addBtn">
                                <ul>
                                    <li><button class="btn-success" id="addClassBtn"></button></li>
                                </ul>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="submitClassrooms">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="submitClassrooms">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab18" v-if="school_form_type==3">
                            <p class="label">E.4 Number of rooms other than classrooms are there in the school by type of room</p>
                            <form action="">
                                <div class="row" v-for="(source,index) in fetch_otherRooms">
                                    <div class="question">
                                        <p>@{{index+1}}. @{{source.value}}</p>
                                    </div>
                                    <div class="answer helper">
                                        <input type="number" min="0" name="" class="theme" v-model="source.no_of_rooms" id=""><label for="">Number</label>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveNoOfOtherrooms">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveNoOfOtherrooms">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab19" v-if="school_form_type==3">
                            <p class="label">F.1 Sources of drinking water</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>Pick source of drinking water</p>
                                    </div>
                                    <div class="answer">
                                        <ul id="list-tile">
                                            <li v-for="(source,index) in fetch_drinkingwater_source"><input type="checkbox" v-on:change="changedwsource(source.value,$event)" v-bind:value="source.value" v-model="sources_of_drinking_water" v-bind:id="source.value" name="Pipeborne"><label for="pbw">@{{source.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveDWSources">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveDWSources">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab20" v-if="school_form_type==3">
                            <p class="label">F.2 Facilities available</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>How many useable facilities does the school have?</p>
                                        <em>If the facilities are not available, write zero</em>
                                    </div>
                                    <div class="answer">
                                        <div class="infoD">
                                            <label for="">Useable</label>
                                            <label for="">Not Useable</label>
                                        </div>
                                        <div class="list-style" v-for="(source,index) in fetch_facilities">
                                            <span>
                                                <label v-bind:for="source.value">@{{source.value}}</label>
                                                <input type="number" min="0" class="theme" v-model="source.useable" name="use-toilet" v-bind:id="source.value">
                                                <input type="number" min="0" class="theme" v-model="source.notuseable" name="not-use-toilet" v-bind:id="source.value">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveFacilitiesAvailable">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveFacilitiesAvailable">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab21" v-if="school_form_type==3">
                            <p class="label">F.3 Shared Facilities</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>Shared Facilities</p>
                                        <em>If your school share facilities specify the facilities shared by separate school/levels by ticking the appropriate box</em>
                                    </div>
                                    <div class="answer">
                                        <ul id="list-tile">
                                            <li v-for="(source,index) in fetch_facilities"><input type="checkbox" v-bind:value="source.value" v-model="shared_facilities" v-on:change="changesharedfacilities(source.value,$event)" name="toilets" v-bind:id="source.value"><label v-bind:for="source.value">@{{source.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveSharedFacilities">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveSharedFacilities">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab22" v-if="school_form_type==3">
                            <p class="label">F.4 Toilet type</p>
                            <form action="" class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td class="effectGray label-effect" rowspan="2"><label></label></td>
                                        <td class="effectGray label-effect" colspan="3"><label>Used only by students</label></td>
                                        <td class="effectGray label-effect" colspan="3"><label>Used only by teachers</label></td>
                                        <td class="effectGray label-effect" colspan="3"><label>Used by students and teachers</label></td>
                                        <td class="effectGray label-effect" rowspan="2"><label>Total</label></td>
                                    </tr>
                                    <tr>
                                        <td class="effectGray label-effect"><label>Male only</label></td>
                                        <td class="effectGray label-effect"><label>Female only</label></td>
                                        <td class="effectGray label-effect"><label>Mixed</label></td>
                                        <td class="effectGray label-effect"><label>Male only</label></td>
                                        <td class="effectGray label-effect"><label>Female only</label></td>
                                        <td class="effectGray label-effect"><label>Mixed</label></td>
                                        <td class="effectGray label-effect"><label>Male only</label></td>
                                        <td class="effectGray label-effect"><label>Female only</label></td>
                                        <td class="effectGray label-effect"><label>Mixed</label></td>
                                    </tr>
                                    <tr v-for="(source,index) in fetch_toilet_type">
                                        <td class="text label-effect"><label>@{{source.value}}</label></td>
                                        <td><input class="theme" v-model.number="source.students_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.students_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.students_mixed" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.teachers_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.teachers_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.teachers_mixed" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.both_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.both_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.both_mixed" type="number" min="0" name="" id=""></td>
                                        <td><input v-model="(source.students_male*1)+(source.students_female*1)+(source.students_mixed*1)+(source.teachers_male*1)+(source.teachers_female*1)+(source.teachers_mixed*1)+(source.both_male*1)+(source.both_female*1)+source.both_mixed*1" type="number" min="0" disabled name="" id=""></td>
                                    </tr>
                                </table>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveToilet">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveToilet">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab23" v-if="school_form_type==3">
                            <p class="label">F.5 Source(s) of power</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>Source(s) of power</p>
                                        <em>Tick available source(s) of Power in the School</em>
                                    </div>
                                    <div class="answer">
                                        <ul id="list-tile">
                                            <li v-for="(source,index) in fetch_power_source"><input type="checkbox" v-bind:value="source.value" v-on:change="changesourceofpower(source.value,$event)" v-model="sources_of_power" name="" id="phcn`"><label for="phcn">@{{source.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="savePowerSource">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="savePowerSource">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab24" v-if="school_form_type==3">
                            <p class="label">F.6 Health facility & Fence/Wall</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>Health facility </p>
                                        <em>Does the school have a health facility?</em>
                                    </div>
                                    <div class="answer">
                                        <ul id="list-tile">
                                            <li v-for="(source,index) in fetch_healthfacility"><input type="radio" v-bind:value="source.value" v-model="health_facility" name="" id="hc"><label for="hc">@{{source.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Fence/Wall</p>
                                        <em>Does the school have a fence or wall around it?</em>
                                    </div>
                                    <div class="answer">
                                        <ul id="list-tile">
                                            <li v-for="(source,index) in fetch_fence"><input type="radio" v-bind:value="source.value" v-model="fence_facility" name="" id="hc"><label for="hc">@{{source.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveHealthFence">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveHealthFence">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab25" v-if="school_form_type==3">
                            <p class="label">F.7 Additional Class Information</p>
                            <form action="" class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td class="effectGray text label-effect" rowspan="2"><label>Class</label></td>
                                        <td class="effectGray label-effect" colspan="3"><label>Total Seating available</label></td>
                                    </tr>
                                    <tr>
                                        <td class="effectGray label-effect"><label>1 Seater</label></td>
                                        <td class="effectGray label-effect"><label>2 Seater</label></td>
                                        <td class="effectGray label-effect"><label>3 Seater</label></td>
                                    </tr>
                                    <tr v-for="(source,index) in fetch_seaters">
                                        <td class="text label-effect"><label>@{{source.value}}</label></td>
                                        <td><input class="theme" type="number" min="0" v-model="source.seater1" name="" id=""></td>
                                        <td><input class="theme" type="number" min="0" v-model="source.seater2" name="" id=""></td>
                                        <td><input class="theme" type="number" min="0" v-model="source.seater3" name="" id=""></td>
                                    </tr>
                                </table>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveSeaters">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveSeaters">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab26" v-if="school_form_type==3">
                            <p class="label">G.1 Number of Junior Students’ by Subject in the current Academic Year</p>
                            <div v-if="!isjs">
                                 <h2>Not a Jss, move to the next section</h2>
                            </div>
                            <div v-if="isjs">
                                <form action="" class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect" rowspan="3"><label for="">Class/Subject</label></td>
                                            <td class="effectGray label-effect" colspan="6"><label for="">Number of pupils by Subject</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect" colspan="2"><label for="">JSS1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label for="">JSS2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label for="">JSS3</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                        </tr>
                                        <tr v-for="(source,index) in fetch_subjects">
                                            <td class="text label-effect"><label>@{{source.value}}</label></td>
                                            <td><input class="theme" v-model="source.jss1_pupils_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.jss1_pupils_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.jss2_pupils_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.jss2_pupils_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.jss3_pupils_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.jss3_pupils_female" type="number" min="0" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveStudentbySubject">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveStudentbySubject">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab27" v-if="school_form_type==3">
                            <p class="label">G.2 Number of Senior Students’ by Subject in the current Academic Year</p>
                            <div v-if="!isss">
                                 <h2>Not a Sss, move to the next section</h2>
                            </div>
                            <div v-if="isss">
                                <form action="" class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect" rowspan="3"><label for="">Class/Subject</label></td>
                                            <td class="effectGray label-effect" colspan="6"><label for="">Number of pupils by Subject</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect" colspan="2"><label for="">SSS1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label for="">SSS2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label for="">SSS3</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                        </tr>
                                        <tr v-for="(source,index) in fetch_subjectsSS">
                                            <td class="text label-effect"><label>@{{source.value}}</label></td>
                                            <td><input class="theme" v-model="source.sss1_pupils_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.sss1_pupils_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.sss2_pupils_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.sss2_pupils_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.sss3_pupils_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.sss3_pupils_female" type="number" min="0" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveStudentbySubjectSS">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveStudentbySubjectSS">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab28" v-if="school_form_type==3">
                            <p class="label">H.1 Number of core subject textbooks available to Junior students in the current Academic Year.</p>
                            <div v-if="!isjs">
                                 <h2>Not a Jss, move to the next section</h2>
                            </div>
                            <div v-if="isjs">
                                <form action="" class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect" rowspan="2"><label for="">Subject Area</label></td>
                                            <td class="effectGray label-effect" colspan="3"><label for="">Number of Students Book Made Available for each Subject</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label for="">JSS1</label></td>
                                            <td class="effectGray label-effect"><label for="">JSS2</label></td>
                                            <td class="effectGray label-effect"><label for="">JSS3</label></td>
                                        </tr>
                                        <tr v-for="(source,index) in fetch_mainsubjects">
                                            <td class="text label-effect"><label>@{{source.value}}</label></td>
                                            <td><input class="theme" v-model="source.jss1_pupils_books" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.jss2_pupils_books" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.jss3_pupils_books" type="number" min="0" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveStudents">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveStudents">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab29" v-if="school_form_type==3">
                            <p class="label">H.2 Number of core subject textbooks available to Senior students in the current Academic Year.</p>
                            <div v-if="!isss">
                                 <h2>Not a Sss, move to the next section</h2>
                            </div>
                            <div v-if="isss">
                                <form action="" class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect" rowspan="2"><label for="">Subject Area</label></td>
                                            <td class="effectGray label-effect" colspan="3"><label for="">Number of Students Book Made Available for each Subject</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label for="">SSS1</label></td>
                                            <td class="effectGray label-effect"><label for="">SSS2</label></td>
                                            <td class="effectGray label-effect"><label for="">SSS3</label></td>
                                        </tr>
                                        <tr v-for="(source,index) in fetch_mainsubjects">
                                            <td class="text label-effect"><label>@{{source.value}}</label></td>
                                            <td><input class="theme" v-model="source.sss1_pupils_books" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.sss2_pupils_books" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.sss3_pupils_books" type="number" min="0" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveStudentsSS">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveStudentsSS">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab30" v-if="school_form_type==3">
                            <p class="label">H.3 Number of core subject Teachers’ Textbooks available in the School in the current Academic Year</p>
                            <div v-if="!isjs">
                                 <h2>Not a Jss, move to the next section</h2>
                            </div>
                            <div v-if="isjs">
                                <form action="" class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect" rowspan="2"><label for="">Subject Area</label></td>
                                            <td class="effectGray label-effect" colspan="5"><label for="">Number of Teachers Book Made Available for each Subject</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label for="">JSS1</label></td>
                                            <td class="effectGray label-effect"><label for="">JSS2</label></td>
                                            <td class="effectGray label-effect"><label for="">JSS3</label></td>
                                        </tr>
                                        <tr v-for="(source,index) in fetch_mainsubjects">
                                            <td class="text label-effect"><label>@{{source.value}}</label></td>
                                            <td><input class="theme" v-model="source.jss1_teachers_books" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.jss2_teachers_books" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.jss3_teachers_books" type="number" min="0" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveTeachers">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveTeachers">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab31" v-if="school_form_type==3">
                            <p class="label">H.4 Number of core subject Teachers’ Textbooks available in the School in the current Academic Year</p>
                            <div v-if="!isss">
                                 <h2>Not a Sss, move to the next section</h2>
                            </div>
                            <div v-if="isss">
                                <form action="" class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect" rowspan="2"><label for="">Subject Area</label></td>
                                            <td class="effectGray label-effect" colspan="5"><label for="">Number of Teachers Book Made Available for each Subject</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label for="">SSS1</label></td>
                                            <td class="effectGray label-effect"><label for="">SSS2</label></td>
                                            <td class="effectGray label-effect"><label for="">SSS3</label></td>
                                        </tr>
                                        <tr v-for="(source,index) in fetch_mainsubjects">
                                            <td class="text label-effect"><label>@{{source.value}}</label></td>
                                            <td><input class="theme" v-model="source.sss1_teachers_books" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.sss2_teachers_books" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="source.sss3_teachers_books" type="number" min="0" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveTeachersSS">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveTeachersSS">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab32" v-if="school_form_type==3">
                            <p class="label">I. Teachers Qualification (By Level and Class) in Current Academic Year</p>
                            <form action="" class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td class="effectGray label-effect"><label for="">Level of Teaching Input</label></td>
                                        <td v-if="isjs" colspan="2" class="effectGray label-effect"><label for="">JSS</label></td>
                                        <td v-if="isss" colspan="2" class="effectGray label-effect"><label for="">SSS</label></td>
                                        <td colspan="2" class="effectGray label-effect"><label for="">Total</label></td>
                                    </tr>
                                    <tr>
                                        <td class="effectGray label-effect"><label for="">Highest qualification</label></td>
                                        <td v-if="isjs" class="effectGray label-effect"><label for="">Male</label></td>
                                        <td v-if="isjs" class="effectGray label-effect"><label for="">Female</label></td>
                                        <td v-if="isss" class="effectGray label-effect"><label for="">Male</label></td>
                                        <td v-if="isss" class="effectGray label-effect"><label for="">Female</label></td>
                                        <td class="effectGray label-effect"><label for="">Male</label></td>
                                        <td class="effectGray label-effect"><label for="">Female</label></td>
                                    </tr>
                                    <tr v-for="(source,index) in fetch_teachingqualification">
                                        <td class="text label-effect"><label>@{{source.value}}</label></td>
                                        <td v-if="isjs"><input class="theme" type="number" v-on:change="tq_jss_male_tot" name="" v-model.number="source.jss_male" min="0" id=""></td>
                                        <td v-if="isjs"><input class="theme" type="number" v-on:change="tq_jss_female_tot" name="" v-model.number="source.jss_female" min="0" id=""></td>
                                        <td v-if="isss"><input class="theme" type="number" v-on:change="tq_sss_male_tot" name="" v-model.number="source.sss_male" min="0" id=""></td>
                                        <td v-if="isss"><input class="theme" type="number" v-on:change="tq_sss_female_tot" name="" v-model.number="source.sss_female" min="0" id=""></td>
                                        <td><input type="number" disabled="disabled" name="" v-model.number="(source.jss_male*1)+source.sss_male*1" min="0" id=""></td>
                                        <td><input type="number" disabled="disabled" name="" v-model.number="(source.jss_female*1)+source.sss_female*1" min="0" id=""></td>
                                    </tr>
                                    <tr>
                                        <td class="text label-effect"><label>Total</label></td>
                                        <td v-if="isjs"><input type="number" min="0" v-model="tq_jss_male_total" name="" id="" disabled="disabled"></td>
                                        <td v-if="isjs"><input type="number" min="0" v-model="tq_jss_female_total" name="" id="" disabled="disabled"></td>
                                        <td v-if="isss"><input type="number" min="0" v-model="tq_sss_male_total" name="" id="" disabled="disabled"></td>
                                        <td v-if="isss"><input type="number" min="0" v-model="tq_sss_female_total" name="" id="" disabled="disabled"></td>
                                        <td><input type="number" min="0" v-model="tq_male_total" name="" id="" disabled="disabled"></td>
                                        <td><input type="number" min="0" v-model="tq_female_total" name="" id="" disabled="disabled"></td>
                                    </tr>
                                </table>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveTeacherQualification">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveTeacherQualification">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab33" v-if="school_form_type==3">
                            <form>
                                <p class="label">Attestation by Head Teacher</p>
                                <div class="row">
                                    <div class="question">
                                        <p>Name</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" v-model="attestation_headteacher_name" name="" id="" required="required" placeholder="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Telephone</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" v-model="attestation_headteacher_telephone" name="" id="" required="required" placeholder="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Date</p>
                                    </div>
                                    <div class="answer">
                                        <input type="date" class="theme" v-model="attestation_headteacher_signdate" name="" id="">
                                    </div>
                                </div>

                                <p class="label">Attestation by Enumerator</p>
                                <div class="row">
                                    <div class="question">
                                        <p>Name</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" name="" v-model="attestation_enumerator_name" id="" required="required" placeholder="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Position</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" name="" v-model="attestation_enumerator_position" id="" required="required" placeholder="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Telephone</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" name="" id="" v-model="attestation_enumerator_telephone" required="required" placeholder="">
                                    </div>
                                </div>

                                <p class="label">Attestation by Supervisor</p>
                                <div class="row">
                                    <div class="question">
                                        <p>Name</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" v-model="attestation_supervisor_name" name="" id="" required="required" placeholder="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Position</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" v-model="attestation_supervisor_position" name="" id="" required="required" placeholder="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Telephone</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" name="" v-model="attestation_supervisor_telephone" id="" required="required" placeholder="">
                                    </div>
                                </div>
                            </form>

                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveUndertaking">finalize save</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab34" v-if="school_form_type==3">
                            <div class="completedIcon"></div>
                            <h3 class="inform">You have completed this form.</h3>
                            <h4 class="action">Click <a href="" id='returnTab'>here</a>  to go back</h4>
                            <br/>
                            <hr/>
                            <h3 class="action">Click <a href='' v-on:click='exportFormData' id=''>here  to EXPORT form data</a></h3>
                        </div>

                        <!--Public Primary-->




                        <!--Private-->




                        <!--Science and Vocational-->
                    </div>		
                        
                </div>
            </div>
            
            <a id="downloadAnchorElem" style="display:none"></a>
        </div>
    </div>



    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/parsley.min.js')}}"></script>
    <script src="{{asset('assets/js/functions.js')}}"></script>
    <script src="{{asset('assets/js/offline_entry.js')}}"></script>
    <script src="{{asset('assets/js/staff.js')}}"></script>
    <script src="{{asset('assets/js/users.js')}}"></script>
    <script>
        const returnTab = document.getElementById('returnTab');
        returnTab.addEventListener('click', (e) => {
            e.preventDefault();
            document.getElementById('tab32').classList.remove('show-current-tab');
            var currentLink = window.location.href;
            window.location.assign(currentLink);
        })
    </script>
</body>

</html> 