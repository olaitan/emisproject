<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Accounts - Developer</title>    
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/css/api.css">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
</head>
<body>
    <div id="app">
        <div class="left"></div>
        <div class="right">
            <div class="brand">
                <h3><a href="">NEMIS</a></h3>
            </div>
            <div class="todos">
                <ul>
                    <li><a id='log' href="#loginForm" class="active">Sign in</a></li>
                    <li><a id='up' href="#signUpForm">Sign Up</a></li>
                </ul>
            </div>
            <div class="forms">
                <div id="loginForm" class="active">
                    <p class="error">Incorrect email/password</p>
                    <form action="">
                        <div>
                            <label for="emaila">E-mail</label>
                            <input type="email" v-model="login_email" name="" id="emaila" placeholder="someone@example.com">
                        </div>                    
                        <div>
                            <label for="pwd">Password</label>
                            <input type="password" v-model="login_password" name="" id="pwd" placeholder="*****************">
                        </div>
                        <div class="check">
                            <input type="checkbox" name="" id="remember"><label for="remember">Keep me signed in</label>
                        </div>
                    </form>
                    <div>
                        <button id="login" v-on:click="login" type="submit"><p id='text'>SIGN IN</p>
                        <span id="spinner"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></span></button>
                    </div>
                </div>
                <div id="signUpForm">
                    <form action="">
                        <div>
                            <label for="appname">Business / App Name</label>
                            <input v-model="name" type="text" name="" id="appname" placeholder="">
                        </div>                    
                        <div>
                            <label for="email">Your Work Email Address</label>
                            <input v-model="email" type="email" name="" id="email" placeholder="">
                        </div>                                       
                        <div>
                            <label for="pwd">Your Password</label>
                            <input v-model="password" type="password" name="" id="key" placeholder="">
                        </div>                   
                        <div>
                            <label for="cpwd"> Confirm Password</label>
                            <input v-model="c_password" type="password" name="" id="ckey" placeholder="">
                        </div>
                        <div>
                        <label for="state"> State </label>
                        <select name="roles" v-model="user_state" id="userRole">
                            <option v-for="(source,index) in fetch_states" v-bind:value="source.data.id">@{{source.data.name}}</option>
                        </select>
                        </div>
                    </form>
                    <p class='agree'>By clicking the button below, you agree to <a href="">NEMIS's</a> terms of acceptable use</p>
                    <div class="signUpBtn">
                        <button id="signup" v-on:click="signup" type="submit"><p id='stext'>SIGN UP</p>
                        <span id="spinn"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('assets/js/appclient_portal.js')}}"></script>
<script src="{{asset('assets/js/api.js')}}"></script>

</body>
</html>