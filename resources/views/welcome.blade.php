<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="{{asset('assets/icons/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/parsley.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/customized.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/forms.css')}}">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <script src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="https://unpkg.com/vue"></script>
    </head>
    <body>
        <div id="app" >
            <div id="main-container">  
                
                <!-- Start Dialog Box -->
                <div id="coverLayer"></div>
                <div id="dialog-form">
                    <p class="dialog-label">New Staff Entry</p>
                    <form action="">
                        <div class="formRow">
                            <div>
                                <label for="staff_id">staff file no.</label>
                                <input type="text" id="staff_id">
                            </div>
                            <div>
                                <label for="staff_name">name of staff</label>
                                <input type="text" id="staff_name">
                            </div>
                        </div>
                        <div class="formRow">
                            <div class="row-column">
                                <label for="staff-gender">gender</label>
                                <select name="gender" required="required" id="">
                                    <option value=""></option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                            <div class="row-column">
                                <label for="type-of-staff">type of staff</label>
                                <select name="type-of-staff" required="required" id="type-of-staff">
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                            <div class="row-column">
                                <label for="source-of-salary">source of salary</label>
                                <select name="source-of-salary" required="required" id="type-of-staff">
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                        </div>
                        <div class="formRow">
                            <div class="row-column">
                                <label for="staff-year">year of birth</label>
                                <select name="year" required="required" id="staff-year">
                                    <option value=""></option>
                                    <option value="">2000</option>
                                    <option value="">2001</option>
                                    <option value="">2002</option>
                                    <option value="">2003</option>
                                    <option value="">2004</option>
                                    <option value="">2005</option>
                                </select>
                            </div>
                            <div class="row-column">
                                <label for="year-appoint">year of first appointment</label>
                                <select name="year-appoint" required="required" id="year-appoint">
                                    <option value=""></option>
                                    <option value="">2000</option>
                                    <option value="">2001</option>
                                    <option value="">2002</option>
                                    <option value="">2003</option>
                                    <option value="">2004</option>
                                    <option value="">2005</option>
                                </select>
                            </div>
                            <div class="row-column">
                                <label for="">year of present appointment</label>
                                <select name="" required="required" id="">
                                    <option value=""></option>
                                    <option value="">2000</option>
                                    <option value="">2001</option>
                                    <option value="">2002</option>
                                    <option value="">2003</option>
                                    <option value="">2004</option>
                                    <option value="">2005</option>
                                </select>
                            </div>
                            <div class="row-column">
                                <label for="">year of posting to school</label>
                                <select name="" required="required" id="">
                                    <option value=""></option>
                                    <option value="">2000</option>
                                    <option value="">2001</option>
                                    <option value="">2002</option>
                                    <option value="">2003</option>
                                    <option value="">2004</option>
                                    <option value="">2005</option>
                                </select>
                            </div>
                        </div>
                        <div class="formRow">
                            <div class="row-column">
                                <label for="">Grade level/Step</label>
                                <select name="" required="required" id="">
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                            <div class="row-column">
                                <label for="">present</label>
                                <select name="" required="required" id="">
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div class="row-column">
                                <label for="">source of salary</label>
                                <select name="" required="required" id="">
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                        </div>
                        <div class="formRow">
                            <div class="row-column">
                                <label for="">teaching qualification</label>
                                <select name="" required="required" id="">
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                            <div class="row-column">
                                <label for="">area of specialisation</label>
                                <select name="" required="required" id="">
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div class="row-column">
                                <label for="">main subject taught</label>
                                <select name="" required="required" id="">
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="5">6</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                        </div>
                        <div class="formRow">
                            <div class="row-column">
                                <label for="">teaching type</label>
                                <select name="" required="required" id="">
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                            <div class="row-column">
                                <label for="">Tick box if teacher also teaches senior secondary classes in this school</label>
                                <input type="checkbox" name="" id="">
                            </div>
                            <div class="row-column">
                                <label for="">Tick box if teacher also teaches senior secondary classes in this school</label>
                                <input type="checkbox" name="" id="">
                            </div>
                        </div>
                    </form>
                    <div id="dialog-btn">
                        <ul>
                            <li><button id="submit-staff" type="submit">Submit</button></li>
                            <li><button id="cancel">Cancel</button></li>
                        </ul>
                    </div>
                </div>
                <!-- End Dialog Boc -->
                <div id="left-tab">
                    <div class="logo">
                        <a href="http://#">NEMIS</a>
                    </div>
                    <div class="links">
                        <ul>
                            <li class="overview"><a href="http://">Overview</a></li>
                            <li class="search"><a href="http://">Search</a></li>
                            <li class="addSchool"><a href="http://">Add a school</a></li>
                            <li class="preference"><a href="http://">Preferences</a></li>
                            <li class="accounts"><a href="http://">Accounts</a></li>
                            <li class="settings"><a href="http://">Settings</a></li>
                        </ul>
                    </div>
                </div>
                <div id="right-tab">                   
                    <div id="loader"></div>
                    <!-- Breadcrumb Div -->
                    <div >
                        <ul class="breadcrumb alert-success">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Junior Secondary school</a></li>
                            <li class="breadcrumb-item active">@{{schoolname}}
                            </li>
                        </ul>
                    </div>

                    <!-- Forms and Navigation -->
                    <div > 
                        <!-- Div containing the various tabs -->
                        <div class="tab-contents"  >             
                            <div id="tab1" class="show-current-tab">
                                <p class="label">A. School Identification</p>
                                <form action="">
                                    <div class="row">
                                        <div class="question">
                                            <p>FOR ENUMERATOR ONLY:</p>
                                            <em>Was this school in the school list?</em>
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li><input type="radio" required="required" name="enumerator" id="enumerator_yes"><label for="enumerator_yes">Yes</label></li>
                                                <li><input type="radio" required="required" name="enumerator" id="enumerator_no"><label for="enumerator_no">No</label></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>School Code</p>
                                        </div>
                                        <div class="answer">
                                            <input type="text" v-model="schoolcode" name="schoolcode" id="schoolcode" required="required" placeholder="School Code">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>School Coordinates</p>
                                        </div>
                                        <div class="answer">
                                            <input type="text"  name="school_elevation" class="form-control" id="elevation" required="required" placeholder="Elevation (Meter)">
                                            <input type="text"  name="school_latitude" class="form-control" id="latitude_north" required="required" placeholder="Latitude  North">
                                            <input type="text"  name="school_longitude" class="form-control" id="longitude_east" required="required" placeholder="Longitude East">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>A.1 school name</p>
                                        </div>
                                        <div class="answer">
                                            <input type="text" v-model="schoolname" name="schoolname" class="form-control" id="schoolname" required="required" placeholder="School Name">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>A.2 Number and Street Address</p>
                                        </div>
                                        <div class="answer">
                                            <input type="text" name="schoolstreet" v-model="schoolstreet" class="form-control"  id="schoolstreet" required="required" placeholder="Number and Street">   
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>A.3 state</p>
                                        </div>
                                        <div class="answer">
                                            <select v-model="state_picked" v-on:change="getLGA" name="year" id="">
                                                <option v-for="(state,index) in fetch_states" v-bind:value="index">(@{{state.code}}) - @{{state.name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>A.4 LGA</p>
                                        </div>
                                        <div class="answer">
                                        <select v-model="lga_picked" name="year"   id="">
                                            <option v-for="(lga,index) in fetch_lga" v-bind:value="index">(@{{lga.code}}) - @{{lga.name}}</option>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>A.5 Village or Town</p>
                                        </div>
                                        <div class="answer">
                                            <input type="text" v-model="schooltown" name="schooltown" id="schooltown" class="form-control"  required="required" placeholder="Village or Town"> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>A.6 Ward</p>
                                        </div>
                                        <div class="answer">
                                            <input type="text"v-model="schoolward" name="schoolward" class="form-control"  id="schoolward" required="required" placeholder="School Ward">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>A.7 School Telephone</p>
                                        </div>
                                        <div class="answer">
                                            <input type="text" v-model="schooltelephone" name="schooltelephone" class="form-control"  id="schooltelephone" required="required" placeholder="School Telephone">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>A.8 E-Mail Address</p>
                                        </div>
                                        <div class="answer">
                                            <input type="email" v-model="schoolemail" name="schoolemail" id="schoolemail" class="form-control"  required="required" placeholder="School Email Address">
                                        </div>
                                    </div>
                                </form>
                                <div id="button-group">
                                    <ul>
                                        <li><button id="save" v-on:click="saveIdentification">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>   

                            <!--Section B -->              
                            <div id="tab2">
                                <p class="label">B. School Characteristics</p>
                                <form action="">
                                    <div class="row">
                                        <div class="question">
                                            <p>B.1 Year of establishment</p>
                                        </div>
                                        <div class="answer">
                                            <span>@{{year_of_establishment}}</span>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>B.2 Location</p>
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li v-for="(location,index) in fetch_location"><input type="radio" v-bind:value="location.value" v-model="location_of_school" name="location" required="required" id="urban"><label for="urban">@{{location.value}}</label></li>
                                            </ul> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>B.3 Levels of education offered</p>
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li v-for="(level,index) in fetch_levels"><input type="radio" v-bind:value="level.value" v-model="level_of_education" name="level-of-edu" required="required" id="level-of-edu-js"><label for="level-of-edu-js">@{{level.value}}</label></li>
                                            </ul> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>B.4 Type of school</p>
                                            <em>Tick only one to describe school</em>
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li v-for="(category,index) in fetch_category"><input type="radio" v-bind:value="category.value" v-model="type_of_school" name="type-of-school" required="required" id="type-of-school-reg"><label for="">@{{category.value}}</label></li>
                                            </ul> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>B.5 Shifts</p>
                                            <em>Does the School operate shift system?</em>
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li><input type="radio" name="shifts" v-model="shifts_choice" value="1" required="required" id="shifts_yes"><label for="shifts_yes">Yes</label></li>
                                                <li><input type="radio" name="shifts" v-model="shifts_choice" value="0" required="required" id="shifts_no"><label for="shifts_no">No</label></li>
                                            </ul> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>B.6 Shared facilities</p>
                                            <em>Does the school share facilities/Teachers/premises with any other school?</em>
                                            <p>If Yes . How many Schools are sharing facilities: </p>
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li><input type="radio" name="shared-facilities" v-model="facilities_choice" value="1" required="required" id="shared-facilities_yes"><label for="shared-facilities_yes">Yes</label></li>
                                                <li><input type="radio" name="shared-facilities" v-model="facilities_choice" value="0" required="required" id="shared-facilities_no"><label for="shared-facilities_no">No</label></li>
                                            </ul> 
                                            <input type="number" name="" v-model="facilities_shared" required="required" id="no_shared-facilities">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>B.7 Multi-grade teaching</p>
                                            <em>Does any teacher teach more than one class at the same time?</em>
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li><input type="radio" name="multi-grade" v-model="multigrade_choice" value="1" required="required" id="multi-grade_yes"><label for="multi-grade_yes">Yes</label></li>
                                                <li><input type="radio" name="multi-grade" v-model="multigrade_choice" value="0" required="required" id="multi-grade_no"><label for="multi-grade_no">No</label></li>
                                            </ul> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>B.8 School: Average Distance from Catchment communities</p>
                                            <em>What is average distance of school from its catchment areas</em>                                    
                                        </div>
                                        <div class="answer helper">
                                            <input type="number" name="" v-model="average_distance" required="required" id="avg-distance"><label for="avg-distance">kilometres (Enter 0 if within 1 km)</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>B.9 Students: Distance from School</p>
                                            <em>How many students live further than 3km from the school?</em>                                    
                                        </div>
                                        <div class="answer helper">
                                            <input type="number" name="" v-model="student_distance" required="required" id="student-distance"><label for="student-distance">students</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>B.10 Students: Boarding</p>
                                            <em>How many students board at the school premises?</em>                                    
                                        </div>
                                        <div class="answer helper">
                                            <input type="number" name="boarding" v-model="students_boarding_male" required="required" id="boarding_male"><label for="boarding_male">male</label>
                                            <input type="number" name="boarding" v-model="students_boarding_female" required="required" id="boarding_female"><label for="boarding_female">female</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>B.11 School Development Plan (SDP)</p>
                                            <em>Did the school prepare SDP in the last school year?</em>                                    
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li><input type="radio" name="sdp" v-model="sdp_choice" value="1" required="required" id="sdp_yes"><label for="sdp_yes">Yes</label></li>
                                                <li><input type="radio" name="sdp" v-model="sdp_choice" value="0" required="required" id="sdp_no"><label for="sdp_no">No</label></li>
                                            </ul> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>B.12 School Based Management Committee (SBMC)</p>
                                            <em>Does the school have SBMC, which met at least once last year?</em>                                    
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li><input type="radio" name="sbmc" v-model="sbmc_choice" value="1" required="required" id="sbmc_yes"><label for="sbmc_yes">Yes</label></li>
                                                <li><input type="radio" name="sbmc" v-model="sbmc_choice" value="0" required="required" id="sbmc_no"><label for="sbmc_no">No</label></li>
                                            </ul> 
                                        </div>
                                    </div>                            
                                    <div class="row">
                                        <div class="question">
                                            <p>B.13 Parents’-Teachers’ Association (PTA) / Parents Forum (PF)</p>
                                            <em>Does the school have PTA / PF, which met at least once last year?</em>                                    
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li><input type="radio" name="pta" v-model="pta_choice" value="1" required="required" id="pta_yes"><label for="pta_yes">Yes</label></li>
                                                <li><input type="radio" name="pta" v-model="pta_choice" value="0" required="required" id="pta_no"><label for="pta_no">No</label></li>
                                            </ul> 
                                        </div>
                                    </div>                        
                                    <div class="row">
                                        <div class="question">
                                            <p>B.14 Date of Last Inspection Visit</p>
                                            <em>When was the school last inspected?</em>                   
                                            <p>Number of inspection Visit in last academic year</p>                 
                                        </div>
                                        <div class="answer helper">
                                            <input type="date" name="" v-model="date_inspection" required="required" id="">
                                            <input type="number" name="" v-model="no_of_inspection" required="required" id=""><label for="">No.</label>
                                        </div>
                                    </div>                      
                                    <div class="row">
                                        <div class="question">
                                            <p>B.15 Authority of Last Inspection</p>
                                            <em>Which authority conducted the last inspection visit?</em>          
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li v-for="(authority,index) in fetch_authority"><input type="radio" v-bind:value="authority.value" v-model="authority_choice" name="authority-inspection" required="required" id="authority-inspection_fed"><label for="authority-inspection_fed">@{{authority.value}}</label></li>
                                            </ul> 
                                        </div>
                                    </div>               
                                    <div class="row">
                                        <div class="question">
                                            <p>B.16 Conditional Cash Transfer</p>
                                            <em>How many pupils benefitted from Conditional Cash Transfer?</em>          
                                        </div>
                                        <div class="answer helper">
                                            <input type="number" name="" v-model="cash_transfer" required="required" id=""><label for="">No</label>
                                        </div>
                                    </div>                    
                                    <div class="row">
                                        <div class="question">
                                            <p>B.17 School Grants</p>
                                            <em>Has your school ever received grants in the last 2 academic sessions?</em>          
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li><input type="radio" name="school_grants" v-model="grants_choice" value="1" required="required" id="school_grants_yes"><label for="school_grants_yes">yes</label></li>
                                                <li><input type="radio" name="school_grants" v-model="grants_choice" value="0" required="required" id="school_grants_no"><label for="school_grants_no">no</label></li>
                                            </ul> 
                                        </div>
                                    </div>                         
                                    <div class="row">
                                        <div class="question">
                                            <p>B.18 Security Guard</p>
                                            <em>Does the school have a security guard?</em>          
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li><input type="radio" name="security_guard" v-model="guard_choice" value="1" required="required" id="security_guard_yes"><label for="security_guard_yes">yes</label></li>
                                                <li><input type="radio" name="security_guard" v-model="guard_choice" value="0" required="required" id="security_guard_no"><label for="security_guard_no">no</label></li>
                                            </ul> 
                                        </div>
                                    </div>                          
                                    <div class="row">
                                        <div class="question">
                                            <p>B.19 Ownership</p>
                                            <em>Which of the tier of Govt. owned  this school</em>          
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li v-for="(owner,index) in fetch_ownership"><input type="radio"  v-bind:value="owner.value" v-model="ownership_choice" name="ownership" required="required" id="ownership_comm"><label for="ownership_comm">@{{owner.value}}</label></li>
                                            </ul> 
                                        </div>
                                    </div>     
                                </form>          
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save" v-on:click="saveCharacteristics">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>                  
                            
                            <div id="tab3">   
                                <p class="label">C.1 Number of Children with Birth Certificates</p> 
                                <form action="">
                                    <table>
                                        <tr>
                                            <td class="effectGray text label-effect" rowspan="3"><label for="">How many children were enrolled with Birth certificates</label></td>
                                        </tr>                                            
                                        <tr>
                                            <td class="effectGray label-effect" colspan="2"><label for="">JSS 1</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>Male<label></td>
                                            <td class="effectGray label-effect"><label>Female<label></td>
                                        </tr>
                                        <tr>
                                            <td><label for=""></label></td>
                                            <td><input class="form-control" required="required" type="number"></td>
                                            <td><input class="form-control" required="required" type="number"></td>
                                        </tr>
                                        <tr>
                                            <td><label for=""></label></td>
                                            <td><input class="form-control" required="required" type="number"></td>
                                            <td><input class="form-control" required="required" type="number"></td>
                                        </tr>
                                    </table>                                              
                                </form>      
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save"  v-on:click="saveBirthCertificates">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>   
                            <div id="tab4">
                                <p class="label">C.2 New entrants</p>
                                <form action="">
                                    <table>
                                        <tr>
                                            <td class="effectGray"></td>
                                            <td colspan="2" class="effectGray label-effect"><label>New Entrants in JSS 1</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray text label-effect"><label>Student Age</label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for=""></label></td>
                                            <td><input required="required" name="" type="number"></td>
                                            <td><input required="required" name="" type="number"></td>
                                        </tr>                           
                                        <tr>
                                            <td class="text label-effect"><label for="">Total</label></td>
                                            <td><input required="required" type="number" name="" id="" placeholder="0"></td>
                                            <td><input required="required" type="number" name="" id="" placeholder="0"></td>                                            
                                        </tr>
                                    </table>
                                </form>  
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save" >save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="tab5">
                                <p class="label">C.3 Junior Secondary Enrolment for the Current Academic Year by age</p>
                                <form action="">
                                    <table>
                                        <tr>
                                            <td class="effectGray"></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 3</label></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">No. of stream</label></td>
                                            <td colspan="2"><input required="required" type="number" name="" id="" ></td>
                                            <td colspan="2"><input required="required" type="number" name="" id="" ></td>
                                            <td colspan="2"><input required="required" type="number" name="" id="" ></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">No of streams with Multigrade teaching</label></td>
                                            <td colspan="2"><input required="required" type="number" name="" id="" ></td>
                                            <td colspan="2"><input required="required" type="number" name="" id="" ></td>
                                            <td colspan="2"><input required="required" type="number" name="" id="" ></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>Age</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label></label></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>Total</label></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>Repeaters</label></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>No. Completed JSS 3 for previous year</label></td>
                                            <td><input type="number" name="" disabled="disabled" id="" ></td>
                                            <td><input type="number" name="" disabled="disabled" id="" ></td>
                                            <td><input type="number" name="" disabled="disabled" id="" ></td>
                                            <td><input type="number" name="" disabled="disabled" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                        </tr>
                                    </table>
                                </form> 
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="tab6">
                                <p class="label">C.4 Additional Class Information</p>
                                <form action="">
                                    <table>                                         
                                        <tr>
                                            <td class="effectGray text label-effect" rowspan="2"><label>Class</label></td>
                                            <td class="effectGray label-effect" colspan="3"><label>Total Seating available</label></td>
                                        </tr>
                                        <tr>                   
                                            <td class="effectGray label-effect"><label>1 Seater</label></td>
                                            <td class="effectGray label-effect"><label>2 Seater</label></td>
                                            <td class="effectGray label-effect"><label>3 Seater</label></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>JSS 1</label></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>JSS 2</label></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>JSS 2</label></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                            <td><input required="required" type="number" name="" id="" ></td>
                                        </tr>
                                    </table>
                                </form>
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="tab7">
                                <p class="label">C.5 Students Flow for the Current Academic Year Junior Secondary School</p>
                                <form action="">
                                    <table>
                                        <tr>
                                            <td class="effectGray text label-effect" rowspan="2"><label>Student Flow</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 3</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>Dropout</label></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>Transfer in</label></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                        <tr>
                                            <td class="text label-effect"><label>Transfer out</label></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td> 
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>Promoted</label></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td> 
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                            <td ><input  required="required" type="number" name="a" id="" ></td>
                                        </tr>
                                    </table>        
                                </form>
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="tab8">
                                <p class="label">C.6 Students with Special Needs for the Current Academic Year</p>
                                <form action="">
                                    <table>
                                        <tr>
                                            <td class="effectGray text label-effect" rowspan="2"><label>Challenge that impacts the ability to learn</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 3</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                        </tr>
                                        <tr >
                                            <td class="text label-effect"><label></label></td>
                                            <td><input type="number" required="required"  name="" id="" ></td>
                                            <td><input type="number" required="required"  name="" id="" ></td>
                                            <td><input type="number" required="required"  name="" id="" ></td>
                                            <td><input type="number" required="required"  name="" id="" ></td>
                                            <td><input type="number" required="required"  name="" id="" ></td>
                                            <td><input type="number" required="required"  name="" id="" ></td>
                                        </tr>
                                    </table>
                                </form>
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="tab9">
                                <p class="label">C.7 JSCE examination for the previous Academic Year</p>
                                <form action="">
                                    <table>
                                        <tr>
                                            <td class="effectGray label-effect"><label for=""></label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                            <td class="effectGray label-effect"><label for="">Total</label></td>
                                        </tr>     
                                        <tr>
                                            <td class="text label-effect"><label for="">How many students were registered for JSCE?</label></td>
                                            <td><input type="number" required="required" name="" id=""></td>
                                            <td><input type="number" required="required" name="" id=""></td>
                                            <td><input type="number" required="required" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">How many students took part in the JSCE?</label></td>
                                            <td><input type="number" required="required" name="" id=""></td>
                                            <td><input type="number" required="required" name="" id=""></td>
                                            <td><input type="number" required="required" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">How many students passed JSCE?</label></td>
                                            <td><input type="number" required="required" name="" id=""></td>
                                            <td><input type="number" required="required" name="" id=""></td>
                                            <td><input type="number" required="required" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="tab10">
                                <p class="label">D. Staff</p>
                                <form action="">
                                    <div class="row">
                                        <div class="question">
                                            <p>D.1 How many non-teaching staff are working at the school?</p>                  
                                        </div>
                                        <div class="answer helper">
                                            <input type="number" name="" required="required" id=""><label for="">male</label>
                                            <input type="number" name="" required="required" id=""><label for="">female</label>
                                            <input type="number" name="" required="required" id=""><label for="">Total</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>D.2 How many teachers are working at the school regardless of whether they are currently present or on course or absent</p>                  
                                        </div>
                                        <div class="answer helper">
                                            <input type="number" name="" required="required" id=""><label for="">male</label>
                                            <input type="number" name="" required="required" id=""><label for="">female</label>
                                            <input type="number" name="" required="required" id=""><label for="">Total</label>
                                        </div>
                                    </div>
                                </form>
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>

                            <div id="tab11">
                                <p class="label">D.3 Information on all staff during the school year</p>
                                <div class="row table-responsive">
                                    <form action="">
                                        <table class="table table-striped">
                                            <thead>
                                                <th>No.</th>
                                                <th>Staff File No</th>
                                                <th>Name of staff</th>
                                                <th>Gender</th>
                                                <th>Type of staff</th>
                                                <th>Source of salary</th>
                                                <th>Year of Birth</th>
                                                <th>Year of first appointment</th>
                                                <th>Year of present appointment</th>
                                                <th>Year of posting to the school</th>
                                                <th>Grade level / Step</th>
                                                <th>Present</th>
                                                <th>Academic Qualification</th>
                                                <th>Teaching Qualification</th>
                                                <th>Area of specialization</th>
                                                <th>Main subject taught</th>
                                                <th>Teaching type</th>
                                                <th>Tick box if teacher also teaches senior secondary classes in this school</th>
                                                <th>Tick box if teacher attended training workshop / seminar in last 12 months</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>P4567</td>
                                                    <td>Fred Abdul</td>
                                                    <td>M</td>
                                                    <td>1</td>
                                                    <td>1</td>
                                                    <td>1998</td>
                                                    <td>1996</td>
                                                    <td>1998</td>
                                                    <td>1996</td>
                                                    <td>7 / 2</td>
                                                    <td>1</td>
                                                    <td>2</td>
                                                    <td>3</td>
                                                    <td>1</td>
                                                    <td>2</td>
                                                    <td>1</td>
                                                    <td><input type="checkbox" name="" id=""></td>
                                                    <td><input type="checkbox" name="" id=""></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <div class="addBtn">
                                    <ul>
                                        <li><button class="btn-success" id="addBtn"></button></li>
                                    </ul>
                                </div>
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="tab12">
                                <p class="label">E. Classrooms</p>
                                <form action="">
                                    <div class="row">
                                        <div class="question">
                                            <p>E.1 How many classrooms are there in the school?</p>                  
                                        </div>
                                        <div class="answer helper">
                                            <input type="number" name="" required="required" id=""><label for="">Number</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>E.2 Are any classes held outside (because classrooms are unusable or insufficient)?</p>                  
                                        </div>
                                        <div class="answer">
                                            <ul>
                                                <li><input type="radio" name="sufficient" required="required" id="sufficient_yes"><label for="sufficient_yes">Yes</label></li>
                                                <li><input type="radio" name="sufficient" required="required" id="sufficient_no"><label for="sufficient_no">No</label></li>
                                            </ul> 
                                        </div>
                                    </div>
                                </form>
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="tab13">
                                <p class="label">E.3 Information on all classrooms</p>
                                <div class="row table-responsive">
                                    <form action="">
                                        <table class="table table-striped">
                                            <thead>
                                                <th>No.</th>
                                                <th>Year of construction</th>
                                                <th>Present condition</th>
                                                <th>Length in metres</th>
                                                <th>Width in metres</th>
                                                <th>Floor material</th>
                                                <th>Walls material</th>
                                                <th>Roof material</th>
                                                <th>Seating</th>
                                                <th>Good blackboard</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>4567</td>
                                                    <td>2</td>
                                                    <td>7</td>
                                                    <td>1</td>
                                                    <td>1</td>
                                                    <td>8</td>
                                                    <td>6</td>
                                                    <td>9</td>
                                                    <td>1</td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>4567</td>
                                                    <td>2</td>
                                                    <td>7</td>
                                                    <td>1</td>
                                                    <td>1</td>
                                                    <td>8</td>
                                                    <td>6</td>
                                                    <td>9</td>
                                                    <td>1</td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>4567</td>
                                                    <td>2</td>
                                                    <td>7</td>
                                                    <td>1</td>
                                                    <td>1</td>
                                                    <td>8</td>
                                                    <td>6</td>
                                                    <td>9</td>
                                                    <td>1</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="tab14">
                                <p class="label">E.4 Number of rooms other than classrooms are there in the school by type of room</p>
                                <form action="">
                                    <div class="row">
                                        <div class="question">
                                            <p>1. Staff rooms</p>                  
                                        </div>
                                        <div class="answer helper">
                                            <input type="number" name="" required="required" id=""><label for="">Number</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>2. Office</p>                  
                                        </div>
                                        <div class="answer helper">
                                            <input type="number" name="" required="required" id=""><label for="">Number</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>3. Library</p>                  
                                        </div>
                                        <div class="answer helper">
                                            <input type="number" name="" required="required" id=""><label for="">Number</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>4. Store room</p>                  
                                        </div>
                                        <div class="answer helper">
                                            <input type="number" name="" required="required" id=""><label for="">Number</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="question">
                                            <p>5. Others</p>                  
                                        </div>
                                        <div class="answer helper">
                                            <input type="number" name="" required="required" id=""><label for="">Number</label>
                                        </div>
                                    </div>
                                </form>
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="tab">
                                <p class="label"></p>
                                <form action=""></form>
                                <div id="button-group">
                                    <ul>
                                        <li><button id="previous">previous</button></li>
                                        <li><button id="save">save</button></li>
                                        <li><button id="next">next</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        
    </body>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/parsley.min.js')}}"></script>
    <script src="{{asset('assets/js/functions.js')}}"></script>
    <script src="{{asset('assets/js/nemis_databind.js')}}"></script>
</html>
