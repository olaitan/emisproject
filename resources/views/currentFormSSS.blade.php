﻿<!-- THIS IS FORM SSS-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin | Dashboard</title>
    <!-- StyleSheets -->
    <link rel="shortcut icon" href="{{asset('assets/icons/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/parsley.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/customized.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/forms.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/users.css')}}">
    <!-- scripting -->
    <script src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
</head>
<body>
<div id="app">
    <!-- Main Container -->
    <div id="main-container">
        <!-- Start Dialog Box -->
        <div id="coverLayer"></div>

        <!-- For the staff -->
        <div id="dialog-form">
            <div id="cancel" class="closeme">
                <p class="dialog-label"></p>
            </div>
            <!-- Removing of Staff -->
            <div class="removeStaff" id="clearStaff">
                <div class="form-Box">  
                    <p class="dialog-label">Remove Staff</p>
                    <form action="" id="RemoveStaffForm">
                        <div class="">                                
                            <div class="formRow"> 
                                <label for="">Category</label>
                                <select name="cat-option" required="required" id="">
                                    <option value=""></option>
                                    <option value="transferred">Transferred</option>
                                    <option value="death">Death</option>
                                    <option value="sacked">Sacked</option>
                                    <option value="retire">Retired</option>
                                </select>
                            </div>
                            <div class="formRow">
                                <label for="">School</label>
                                <select name="school" required="required">
                                    <option value="">School Name</option>
                                    <option value="">School Name</option>
                                    <option value="">School Name</option>
                                    <option value="">School Name</option>
                                    <option value="">School Name</option>
                                </select>
                            </div>
                            <div class="formRow">  
                                <label for="staff-year">year</label>
                                <select name="year" required="required" id="staff-year">
                                    <option value=""></option>
                                    <option value="">2000</option>
                                    <option value="">2001</option>
                                    <option value="">2002</option>
                                    <option value="">2003</option>
                                    <option value="">2004</option>
                                    <option value="">2005</option>
                                </select>
                            </div> 
                        </div>
                    </form>
                    <div id="Buttons">
                        <button id="Remove">Remove Staff</button>
                    </div>                        
                </div>                  
            </div>
            <!-- Navigation Tabs -->
            <nav>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="Staff-List" data-toggle="tab" href="#StaffList" role="tab" aria-controls="home" aria-selected="true">Existing Staff List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Add-Staff" data-toggle="tab" href="#AddStaff" role="tab" aria-controls="profile" aria-selected="false">Add New Staff</a>
                    </li>
                </ul>
            </nav>
            <!-- Tab panes -->
            <div class="tab-content" id="allowFlow">
                <div class="tab-pane active" id="StaffList" role="tabpanel" aria-labelledby="Staff-List">  
                    <p class="dialog-label">Current Staff List</p> 
                    <div class="table-responsive mainCover">
                        <table class="table table-hover" id="StaffList">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col"><input type="checkbox" name="" id=""></th>
                                    <th scope="col">Staff file no</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Gender</th>
                                    <th scope="col">Type of staff</th>
                                    <th scope="col">Source of Salary</th>
                                    <th scope="col">Year of Birth</th>
                                    <th scope="col">Year of First Appointment</th>
                                    <th scope="col">Year of Present Appointment</th>
                                    <th scope="col">Year of Posting to this School</th>
                                    <th scope="col">Grade level/Step</th>
                                    <th scope="col">Present	Academic Qualification</th>
                                    <th scope="col">Teaching Qualification</th>
                                    <th scope="col">Subject of Qualification</th>
                                    <th scope="col">Area of Specialisation</th>
                                    <th scope="col">Main Subbject Taught</th>
                                    <th scope="col">Teaching type</th>
                                    <th scope="col">Teaches both junior & senior secondary classes</th>
                                    <th scope="col">Teacher attended training workshop / seminar</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td><input type="text" class="StaffNum" name="" id="" placeholder="P456"></td>
                                    <td><input type="text" class="StaffName" name="" id="" placeholder="Fred Abdul"></td>
                                    <td>
                                        <select name="gender" id="">
                                            <option value="" disabled="disabled"></option>
                                            <option value="female">F</option>
                                            <option value="male">M</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="type-of-staff" id="">
                                            <option value="" disabled="disabled"></option>
                                            <option value="">1 – Principal	</option>
                                            <option value="">2</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="source-of-salary" id="">
                                            <option value="" disabled="disabled"></option>
                                            <option value="">1</option>
                                            <option value="">2</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="year-of-birth" id="">
                                            <option value="" disabled="disabled"></option>
                                            <option value="">1998</option>
                                            <option value="">1992</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="year-of-first-appointment" id="">
                                            <option value="" disabled="disabled"></option>
                                            <option value="">1998</option>
                                            <option value="">1992</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="year-of-present-appointment" id="">
                                            <option value="" disabled="disabled"></option>
                                            <option value="">1998</option>
                                            <option value="">1992</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="year-of-posting" id="">
                                            <option value="" disabled="disabled"></option>
                                            <option value="">1998</option>
                                            <option value="">1992</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input class="Grade" type="text" name="" id="" placeholder="7/2">
                                    </td>
                                    <td>
                                        <select name="present-academic-qualification" id="">
                                            <option value="" disabled="disabled"></option>
                                            <option value="">1998</option>
                                            <option value="">1992</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="teaching-qualification" id="">
                                            <option value="" disabled="disabled"></option>
                                            <option value="">1998</option>
                                            <option value="">1992</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="subject-qualification" id="">
                                            <option value="" disabled="disabled"></option>
                                            <option value="">1998</option>
                                            <option value="">1992</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="area-of-specialization" id="">
                                            <option value="" disabled="disabled"></option>
                                            <option value="">1998</option>
                                            <option value="">1992</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="main-subject-taught" id="">
                                            <option value="" disabled="disabled"></option>
                                            <option value="">1998</option>
                                            <option value="">1992</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="teaching-type" id="">
                                            <option value="" disabled="disabled"></option>
                                            <option value="">1998</option>
                                            <option value="">1992</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="checkbox" name="teach-both-jss-sss" id="">
                                    </td>
                                    <td>
                                        <input type="checkbox" name="teach-both-jss-sss" id="">
                                    </td>
                                    <td id="actionBtn">
                                        <p id="more"></p>
                                    </td>
                                </tr>                   
                            </tbody>
                        </table>
                    </div> 
                    <div>
                        <button id="addStaff">Add Current Staff</button>
                    </div>                         
                </div>  
                <div class="tab-pane" id="AddStaff" role="tabpanel" aria-labelledby="Add-Staff">
                    <div class="addNewStaff" id="sizingStaff">
                        <div class="form-Box">  
                            <p class="dialog-label">New Staff Entry</p>
                            <form action="" id="NewStaffForm">
                                <div class="category">
                                    <div class="formRow">
                                        <label for="staff_id">staff file no.</label>
                                        <input type="text" id="staff_id">
                                    </div>
                                    <div class="formRow">  
                                        <label for="staff_name">name of staff</label>
                                        <input type="text" id="staff_name">
                                    </div> 
                                    <div class="formRow"> 
                                        <label for="staff-gender">gender</label>
                                        <select v-model="staff_gender" name="gender" required="required" id="">
                                            <option value="" disabled></option>
                                            <option value="m">Male</option>
                                            <option value="f">Female</option>
                                        </select>
                                    </div>
                                    <div class="formRow">
                                        <label for="type-of-staff">type of staff</label>
                                        <select v-model="staff_type"  name="source-of-salary"  id="type-of-staff">
                                            <option v-for="(source,index) in fetch_stafftype" v-bind:value="source.value"> @{{source.value}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="category">
                                    <div class="formRow">
                                        <label for="staff_id">source of salary</label>
                                        <select v-model="salary_source"  name="source-of-salary"  id="type-of-staff">
                                            <option v-for="(source,index) in fetch_salarysource" v-bind:value="source.value"> @{{source.value}}</option>
                                        </select>
                                    </div>
                                    <div class="formRow">  
                                        <label for="staff-year">year of birth</label>
                                        <select name="year" v-model="staff_yob" required="required" id="staff-year">
                                            <option value="" disabled></option>
                                            <option value="2000">2000</option>
                                            <option value="2000">2001</option>
                                            <option value="2000">2002</option>
                                            <option value="2000">2003</option>
                                        </select>
                                    </div> 
                                    <div class="formRow"> 
                                        <label for="staff-gender">year of first appointment</label>
                                        <select name="year-appoint" v-model="staff_yfa" required="required" id="year-appoint">
                                            <option value="" disabled></option>
                                            <option value="2000">2000</option>
                                            <option value="2000">2001</option>
                                            <option value="2000">2002</option>
                                            <option value="2000">2003</option>
                                            <option value="2000">2004</option>
                                            <option value="2000">2005</option>
                                        </select>
                                    </div>
                                    <div class="formRow">
                                        <label for="type-of-staff">year of present appointment</label>
                                        <select name="" v-model="staff_ypa" required="required" id="">
                                            <option value="" disabled></option>
                                            <option value="2000">2000</option>
                                            <option value="2000">2001</option>
                                            <option value="2000">2002</option>
                                            <option value="2000">2003</option>
                                            <option value="2000">2004</option>
                                            <option value="2000">2005</option>
                                        </select>
                                    </div>
                                    <div class="formRow">
                                        <label for="">year of posting to school</label>
                                        <select name="" v-model="staff_yps" required="required" id="">
                                            <option value="" disabled></option>
                                            <option value="2000">2000</option>
                                            <option value="2000">2001</option>
                                            <option value="2000">2002</option>
                                            <option value="2000">2003</option>
                                            <option value="2000">2004</option>
                                            <option value="2000">2005</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="category">
                                    <div class="formRow">
                                        <label for="">Grade level/Step</label>
                                        <select name="" required="required" id="">
                                            <option value=""></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                    <div class="formRow">  
                                        <label for="">present</label>
                                        <select name="" required="required" id="">
                                            <option value=""></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div> 
                                    <div class="formRow"> 
                                        <label for="">academic qualification</label>
                                        <select name="" required="required" id="">
                                            <option value=""></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </div>
                                    <div class="formRow">
                                        <label for="">teaching qualification</label>
                                        <select name="" required="required" id="">
                                            <option value=""></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </div>
                                    <div class="formRow">
                                        <label for="">area of specialisation</label>
                                        <select name="" required="required" id="">
                                            <option value=""></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="category">
                                    <div class="formRow">
                                        <label for="">main subject taught</label>
                                        <select name="" required="required" id="">
                                            <option value=""></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="5">6</option>
                                            <option value="6">6</option>
                                        </select>
                                    </div>
                                    <div class="formRow">  
                                        <label for="">teaching type</label>
                                        <select name="" required="required" id="">
                                            <option value=""></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </div> 
                                    <div class="formRow"> 
                                        <label for="">Tick box if teacher also teaches senior secondary classes in this school</label>
                                        <input type="checkbox" name="" id="">
                                    </div>
                                    <div class="formRow">
                                        <label for="">Tick box if teacher also teaches senior secondary classes in this school</label>
                                        <input type="checkbox" name="" id="">
                                    </div>
                                </div>
                            </form>
                            <div style="text-align:center;margin-top:25px;">
                                <span class="step"></span>
                                <span class="step"></span>
                                <span class="step"></span>
                                <span class="step"></span>
                            </div>
                            <div id="Buttons">
                                <button id="Previous" onclick="NextPrev(-1)">Previous</button>
                                <button id="Next" onclick="NextPrev(1)" v-on:click="submitStaff">Next</button>
                            </div>                        
                        </div>                  
                    </div> 
                </div> 
            </div>
        </div>

        <!-- For Classrooms -->
        <div id="addClassRooms">
            <div class="classes">
                <div class="form-Box">  
                    <p class="dialog-label">Information on new Classrooms</p>
                    <form action="" id="StaffForm">
                        <div>
                            <div class="formRow">
                                <label for="staff_id">Year of construction</label>
                                <select name="" id="">
                                    <option value="">1995</option>
                                    <option value="">1996</option>
                                    <option value="">1997</option>
                                    <option value="">1998</option>
                                    <option value="">1999</option>
                                </select>
                            </div>
                            <div class="formRow">  
                                <label for="">Present condition</label>
                                    <select name="" id="">
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                        <option value="">4</option>
                                        <option value="">5</option>
                                    </select>
                            </div> 
                            <div class="formRow special">     
                                <div class="left">                                                                  
                                    <label for="">Length(metre)</label>
                                    <input type="number" name="" id="">
                                </div>
                                <div class="right"> 
                                    <label for="">Width(metre)</label>
                                    <input type="number" name="" id="">
                                </div> 
                            </div>                            
                            <div class="formRow  special">
                                <div>
                                    <label for="">Floor material</label>
                                    <select name="year" required="required" id="staff-year">
                                        <option value="" disabled></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                                <div>
                                    <label for="year-appoint">Wall Materials</label>
                                    <select name="year-appoint" required="required" id="year-appoint">
                                        <option value="" disabled></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                    </select>
                                </div>
                            </div>
                            <div class="formRow special"> 
                                <div>
                                    <label for="">Seating</label>
                                    <select name="" required="required" id="">
                                        <option value="" disabled></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                                <div>
                                    <label for="">Good black board</label>
                                    <select name="" required="required" id="">
                                        <option value="" disabled></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div id="Buttons">
                        <button id="submitClassRoom" type="submit">Submit</button>
                        <button id="cancelClassRoom">Cancel</button>
                    </div>                        
                </div>  
            </div>                
        </div>

        <!-- For Workshops -->
        <div id="addWorkShop">
            <div class="classes">
                <div class="form-Box">
                    <p class="dialog-label">Information on new Workshops</p>
                    <form action="">
                        <div class="formRow special">
                            <div>
                                <label for="">Type of workshop</label>
                                <select name="" id="">
                                    <option value="">1995</option>
                                    <option value="">1996</option>
                                    <option value="">1997</option>
                                    <option value="">1998</option>
                                    <option value="">1999</option>
                                </select>
                            </div>
                            <div>
                                <label for="">Shared</label>
                                <select name="" id="">
                                    <option value="">1</option>
                                    <option value="">2</option>
                                    <option value="">3</option>
                                    <option value="">4</option>
                                    <option value="">5</option>
                                </select>
                            </div>
                        </div>
                        <div class="formRow special">
                            <div>
                                <label for="">Year of Construction</label>
                                <select name="" id="">
                                    <option value="">1995</option>
                                    <option value="">1996</option>
                                    <option value="">1997</option>
                                    <option value="">1998</option>
                                    <option value="">1999</option>
                                </select>
                            </div>
                            <div>
                                <label for="">Present Condition</label>
                                <select name="" id="">
                                    <option value="">1</option>
                                    <option value="">2</option>
                                    <option value="">3</option>
                                    <option value="">4</option>
                                    <option value="">5</option>
                                </select>
                            </div>
                        </div>
                        <div class="formRow special">
                            <div>
                                <label for="">Length(metres)</label>
                                <input type="number" name="" id="">
                            </div>
                            <div>
                                <label for="">Width(metres)</label>
                                <input type="number" name="" id="">
                            </div>
                        </div>
                        <div class="formRow special">
                            <div>
                                <label for="">Floor material</label>
                                <select name="" id="">
                                    <option value="">1995</option>
                                    <option value="">1996</option>
                                    <option value="">1997</option>
                                    <option value="">1998</option>
                                    <option value="">1999</option>
                                </select>
                            </div>
                            <div>
                                <label for="">Walls material</label>
                                <select name="" id="">
                                    <option value="">1995</option>
                                    <option value="">1996</option>
                                    <option value="">1997</option>
                                    <option value="">1998</option>
                                    <option value="">1999</option>
                                </select>
                            </div>
                        </div>
                        <div class="formRow special">
                            <div>
                                <label for="">Roof Material</label>
                                <select name="" id="">
                                    <option value="">1995</option>
                                    <option value="">1996</option>
                                    <option value="">1997</option>
                                    <option value="">1998</option>
                                    <option value="">1999</option>
                                </select>
                            </div>
                            <div>
                                <label for="">Seating</label>
                                <select name="" id="">
                                    <option value="">1995</option>
                                    <option value="">1996</option>
                                    <option value="">1997</option>
                                    <option value="">1998</option>
                                    <option value="">1999</option>
                                </select>
                            </div>
                        </div>
                        <div class="formRow">
                            <div>
                                <label for="">Good blackboard</label>
                                <select name="" id="">
                                    <option value="">1995</option>
                                    <option value="">1996</option>
                                    <option value="">1997</option>
                                    <option value="">1998</option>
                                    <option value="">1999</option>
                                </select>
                            </div>
                        </div>
                    </form>
                    <div id="dialog-btn">
                        <ul>
                            <li><button id="submit-workshop" type="submit">Submit</button></li>
                            <li><button id="cancelWks">Cancel</button></li>
                        </ul>
                    </div>
                </div>
            </div>            
        </div>
        
        <!-- End Dialog Boc -->
        <div id="left-tab">
            <div class="logo">
                <span id="collapse"></span>
                <a href="/"><p>NEMIS</p></a>
            </div>            
            <div class="links">
                <ul>
                    <li><a href="/"><span class="overview"></span><p>Overview</p></a></li>
                    <li><a href="/search"><span class="search"></span><p>Search</p></a></li>
                    <li><a href="/new"><span class="addSchool"></span><p>Add a school</p></a></li>
                    <li><a href="/" id="moreLinks" class="moreLinks"><span class="settings"></span><p>Settings</p></a>
                        <ul id="extraLinks">
                            <li><a href="/metadatas">Configure Metadatas</a></li>
                            <li><a href="/">Color Code</a></li>
                        </ul>
                    </li>
                </ul>
            </div> 
            <div class="users">
                <div class="user-avatar">
                    <img src="" alt="IA" srcset="" >
                </div>
                <h2 class="name"><a href="" id="showDetails">Israel Akpan</a></h2>
                <p class="role">Product Designer</p>

                <div class="users-links" id="users-links">
                    <ul>
                        <li><a href="/userID">View Profile</a></li>
                        <li><a href="/config">Configure</a></li>
                        <li><a href="/edituser">Accounts</a></li>
                        <li><a href="/login">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div id="right-tab">
            <div id="alert">
                <p>Saved Successfully...</p>
                <div class="close"></div>
            </div>
            <div id="loader"></div>
            <!-- Breadcrumb Div -->
            <div>
                <ul class="breadcrumb alert-success">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a v-bind:href="schoolstate_href">@{{schoolstate}}</a></li>
                    <li class="breadcrumb-item"><a v-bind:href="lga_href">@{{schoollga}}</a></li>
                    <li class="breadcrumb-item"><a v-bind:href="school_type_href">Public </a></li>
                    <li class="breadcrumb-item"><a v-bind:href="level_of_education_href">@{{level_of_education}}</a></li>
                    <li class="breadcrumb-item active">@{{schoolname}}</li>
                </ul>
            </div>

            <!-- Forms and Navigation -->
            <div>
                <!-- Div containing the various tabs -->
                <div class="tab-contents" id="content">
                    <div id="tab1" class="show-current-tab">
                        <p class="label">A. School Identification</p>
                        <form action="">
                            <div class="row">
                                <div class="question">
                                    <p>Census Year</p>
                                </div>
                                <div class="answer">
                                    <select name="" id="">
                                        <option value="2020">2020</option>
                                        <option value="2019">2019</option>
                                        <option value="2018">2018</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>School Code</p>
                                </div>
                                <div class="answer">
                                    <input type="text" v-model="schoolcode"  name="schoolcode" id="schoolcode" required="required" placeholder="School Code">
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>School Coordinates</p>
                                </div>
                                <div class="answer">
                                    <input type="text" v-model="xcoordinate" name="school_elevation" class="form-control" id="elevation" required="required" placeholder="Elevation (Meter)">
                                    <input type="text" v-model="ycoordinate" name="school_latitude" class="form-control" id="latitude_north" required="required" placeholder="Latitude  North">
                                    <input type="text" v-model="zcoordinate" name="school_longitude" class="form-control" id="longitude_east" required="required" placeholder="Longitude East">
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>A.1 school name</p>
                                </div>
                                <div class="answer">
                                    <input type="text" v-model="schoolname"  name="schoolname" class="form-control" id="schoolname" required="required" placeholder="School Name">
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>A.2 Number and Street Address</p>
                                </div>
                                <div class="answer">
                                    <input type="text" v-model="schoolstreet"  name="schoolstreet"  class="form-control"  id="schoolstreet" required="required" placeholder="Number and Street">
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>A.3 state</p>
                                </div>
                                <div class="answer">
                                    <select v-model="state_picked" v-on:change="getLGA" name="year" id="">
                                        <option v-for="(state,index) in fetch_states" v-bind:value="index">(@{{state.code}}) - @{{state.name}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>A.4 LGA</p>
                                </div>
                                <div class="answer">
                                    <select v-model="lga_picked" name="year"   id="">
                                        <option v-for="(lga,index) in fetch_lga" v-bind:value="index">(@{{lga.code}}) - @{{lga.name}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>A.5 Village or Town</p>
                                </div>
                                <div class="answer">
                                    <input type="text" v-model="schooltown" name="schooltown" id="schooltown" class="form-control"  required="required" placeholder="Village or Town">
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>A.6 ward</p>
                                </div>
                                <div class="answer">
                                    <input type="text"v-model="schoolward" name="schoolward" class="form-control"  id="schoolward" required="required" placeholder="School Ward">
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>A.7 School Telephone</p>
                                </div>
                                <div class="answer">
                                    <input type="text" v-model="schooltelephone" name="schooltelephone" class="form-control"  id="schooltelephone" required="required" placeholder="School Telephone">
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>A.8 E-Mail Address</p>
                                </div>
                                <div class="answer">
                                    <input type="email" v-model="schoolemail" name="schoolemail" id="schoolemail" class="form-control"  required="required" placeholder="School Email Address">
                                </div>
                            </div>
                        </form>
                        <div id="button-group">
                            <ul>
                                <li><button id="save" v-on:click="saveIdentification">save</button></li>
                                <li><button id="next" v-on:click="saveIdentification">next</button></li>
                            </ul>
                        </div>
                    </div>
                    <div id="tab2">
                        <p class="label">B. School Characteristics</p>
                        <form action="">
                            <div class="row">
                                <div class="question">
                                    <p>B.1 Year of establishment</p>
                                </div>
                                <div class="answer">
                                        <input type="text" v-model="year_of_establishment" name="year_of_establishment"   id="schooltelephone" required="required" placeholder="School Telephone">
                                    </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.2 Location</p>
                                </div>
                                <div class="answer">
                                <ul>
                                                <li v-for="(location,index) in fetch_location"><input type="radio" v-bind:value="location.value" v-model="location_of_school" name="location" required="required" id="urban"><label for="urban">@{{location.value}}</label></li>
                                            </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.3 Levels of education offered</p>
                                </div>
                                <div class="answer">
                                <ul>
                                                <li v-for="(level,index) in fetch_levels"><input type="radio" v-bind:value="level.value" v-model="level_of_education" name="level-of-edu" required="required" id="level-of-edu-js"><label for="level-of-edu-js">@{{level.value}}</label></li>
                                            </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.4 Type of school</p>
                                    <em>Tick only one to describe school</em>
                                </div>
                                <div class="answer">
                                <ul>
                                                <li v-for="(category,index) in fetch_category"><input type="radio" v-bind:value="category.value" v-model="type_of_school" name="type-of-school" required="required" id="type-of-school-reg"><label for="">@{{category.value}}</label></li>
                                            </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.5 Shifts</p>
                                    <em>Does the School operate shift system?</em>
                                </div>
                                <div class="answer">
                                    <ul>
                                        <li><input type="radio" name="shifts" v-model="shifts_choice" value="1"  id="shifts_yes"><label for="shifts_yes">Yes</label></li>
                                        <li><input type="radio" name="shifts" v-model="shifts_choice" value="0"  id="shifts_no"><label for="shifts_no">No</label></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.6 Shared facilities</p>
                                    <em>Does the school share facilities/Teachers/premises with any other school?</em>
                                    <p>If Yes . How many Schools are sharing facilities: </p>
                                    <em>(Please provide  in  Number)</em>
                                </div>
                                <div class="answer">
                                    <ul>
                                        <li><input type="radio" name="shared-facilities" v-model="facilities_choice" value="1" required="required" id="shared-facilities_yes"><label for="shared-facilities_yes">Yes</label></li>
                                        <li><input type="radio" name="shared-facilities" v-model="facilities_choice" value="0" required="required" id="shared-facilities_no"><label for="shared-facilities_no">No</label></li>
                                    </ul>
                                    <input type="number" name="" v-model="facilities_shared" required="required" id="no_shared-facilities">
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.7 Multi-grade teaching</p>
                                    <em>Does any teacher teach more than one class at the same time?</em>
                                </div>
                                <div class="answer">
                                <ul>
                                                <li><input type="radio" name="multi-grade" v-model="multigrade_choice" value="1" required="required" id="multi-grade_yes"><label for="multi-grade_yes">Yes</label></li>
                                                <li><input type="radio" name="multi-grade" v-model="multigrade_choice" value="0" required="required" id="multi-grade_no"><label for="multi-grade_no">No</label></li>
                                            </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.8 School: Average Distance from Catchment communities</p>
                                    <em>What is average distance of school from its catchment areas</em>
                                </div>
                                <div class="answer helper">
                                            <input type="number" name="" v-model="average_distance" required="required" id="avg-distance"><label for="avg-distance">kilometres (Enter 0 if within 1 km)</label>
                                        </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.9 Students: Distance from School</p>
                                    <em>How many students live further than 3km from the school?</em>
                                </div>
                                <div class="answer helper">
                                            <input type="number" name="" v-model="student_distance" required="required" id="student-distance"><label for="student-distance">students</label>
                                        </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.10 Students: Boarding</p>
                                    <em>How many students board at the school premises?</em>
                                </div>
                                <div class="answer helper">
                                            <input type="number" name="boarding" v-model="students_boarding_male" required="required" id="boarding_male"><label for="boarding_male">male</label>
                                            <input type="number" name="boarding" v-model="students_boarding_female" required="required" id="boarding_female"><label for="boarding_female">female</label>
                                        </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.11 School Development Plan (SDP)</p>
                                    <em>Did the school prepare SDP in the last school year?</em>
                                </div>
                                <div class="answer">
                                <ul>
                                                <li><input type="radio" name="sdp" v-model="sdp_choice" value="1" required="required" id="sdp_yes"><label for="sdp_yes">Yes</label></li>
                                                <li><input type="radio" name="sdp" v-model="sdp_choice" value="0" required="required" id="sdp_no"><label for="sdp_no">No</label></li>
                                            </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.12 School Based Management Committee (SBMC)</p>
                                    <em>Does the school have SBMC, which met at least once last year?</em>
                                </div>
                                <div class="answer">
                                <ul>
                                                <li><input type="radio" name="sbmc" v-model="sbmc_choice" value="1" required="required" id="sbmc_yes"><label for="sbmc_yes">Yes</label></li>
                                                <li><input type="radio" name="sbmc" v-model="sbmc_choice" value="0" required="required" id="sbmc_no"><label for="sbmc_no">No</label></li>
                                            </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.13 Parents’-Teachers’ Association (PTA) / Parents Forum (PF)</p>
                                    <em>Does the school have PTA / PF, which met at least once last year?</em>
                                </div>
                                <div class="answer">
                                <ul>
                                                <li><input type="radio" name="pta" v-model="pta_choice" value="1" required="required" id="pta_yes"><label for="pta_yes">Yes</label></li>
                                                <li><input type="radio" name="pta" v-model="pta_choice" value="0" required="required" id="pta_no"><label for="pta_no">No</label></li>
                                            </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.14 Date of Last Inspection Visit</p>
                                    <em>When was the school last inspected?</em>
                                    <p>Number of inspection Visit in last academic year</p>
                                </div>
                                <div class="answer helper">
                                            <input type="date" name="" v-model="date_inspection" required="required" id="">
                                            <input type="number" name="" v-model="no_of_inspection"  id=""><label for="">No.</label>
                                        </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.15 Authority of Last Inspection</p>
                                    <em>Which authority conducted the last inspection visit?</em>
                                </div>
                                <div class="answer">
                                <ul>
                                                <li v-for="(authority,index) in fetch_authority"><input type="radio" v-bind:value="authority.value" v-model="authority_choice" name="authority-inspection" required="required" id="authority-inspection_fed"><label for="authority-inspection_fed">@{{authority.value}}</label></li>
                                            </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.16 Conditional Cash Transfer</p>
                                    <em>How many pupils benefitted from Conditional Cash Transfer?</em>
                                </div>
                                <div class="answer helper">
                                            <input type="number" name="" v-model="cash_transfer" required="required" id=""><label for="">No</label>
                                        </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.17 School Grants</p>
                                    <em>Has your school ever received grants in the last 2 academic sessions?</em>
                                </div>
                                <div class="answer">
                                <ul>
                                                <li><input type="radio" name="school_grants" v-model="grants_choice" value="1" required="required" id="school_grants_yes"><label for="school_grants_yes">yes</label></li>
                                                <li><input type="radio" name="school_grants" v-model="grants_choice" value="0" required="required" id="school_grants_no"><label for="school_grants_no">no</label></li>
                                            </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.18 Security Guard</p>
                                    <em>Does the school have a security guard?</em>
                                </div>
                                <div class="answer">
                                <ul>
                                                <li><input type="radio" name="security_guard" v-model="guard_choice" value="1" required="required" id="security_guard_yes"><label for="security_guard_yes">yes</label></li>
                                                <li><input type="radio" name="security_guard" v-model="guard_choice" value="0" required="required" id="security_guard_no"><label for="security_guard_no">no</label></li>
                                            </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>B.19 Ownership</p>
                                    <em>Which of the tier of Govt. owned  this school</em>
                                </div>
                                <div class="answer">
                                <ul>
                                                <li v-for="(owner,index) in fetch_ownership"><input type="radio"  v-bind:value="owner.value" v-model="ownership_choice" name="ownership" required="required" id="ownership_comm"><label for="ownership_comm">@{{owner.value}}</label></li>
                                            </ul>
                                </div>
                            </div>
                        </form>
                        <div id="button-group">
                            <ul>
                                <li><button id="previous">previous</button></li>
                                <li><button id="save" v-on:click="saveCharacteristics">save</button></li>
                                <li><button id="next" v-on:click="saveCharacteristics">next</button></li>
                            </ul>
                        </div>
                    </div>
                    <div id="tab3">
                        <p class="label">C.1 Number of Children with Birth Certificates</p>
                        <form action="">
                            <table>
                                <tr>
                                    <td class="effectGray text label-effect" rowspan="3"><label for="">How many children were enrolled with Birth certificates</label></td>
                                </tr>
                                <tr>
                                    <td class="effectGray label-effect" colspan="2"><label for="">SSS 1</label></td>
                                </tr>
                                <tr>
                                    <td class="effectGray label-effect"><label>Male<label></td>
                                    <td class="effectGray label-effect"><label>Female<label></td>
                                </tr>
                                    <tr v-for="(birth_certificate,index) in fetch_birth_certificate">
                                        <td><label for="">@{{birth_certificate.value}}</label></td>
                                        <td><input class="form-control" v-model="birth_certificate.male" type="number"></td>
                                        <td><input class="form-control" v-model="birth_certificate.female" type="number"></td>
                                    </tr>
                            </table>
                        </form>
                        <div id="button-group">
                            <ul>
                                <li><button id="previous">previous</button></li>
                                <li><button id="save"  v-on:click="saveBirthCertificates">save</button></li>
                                <li><button id="next" v-on:click="saveBirthCertificates">next</button></li>
                            </ul>
                        </div>
                    </div>
                    <div id="tab4">
                        <p class="label">C.2 New entrants</p>
                        <form action="">
                            <table>
                                <tr>
                                    <td class="effectGray"></td>
                                    <td colspan="2" class="effectGray label-effect"><label>New Entrants in SSS 1</label></td>
                                </tr>
                                <tr>
                                    <td class="effectGray text label-effect"><label>Student Age</label></td>
                                    <td class="effectGray label-effect"><label for="">Male</label></td>
                                    <td class="effectGray label-effect"><label for="">Female</label></td>
                                </tr>
                                    <tr v-for="(age,index) in fetch_age">
                                        <td class="text label-effect"><label for="">@{{age.value}}</label></td>
                                        <td><input v-model="age.male" v-on:change="entrantmale_total" name="" type="number"></td>
                                        <td><input v-model="age.female" v-on:change="entrantfemale_total" name="" type="number"></td>
                                    </tr>
                                <tr>
                                    <td class="text label-effect"><label for="">Total</label></td>
                                    <td><input disabled="disabled" v-bind:value="entrantmaletotal" type="number" name="" id="" placeholder="0"></td>
                                    <td><input disabled="disabled" v-bind:value="entrantfemaletotal" type="number" name="" id="" placeholder="0"></td>
                                </tr>
                            </table>
                        </form>
                        <div id="button-group">
                            <ul>
                                <li><button id="previous">previous</button></li>
                                <li><button id="save" v-on:click="saveEntrants" >save</button></li>
                                <li><button id="next" v-on:click="saveEntrants">next</button></li>
                            </ul>
                        </div>
                    </div>
                    <div id="tab5">
                        <p class="label">C.3 Senior Secondary Enrolment for the Current Academic Year by age</p>
                        <form action="">
                            <table>
                                <tr>
                                    <td class="effectGray"></td>
                                    <td class="effectGray label-effect" colspan="2"><label>SSS 1</label></td>
                                    <td class="effectGray label-effect" colspan="2"><label>SSS 2</label></td>
                                    <td class="effectGray label-effect" colspan="2"><label>SSS 3</label></td>
                                </tr>
                                <tr>
                                    <td class="text label-effect"><label for="">No. of stream</label></td>
                                    <td colspan="2"><input v-model="ss1_stream" type="number" name="" id="" ></td>
                                    <td colspan="2"><input v-model="ss2_stream" type="number" name="" id="" ></td>
                                    <td colspan="2"><input v-model="ss3_stream" type="number" name="" id="" ></td>
                                </tr>
                                <tr>
                                    <td class="text label-effect"><label for="">No of streams with Multigrade teaching</label></td>
                                    <td colspan="2"><input v-model="ss1_stream_with_multigrade" type="number" name="" id="" ></td>
                                    <td colspan="2"><input v-model="ss2_stream_with_multigrade" type="number" name="" id="" ></td>
                                    <td colspan="2"><input v-model="ss3_stream_with_multigrade" type="number" name="" id="" ></td>
                                </tr>
                                <tr>
                                    <td class="effectGray label-effect"><label>Age</label></td>
                                    <td class="effectGray label-effect"><label>Male</label></td>
                                    <td class="effectGray label-effect"><label>Female</label></td>
                                    <td class="effectGray label-effect"><label>Male</label></td>
                                    <td class="effectGray label-effect"><label>Female</label></td>
                                    <td class="effectGray label-effect"><label>Male</label></td>
                                    <td class="effectGray label-effect"><label>Female</label></td>
                                </tr>
                                <tr v-for="(age,index) in fetch_year_by_age">
                                    <td class="text label-effect"><label>@{{age.value}}</label></td>
                                    <td><input v-model="age.ss1_male" v-on:change="ss1entrantmale_total" type="number" name="" id="" ></td>
                                    <td><input v-model="age.ss1_female" v-on:change="ss1entrantfemale_total" type="number" name="" id="" ></td>
                                    <td><input v-model="age.ss2_male" v-on:change="ss2entrantmale_total" type="number" name="" id="" ></td>
                                    <td><input v-model="age.ss2_female" v-on:change="ss2entrantfemale_total" type="number" name="" id="" ></td>
                                    <td><input v-model="age.ss3_male" v-on:change="ss3entrantmale_total" type="number" name="" id="" ></td>
                                    <td><input v-model="age.ss3_female" v-on:change="ss3entrantfemale_total" type="number" name="" id="" ></td>
                                </tr>
                                <tr>
                                    <td class="text label-effect"><label>Total</label></td>
                                    <td><input  type="number" v-model="ss1maletotal" disabled="disabled" name="" id="" ></td>
                                    <td><input  type="number" v-model="ss1femaletotal" disabled="disabled" name="" id="" ></td>
                                    <td><input  type="number" v-model="ss2maletotal" disabled="disabled" name="" id="" ></td>
                                    <td><input  type="number" v-model="ss2femaletotal" disabled="disabled" name="" id="" ></td>
                                    <td><input  type="number" v-model="ss3maletotal" disabled="disabled" name="" id="" ></td>
                                    <td><input  type="number" v-model="ss3femaletotal" disabled="disabled" name="" id="" ></td>
                                </tr>
                                <tr>
                                    <td class="text label-effect"><label>Repeaters</label></td>
                                    <td><input v-model="ss1_repeaters_male" type="number" name="" id="" ></td>
                                    <td><input v-model="ss1_repeaters_female" type="number" name="" id="" ></td>
                                    <td><input v-model="ss2_repeaters_male" type="number" name="" id="" ></td>
                                    <td><input v-model="ss2_repeaters_female" type="number" name="" id="" ></td>
                                    <td><input v-model="ss3_repeaters_male" type="number" name="" id="" ></td>
                                    <td><input v-model="ss3_repeaters_female" type="number" name="" id="" ></td>
                                </tr>
                                <tr>
                                    <td class="text label-effect"><label>No. Completed SSS 3 for previous year</label></td>
                                    <td><input type="number" name="" disabled="disabled" id="" ></td>
                                    <td><input type="number" name="" disabled="disabled" id="" ></td>
                                    <td><input type="number" name="" disabled="disabled" id="" ></td>
                                    <td><input type="number" name="" disabled="disabled" id="" ></td>
                                    <td><input v-model="prevyear_sss3_male" type="number" name="" id="" ></td>
                                    <td><input v-model="prevyear_sss3_female"type="number" name="" id="" ></td>
                                </tr>
                            </table>
                        </form>
                        <div id="button-group">
                            <ul>
                                <li><button id="previous">previous</button></li>
                                <li><button id="save" v-on:click="saveEnrollmentByAge" >save</button></li>
                                <li><button id="next" v-on:click="saveEnrollmentByAge">next</button></li>
                            </ul>
                        </div>
                    </div>
                    <div id="tab6">
                        <p class="label">C.4 Students with Special Needs for the Current Academic Year</p>
                        <form action="">
                            <table>
                                <tr>
                                    <td class="effectGray text label-effect" rowspan="2"><label>Challenge that impacts the ability to learn</label></td>
                                    <td class="effectGray label-effect" colspan="2"><label>SSS 1</label></td>
                                    <td class="effectGray label-effect" colspan="2"><label>SSS 2</label></td>
                                    <td class="effectGray label-effect" colspan="2"><label>SSS 3</label></td>
                                </tr>
                                <tr>
                                    <td class="effectGray label-effect"><label>Male</label></td>
                                    <td class="effectGray label-effect"><label>Female</label></td>
                                    <td class="effectGray label-effect"><label>Male</label></td>
                                    <td class="effectGray label-effect"><label>Female</label></td>
                                    <td class="effectGray label-effect"><label>Male</label></td>
                                    <td class="effectGray label-effect"><label>Female</label></td>
                                </tr>
                                <tr v-for="(need,index) in fetch_special_needs">
                                    <td class="text label-effect"><label>@{{need.value}}</label></td>
                                    <td><input type="number" v-model="need.ss1_male" name="" id="" ></td>
                                    <td><input type="number" v-model="need.ss1_female" name="" id="" ></td>
                                    <td><input type="number" v-model="need.ss2_male" name="" id="" ></td>
                                    <td><input type="number" v-model="need.ss2_female" name="" id="" ></td>
                                    <td><input type="number" v-model="need.ss3_male" name="" id="" ></td>
                                    <td><input type="number" v-model="need.ss3_female" name="" id="" ></td>
                                </tr>
                            </table>
                        </form>
                        <div id="button-group">
                            <ul>
                                <li><button id="previous">previous</button></li>
                                <li><button id="save" v-on:click="saveSpecialNeed" >save</button></li>
                                <li><button id="next" v-on:click="saveSpecialNeed">next</button></li>
                            </ul>
                        </div>
                    </div>
                    <div id="tab7">
                        <p class="label">C.5 SSCE examination for the previous Academic Year</p>
                        <form action="">
                            <table>
                                <tr>
                                    <td class="effectGray label-effect"><label for=""></label></td>
                                    <td class="effectGray label-effect"><label for="">Male</label></td>
                                    <td class="effectGray label-effect"><label for="">Female</label></td>
                                    <td class="effectGray label-effect"><label for="">Total</label></td>
                                </tr>
                                <tr>
                                    <td class="text label-effect"><label for="">How many students were registered for SSCE?</label></td>
                                    <td><input type="number" v-model="registered_male" v-on:change="reg_ssce" name="" id=""></td>
                                    <td><input type="number" v-model="registered_female" v-on:change="reg_ssce" name="" id=""></td>
                                    <td><input type="number" disabled v-model="registered_total" required="required" name="" id=""></td>
                                </tr>
                                <tr>
                                    <td class="text label-effect"><label for="">How many students took part in the SSCE?</label></td>
                                    <td><input type="number" v-model="took_part_male" v-on:change="took_ssce" name="" id=""></td>
                                    <td><input type="number" v-model="took_part_female" v-on:change="took_ssce" name="" id=""></td>
                                    <td><input type="number" disabled v-model="took_part_total" name="" id=""></td>
                                </tr>
                                <tr>
                                    <td class="text label-effect"><label for="">How many students passed SSCE?</label></td>
                                    <td><input type="number" v-model="passed_male" v-on:change="passed_ssce" name="" id=""></td>
                                    <td><input type="number" v-model="passed_female" v-on:change="passed_ssce" name="" id=""></td>
                                    <td><input type="number" disabled v-model="passed_total" name="" id=""></td>
                                </tr>
                            </table>
                        </form>
                        <div id="button-group">
                            <ul>
                                <li><button id="previous">previous</button></li>
                                <li><button id="save" v-on:click="saveExamination">save</button></li>
                                <li><button id="next" v-on:click="saveExamination">next</button></li>
                            </ul>
                        </div>
                    </div>
                    <div id="tab8" >
                        <p class="label">D. Staff</p>
                        <form action="">
                            <div class="row">
                                <div class="question">
                                    <p>D.1 How many non-teaching staff are working at the school?</p>
                                </div>
                                <div class="answer helper">
                                    <input type="number" name="" id=""><label for="">male</label>
                                    <input type="number" name="" id=""><label for="">female</label>
                                    <input type="number" name="" id=""><label for="">Total</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>D.2 How many teachers are working at the school regardless of whether they are currently present or on course or absent</p>
                                </div>
                                <div class="answer helper">
                                    <input type="number" name="" id=""><label for="">male</label>
                                    <input type="number" name="" id=""><label for="">female</label>
                                    <input type="number" name="" id="" disabled><label for="">Total</label>
                                </div>
                            </div>
                        </form>
                        <div id="button-group">
                            <ul>
                                <li><button id="previous">previous</button></li>
                                <li><button id="save">save</button></li>
                                <li><button id="next">next</button></li>
                            </ul>
                        </div>
                    </div>
                    <div id="tab9" >
                        <p class="label">D.3 Information on all staff during the school year</p>
                        <div class="row table-responsive">
                            <form action="">
                                <table class="table table-striped">
                                    <thead>
                                        <th>No.</th>
                                        <th>Staff File No</th>
                                        <th>Name of staff</th>
                                        <th>Gender</th>
                                        <th>Type of staff</th>
                                        <th>Source of salary</th>
                                        <th>Year of Birth</th>
                                        <th>Year of first appointment</th>
                                        <th>Year of present appointment</th>
                                        <th>Year of posting to the school</th>
                                        <th>Grade level / Step</th>
                                        <th>Present</th>
                                        <th>Academic Qualification</th>
                                        <th>Teaching Qualification</th>
                                        <th>Area of specialization</th>
                                        <th>Main subject taught</th>
                                        <th>Teaching type</th>
                                        <th>Tick box if teacher also teaches senior secondary classes in this school</th>
                                        <th>Tick box if teacher attended training workshop / seminar in last 12 months</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>P4567</td>
                                            <td>Fred Abdul</td>
                                            <td>M</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1998</td>
                                            <td>1996</td>
                                            <td>1998</td>
                                            <td>1996</td>
                                            <td>7 / 2</td>
                                            <td>1</td>
                                            <td>2</td>
                                            <td>3</td>
                                            <td>1</td>
                                            <td>2</td>
                                            <td>1</td>
                                            <td><input type="checkbox" name="" id=""></td>
                                            <td><input type="checkbox" name="" id=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <div class="addBtn">
                            <ul>
                                <li><button class="btn-success" id="addBtn"></button></li>
                            </ul>
                        </div>
                        <div id="button-group">
                            <ul>
                                <li><button id="previous">previous</button></li>
                                <li><button id="save">save</button></li>
                                <li><button id="next">next</button></li>
                            </ul>
                        </div>
                    </div>
                    <div id="tab10">
                        <p class="label">E. Classrooms</p>
                        <form action="">
                            <div class="row">
                                <div class="question">
                                    <p>E.1 How many classrooms are there in the school?</p>
                                </div>
                                <div class="answer helper">
                                    <input type="number" name="" id=""><label for="">Number</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="question">
                                    <p>E.2 Are any classes held outside (because classrooms are unusable or insufficient)?</p>
                                </div>
                                <div class="answer">
                                    <ul>
                                        <li><input type="radio" name="sufficient" id="sufficient_yes"><label for="sufficient_yes">Yes</label></li>
                                        <li><input type="radio" name="sufficient" id="sufficient_no"><label for="sufficient_no">No</label></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                        <div id="button-group">
                            <ul>
                                <li><button id="previous">previous</button></li>
                                <li><button id="save">save</button></li>
                                <li><button id="next">next</button></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Section E3  -->
                    <div id="tab11">
                        <p class="label">E.3 Information on all classrooms</p>
                        <div class="row table-responsive">
                            <form action="">
                                <table class="table table-striped">
                                    <thead>
                                        <th>No.</th>
                                        <th>Year of construction</th>
                                        <th>Present condition</th>
                                        <th>Length in metres</th>
                                        <th>Width in metres</th>
                                        <th>Floor material</th>
                                        <th>Walls material</th>
                                        <th>Roof material</th>
                                        <th>Seating</th>
                                        <th>Good blackboard</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>4567</td>
                                            <td>2</td>
                                            <td>7</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>8</td>
                                            <td>6</td>
                                            <td>9</td>
                                            <td>1</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>4567</td>
                                            <td>2</td>
                                            <td>7</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>8</td>
                                            <td>6</td>
                                            <td>9</td>
                                            <td>1</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>4567</td>
                                            <td>2</td>
                                            <td>7</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>8</td>
                                            <td>6</td>
                                            <td>9</td>
                                            <td>1</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <div class="addBtn">
                            <ul>
                                <li><button class="btn-success" id="addClassBtn"></button></li>
                            </ul>
                        </div>
                        <div id="button-group">
                            <ul>
                                <li><button id="previous">previous</button></li>
                                <li><button id="save">save</button></li>
                                <li><button id="next">next</button></li>
                            </ul>
                        </div>
                    </div>
                    <div id="tab12">
                            <p class="label">E.4 Number of rooms other than classrooms are there in the school by type of room</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>Staff Room</p>
                                    </div>
                                    <div class="answer helper">
                                        <input type="number" name="" id=""><label for="">Number</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Office</p>
                                    </div>
                                    <div class="answer helper">
                                        <input type="number" name="" id=""><label for="">Number</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Library</p>
                                    </div>
                                    <div class="answer helper">
                                        <input type="number" name="" id=""><label for="">Number</label>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save">save</button></li>
                                    <li><button id="next">next</button></li>
                                </ul>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/parsley.min.js')}}"></script>
    <script src="{{asset('assets/js/functions.js')}}"></script>
    <script src="{{asset('assets/js/nemis_functionality_sss.js')}}"></script>
    <script src="{{asset('assets/js/staff.js')}}"></script>
<script src="{{asset('assets/js/users.js')}}"></script>
</body>
</html>
