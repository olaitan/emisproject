<div class="logo">                
    <span id="collapse"></span>
    <a href="/"><p>NEMIS</p></a>
</div>            
<div class="links">
    <ul>
        <li><a href="/"><span class="overview"></span><p>Overview</p></a></li>
        <li><a href="/search"><span class="search"></span><p>Search</p></a></li>
        <li><a href="/new"><span class="addSchool"></span><p>Add a school</p></a></li>
        <li><a href="/" id="moreLinks" class="moreLinks"><span class="settings"></span><p>Settings</p></a>
            <ul id="extraLinks">
                <li><a href="/metadatas">Configure Metadatas</a></li>
                <li><a href="/">Color Code</a></li>
            </ul>
        </li>
    </ul>
</div> 
<div class="users">
    <div class="user-avatar">
        <img src="" alt="IA" srcset="" >
    </div>
    <h2 class="name"><a href="" id="showDetails">Israel Akpan</a></h2>
    <p class="role">Product Designer</p>

    <div class="users-links" id="users-links">
        <ul>
            <li><a href="/userID">View Profile</a></li>
            <li><a href="/configure">Configure</a></li>
            <li><a href="/edituser">Accounts</a></li>
            <li><a href="/login">Logout</a></li>
        </ul>
    </div>
</div>