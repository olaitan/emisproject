<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin | Dashboard</title>
    <!-- StyleSheets -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/customized.css">
    <link rel="stylesheet" href="assets/css/users.css">
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
</head>
<body>
    <div id="app">
        <!-- Main Container -->
        <div id="main-container">
            <div id="left-tab">
                <div class="logo">                
                    <span id="collapse"></span>
                    <a href="/"><p>NEMIS</p></a>
                </div>            
                <div class="links">
                    <ul>
                        <li><a href="/"><span class="overview"></span><p>Overview</p></a></li>
                        <li><a href="/search"><span class="search"></span><p>Search</p></a></li>
                        <li><a href="/new"><span class="addSchool"></span><p>Add a school</p></a></li>
                        <li><a href="/" id="moreLinks" class="moreLinks"><span class="settings"></span><p>Settings</p></a>
                            <ul id="extraLinks">
                                <li><a href="/metadatas">Configure Metadatas</a></li>
                                <li><a href="/">Color Code</a></li>
                            </ul>
                        </li>
                    </ul>
                </div> 
                <div class="users">
                    <div class="user-avatar">
                        <img src="./assets/images/avatar.png" alt="IA" srcset="" >
                    </div>
                    <h2 class="name"><a href="" id="showDetails">Israel Akpan</a></h2>
                    <p class="role">Product Designer</p>

                    <div class="users-links" id="users-links">
                        <ul>
                            <li><a href="/userID">View Profile</a></li>
                            <li><a href="/configure">Configure</a></li>
                            <li><a href="/edituser">Accounts</a></li>
                            <li><a href="/login">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="right-tab">
            </div>
        </div>
    </div>

<script src="assets/js/jquery-1.11.2.min.js"></script> 
<script src="assets/js/functions.js"></script>
<script src="assets/js/users.js"></script>

</body>
</html>