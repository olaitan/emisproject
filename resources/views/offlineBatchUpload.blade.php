<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin | Dashboard</title>
    <!-- StyleSheets -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/assets/datatables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/users.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/forms.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/customized.css')}}">
    <!-- scripting -->
    <script src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
</head>
<body>
<div id="app">
    <!-- Main Container -->
    <div id="main-container">
        <div id="left-tab" class="theme">
            <div class="logo">
                <span id="collapse"></span>
                <a href="/"><p>NEMIS</p></a>
            </div>           
            <div class="links">
                <ul>
                    <li><a href="/"><span class="overview"></span><p>Overview</p></a></li>
                    <li><a href="/" id="moreReportLinks"><span class="overview"></span><p>Reports</p></a>
                            <ul id="extraReportLinks">
                            <li v-if="auth_user.role_id==1"><a href="/school_characteristics_report">School Characteristics</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/enrolment_report">Enrolment</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/repeaters_report">Repeaters</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_facility_report">School_Facility</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_streams_report">School_Streams</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_teachers_report">School_Teachers</a></li>                                
                            </ul>
                        </li>
                    <li><a href="/search"><span class="search"></span><p>Search</p></a></li>
                    <li><a href="/new"><span class="addSchool"></span><p>Add a school</p></a></li>
                    <li ><a href="/" id="moreLinks"><span class="settings"></span><p>Settings</p></a>
                        <ul id="extraLinks">
                            <li v-if="auth_user.role_id==1"><a href="/metadatas">Configure Metadatas</a></li>
                            <li v-if="auth_user.role_id==1"><a href="/lgaMetadatas">Configure LGA</a></li>
                            <li v-if="auth_user.role_id==1"><a href="/userlogs">User Activity Logs</a></li>
                        </ul>
                    </li>
                    <li><a href="/" id="moreOfflineLinks"><span class="overview"></span><p>Offline(Alpha)</p></a>
                            <ul id="extraOfflineLinks">
                                <li ><a href="/offline_data_entry">Form Entry</a></li>
                                <li ><a href="/offline_batch_upload">Batch Upload</a></li>  
                            </ul>
                        </li>
                </ul>
            </div>
            <div class="users">
                <div class="user-avatar">
                    <img v-bind:src="auth_user.profile_pic" alt="IA" srcset="" >
                </div>
                <h2 class="name"><a href="" id="showDetails">@{{auth_user.name}}</a></h2>
                <p class="role">@{{auth_user.role}}</p>

                <div class="users-links" id="users-links">
                    <ul>
                        <li><a href="/userID">View Profile</a></li>
                        <li v-if="auth_user.role_id!=4"><a href="/configure">Configure</a></li>
                        <li v-if="auth_user.role_id!=4"><a href="/edituser">Accounts</a></li>
                        <li v-on:click="logout"><a href="/login">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="right-tab" class="contents">                   
            <h1>Upload form json files to the server(Development in progress)</h1>
        </div>
    </div>
</div>
    

<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/nemis_search.js')}}"></script>
<script src="{{asset('assets/assets/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/functions.js')}}"></script>
<script src="{{asset('assets/js/users.js')}}"></script>
<script>
    var table = $('#school_search_results').DataTable( {
        "dataSrc":"",
        "columns": [
            { "data": "data",
                "render": function(data,type,row){
                return '<a href="'+row.link+'" target="_blank">'+data.schoolcode+'</a>'
                } },
            { "data": "data.name" },
            { "data": "data.lga" },
            { "data": "data.town" },
            { "data": "data.level" }
            
        ]
    } );
</script>
</body>
</html>