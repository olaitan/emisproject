<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin | Dashboard</title>
    <!-- StyleSheets -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/customized.css">
    <link rel="stylesheet" href="assets/css/metadatas.css">
    <link rel="stylesheet" href="assets/css/users.css">
    <link rel="stylesheet" href="{{asset('assets/assets/datatables.min.css')}}">
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
</head>
<body>
    <!-- Main Container -->
    <div id="app">
        <div id="main-container">
            <div id="left-tab" class="theme">
                <div class="logo">                
                    <span id="collapse"></span>
                    <a href="/"><p>NEMIS</p></a>
                </div>            
                <div class="links">
                    <ul>
                        <li><a href="/"><span class="overview"></span><p>Overview</p></a></li>
                        <li><a href="/" id="moreReportLinks"><span class="reports"></span><p>Reports</p></a>
                            <ul id="extraReportLinks">
                            <li v-if="auth_user.role_id==1"><a href="/school_characteristics_report">School Characteristics</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/enrolment_report">Enrolment</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/repeaters_report">Repeaters</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_facility_report">School_Facility</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_streams_report">School_Streams</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_teachers_report">School_Teachers</a></li>                                 
                            </ul>
                        </li>
                        <li><a href="/search"><span class="search"></span><p>Search</p></a></li>
                        <li><a href="/new"><span class="addSchool"></span><p>Add a school</p></a></li>
                        <li v-if="auth_user.role_id==1"><a href="/" id="moreLinks" class="moreLinks"><span class="settings"></span><p>Settings</p></a>
                            <ul id="extraLinks">
                            <li><a href="/metadatas">Configure Metadatas</a></li>
                                <!-- <li><a href="/stateMetadatas">Configure States</a></li> -->
                                <li><a href="/lgaMetadatas">Configure LGA</a></li>
                                <li><a href="/userlogs">User Logs</a></li>
                            </ul>
                        </li>
                    </ul>
                </div> 
                <div class="users">
                    <div class="user-avatar">
                        <img v-bind:src="auth_user.profile_pic" alt="IA" srcset="" >
                    </div>
                    <h2 class="name"><a href="" id="showDetails">@{{auth_user.name}}</a></h2>
                    <p class="role">@{{auth_user.role}}</p>

                    <div class="users-links" id="users-links">
                        <ul>
                            <li><a href="/userID">View Profile</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/configure">Configure</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/edituser">Accounts</a></li>
                            <li v-on:click="logout"><a href="/login" >Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div id="right-tab">                
                <div id="alertUsers">
                    <p id="success">Hello World!</p>
                </div>
                <h3 class="label theme">User log activities</h3>
                <div class="search-result table-responsive ">
                    <table class="table table-hover" id="userlogs">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">User name</th>
                                <th scope="col">School</th>
                                <th scope="col">Census year</th>
                                <th scope="col">Category</th>
                                <th scope="col">User Activity</th>
                                <th scope="col">Notes</th>
                            </tr>
                        </thead>
                    </table>
                </div>

                
            </div>
        </div>
    </div>

<script src="assets/js/jquery-1.11.2.min.js"></script>
<script src="{{asset('assets/assets/datatables.min.js')}}"></script> 
<script src="{{asset('assets/js/userlogs.js')}}"></script>
<script src="assets/js/functions.js"></script>
<script src="{{asset('assets/js/users.js')}}"></script>
<script>
    var table = $('#userlogs').DataTable( {
        processing: true,
        "columns": [
            { "data": "name"},
            { "data": "school" },
            { "data": "census" },
            { "data": "category" },
            { "data": "activity" },
            { "data": "notes" }
            
        ]
    } );
    table.ajax.url("/api/userlogs").load();
</script>
</body>
</html>