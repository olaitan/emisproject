<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NEMIS - Log in</title>    
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/customized.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <!-- scripting -->
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
</head>
<body>
<div id="app">
    <div id="loginApp">
        <div class="header">
            <h2><a href="/">NEMIS</a></h2>
            <div class="nav">
                <ul>
                    <li><a href="/api">Developers</a></li>
                    <li><a href="/docs">Documentation</a></li>
                    <!-- <li><a href="/examples">Examples</a></li> -->
                </ul>
            </div>
        </div>
        <div class="mainWrap">
            <div class="main">
                <div id="searchUser" class="searchUser active">
                    <h3 class="caption">Find my Account</h3>                    
                    <p id="errorEmail" class="error">Email not registered, contact Administrator</p>
                    <div class="wrap">
                        <label for="email">Enter your Email Address</label>
                        <input type="email" name="" v-model="email" required="required" id="email" placeholder="someone@example.com">
                    </div>
                    <div class="buttons">
                        <button id="search" v-on:click="searchEmail" type="submit" >Search</button>
                        <span id="spinner"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></span>
                    </div>
                </div>
                <div id="createPwd" class="createPwd">
                    <h3 class="caption">Welcome,</h3>
                    <p class="caption">Create a new password</p>
                    <div class="wrap">
                        <label for="email">New Password</label>
                        <input type="password" v-model="password" name="" id="newpassword" required="required" placeholder="Password">
                        <label for="email">Confirm New Password</label>
                        <input type="password" v-model="c_password" name="" id="confirmpassword" required="required" placeholder="Confirm Password">
                    </div>
                    <div class="buttons">
                        <button id="newlogin" v-on:click="createPassword" type="submit">Log in</button>
                        <span id="spinner"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></span>
                    </div>
                </div> 
                <div id="loginDetails" class="loginDetails">
                    
                    <h3 class="caption">Welcome,</h3>
                    <div class="avatar"><img src="{{asset('assets/images/avatar.png')}}" alt="" srcset=""></div>
                    
                    <p class="email">@{{email}}</p>
                    <div class="wrap">
                        <label for="email">Enter Password</label>
                        <input type="password" v-model="password" name="" id="password" placeholder="Password">
                    </div>
                    <div class="buttons">
                        <button id="login" type="submit" v-on:click="login">Log in</button></i>
                        <span id="spinner"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <p>Copyright &copy 2018</p>
        </div>
    </div>
</div>
        
    
    
    <script src="{{asset('assets/js/login.js')}}"></script>
    <script src="{{asset('assets/js/authenticate.js')}}"></script>
</body>
</html>
