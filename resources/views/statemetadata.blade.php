<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin | Dashboard</title>
    <!-- StyleSheets -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/customized.css">
    <link rel="stylesheet" href="assets/css/users.css">
    <link rel="stylesheet" href="assets/css/metadatas.css">
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
</head>
<body>
    <div id="app">
        <!-- Main Container -->
        <div id="main-container">
            <div id="alertUsers">
                <p id="success"></p>
            </div>
            <div id="left-tab">
                <div class="logo">                
                    <span id="collapse"></span>
                    <a href="/"><p>NEMIS</p></a>
                </div>           
                <div class="links">
                    <ul>
                        <li><a href="/"><span class="overview"></span><p>Overview</p></a></li>
                        <li><a href="/" id="moreReportLinks"><span class="reports"></span><p>Reports</p></a>
                            <ul id="extraReportLinks">
                            <li v-if="auth_user.role_id==1"><a href="/school_characteristics_report">School Characteristics</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/enrolment_report">Enrolment</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/repeaters_report">Repeaters</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_facility_report">School_Facility</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_streams_report">School_Streams</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_teachers_report">School_Teachers</a></li>                                 
                            </ul>
                        </li>
                        <li><a href="/search"><span class="search"></span><p>Search</p></a></li>
                        <li><a href="/new"><span class="addSchool"></span><p>Add a school</p></a></li>
                        <li><a href="/" id="moreLinks" class="moreLinks"><span class="settings"></span><p>Settings</p></a>
                            <ul id="extraLinks">
                                <li><a href="/metadatas">Configure Metadatas</a></li>
                                <li><a href="/stateMetadatas">Configure States</a></li>
                                <li><a href="/lgaMetadatas">Configure LGA</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="users">
                    <div class="user-avatar">
                        <img src="./assets/images/avatar.png" alt="IA" srcset="" >
                    </div>
                    <h2 class="name"><a href="" id="showDetails">Israel Akpan</a></h2>
                    <p class="role">Product Designer</p>

                    <div class="users-links" id="users-links">
                        <ul>
                            <li><a href="/userID">View Profile</a></li>
                            <li><a href="/config">Configure</a></li>
                            <li><a href="/edituser">Accounts</a></li>
                            <li><a href="/login">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div id="right-tab">
            <!-- Create a page to edit and add states,capital and statecode -->
                <h3 class="label">Edit State Characteristics</h3>
                <div class="table-responsive mainCover">
                    <table class="table table-hover">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">State Code</th>
                                <th scope="col">State</th>
                                <th scope="col">Capital</th>
                            </tr>
                        </thead>
                        <tbody id="addEffect">
                            <tr>
                                <td><input type="text" name="" placeholder="State Code" id=""></td>
                                <td><input type="text" name="" placeholder="State" id=""></td>
                                <td><input type="text" name="" placeholder="Capital" id=""></td>
                            </tr>                                                          
                        </tbody>
                    </table>
                    <div class="newMetadata">
                        <button id="addNewState">Add New State</button>
                        <button id="saveChanges">Save Changes</button>
                    </div>
                </div>          
            </div>
        </div>
    </div>

    <script src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script> 
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/functions.js')}}"></script>
    <script src="{{asset('assets/js/users.js')}}"></script>
    <script>
        let addButton = document.getElementById('addNewState');
        addButton.addEventListener('click', ()=>{
            document.getElementById('alertUsers').classList.add('active');
            document.getElementById('success').innerHTML = "Saved";
            clearSuccess();
        })
        let saveButton = document.getElementById('saveChanges');
        saveButton.addEventListener('click', ()=>{
            document.getElementById('alertUsers').classList.add('active');
            document.getElementById('success').innerHTML = "Changes Saved";
            clearSuccess();
        })
        function clearSuccess(){
            setTimeout(() => {
                document.getElementById('alertUsers').classList.remove('active')
                document.getElementById('success').innerHTML = "";
            }, 1500);
        }    
</script>
</body>
</html>