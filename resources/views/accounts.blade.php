<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin | Dashboard</title>
    <!-- StyleSheets -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/customized.css">
    <link rel="stylesheet" href="assets/css/users.css">
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
</head>
<body>
    <div id="app">
        <!-- Main Container -->
        <div id="main-container">
            <div id="alertUsers">
                <p id="info"></p>
            </div>
            <div id="left-tab" class="theme">
                <div class="logo">                
                    <span id="collapse"></span>
                    <a href="/"><p>NEMIS</p></a>
                </div>             
                <div class="links">
                    <ul>
                        <li><a href="/"><span class="overview"></span><p>Overview</p></a></li>
                        <li><a href="/" id="moreReportLinks"><span class="reports"></span><p>Reports</p></a>
                            <ul id="extraReportLinks">
                            <li v-if="auth_user.role_id==1"><a href="/school_characteristics_report">School Characteristics</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/enrolment_report">Enrolment</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/repeaters_report">Repeaters</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_facility_report">School_Facility</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_streams_report">School_Streams</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_teachers_report">School_Teachers</a></li>                                 
                            </ul>
                        </li>
                        <li><a href="/search"><span class="search"></span><p>Search</p></a></li>
                        <li><a href="/new"><span class="addSchool"></span><p>Add a school</p></a></li>
                        <li v-if="auth_user.role_id==1"><a href="/" id="moreLinks" ><span class="settings"></span><p>Settings</p></a>
                            <ul id="extraLinks">
                                <li><a href="/metadatas">Configure Metadatas</a></li>                                
                                <li><a href="/lgaMetadatas">Configure LGA</a></li>
                                <li><a href="/userlogs">User Logs</a></li>
                            </ul>
                        </li>
                    </ul>
                </div> 
                <div class="users">
                    <div class="user-avatar">
                        <img v-bind:src="auth_user.profile_pic" alt="IA" srcset="" >
                    </div>
                    <h2 class="name"><a href="" id="showDetails">@{{auth_user.name}}</a></h2>
                    <p class="role">@{{auth_user.role}}</p>

                    <div class="users-links" id="users-links">
                        <ul>
                            <li><a href="/userID">View Profile</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/configure">Configure</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/edituser">Accounts</a></li>
                            <li v-on:click="logout"><a href="/login">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div id="right-tab">
                <div class="accountsRow">
                    <nav>
                        <ul class="nav nav-tabs" id="accountsTab" role="tablist">
                            <li class="nav-item"><a class="nav-link active" id="createUser" data-toggle="tab" href="#createUser" role="tab" aria-controls="createUser" aria-selected="false">Create  User</a></li>
                            <li class="nav-item"><a class="nav-link" id="deleteUser" data-toggle="tab" href="#deleteUser" role="tab" aria-controls="deleteUser" aria-selected="false">Delete User</a></li>
                            <li class="nav-item"><a class="nav-link" id="revokeUser" data-toggle="tab" href="#revokeUser" role="tab" aria-controls="revokeUser" aria-selected="false">Revoke User Access</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="tab-content">
                    <div class="tabPane active" id="createUser" role="tabpanel">
                        <div>
                            <h3 class="header theme">Create a User</h3>
                            <div class="line"></div>
                            <div>
                                <label for="name">Full Name</label>
                                <input type="text" v-model="user_fullname" name="" id="name" placeholder="Full Name">
                            </div>
                            <div>
                                <label for="email">Enter User Email Address</label>
                                <input type="email" v-model="user_email" name="" id="" placeholder="someone@example.com">
                            </div>
                            <div>
                                <label for="userRole">Choose User Role</label>
                                <select name="roles" v-model="user_role" id="userRole">
                                    <option value="" disabled></option>
                                    <option v-for="(source,index) in fetch_roles" v-if="auth_user.role_id==1 || source.Id==4" v-bind:value="source.Id">@{{source.Role}}</option>
                                </select>
                            </div>
                            <div v-if="auth_user.role_id==1">
                                <label for="userRole">State</label>
                                <select name="roles" v-model="user_state" id="userRole">
                                    <option v-for="(source,index) in fetch_states" v-bind:value="source.data.id">@{{source.data.name}}</option>
                                </select>
                            </div>
                            <div class="buttons">
                                <button id="addRoles" class="theme" v-on:click="addUser">Add New User</button>
                            </div>
                        </div>
                    </div>
                    <div class="tabPane" id="deleteUser" role="tabpanel">
                        <div>
                            <h3 class="header theme">Delete a User</h3>
                            <div class="line"></div>
                            <div>
                                <label for="searchUser">Enter User Email</label>
                                <input type="email" v-model="delete_user_email" name="" id="searchUser" placeholder="email address">
                            </div>
                            <div class="buttons">
                                <button id="delete" v-on:click="removeUser">Delete</button>
                            </div>
                        </div>
                    </div>
                    <div v-if="auth_user.role_id==1" class="tabPane" id="revokeUser" role="tabpanel">
                        <div>
                            <h3 class="header theme">Revoke User Access</h3>
                            <div class="line"></div>
                            <div>
                                <label for="userEmail">Find a user</label>
                                <input type="email" v-model="find_user_email" name="" id="" placeholder="email address">
                            </div>                        
                            <div>
                                <label for="userRole">Change User Role</label>
                                <select name="roles" v-model="change_user_role" id="userRole">
                                    <option value="" disabled></option>
                                    <option v-for="(source,index) in fetch_roles" v-bind:value="source.Id">@{{source.Role}}</option>
                                </select>
                            </div>
                            <div class="buttons">
                                <button id="revoke" v-on:click="changeUserRole" >Revoke Access</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

<script src="assets/js/jquery-1.11.2.min.js"></script> 
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/usersconfig.js')}}"></script>
<script src="{{asset('assets/js/accounts.js')}}"></script>
<script src="assets/js/functions.js"></script>
<script src="{{asset('assets/js/users.js')}}"></script>
</body>
</html>