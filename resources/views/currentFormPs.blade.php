<!-- THIS IS PRIVATE SCHOOL FORM -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin | Dashboard</title>
    <!-- StyleSheets -->
    <link rel="shortcut icon" href="{{asset('assets/icons/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/parsley.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/customized.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/forms.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/users.css')}}">
    <!-- scripting -->
    <script src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('assets/js/vue.min.js')}}"></script>
    <script src="{{asset('assets/js/vue-cookies.js')}}"></script>
</head>

<body>
    <div id="app">
        <!-- Main Container -->
        <div id="main-container">
            <!-- Start Dialog Box -->
            <div id="coverLayer"></div>

            <!-- End Dialog Boc -->
            <div id="left-tab" class="theme">
                <div class="logo">
                    <span id="collapse"></span>
                    <a href="/">
                        <p>NEMIS</p>
                    </a>
                </div>
                <div class="links">
                    <ul>
                        <li><a href="/"><span class="overview"></span>
                                <p>Overview</p>
                            </a></li>
                        <li><a href="/" id="moreReportLinks"><span class="reports"></span><p>Reports</p></a>
                            <ul id="extraReportLinks">
                            <li v-if="auth_user.role_id==1"><a href="/school_characteristics_report">School Characteristics</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/enrolment_report">Enrolment</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/repeaters_report">Repeaters</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_facility_report">School_Facility</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_streams_report">School_Streams</a></li>  
                                <li v-if="auth_user.role_id==1"><a href="/school_teachers_report">School_Teachers</a></li>                                 
                            </ul>
                        </li>
                        <li><a href="/search"><span class="search"></span>
                                <p>Search</p>
                            </a></li>
                        <li><a href="/new"><span class="addSchool"></span>
                                <p>Add a school</p>
                            </a></li>
                        <li ><a href="/" id="moreLinks" class="moreLinks"><span class="settings"></span>
                                <p>Settings</p>
                            </a>
                            <ul id="extraLinks">
                                <li v-if="auth_user.role_id==1"><a href="/metadatas">Configure Metadatas</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/lgaMetadatas">Configure LGA</a></li>
                                <li v-if="auth_user.role_id==1"><a href="/userlogs">User Logs</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="users">
                    <div class="user-avatar">
                        <img v-bind:src="auth_user.profile_pic" alt="IA" srcset="">
                    </div>
                    <h2 class="name"><a href="" id="showDetails">@{{auth_user.name}}</a></h2>
                    <p class="role">@{{auth_user.role}}</p>

                    <div class="users-links" id="users-links">
                        <ul>
                            <li><a href="/userID">View Profile</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/configure">Configure</a></li>
                            <li v-if="auth_user.role_id!=4"><a href="/edituser">Accounts</a></li>
                            <li v-on:click="logout"><a href="/login">Logout</a></li>
                        </ul>
                    </div>

                </div>
            </div>
            <!-- Right Tab -->
            <div id="right-tab">
                <div id="alert">
                    <p v-text="responseMsg"></p>
                </div>
                <div id="loader" class="theme"></div>
                <!-- Breadcrumb Div -->
                <div>
                    <ul class="breadcrumb alert-success">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a v-bind:href="schoolstate_href">@{{schoolstate}}</a></li>
                        <li class="breadcrumb-item"><a v-bind:href="lga_href">@{{schoollga}}</a></li>
                        <li class="breadcrumb-item"><a v-bind:href="school_type_href">Private </a></li>
                        <li class="breadcrumb-item active">@{{schoolname}}</li>
                    </ul>
                    <a v-bind:href="datapreviewurl" target="_blank" id=''>Preview data</a>
                </div>


                <!-- Forms and Navigation -->
                <div>
                    <!-- Div containing the various tabs -->
                    <div class="tab-contents" id="content">
                        <div id="tab1" class="show-current-tab">
                            <p class="label">Choose Census Year</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>Census Year</p>
                                    </div>
                                    <div class="answer">
                                        <select v-model="registered_censusyear" required class="theme" name="" id="">
                                            <option value="" disabled>Choose Year</option>
                                            <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="save" v-on:click="saveCensusYear">save</button></li>
                                    <li><button id="next" v-on:click="saveCensusYear" class="theme">next</button></li>
                                </ul>
                            </div>

                        </div>
                        <div id="tab2">
                            <p class="label">A. School Identification</p>
                            <form action="">
                            <p v-if="school_identification_errors.length" style="color:red">
                                <b>Please correct the following error(s):</b>
                                <ul style="list-style-type: circle !important">
                                    <li style="color:red" v-for="error in school_identification_errors">-> @{{ error }}</li>
                                </ul>
                            </p>
                                <div class="row">
                                    <div class="question">
                                        <p>PRE-PRY & PRY School Code</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" v-model="primary_schoolcode" name="schoolcode" id="schoolcode" required="required" placeholder="School Code">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>JSS School Code</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" v-model="jss_schoolcode" name="schoolcode" id="schoolcode" required="required" placeholder="School Code">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>SSS School Code</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" v-model="sss_schoolcode" name="schoolcode" id="schoolcode" required="required" placeholder="School Code">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>School Coordinates</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" v-model="xcoordinate" name="school_elevation" class="theme" id="elevation" placeholder="Elevation (Meter)">
                                        <input type="text" v-model="ycoordinate" name="school_latitude" class="theme" id="latitude_north" placeholder="Latitude  North">
                                        <input type="text" v-model="zcoordinate" name="school_longitude" class="theme" id="longitude_east" placeholder="Longitude East">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.1 school name</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" v-model="schoolname" name="schoolname" class="theme" id="schoolname" required="required" placeholder="School Name">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.2 name of proprietor</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" v-model="proprietor" name="proprietorname" class="theme" id="proprietorname" required="required" placeholder="Proprietor Name">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.3 Number and Street Address</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" name="schoolstreet" v-model="schoolstreet" class="theme" id="schoolstreet" required="required" placeholder="Number and Street">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.4 state</p>
                                    </div>
                                    <div class="answer">
                                        <select v-model="state_picked" class="theme" v-on:change="getLGA" name="year" id="">
                                            <option v-for="(state,index) in fetch_states" v-bind:value="index">(@{{state.code}}) - @{{state.name}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.5 LGA</p>
                                    </div>
                                    <div class="answer">
                                        <select v-model="lga_picked" name="year" class="theme" id="">
                                            <option v-for="(lga,index) in fetch_lga" v-bind:value="index">(@{{lga.code}}) - @{{lga.name}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.6 Name of Village or Town</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" v-model="schooltown" name="schooltown" id="schooltown" class="theme" required="required" placeholder="Village or Town">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.7 ward</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" v-model="schoolward" name="schoolward" class="theme" id="schoolward" required="required" placeholder="School Ward">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.8 School Telephone</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" v-model="schooltelephone" name="schooltelephone" class="theme" id="schooltelephone" required="required" placeholder="School Telephone">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>A.9 E-Mail Address</p>
                                    </div>
                                    <div class="answer">
                                        <input type="email" v-model="schoolemail" name="schoolemail" id="schoolemail" class="theme" required="required" placeholder="School Email Address">
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveIdentification">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveIdentification">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab3">
                            <p class="label">B. School Characteristics</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>B.1 Year of establishment-Pre-primary</p>
                                    </div>
                                    <div class="answer">
                                        <select v-model="year_of_establishment_preprimary" name="" class="theme" id="">
                                            <option value="" disabled>Choose Year</option>
                                            <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                        </select>
                                        <!-- <input type="text" v-model="year_of_establishment_preprimary" name="year_of_establishment_preprimary" class="theme"  id=""  placeholder=""> -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.2 Year of establishment-Primary</p>
                                    </div>
                                    <div class="answer">
                                        <select v-model="year_of_establishment_primary" name="" class="theme" id="">
                                            <option value="" disabled>Choose Year</option>
                                            <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                        </select>
                                        <!-- <input type="text" v-model="year_of_establishment_primary" name="year_of_establishment_primary" class="theme"  id=""  placeholder=""> -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.3 Year of establishment-Junior Secondary School</p>
                                    </div>
                                    <div class="answer">
                                        <select v-model="year_of_establishment_jss" name="" class="theme" id="">
                                            <option value="" disabled>Choose Year</option>
                                            <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>

                                        </select>
                                        <!-- <input type="text" v-model="year_of_establishment_jss" name="year_of_establishment_jss" class="theme"  id=""  placeholder=""> -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.4 Year of establishment-Senior Secondary School</p>
                                    </div>
                                    <div class="answer">
                                        <select v-model="year_of_establishment_ss" name="" class="theme" id="">
                                            <option value="" disabled>Choose Year</option>
                                            <option v-for="n in fetch_yearslist" v-bind:value="n">@{{n}}</option>
                                        </select>
                                        <!-- <input type="text" v-model="year_of_establishment_ss" name="year_of_establishment_ss" class="theme"  id=""  placeholder=""> -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.5 Location</p>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li v-for="(location,index) in fetch_location"><input type="radio" v-bind:value="location.value" v-model="location_of_school" name="loca" id="urban"><label for="urban">@{{location.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.6 Ownership</p>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li v-for="(owner,index) in fetch_ownership"><input type="radio" v-bind:value="owner.value" v-model="ownership" name="owner" id="urban"><label for="urban">@{{owner.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.7 Recognition status</p>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li v-for="(recognition,index) in fetch_recognition_status"><input type="radio" v-bind:value="recognition.value" v-model="recognition_status" name="recog" required="required" id="urban"><label for="urban">@{{recognition.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.8 Levels of education offered</p>
                                        <em>Tick all that apply</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li v-for="(level,index) in fetch_levels"><input type="checkbox" v-on:change="changelevel(level.value,$event)" v-bind:value="level.value" v-model="level_of_education" name="level-of-edu" v-bind:id="level.value"><label v-bind:for="level.value">@{{level.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.9 Shifts</p>
                                        <em>Does the School operate shift system?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" name="shifts" v-model="shifts_choice" value="1" id="shifts_yes"><label for="shifts_yes">Yes</label></li>
                                            <li><input type="radio" name="shifts" v-model="shifts_choice" value="0" id="shifts_no"><label for="shifts_no">No</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.10 Shared facilities</p>
                                        <em>Does the school share facilities/Teachers/premises with any other school?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" name="shared-facilities" v-model="facilities_choice" value="1" required="required" id="shared-facilities_yes"><label for="shared-facilities_yes">Yes</label></li>
                                            <li><input type="radio" name="shared-facilities" v-model="facilities_choice" value="0" required="required" id="shared-facilities_no"><label for="shared-facilities_no">No</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.11 Type of school</p>
                                        <em>Does your school fall into any of these special categories?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li v-for="(category,index) in fetch_category"><input type="radio" v-bind:value="category.value" v-model="type_of_school" name="type-of-school" required="required" id="type-of-school-reg"><label for="">@{{category.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.12 Is the School a member of Private Schools Association?</p>
                                        <em>If a member write name otherwise write None</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" name="psa" v-model="is_psa" value="1" id="psa_yes"><label for="psa_yes">Yes</label></li>
                                            <li><input type="radio" name="psa" v-model="is_psa" value="0" id="psa_no"><label for="psa_no">No</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.13 School: Average Distance from Catchment communities</p>
                                        <em>What is average distance of school from its catchment areas</em>
                                    </div>
                                    <div class="answer helper">
                                        <input type="number" min="0" class="theme" name="" v-model="average_distance" required="required" id="avg-distance"><label for="avg-distance">kilometres (Enter 0 if within 1 km)</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.14 Students/Pupils Boarding</p>
                                        <em>How many students board at the school premises?</em>
                                    </div>
                                    <div class="answer helper">
                                        <ul>
                                            <li><input type="radio" name="isboarding" v-model="boarding_choice" value="1" required="required" id="shared-facilities_yes"><label for="shared-facilities_yes">Yes</label></li>
                                            <li><input type="radio" name="isboarding" v-model="boarding_choice" value="0" required="required" id="shared-facilities_no"><label for="shared-facilities_no">No</label></li>
                                        </ul>
                                        <input type="number" min="0" class="theme" name="boarding" v-model="students_boarding_male" required="required" id="boarding_male"><label for="boarding_male">male</label>
                                        <input type="number" min="0" class="theme" name="boarding" v-model="students_boarding_female" required="required" id="boarding_female"><label for="boarding_female">female</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.15 School Development Plan (SDP)</p>
                                        <em>Did the school prepare SDP in the last school year?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" name="sdp" v-model="sdp_choice" value="1" required="required" id="sdp_yes"><label for="sdp_yes">Yes</label></li>
                                            <li><input type="radio" name="sdp" v-model="sdp_choice" value="0" required="required" id="sdp_no"><label for="sdp_no">No</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.16 School Based Management Committee (SBMC)</p>
                                        <em>Does the school have SBMC, which met at least once last year?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" name="sbmc" v-model="sbmc_choice" value="1" required="required" id="sbmc_yes"><label for="sbmc_yes">Yes</label></li>
                                            <li><input type="radio" name="sbmc" v-model="sbmc_choice" value="0" required="required" id="sbmc_no"><label for="sbmc_no">No</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.17 Parents’-Teachers’ Association (PTA) / Parents Forum (PF)</p>
                                        <em>Does the school have PTA / PF, which met at least once last year?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li><input type="radio" name="pta" v-model="pta_choice" value="1" required="required" id="pta_yes"><label for="pta_yes">Yes</label></li>
                                            <li><input type="radio" name="pta" v-model="pta_choice" value="0" required="required" id="pta_no"><label for="pta_no">No</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.18 Date of Last Inspection Visit</p>
                                        <em>When was the school last inspected?</em>
                                        <p>Number of inspection Visit in last academic year</p>
                                    </div>
                                    <div class="answer helper">
                                        <input type="date" class="theme" name="" v-model="date_inspection" required="required" id="">
                                        <input type="number" min="0" class="theme" name="" v-model="no_of_inspection" id=""><label for="">No.</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.19 Authority of Last Inspection</p>
                                        <em>Which authority conducted the last inspection visit?</em>
                                    </div>
                                    <div class="answer">
                                        <ul>
                                            <li v-for="(authority,index) in fetch_authority"><input type="radio" v-bind:value="authority.value" v-model="authority_choice" name="authority-inspection" required="required" id="authority-inspection_fed"><label for="authority-inspection_fed">@{{authority.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>B.20 Security Guard</p>
                                        <em>How many employed Security Guards does the school have?</em>
                                    </div>
                                    <div class="answer helper">
                                        <input type="number" min="0" class="theme" v-model="guard_choice" name="" id=""><label for="">(Number)</label>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveCharacteristics">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveCharacteristics">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab4">
                            <p class="label">C.1 Number of Children with Birth Certificates</p>
                            <div v-if="!ispreprimary && !isprimary">
                                <h2>Not a Pre-primary or Primary, move to the next section</h2>
                            </div>
                            <div v-if="ispreprimary || isprimary">
                                <form action="" class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect" rowspan="3"><label for="">How many children were enrolled with Birth certificates</label></td>
                                            <td v-if="ispreprimary" class="effectGray label-effect" colspan="10"><label for="">Pre-primary</label></td>
                                            <td v-if="isprimary" class="effectGray label-effect" colspan="2" rowspan="2"><label for="">Primary 1</label></td>
                                        </tr>
                                        <tr v-if="ispreprimary">
                                            <td class="effectGray label-effect" colspan="2"><label for="">Kindergarten 1/ECCD</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>Kindergarten2/ECCD</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>Nursery 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>Nursery 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>Nursery 3 / One Year pre-primary</label></td>
                                        </tr>
                                        <tr>
                                            <td v-if="ispreprimary" class="effectGray label-effect"><label>Male<label></td>
                                            <td v-if="ispreprimary" class="effectGray label-effect"><label>Female<label></td>
                                            <td v-if="ispreprimary" class="effectGray label-effect"><label>Male<label></td>
                                            <td v-if="ispreprimary" class="effectGray label-effect"><label>Female<label></td>
                                            <td v-if="ispreprimary" class="effectGray label-effect"><label>Male<label></td>
                                            <td v-if="ispreprimary" class="effectGray label-effect"><label>Female<label></td>
                                            <td v-if="ispreprimary" class="effectGray label-effect"><label>Male<label></td>
                                            <td v-if="ispreprimary" class="effectGray label-effect"><label>Female<label></td>
                                            <td v-if="ispreprimary" class="effectGray label-effect"><label>Male<label></td>
                                            <td v-if="ispreprimary" class="effectGray label-effect"><label>Female<label></td>
                                            <td v-if="isprimary" class="effectGray label-effect"><label>Male<label></td>
                                            <td v-if="isprimary" class="effectGray label-effect"><label>Female<label></td>
                                        </tr>
                                        <tr v-for="(birth_certificate,index) in fetch_birth_certificate">
                                            <td><label for="">@{{birth_certificate.value}}</label></td>
                                            <td v-if="ispreprimary"><input class="theme" v-model="birth_certificate.kindergarten1_male" type="number" min="0"></td>
                                            <td v-if="ispreprimary"><input class="theme" v-model="birth_certificate.kindergarten1_female" type="number" min="0"></td>
                                            <td v-if="ispreprimary"><input class="theme" v-model="birth_certificate.kindergarten2_male" type="number" min="0"></td>
                                            <td v-if="ispreprimary"><input class="theme" v-model="birth_certificate.kindergarten2_female" type="number" min="0"></td>
                                            <td v-if="ispreprimary"><input class="theme" v-model="birth_certificate.nursery1_male" type="number" min="0"></td>
                                            <td v-if="ispreprimary"><input class="theme" v-model="birth_certificate.nursery1_female" type="number" min="0"></td>
                                            <td v-if="ispreprimary"><input class="theme" v-model="birth_certificate.nursery2_male" type="number" min="0"></td>
                                            <td v-if="ispreprimary"><input class="theme" v-model="birth_certificate.nursery2_female" type="number" min="0"></td>
                                            <td v-if="ispreprimary"><input class="theme" v-model="birth_certificate.nursery3_male" type="number" min="0"></td>
                                            <td v-if="ispreprimary"><input class="theme" v-model="birth_certificate.nursery3_female" type="number" min="0"></td>
                                            <td v-if="isprimary"><input class="theme" v-model="birth_certificate.primary1_male" type="number" min="0"></td>
                                            <td v-if="isprimary"><input class="theme" v-model="birth_certificate.primary1_female" type="number" min="0"></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveBirthCertificates">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveBirthCertificates">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab5">
                            <p class="label">C.2 Number of Children with Birth Certificates</p>
                            <div v-if="!isjss && !issss">
                                <h2>Not a Jss or Sss, move to the next section</h2>
                            </div>
                            <div v-if="isjss || issss">
                                <form action="" class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect" rowspan="2"><label for="">How many children were enrolled with Birth certificates</label></td>
                                            <td v-if="isjss" class="effectGray label-effect" colspan="2"><label for="">JSS 1</label></td>
                                            <td v-if="issss" class="effectGray label-effect" colspan="2"><label for="">SS 1</label></td>
                                        </tr>
                                        <tr>
                                            <td v-if="isjss" class="effectGray label-effect"><label for="">Male</label></td>
                                            <td v-if="isjss" class="effectGray label-effect"><label for="">Female</label></td>
                                            <td v-if="issss" class="effectGray label-effect"><label for="">Male</label></td>
                                            <td v-if="issss" class="effectGray label-effect"><label for="">Female</label></td>
                                        </tr>
                                        <tr v-for="(birth_certificate,index) in fetch_birth_certificate">
                                            <td><label for="">@{{birth_certificate.value}}</label></td>
                                            <td v-if="isjss"><input class="theme" v-model="birth_certificate.male" type="number" min="0"></td>
                                            <td v-if="isjss"><input class="theme" v-model="birth_certificate.female" type="number" min="0"></td>
                                            <td v-if="issss"><input class="theme" v-model="birth_certificate.sss1_male" type="number" min="0"></td>
                                            <td v-if="issss"><input class="theme" v-model="birth_certificate.sss1_female" type="number" min="0"></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveSecondaryBirthCertificates">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveSecondaryBirthCertificates">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab6">
                            <p class="label">C.3 Pre-primary Enrolment for the Current Academic Year</p>
                            <div v-if="!ispreprimary">
                                <h2>Not a Pre-primary or Primary, move to the next section</h2>
                            </div>
                            <div v-if="ispreprimary">
                                <form action=""  class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect"></td>
                                            <td class="effectGray label-effect" colspan="2"><label>Kindergarten 1/ECCD</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>Kindergarten 2/ECCD</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>Nursery 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>Nursery 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>Nursery 3</label></td>

                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">No. of Streams</label></td>
                                            <td colspan="2"><input class="theme" v-model="kindergarten1_stream" name="" type="number" min="0"></td>
                                            <td colspan="2"><input class="theme" v-model="kindergarten2_stream" name="" type="number" min="0"></td>
                                            <td colspan="2"><input class="theme" v-model="nursery1_stream" name="" type="number" min="0"></td>
                                            <td colspan="2"><input class="theme" v-model="nursery2_stream" name="" type="number" min="0"></td>
                                            <td colspan="2"><input class="theme" v-model="nursery3_stream" name="" type="number" min="0"></td>

                                        </tr>
                                        <tr>
                                            <td class="effectGray"></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>

                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">Below 3 Years</label></td>
                                            <td><input class="theme" v-model="below3_kindergarten1_male" v-on:change="kindergarten1_male_tot" type="number" name="" id="" min="0"></td>
                                            <td><input class="theme" v-model="below3_kindergarten1_female" v-on:change="kindergarten1_female_tot" type="number" name="" id="" min="0"></td>
                                            <td><input class="theme" v-model="below3_kindergarten2_male" v-on:change="kindergarten2_male_tot" type="number" name="" id="" min="0"></td>
                                            <td><input class="theme" v-model="below3_kindergarten2_female" v-on:change="kindergarten2_female_tot" type="number" name="" id="" min="0"></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">3 Years</label></td>
                                            <td><input class="theme" v-model="age3_kindergarten1_male" v-on:change="kindergarten1_male_tot" type="number" name="" id="" min="0"></td>
                                            <td><input class="theme" v-model="age3_kindergarten1_female" v-on:change="kindergarten1_female_tot" type="number" name="" id="" min="0"></td>
                                            <td><input class="theme" v-model="age3_kindergarten2_male" v-on:change="kindergarten2_male_tot" type="number" name="" id="" min="0"></td>
                                            <td><input class="theme" v-model="age3_kindergarten2_female" v-on:change="kindergarten2_female_tot" type="number" name="" min="0" id=""></td>
                                            <td><input class="theme" v-model="age3_nursery1_male" v-on:change="nursery1_male_tot" type="number" name="" min="0" id=""></td>
                                            <td><input class="theme" v-model="age3_nursery1_female" v-on:change="nursery1_female_tot" type="number" name="" min="0" id=""></td>
                                            <td><input class="theme" v-model="age3_nursery2_male" v-on:change="nursery2_male_tot" type="number" name="" min="0" id=""></td>
                                            <td><input class="theme" v-model="age3_nursery2_female" v-on:change="nursery2_female_tot" type="number" name="" min="0" id=""></td>
                                            <td><input class="theme" v-model="age3_nursery3_male" v-on:change="nursery3_male_tot" type="number" name="" min="0" id=""></td>
                                            <td><input class="theme" v-model="age3_nursery3_female" v-on:change="nursery3_female_tot" type="number" name="" min="0" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">4 Years</label></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input class="theme" v-model="age4_nursery1_male" v-on:change="nursery1_male_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age4_nursery1_female" v-on:change="nursery1_female_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age4_nursery2_male" v-on:change="nursery2_male_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age4_nursery2_female" v-on:change="nursery2_female_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age4_nursery3_male" v-on:change="nursery3_male_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age4_nursery3_female" v-on:change="nursery3_female_tot" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">5 Years</label></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input class="theme" v-model="age5_nursery1_male" v-on:change="nursery1_male_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age5_nursery1_female" v-on:change="nursery1_female_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age5_nursery2_male" v-on:change="nursery2_male_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age5_nursery2_female" v-on:change="nursery2_female_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age5_nursery3_male" v-on:change="nursery3_male_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age5_nursery3_female" v-on:change="nursery3_female_tot" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">Above 5 Years</label></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input type="number" min="0" name="" id="" disabled></td>
                                            <td><input class="theme" v-model="above5_nursery1_male" v-on:change="nursery1_male_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="above5_nursery1_female" v-on:change="nursery1_female_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="above5_nursery2_male" v-on:change="nursery2_male_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="above5_nursery2_female" v-on:change="nursery2_female_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="above5_nursery3_male" v-on:change="nursery3_male_tot" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="above5_nursery3_female" v-on:change="nursery3_female_tot" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray text label-effect"><label for="">Total</label></td>
                                            <td class="effectGray"><input type="number" min="0" v-model="kindergarten1_male_total" name="" id="" disabled placeholder="0"></td>
                                            <td class="effectGray"><input type="number" min="0" v-model="kindergarten1_female_total" name="" id="" disabled placeholder="0"></td>
                                            <td class="effectGray"><input type="number" min="0" v-model="kindergarten2_male_total" name="" id="" disabled placeholder="0"></td>
                                            <td class="effectGray"><input type="number" min="0" v-model="kindergarten2_female_total" name="" id="" disabled placeholder="0"></td>
                                            <td class="effectGray"><input type="number" min="0" v-model="nursery1_male_total" name="" id="" disabled placeholder="0"></td>
                                            <td class="effectGray"><input type="number" min="0" v-model="nursery1_female_total" name="" id="" disabled placeholder="0"></td>
                                            <td class="effectGray"><input type="number" min="0" v-model="nursery2_male_total" name="" id="" disabled placeholder="0"></td>
                                            <td class="effectGray"><input type="number" min="0" v-model="nursery2_female_total" name="" id="" disabled placeholder="0"></td>
                                            <td class="effectGray"><input type="number" min="0" v-model="nursery3_male_total" name="" id="" disabled placeholder="0"></td>
                                            <td class="effectGray"><input type="number" min="0" v-model="nursery3_female_total" name="" id="" disabled placeholder="0"></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="savePreprimaryEnrollmentByAge">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="savePreprimaryEnrollmentByAge">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab7">
                            <p class="label"> C.4 New entrants in PRY 1</p>
                            <div v-if="!isprimary">
                                <h2>Not a Primary, move to the next section</h2>
                            </div>
                            <div v-if="isprimary">
                                <form action="" class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray"></td>
                                            <td class="effectGray label-effect" colspan="2"><label for="">New entrants in PRY1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label for="">How many of the new entrants attended any early childhood education </label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label for="">Pupil Age</label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                        </tr>
                                        <tr v-for="(age,index) in fetch_primary_age">
                                            <td class="text label-effect"><label for="">@{{age.value}}</label></td>
                                            <td><input class="theme" name="" v-on:change="pp_entrantmale_total" v-model="age.male" type="number" min="0"></td>
                                            <td><input class="theme" name="" v-on:change="pp_entrantfemale_total" v-model="age.female" type="number" min="0"></td>
                                            <td><input class="theme" name="" v-on:change="eccd_entrantmale_total" v-model="age.eccd_male" type="number" min="0"></td>
                                            <td><input class="theme" name="" v-on:change="eccd_entrantfemale_total" v-model="age.eccd_female" type="number" min="0"></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray text label-effect"><label for="">Total</label></td>
                                            <td class="effectGray"><input disabled v-model="p1_entrant_male_total" type="number" min="0" name="" id="" placeholder="0"></td>
                                            <td class="effectGray"><input disabled v-model="p1_entrant_female_total" type="number" min="0" name="" id="" placeholder="0"></td>
                                            <td class="effectGray"><input disabled v-model="p1_eccd_entrant_male_total" type="number" min="0" name="" id="" placeholder="0"></td>
                                            <td class="effectGray"><input disabled v-model="p1_eccd_entrant_female_total" type="number" min="0" name="" id="" placeholder="0"></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="savePreprimaryEntrants">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="savePreprimaryEntrants">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab8">
                            <p class="label">C5. Primary Enrolment for the Current Academic Year by age</p>
                            <div v-if="!isprimary">
                                <h2>Not a Primary, move to the next section</h2>
                            </div>
                            <div v-if="isprimary">
                            <form action=""  class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td class="effectGray"></td>
                                        <td class="effectGray label-effect" colspan="2"><label>PRY 1</label></td>
                                        <td class="effectGray label-effect" colspan="2"><label>PRY 2</label></td>
                                        <td class="effectGray label-effect" colspan="2"><label>PRY 3</label></td>
                                        <td class="effectGray label-effect" colspan="2"><label>PRY 4</label></td>
                                        <td class="effectGray label-effect" colspan="2"><label>PRY 5</label></td>
                                        <td class="effectGray label-effect" colspan="2"><label>PRY 6</label></td>
                                    </tr>
                                    <tr>
                                        <td class="text label-effect"><label for="">No. of stream</label></td>
                                        <td colspan="2"><input class="theme" v-model="p1_stream" type="number" min="0" name="" id=""></td>
                                        <td colspan="2"><input class="theme" v-model="p2_stream" type="number" min="0" name="" id=""></td>
                                        <td colspan="2"><input class="theme" v-model="p3_stream" type="number" min="0" name="" id=""></td>
                                        <td colspan="2"><input class="theme" v-model="p4_stream" type="number" min="0" name="" id=""></td>
                                        <td colspan="2"><input class="theme" v-model="p5_stream" type="number" min="0" name="" id=""></td>
                                        <td colspan="2"><input class="theme" v-model="p6_stream" type="number" min="0" name="" id=""></td>
                                    </tr>
                                    <tr>
                                        <td class="text label-effect"><label for="">No of streams with Multigrade teaching</label></td>
                                        <td colspan="2"><input class="theme" v-model="p1_stream_with_multigrade" type="number" min="0" name="" id=""></td>
                                        <td colspan="2"><input class="theme" v-model="p2_stream_with_multigrade" type="number" min="0" name="" id=""></td>
                                        <td colspan="2"><input class="theme" v-model="p3_stream_with_multigrade" type="number" min="0" name="" id=""></td>
                                        <td colspan="2"><input class="theme" v-model="p4_stream_with_multigrade" type="number" min="0" name="" id=""></td>
                                        <td colspan="2"><input class="theme" v-model="p5_stream_with_multigrade" type="number" min="0" name="" id=""></td>
                                        <td colspan="2"><input class="theme" v-model="p6_stream_with_multigrade" type="number" min="0" name="" id=""></td>
                                    </tr>
                                    <tr>
                                        <td class="effectGray label-effect"><label>Pupil Age</label></td>
                                        <td class="effectGray label-effect"><label>Male</label></td>
                                        <td class="effectGray label-effect"><label>Female</label></td>
                                        <td class="effectGray label-effect"><label>Male</label></td>
                                        <td class="effectGray label-effect"><label>Female</label></td>
                                        <td class="effectGray label-effect"><label>Male</label></td>
                                        <td class="effectGray label-effect"><label>Female</label></td>
                                        <td class="effectGray label-effect"><label>Male</label></td>
                                        <td class="effectGray label-effect"><label>Female</label></td>
                                        <td class="effectGray label-effect"><label>Male</label></td>
                                        <td class="effectGray label-effect"><label>Female</label></td>
                                        <td class="effectGray label-effect"><label>Male</label></td>
                                        <td class="effectGray label-effect"><label>Female</label></td>
                                    </tr>
                                    <tr v-for="(age,index) in fetch_age_primary">
                                        <td class="text label-effect"><label>@{{age.value}}</label></td>
                                        <td><input class="theme" v-on:change="yearbyage_p1_male_tot" v-model="age.primary1_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-on:change="yearbyage_p1_female_tot" v-model="age.primary1_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-on:change="yearbyage_p2_male_tot" v-model="age.primary2_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-on:change="yearbyage_p2_female_tot" v-model="age.primary2_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-on:change="yearbyage_p3_male_tot" v-model="age.primary3_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-on:change="yearbyage_p3_female_tot" v-model="age.primary3_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-on:change="yearbyage_p4_male_tot" v-model="age.primary4_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-on:change="yearbyage_p4_female_tot" v-model="age.primary4_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-on:change="yearbyage_p5_male_tot" v-model="age.primary5_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-on:change="yearbyage_p5_female_tot" v-model="age.primary5_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-on:change="yearbyage_p6_male_tot" v-model="age.primary6_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-on:change="yearbyage_p6_female_tot" v-model="age.primary6_female" type="number" min="0" name="" id=""></td>
                                    </tr>
                                    <tr>
                                        <td class="label-effect"><label>Total</label></td>
                                        <td><input type="number" min="0" disabled="disabled" v-model="yearbyage_p1_male_total" name="" id=""></td>
                                        <td><input type="number" min="0"  disabled="disabled" v-model="yearbyage_p1_female_total" name="" id=""></td>
                                        <td><input type="number" min="0" disabled="disabled" v-model="yearbyage_p2_male_total" name="" id=""></td>
                                        <td><input type="number" min="0" disabled="disabled" v-model="yearbyage_p2_female_total" name="" id=""></td>
                                        <td><input type="number" min="0" disabled="disabled" v-model="yearbyage_p3_male_total" name="" id=""></td>
                                        <td><input type="number" min="0" disabled="disabled" v-model="yearbyage_p3_female_total" name="" id=""></td>
                                        <td><input type="number" min="0" disabled="disabled" v-model="yearbyage_p4_male_total" name="" id=""></td>
                                        <td><input type="number" min="0" disabled="disabled" v-model="yearbyage_p4_female_total" name="" id=""></td>
                                        <td><input type="number" min="0" disabled="disabled" v-model="yearbyage_p5_male_total" name="" id=""></td>
                                        <td><input type="number" min="0" disabled="disabled" v-model="yearbyage_p5_female_total" name="" id=""></td>
                                        <td><input type="number" min="0" disabled="disabled" v-model="yearbyage_p6_male_total" name="" id=""></td>
                                        <td><input type="number" min="0" disabled="disabled" v-model="yearbyage_p6_female_total" name="" id=""></td>
                                    </tr>
                                    <tr>
                                        <td class="text label-effect"><label>Repeaters</label></td>
                                        <td><input class="theme" v-model="p1_repeaters_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model="p1_repeaters_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model="p2_repeaters_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model="p2_repeaters_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model="p3_repeaters_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model="p3_repeaters_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model="p4_repeaters_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model="p4_repeaters_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model="p5_repeaters_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model="p5_repeaters_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model="p6_repeaters_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model="p6_repeaters_female" type="number" min="0" name="" id=""></td>
                                    </tr>
                                    <tr>
                                        <td class="text label-effect"><label>Completed P6 for previous year</label></td>
                                        <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                        <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                        <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                        <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                        <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                        <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                        <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                        <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                        <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                        <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                        <td><input class="theme" v-model="prevyear_primary6_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model="prevyear_primary6_female" type="number" min="0" name="" id=""></td>
                                    </tr>
                                </table>
                            </form>
                            </div>
                            
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="savePrimaryEnrollmentByAge">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="savePrimaryEnrollmentByAge">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab9">
                            <p class="label">C.6 Students with Special Needs for the Current Academic Year</p>
                            <div v-if="!ispreprimary && !isprimary">
                                <h2>Not a Pre-primary or Primary, move to the next section</h2>
                            </div>
                            <div v-if="ispreprimary || isprimary">
                                <form action=""  class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect" rowspan="2"><label>Challenge that impacts the ability to learn</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>ECCS (KG1-KG2)</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>NURS (NR1 - NR2)</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>One Year Pre-primary</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 3</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 4</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 5</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 6</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                        </tr>
                                        <tr v-for="(need,index) in fetch_special_needs">
                                            <td class="text label-effect"><label>@{{need.value}}</label></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.eccd_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.eccd_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.nursery_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.nursery_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.nursery3_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.nursery3_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.primary1_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.primary1_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.primary2_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.primary2_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.primary3_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.primary3_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.primary4_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.primary4_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.primary5_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.primary5_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.primary6_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.primary6_female" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="savePreprimarySpecialNeed">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="savePreprimarySpecialNeed">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab10">
                            <p class="label">C. 7 Number of Pupils by Vulnerability by Grade</p>
                            <div v-if="!ispreprimary && !isprimary">
                                <h2>Not a Pre-primary or Primary, move to the next section</h2>
                            </div>
                            <div v-if="ispreprimary || isprimary">
                                <form action=""  class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect" rowspan="2"><label>Vulnerability</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>ECCS</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>NURS</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>One Year Pre-primary</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 3</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 4</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 5</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 6</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                            <td class="effectGray label-effect"><label>M</label></td>
                                            <td class="effectGray label-effect"><label>F</label></td>
                                        </tr>
                                        <tr v-for="(orphan,index) in fetch_orphans">
                                            <td class="text label-effect"><label>@{{orphan.value}}</label></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.eccd_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.eccd_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.nursery_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.nursery_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.nursery3_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.nursery3_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.primary1_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.primary1_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.primary2_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.primary2_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.primary3_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.primary3_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.primary4_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.primary4_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.primary5_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.primary5_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.primary6_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="orphan.primary6_female" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveStudentsOrphan">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveStudentsOrphan">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab11">
                            <p class="label">C.8 Pupil Flow for the Current Academic Year (PRIMARY)</p>
                            <div v-if="!isprimary">
                                <h2>Not a Primary, move to the next section</h2>
                            </div>
                            <div v-if="isprimary">
                                <form action=""  class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect" rowspan="2"><label>Pupil Flow</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 3</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 4</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 5</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>PRY 6</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                        </tr>
                                        <tr v-for="(pupil,index) in fetch_pupilflow">
                                            <td class="text label-effect"><label>@{{pupil.value}}</label></td>
                                            <td><input class="theme" v-model="pupil.primary1_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="pupil.primary1_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="pupil.primary2_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="pupil.primary2_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="pupil.primary3_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="pupil.primary3_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="pupil.primary4_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="pupil.primary4_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="pupil.primary5_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="pupil.primary5_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="pupil.primary6_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="pupil.primary6_female" type="number" min="0" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="savePreprimaryStudentsFlow">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="savePreprimaryStudentsFlow">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab12">
                            <p class="label">C.9 New entrants in JSS1</p>
                            <div v-if="!isjss">
                                <h2>Not a Jss, move to the next section</h2>
                            </div>
                            <div v-if="isjss">
                                <form action=""  class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray"></td>
                                            <td colspan="2" class="effectGray label-effect"><label>New Entrants in JSS 1</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>Student Age</label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                        </tr>
                                        <tr v-for="(age,index) in fetch_age">
                                            <td class="text label-effect"><label for="">@{{age.value}}</label></td>
                                            <td><input class="theme" v-model="age.male" name="" v-on:change="entrantmale_total" min="0" type="number"></td>
                                            <td><input class="theme" v-model="age.female" name="" v-on:change="entrantfemale_total" min="0" type="number"></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">Total</label></td>
                                            <td><input type="number" min="0" disabled v-bind:value="entrantmaletotal" name="" id="" placeholder="0"></td>
                                            <td><input type="number" min="0" disabled v-bind:value="entrantfemaletotal" name="" id="" placeholder="0"></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveEntrants">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveEntrants">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab13">
                            <p class="label">C10. Junior Secondary Enrolment for the Current Academic Year by age</p>
                            <div v-if="!isjss">
                                <h2>Not a Jss, move to the next section</h2>
                            </div>
                            <div v-if="isjss">
                                <form action=""  class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect"></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 3</label></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">No. of stream</label></td>
                                            <td colspan="2"><input class="theme" v-model="jss1_stream" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input class="theme" v-model="jss2_stream" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input class="theme" v-model="jss3_stream" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">No of streams with Multigrade teaching</label></td>
                                            <td colspan="2"><input class="theme" v-model="jss1_stream_with_multigrade" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input class="theme" v-model="jss2_stream_with_multigrade" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input class="theme" v-model="jss3_stream_with_multigrade" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>Age</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                        </tr>
                                        <tr v-for="(age,index) in fetch_year_by_age">
                                            <td class="text label-effect"><label>@{{age.value}}</label></td>
                                            <td><input class="theme" v-model="age.jss1_male" v-on:change="jss1entrantmale_total" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age.jss1_female" v-on:change="jss1entrantfemale_total" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age.jss2_male" v-on:change="jss2entrantmale_total" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age.jss2_female" v-on:change="jss2entrantfemale_total" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age.jss3_male" v-on:change="jss3entrantmale_total" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="age.jss3_female" v-on:change="jss3entrantfemale_total" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>Total</label></td>
                                            <td><input type="number" min="0" disabled="disabled" v-model="jss1maletotal" name="" id=""></td>
                                            <td><input type="number" min="0" disabled="disabled" v-model="jss1femaletotal" name="" id=""></td>
                                            <td><input type="number" min="0" disabled="disabled" v-model="jss2maletotal" name="" id=""></td>
                                            <td><input type="number" min="0" disabled="disabled" v-model="jss2femaletotal" name="" id=""></td>
                                            <td><input type="number" min="0" disabled="disabled" v-model="jss3maletotal" name="" id=""></td>
                                            <td><input type="number" min="0" disabled="disabled" v-model="jss3femaletotal" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>Repeaters</label></td>
                                            <td><input class="theme" v-model="jss1_repeaters_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="jss1_repeaters_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="jss2_repeaters_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="jss2_repeaters_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="jss3_repeaters_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="jss3_repeaters_female" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="label-effect"><label>No. Completed JSS 3 for previous year</label></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input class="theme" type="number" min="0" name="" v-model="prevyear_jss3_male" id=""></td>
                                            <td><input class="theme" type="number" min="0" name="" v-model="prevyear_jss3_female" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveEnrollmentByAge">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveEnrollmentByAge">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab14">
                            <p class="label">C.11 New entrants in SS1</p>
                            <div v-if="!issss">
                                <h2>Not a Sss, move to the next section</h2>
                            </div>
                            <div v-if="issss">
                                <form action=""  class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray"></td>
                                            <td colspan="2" class="effectGray label-effect"><label>New Entrants in SSS 1</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>Student Age</label></td>
                                            <td class="effectGray label-effect"><label for="">Male</label></td>
                                            <td class="effectGray label-effect"><label for="">Female</label></td>
                                        </tr>
                                        <tr v-for="(age,index) in fetch_ss_age">
                                            <td class="text label-effect"><label for="">@{{age.value}}</label></td>
                                            <td><input class="theme" v-on:change="ss1_entrantmale_total" v-model="age.male" name="" min="0" type="number"></td>
                                            <td><input class="theme" v-on:change="ss1_entrantfemale_total" v-model="age.female" name="" min="0" type="number"></td>
                                        </tr>
                                        <tr>
                                            <td class="label-effect"><label for="">Total</label></td>
                                            <td><input type="number" min="0" disabled="disabled" v-bind:value="ss1_entrantmaletotal" name="" id=""></td>
                                            <td><input type="number" min="0" disabled="disabled" v-bind:value="ss1_entrantfemaletotal" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveSsEntrants">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveSsEntrants">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab15">
                            <p class="label">C12. Senior Secondary Enrolment for the Current Academic Year by age</p>
                            <div v-if="!issss">
                                <h2>Not a Sss, move to the next section</h2>
                            </div>
                            <div v-if="issss">
                                <form action=""  class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray"></td>
                                            <td class="effectGray label-effect" colspan="2"><label>SSS 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>SSS 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>SSS 3</label></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">No. of stream</label></td>
                                            <td colspan="2"><input class="theme" v-model="ss1_stream" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input class="theme" v-model="ss2_stream" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input class="theme" v-model="ss3_stream" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label for="">No of streams with Multigrade teaching</label></td>
                                            <td colspan="2"><input class="theme" v-model="ss1_stream_with_multigrade" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input class="theme" v-model="ss2_stream_with_multigrade" type="number" min="0" name="" id=""></td>
                                            <td colspan="2"><input class="theme" v-model="ss3_stream_with_multigrade" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>Age</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                        </tr>
                                        <tr v-for="(age,index) in fetch_ss_year_by_age">
                                            <td class="text label-effect"><label>@{{age.value}}</label></td>
                                            <td><input class="theme" v-on:change="ss1entrantmale_total" v-model="age.sss1_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-on:change="ss1entrantfemale_total" v-model="age.sss1_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-on:change="ss2entrantmale_total" v-model="age.sss2_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-on:change="ss2entrantfemale_total" v-model="age.sss2_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-on:change="ss3entrantmale_total" v-model="age.sss3_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-on:change="ss3entrantfemale_total" v-model="age.sss3_female" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="label-effect"><label>Total</label></td>
                                            <td><input type="number" min="0" disabled="disabled" v-model="ss1maletotal" name="" id=""></td>
                                            <td><input type="number" min="0" disabled="disabled" v-model="ss1femaletotal" name="" id=""></td>
                                            <td><input type="number" min="0" disabled="disabled" v-model="ss2maletotal" name="" id=""></td>
                                            <td><input type="number" min="0" disabled="disabled" v-model="ss2femaletotal" name="" id=""></td>
                                            <td><input type="number" min="0" disabled="disabled" v-model="ss3maletotal" name="" id=""></td>
                                            <td><input type="number" min="0" disabled="disabled" v-model="ss3femaletotal" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>Repeaters</label></td>
                                            <td><input class="theme" v-model="ss1_repeaters_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="ss1_repeaters_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="ss2_repeaters_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="ss2_repeaters_female" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="ss3_repeaters_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="ss3_repeaters_female" type="number" min="0" name="" id=""></td>
                                        </tr>
                                        <tr>
                                            <td class="text label-effect"><label>No. Completed SSS 3 for previous year</label></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input type="number" min="0" name="" disabled="disabled" id=""></td>
                                            <td><input class="theme" v-model="prevyear_sss3_male" type="number" min="0" name="" id=""></td>
                                            <td><input class="theme" v-model="prevyear_sss3_female" type="number" min="0" name="" id=""></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveSsEnrollmentByAge">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveSsEnrollmentByAge">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab16">
                            <p class="label">C.13 Student Flow for the Current Academic Year (JSS & SSS)</p>
                            <div v-if="!isjss && !issss">
                                <h2>Not a Jss or Sss, move to the next section</h2>
                            </div>
                            <div v-if="isjss || issss">
                                <form action=""  class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect" rowspan="2"><label>Student Flow</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 3</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>SSS 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>SSS 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>SSS 3</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                        </tr>
                                        <tr v-for="(pupil,index) in fetch_pupilflow">
                                            <td class="text label-effect"><label>@{{pupil.value}}</label></td>
                                            <td><input class="theme" v-model="pupil.jss1_male" type="number" min="0" name="a" id=""></td>
                                            <td><input class="theme" v-model="pupil.jss1_female" type="number" min="0" name="a" id=""></td>
                                            <td><input class="theme" v-model="pupil.jss2_male" type="number" min="0" name="a" id=""></td>
                                            <td><input class="theme" v-model="pupil.jss2_female" type="number" min="0" name="a" id=""></td>
                                            <td><input class="theme" v-model="pupil.jss3_male" type="number" min="0" name="a" id=""></td>
                                            <td><input class="theme" v-model="pupil.jss3_female" type="number" min="0" name="a" id=""></td>
                                            <td><input class="theme" v-model="pupil.sss1_male" type="number" min="0" name="a" id=""></td>
                                            <td><input class="theme" v-model="pupil.sss1_female" type="number" min="0" name="a" id=""></td>
                                            <td><input class="theme" v-model="pupil.sss2_male" type="number" min="0" name="a" id=""></td>
                                            <td><input class="theme" v-model="pupil.sss2_female" type="number" min="0" name="a" id=""></td>
                                            <td><input class="theme" v-model="pupil.sss3_male" type="number" min="0" name="a" id=""></td>
                                            <td><input class="theme" v-model="pupil.sss3_female" type="number" min="0" name="a" id=""></td>
                                        </tr>
                                    </table>

                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveStudentsFlow">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveStudentsFlow">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab17">
                            <p class="label"> C.14 Secondary Students with Special needs in the current school year</p>
                            <div v-if="!isjss && !issss">
                                <h2>Not a Jss or Sss, move to the next section</h2>
                            </div>
                            <div v-if="isjss || issss">
                                <form action=""  class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="effectGray label-effect" rowspan="2"><label>Challenge that impacts the ability to learn</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>JSS 3</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>SSS 1</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>SSS 2</label></td>
                                            <td class="effectGray label-effect" colspan="2"><label>SSS 3</label></td>
                                        </tr>
                                        <tr>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                            <td class="effectGray label-effect"><label>Male</label></td>
                                            <td class="effectGray label-effect"><label>Female</label></td>
                                        </tr>
                                        <tr v-for="(need,index) in fetch_special_needs">
                                            <td class="text label-effect"><label>@{{need.value}}</label></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.jss1_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.jss1_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.jss2_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.jss2_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.jss3_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.jss3_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.sss1_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.sss1_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.sss2_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.sss2_female" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.sss3_male" name="" id=""></td>
                                            <td><input class="theme" type="number" min="0" v-model="need.sss3_female" name="" id=""></td>
                                        </tr>

                                    </table>
                                </form>
                            </div>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveSpecialNeed">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveSpecialNeed">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab18">
                            <p class="label">D.1 Sources of drinking water</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>Pick source of drinking water</p>
                                    </div>
                                    <div class="answer">
                                        <ul id="list-tile">
                                            <li v-for="(source,index) in fetch_drinkingwater_source"><input type="checkbox" v-on:change="changedwsource(source.value,$event)" v-bind:value="source.value" v-model="sources_of_drinking_water" v-bind:id="source.value" name="Pipeborne"><label for="pbw">@{{source.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveDWSources">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveDWSources">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab19">
                            <p class="label">D.2 Toilet type</p>
                            <form action="" class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td class="effectGray label-effect" rowspan="2"><label></label></td>
                                        <td class="effectGray label-effect" colspan="3"><label>Used only by students</label></td>
                                        <td class="effectGray label-effect" colspan="3"><label>Used only by teachers</label></td>
                                        <td class="effectGray label-effect" colspan="3"><label>Used by students and teachers</label></td>
                                        <td class="effectGray label-effect" rowspan="2"><label>Total</label></td>
                                    </tr>
                                    <tr>
                                        <td class="effectGray label-effect"><label>Male only</label></td>
                                        <td class="effectGray label-effect"><label>Female only</label></td>
                                        <td class="effectGray label-effect"><label>Mixed</label></td>
                                        <td class="effectGray label-effect"><label>Male only</label></td>
                                        <td class="effectGray label-effect"><label>Female only</label></td>
                                        <td class="effectGray label-effect"><label>Mixed</label></td>
                                        <td class="effectGray label-effect"><label>Male only</label></td>
                                        <td class="effectGray label-effect"><label>Female only</label></td>
                                        <td class="effectGray label-effect"><label>Mixed</label></td>
                                    </tr>
                                    <tr v-for="(source,index) in fetch_toilet_type">
                                        <td class="text label-effect"><label>@{{source.value}}</label></td>
                                        <td><input class="theme" v-model.number="source.students_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.students_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.students_mixed" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.teachers_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.teachers_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.teachers_mixed" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.both_male" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.both_female" type="number" min="0" name="" id=""></td>
                                        <td><input class="theme" v-model.number="source.both_mixed" type="number" min="0" name="" id=""></td>
                                        <td><input v-model="(source.students_male*1)+(source.students_female*1)+(source.students_mixed*1)+(source.teachers_male*1)+(source.teachers_female*1)+(source.teachers_mixed*1)+(source.both_male*1)+(source.both_female*1)+source.both_mixed*1" type="number" min="0" disabled name="" id=""></td>
                                    </tr>
                                </table>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveToilet">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveToilet">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab20">
                            <p class="label">D.3 Facilities available</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>How many useable facilities does the school have?</p>
                                        <em>If the facilities are not available, write zero</em>
                                    </div>
                                    <div class="answer">
                                        <div class="infoD">
                                            <label for="">Useable</label>
                                            <label for="">Not Useable</label>
                                        </div>
                                        <div class="list-style" v-for="(source,index) in fetch_facilities">
                                            <span>
                                                <label v-bind:for="source.value">@{{source.value}}</label>
                                                <input class="theme" type="number" min="0" v-model="source.useable" name="use-toilet" v-bind:id="source.value">
                                                <input class="theme" type="number" min="0" v-model="source.notuseable" name="not-use-toilet" v-bind:id="source.value">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveFacilitiesAvailable">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveFacilitiesAvailable">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab21">
                            <p class="label">D.4 Shared Facilities</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>Tick shared Facilities</p>
                                    </div>
                                    <div class="answer">
                                        <ul id="list-tile">
                                            <li v-for="(source,index) in fetch_facilities"><input type="checkbox" v-bind:value="source.value" v-model="shared_facilities" v-on:change="changesharedfacilities(source.value,$event)" name="toilets" v-bind:id="source.value"><label v-bind:for="source.value">@{{source.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveSharedFacilities">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveSharedFacilities">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab22">
                            <p class="label">D.5 Source(s) of power</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>Source(s) of power</p>
                                        <em>Tick available source(s) of Power in the School </em>
                                    </div>
                                    <div class="answer">
                                        <ul id="list-tile">
                                            <li v-for="(source,index) in fetch_power_source"><input type="checkbox" v-bind:value="source.value" v-on:change="changesourceofpower(source.value,$event)" v-model="sources_of_power" name="" id="phcn`"><label for="phcn">@{{source.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="savePowerSource">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="savePowerSource">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab23">
                            <p class="label">D.6 Health facility</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>Health facility </p>
                                        <em>Does the school have a health facility?</em>
                                    </div>
                                    <div class="answer">
                                        <ul id="list-tile">
                                            <li v-for="(source,index) in fetch_healthfacility"><input type="radio" v-bind:value="source.value" v-model="health_facility" name="" id="hc"><label for="hc">@{{source.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveHealthFence">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveHealthFence">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab24">
                            <p class="label">D.7 Ownership</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>Ownership status of school building</p>
                                        <em>Are the school premises rented or owned?</em>
                                    </div>
                                    <div class="answer">
                                        <ul id="list-tile">
                                            <li v-for="(source,index) in fetch_ownership"><input type="radio" name="" v-bind:value="source.value" v-model="facility_ownership" id=""><label for="">@{{source.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveOwnership">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveOwnership">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab25">
                            <p class="label">D.8 Type of school building</p>
                            <form action="">
                                <div class="row">
                                    <div class="question">
                                        <p>Type of school building</p>
                                    </div>
                                    <div class="answer">
                                        <ul id="list-tile">
                                            <li v-for="(source,index) in fetch_schoolbuilding"><input type="radio" name="" v-bind:value="source.value" v-model="facility_schoolbuilding" id=""><label for="">@{{source.value}}</label></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveSchoolBuilding">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveSchoolBuilding">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab26">
                            <p class="label"> D.9 Additional Class Information - JSS and SSS</p>
                            <form action="" class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td class="effectGray label-effect" rowspan="2"><label>Class</label></td>
                                        <td class="effectGray label-effect" colspan="6"><label>Total Seating available</label></td>
                                    </tr>
                                    <tr>
                                        <td class="effectGray label-effect"><label>1 Seater</label></td>
                                        <td class="effectGray label-effect"><label>2 Seater</label></td>
                                        <td class="effectGray label-effect"><label>3 Seater</label></td>
                                        <td class="effectGray label-effect"><label>4 Seater</label></td>
                                        <td class="effectGray label-effect"><label>5 Seater</label></td>
                                        <td class="effectGray label-effect"><label>6 Seater</label></td>
                                    </tr>
                                    <tr v-for="(source,index) in fetch_seaters">
                                        <td class="text label-effect"><label>@{{source.value}}</label></td>
                                        <td><input class="theme" type="number" min="0" v-model="source.seater1" name="" id=""></td>
                                        <td><input class="theme" type="number" min="0" v-model="source.seater2" name="" id=""></td>
                                        <td><input class="theme" type="number" min="0" v-model="source.seater3" name="" id=""></td>
                                        <td><input class="theme" type="number" min="0" v-model="source.seater4" name="" id=""></td>
                                        <td><input class="theme" type="number" min="0" v-model="source.seater5" name="" id=""></td>
                                        <td><input class="theme" type="number" min="0" v-model="source.seater6" name="" id=""></td>
                                    </tr>
                                </table>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveSeaters">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveSeaters">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab27">
                            <p class="label">E. Teachers (By level of main teaching input) in current academic year</p>
                            <form action="" class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td class="effectGray label-effect"><label for="">Level of Main Teaching Input</label></td>
                                        <td v-if="ispreprimary" class="effectGray label-effect" colspan="2"><label for="">Pre PRY</label></td>
                                        <td v-if="isprimary" class="effectGray label-effect" colspan="2"><label for="">Pry</label></td>
                                        <td v-if="isjss" class="effectGray label-effect" colspan="2"><label for="">JSS</label></td>
                                        <td v-if="issss" class="effectGray label-effect" colspan="2"><label for="">SSS</label></td>
                                        <td class="effectGray label-effect" colspan="2"><label for="">TOTAl</label></td>
                                    </tr>
                                    <tr>
                                        <td class="effectGray label-effect"><label for="">Highest qualification</label></td>
                                        <td v-if="ispreprimary" class="effectGray label-effect"><label for="">Male</label></td>
                                        <td v-if="ispreprimary" class="effectGray label-effect"><label for="">Female</label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">Male</label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">Female</label></td>
                                        <td v-if="isjss" class="effectGray label-effect"><label for="">Male</label></td>
                                        <td v-if="isjss" class="effectGray label-effect"><label for="">Female</label></td>
                                        <td v-if="issss" class="effectGray label-effect"><label for="">Male</label></td>
                                        <td v-if="issss" class="effectGray label-effect"><label for="">Female</label></td>
                                        <td class="effectGray label-effect"><label for="">Male</label></td>
                                        <td class="effectGray label-effect"><label for="">Female</label></td>
                                    </tr>
                                    <tr v-for="(source,index) in fetch_teachingqualification">
                                        <td class="text label-effect"><label>@{{source.value}}</label></td>
                                        <td v-if="ispreprimary"><input class="theme" type="number" min="0" v-on:change="tq_preprimary_male_tot" name="" v-model.number="source.preprimary_male" id=""></td>
                                        <td v-if="ispreprimary"><input class="theme" type="number" min="0" v-on:change="tq_preprimary_female_tot" name="" v-model.number="source.preprimary_female" id=""></td>
                                        <td v-if="isprimary"><input class="theme" type="number" min="0" v-on:change="tq_primary_male_tot" name="" v-model.number="source.primary_male" id=""></td>
                                        <td v-if="isprimary"><input class="theme" type="number" min="0" v-on:change="tq_primary_female_tot" name="" v-model.number="source.primary_female" id=""></td>
                                        <td v-if="isjss"><input class="theme" type="number" min="0" v-on:change="tq_jss_male_tot" name="" v-model.number="source.jss_male" id=""></td>
                                        <td v-if="isjss"><input class="theme" type="number" min="0" v-on:change="tq_jss_female_tot" name="" v-model.number="source.jss_female" id=""></td>
                                        <td v-if="issss"><input class="theme" type="number" min="0" v-on:change="tq_sss_male_tot" name="" v-model.number="source.sss_male" id=""></td>
                                        <td v-if="issss"><input class="theme" type="number" min="0" v-on:change="tq_sss_female_tot" name="" v-model.number="source.sss_female" id=""></td>

                                        <td><input type="number" min="0" disabled="disabled" name="" v-model.number="((ispreprimary)?source.preprimary_male*1:0)+((isprimary)?source.primary_male*1:0)+((isjss)?source.jss_male*1:0)+((issss)?source.sss_male*1:0)+source.sss_male*0" id=""></td>
                                        <td><input type="number" min="0" disabled="disabled" name="" v-model.number="((ispreprimary)?source.preprimary_female*1:0)+((isprimary)?source.primary_female*1:0)+((isjss)?source.jss_female*1:0)+((issss)?source.sss_female*1:0)+source.sss_female*0" id=""></td>


                                    </tr>
                                    <tr>
                                        <td class="text label-effect"><label>Total</label></td>
                                        <td v-if="ispreprimary"><input disabled="disabled" v-model="tq_preprimary_male_total" type="number" min="0" name="" id=""></td>
                                        <td v-if="ispreprimary"><input disabled="disabled" v-model="tq_preprimary_female_total" type="number" min="0" name="" id=""></td>
                                        <td v-if="isprimary"><input disabled="disabled" v-model="tq_primary_male_total" type="number" min="0" name="" id=""></td>
                                        <td v-if="isprimary"><input disabled="disabled" v-model="tq_primary_female_total" type="number" min="0" name="" id=""></td>
                                        <td v-if="isjss"><input disabled="disabled" v-model="tq_jss_male_total" type="number" min="0" name="" id=""></td>
                                        <td v-if="isjss"><input disabled="disabled" v-model="tq_jss_female_total" type="number" min="0" name="" id=""></td>
                                        <td v-if="issss"><input disabled="disabled" v-model="tq_sss_male_total" type="number" min="0" name="" id=""></td>
                                        <td v-if="issss"><input disabled="disabled" v-model="tq_sss_female_total" type="number" min="0" name="" id=""></td>
                                        <td><input disabled="disabled" v-model="tq_male_total" type="number" min="0" name="" id=""></td>
                                        <td><input disabled="disabled" v-model="tq_female_total" type="number" min="0" name="" id=""></td>
                                    </tr>
                                </table>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveTeacherQualification">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveTeacherQualification">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab28">
                            <p class="label">F. 1 Number of Pupils/Students Textbooks available to Pupils on average in the Current Academic Year </p>
                            <form action="" class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td class="effectGray label-effect"><label for=""></label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">PRY1</label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">PRY2</label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">PRY3</label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">PRY4</label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">PRY5</label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">PRY6</label></td>
                                        <td v-if="isjss" class="effectGray label-effect"><label for="">JSS1</label></td>
                                        <td v-if="isjss" class="effectGray label-effect"><label for="">JSS2</label></td>
                                        <td v-if="isjss" class="effectGray label-effect"><label for="">JSS3</label></td>
                                        <td v-if="issss" class="effectGray label-effect"><label for="">SSS1</label></td>
                                        <td v-if="issss" class="effectGray label-effect"><label for="">SSS2</label></td>
                                        <td v-if="issss" class="effectGray label-effect"><label for="">SSS3</label></td>
                                    </tr>
                                    <tr v-for="(source,index) in fetch_books">
                                        <td class="text label-effect"><label>@{{source.value}}</label></td>
                                        <td v-if="isprimary"><input class="theme" v-model="source.prm1_pupils_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isprimary"><input class="theme" v-model="source.prm2_pupils_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isprimary"><input class="theme" v-model="source.prm3_pupils_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isprimary"><input class="theme" v-model="source.prm4_pupils_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isprimary"><input class="theme" v-model="source.prm5_pupils_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isprimary"><input class="theme" v-model="source.prm6_pupils_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isjss"><input class="theme" v-model="source.jss1_pupils_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isjss"><input class="theme" v-model="source.jss2_pupils_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isjss"><input class="theme" v-model="source.jss3_pupils_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="issss"><input class="theme" v-model="source.sss1_pupils_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="issss"><input class="theme" v-model="source.sss2_pupils_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="issss"><input class="theme" v-model="source.sss3_pupils_books" type="number" min="0" name="" id=""></td>
                                    </tr>
                                </table>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveStudents">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveStudents">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab29">
                            <p class="label">F. 2 Number of Teachers’ Textbooks available to teachers on average in the Current Academic Year</p>
                            <form action="" class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td class="effectGray label-effect"><label for=""></label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">PRY1</label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">PRY2</label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">PRY3</label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">PRY4</label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">PRY5</label></td>
                                        <td v-if="isprimary" class="effectGray label-effect"><label for="">PRY6</label></td>
                                        <td v-if="isjss" class="effectGray label-effect"><label for="">JSS1</label></td>
                                        <td v-if="isjss" class="effectGray label-effect"><label for="">JSS2</label></td>
                                        <td v-if="isjss" class="effectGray label-effect"><label for="">JSS3</label></td>
                                        <td v-if="issss" class="effectGray label-effect"><label for="">SSS1</label></td>
                                        <td v-if="issss" class="effectGray label-effect"><label for="">SSS2</label></td>
                                        <td v-if="issss" class="effectGray label-effect"><label for="">SSS3</label></td>
                                    </tr>
                                    <tr v-for="(source,index) in fetch_books">
                                        <td class="text label-effect"><label>@{{source.value}}</label></td>
                                        <td v-if="isprimary"><input class="theme" v-model="source.prm1_teachers_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isprimary"><input class="theme" v-model="source.prm2_teachers_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isprimary"><input class="theme" v-model="source.prm3_teachers_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isprimary"><input class="theme" v-model="source.prm4_teachers_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isprimary"><input class="theme" v-model="source.prm5_teachers_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isprimary"><input class="theme" v-model="source.prm6_teachers_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isjss"><input class="theme" v-model="source.jss1_teachers_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isjss"><input class="theme" v-model="source.jss2_teachers_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="isjss"><input class="theme" v-model="source.jss3_teachers_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="issss"><input class="theme" v-model="source.sss1_teachers_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="issss"><input class="theme" v-model="source.sss2_teachers_books" type="number" min="0" name="" id=""></td>
                                        <td v-if="issss"><input class="theme" v-model="source.sss3_teachers_books" type="number" min="0" name="" id=""></td>
                                    </tr>
                                </table>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="save" v-on:click="saveTeachers">save</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveTeachers">next</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab30">
                            <form action="">
                                <p class="label">Attestation by Head Teacher</p>
                                <div class="row">
                                    <div class="question">
                                        <p>Name</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" v-model="attestation_headteacher_name" name="" id="" required="required" placeholder="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Telephone</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" v-model="attestation_headteacher_telephone" name="" id="" required="required" placeholder="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Date</p>
                                    </div>
                                    <div class="answer">
                                        <input type="date" class="theme" v-model="attestation_headteacher_signdate" name="" id="">
                                    </div>
                                </div>
                                <p class="label">Attestation by Enumerator</p>
                                <div class="row">
                                    <div class="question">
                                        <p>Name</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" name="" v-model="attestation_enumerator_name" id="" required="required" placeholder="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Position</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" name="" v-model="attestation_enumerator_position" id="" required="required" placeholder="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Telephone</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" name="" id="" v-model="attestation_enumerator_telephone" required="required" placeholder="">
                                    </div>
                                </div>
                                <p class="label">Attestation by Supervisor</p>
                                <div class="row">
                                    <div class="question">
                                        <p>Name</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" v-model="attestation_supervisor_name" name="" id="" required="required" placeholder="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Position</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" v-model="attestation_supervisor_position" name="" id="" required="required" placeholder="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="question">
                                        <p>Telephone</p>
                                    </div>
                                    <div class="answer">
                                        <input type="text" class="theme" name="" v-model="attestation_supervisor_telephone" id="" required="required" placeholder="">
                                    </div>
                                </div>
                            </form>
                            <div id="button-group">
                                <ul>
                                    <li><button id="previous">previous</button></li>
                                    <li><button id="next" class="theme" v-on:click="saveUndertaking">finalize save</button></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tab31">
                            <div class="completedIcon"></div>
                            <h3 class="inform">You have completed this form.</h3>
                            <h4 class="action">Click <a href="" id='returnTab'>here</a> to go back</h4>
                            <br />
                            <hr />
                            <h3 class="action">Click <a v-bind:href="datapreviewurl" target="_blank" id=''>here</a> to PREVIEW data posted</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/parsley.min.js')}}"></script>
    <script src="{{asset('assets/js/functions.js')}}"></script>
    <script src="{{asset('assets/js/nemis_functionality_private.js')}}"></script>
    <script src="{{asset('assets/js/users.js')}}"></script>

    <script>
        const returnTab = document.getElementById('returnTab');
        returnTab.addEventListener('click', (e) => {
            e.preventDefault();
            document.getElementById('tab30').classList.remove('show-current-tab');
            var currentLink = window.location.href;
            window.location.assign(currentLink);
        })
    </script>
</body>

</html>